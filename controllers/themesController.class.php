<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_themesController extends Applications_BaseController {
    public function adminOnly($redirect) {
        $this->helpers->adminNext($redirect);
    }
    function checkPermission() {
        $this->adminOnly($this->helpers->getUrl() . 'auth/login/&ref='.$this->helpers->curPageURL());
    }
    function index() {
        $forms_obj = new Models_FormsModel($this->registry);
        $themes_obj = new Models_ThemesModel($this->registry);
        $this->view->title = 'Theme Builder';
        $this->view->render('themes');
    }
    function postReq() {
        $routers = $this->registry->router->getRouters();
        $req = $this->registry->req;
        $ajax = new Controllers_AjaxController($this->registry);
        if($routers['action'] == 'loadThemes') {
            $ajax->themes_loadThemes();
        } elseif($routers['action'] == 'save') {
            $ajax->themes_save();
        } elseif($routers['action'] == 'rename') {
            $ajax->themes_rename();
        } elseif($routers['action'] == 'delete') {
            $ajax->themes_delete();
        }
        echo $ajax->getJson();
    }
}