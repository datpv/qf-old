<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_reportsController extends Applications_BaseController {
    public function adminOnly($redirect) {
        $this->helpers->adminNext($redirect);
    }
    function index() {
        $siteUrl = $this->helpers->getUrl();
        $routers = $this->registry->router->getRouters();
        $reportId = $routers['id'];
        if(!empty($reportId)) {
            $this->report($reportId);
            return;
        }
        $this->adminOnly($siteUrl . 'auth/login/&ref='.$this->helpers->curPageURL());
        $reports_obj = new Models_ReportsModel($this->registry);
        $forms_obj = new Models_FormsModel($this->registry);
        $reports_result = $reports_obj->getReports('','report_create','DESC');
        $this->view->count = count($reports_result);
        $forms_result = $forms_obj->getForms('*',"`form_approved` = 'Y'");
        $this->view->form_count = count($forms_result);
        $this->view->reports_result = ($reports_result)?$reports_result:array();
        $this->view->forms_result = ($forms_result)?$forms_result:array();
        $this->view->render('reports');
    }
    function report($reportId) {
        $reports_obj = new Models_ReportsModel($this->registry);
        $notfound_obj = new Controllers_notfoundController($this->registry);
        $reportId = $this->helpers->escape_string($reportId);
        if(!$report_result = $reports_obj->getReport($reportId,'','report_uuid','',"AND `report_status` = 'A'")) {
            $notfound_obj->index();
            exit();
        }
        /*
        * Report
        */
        $report_pass = $report_result['report_password'];
        if(!empty($report_pass)) {
            if(!isset($_SESSION['report_'.$reportId])) {
                $this->view->pass_require = ' Unauthorized access.';
                $this->view->render('password_protect','only');
                return;
            }
        }
        $report = ($report_result['report_content']);
        $_report = $this->helpers->json_decode_to_array($report);
        /*
        * Form
        */
        $form_id = $report_result['form_id'];
        $forms_obj = new Models_FormsModel($this->registry);
        if(!$form = $forms_obj->getForm('',$form_id)) {
            $notfound_obj->index();
            exit();
        }
        $formId = $form['form_uuid'];
        //$form = stripslashes($form['form_content']);
        $form = ($form['form_content']);
        $_form = $this->helpers->json_decode_to_array($form);
        $fields = $_form['Fields'];
        /*
        * Entry
        */
        $entries_obj = new Models_EntriesModel($this->registry);
        $_entry = $entries_obj->entriesInForm($form_id);
        $entty_count = count($_entry);
        $entries = array();
        if($_entry) foreach($_entry as $entry) {
            $posts = ($entry['entry_content']);
            $posts = $this->helpers->json_decode_to_array_not_in_mysql($posts);
            foreach($posts as $k => $post) {
                if(substr($k,0,5) == 'Field') {
                    $entries[$k][] = trim($post);
                }
            }
        }
        /*
        * 
        */
        
        $this->helpers->aasort($_report['Graphs'],'Zone');
        $this->helpers->aasort($_report['Graphs'],'Position');
        $_report['Graphs'] = array_values($_report['Graphs']);
        $z1 = $z2 = $z3 = $html = '';
        if(!empty($_report['Graphs']))
        foreach($_report['Graphs'] as $graph) {
            ob_start();
            if($graph['Typeof'] == 'fieldChart') :
            ?>
            <div class="wfo_widget fc">
        		<table cellspacing="0" class="choices ">
        			<caption>
        				<h4>
        					<?php echo $graph['Name']; ?>
        				</h4>
        				<span class="fcNav">
        					<a>&nbsp;</a>
        				</span>
        			</caption>
        			<thead>
        				<tr>
        					<th>
        						Choices
        					</th>
                            <?php if($graph['Detail'] != 'count'): ?>
        					<th class="percent">
        						Percentage
        					</th>
                            <?php 
                                endif; 
                                if($graph['Detail'] != 'percent' ):
                            ?>
        					<th class="count">
        						Count
        					</th>
                            <?php endif; ?>
        				</tr>
        			</thead>
                    <?php
                    $field = $this->helpers->getField($fields, $graph['FieldName']);
                    $choices = $this->helpers->getChoices($field, $entries, $graph['FieldName']);
                    ?>
        			<tfoot>
        				<tr>
        					<td class="empty">
        						&nbsp;
        					</td>
        					<th>
        						Total
        					</th>
        					<td class="count">
        						<?php echo $choices['total']; ?>
        					</td>
        				</tr>
        				<tr>
        					<td class="empty">
        						&nbsp;
        					</td>
        					<th>
        						<em>
        							Unanswered
        						</em>
        					</th>
        					<td class="count">
        						<em>
        							<?php
                                    if($field['Typeof'] == 'checkbox' || $field['Typeof'] == 'likert')
                                    echo ($entty_count - $choices['unanswered']);
                                    else
                                    echo $choices['unanswered']; 
                                    ?>
        						</em>
        					</td>
        				</tr>
        			</tfoot>
        			<tbody>
                        <?php
                        $i=0;
                        if(!empty($choices['choices'])) :
                        $remaining_fields = $remaining_fields2 = array();
                        if(isset($field['Choices'])) {
                            $remaining_fields = $field['Choices'];
                        }
                        $k_f2 = array();
                        foreach($choices['choices'] as $k => $choice) {
                            $i++;
                            if(!empty($remaining_fields)) {
                                foreach($remaining_fields as $k_f => $r_f) {
                                    if(!in_array($k_f,$k_f2)) {
                                        $remaining_fields2[$k_f] = $r_f;
                                    }
                                    if(in_array($k,$r_f)) {
                                        unset($remaining_fields2[$k_f]);
                                        $k_f2[] = $k_f;
                                    }
                                }
                                
                            }
                            
                            if($i == 8) $i=1;
                            $percent = number_format($choice/$choices['total']*100,2);
                        ?>
        				<tr class="color<?php echo $i; ?>">
        					<th>
        						<div>
        							<?php echo $k; ?>
        						</div>
        					</th>
                            <?php if($graph['Detail'] != 'count'): ?>
        					<td class="percent">
        						<div class="bar" style="width:<?php echo $percent; ?>%">
        						</div>
        						<var>
        							<?php echo $percent; ?>%
        						</var>
        					</td>
                            <?php 
                                endif; 
                                if($graph['Detail'] != 'percent' ):
                            ?>
        					<td class="count">
        						<?php echo $choice; ?>
        					</td>
                            <?php endif; ?>
        				</tr>
                        <?php
                        }
                        foreach($remaining_fields2 as $r_k => $_v) {
                            $i++; if($i == 8) $i=1;
                        ?>
                        <tr class="color<?php echo $i; ?>">
        					<th>
        						<div>
                                    <?php echo $_v['Choice']; ?>
        						</div>
        					</th>
                            <?php if($graph['Detail'] != 'count'): ?>
        					<td class="percent">
        						<div class="bar" style="width:<?php echo '0'; ?>%">
        						</div>
        						<var>
        							<?php echo '0'; ?>%
        						</var>
        					</td>
                            <?php 
                                endif; 
                                if($graph['Detail'] != 'percent' ):
                            ?>
        					<td class="count">
        						0
        					</td>
                            <?php endif; ?>
        				</tr>
                        <?php
                        }
                        endif;
                        ?>
        			</tbody>
        		</table>
        	</div>
            <?php
            endif;
            $html = ob_get_contents();
            ob_clean();
            if($graph['Zone'] == 1) {
                $z1 .= $html;
            } elseif($graph['Zone'] == 2) {
                $z2 .= $html;
            } elseif($graph['Zone'] == 3) {
                $z3 .= $html;
            }
        }
        $this->view->title = $_report['Name'];
        $this->view->z1 = $z1;
        $this->view->z2 = $z2;
        $this->view->z3 = $z3;
        $this->view->report_detail = true;
        $this->view->report_result = $report_result;
        $this->view->_report = $_report;
        $this->view->render('report');
    }
    function postReq() {
        $req = $this->registry->req;
        $ajax = new Controllers_AjaxController($this->registry);
        $ajax->index($req, $this);
        if(!$this->getAllowNext()) echo $ajax->getJson();
    }
}