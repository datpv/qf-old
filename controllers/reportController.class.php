<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_reportController extends Applications_BaseController {
    public function adminOnly($redirect) {
        $this->helpers->adminNext($redirect);
    }
    function checkPermission() {
        $this->adminOnly($this->helpers->getUrl() . 'auth/login/&ref='.$this->helpers->curPageURL());
    }
    function index() {
		
    }
    function auto_builder() {
        $routers = $this->registry->router->getRouters();
        $forms_obj = new Models_FormsModel($this->registry);
        $errorFields = array();$entry = $_form = '[]';$next = false;
        $e_f = false;
        $formId = $routers['id'];
        if(!$form_result = $forms_obj->getForm($formId,'')) {
            echo 'Form Auto Builder Fail <br>';
            echo $forms_obj->getErrors();
        }
        $form_id = $form_result['form_id'];
        $form_orgn = $this->helpers->json_decode_to_array($form_result['form_content']);
        $this->view->form_orgn = $form_orgn;
        $this->view->_form = $_form;
        $this->view->form_id = $form_id;
        $this->view->render('report_autobuilder','only');
    }
    function builder() {
        $result = array();
        $catetory = $this->registry->category;
        $routers = $this->registry->router->getRouters();
        $errorFields = array();$entry = $_form = '[]';$next = false;
        $e_f = false;
        $forms_obj = new Models_FormsModel($this->registry);
        $reports_obj = new Models_ReportsModel($this->registry);
        $reports_c_obj = new Controllers_reportsController($this->registry);
        $results = $forms_obj->getForms('*',"`form_approved` = 'Y'",'form_create','DESC');
        if(!$results) {
            $reports_c_obj->index();
            exit();
        }
        /*
        * Report
        */
        $reportId = $routers['id'];
        $reportId = $this->helpers->escape_string($reportId);
        $_report = $_form = array();
        
        if($report_result = $reports_obj->getReport($reportId,'')) {
            $report = ($report_result['report_content']);
            $_report = $this->helpers->json_decode_to_array($report);
            $_report['ReportId'] = $reportId;
            $_report['Url'] = $reportId;
            
            $this->helpers->aasort($_report['Graphs'],'Zone');
            $this->helpers->aasort($_report['Graphs'],'Position');
            $_report['Graphs'] = array_values($_report['Graphs']);
            /*
            * Form
            */
            $form_id = $report_result['form_id'];
            if($form = $forms_obj->getForm('',$form_id)) {
                $formUuid = $form['form_uuid'];
                //$form = stripslashes($form['form_content']);
                $form = ($form['form_content']);
                $_form = $this->helpers->json_decode_to_array($form);
                $_form['FormId'] = $formUuid;
                $_form['Url'] = $formUuid;
                foreach($catetory as $cat) {
                    array_push($_form['Fields'],$cat);
                }
                $e_f = true;
            }
        }
        if(!$report_result || !$e_f) {
            /*
            * Form
            */
            if($results[0]) {
                $result = $results[0];
                $form = stripslashes($result['form_content']);
                $form = ($result['form_content']);
                $_form = $form_orgn = $this->helpers->json_decode_to_array($form);
                foreach($catetory as $cat) {
                    array_push($_form['Fields'],$cat);
                }
            }
        }
        
        /*
        */
        $html = '';
        $this->view->results = $results;
        $this->view->result = $result;
        $this->view->_report = $_report;
        $this->view->report_result = $report_result;
        $this->view->_form = $_form;
        $this->view->render('report_builder');
    }
    function postReq() {
        $req = $this->registry->req;
        $ajax = new Controllers_AjaxController($this->registry);
        $ajax->index($req);
        echo $ajax->getJson();
    }
}