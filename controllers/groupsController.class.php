<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_groupsController extends Applications_BaseController {
    public function adminOnly($redirect) {
        $this->helpers->adminNext($redirect);
    }
    function checkPermission() {
        $this->adminOnly($this->helpers->getUrl() . 'auth/login/&ref='.$this->helpers->curPageURL());
    }
    function index() {
        $groups_obj = new Models_GroupsModel($this->registry);
		$groups_result = $groups_obj->getGroups();
        if($groups_result) foreach($groups_result as $k => $group) {
            $group_id = $group['group_id'];
            $user_group_meta = $groups_obj->getGroupsMeta("`group_id` = '$group_id'");
            $group['user_group_meta'] = $user_group_meta;
            $groups_result[$k] = $group;
        }
        $this->view->groups_result = $groups_result;
        $this->view->render('groups');
    }
    function postReq() {
        $req = $this->registry->req;
        $ajax = new Controllers_AjaxController($this->registry);
        $ajax->index($req);
        echo $ajax->getJson();
    }
}