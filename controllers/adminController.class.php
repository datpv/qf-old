<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_adminController extends Applications_BaseController {
    public static $form_limit = 15;
    public function adminOnly($redirect) {
        $this->helpers->adminNext($redirect);
    }
    function checkPermission() {
        $this->adminOnly($this->helpers->getUrl() . 'auth/login/&ref='.$this->helpers->curPageURL());
    }
    function index() {
        $forms_obj = new Models_FormsModel($this->registry);
        $themes_obj = new Models_ThemesModel($this->registry);
        $entries_obj = new Models_EntriesModel($this->registry);
        $categories_obj = new Models_CategoriesModel($this->registry);
        $users_obj = new Models_UsersModel($this->registry);
        /*
        * Get Forms, Themes, New Entries Count
        */
        $this->view->categories = $categories_obj->getCategories();
        $form_limit = self::$form_limit;
        $this->view->cate_id = $cate_id = isset($_COOKIE['cate_id'])?$_COOKIE['cate_id']:'0';
        if(!empty($cate_id)) {
            $forms_result = $forms_obj->getFormsInCategory($cate_id,'Y','','',"0,$form_limit");
        } else {
            $forms_result = $forms_obj->getForms('SQL_CALC_FOUND_ROWS *',"`form_approved` = 'Y'", 'form_create', 'DESC',"0,$form_limit");
        }
        
        $this->view->total_page = $total_page = $forms_obj->getCalcNumRows();
        $this->view->total_paged = ceil($total_page / $form_limit);
        $theme_result = $themes_obj->getThemes();
        $forms_result = ($forms_result)?$forms_result:array();
        $this->view->count = count($forms_result);
        foreach($forms_result as $k => $form) {
            $forms_result[$k]['entry_count'] = $entries_obj->getEntriesNumber($form['form_id']);
            $forms_result[$k]['user_by'] = $users_obj->getUser("`user_id` = '$form[form_user]'");
        }
        /*
        *
        */
        $this->view->paged = 1;
        $this->view->forms_result = $forms_result;
        $this->view->theme_result = $theme_result;
        $this->view->title = 'Form Manager';
		$this->view->render('admin');
        
    }
    function postReq() {
        $req = $this->registry->req;
        $ajax = new Controllers_AjaxController($this->registry);
        $ajax->index($req);
        echo $ajax->getJson();
    }
}