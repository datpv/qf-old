<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_cachesController extends Applications_BaseController {
    public function adminOnly($redirect) {
        $this->helpers->adminNext($redirect);
    }
    function checkPermission() {
        $this->adminOnly($this->helpers->getUrl() . 'auth/login/&ref='.$this->helpers->curPageURL());
    }
    function index() {
        $routers = $this->registry->router->getRouters();
		$page_limit = 15;
        $paged = $this->registry->router->paged;
        $_paged = $paged*$page_limit;
        $users_obj = new Models_UsersModel($this->registry);
        $cached_obj = new Models_CachedModel($this->registry);
        $user_count_total = $users_obj->getUsersRows();
        $users_result = $users_obj->getUsers('SQL_CALC_FOUND_ROWS *',"`user_status` = 'A'",'','',"$_paged, $page_limit");
        $all_user_count = $users_obj->getCalcNumRows();
        if(!$users_result) {
            $users_result = $users_obj->getUsers('*',"`user_status` = 'A'",'','',"0, $page_limit");
            $paged = 1;
        }
        $user_count = count($users_result);
        if($user_count > 0) {
            $pages = ceil($user_count_total / ($user_count));
            $total_paged = ceil($user_count_total/$page_limit);
        }
        foreach($users_result as $k => $user) {
            $user_id = $user['user_id'];
            $user['as_count'] = $cached_obj->getASRows("`cache_user_id` = '$user_id'");
            $user['nb_count'] = $cached_obj->getNBRows("`cache_user_id` = '$user_id'");
            $user['sced_count'] = $cached_obj->getSCEDRows("`cache_user_id` = '$user_id'");
            $user['nbed_count'] = $cached_obj->getNBEDRows("`cache_user_id` = '$user_id'");
            $users_result[$k] = $user;
        }
        $this->view->users_result = $users_result;
        $this->view->user_count = $user_count;
        $this->view->pages = $pages;
        $this->view->paged = $paged;
        $this->view->total_paged = $total_paged;
        $this->view->all_user_count = $all_user_count;
        $this->view->cached_users_result = $cached_obj->getCachedEntries('*','users');
        $this->view->title = 'Caches Manager';
        $this->view->render('caches_manager');
    }
    function echoTd1($title, $value, $extra_values = array()) {
        $title = trim($title);
        if(empty($title)) return '';
        ?>
        <tr style="background-color:#F2F2F2">
            <th style="vertical-align:top;color:#222;text-align:left;padding:7px 9px 7px 9px;border-top:1px solid #eee;">
                <?php echo $title; ?>
            </th>
            <td style="vertical-align:top;color:#333;width:60%;padding:7px 9px 7px 0;border-top:1px solid #eee;">
                <?php
                if(is_array($value)) {
                ?>
                <ul style="list-style:inside;margin:0;padding:0;">
                    <?php foreach($value as $v) { ?>
                	<li style="margin:0;padding:0 0 4px 0;"><?php echo $v; ?></li>
                    <?php } ?>
                </ul>
                <?php
                } else {
                ?>
                <div>
                	<?php echo $value; ?>
                </div>
                <?php  
                }
                ?>
            </td>
        </tr>
        <?php
        $html = ob_get_contents();
        ob_clean();
        return $html;
    }
    function echoTd2($title, $value, $extra_values = array()) {
        $title = trim($title);
        if(empty($title)) return '';
        ?>
        <tr style="background-color:#f9f9f9">
            <th style="vertical-align:top;color:#222;text-align:left;padding:7px 9px 7px 9px;border-top:1px solid #eee;">
                <?php echo $title; ?>
            </th>
            <td style="vertical-align:top;color:#333;width:50%;padding:7px 9px 7px 0;border-top:1px solid #eee;">
                <?php
                if(is_array($value)) {
                ?>
                <ul style="list-style:inside;margin:0;padding:0;">
                    <?php foreach($value as $v) { ?>
                	<li style="margin:0;padding:0 0 4px 0;"><?php echo $v; ?></li>
                    <?php } ?>
                </ul>
                <?php
                } else {
                ?>
                <div>
                	<?php echo $value; ?>
                </div>
                <?php  
                }
                ?>
            </td>
        </tr>
        <?php
        $html = ob_get_contents();
        ob_clean();
        return $html;
    }
    function echoAsTd1($title, $value, $extra_values = array(), $answer = '', $atype = 'wrong') {
        //ob_start();
        $title = trim($title);
        if(empty($title)) return '';
        ?>
        <tr style="background-color:#F2F2F2">
        <th style="vertical-align:top;color:#222;text-align:left;padding:7px 9px 7px 9px;border-top:1px solid #eee;">
        <?php echo $title; ?>
        </th>
        <td style="vertical-align:top;color:#333;width:50%;padding:7px 9px 7px 0;border-top:1px solid #eee;">
        <?php
        if(is_array($value)) {
        ?>
        <ul style="list-style:inside;margin:0;padding:0;">
            <?php foreach($value as $v) { ?>
        	<li style="margin:0;padding:0 0 4px 0;"><?php echo $v; ?></li>
            <?php } ?>
        </ul>
        <?php
        } else {
        ?>
        <div>
        	<?php echo $value; ?>
        </div>
        <?php  
        }
        if(!empty($extra_values)) {
        ?>
        <div class="extras">
            <ul>
        <?php
        foreach($extra_values as $extra_value) {
        
        ?>
        <li>
        <h5><?php echo $extra_value['title']; ?></h5>
        <?php
            if(is_array($extra_value['values'])) {
                echo implode(' | ',$extra_value['values']);
            } else {
                echo $extra_value['values'];
            }
        ?>
        </li>
        <?php
        }
        ?>
            </ul>
        </div>
        <?php
        }
        ?>
        </td>
        <?php
        if(!empty($answer)) {
        ?>
        <td class="<?php echo $atype; ?>">
            <?php echo $answer; ?>
        </td>
        <?php  
        }
        ?>
        </tr>
        <?php
        $html = ob_get_contents();
        ob_clean();
        return $html;
    }
    function echoAsTd2($title, $value, $extra_values = array(), $answer = '', $atype = 'wrong') {
        //ob_start();
        $title = trim($title);
        if(empty($title)) return '';
        ?>
        <tr style="background-color:#f9f9f9">
        <th style="vertical-align:top;color:#222;text-align:left;padding:7px 9px 7px 9px;border-top:1px solid #eee;">
        <?php echo $title; ?>
        </th>
        <td style="vertical-align:top;color:#333;width:60%;padding:7px 9px 7px 0;border-top:1px solid #eee;">
        <?php
        if(is_array($value)) {
        ?>
        <ul style="list-style:inside;margin:0;padding:0;">
            <?php foreach($value as $v) { ?>
        	<li style="margin:0;padding:0 0 4px 0;"><?php echo $v; ?></li>
            <?php } ?>
        </ul>
        <?php
        } else {
        ?>
        <div>
        	<?php echo $value; ?>
        </div>
        <?php  
        }
        if(!empty($extra_values)) {
        ?>
        <div class="extras">
            <ul>
        <?php
        foreach($extra_values as $extra_value) {
        
        ?>
        <li>
        <h5><?php echo $extra_value['title']; ?></h5>
        <?php
            if(is_array($extra_value['values'])) {
                echo implode(' | ',$extra_value['values']);
            } else {
                echo $extra_value['values'];
            }
        ?>
        </li>
        <?php
        }
        ?>
            </ul>
        </div>
        <?php
        }
        ?>
        </td>
        <?php
        if(!empty($answer)) {
        ?>
        <td class="<?php echo $atype; ?>">
            <?php echo $answer; ?>
        </td>
        <?php  
        }
        ?>
        </tr>
        <?php
        $html = ob_get_contents();
        ob_clean();
        return $html;
    }
    function getFieldDisplay($fields, $posts, $extra = '') {
        $fieldValues = array();
        foreach($fields as $field) {
            $type = $field['Typeof'];
            $values = array();
            if($type == 'checkbox') {
                foreach($field['SubFields'] as $subF) {
                    if(!empty($posts[$extra.'Field'.$subF['ColumnId']])) {
                        $values[] = $posts[$extra.'Field'.$subF['ColumnId']];
                    }
                }
                $fieldValues[] = array(
                    'field_id' => $field['ColumnId'],
                    'title' => $field['Title'],
                    'values' => $values
                );
            } elseif($type == 'likert' || $type == 'address') {
                $validation = isset($field['Validation'])?$field['Validation']:'';
                $post_values = '';
                if($validation == 'na' || empty($validation)) {
                    foreach($field['SubFields'] as $subF) {
                        $post_value = '';
                        if(!empty($posts[$extra.'Field'.$subF['ColumnId']])) {
                            $post_value = $posts[$extra.'Field'.$subF['ColumnId']];
                        }
                        $fieldValues[] = array(
                            'field_id' => $subF['ColumnId'],
                            'title' => $subF['Title'],
                            'values' => $post_value
                        );
                    }
                    
                } elseif($validation == 'dc') {
    				foreach($field['SubFields'] as $subF) {
						foreach($field['Choices'] as $k => $choiC) {
							$post_value = $posts[$extra.'Field'.$subF['ColumnId'].'_'.($k+1)];
							if(!empty($post_value)) {
								$values[] = $post_value;
							}
						}
                        $fieldValues[] = array(
                            'field_id' => $subF['ColumnId'],
                            'title' => $subF['Title'],
                            'values' => $values
                        );
					}
                }
                
                
            } else {
                $post_value = isset($posts[$extra.'Field'.$field['ColumnId']])?$posts[$extra.'Field'.$field['ColumnId']]:'';
                if(!empty($post_value)) {
                    $rangeMax = isset($field['RangeMax'])?$field['RangeMax']:'';
                    $rangeMin = isset($field['RangeMin'])?$field['RangeMin']:'0';
                    $rangeType = isset($field['RangeType'])?$field['RangeType']:'';
                    if($type == 'date' || $type == 'eurodate') {
                        $dates = isset($posts[$extra.'Field'.$field['ColumnId'].'-1'])?$posts[$extra.'Field'.$field['ColumnId'].'-1']:'';
                        $dates .= '/';
                        $dates .= isset($posts[$extra.'Field'.$field['ColumnId'].'-2'])?$posts[$extra.'Field'.$field['ColumnId'].'-2']:'';
                        $dates .= '/';
                        $dates .= isset($posts[$extra.'Field'.$field['ColumnId']])?$posts[$extra.'Field'.$field['ColumnId']]:'';
                        $post_value = $dates;
                    } elseif($type == 'email') {
                    } elseif($type == 'url') {
                    } elseif($type == 'time') {
                        $post_values = $post_value . ':' . $posts[$extra.'Field'.$field['ColumnId'].'-1'] . ':' . $posts[$extra.'Field'.$field['ColumnId'].'-2'] . ' ' . $posts[$extra.'Field'.$field['ColumnId'].'-3'];
                        $post_value = $post_values;
                    } elseif($type == 'phone') {
                        $post_values = $post_value . $posts[$extra.'Field'.$field['ColumnId'].'-1'] . $posts[$extra.'Field'.$field['ColumnId'].'-2'];
                        $post_value= $post_values;
                    } elseif($type == 'number') {
                    } elseif($type == 'text') {
                    }
                }
                $fieldValues[] = array(
                    'field_id' => $field['ColumnId'],
                    'title' => $field['Title'],
                    'values' => $post_value
                );
            }
        }
        return $fieldValues;
    }
    function getFormASDisplayResult($form_nb, $entry_content, $user, $review = false, $hideHeadInfo=false, $hideHeader = false) {
        $html2 = '';
        ob_start();
        $entry_id = $entry_content['entry_id'];
        $user_id = $entry_content['entry_user'];
        $entries = $this->helpers->json_decode_to_array_not_in_mysql($entry_content['entry_content']);
        $form_nb_content = $this->helpers->json_decode_to_array($form_nb['form_content']);
        $form_nb_fields = $form_nb_content['Fields'];
        if(!$hideHeader) {
        ?>
        <table cellspacing="0" style="width:100%;font-family:'Lucida Grande','Lucida Sans Unicode', Tahoma, sans-serif;">
        <tr>
        <?php if($entry_content['entry_type'] == 'AS') :
        $user_content = $this->helpers->json_decode_to_array($user['user_content']);
        if(!$review) {
            if(!$hideHeadInfo) :
            ?>
                <td>Bài Nộp Của: <?php echo $user_content['Name']; ?><br />
                <?php
                if($this->options['show_nickh2d']) {
                ?>
                Nick H2d: <?php echo $user_content['NickH2d']; ?>
                <?php
                }
                ?>
                <br /></td>
            <?php
            else :
            ?>
                <td><h3><strong>Form:</strong> <?php echo $form_nb_content['Name']; ?></h3><br /></td>
            
            <?php
            endif; 
        } else {
        ?>
            <td>Bài Nộp Của: <?php echo $user_content['Name']; ?><br />
            <?php
            if($this->options['show_nickh2d']) {
            ?>
            Nick H2d: <?php echo $user_content['NickH2d']; ?><br />
            <?php
            }
            ?>
            </td>
            <td><h3><strong>Form:</strong> <?php echo $form_nb_content['Name']; ?></h3><br /></td>
        <?php 
        }
        endif; ?>
        </tr>
        </table>
        <?php
        }
        ?>
        <table class="readonly" cellspacing="0" cellpadding="0" style="width:100%;border-bottom:1px solid #eee;font-size:12px;line-height:135%;font-family:'Lucida Grande','Lucida Sans Unicode', Tahoma, sans-serif">
            <?php
            $i = 0;
            foreach($form_nb_fields as $k => $field) {
                $values =  $titles = $extra_values = array();
                $extra_fields = isset($field['ExtraFields'])?$field['ExtraFields']:array();
                $extra_values = $this->getFieldDisplay($extra_fields,$entries,'extra_');
                $value = $title = $answer = '';
                $validation = isset($field['Validation'])?$field['Validation']:'';
                if(isset($field['SubFields']) && !empty($field['SubFields'])) {
                    $j = 0;
                    $h=$k=0;
                    foreach($field['SubFields'] as $subF) {
                        if(isset($entries['Field'.$subF['ColumnId']])) {
                            if(!empty($entries['Field'.$subF['ColumnId']])) {
                                if($field['Typeof'] == 'likert') {
                                    $titles[] = $subF['ChoicesText'];
                                } else {
                                    $titles[] = $subF['Title'];
                                }
                                
                                $values[] = $entries['Field'.$subF['ColumnId']];
                            }
                        }
                        if($field['Typeof'] == 'checkbox') {
                            $post_value = isset($entries['Field'.$subF['ColumnId']])?$entries['Field'.$subF['ColumnId']]:'';
                            if(!empty($post_value)) {
                                $k++;
                                if($subF['IsRight']) {
                                    $h++;
                                }
                            }
                            if($subF['IsRight']) {
                                $h++;
                                if(!empty($post_value)) {
                                    $k++;
                                }
                            }
                        } elseif($field['Typeof'] == 'likert' && $field['Validation'] == 'dc') {
                            $titles[$j] = $subF['Title'];
                            foreach($field['Choices'] as $k1 => $choiC) {
                                
                                if(isset($entries['Field'.$subF['ColumnId'].'_'.($k1+1)])) {
                                    if(!empty($entries['Field'.$subF['ColumnId'].'_'.($k1+1)])) {
                                        $values[$j][] = $entries['Field'.$subF['ColumnId'].'_'.($k1+1)];
                                    }
                                } else {
                                    $values[$j][] = '';
                                }
                            }
                            $j++;
                        }
                    }
                    if($h==$k && $h != 0) {
                        $answer = isset($field['RightAnswerDescription'])?$field['RightAnswerDescription']:'';
                        if(empty($answer)) {
                            $answer = 'Right';
                        }
                        $atype = 'aright';
                    } else {
                        $answer = isset($field['WrongAnswerDescription'])?$field['WrongAnswerDescription']:'';
                        if(empty($answer)) {
                            $answer = 'Wrong';
                        }
                        $atype = 'awrong';
                    }
                } else {
                    $title = $field['Title'];
                    if(isset($entries['Field'.$field['ColumnId']])) {
                        $value = $entries['Field'.$field['ColumnId']];
                        if($field['Typeof'] == 'date' || $field['Typeof'] == 'eurodate') {
                            $value = $entries['Field'.$field['ColumnId'].(($field['Typeof'] == 'eurodate')?'-2':'-1')] .'/'.$entries['Field'.$field['ColumnId'].(($field['Typeof'] == 'eurodate')?'-1':'-2')].'/'.$entries['Field'.$field['ColumnId'].''] . ' (mm/dd/yyy)';
                        } elseif($field['Typeof'] == 'time') {
                            $value = $value . ':' . $entries['Field'.$field['ColumnId'].'-1'] . ':' . $entries['Field'.$field['ColumnId'].'-2'] . ' ' . $entries['Field'.$field['ColumnId'].'-3'];
                        } elseif($field['Typeof'] == 'shortname') {
                            $value = $value . ':' . $entries['Field'.$field['ColumnId'].'-1'] . ':' . $entries['Field'.$field['ColumnId'].'-2'] . ' ' . $entries['Field'.$field['ColumnId'].'-3'];
                        }
                    }
                }
                if($field['Typeof'] == 'select' || $field['Typeof'] == 'radio') {
                    $post_value = isset($entries['Field'.$field['ColumnId']])?$entries['Field'.$field['ColumnId']]:'';
                    $b=0;
                    $rightInput = isset($field['InputRightAnswer'])?$field['InputRightAnswer']:'';
                    $rightInput = $this->helpers->analizeInput($rightInput);
                    foreach($field['Choices'] as $k => $choiC) {
        				if($choiC['IsRight']) {
        				    if($field['Typeof'] == 'radio') {
        				        if($choiC['Choice'] == 'Other') {
        				            $post_value_other = isset($entries['Field'.$field['ColumnId'].'_other_Other'])?$entries['Field'.$field['ColumnId'].'_other_Other']:'';
                                    
                                    if(!empty($post_value_other)) {
                                        $value = $post_value_other;
                                        foreach($rightInput as $ri)  {
                                           if(trim($post_value_other) == $ri) {
                                                $b++;
                                                break;
                                            } 
                                        }
                                    }
        				        } else {
        				            if(!empty($post_value) && $post_value == $choiC['Choice']) {
                                        $b++;
                                    }
        				        }
        				        
        				    } elseif($field['Typeof'] == 'select') {
        				        if(!empty($post_value) && $post_value == $choiC['Choice']) {
                                        $b++;
                                    }
        				    }
                            
                        }
        			}
                    if($b!=0) {
                        $answer = isset($field['RightAnswerDescription'])?$field['RightAnswerDescription']:'';
                        if(empty($answer)) {
                            $answer = 'Right';
                        }
                        $atype = 'aright';
                    } else {
                        $answer = isset($field['WrongAnswerDescription'])?$field['WrongAnswerDescription']:'';
                        if(empty($answer)) {
                            $answer = 'Wrong';
                        }
                        $atype = 'awrong';
                    }
                    if($review) {
                        if((!isset($_SESSION['admin_user']) && ( isset($_SESSION['user_normal']) && $_SESSION['user_normal'] != $user_id) ) && $atype == 'aright') {
                            $answer = $value = '****';
                        }
                    }
                    $title = $this->helpers->detectLink($title);
                    $value = $this->helpers->detectLink($value);
                    if($i % 2 == 0) { 
                        echo $this->echoAsTd1($title, $value, $extra_values, $answer, $atype);
                    } else {
                        echo $this->echoAsTd2($title, $value, $extra_values, $answer, $atype);
                    }
                } elseif($field['Typeof'] == 'likert') {
                    $ik = $i;
                    foreach($titles as $k => $title) {
                        if(is_array($values[$k])) {
                            $values[$k] = array_filter($values[$k]);
                        }
                        if(!empty($values[$k])) {
                            if($ik % 2 == 0) { 
                                echo $this->echoAsTd1($title, $values[$k], $extra_values);
                            } else {
                                echo $this->echoAsTd2($title, $values[$k], $extra_values);
                            }
                        }
                        $ik++;
                    }
                    $i = $ik;
                } elseif($field['Typeof'] == 'checkbox') {
                    if($review) {
                        if((!isset($_SESSION['admin_user']) && ( isset($_SESSION['user_normal']) && $_SESSION['user_normal'] != $user_id) ) && $atype == 'aright') {
                            $answer = '****';
                            $values = '****';
                        }
                    }
                    if($i % 2 == 0) { 
                        echo $this->echoAsTd1($field['Title'], $values, $extra_values, $answer, $atype);
                    } else {
                        echo $this->echoAsTd2($field['Title'], $values, $extra_values, $answer, $atype);
                    }
                } elseif($field['Typeof'] == 'shortname') {
                    $value = '';
                    foreach($titles as $k => $title) {
                        $value .= ' '.$values[$k];
                    }
                    if($i % 2 == 0) { 
                        echo $this->echoAsTd1($title, $value, $extra_values);
                    } else {
                        echo $this->echoAsTd2($title, $value, $extra_values);
                    }
                } elseif($field['Typeof'] == 'address') {
                    foreach($field['SubFields'] as $subF) {
                        $index = strtolower(str_replace(' ','-',$subF['Title']));
                        $values[] = isset($entries['Field'.$subF['ColumnId']])?$entries['Field'.$subF['ColumnId']]:'';
                    }
                    $values = array_filter($values);
                    $value = '';
                    $search = (isset($values[0])?$values[0]:'').'+'.(isset($values[1])?$values[1]:'').'+'.(isset($values[2])?$values[2]:'');
                    $value .= '<a href="http://maps.google.com/?q='.$search.'" style="text-decoration:none;" title="Show a Map of this Location" target="_blank">';
        				$value .= '<img width="16" height="16" style="padding:2px 0 0 0;float:left;" alt="" src="../images/icons/map.png" class="mapicon">';
        			$value .= '</a>';
                    $value .= '<address style="color:#333;font-style:normal;line-height:130%;padding:2px 0 2px 25px;" class="adr">';
        				
                        $value .= '<span class="street-address">'.(isset($values[0])?$values[0]:'').'</span>';
        				$value .= '<span class="extended-address"> '.(isset($values[1])?$values[1]:'').'</span><br>';			
                        $value .= '<span class="locality"> '.(isset($values[2])?$values[2]:'').'</span>'; 				
                        //$value .= '<span class="region">'.(isset($values[3])?$values[3]:'').'</span>';
        				//$value .= '<span class="postal-code">'.(isset($values[4])?$values[4]:'').'</span>';
        				//$value .= '<br>';				
                        //$value .= '<span class="country-name">'.(isset($values[5])?$values[5]:'').'</span>';
                        
        			$value .= '</address>';
                    if($i % 2 == 0) { 
                        echo $this->echoAsTd1($field['Title'], $value, $extra_values);
                    } else {
                        echo $this->echoAsTd2($field['Title'], $value, $extra_values);
                    }
                } elseif($field['Typeof'] == 'text') {
                    $b=0;
                    $rightInput = isset($field['InputRightAnswer'])?$field['InputRightAnswer']:'';
                    $rightInput = $this->helpers->analizeInput($rightInput);
                    if(!empty($value)) {
                        foreach($rightInput as $ri)  {
                           if(trim($value) == $ri) {
                                $b++;
                                break;
                            } 
                        }
                    }
                    if($b!=0) {
                        $answer = isset($field['RightAnswerDescription'])?$field['RightAnswerDescription']:'';
                        if(empty($answer)) {
                            $answer = 'Wrong';
                        }
                        $atype = 'aright';
                    } else {
                        $answer = isset($field['WrongAnswerDescription'])?$field['WrongAnswerDescription']:'';
                        if(empty($answer)) {
                            $answer = 'Wrong';
                        }
                        $atype = 'awrong';
                    }
                    if($review) {
                        if($atype == 'awrong') {
                            $ex = '';$exk = 0;
                            $ex .= '<p style="font-size: 12px; background: #BBF5BB;">';
                            foreach($rightInput as $ri)  {
                                $p  = (strripos($ri,$value));
                                if($p !== false) {
                                    //$wr = preg_replace('#[^'.$value.']#','*',$ri);
                                    $wr = str_replace($value,'````',$ri);
                                    $wr = preg_replace('#[^`]#','*',$wr);
                                    $wr = str_replace('````',$value,$wr);
                                    $ex .= '+ '.$wr . '<br>';
                                    $exk++;
                                }
                            }
                            $ex .= '</p>';
                            if($exk>0) $value .= $ex;
                            else $value .= '<p style="font-size: 12px; background: #FFCDCD;"> <strong>Không có gợi ý nào vì làm sai hoặc sai kí tự đầu </strong> </p>';
                        }
                        if((!isset($_SESSION['admin_user']) && ( isset($_SESSION['user_normal']) && $_SESSION['user_normal'] != $user_id) ) && $atype == 'aright') {
                            $answer = $value = '****';
                        }
                    }
                    $title = $this->helpers->detectLink($title);
                    $value = $this->helpers->detectLink($value);
                    if($i % 2 == 0) { 
                        echo $this->echoAsTd1($title, $value, $extra_values, $answer, $atype);
                    } else {
                        echo $this->echoAsTd2($title, $value, $extra_values, $answer, $atype);
                    }
                } else {
                    $title = $this->helpers->detectLink($title);
                    $value = $this->helpers->detectLink($value);
                    if($i % 2 == 0) { 
                        echo $this->echoAsTd1($title, $value, $extra_values);
                    } else {
                        echo $this->echoAsTd2($title, $value, $extra_values);
                    }
                }
                $i++;
            }
            ?>
        </table>
        <?php
        $html2 .= ob_get_contents();
        ob_end_clean();
        return $html2;
    }
    function getAScontent() {
        $content = '';
        
        $content = ob_get_contents();
        ob_clean();
        return $content;
    }
    function getFormNBDisplayResult($form_nb, $entry_content, $user, $hideHeadInfo=false, $hideHeader = false) {
        $html2 = '';
        if (!ob_get_level()) ob_start();
        $entry_id = $entry_content['entry_id'];
        $entries = $this->helpers->json_decode_to_array_not_in_mysql($entry_content['entry_content']);
        $form_nb_content = $this->helpers->json_decode_to_array($form_nb['form_content']);
        $form_nb_fields = $form_nb_content['Fields'];
        if(!$hideHeader) {
        ?>
        <table cellspacing="0" style="width:100%;font-family:'Lucida Grande','Lucida Sans Unicode', Tahoma, sans-serif;">
            <?php
            if($entry_content['entry_type'] == 'NB' || $entry_content['entry_type'] == 'AS') :
                $user_content = $this->helpers->json_decode_to_array($user['user_content']);
                if(!$hideHeadInfo) :
                    ?>
                    <tr>
                        <td>Bài Nộp Của: <?php echo $user_content['Name']; ?><br />
                        <?php
                        if($this->options['show_nickh2d']) {
                        ?>
                        Nick H2d: <?php echo $user_content['NickH2d']; ?><br />
                        <?php
                        }
                        ?>
                        </td>
                    </tr>
                    <?php 
                else:
                    ?>
                    <tr>
                        <td><h3><strong>Form:</strong> <?php echo $form_nb_content['Name']; ?></h3><br /></td>
                    </tr>
                    <?php
                endif;
            endif; 
            ?>
        </table>
        <?php
        }
        ?>
        <table class="readonly" cellspacing="0" cellpadding="0" style="width:100%;border-bottom:1px solid #eee;font-size:12px;line-height:135%;font-family:'Lucida Grande','Lucida Sans Unicode', Tahoma, sans-serif">
        <?php
        $i = 0;
        foreach($form_nb_fields as $k => $field) {
            $values =  $titles = array();
            $value = $title = '';
            $validation = isset($field['Validation'])?$field['Validation']:'';
            if(isset($field['SubFields'])) {
                $j = 0;
                foreach($field['SubFields'] as $subF) {
                    if(isset($entries['Field'.$subF['ColumnId']])) {
                        if(!empty($entries['Field'.$subF['ColumnId']])) {
                            if($field['Typeof'] == 'likert') {
                                $titles[] = $subF['ChoicesText'];
                            } else {
                                $titles[] = $subF['Title'];
                            }
                            $values[] = $entries['Field'.$subF['ColumnId']];
                        }
                    }
                    if($field['Typeof'] == 'likert' && $field['Validation'] == 'dc') {
                        $titles[$j] = $subF['Title'];
                        foreach($field['Choices'] as $k => $choiC) {
                            
                            if(isset($entries['Field'.$subF['ColumnId'].'_'.($k+1)])) {
                                if(!empty($entries['Field'.$subF['ColumnId'].'_'.($k+1)])) {
                                    $values[$j][] = $entries['Field'.$subF['ColumnId'].'_'.($k+1)];
                                }
                            } else {
                                $values[$j][] = '';
                            }
                        }
                        $j++;
                    }
                }
            } else {
                if(isset($entries['Field'.$field['ColumnId']])) {
                    $title = $field['Title'];
                    $value = $entries['Field'.$field['ColumnId']];
                    if($field['Typeof'] == 'date' || $field['Typeof'] == 'eurodate') {
                        $value = $entries['Field'.$field['ColumnId'].(($field['Typeof'] == 'eurodate')?'-2':'-1')] .'/'.$entries['Field'.$field['ColumnId'].(($field['Typeof'] == 'eurodate')?'-1':'-2')].'/'.$entries['Field'.$field['ColumnId'].''] . ' (mm/dd/yyy)';
                    } elseif($field['Typeof'] == 'time') {
                        $value = $value . ':' . $entries['Field'.$field['ColumnId'].'-1'] . ':' . $entries['Field'.$field['ColumnId'].'-2'] . ' ' . $entries['Field'.$field['ColumnId'].'-3'];
                    } elseif($field['Typeof'] == 'shortname') {
                        $value = $value . ':' . $entries['Field'.$field['ColumnId'].'-1'] . ':' . $entries['Field'.$field['ColumnId'].'-2'] . ' ' . $entries['Field'.$field['ColumnId'].'-3'];
                    }
                }
            }
            if($field['Typeof'] == 'likert') {
                foreach($titles as $k => $title) {
                    if(is_array($values[$k])) {
                        $values[$k] = array_filter($values[$k]);
                    }
                    if(!empty($values[$k])) {
                        if($i % 2 == 0) { 
                            echo $this->echoTd1($title, $values[$k]);
                        } else {
                            echo $this->echoTd2($title, $values[$k]);
                        }
                    }
                    $i++;
                }
            } elseif($field['Typeof'] == 'radio') {
                foreach($field['Choices'] as $k => $choiC) {
			        if($choiC['Choice'] == 'Other') {
			            $post_value_other = isset($entries['Field'.$field['ColumnId'].'_other_Other'])?$entries['Field'.$field['ColumnId'].'_other_Other']:'';
                        if(!empty($post_value_other)) $value = $post_value_other;
                        break;
			        }
    			}
                $title = $this->helpers->detectLink($title);
                $value = $this->helpers->detectLink($value);
                if($i % 2 == 0) { 
                    echo $this->echoTd1($title, $value);
                } else {
                    echo $this->echoTd2($title, $value);
                }
                $i++;
            }  elseif($field['Typeof'] == 'checkbox') {
                if($i % 2 == 0) { 
                    echo $this->echoTd1($field['Title'], $values);
                } else {
                    echo $this->echoTd2($field['Title'], $values);
                }
                $i++;
            } elseif($field['Typeof'] == 'shortname') {
                $value = '';
                foreach($titles as $k => $title) {
                    $value .= ' '.$values[$k];
                }
                if($i % 2 == 0) { 
                    echo $this->echoTd1($title, $value);
                } else {
                    echo $this->echoTd2($title, $value);
                }
                $i++;
            } elseif($field['Typeof'] == 'address') {
                foreach($field['SubFields'] as $subF) {
                    $index = strtolower(str_replace(' ','-',$subF['Title']));
                    $values[] = isset($entries['Field'.$subF['ColumnId']])?$entries['Field'.$subF['ColumnId']]:'';
                }
                $values = array_filter($values);
                $value = '';
                $search = (isset($values[0])?$values[0]:'').'+'.(isset($values[1])?$values[1]:'').'+'.(isset($values[2])?$values[2]:'');
                $value .= '<a href="http://maps.google.com/?q='.$search.'" style="text-decoration:none;" title="Show a Map of this Location" target="_blank">';
    				$value .= '<img width="16" height="16" style="padding:2px 0 0 0;float:left;" alt="" src="../images/icons/map.png" class="mapicon">';
    			$value .= '</a>';
                $value .= '<address style="color:#333;font-style:normal;line-height:130%;padding:2px 0 2px 25px;" class="adr">';
    				
                    $value .= '<span class="street-address">'.(isset($values[0])?$values[0]:'').'</span>';
    				$value .= '<span class="extended-address"> '.(isset($values[1])?$values[1]:'').'</span><br>';			
                    $value .= '<span class="locality"> '.(isset($values[2])?$values[2]:'').'</span>'; 				
                    //$value .= '<span class="region">'.(isset($values[3])?$values[3]:'').'</span>';
    				//$value .= '<span class="postal-code">'.(isset($values[4])?$values[4]:'').'</span>';
    				//$value .= '<br>';				
                    //$value .= '<span class="country-name">'.(isset($values[5])?$values[5]:'').'</span>';
                    
    			$value .= '</address>';
                if($i % 2 == 0) { 
                    echo $this->echoTd1($field['Title'], $value);
                } else {
                    echo $this->echoTd2($field['Title'], $value);
                }
                $i++;
            }  else {
                $title = $this->helpers->detectLink($title);
                $value = $this->helpers->detectLink($value);
                if($i % 2 == 0) { 
                    echo $this->echoTd1($title, $value);
                } else {
                    echo $this->echoTd2($title, $value);
                }
                $i++;
            }
        }
        ?>
        </table>
        <?php
        $html2 .= ob_get_contents();
        ob_end_clean();
        return $html2;
    }
    function getFormDisplayResult() {
        
    }
    function postReq() {
        $req = $this->registry->req;
        $ajax = new Controllers_AjaxController($this->registry);
        $ajax->index($req);
        if(!$this->getAllowNext()) echo $ajax->getJson();
        return;
    }
}