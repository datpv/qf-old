<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_entriesController extends Applications_BaseController {
    public function adminOnly($redirect) {
        $this->helpers->adminNext($redirect);
    }
    function checkPermission() {
        $this->adminOnly($this->helpers->getUrl() . 'auth/login/&ref='.$this->helpers->curPageURL());
    }
    function index() {
        $routers = $this->registry->router->getRouters();
        if($routers['id'] == 'EntryManagerReadOnly') {
            $this->EntryManagerReadOnly();
            return;
        }
		$forms_c_obj = new Controllers_formsController($this->registry);
        $forms_obj = new Models_FormsModel($this->registry);
        $entries_obj = new Models_EntriesModel($this->registry);
        
        $formId = $this->helpers->escape_string($routers['id']);
        $result = $forms_obj->getForm($formId,'','form_uuid','');
        $_form = $this->helpers->json_decode_to_array($result['form_content']);
        $fields = $_form['Fields'];
        $form_id = $result['form_id'];
        $entty_count = $entries_obj->getEntriesNumber($form_id);
        $_entry = $entries_obj->fetchAll("SELECT * FROM {$this->db_prefix}entries_meta a, {$this->db_prefix}entries b WHERE a.`form_id` = '$form_id' AND a.`entry_id` = b.`entry_id` ORDER BY b.`entry_id` DESC LIMIT 0,6 ");
        $forms_obj->updateForm("`form_flag` = '$entty_count'","`form_id` = '$form_id'");
        $_form['EntryCount'] = $entty_count;
        $_form['FormId'] = $formId;
        $_form['Url'] = $formId;
        $catetory = $this->registry->category;
        if(isset($_form['Entries'])) {
            $i = 0;
            if($_entry) foreach($_entry as $entry) {
                $_form['Entries'][$i] = array(
                    'EntryId' => $entry['entry_id'],
                    'HitId' => '',
                    'AssignmentId' => '',
                    'Environment' => '',
                    'Status' => '',
                    'PurchaseTotal' => '',
                    'Currency' => '',
                    'TransactionId' => '',
                    'MerchantType' => '',
                    'DateCreated' => $entry['entry_create'],
                    'CreatedBy' => 'datphan',
                    'DateUpdated' => '',
                    'UpdatedBy' => '',
                    'IP' => $entry['entry_ip'],
                    'LastPage' => '1',
                    'CompleteSubmission' => $entry['entry_complete']
                );
                
                $posts = ($entry['entry_content']);
                // SIGN: $posts = json_decode_to_array($posts);  Old version - not working??????
                $posts = $this->helpers->json_decode_to_array_not_in_mysql($posts);
                foreach($posts as $k => $post) {
                    if(substr($k,0,5) == 'Field' && !strpos($k,'Other') && !empty($post)) {
                        $_form['Entries'][$i][$k] = $post;
                    }
                }
                $i++;
            }
            $_form['Entries'] = array_values($_form['Entries']);
            foreach($catetory as $cat) {
                array_push($_form['Fields'],$cat);
            }
            
        }
        $html = $forms_c_obj->getDisplayFields($fields,array());
        $this->view->title = $_form['Name'] .' - Entries';
        $this->view->html = $html;
        $this->view->formId = $formId;
        $this->view->_form = $_form;
        $this->view->render('entries');
    }
    function EntryManagerReadOnly() {
        
    }
    function postReq() {
        $req = $this->registry->req;
        $ajax = new Controllers_AjaxController($this->registry);
        $ajax->index($req);
        echo $ajax->getJson();
    }
}