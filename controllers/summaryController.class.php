<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_summaryController extends Applications_BaseController {
    function userOnly($redirect) {
        $this->helpers->userNext($redirect);
    }
    function checkPermission() {
        $this->userOnly($this->helpers->getUrl() . 'auth/login&ref='.$this->helpers->curPageURL());
    }
    function index() {
        $routers = $this->registry->router->getRouters();
		$entryId = $routers['id'];
        $entryId = $this->helpers->escape_string($entryId);
        $entry_type = '';
        $cached_obj = new Models_CachedModel($this->registry);
        $notfound_obj = new Controllers_notfoundController($this->registry);
        if(!$entry = $cached_obj->getCachedEntry('*','nb',"`cache_entry_uuid` = '$entryId'")) {
            $entry = $cached_obj->getCachedEntry('*','as',"`cache_entry_uuid` = '$entryId'");
            if($entry) {
                $entry_type = 'AS';
            }
        }
        if(!$entry) {
            $notfound_obj->index();
            exit();
        }
        if($entry_type== 'AS') {
            $user_viewing_id = isset($_SESSION['user_normal'])?$_SESSION['user_normal']:'';
            if($entry['cache_user_id'] != $user_viewing_id && !isset($_SESSION['admin_user'])) {
                $this->view->message = 'Bạn Không Có Quyền Truy Cập!';
                $this->view->render('message','only');
                exit();
            }
        }
        $this->view->scoring = $this->helpers->json_decode_to_array_not_in_mysql($entry['cache_content']);
        $this->view->entry_type = $entry_type;
        $this->view->title = 'Summary Of ' . $entry['cache_user_username'];
        $this->view->entry = $entry;
        $this->view->render('summary');
    }
}