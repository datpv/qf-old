<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_authController extends Applications_BaseController {
    function userOnce($redirect) {
        $this->helpers->userPrev($redirect);
    }
    function index() {
        $notfound = new Controllers_notfoundController($this->registry);
        $notfound->index();
    }
    function login() {
        $this->userOnce($this->helpers->getUrl() . 'dashboard/');
        $this->view->title = 'Login';
        $this->view->render('login');
    }
    function logout() {
        $siteUrl = $this->helpers->getUrl();
        if(isset($_SESSION['admin_user'])) {
            unset($_SESSION['admin_user']);
        }
        if(isset($_SESSION['user_normal'])) {
            unset($_SESSION['user_normal']);
        }
        header('location: '.$siteUrl.'auth/login/');
        exit();
    }
    function signup() {
        $this->userOnce($this->helpers->getUrl() . 'dashboard/');
        $errors = array();
        
        $this->view->title = 'Register Account';
        $this->view->render('signup');
    }
    function doLogin() {
        $siteUrl = $this->helpers->getUrl();
        $errors = array();
        $req = $this->registry->req;
        $user_acc = isset($req['username'])?$req['username']:'';
        $user_pass = isset($req['password'])?$req['password']:'';
        $user_acc = htmlspecialchars(stripslashes($user_acc));
        $user_pass = md5(htmlspecialchars(stripslashes($user_pass)));
        $ref = isset($req['ref'])?$req['ref']:'';
        $user_obj = new Models_UsersModel($this->registry);
        if( $user = $user_obj->userLogin($user_acc, $user_pass) ) {
            if($user['user_type'] == 'A') {
                $_SESSION['admin_user'] = $user['user_id'];
                if(!empty($ref)) {
                    header('location: '.$ref);
                    exit();
                }
                header('location: '.$siteUrl.'admin/');
                exit();
            } elseif($user['user_type'] == 'C') {
                $_SESSION['user_normal'] = $user['user_id']; 
                if(!empty($ref)) {
                    header('location: '.$ref); 
                    exit();
                }
                header('location: '.$siteUrl.'dashboard/');
                exit();
            }
        } else {
            $errors['fail'] = 'login fail, account and pass not match or your account have\'nt yet activated.';
        }
        $this->view->errors = $errors;
        $this->allowNext = true;
    }
    function doSignup() {
        $siteUrl = $this->helpers->getUrl();
        $errors = array();
        $req = $this->registry->req;
        $posts = $req;
        $this->view->posts = $posts;
        $posts = $this->helpers->unescape_string($posts);
        $next = true;
        $errors = $this->helpers->validateSignup($posts);
        require_once('lib/recaptchalib.php');
        $privatekey = "6LfNO9wSAAAAAC6Khm2xwF1iTKY2OzwWPpRj3yI8";
        $resp = recaptcha_check_answer ($privatekey,
                                    $_SERVER["REMOTE_ADDR"],
                                    $posts["recaptcha_challenge_field"],
                                    $posts["recaptcha_response_field"]);
        
        if (!$resp->is_valid) {
            // What happens when the CAPTCHA was entered incorrectly
            $errors['captcha']['message'] = ("$resp->error");
        }
        if(empty($errors)) {
            $username = isset($posts['username'])?$posts['username']:'';
            $email= isset($posts['email'])?$posts['email']:'';
            $password = isset($posts['password'])?$posts['password']:'';
            $nickh2d= isset($posts['nickh2d'])?$posts['nickh2d']:'';
            $nickskype = isset($posts['nickskype'])?$posts['nickskype']:'';
            $fullname = isset($posts['fullname'])?$posts['fullname']:'';
            
            $email = htmlspecialchars($this->helpers->escape_string($email));
            $username = ($username);
            $nickskype = htmlspecialchars($this->helpers->escape_string($nickskype));
            $password = md5(htmlspecialchars($this->helpers->escape_string($password)));
            
            $user_content = array(
                'Name' => $fullname,
                'NickH2d' => $nickh2d,
                'NickSkype' => $nickskype
            );
            $user_content = $this->helpers->json_encode_then_escape($user_content);
            $users_obj = new Models_UsersModel($this->registry);
            if($usered = $users_obj->getUsers('*',"`username` = '$username' OR `user_email` = '$email'")) {
                $errors['message'] = 'Username or Email Existed';
                $errors['username']['message'] = '';
                $errors['email']['message'] = '';
            } elseif($user_insert = $users_obj->insertUser("`username`,`password`,`user_email`,`user_content`", "'$username','$password','$email','$user_content'")) {
                $this->view->message = 'Please Wait For Admin Complete Your Registration.';
                $this->view->redirect = true;
                $this->view->render('message','only');
                exit();
            }
        }
        $this->view->errors = $errors;
        $this->setAllowNext(true);
    }
    function postReq() {
        $siteUrl = $this->helpers->getUrl();
        $this->userOnce($siteUrl . 'dashboard/');
        $routers = $this->registry->router->getRouters();
        if($routers['action'] == 'login') {
            $this->doLogin();
        } elseif($routers['action'] == 'signup') {
            $this->doSignup();
        }
        
    }
}