<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_formsController extends Applications_BaseController {
    private $errorFields = array();
    private $json = array(
        'sl_translate' => 'markup,lbmarkup,html,message',
        'success' => false,
        'response' => array()
    );
    function userOnly() {
        
    }
    function checkSubject($args) {
        extract($args);
        $form_id = $result['form_id'];
        $sf_results = $subjects_obj->getSFs('*',"`form_id` = '$form_id'");
        if($sf_results) {
            $user_id = $this->helpers->getCurrentUser();
            if($user_id != 1) {
                $subject_ids = array();
                foreach($sf_results as $sf_result) {
                    $subject_ids[] = $sf_result['subject_id'];
                }
                $subject_ids = implode(',',$subject_ids);
                $su_results = $subjects_obj->getSUs('*',"`user_id` = '$user_id' AND `subject_id` IN ($subject_ids)");
                $unlocked = $uncompleted = false;
                if($su_results) {
                    foreach($su_results as $su_result) {
                        if($su_result['subject_unlocked'] == 'N') {
                            $unlocked = false;
                        } else {
                            $subject_id = $su_result['subject_id'];
                            $subject_result = $subjects_obj->getSubject("`subject_id` = '$subject_id'");
                            if($subject_result) {
                                $unlocked = true;
                                
                                $subject_settings = $this->helpers->json_decode_to_array($subject_result['subject_settings']);
                                $form_order = $subject_settings['FormOrder'];
                                foreach($form_order as $kfo => $fo) {
                                    if($fo == $form_id) {
                                        if($kfo > 0) {
                                            $form_id_check = $form_order[$kfo-1];
                                            $entry = $subjects_obj->fetch("SELECT a.`entry_id` FROM {$this->db_prefix}.entries_meta a, {$this->db_prefix}.entries b WHERE a.`entry_id` = b.`entry_id` AND b.`entry_user` = '$user_id' AND a.`form_id` = '$form_id_check' LIMIT 1");
                                            if(!$entry) {
                                                $uncompleted = true;
                                            }
                                        }
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
                if(!$unlocked) {
                    $this->view->message = 'You Need To Unlock This Form First';
                    $this->view->render('message','only');
                    exit();
                }
                if($uncompleted) {
                    $this->view->message = 'You Must Completed Previous Form';
                    $this->view->render('message','only');
                    exit();
                }
            }
        }
    }
    function index() {
		$routers = $this->registry->router->getRouters();
        $formId = $routers['id'];
        $entryId = $routers['id2'];
        $f = $this->registry->router->f;
        $formId = $this->helpers->escape_string($formId);
        $entryId = $this->helpers->escape_string($entryId);
        $forms_obj = new Models_FormsModel($this->registry);
        $subjects_obj = new Models_SubjectsModel($this->registry);
        $themes_obj = new Models_ThemesModel($this->registry);
        $entries_obj = new Models_EntriesModel($this->registry);
        $notfound_obj = new Controllers_notfoundController($this->registry);
        /*
        *
        */
        $field_id1 = false;
        /*
        *
        */
        $errorFields = (array) $this->view->errorFields;
        $entry = '[]';
        $html = '';
        if(empty($formId)) {
            $notfound_obj->index();
            exit();
        } if(!empty($formId)) {
            if(!$result = $forms_obj->getForm($formId,'','form_uuid','',"AND `form_status` = 'A' AND `form_approved` = 'Y'")) {
                $notfound_obj->index();
                exit();
            }
            $args = array(
                'result' => $result,
                'forms_obj' => $forms_obj,
                'subjects_obj' => $subjects_obj,
                'notfound_obj' => $notfound_obj
            );
            $this->checkSubject($args);
            /*
            * Theme
            */
            $theme_id = $result['theme_id'];
            if($theme = $themes_obj->getTheme('',$theme_id)) {
                $_theme = $this->helpers->json_decode_to_array($theme['theme_content']);
            }
            /*
            * Form
            */
            // check form password
            $form_pass = $result['form_password'];
            if(!empty($form_pass)) {
                if(!isset($_SESSION['form_'.$formId])) {
                    $this->view->pass_require = ' Unauthorized access.';
                    $this->view->render('password_protect','only');
                    exit();
                }
            }
            
            $form = ($result['form_content']);
            $form_id = $result['form_id'];
            $_form = $this->helpers->json_decode_to_array($form);
            // check form datetime
            $start_date = $_form['StartDate'];
            $end_date = $_form['EndDate'];
            $current_date = $this->helpers->get_datetime();
            //print_r(($start_date)); echo ' ';print_r(($end_date)); echo ' ';print_r(($current_date));
            if((strtotime($end_date) < strtotime($current_date)) || (strtotime($start_date) > strtotime($current_date))) {
                $notfound_obj->index();
                exit();
            }
            $_form['FormId'] = $formId;
            $_form['Url'] = $formId;
            $formType = $_form['Formtype'];
            $ajaxEnable = isset($_form['AjaxEnable'])?$_form['AjaxEnable']:false;
            $ajaxLiveResponse = isset($_form['AjaxLiveResponse'])?$_form['AjaxLiveResponse']:false;
            $normal_login = false;
            if($formType == 'normal') {
                if(isset($_form['UserLogin']) && $_form['UserLogin']) {
                    $normal_login = true;
                }
            }
            
            if($formType == 'autoscore' || $normal_login) {
                if(!empty($f)) {
                    $part = explode('_',$this->helpers->decrypt($f,'dpkey'));
                    if(!isset($part[1])) {
                        $notfound_obj->index();
                        exit();
                    }
                    $part2 = explode('-',$part[1]);
                    if(!isset($part2[1]) || !is_numeric($part2[1])) {
                        $notfound_obj->index();
                        exit();
                    }
                    $field_id1 = $part2[1];
                }
            }
            $fields = $_form['Fields'];
            if($field_id1) {
                $fields = $this->helpers->getFieldById($_form['Fields'], $field_id1);
                if(!$fields) {
                    $notfound_obj->index();
                    exit();
                }
            }
            $title = $_form['Name'];
            if($field_id1) {
               $title = $fields[0]['Title'];
            }
            // check login
            if($formType == 'nopbai' || $formType == 'score' || $formType == 'autoscore' || $normal_login) {
                if(!isset($_SESSION['user_normal']) && !isset($_SESSION['admin_user'])) {
                    $this->view->normal_login = $normal_login;
                    $this->view->pass_require = ' Unauthorized access.';
                    $this->view->title = $title;
                    $this->view->render('login');
                    return;
                }
            }
            if($formType == 'score') {
                if(empty($entryId)) {
                    $notfound_obj->index();
                    exit();
                }
                if(!$entry_content = $entries_obj->getEntry($entryId,'')) {
                    $notfound_obj->index();
                    exit();
                }
                $user_scoring_id = isset($_SESSION['user_normal'])?$_SESSION['user_normal']:'';
                // check scoring 1
                if($entry_content['entry_user'] == $user_scoring_id) {
                    $this->view->message = 'Bạn Không Được Phép Chấm Bài Của Mình.';
                    $this->view->render('message','only');
                    return;
                }
                if(isset($_SESSION['admin_user'])) {
                    $user_scoring_id = $_SESSION['admin_user'];
                }
                // check scoring 2
                if($entries_obj->getEntries("`entry_of` = '$entryId' AND `entry_user` = '$user_scoring_id'")) {
                    $this->view->message = 'Bạn Không Được Phép Chấm Bài Cho Bạn Mình 2 Lần.';
                    $this->view->render('message','only');
                    return;
                }
                $entry_id = $entry_content['entry_id'];
                $user_id = $entry_content['entry_user'];
                if($entries_meta_content = $entries_obj->getEntryMeta("`entry_id` = '$entry_id'")) {
                    $form_nb_id = $entries_meta_content['form_id'];
                    if($form_nb = $forms_obj->getForm('',$form_nb_id)) {
                        $caches_obj = new Controllers_cachesController($this->registry);
                        $users_obj = new Models_UsersModel($this->registry);
                        $user_nb = $users_obj->getUser("`user_id` = '$user_id'");
                        $this->view->formNBDisplayResult = $caches_obj->getFormNBDisplayResult($form_nb,$entry_content,$user_nb);
                    }
                }
            }
            $formUserSubmitLimit = isset($_form['UserSubmitLimit'])?$_form['UserSubmitLimit']:1;
            if(!is_numeric($formUserSubmitLimit) || $formUserSubmitLimit < 0) $formUserSubmitLimit = 1;
            $user_scoring_id = $this->helpers->getCurrentUser();
            // check submit limit
            if($formType == 'autoscore') {
                if($userScored = $entries_obj->scoringOnce($user_scoring_id, $form_id)) {
                    if($formUserSubmitLimit != 0 && $userScored['entry_flag'] >= $formUserSubmitLimit) {
                        $this->view->message = 'Bạn Không Được Phép Gửi Bài Này Quá Nhiều Lần. (' . $formUserSubmitLimit . ' Tiny Times)';
                        $this->view->render('message','only');
                        exit();
                    }
                }
            } 
            if($normal_login) {
                if($userScored = $entries_obj->submitingOnce($user_scoring_id, $form_id)) {
                    if($formUserSubmitLimit != 0 && $userScored['entry_flag'] >= $formUserSubmitLimit) {
                        $this->view->message = 'Bạn Không Được Phép Gửi Bài Này Quá Nhiều Lần. (' . $formUserSubmitLimit . ' Tiny Times)';
                        $this->view->render('message','only');
                        exit();
                    }
                }
            }
            $extra_options = array();
            if($formType == 'autoscore' || $normal_login) {
                if($ajaxEnable) {
                    $extra_options['ajaxEnable'] = true;
                    $posted_contents = $this->helpers->json_decode_to_array($userScored['entry_content']);
                    $posted_ids = array();
                    if($posted_contents) foreach($posted_contents as $pk => $posted_content) {
                        if( strpos($pk, 'Field') === 0 ) {
                            $posted_ids[$pk] = $posted_content;
                        }
                    }
                    $extra_options['posted_ids'] = $posted_ids;
                }
            }
            /*
            * Fields
            */
            if(isset($_form['Fields']['HasntBeenSaved'])) unset($_form['Fields']['HasntBeenSaved']);
            
            
            $hasFieldClass = '';
            $buttonClass = 'hide';
            $html = $this->getDisplayFields($fields,$errorFields,'',$extra_options);
        }
        /*
        *
        */
        $this->view->formId = $formId;
        $this->view->entryId = $entryId;
        $this->view->formType = $formType;
        $this->view->_form = $_form;
        $this->view->title = $title;
        $this->view->html = $html;
        $this->view->ajaxEnable = $ajaxEnable;
        $this->view->f = $f;
        $this->view->render('form');
    }
    function postReq() {
        $req = $this->registry->req;
        $action = isset($req['action'])?$req['action']:'';
        if($action == 'save') {
            $this->submitField();
        } else {
            $this->submitForm();
        }
    }
    function getDisplayFields($fields,$errorFields, $extra = '', $extra_options = array()) {
        $html = '';
        if(!empty($fields)) {
            if (!ob_get_level()) ob_start();
            $countries = $this->getCountries();
            $ajaxEnable = false;
            $posted_ids = array();
            if(isset($extra_options['ajaxEnable'])) {
                $ajaxEnable = $extra_options['ajaxEnable'];
                $posted_ids = $extra_options['posted_ids'];
            }
            $hasFieldClass = 'hasFields';
            $buttonClass = '';
            $this->helpers->aasort($fields,"Pos");
            foreach($fields as $field) {
                $disabled_value = false;
                $classes = 'notranslate ';
                $type = $field['Typeof'];
                $fieldId = $field['ColumnId'];
                $showAjaxButton = true;
                $postedValue = '';
                if($ajaxEnable) {
                    if(isset($posted_ids['Field'.$fieldId])) {
                        $classes .= ' saved';
                        $postedValue = $posted_ids['Field'.$fieldId];
                        $showAjaxButton = false;
                        $disabled_value = true;
                    }
                    if($type == 'checkbox') {
                        $postedValue = array();
                        foreach($field['SubFields'] as $v) {
                            if(isset($posted_ids['Field'.($v['ColumnId'])]) && !empty($posted_ids['Field'.($v['ColumnId'])])) {
                                $postedValue[] = $posted_ids['Field'.$v['ColumnId']];
                            }
                        }
                        $postedValue = implode(' | ', $postedValue);
                    }
                }
                $fieldTitle = $this->helpers->detectLink($field['Title']);
                $settings = $field['Settings'];
                $fieldInstruction = $this->helpers->detectLink($field['Instructions']);
                $subFields = isset($field['SubFields'])?$field['SubFields']:array();
                $choices = isset($field['Choices'])?$field['Choices']:array();
                if($type == 'section') {
                    $classes .= ' section';
                }
                if($type == 'likert') {
                    $classes .= ' col4 likert';
                }
                if(isset($settings['hideNumbers']) || in_array('hideNumbers',$settings)) {
                    $classes .= ' hideNumbers';
                }
                if(isset($field['ClassNames'])) {
                    $classes .= ' ' . $field['ClassNames'];
                }
                if($type == 'date' || $type == 'eurodate') {
                    $classes .= ' '.$type;
                } elseif($type == 'time') {
                    $classes .= ' time';
                } elseif($type == 'phone') {
                    $classes .= ' phone';
                } elseif($type == 'address') {
                    $classes .= ' complex';
                }
                /*
                * FORM BUILDER
                */
                
                ?>
                <li id="<?php echo $extra; ?><?php echo $extra; ?>foli<?php echo $fieldId; ?>" class="<?php echo $classes; if(isset($errorFields[$fieldId])): echo ' error'; endif; ?>">
                    <?php
                    if($type == 'checkbox' || $type == 'radio') {
                    ?>
                        <fieldset>
                    	<![if !IE | (gte IE 8)]>
                    	<legend id="<?php echo $extra; ?>title<?php echo $fieldId; ?>" class="desc">
                    	<?php echo $fieldTitle; ?>
                    	<?php if($field['IsRequired']): ?><span id="<?php echo $extra; ?>req_<?php echo $fieldId; ?>" class="req"> * </span><?php endif; ?>
                    	</legend>
                    	<![endif]>
                    	<!--[if lt IE 8]>
                    	<label id="<?php echo $extra; ?>title<?php echo $fieldId; ?>" class="desc">
                    	<?php echo $fieldTitle; ?>
                    	<?php if($field['IsRequired']): ?><span id="<?php echo $extra; ?>req_<?php echo $fieldId; ?>" class="req"> * </span><?php endif; ?>
                    	</label>
                    	<![endif]-->
                    <?php
                    } elseif($type == 'section' || $type == 'likert') {
                    } else {
                    ?>
                        <label class="desc" id="<?php echo $extra; ?>title<?php echo $fieldId; ?>" for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>">
                        <?php echo $fieldTitle; ?>
                        <?php if($field['IsRequired']): ?><span id="<?php echo $extra; ?>req_<?php echo $fieldId; ?>" class="req"> * </span><?php endif; ?>
                        </label>
                    <?php
                    }
                    /*
                    * Field Content
                    */
                    if($showAjaxButton) {
                    if($type == 'text') {
                    ?>
                	<div>
                		<input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" type="text" class="field text <?php echo $field['Size']; ?>" value="<?php echo $field['DefaultVal']; ?>" maxlength="<?php
                        if($field['RangeType'] == 'character') {
                            echo empty($field['RangeMax'])?'255':$field['RangeMax']; 
                        }
                        ?>" tabindex="<?php echo $field['DisplayPos']; ?>" onkeyup="handleInput(this); " onchange="handleInput(this);" <?php if($field['IsRequired'] == 1): echo 'required'; endif; ?>  />
                        <?php
                        if(!empty($field['RangeMax'])) :
                        ?>
                        <label for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>">Maximum Allowed: <var id="<?php echo $extra; ?>rangeMaxMsg<?php echo $fieldId; ?>"><?php echo $field['RangeMax']; ?></var> <?php echo $field['RangeType']; ?>.&nbsp;&nbsp;&nbsp; <em class="currently" style="display: inline;"><!--Currently Used: <var id="<?php echo $extra; ?>rangeUsedMsg<?php echo $fieldId; ?>">0</var> characters.--></em></label>
                        <?php
                        endif;
                        ?>
                	</div>
                    <?php
                    } elseif($type == 'shortname') {
                        if(!empty($subFields)) {
                            $i=0;
                            foreach($subFields as $v) {
                                $subFieldId = $v['ColumnId'];
                    ?>
                    <span>
                		<input id="<?php echo $extra; ?>Field<?php echo $subFieldId; ?>" name="<?php echo $extra; ?>Field<?php echo $subFieldId; ?>" type="text" class="field text <?php echo ($i==0)?'fn':'ln'; ?>" value="" size="8" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" <?php if($field['IsRequired']): echo 'required'; endif; ?> />
                		<label for="<?php echo $extra; ?>Field<?php echo $subFieldId; ?>">
                			<?php echo $v['ChoicesText']; ?>
                		</label>
                	</span>
                    <?php
                                $i++;
                            }
                        }
                    } elseif($type == 'address') {
                    ?>
                    <div>
                    <?php
                        if(!empty($subFields)) {
                            $i=0;
                            foreach($subFields as $v) {
                                $subFieldId = $v['ColumnId'];
                                $eclass = '';
                                if($i==0) $eclass = 'full addr1';
                                elseif($i==1) $eclass = 'full addr2';
                                elseif($i==2) $eclass = 'left city';
                                elseif($i==3) $eclass = 'right state';
                                elseif($i==4) $eclass = 'left zip';
                                elseif($i==5) $eclass = 'right country';
                                
                    ?>
                        <span class="<?php echo $eclass; ?>">
                            <?php if($i!=5): ?>
                        	<input id="<?php echo $extra; ?>Field<?php echo $subFieldId; ?>" name="<?php echo $extra; ?>Field<?php echo $subFieldId; ?>" type="text" class="field text addr" value="" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);"/>
                            <?php else: ?>
                            <select id="<?php echo $extra; ?>Field<?php echo $subFieldId; ?>" name="<?php echo $extra; ?>Field<?php echo $subFieldId; ?>" class="field select addr" tabindex="6" onclick="handleInput(this);" onkeyup="handleInput(this);">
                            <?php echo $countries; ?>
                            </select>
                            <?php endif; ?>
                        	<label for="<?php echo $extra; ?>Field<?php echo $subFieldId; ?>">
                        		<?php echo $v['ChoicesText']; ?>
                        	</label>
                        </span>
                    <?php
                                $i++;
                            }
                        }
                    ?>
                    </div>
                    <?php
                    } elseif($type == 'eurodate') {
                    ?>
                    <span>
                    	<input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-2" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-2" type="text" class="field text" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" />
                    	<label for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-2">
                    		DD
                    	</label>
                    </span>
                    <span class="symbol">
                    	/
                    </span>
                    <span>
                    	<input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-1" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-1" type="text" class="field text" value="" size="2" maxlength="2" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" />
                    	<label for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-1">
                    		MM
                    	</label>
                    </span>
                    <span class="symbol">
                    	/
                    </span>
                    <span>
                    	<input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" type="text" class="field text" value="" size="4" maxlength="4" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" />
                    	<label for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>">
                    		YYYY
                    	</label>
                    </span>
                    <span id="<?php echo $extra; ?>cal<?php echo $fieldId; ?>">
                    	<img id="<?php echo $extra; ?>pick<?php echo $fieldId; ?>" class="datepicker" src="../images/icons/calendar.png" alt="Pick a date." />
                    </span>
                    <?php
                    } elseif($type == 'date') {
                    ?>
                    <span>
                    	<input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-1" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-1" type="text" class="field text" value="" size="2" maxlength="2" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" />
                    	<label for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-1">
                    		MM
                    	</label>
                    </span>
                    <span class="symbol">
                    	/
                    </span>
                    <span>
                    	<input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-2" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-2" type="text" class="field text" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" />
                    	<label for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-2">
                    		DD
                    	</label>
                    </span>
                    <span class="symbol">
                    	/
                    </span>
                    <span>
                    	<input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" type="text" class="field text" value="" size="4" maxlength="4" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" />
                    	<label for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>">
                    		YYYY
                    	</label>
                    </span>
                    <span id="<?php echo $extra; ?>cal<?php echo $fieldId; ?>">
                    	<img id="<?php echo $extra; ?>pick<?php echo $fieldId; ?>" class="datepicker" src="../images/icons/calendar.png" alt="Pick a date." />
                    </span>
                    <?php
                    } elseif($type == 'email') {
                    ?>
                    <div>
                		<input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" type="email" spellcheck="false" class="field text medium" value="" maxlength="255" tabindex="<?php echo $field['DisplayPos']; ?>" onkeyup="handleInput(this);" onchange="handleInput(this);"/>
                	</div>
                    <?php
                    } elseif($type == 'time') {
                    ?>
                    <span class="hours">
                    	<input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" type="text" class="field text" value="<?php echo $field['DefaultVal']; ?>" size="2" maxlength="2" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" />
                    	<label for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>">
                    		HH
                    	</label>
                    </span>
                    <span class="symbol minutes">
                    	:
                    </span>
                    <span class="minutes">
                    	<input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-1" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-1" type="text" class="field text" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" />
                    	<label for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-1">
                    		MM
                    	</label>
                    </span>
                    <span class="symbol seconds">
                    	:
                    </span>
                    <span class="seconds">
                    	<input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-2" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-2" type="text" class="field text" value="" size="2" maxlength="2" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" />
                    	<label for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-2">
                    		SS
                    	</label>
                    </span>
                    <span class="ampm">
                    	<select id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-3" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-3" class="field select" style="width:4em" tabindex="4" onclick="handleInput(this);" onkeyup="handleInput(this);">
                    		<option value="AM" selected="selected">
                    			AM
                    		</option>
                    		<option value="PM">
                    			PM
                    		</option>
                    	</select>
                    	<label for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-3">
                    		AM/PM
                    	</label>
                    </span>
                    <?php
                    } elseif($type == 'phone') {
                        $p1 = $p2 = $p3 = '';
                        if(!empty($field['DefaultVal'])) {
                            $p = $field['DefaultVal'];
                            $p1 = substr($p,0,3);
                            $p2 = substr($p,3,6);
                            $p3 = substr($p,6,9);
                        }
                    ?>
                    <span>
                    	<input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" type="tel" class="field text" value="<?php echo $p1; ?>" size="3" maxlength="3" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" />
                    	<label for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>">
                    		###
                    	</label>
                    </span>
                    <span class="symbol">
                    	-
                    </span>
                    <span>
                    	<input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-1" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-1" type="tel" class="field text" value="<?php echo $p2; ?>" size="3" maxlength="3" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" />
                    	<label for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-1">
                    		###
                    	</label>
                    </span>
                    <span class="symbol">
                    	-
                    </span>
                    <span>
                    	<input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-2" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-2" type="tel" class="field text" value="<?php echo $p3; ?>" size="4" maxlength="4" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" />
                    	<label for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-2">
                    		####
                    	</label>
                    </span>
                    <?php
                    } elseif($type == 'url') {
                    ?>
                    <div>
                		<input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" type="url" class="field text medium" value="<?php echo $field['DefaultVal']; ?>" maxlength="255" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" />
                	</div>
                    <?php
                    } elseif($type == 'money') {
                    ?>
                    <span class="symbol">
                    	$
                    </span>
                    <span>
                    	<input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" type="text" class="field text currency nospin" value="" size="10" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" />
                    	<label for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>">
                    		Dollars
                    	</label>
                    </span>
                    <span class="symbol radix">
                    	.
                    </span>
                    <span class="cents">
                    	<input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-1" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-1" type="text" class="field text nospin" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" />
                    	<label for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>-1">
                    		Cents
                    	</label>
                    </span>
                    <?php
                    } elseif($type == 'number') {
                    ?>
                    <div>
                		<input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" type="text" class="field text nospin <?php echo $field['Size']; ?>" value="<?php echo $field['DefaultVal']; ?>" tabindex="<?php echo $field['DisplayPos']; ?>" onkeyup="handleInput(this); " onchange="handleInput(this);" <?php if($field['IsRequired']): echo 'required'; endif; ?> />
                        <?php
                        if(!empty($field['RangeMax'])) :
                        ?>
                        <label for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>">Enter a number between <var id="<?php echo $extra; ?>rangeMinMsg<?php echo $fieldId; ?>"><?php echo $field['RangeMin']; ?></var> and <var id="<?php echo $extra; ?>rangeMaxMsg<?php echo $fieldId; ?>"><?php echo $field['RangeMax']; ?></var>.</label>
                        <?php
                        endif;
                        ?>
                	</div>
                    
                    <?php
                    } elseif($type == 'textarea') {
                    ?>
                    <div>
                		<div class="handle"></div><textarea id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" class="field textarea <?php echo $field['Size']; ?>" spellcheck="true" rows="10" cols="50" tabindex="<?php echo $field['DisplayPos']; ?>" onkeyup="handleInput(this); " onchange="handleInput(this);" <?php if($field['IsRequired']): echo 'required'; endif; ?> ><?php echo $field['DefaultVal']; ?></textarea>
                        <?php
                        if(!empty($field['RangeMax'])) :
                        ?>
                        <label for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>">Maximum Allowed: <var id="<?php echo $extra; ?>rangeMaxMsg<?php echo $fieldId; ?>"><?php echo $field['RangeMax']; ?></var> <?php echo $field['RangeType']; ?>.&nbsp;&nbsp;&nbsp; <em class="currently" style="display: inline;">Currently Used: <var id="<?php echo $extra; ?>rangeUsedMsg<?php echo $fieldId; ?>">0</var> characters.</em></label>
                        <?php
                        endif;
                        ?>
                	</div>
                    <?php
                    } elseif($type == 'checkbox') {
                    ?>
                    <div>
                    <?php
                        
                        if(!empty($subFields)) {
                            foreach($subFields as $v) {
                                $subFieldId = $v['ColumnId'];
                                
                    ?>
                		<span>
                			<input id="<?php echo $extra; ?>Field<?php echo $subFieldId; ?>" name="<?php echo $extra; ?>Field<?php echo $subFieldId; ?>" type="checkbox" class="field checkbox" value="<?php echo $v['ChoicesText']; ?>" tabindex="1" onchange="handleInput(this);"  <?php echo $v['DefaultVal']?'checked="checked"':''; ?> />
                			<label class="choice" for="<?php echo $extra; ?>Field<?php echo $subFieldId; ?>">
                				<?php echo $this->helpers->detectLink($v['ChoicesText']); ?>
                			</label>
                		</span>
                    <?php
                            }
                        }
                    ?>
                    </div>
                    <?php
                    } elseif($type == 'radio') {
                    ?>
                    <div>
                    <input id="<?php echo $extra; ?>radioDefault_<?php echo $fieldId; ?>" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" type="hidden" value="" />
                    <?php
                        
                        if(!empty($choices)) {
                            $i = 0;
                            foreach($choices as $v) {
                    ?>
                        
                		<span>
                			<input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>_<?php echo $i; ?>" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" type="radio" class="field radio" value="<?php echo $v['Choice']; ?>" tabindex="<?php echo $i; ?>" onchange="handleInput(this);" onmouseup="handleInput(this);" <?php echo $v['IsDefault']?'checked="checked"':''; ?> />
                            <label class="choice" for="<?php echo $extra; ?>Field<?php echo $fieldId; ?>_<?php echo $i; ?>">
                            <?php echo $this->helpers->detectLink($v['Choice']); ?>
                            </label>
                		</span>
                        <?php if($v['Choice'] == 'Other') { ?>
                            <input id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>_other" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>_other_Other" type="text" class="field text other" value="" onclick="document.getElementById('Field<?php echo $fieldId; ?>_<?php echo $i; ?>').checked = 'checked';" onkeyup="handleInput(this);" onchange="handleInput(this);"
                			tabindex="<?php echo $i; ?>" />
                        <?php }
                        $i++;
                            }
                        }
                    ?>
                    </div>
                    <?php
                    } elseif($type == 'select') {
                    ?>
                    <div>
                    <select id="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" name="<?php echo $extra; ?>Field<?php echo $fieldId; ?>" class="field select <?php echo $field['Size']; ?>" onclick="handleInput(this);" onkeyup="handleInput(this);" tabindex="<?php echo $field['DisplayPos']; ?>" >
                    <?php
                        if(!empty($choices)) {
                            foreach($choices as $v) {
                    ?>
                			<option <?php echo ($v['IsDefault'])?'selected="selected"':''; ?> value="<?php echo $v['Choice']; ?>">
                				<?php echo $v['Choice']; ?>
                			</option>
                    <?php
                            }
                        }
                    ?>
                    </select>
                    </div>
                    <?php
                    } elseif($type == 'section') {
                    ?>
                    <div>
                		<section>
                    	<h3 id="<?php echo $extra; ?>title<?php echo $fieldId; ?>">
                    	<?php echo $fieldTitle; ?>
                    	</h3>
                    	<div id="<?php echo $extra; ?>instruct<?php echo $fieldId; ?>">
                    	<?php echo $this->helpers->detectLink($fieldInstruction); ?>
                    	</div>
                        </section>
                	</div>
                    <?php
                    } elseif($type == 'likert') {
                        if(!empty($subFields) && !empty($choices)) {
                            $validation = $field['Validation'];
                    ?>
                    <table cellspacing="0">
                	<caption id="<?php echo $extra; ?>title<?php echo $fieldId; ?>">
                		<?php echo $fieldTitle; ?>
                		<span id="<?php echo $extra; ?>req_<?php echo $fieldId; ?>" class="req"><?php echo ($field['IsRequired'])?'*':''; ?>
                		</span>
                	</caption>
                    <thead>
                		<tr>
                            <th>
                				&nbsp;
                			</th>
                    <?php
                            foreach($choices as $choice) {
                    ?>
                			<td>
                				<?php echo $choice['Choice']; ?>
                			</td>
                		
                    <?php
                            }
                    ?>
                        </tr>
                	</thead>
                    <tbody>
                    <?php
                        $i = 0;
                        foreach($subFields as $subField) {
                            $subFieldid = $subField['ColumnId'];
                    ?>
                    <tr class="statement<?php echo $subFieldid; if($i%2!=0) echo ' alt'; ?>">
                    <?php
                            $j=0;
                            foreach($choices as $choice) {
                            if($j==0) {
                    ?>
                            <?php if($validation == 'na' || empty($validation)) : ?>
                            <th>
                				<label for="<?php echo $extra; ?>Field<?php echo $subFieldid; ?>">
                					<?php echo $subField['ChoicesText']; ?>
                				</label>
                			</th>
                            <?php elseif($validation == 'dc'): ?>
                            <th>
                				<label>
                					<?php echo $subField['ChoicesText']; ?>
                				</label>
                			</th>
                            <?php endif; ?>
                    <?php
                            }
                    ?>
                            <td title="<?php echo $choice['Choice']; ?>">
                                <?php if($validation == 'na' || empty($validation)) : ?>
                				<input <?php echo ($choice['IsDefault'])?'checked="checked"':''; ?> class="field" id="<?php echo $extra; ?>Field<?php echo $subFieldid; ?>_<?php echo $j + 1; ?>" name="<?php echo $extra; ?>Field<?php echo $subFieldid; ?>" type="radio" tabindex="<?php echo $j + 1; ?>" value="<?php echo $choice['Choice']; ?>" onchange="handleInput(this);" />
                				<label for="<?php echo $extra; ?>Field<?php echo $subFieldid; ?>_<?php echo $j + 1; ?>">
                					<?php echo isset($choice['Score'])?$choice['Score']:''; ?>
                				</label>
                                <?php elseif($validation == 'dc'): ?>
                                <input <?php echo ($choice['IsDefault'])?'checked="checked"':''; ?> class="field" id="<?php echo $extra; ?>Field<?php echo $subFieldid; ?>_<?php echo $j + 1; ?>" name="<?php echo $extra; ?>Field<?php echo $subFieldid; ?>_<?php echo $j + 1; ?>" type="checkbox" tabindex="<?php echo $j + 1; ?>" value="<?php echo $choice['Choice']; ?>" onchange="handleInput(this);" />
                                <?php endif; ?>
                			</td>
                    <?php
                            
                            $j++;
                            }
                    ?>
                    </tr>
                    <?php
                        $i++;
                        }
                    ?>
                    </tbody>
                    </table>
                    <?php
                        }
                    }
                    ?>
                    
                    <?php
                    if(isset($errorFields[$fieldId])) echo '<p class="error">'.$errorFields[$fieldId]['message'].'</p>';
                    if(!empty($field['Instructions'])): 
                    ?>
                	<p class="instruct" id="<?php echo $extra; ?>instruct<?php echo $fieldId; ?>">
                		<small>
                        <?php echo $this->helpers->detectLink($field['Instructions']); ?>
                		</small>
                	</p>
                    
                	<?php 
                    endif;
                    }
                    if($type == 'checkbox' || $type == 'radio') {
                    ?>
                    </fieldset>
                    <?php
                    }
                    
                    if($showAjaxButton) {
                    if(isset($field['ExtraFields']) && !empty($field['ExtraFields'])) {
                    ?>
                    <div class="extra_fields">
                    <ul>
                    <?php
                        echo $this->getDisplayFields($field['ExtraFields'], array(), 'extra_');
                    ?>
                    </ul>
                    </div>
                    <?php
                    }
                    }
                    if($ajaxEnable) {
                    if($showAjaxButton) {
                    ?>
                    <div class="submit_place">
                        <div class="right">
                            <a href="#" class="send button">Send</a>
                        </div>
                    </div>
                    <?php
                    } else {
                    ?>
                    <div class="message success"><?php echo $postedValue; ?></div>
                    <?php
                    }
                    }
                    ?>
                </li>
                <?php
                /*
                * END FORM BUILDER
                */
            }
            $html = ob_get_contents();
            ob_end_clean();
        }
        return $html;
    }
    function getCountries() {
        ob_start();
        ?>
        <option value="" selected="selected"></option><option value="United States"> United States </option><option value="United Kingdom"> United Kingdom </option><option value="Australia"> Australia </option><option value="Canada"> Canada </option><option value="France"> France </option><option value="New Zealand"> New Zealand </option><option value="India"> India </option><option value="Brazil"> Brazil </option><option value="----"> ---- </option><option value="Afghanistan"> Afghanistan </option><option value="Åland Islands"> Åland Islands </option><option value="Albania"> Albania </option><option value="Algeria"> Algeria </option><option value="American Samoa"> American Samoa </option><option value="Andorra"> Andorra </option><option value="Angola"> Angola </option><option value="Anguilla"> Anguilla </option><option value="Antarctica"> Antarctica </option><option value="Antigua and Barbuda"> Antigua and Barbuda </option><option value="Argentina"> Argentina </option><option value="Armenia"> Armenia </option><option value="Aruba"> Aruba </option><option value="Austria"> Austria </option><option value="Azerbaijan"> Azerbaijan </option><option value="Bahamas"> Bahamas </option><option value="Bahrain"> Bahrain </option><option value="Bangladesh"> Bangladesh </option><option value="Barbados"> Barbados </option><option value="Belarus"> Belarus </option><option value="Belgium"> Belgium </option><option value="Belize"> Belize </option><option value="Benin"> Benin </option><option value="Bermuda"> Bermuda </option><option value="Bhutan"> Bhutan </option><option value="Bolivia"> Bolivia </option><option value="Bosnia and Herzegovina"> Bosnia and Herzegovina </option><option value="Botswana"> Botswana </option><option value="British Indian Ocean Territory"> British Indian Ocean Territory </option><option value="Brunei Darussalam"> Brunei Darussalam </option><option value="Bulgaria"> Bulgaria </option><option value="Burkina Faso"> Burkina Faso </option><option value="Burundi"> Burundi </option><option value="Cambodia"> Cambodia </option><option value="Cameroon"> Cameroon </option><option value="Cape Verde"> Cape Verde </option><option value="Cayman Islands"> Cayman Islands </option><option value="Central African Republic"> Central African Republic </option><option value="Chad"> Chad </option><option value="Chile"> Chile </option><option value="China"> China </option><option value="Colombia"> Colombia </option><option value="Comoros"> Comoros </option><option value="Democratic Republic of the Congo"> Democratic Republic of the Congo </option><option value="Republic of the Congo"> Republic of the Congo </option><option value="Cook Islands"> Cook Islands </option><option value="Costa Rica"> Costa Rica </option><option value="Côte dIvoire"> Côte dIvoire </option><option value="Croatia"> Croatia </option><option value="Cuba"> Cuba </option><option value="Cyprus"> Cyprus </option><option value="Czech Republic"> Czech Republic </option><option value="Denmark"> Denmark </option><option value="Djibouti"> Djibouti </option><option value="Dominica"> Dominica </option><option value="Dominican Republic"> Dominican Republic </option><option value="East Timor"> East Timor </option><option value="Ecuador"> Ecuador </option><option value="Egypt"> Egypt </option><option value="El Salvador"> El Salvador </option><option value="Equatorial Guinea"> Equatorial Guinea </option><option value="Eritrea"> Eritrea </option><option value="Estonia"> Estonia </option><option value="Ethiopia"> Ethiopia </option><option value="Faroe Islands"> Faroe Islands </option><option value="Fiji"> Fiji </option><option value="Finland"> Finland </option><option value="Gabon"> Gabon </option><option value="Gambia"> Gambia </option><option value="Georgia"> Georgia </option><option value="Germany"> Germany </option><option value="Ghana"> Ghana </option><option value="Gibraltar"> Gibraltar </option><option value="Greece"> Greece </option><option value="Grenada"> Grenada </option><option value="Guatemala"> Guatemala </option><option value="Guinea"> Guinea </option><option value="Guinea-Bissau"> Guinea-Bissau </option><option value="Guyana"> Guyana </option><option value="Haiti"> Haiti </option><option value="Honduras"> Honduras </option><option value="Hong Kong"> Hong Kong </option><option value="Hungary"> Hungary </option><option value="Iceland"> Iceland </option><option value="Indonesia"> Indonesia </option><option value="Iran"> Iran </option><option value="Iraq"> Iraq </option><option value="Ireland"> Ireland </option><option value="Israel"> Israel </option><option value="Italy"> Italy </option><option value="Jamaica"> Jamaica </option><option value="Japan"> Japan </option><option value="Jordan"> Jordan </option><option value="Kazakhstan"> Kazakhstan </option><option value="Kenya"> Kenya </option><option value="Kiribati"> Kiribati </option><option value="North Korea"> North Korea </option><option value="South Korea"> South Korea </option><option value="Kuwait"> Kuwait </option><option value="Kyrgyzstan"> Kyrgyzstan </option><option value="Laos"> Laos </option><option value="Latvia"> Latvia </option><option value="Lebanon"> Lebanon </option><option value="Lesotho"> Lesotho </option><option value="Liberia"> Liberia </option><option value="Libya"> Libya </option><option value="Liechtenstein"> Liechtenstein </option><option value="Lithuania"> Lithuania </option><option value="Luxembourg"> Luxembourg </option><option value="Macedonia"> Macedonia </option><option value="Madagascar"> Madagascar </option><option value="Malawi"> Malawi </option><option value="Malaysia"> Malaysia </option><option value="Maldives"> Maldives </option><option value="Mali"> Mali </option><option value="Malta"> Malta </option><option value="Marshall Islands"> Marshall Islands </option><option value="Mauritania"> Mauritania </option><option value="Mauritius"> Mauritius </option><option value="Mexico"> Mexico </option><option value="Micronesia"> Micronesia </option><option value="Moldova"> Moldova </option><option value="Monaco"> Monaco </option><option value="Mongolia"> Mongolia </option><option value="Montenegro"> Montenegro </option><option value="Morocco"> Morocco </option><option value="Mozambique"> Mozambique </option><option value="Myanmar"> Myanmar </option><option value="Namibia"> Namibia </option><option value="Nauru"> Nauru </option><option value="Nepal"> Nepal </option><option value="Netherlands"> Netherlands </option><option value="Netherlands Antilles"> Netherlands Antilles </option><option value="Nicaragua"> Nicaragua </option><option value="Niger"> Niger </option><option value="Nigeria"> Nigeria </option><option value="Norway"> Norway </option><option value="Oman"> Oman </option><option value="Pakistan"> Pakistan </option><option value="Palau"> Palau </option><option value="Palestine"> Palestine </option><option value="Panama"> Panama </option><option value="Papua New Guinea"> Papua New Guinea </option><option value="Paraguay"> Paraguay </option><option value="Peru"> Peru </option><option value="Philippines"> Philippines </option><option value="Poland"> Poland </option><option value="Portugal"> Portugal </option><option value="Puerto Rico"> Puerto Rico </option><option value="Qatar"> Qatar </option><option value="Romania"> Romania </option><option value="Russia"> Russia </option><option value="Rwanda"> Rwanda </option><option value="Saint Kitts and Nevis"> Saint Kitts and Nevis </option><option value="Saint Lucia"> Saint Lucia </option><option value="Saint Vincent and the Grenadines"> Saint Vincent and the Grenadines </option><option value="Samoa"> Samoa </option><option value="San Marino"> San Marino </option><option value="Sao Tome and Principe"> Sao Tome and Principe </option><option value="Saudi Arabia"> Saudi Arabia </option><option value="Senegal"> Senegal </option><option value="Serbia and Montenegro"> Serbia and Montenegro </option><option value="Seychelles"> Seychelles </option><option value="Sierra Leone"> Sierra Leone </option><option value="Singapore"> Singapore </option><option value="Slovakia"> Slovakia </option><option value="Slovenia"> Slovenia </option><option value="Solomon Islands"> Solomon Islands </option><option value="Somalia"> Somalia </option><option value="South Africa"> South Africa </option><option value="Spain"> Spain </option><option value="Sri Lanka"> Sri Lanka </option><option value="Sudan"> Sudan </option><option value="Suriname"> Suriname </option><option value="Swaziland"> Swaziland </option><option value="Sweden"> Sweden </option><option value="Switzerland"> Switzerland </option><option value="Syria"> Syria </option><option value="Taiwan"> Taiwan </option><option value="Tajikistan"> Tajikistan </option><option value="Tanzania"> Tanzania </option><option value="Thailand"> Thailand </option><option value="Togo"> Togo </option><option value="Tonga"> Tonga </option><option value="Trinidad and Tobago"> Trinidad and Tobago </option><option value="Tunisia"> Tunisia </option><option value="Turkey"> Turkey </option><option value="Turkmenistan"> Turkmenistan </option><option value="Tuvalu"> Tuvalu </option><option value="Uganda"> Uganda </option><option value="Ukraine"> Ukraine </option><option value="United Arab Emirates"> United Arab Emirates </option><option value="United States Minor Outlying Islands"> United States Minor Outlying Islands </option><option value="Uruguay"> Uruguay </option><option value="Uzbekistan"> Uzbekistan </option><option value="Vanuatu"> Vanuatu </option><option value="Vatican City"> Vatican City </option><option value="Venezuela"> Venezuela </option><option selected="selected" value="Vietnam"> Vietnam </option><option value="Virgin Islands, British"> Virgin Islands, British </option><option value="Virgin Islands, U.S."> Virgin Islands, U.S. </option><option value="Yemen"> Yemen </option><option value="Zambia"> Zambia </option><option value="Zimbabwe"> Zimbabwe </option>
        <?php
        $html = ob_get_contents();
        ob_clean();
        return $html;
    }
    /*
    * Functions Checking
    */
    function form_check_password($args) {
        extract($args);
        $form_pass = $result['form_password'];
        /*
        * Protect Pass Check
        */
        if(!empty($form_pass)) {
            if(!isset($_SESSION['form_'.$formId])) {
                $post_pass = isset($posts['passKey'])?$posts['passKey']:'';
                $md5_post_pass = md5($this->helpers->escape_string($post_pass));
                if(empty($post_pass) || ($md5_post_pass != $form_pass )) {
            
                } else {
                    $_SESSION['form_'.$formId] = $formId;
                }
                $this->view->pass_err = 'Wrong password!';
                $this->setAllowNext(true);
                exit();
            }
        }
    }
    function form_login_check($args) {
        extract($args);
        if(!isset($_SESSION['user_normal']) && !isset($_SESSION['admin_user'])) {
            $username = isset($posts['username'])?$posts['username']:'';
            $password = isset($posts['password'])?$posts['password']:'';
            $username = $this->helpers->escape_string($username);
            $password = md5($this->helpers->escape_string($password));
            if($user_result = $users_obj->userLogin($username, $password) ) {
                if($user_result['user_type'] == 'A') {
                    $_SESSION['admin_user'] = $user_result['user_id'];
                } elseif($user_result['user_type'] == 'C') {
                    $_SESSION['user_normal'] = $user_result['user_id'];
                }
            } else {
                $this->view->errors['fail'] = 'Wrong Username or Password!';
            }
            $this->setAllowNext(true);
            exit();
        }
    }
    /*
    * Post
    */
    function submitForm() {
        $posts = isset($_POST)?$_POST:array();
        // get router
        $routers = $this->registry->router->getRouters();
        // off magic quotes gpc
        $posts = $this->helpers->unescape_string($posts);
        /*
        *
        */
        $formId = $routers['id'];
        $entryId = $routers['id2'];
        $ip = isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:'';
        $ip = $this->helpers->escape_string($ip);
        $formId = $this->helpers->escape_string($formId);
        $entryId = $this->helpers->escape_string($entryId);
        $forms_obj = new Models_FormsModel($this->registry);
        $subjects_obj = new Models_SubjectsModel($this->registry);
        $entries_obj = new Models_EntriesModel($this->registry);
        $notfound_obj = new Controllers_notfoundController($this->registry);
        $result = $forms_obj->getForm($formId,'','form_uuid');
        if(!$result || empty($posts)) {
            $notfound_obj->index();
            exit();
        }
        $args = array(
            'result' => $result,
            'forms_obj' => $forms_obj,
            'subjects_obj' => $subjects_obj,
            'notfound_obj' => $notfound_obj
        );
        $this->checkSubject($args);
        $field_id1 = false;
        $f = $this->registry->router->f;
        $args = array(
            'posts' => $posts,
            'result' => $result,
            'forms_obj' => $forms_obj,
            'entries_obj' => $entries_obj,
            'notfound_obj' => $notfound_obj
        );
        /*
        * Protect Pass Check
        */
        $form_pass = $result['form_password'];
        $args['form_pass'] = $form_pass;
        $this->form_check_password($args);
        
        $form = ($result['form_content']);
        $form_id = $result['form_id'];
        $_form = $this->helpers->json_decode_to_array($form);
        $_notify = $this->helpers->json_decode_to_array($result['form_notify']);
        /*
        * Form Type Check
        */
        $formType = $_form['Formtype'];
        $args['formType'] = $formType;
        $normal_login = false;
        if($formType == 'normal') {
            if($_form['UserLogin']) {
                $normal_login = true;
            }
        }
        if($formType == 'nopbai' || $formType == 'score' || $formType == 'autoscore' || $normal_login) {
            $user_scoring_id = $this->helpers->getCurrentUser();
            $users_obj = new Models_UsersModel($this->registry);
            // login check
            $args['users_obj'] = $users_obj;
            $args['user_scoring_id'] = $user_scoring_id;
            $this->form_login_check($args);
        }
        $updateEntry = false;
        $formUserSubmitLimit = isset($_form['UserSubmitLimit'])?$_form['UserSubmitLimit']:1;
        if(!is_numeric($formUserSubmitLimit) || $formUserSubmitLimit < 0) $formUserSubmitLimit = 1;
        if($formType == 'autoscore') {
            if($userScored = $entries_obj->scoringOnce($user_scoring_id, $form_id)) {
                if($formUserSubmitLimit != 0 && $userScored['entry_flag'] >= $formUserSubmitLimit) {
                    $this->view->message = 'Bạn Không Được Phép Gửi Bài Này Quá Nhiều Lần. (' . $formUserSubmitLimit . ' Tiny Times)';
                    $this->view->render('message','only');
                    exit();
                } else {
                    $updateEntry = true;
                    $entry_upd_id = $userScored['entry_id'];
                    $entry_upd_uuid = $userScored['entry_uuid'];
                }
            }
        }
        if($normal_login) {
            if($userScored = $entries_obj->submitingOnce($user_scoring_id, $form_id)) {
                if($formUserSubmitLimit != 0 && $userScored['entry_flag'] >= $formUserSubmitLimit) {
                    $this->view->message = 'Bạn Không Được Phép Gửi Bài Này Quá Nhiều Lần. (' . $formUserSubmitLimit . ' Tiny Times)';
                    $this->view->render('message','only');
                    exit();
                } else {
                    $updateEntry = true;
                    $entry_upd_id = $userScored['entry_id'];
                }
            }
        }
        $ajaxEnable = false;
        if($formType == 'autoscore' || $normal_login) {
            $ajaxEnable = isset($_form['AjaxEnable'])?$_form['AjaxEnable']:false;
            if(!empty($f)) {
                $part = explode('_',$this->helpers->decrypt($f,'dpkey'));
                if(!isset($part[1])) {
                    $notfound_obj->index();
                    return;
                }
                $part2 = explode('-',$part[1]);
                if(!isset($part2[1]) || !is_numeric($part2[1])) {
                    $notfound_obj->index();
                    return;
                }
                $field_id1 = $part2[1];
            }
        }
        /*
        * Date check
        */
        $start_date = $_form['StartDate'];
        $end_date = $_form['EndDate'];
        $current_date = $this->helpers->get_datetime();
        if((strtotime($end_date) < strtotime($current_date)) || (strtotime($start_date) > strtotime($current_date))) {
            $this->view->message = 'Form đã hết hạn';
            $this->view->render('message','only');
            exit();
        }
        /*
        * Captcha check
        */
        $captcha_err = array();
        if($_form['UseCaptcha']) {
            require_once('lib/recaptchalib.php');
            $privatekey = "6LfNO9wSAAAAAC6Khm2xwF1iTKY2OzwWPpRj3yI8";
            $resp = recaptcha_check_answer ($privatekey,
                                        $_SERVER["REMOTE_ADDR"],
                                        $_POST["recaptcha_challenge_field"],
                                        $_POST["recaptcha_response_field"]);
            
            if (!$resp->is_valid) {
                // What happens when the CAPTCHA was entered incorrectly
                $errors['captcha'] = ("$resp->error");
                $captcha_err['recaptcha_response_field'] = array(
                    'message' => 'The CAPTCHA wasn\'t entered correctly.'
                );
            }
        }
        /*
        * IP check
        */
        if($_form['UniqueIP'] || $_form['EntryLimit'] > 0) {
            $ents = $entries_obj->entryOnce($form_id);
            if($_form['UniqueIP']) {
                if($ents) {
                    foreach($ents as $ent) {
                        if($ip == $ent['entry_ip']) {
                            $this->view->errors['limit'] = 'Sorry, but this form is limited to one submission per user. ';
                            $this->setAllowNext(true);
                            return;
                        }
                    }
                }
            } elseif(count($ents) > $_form['EntryLimit']) {
                $this->view->errors['limit'] = 'Limited submission for this form. Sory!';
                $this->setAllowNext(true);
                return;
            }
            
        }
        /*
        * Field Errors Check
        */
        $singleField = false;
        $_fields = $_form['Fields'];
        if($field_id1) {
            $_fields = $this->helpers->getFieldById($_form['Fields'], $field_id1);
            $singleField = true;
            if(!$_fields) {
                $notfound_obj->index();
                exit();
            }
        }
        $posted = $posted1 = array();
        $errorFields = $this->helpers->validateFields($_fields,$posts);
        $sendConfirm = $this->helpers->checkSendConfirm($_fields);
        $errorFields = $errorFields + $captcha_err;
        if(empty($errorFields)) {
            $logs_obj = new Models_FormsLogModel($this->registry);
            $siteUrl = $this->helpers->getUrl();
            if($updateEntry) {
                $posted1 = $this->helpers->json_decode_to_array($userScored['entry_content']);
                if($ajaxEnable && !$singleField) {
                    $posts = array_merge($posts, $posted1);
                } else {
                    $posts = array_merge($posted1, $posts);
                }
            }
            $posted = $this->helpers->json_encode_then_escape($posts);
            $alt1 = $alt2 = $alt3 = $alt4 = '';
            $comp = 1;
            if(!empty($sendConfirm)) {
               $alt1 = ",`entry_complete`";
               $alt2 = ",'0'";
               $comp = 0;   
            }
            /*
            * Params To Insert To DB
            */
            $user_id = $this->helpers->getUserByAdmin();
            $entry_uuid = 0;
            if($formType == 'nopbai') {
                $entry_uuid = $this->helpers->uuidSecure();
                $alt3 = ",`entry_uuid`,`entry_type`,`entry_user`";
                $alt4 = ",'$entry_uuid','NB','$user_id'";
            } elseif($formType == 'score') {
                $scored = $this->helpers->scoring($posts, $_fields);
                $entry_uuid = $this->helpers->uuidSecure();
                $scores = $this->helpers->json_encode_then_escape($scored);
                $alt3 = ",`entry_uuid`,`entry_type`,`entry_user`,`entry_of`,`entry_scoring`";
                $alt4 = ",'$entry_uuid','SC','$user_id','$entryId','$scores'";
            } elseif($formType == 'autoscore') {
                $scores2 = $this->helpers->scoring($posts, $_fields);
                $flag = 1;
                if($ajaxEnable && !$singleField) {
                    $flag = count($_fields) - $this->helpers->getPostCount($posted1, 'Field');
                    if($flag <= 0) $flag = 1;
                }
                $entry_uuid = $this->helpers->uuidSecure();
                $scores = $this->helpers->json_encode_then_escape($scores2);
                $alt3 = ",`entry_uuid`,`entry_type`,`entry_user`,`entry_of`,`entry_scoring`,`entry_flag`";
                $alt4 = ",'$entry_uuid','AS','$user_id','$entry_uuid','$scores','$flag'";
                $upd1 = ",`entry_scoring` = '$scores', `entry_flag` = `entry_flag` + $flag";
            } elseif($normal_login) {
                $flag = 1;
                if($ajaxEnable && !$singleField) {
                    $flag = count($_fields) - $this->helpers->getPostCount($posted1, 'Field');
                    if($flag <= 0) $flag = 1;
                }
                $alt3 = ",`entry_user`,`entry_flag`";
                $alt4 = ",'$user_id','$flag'";
                $upd1 = ",`entry_flag` = `entry_flag` + $flag";
            }
            $entd = $this->helpers->get_datetime();
            if($updateEntry) {
                if($entries_obj->updateEntry("`entry_ip` = '$ip', `entry_update` = '$entd', `entry_content` = '$posted' $upd1","`entry_id` = '$entry_upd_id'",'1')) {
                    if($formType == 'autoscore') {
                        $caches_obj = new Controllers_cachesController($this->registry);
                        $entry_content = $entries_obj->getEntry('',$entry_upd_id);
                        
                        $user_id2 = $entry_content['entry_user'];
                        $user_nb = $users_obj->getUser("`user_id` = '$user_id2'");
                        $display_ = $caches_obj->getFormASDisplayResult($result,$entry_content,$user_nb);
                        $this->view->display_ = $display_;
                        $this->view->entry_uuid = $entry_upd_uuid;
                        // Wrong Answers Suggestions
                        $scoring_detail = $this->helpers->scoring_detail($_fields,$entry_content);
                        
                        $logs = array();
                        foreach($scoring_detail as $sd) {
                            $field_id = $sd['FieldId'];
                            
                            $log_subject = $sd['Title'];
                            if(is_string($sd['Value'])) $log_subject = $sd['Value'];
                            $log_subject = $this->helpers->escape_string(trim($log_subject));
                            $log = $logs_obj->getLog("`form_id` = '$form_id' AND `field_id` = '$field_id' AND `log_subject` = '$log_subject'");
                            $old_logconten = $this->helpers->json_decode_to_array($log['log_content']);
                            $old_logconten = is_array($old_logconten)?$old_logconten:array();
                            if($ajaxEnable && !$singleField) {
                                $sd = array_merge($sd, $old_logconten);
                            } else {
                                $sd = array_merge($old_logconten, $sd);
                            }
                            $log_content = $this->helpers->json_encode_then_escape($sd);
                            
                            if($log) {
                                $log_id = $log['log_id'];
                                $logs_obj->updateLog("`log_hit` = `log_hit` + 1, `log_update` = '$entd', `log_content` = '$log_content', `log_subject` = '$log_subject'", "`log_id` = '$log_id'");
                            } else {
                                $logs_obj->insertLog('`form_id`, `field_id`, `log_subject`, `log_content`', "'$form_id', '$field_id', '$log_subject', '$log_content'");
                                $log_id = $logs_obj->registry->mysqli->insert_id;
                                
                                $logs_obj->insertLogUser('`user_id`, `log_id`', "'$user_id', '$log_id'");
                                
                                $log = array(
                                    'log_id' => $log_id,
                                    'field_id' => $field_id,
                                    'log_subject' => $sd['Value'],
                                    'log_content2' => $sd,
                                    'log_hit' => 1
                                );
                            }
                            // $log_user = $logs_obj->getLogUser("`user_id` = '$user_id' AND `log_id` = '$log_id'");
                            // if(!$log_user) {
                            // $logs_obj->insertLogUser('`user_id`, `log_id`', "'$user_id', '$log_id'");
                            // }
                            $logs[] = $log;
                        }
                        $this->view->scoring = $scoring_detail;
                        $this->view->logs = $logs;
                        
                        $this->view->render('yourscore','only');
                        return;
                    }
                    header('location: '.$siteUrl.'confirm/'.$formId);
                    exit();
                }
            } else {
                if($entries_obj->insertEntry("`entry_ip`,`entry_content` $alt1 $alt3", "'$ip','$posted' $alt2 $alt4")) {
                    $entry_id = $entries_obj->registry->mysqli->insert_id;
                    if($entries_obj->insertEntryMeta('`form_id`,`entry_id`', "'$form_id','$entry_id'")) {
                        if(!empty($_notify['Email'])) {
                            $html = 'Click <a href="'.$siteUrl.'entries/'.$formId.'">here</a> for details';
                            $address[$_notify['Email']] = $_notify['ConfirmationFromAddress'];
                            $replyTo = isset($posts['Field'.$_notify['ReplyTo']])?$posts['Field'.$_notify['ReplyTo']]:'';
                            $options[] = $_notify['ConfirmationFromAddress'];
                            //$_notify['ConfirmationSubject'];
                            $this->helpers->sendMail('New Submission from: '.$_form['Name'],$html, $address, $replyTo, $options);
                        }
                        if(!empty($sendConfirm)) {
                            if(!empty($posts['Field'.$sendConfirm['ColumnId']])) {
            					$str = $entry_id . '' . md5($entry_id);
                                $html = 'Click <a href="'.$siteUrl.'confirm/&a='.$str.'">here</a> to confirm your submission.';
                                $address[$posts['Field'.$sendConfirm['ColumnId']]] = $_form['FromAddress'];
                                $replyTo = $_form['ReceiptReplyTo'];
                                $options[] = $_form['FromAddress'];
                                //$_notify['ConfirmationSubject'];
                                $this->helpers->sendMail('Confirm submission from '.$_form['Name'],$html, $address, $replyTo, $options);
                            }
                        }
                        if($formType == 'autoscore') {
                            $caches_obj = new Controllers_cachesController($this->registry);
                            $entry_content = $entries_obj->getEntry('',$entry_id);
                            $user_id2 = $entry_content['entry_user'];
                            $user_nb = $users_obj->getUser("`user_id` = '$user_id2'");
                            $display_ = $caches_obj->getFormASDisplayResult($result,$entry_content,$user_nb);
                            $this->view->display_ = $display_;
                            $this->view->entry_uuid = $entry_uuid;
                            
                            // Wrong Answers Suggestions
                            $scoring_detail = $this->helpers->scoring_detail($_fields,$entry_content);
                            $logs = array();
                            foreach($scoring_detail as $sd) {
                                $field_id = $sd['FieldId'];
                                
                                $log_subject = $sd['Title'];
                                if(is_string($sd['Value'])) $log_subject = $sd['Value'];
                                $log = $logs_obj->getLog("`form_id` = '$form_id' AND `field_id` = '$field_id'");
                                
                                $old_logconten = $this->helpers->json_decode_to_array($log['log_content']);
                                $old_logconten = is_array($old_logconten)?$old_logconten:array();
                                if($ajaxEnable && !$singleField) {
                                    $sd = array_merge($sd, $old_logconten);
                                } else {
                                    $sd = array_merge($old_logconten, $sd);
                                }
                                $log_content = $this->helpers->json_encode_then_escape($sd);
                                
                                if($log) {
                                    $log_id = $log['log_id'];
                                    $logs_obj->updateLog("`log_hit` = `log_hit` + 1, `log_update` = '$entd', `log_content` = '$log_content', `log_subject` = '$log_subject'", "`log_id` = '$log_id'");
                                } else {
                                    $logs_obj->insertLog('`form_id`, `field_id`, `log_subject`, `log_content`', "'$form_id', '$field_id', '$log_subject', '$log_content'");
                                    $log_id = $logs_obj->registry->mysqli->insert_id;
                                    
                                    $logs_obj->insertLogUser('`user_id`, `log_id`', "'$user_id', '$log_id'");
                                    
                                    $log = array(
                                        'log_id' => $log_id,
                                        'field_id' => $field_id,
                                        'log_subject' => $log_subject,
                                        'log_content2' => $sd,
                                        'log_hit' => 1
                                    );
                                }
                                $logs[] = $log;
                            }
                            $this->view->scoring = $scoring_detail;
                            $this->view->logs = $logs;
                            
                            
                            $this->view->render('yourscore','only');
                            return;   
                        }
                        header('location: '.$siteUrl.'confirm/'.$formId);
                        exit();
                    }
                }
            }
            if(!empty($this->registry->mysqli->error)) {
                $errors['field'] = 'Some errors occured!';
            }
        } else {
            $errors['field'] = 'Some errors occured!';
        }
        foreach($posts as $k => $post) {
            if(substr($k,0,5) == 'Field') {
                $posted[$k] = $post;
            }
        }
        $entry = json_encode($posted);
        $this->view->entry = $entry;
        if(isset($errorFields)) $this->view->errorFields = $errorFields;
        $this->view->errors = $errors;
        $this->setAllowNext(true);
        
        /*
        *
        */
    }
    function submitField() {
        $this->forms_saveindex();
        echo json_encode($this->json);  
    }
    function forms_saveindex() {
        $posts = isset($_POST)?$_POST:array();
        // get router
        $routers = $this->registry->router->getRouters();
        // off magic quotes gpc
        $posts = $this->helpers->unescape_string($posts);
        /*
        *
        */
        $formId = $routers['id'];
        $entryId = $routers['id2'];
        $ip = isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:'';
        $ip = $this->helpers->escape_string($ip);
        $formId = $this->helpers->escape_string($formId);
        $entryId = $this->helpers->escape_string($entryId);
        $forms_obj = new Models_FormsModel($this->registry);
        $subjects_obj = new Models_SubjectsModel($this->registry);
        $entries_obj = new Models_EntriesModel($this->registry);
        $notfound_obj = new Controllers_notfoundController($this->registry);
        $result = $forms_obj->getForm($formId,'','form_uuid');
        if(!$result || empty($posts)) {
            return;
        }
        $args = array(
            'result' => $result,
            'forms_obj' => $forms_obj,
            'subjects_obj' => $subjects_obj,
            'notfound_obj' => $notfound_obj
        );
        $this->checkSubject($args);
        $field_id1 = false;
        $f = $this->registry->router->f;
        
        $form = ($result['form_content']);
        $form_id = $result['form_id'];
        $_form = $this->helpers->json_decode_to_array($form);
        $_notify = $this->helpers->json_decode_to_array($result['form_notify']);
        /*
        * Form Type Check
        */
        $formType = $_form['Formtype'];
        $normal_login = false;
        if($formType == 'normal') {
            if($_form['UserLogin']) {
                $normal_login = true;
            }
        }
        if($formType == 'nopbai' || $formType == 'score' || $formType == 'autoscore' || $normal_login) {
            $user_scoring_id = $this->helpers->getCurrentUser();
            $users_obj = new Models_UsersModel($this->registry);
            // login check
            $args['users_obj'] = $users_obj;
            $args['user_scoring_id'] = $user_scoring_id;
            $this->form_login_check($args);
        }
        $updateEntry = false;
        $formUserSubmitLimit = isset($_form['UserSubmitLimit'])?$_form['UserSubmitLimit']:1;
        if(!is_numeric($formUserSubmitLimit) || $formUserSubmitLimit < 0) $formUserSubmitLimit = 1;
        if($formType == 'autoscore') {
            if($userScored = $entries_obj->scoringOnce($user_scoring_id, $form_id)) {
                if($formUserSubmitLimit != 0 && $userScored['entry_flag'] >= $formUserSubmitLimit) {
                    $this->json['response']['message'] = 'Bạn Không Được Phép Gửi Bài Này Quá ' . $formUserSubmitLimit . ' Lần';
                    return;
                } else {
                    $updateEntry = true;
                    $entry_upd_id = $userScored['entry_id'];
                    $entry_upd_uuid = $userScored['entry_uuid'];
                }
            }
        }
        if($normal_login) {
            if($userScored = $entries_obj->submitingOnce($user_scoring_id, $form_id)) {
                if($formUserSubmitLimit != 0 && $userScored['entry_flag'] >= $formUserSubmitLimit) {
                    $this->json['response']['message'] = 'Bạn Không Được Phép Gửi Bài Này Quá ' . $formUserSubmitLimit . ' Lần';
                    return;
                } else {
                    $updateEntry = true;
                    $entry_upd_id = $userScored['entry_id'];
                }
            }
        }
        $ajaxEnable = false;
        if($formType == 'autoscore' || $normal_login) {
            $ajaxEnable = isset($_form['AjaxEnable'])?$_form['AjaxEnable']:false;
            $ajaxShowWhatJustType = isset($_form['AjaxShowWhatJustType'])?$_form['AjaxShowWhatJustType']:true;
            $ajaxLiveResponse = isset($_form['AjaxLiveResponse'])?$_form['AjaxLiveResponse']:false;
            if(!empty($f)) {
                // $part = explode('_',$this->helpers->decrypt($f,'dpkey'));
                // if(!isset($part[1])) {
                // $notfound_obj->index();
                // return;
                // }
                // $part2 = explode('-',$part[1]);
                // if(!isset($part2[1]) || !is_numeric($part2[1])) {
                // $notfound_obj->index();
                // return;
                // }
                $field_id1 = $f;
            }
        }
        /*
        * IP check
        */
        if($_form['UniqueIP'] || $_form['EntryLimit'] > 0) {
            $ents = $entries_obj->entryOnce($form_id);
            if(count($ents) > $_form['EntryLimit']) {
                $this->json['response']['message'] = 'Limited submission for this form. Sory!';
                return;
            }
            
        }
        /*
        * Field Errors Check
        */
        $singleField = false;
        $_fields = $_form['Fields'];
        if($field_id1) {
            $_fields = $this->helpers->getFieldById($_form['Fields'], $field_id1);
            $singleField = true;
            if(!$_fields) {
                $this->json['response']['message'] = 'Some Errors Occured. Check What You Typed Or Reload The Page.';
                return;
            }
        }
        $posted = $posted1 = array();
        $errorFields = $this->helpers->validateFields($_fields,$posts);
        if(empty($errorFields)) {
            $logs_obj = new Models_FormsLogModel($this->registry);
            $siteUrl = $this->helpers->getUrl();
            if($updateEntry) {
                $posted1 = $this->helpers->json_decode_to_array($userScored['entry_content']);
                if($ajaxEnable && !$singleField) {
                    $posts = array_merge($posts, $posted1);
                } else {
                    $posts = array_merge($posted1, $posts);
                }
            }
            $posted = $this->helpers->json_encode_then_escape($posts);
            $alt1 = $alt2 = $alt3 = $alt4 = '';
            $comp = 1;
            /*
            * Params To Insert To DB
            */
            $user_id = $this->helpers->getUserByAdmin();
            $entry_uuid = 0;
            $lastFieldSubmit = false;
            if($formType == 'nopbai') {
                $entry_uuid = $this->helpers->uuidSecure();
                $alt3 = ",`entry_uuid`,`entry_type`,`entry_user`";
                $alt4 = ",'$entry_uuid','NB','$user_id'";
            } elseif($formType == 'score') {
                $scored = $this->helpers->scoring($posts, $_fields);
                $entry_uuid = $this->helpers->uuidSecure();
                $scores = $this->helpers->json_encode_then_escape($scored);
                $alt3 = ",`entry_uuid`,`entry_type`,`entry_user`,`entry_of`,`entry_scoring`";
                $alt4 = ",'$entry_uuid','SC','$user_id','$entryId','$scores'";
            } elseif($formType == 'autoscore') {
                $scores2 = $this->helpers->scoring($posts, $_fields);
                $flag = 1;
                if($ajaxEnable && !$singleField) {
                    $flag = count($_fields) - $this->helpers->getPostCount($posted1, 'Field');
                    if($flag <= 0) $flag = 1;
                } else {
                    if($formUserSubmitLimit - $userScored['entry_flag'] <= 1) {
                        $lastFieldSubmit = true;
                    }
                }
                $entry_uuid = $this->helpers->uuidSecure();
                $scores = $this->helpers->json_encode_then_escape($scores2);
                $alt3 = ",`entry_uuid`,`entry_type`,`entry_user`,`entry_of`,`entry_scoring`";
                $alt4 = ",'$entry_uuid','AS','$user_id','$entry_uuid','$scores'";
                $upd1 = ",`entry_scoring` = '$scores', `entry_flag` = `entry_flag` + $flag";
            } elseif($normal_login) {
                $flag = 1;
                if($ajaxEnable && !$singleField) {
                    $flag = count($_fields) - $this->helpers->getPostCount($posted1, 'Field');
                    if($flag <= 0) $flag = 1;
                } else {
                    if($formUserSubmitLimit - $userScored['entry_flag'] <= 1) {
                        $lastFieldSubmit = true;
                    }
                }
                $alt3 = ",`entry_user`";
                $alt4 = ",'$user_id'";
                $upd1 = ",`entry_flag` = `entry_flag` + $flag";
            }
            $entd = $this->helpers->get_datetime();
            if($updateEntry) {
                if($entries_obj->updateEntry("`entry_ip` = '$ip', `entry_update` = '$entd', `entry_content` = '$posted' $upd1","`entry_id` = '$entry_upd_id'",'1')) {
                    if($formType == 'autoscore') {
                        $caches_obj = new Controllers_cachesController($this->registry);
                        $entry_content = $entries_obj->getEntry('',$entry_upd_id);
                        
                        $user_id2 = $entry_content['entry_user'];
                        $user_nb = $users_obj->getUser("`user_id` = '$user_id2'");
                        $display_ = $caches_obj->getFormASDisplayResult($result,$entry_content,$user_nb);
                        $this->view->display_ = $display_;
                        $this->view->entry_uuid = $entry_upd_uuid;
                        // Wrong Answers Suggestions
                        $scoring_detail = $this->helpers->scoring_detail($_fields,$entry_content);
                        
                        $logs = array();
                        foreach($scoring_detail as $sd) {
                            $field_id = $sd['FieldId'];
                            
                            $log_subject = $sd['Title'];
                            if(is_string($sd['Value'])) $log_subject = $sd['Value'];
                            $log_subject = $this->helpers->escape_string(trim($log_subject));
                            $log = $logs_obj->getLog("`form_id` = '$form_id' AND `field_id` = '$field_id' AND `log_subject` = '$log_subject'");
                            $old_logconten = $this->helpers->json_decode_to_array($log['log_content']);
                            if($ajaxEnable && !$singleField) {
                                $sd = array_merge($sd, $old_logconten);
                            } else {
                                $sd = array_merge($old_logconten, $sd);
                            }
                            $log_content = $this->helpers->json_encode_then_escape($sd);
                            if($log) {
                                $log_id = $log['log_id'];
                                $logs_obj->updateLog("`log_hit` = `log_hit` + 1, `log_update` = '$entd', `log_content` = '$log_content', `log_subject` = '$log_subject'", "`log_id` = '$log_id'");
                            } else {
                                $logs_obj->insertLog('`form_id`, `field_id`, `log_subject`, `log_content`', "'$form_id', '$field_id', '$log_subject', '$log_content'");
                                $log_id = $logs_obj->registry->mysqli->insert_id;
                                
                                $logs_obj->insertLogUser('`user_id`, `log_id`', "'$user_id', '$log_id'");
                                
                                $log = array(
                                    'log_id' => $log_id,
                                    'field_id' => $field_id,
                                    'log_subject' => $sd['Value'],
                                    'log_content2' => $sd,
                                    'log_hit' => 1
                                );
                            }
                            // $log_user = $logs_obj->getLogUser("`user_id` = '$user_id' AND `log_id` = '$log_id'");
                            // if(!$log_user) {
                            // $logs_obj->insertLogUser('`user_id`, `log_id`', "'$user_id', '$log_id'");
                            // }
                            $logs[] = $log;
                        }
                        if($lastFieldSubmit) {
                            $this->json['response']['review'] = $siteUrl . 'review/' . $entry_upd_uuid;
                        }
                        if($ajaxLiveResponse) {
                            $this->json['response']['lr'] = $scores2;
                        }
                    }
                    $this->json['swjt'] = $ajaxShowWhatJustType;
                    $this->json['success'] = true; 
                    return;
                }
            } else {
                if($entries_obj->insertEntry("`entry_ip`,`entry_content` $alt1 $alt3", "'$ip','$posted' $alt2 $alt4")) {
                    $entry_id = $entries_obj->registry->mysqli->insert_id;
                    if($entries_obj->insertEntryMeta('`form_id`,`entry_id`', "'$form_id','$entry_id'")) {
                        if($formType == 'autoscore') {
                            $caches_obj = new Controllers_cachesController($this->registry);
                            $entry_content = $entries_obj->getEntry('',$entry_id);
                            $user_id2 = $entry_content['entry_user'];
                            $user_nb = $users_obj->getUser("`user_id` = '$user_id2'");
                            $display_ = $caches_obj->getFormASDisplayResult($result,$entry_content,$user_nb);
                            $this->view->display_ = $display_;
                            $this->view->entry_uuid = $entry_uuid;
                            
                            // Wrong Answers Suggestions
                            $scoring_detail = $this->helpers->scoring_detail($_fields,$entry_content);
                            $logs = array();
                            foreach($scoring_detail as $sd) {
                                $field_id = $sd['FieldId'];
                                
                                $log_subject = $sd['Title'];
                                if(is_string($sd['Value'])) $log_subject = $sd['Value'];
                                $log = $logs_obj->getLog("`form_id` = '$form_id' AND `field_id` = '$field_id'");
                                $old_logconten = $this->helpers->json_decode_to_array($log['log_content']);
                                if($ajaxEnable && !$singleField) {
                                    $sd = array_merge($sd, $old_logconten);
                                } else {
                                    $sd = array_merge($old_logconten, $sd);
                                }
                                $log_content = $this->helpers->json_encode_then_escape($sd);
                                if($log) {
                                    $log_id = $log['log_id'];
                                    $logs_obj->updateLog("`log_hit` = `log_hit` + 1, `log_update` = '$entd', `log_content` = '$log_content', `log_subject` = '$log_subject'", "`log_id` = '$log_id'");
                                } else {
                                    $logs_obj->insertLog('`form_id`, `field_id`, `log_subject`, `log_content`', "'$form_id', '$field_id', '$log_subject', '$log_content'");
                                    $log_id = $logs_obj->registry->mysqli->insert_id;
                                    
                                    $logs_obj->insertLogUser('`user_id`, `log_id`', "'$user_id', '$log_id'");
                                    
                                    $log = array(
                                        'log_id' => $log_id,
                                        'field_id' => $field_id,
                                        'log_subject' => $log_subject,
                                        'log_content2' => $sd,
                                        'log_hit' => 1
                                    );
                                }
                                $logs[] = $log;
                            }
                            if($lastFieldSubmit) {
                                $this->json['response']['review'] = $siteUrl . 'review/' . $entry_upd_uuid;
                            }
                            if($ajaxLiveResponse) {
                                $this->json['response']['lr'] = $scores2;
                            }
                        }
                        $this->json['swjt'] = $ajaxShowWhatJustType;
                        $this->json['success'] = true; 
                        return;
                    }
                }
            }
            if(!empty($this->registry->mysqli->error)) {
                $this->json['response']['message'] = 'Some Errors Occured. Check What You Typed Or Reload The Page.';
            }
        } else {
            $this->json['response']['message'] = 'Some Errors Occured. Check What You Typed Or Reload The Page.';
        }
        /*
        *
        */
    }
    /*
    * INSERT TO DB
    */
    function insertToDb() {
    }
    /*
    *
    */
}