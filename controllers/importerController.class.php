<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_importerController extends Applications_BaseController {
    function userOnly($redirect) {
        $this->helpers->userNext($redirect);
    }
    function checkPermission() {
        $this->userOnly($this->helpers->getUrl() . 'auth/login&ref='.$this->helpers->curPageURL());
    }
    function index() {
        $siteUrl = $this->helpers->getUrl();
        /*
        *
        */
        $cached_obj = new Models_CachedModel($this->registry);
        $notfound_obj = new Controllers_notfoundController($this->registry);
        /*
        *
        */
		$extends = array(
            'Formtype' => 'autoscore',
            'FormScoreUrl' => '',
            'RedirectMessage' => 'Great! Thanks for filling out my form!',
            'UserSubmitLimit' => '',
            'FormScoreUrl' => '',
            'InputRightAnswerRegex' => false,
            'InputRightAnswer' => '',
            'Password' => '',
            'Redirect' => '',
            'Email' => '',
            'StyleId' => '1',
            'IsPublic' => '0',
            'UniqueIP' => '0',
            'ReplyTo' => '0',
            'ConfirmationFromAddress' => '',
            'ConfirmationSubject' => '',
            'ReceiptSubject' => '',
            'ReceiptReplyTo' => '',
            'FromAddress' => '',
            'ReceiptMessage' => '',
            'ReceiptCopy' => '',
            'ReceiptSubject' => '',
            'ReceiptReplyTo' => '',
            'FromAddress' => '',
            'ReceiptMessage' => '',
            'Language' => 'english',
            'EmailNewEntries' => '1',
            'EmailNewComments' => '0',
            'PhoneNewEntries' => '0',
            'PhoneNewComments' => '0',
            'Mobile' => '0',
            'Carrier' => '0',
            'MerchantEnabled' => '0',
            'StartDate' => '2000-01-01 12:00:00',
            'EndDate' => '2030-01-01 12:00:00',
            'EntryLimit' => '0',
            'PlainText' => '0',
            'UseCaptcha' => '0',
            'BreadCrumbType' => '1',
            'ShowPageTitle' => '1',
            'ShowPageFooter' => '1',
            'PaymentPageTitle' => 'Checkout',
            'RulesEnabled' => '2',
            'GAKey' => '0',
            'NonCriticalIntegrations' => null,
            'EntryCount' => null,
            'CurrentPage' => false,
            'PageCount' => '1',
            'PageBreaks' => array(),
            'EntryComments' => array(),
            'IntegrationList' => array(),
            'NoInstructions' => true,
            'Entries' => array()
        );
        $this->view->title = 'Forms Contributer';
        $this->view->render('importer/importer');
    }
    function postReq() {
        $req = $this->registry->req;
        $ajax = new Controllers_AjaxController($this->registry);
        $ajax->index($req);
        echo $ajax->getJson();
    }
}