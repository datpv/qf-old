<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_cwaController extends Applications_BaseController {
    public function adminOnly($redirect) {
        $this->helpers->adminNext($redirect);
    }
    function checkPermission() {
        $this->adminOnly($this->helpers->getUrl() . 'auth/login/&ref='.$this->helpers->curPageURL());
    }
    function index() {
        $notfound_obj = new Controllers_notfoundController($this->registry);
        $comments_obj = new Models_CommentsModel($this->registry);
        $forms_obj = new Models_FormsModel($this->registry);
        $logs_obj = new Models_FormsLogModel($this->registry);
        $users_obj = new Models_UsersModel($this->registry);
		$routers = $this->registry->router->getRouters();
        $formId = $routers['id'];
        $formId = $this->helpers->escape_string($formId);
        if(empty($formId)) {
            $notfound_obj->index();
            return;
        }
        $result = $forms_obj->getForm($formId,'','form_uuid','');
        if(!$result) {
            $notfound_obj->index();
            exit();
        }
        if(!$this->helpers->isAdmin() && $result['form_status'] != 'A' && $result['form_approved'] != 'Y') {
            $notfound_obj->index();
            exit();
        }
        
        /*
        * Fields
        */
        $form_id = $result['form_id'];
        $_form = $this->helpers->json_decode_to_array($result['form_content']);
        $fields = $_form['Fields'];
        /*
        * GET FIELDS WITH LOG HITs AND COMMENTS
        */
        $logs2 = $logs3 = $logs4 = $checkBoxFields =array();
        foreach($fields as $field) {
            $field_id = $field['ColumnId'];
            $type = $field['Typeof'];
            if($type == 'text' || $type == 'select' || $type == 'radio' || $type == 'checkbox') {
                $field_id = $field['ColumnId'];
                $logs = $logs_obj->getLogs('*',"`form_id` = '$form_id' AND `field_id` = '$field_id'");
                if($logs) {
                    foreach($logs as $k => $log) {
                        $log_id = $log['log_id'];
                        $comments = $comments_obj->getCommentsOfLog($log_id, '\'0\'', 'comment_create', 'ASC', '3');
                        $comments_count = $comments_obj->getCalcNumRows();
                        if($comments) foreach($comments as $k2 => $comment) {
                            $comment_user = $comment['comment_user'];
                            $comment['user'] = $users_obj->getUser("`user_id` = '$comment_user'");
                            $comments[$k2] = $comment;
                        }
                        $log['comments'] = $comments;
                        $log['total_comments'] = $comments_count;
                        $log['type'] = $type;
                        $log['field'] = $field;
                        $logs[$k] = $log;
                    }
                    $logs2[] = $logs;
                }
            }
        }
        /*
        * GROUP LOGS WITH SAME FIELD ID
        */
        foreach($logs2 as $logs) {
            if($logs) foreach($logs as $k => $log) {
                if(!isset($logs4[$log['field_id']])) {
                    $log_content = $this->helpers->json_decode_to_array($log['log_content']);
                    $logs4[$log['field_id']] = $log_content['Title'];
                }
                $logs3[$log['field_id']][] = $log;
            }
        }
        
        $this->view->title = 'Common Wrong Answers';
        $this->view->form = $result;
        $this->view->fields = $fields;
        $this->view->_form = $_form;
        $this->view->logs3 = $logs3;
        $this->view->logs4 = $logs4;
        $this->view->render('cwa');
    }
    function postReq() {
        $req = $this->registry->req;
        $ajax = new Controllers_AjaxController($this->registry);
        $ajax->index($req);
        echo $ajax->getJson();
    }
}