<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_statisticController extends Applications_BaseController {
    public function adminOnly($redirect) {
        $this->helpers->adminNext($redirect);
    }
    function checkPermission() {
        $this->adminOnly($this->helpers->getUrl() . 'auth/login/&ref='.$this->helpers->curPageURL());
    }
    function index() {
        $forms_obj = new Models_FormsModel($this->registry);
        $cached_obj = new Models_CachedModel($this->registry);
        $routers = $this->registry->router->getRouters();
		$limit = 5;
        $form_results = $forms_obj->getForms('*',"`form_type` = 'NB' OR `form_type` = 'AS' AND `form_approved` = 'Y'");
        $best_users_result = $cached_obj->getCachedEntries('*','users','','cache_stats_hs','DESC',$limit);
        $best_users_result_private = $cached_obj->getCachedEntries('*','users','','cache_stats_sys','DESC',$limit);
        
        foreach($form_results as $k => $form) {
            $form_id = $form['form_id'];
            $form_content = $this->helpers->json_decode_to_array_not_in_mysql($form['form_content']);
            if($form_content['Formtype'] == 'nopbai') {
                $form_result = $cached_obj->getCachedEntries('*','nb',"`cache_form_nb_id` = '$form_id'",'cache_entry_hs','DESC');
                $form['form_nb_result'] = $form_result;
                $form['form_count'] = count($form_result);
            } elseif($form_content['Formtype'] == 'autoscore') {
                $form_result = $cached_obj->getCachedEntries('*','as',"`cache_form_nb_id` = '$form_id'",'cache_entry_sys','DESC');
                $form['form_as_result'] = ($form_result)?$form_result:array();
                $form['form_count'] = count($form_result);
            }
            $form_results[$k] = $form;        
        }
        $this->view->form_results = $form_results;
        $this->view->best_users_result = $best_users_result;
        $this->view->best_users_result_private = $best_users_result_private;
        $this->view->limit = $limit;
        $this->view->render('statistic');
    }
}