<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_dashboardController extends Applications_BaseController {
    public static $form_limit = 10;
    function userOnly($redirect) {
        $this->helpers->userNext($redirect);
    }
    function checkPermission() {
        $this->userOnly($this->helpers->getUrl() . 'auth/login&ref='.$this->helpers->curPageURL());
    }
    function index() {
        $siteUrl = $this->helpers->getUrl();
        /*
        *
        */
        $cached_obj = new Models_CachedModel($this->registry);
        $notfound_obj = new Controllers_notfoundController($this->registry);
        $users_obj = new Models_UsersModel($this->registry);
        /*
        *
        */
		$form_limited = self::$form_limit;
        $cache_hour = 8;
        $use_cache = true;
        $cookies = $this->helpers->getCookies();
        extract($cookies);
        $user_id = $this->helpers->getUserByParam();
        $next = true;
        $users3 = $users_obj->getUser("`user_id` = '$user_id'");
        if(!$users3) {
            $notfound_obj->index();
            exit();
        }
        $user = $cached_obj->getCachedUser("`cache_user_id` = '$user_id'");
        if(!$user) {
            $cached_c_obj = new Controllers_cachesController($this->registry);
            $ajax_obj = new Controllers_AjaxController($this->registry);
            /*
            * BUILD CACHE
            */
            $next = false;
            $ajax_obj->rebuildCacheFor($user_id, $cached_c_obj);
            $next = $cached_c_obj->getAllowNext();
            /*
            * #BUILD CACHE
            */
            if($next) header('location: '.$this->helpers->curPageURL());
            else {
                $notfound_obj->index();
            }
            exit();
        }
        if($user['cache_time'] + ($cache_hour*60*60) > time()) {
            $next = true;
        } else {
            $cached_c_obj = new Controllers_cachesController($this->registry);
            $ajax_obj = new Controllers_AjaxController($this->registry);
            /*
            * BUILD CACHE
            */
            $next = false;
            $ajax_obj->rebuildCacheFor($user_id, $cached_c_obj);
            $next = $cached_c_obj->getAllowNext();
            if($next) header('location: '.$this->helpers->curPageURL());
            else {
                $notfound_obj->index();
            }
            exit();
            /*
            * #BUILD CACHE
            */      
        }
        if(!$next) {
            $notfound_obj->index();
            exit();
        }
        /*
        * GROUPS
        */
        $groups_obj = new Models_GroupsModel($this->registry);
        $groups_meta_result = $groups_obj->getGroupedUsers($user_id);
        if($groups_meta_result)
        foreach($groups_meta_result as $k => $group_meta) {
            $group_id = $group_meta['group_id'];
            $group_meta['users_grouped'] = $cached_obj->getCachedUserGrouped($group_id);
            $groups_meta_result[$k] = $group_meta;
        }
        $this->view->groups_meta_result = ($groups_meta_result)?$groups_meta_result:array();
        /*
        * #GROUPS
        */
        /*
        * CACHED FORM NB
        */
        $entries_result_nb = $cached_obj->getCachedEntries('*','nb',"`cache_user_id` = '$user_id'",$sort_by_nb,$sort_dir_nb);
        $this->view->entries_result_nb = ($entries_result_nb)?$entries_result_nb:array();
        /*
        * CACHED FORM AS
        */
        $entries_result_as = $cached_obj->getCachedEntries('*','as',"`cache_user_id` = '$user_id'",$sort_by_as,$sort_dir_as);
        $this->view->entries_result_as = ($entries_result_as)?$entries_result_as:array();
        /*
        * CACHED FORM SCED
        */
        $entries_result_sced = $cached_obj->getCachedEntries('SQL_CALC_FOUND_ROWS *','sced',"`cache_user_id` = '$user_id'",$sort_by_sced,$sort_dir_sced,"0,$form_limited");
        $this->view->entries_result_sced = ($entries_result_sced)?$entries_result_sced:array();
        $this->view->entries_result_sced_rows = $cached_obj->getCalcNumRows();
        /*
        * CACHED FORM NBED
        */
        $entries_result_nbed = $cached_obj->getCachedEntries('SQL_CALC_FOUND_ROWS *','nbed',"`cache_user_id` = '$user_id'",$sort_by_nbed,$sort_dir_nbed,"0,$form_limited");
        $this->view->entries_result_nbed = ($entries_result_nbed)?$entries_result_nbed:array();
        $this->view->entries_result_nbed_rows = $cached_obj->getCalcNumRows();
        $this->view->entri_sc_ids = array();
        
        $this->view->groups_result = $groups_obj->getGroups();
        $this->view->form_limited = $form_limited;
        $this->view->user = $user;
        $this->view->user_id = $user_id;
        $this->view->title = $user['cache_user_username'] . ' - Hoctudau Quiz Form';
        $this->view->render('dashboard');
    }
    function postReq() {
        $req = $this->registry->req;
        $ajax = new Controllers_AjaxController($this->registry);
        $ajax->index($req);
        if(!$this->getAllowNext()) echo $ajax->getJson();
    }
}