<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_accountsController extends Applications_BaseController {
    public function adminOnly($redirect) {
        $this->helpers->adminNext($redirect);
    }
    function checkPermission() {
        $this->adminOnly($this->helpers->getUrl() . 'auth/login/&ref='.$this->helpers->curPageURL());
    }
    function index() {
        $page_limit = 4;
        $paged = $this->registry->router->paged;
        $_paged = ($paged - 1)*$page_limit;
        $users_obj = new Models_UsersModel($this->registry);
        $groups_obj = new Models_GroupsModel($this->registry);
        if(!$users = $users_obj->getUsers('SQL_CALC_FOUND_ROWS *','','user_id','DESC',"$_paged,$page_limit")) {
            $users = $users_obj->getUsers('SQL_CALC_FOUND_ROWS *','','user_id','DESC',"0,$page_limit");
            $paged = 1;
        }
        $this->view->user_count_total = $user_count_total = $users_obj->getCalcNumRows();
        $this->view->user_count = $user_count = count($users);
        $this->view->total_paged = ceil($user_count_total/$page_limit);
        foreach($users as $ku => $user) {
            $user_id = $user['user_id'];
            $su_results = $users_obj->fetchAll("SELECT * FROM {$this->db_prefix}subject_user a, {$this->db_prefix}subjects b WHERE a.`subject_id` = b.`subject_id` AND `user_id` = '$user_id'");
            if($su_results) {
                $user['su_results'] = $su_results;
                $users[$ku] = $user;
            }
        }
        $this->view->users = $users;
        $this->view->paged = $paged;
        $this->view->groups_result = $groups_obj->getGroups();
        $this->view->user_grouped = $groups_obj->fetchAll("SELECT * FROM {$this->db_prefix}groups_meta a, {$this->db_prefix}groups b WHERE a.`group_id` = b.`group_id`");
        $this->view->title = 'Accounts Manager';
        $this->view->render('accounts');
    }
    function postReq() {
        $req = $this->registry->req;
        $ajax = new Controllers_AjaxController($this->registry);
        $ajax->index($req);
        echo $ajax->getJson();
    }
}