<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_indexController extends Applications_BaseController {
    function index() {
        $x = 'X';
        $y = 'Y';
        $forms = array();
        $this->view->title = 'Hoctudau Quiz Form Homepage';
        if($this->helpers->isAdmin() || $this->helpers->isUser()) {
            $user_id = $this->helpers->getCurrentUser();
            $forms_obj = new Models_FormsModel($this->registry);
            $forms = $forms_obj->getForms('*',"`form_user` = '$user_id'");
            
            if($this->helpers->isAdmin()) {
                $x = $y = 'Unlimit';
            } else {
                $x = 10 - count($forms);
                $y = 50;
            }
        }
        $this->view->forms = $forms;
        $this->view->x = $x;
        $this->view->y = $y;
		$this->view->render('index');
    }
}