<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_notfoundController extends Applications_BaseController {
    function index() {
        $this->registry->router->setRouters(array('controller' => 'notfound'));
		$this->view->render('notfound');
    }
}