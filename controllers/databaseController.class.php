<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_databaseController extends Applications_BaseController {
    public function adminOnly($redirect) {
        $this->helpers->adminNext($redirect);
    }
    function checkPermission() {
        $this->adminOnly($this->helpers->getUrl() . 'auth/login/&ref='.$this->helpers->curPageURL());
    }
    function index() {
        $forms_obj = new Models_FormsModel($this->registry);
        $themes_obj = new Models_ThemesModel($this->registry);
        $entries_obj = new Models_EntriesModel($this->registry);
        $req = isset($_GET['a'])?$_GET['a']:'';
        if($req == 'alter') {
            $entries_obj->doQuery("CREATE TABLE IF NOT EXISTS `{$this->db_prefix}subjects` (
              `subject_id` int(11) NOT NULL AUTO_INCREMENT,
              `subject_uuid` varchar(32) NOT NULL,
              `subject_name` varchar(255) NOT NULL,
              `subject_content` text NOT NULL,
              `subject_settings` text NOT NULL,
              `subject_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `subject_status` varchar(1) NOT NULL DEFAULT 'A',
              PRIMARY KEY (`subject_id`)
            ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2");
            $entries_obj->doQuery("CREATE TABLE IF NOT EXISTS `{$this->db_prefix}subjects_meta` (
              `subject_meta_id` int(11) NOT NULL AUTO_INCREMENT,
              `subject_id` int(11) NOT NULL,
              `meta_key` varchar(255) NOT NULL,
              `meta_value` text NOT NULL,
              `key_name` varchar(255) NOT NULL,
              `key_type` varchar(2) NOT NULL DEFAULT 'TA',
              PRIMARY KEY (`subject_meta_id`)
            ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2");
            $entries_obj->doQuery("CREATE TABLE IF NOT EXISTS `{$this->db_prefix}subject_form` (
              `sf_id` int(11) NOT NULL AUTO_INCREMENT,
              `subject_id` int(11) NOT NULL,
              `form_id` int(11) NOT NULL,
              PRIMARY KEY (`sf_id`)
            ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2");
            $entries_obj->doQuery("CREATE TABLE IF NOT EXISTS `{$this->db_prefix}subject_user` (
              `su_id` int(11) NOT NULL AUTO_INCREMENT,
              `subject_id` int(11) NOT NULL,
              `user_id` int(11) NOT NULL,
              `su_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `subject_unlocked` varchar(1) NOT NULL DEFAULT 'N',
              PRIMARY KEY (`su_id`)
            ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2");
            
            echo $entries_obj->getErrors();
        }
        if($req == 'db_manager') {
            header('location: db_manager_/adminer.php');
            return;
        }
        $this->view->title = 'Database Manager';
        $this->view->render('database');
    }
    function postReq() {
        $req = $this->registry->req;
        $ajax = new Controllers_AjaxController($this->registry);
        $ajax->index($req);
        echo $ajax->getJson();
    }
}