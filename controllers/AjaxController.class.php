<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_AjaxController extends Applications_BaseController {
    private $json = array(
        'sl_translate' => 'markup,lbmarkup,html,message',
        'success' => false,
        'response' => array()
    );
    private $args = array(), $json_ = array();
    
    function index($req = array(), $obj = '') {
        // get router
        $routers = $this->registry->router->getRouters();
        // get action
		$action = isset($req['action'])?$req['action']:'';
        $this->args['action'] = $action;
        /*
        * From Admin controller
        */
        
        // get form uuid
        $formId = isset($req['formId'])?$req['formId']:'';
        $formId = $this->helpers->escapeString($formId);
        $this->args['formId'] = $formId;
        // get form name
        $formName = isset($req['formName'])?$req['formName']:'';
        $this->args['formName'] = $formName;
        // get sql order by
        $orderBy = isset($req['orderBy'])?$req['orderBy']:'';
        $this->args['orderBy'] = $orderBy;
        // get sql order direction
        $orderDirection = isset($req['orderDirection'])?$req['orderDirection']:'DESC';
        $this->args['orderDirection'] = $orderDirection;
        // get public status
        $publicStatus = isset($req['publicStatus'])?$req['publicStatus']:'0';
        $this->args['publicStatus'] = $publicStatus;
        // get password
        $password = isset($req['password'])?$req['password']:'0';
        $this->args['password'] = $password;
        
        $approveType = isset($req['approveType'])?$req['approveType']:'';
        $this->args['approveType'] = $approveType;
        
        $category_id = isset($req['category_id'])?$req['category_id']:'';
        $this->args['category_id'] = $category_id;
        
        $s_categories = isset($req['s_categories'])?$req['s_categories']:'';
        $this->args['s_categories'] = $s_categories;
        
        $category_name = isset($req['category_name'])?$req['category_name']:'';
        $this->args['category_name'] = $category_name;
        /*
        * From Build controller
        */
        $type = isset($req['type'])?$req['type']:'';
        $this->args['type'] = $type;
        $field = isset($req['field'])?$req['field']:'';
        $this->args['field'] = $field;
        $fieldId = isset($req['fieldId'])?$req['fieldId']:'';
        $this->args['fieldId'] = $fieldId;
        $fieldFormId = $routers['id'];
        $this->args['fieldFormId'] = $fieldFormId;
        $form = isset($req['form'])?$req['form']:'';
        $this->args['form'] = $form;
        
        /*
        * Report Build controller
        */
        $newWidgetInfo = isset($req['newWidgetInfo'])?$req['newWidgetInfo']:'';
        $this->args['newWidgetInfo'] = $newWidgetInfo;
        $widget = isset($req['widget'])?$req['widget']:'';
        $this->args['widget'] = $widget;
        $report = isset($req['report'])?$req['report']:'';
        $this->args['report'] = $report;
        $fields = isset($req['fields'])?$req['fields']:'';
        $this->args['fields'] = $fields;
        $field_name = isset($req['field_name'])?$req['field_name']:'';
        $this->args['field_name'] = $field_name;
        $field_default = isset($req['field_default'])?$req['field_default']:'';
        $this->args['field_default'] = $field_default;
        /*
        * Reports Manager Contollers
        */
        $reportId = isset($req['reportId'])?$req['reportId']:'';
        $this->args['reportId'] = $reportId;
        $reportName = isset($req['formName'])?$req['formName']:'';
        $this->args['reportName'] = $reportName;
        /*
        * Caches Manager
        */
        $user_id = isset($req['user_id'])?$req['user_id']:'';
        $this->args['user_id'] = $user_id;
        /*
        * DASHBOARD
        */
        $sort_by = isset($req['sortby'])?$req['sortby']:'';
        $this->args['sort_by'] = $sort_by;
        $sort_dir = isset($req['direction'])?$req['direction']:'ASC';
        $this->args['sort_dir'] = $sort_dir;
        $section = isset($req['section'])?$req['section']:'';
        $this->args['section'] = $section;
        $paged = isset($req['paged'])?$req['paged']:'';
        $this->args['paged'] = $paged;
        /*
        *
        */
        $anyAll= isset($req['anyAll'])?$req['anyAll']:'';
        $this->args['anyAll'] = $anyAll;
        $conditions = isset($req['conditions'])?$req['conditions']:'';
        $this->args['conditions'] = $conditions;
        $direction = isset($req['direction'])?$req['direction']:'DESC';
        $this->args['direction'] = $direction;
        $order = isset($_GET['order'])?$_GET['order']:'entry_create';
        $this->args['order'] = $order;
        $page =  isset($_POST['page'])?$_POST['page']:'0';
        $this->args['page'] = $page;
        $entryId = isset($req['entryId'])?$req['entryId']:'';
        $this->args['entryId'] = $entryId;
        $entry = isset($req['entry'])?$req['entry']:'';
        $this->args['entry'] = $entry;
        /*
        * Groups controller
        */
        $group_id = isset($req['group_id'])?$req['group_id']:'';
        $group_name = isset($req['group_name'])?$req['group_name']:'';
        $group_name = $this->helpers->escape_string(trim($group_name));
        $group_id = $this->helpers->escape_string(trim($group_id));
        $this->args['group_id'] = $group_id;
        $this->args['group_name'] = $group_name;
        /*
        * Accounts controller
        */
        $old_group_id = isset($req['old_group'])?$req['old_group']:'';
        $this->args['old_group_id'] = $old_group_id;
        
        /*
        * Comments
        */
        $log_id = isset($req['log_id'])?$req['log_id']:'';
        $this->args['log_id'] = $log_id;
        $commentText = isset($req['commentText'])?$req['commentText']:'';
        $this->args['commentText'] = $commentText;
        $comment_id = isset($req['comment_id'])?$req['comment_id']:array();
        $this->args['comment_id'] = $comment_id;
        $comment_ids = isset($req['comment_ids'])?$req['comment_ids']:array();
        $this->args['comment_ids'] = $comment_ids;
        $fieldValue = isset($req['fieldValue'])?$req['fieldValue']:array();
        $this->args['fieldValue'] = $fieldValue;
        $fieldTitle = isset($req['fieldTitle'])?$req['fieldTitle']:array();
        $this->args['fieldTitle'] = $fieldTitle;
        /*
        *
        */
        $passKey = isset($req['passKey'])?$req['passKey']:'';
        $this->args['passKey'] = $passKey;
        
        // call method
        $action = $routers['controller'].'_'.$action;
        if(is_callable(array($this, $action))) {
            $this->$action($this->args);
        } else {
            $noaction = $routers['controller'].'_index';
            if(is_callable(array($this, $noaction))) {
                $this->$noaction(array_merge($this->args, $req), $obj);
            }
        }
    }
    function entries_index($posts = array(), $c_entry) {
        $req = $this->registry->req;
        $posted = $req;
        if(!isset($posted['saveForm'])) return;
        $posted = $this->helpers->unescape_string($posted);
        $routers = $this->registry->router->getRouters();
        $formId = $routers['id2'];
        $entries_obj = new Models_EntriesModel($this->registry);
        $forms_obj = new Models_FormsModel($this->registry);
        $formId = $this->helpers->escape_string($formId);
        $s2 = false;
        $result = $forms_obj->getForm($formId,'');
        if($result && !empty($posted)) {
            $ip = isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:"";
            $ip = $this->helpers->escape_string($ip);
            $posts = $this->helpers->json_encode_then_escape($posted);
            $_form = $this->helpers->json_decode_to_array($result['form_content']);
            $_fields = $_form['Fields'];
            $formType = $_form['Formtype'];
            $normal_login = false;
            if($formType == 'normal') {
                if($_form['UserLogin']) {
                    $normal_login = true;
                }
            }
            $up1 = $alt3 = $alt4 = '';
            $user_id = '1';
            if($formType == 'nopbai') {
                $entry_uuid = $this->helpers->uuidSecure();
                $alt3 = ",`entry_uuid`,`entry_type`,`entry_user`";
                $alt4 = ",'$entry_uuid','NB','$user_id'";
            } elseif($formType == 'score') {
                $up1 = ",`entry_scoring` = '$scores'";
            } elseif($formType == 'autoscore') {
                $scores2 = $this->helpers->scoring($posts, $_fields);
                $entry_uuid = $this->helpers->uuidSecure();
                $scores = $this->helpers->json_encode_then_escape($scores2);
                $alt3 = ",`entry_uuid`,`entry_type`,`entry_user`,`entry_of`,`entry_scoring`";
                $alt4 = ",'$entry_uuid','AS','$user_id','$entry_uuid','$scores'";
                $up1 = ",`entry_scoring` = '$scores'";
            } elseif($normal_login) {
                $alt3 = ",`entry_user`";
                $alt4 = ",'$user_id'";
            }
            if($posted['saveForm'] == 'Save Changes') {
                $entry_id = isset($_GET['entryId'])?$_GET['entryId']:'';
                $entry_id = $this->helpers->escape_string($entry_id);
                $entd = $this->helpers->get_datetime();
                if($entries_obj->updateEntry("`entry_update` = '$entd',`entry_content` = '$posts' $up1", "`entry_id` = '$entry_id'")) {
                    $s2 = true;
                }
            } elseif($posted['saveForm'] == 'Submit') {
                if($entries_obj->insertEntry("`entry_ip`,`entry_content` $alt3","'$ip','$posts' $alt4")) {
                    $entry_id = $entries_obj->registry->mysqli->insert_id;
                    $form_id = $result['form_id'];
                    $entries_obj->insertEntryMeta('`form_id`,`entry_id`',"'$form_id','$entry_id'");
                    $s2 = true;
                }
            }
        }
        $this->view->s2 = $s2;
        $this->view->render('entries/submit','only');
        exit();
    }
    function reports_index($posts = array(), $c_report) {
        extract($posts);
        $posts = $this->helpers->unescape_string($posts);
        $reports_obj = new Models_ReportsModel($this->registry);
        $routers = $this->registry->router->getRouters();
        $reportId = $routers['id'];
        if($result = $reports_obj->getReport($reportId,'')) {
            $report_pass = $result['report_password'];
            if(!empty($report_pass)) {
                if(!isset($_SESSION['report_'.$reportId])) {
                    $post_pass = isset($posts['passKey'])?$posts['passKey']:'';
                    $md5_post_pass = md5($this->helpers->escape_string($post_pass));
                    if(empty($post_pass) || ($md5_post_pass != $report_pass )) {
                
                    } else {
                        $_SESSION['report_'.$reportId] = $reportId;
                    }
                    $c_report->view->pass_err = 'Wrong password!';
                }
            }
        }
        $c_report->setAllowNext(true);
    }
    function forms_index($posts2 = array(), $c_form) {
    }
    /* ---------------------------------------------------------------------------- */
    /* ------------------------ FORMS ----------------------------- */
    /* ---------------------------------------------------------------------------- */
    function forms_save($args) {
    }
    function forms_saveindex($args) {
    }
    /* ---------------------------------------------------------------------------- */
    /* ------------------------ ADMIN ----------------------------- */
    /* ---------------------------------------------------------------------------- */
    function admin_categoryRename($args) {
        extract($args);
        $categories_obj = new Models_CategoriesModel($this->registry);
        
        $category_id = $this->helpers->escape_string($category_id);
        $category_name = $this->helpers->escape_string($category_name);
        if(strlen($category_name) > 1 && !empty($category_id)) {
            $categories_obj->updateCategory("`category_name` = '$category_name'", "`category_id` = '$category_id'");
            if(!$categories_obj->getErrors()) {
                $this->json['success'] = true;
            }
        }
    }
    function admin_categoryDelete($args) {
        extract($args);
        $categories_obj = new Models_CategoriesModel($this->registry);
        
        $category_id = $this->helpers->escape_string($category_id);
        if(!empty($category_id)) {
            $categories_obj->deleteCategory("`category_id` = '$category_id'");
            $categories_obj->deleteCategoriesMeta("`category_id` = '$category_id'");
            if(!$categories_obj->getErrors()) {
                $this->json['success'] = true;
            }
        }
    }
    function admin_categoryAdd($args) {
        extract($args);
        $categories_obj = new Models_CategoriesModel($this->registry);
        $category_name = $this->helpers->escape_string($category_name);
        if(strlen($category_name) > 1) {
            $categories_obj->insertCategory("`category_name`","'$category_name'");
            if(!$categories_obj->getErrors()) {
                $this->json['success'] = true;
            }
        }
    }
    function admin_addCategory($args) {
        extract($args);
        $categories_obj = new Models_CategoriesModel($this->registry);
        $categories = $categories_obj->getCategoriesMeta('*',"`form_id` = '$formId'");
        $categories = ($categories)?$categories:array();
        // get new categoriy ids
        $new_ids = explode(',',$s_categories);
        $new_ids = array_map('trim',$new_ids);
		// get old categoriy ids
        $old_ids = array();
        
        foreach($categories as $category) {
            $old_ids[] = $category['category_id'];
        }
		// categoriy ids to add
        $add_ids = array_diff($new_ids,$old_ids);
		// tags to delete
        $del_ids = array_diff($old_ids,$new_ids);
        $delete_ids = array();
        foreach($categories as $k => $category) {
            foreach($del_ids as $del_id) {
                if($del_id == $category['category_id']) {
                    $delete_ids[] = '\''.$category['category_id'].'\'';
                }
            }
        }
        $delete_ids = implode(',',$delete_ids);
        if(empty($delete_ids)) $delete_ids = '\'0\'';
        
        $categories_obj->deleteCategoriesMeta("`category_id` IN ($delete_ids) AND `form_id` = '$formId'");
        foreach($add_ids as $add_id) {
            $add_id = $this->helpers->escape_string($add_id);
            if($categories_obj->getCategory($add_id,'','category_id')) {
                $categories_obj->insertCategoryMeta("`form_id`, `category_id`","'$formId','$add_id'");
            }
        }
        if(!$categories_obj->getErrors()) {
            $this->json['success'] = true;   
        } else {
            $this->json['message'] = $categories_obj->getErrors();
        }
    }
    function admin_getForms($args) {
        extract($args);
        $siteUrl = $this->helpers->getUrl();
        $forms_obj = new Models_FormsModel($this->registry);
        $entries_obj = new Models_EntriesModel($this->registry);
        $users_obj = new Models_UsersModel($this->registry);
        $html = $html2 = '';
        $form_limit = Controllers_adminController::$form_limit;
        if($approveType == 'y') $approveType = 'Y';
        elseif($approveType == 'n') $approveType = 'N';
        else $approveType = 'Y';
        
        if($direction == 'desc') $direction = 'DESC';
        elseif($direction == 'asc') $direction = 'ASC';
        else $direction = 'DESC';
        
        if($sort_by == 'FormId') $sort_by = 'form_create';
        elseif($sort_by == 'DateUpdated') $sort_by = 'form_update';
        else $sort_by = 'form_create';
        $form_limit = Controllers_adminController::$form_limit;
        if(empty($paged) || !is_numeric($paged) || $paged <= 0) $paged = 1;
        $_paged = ($paged - 1) * $form_limit;
        if(!empty($category_id)) {
            $forms_result = $forms_obj->getFormsInCategory($category_id,$approveType,$sort_by,$direction,"$_paged,$form_limit");
        } else {
            $forms_result = $forms_obj->getForms('SQL_CALC_FOUND_ROWS *',"`form_approved` = '$approveType'", $sort_by, $direction,"$_paged,$form_limit");
        }
        $total_rows = $forms_obj->getCalcNumRows();
        $json = array();
        $html_paged = '';
        if($forms_result) {
            $i = 0;
            foreach($forms_result as $form) {
                ob_start();
                $form_id = $form['form_id'];
                $entty_count = 0;
                $entty_count = $entries_obj->getEntriesNumber($form_id);
                $entty_count = $entty_count - $form['form_flag'];
                $user_by = $users_obj->getUser("`user_id` = '$form[form_user]'");
                $user_c = $this->helpers->json_decode_to_array_not_in_mysql($user_by['user_content']);
                if(empty($form['form_content'])) continue;
                $_form = $this->helpers->json_decode_to_array($form['form_content']);
                $json[$form_id] = array(
                    'FormId' => $form_id,
                    'Url' => $form['form_uuid'],
                    'Name' => $_form['Name']
                );
            ?>
            <li id="li<?php echo $form['form_id']; ?>" class="<?php echo $form['form_type'] . ' '; echo ($i==0)?'first':''; $i++; if($approveType == 'Y') echo ' approvedForm'; else echo ' unApprovedForm'; ?> " onmouseover="showActions(this)" onmouseout="hideActions(this)">
            <h4>
                <span class="by_user"><a title="<?php echo !empty($user_c['NickH2d'])?$user_c['NickH2d']:$user_c['Name']; ?>" href="<?php echo $siteUrl; ?>dashboard/&user_id=<?php echo $user_by['user_id']; ?>" target="_blank"><?php echo $this->helpers->truncate(!empty($user_c['NickH2d'])?$user_c['NickH2d']:$user_c['Name'],6,' ',3); ?></a></span>
            	<a title="<?php echo $_form['Name']; ?>" id="link<?php echo $form['form_id']; ?>" href="<?php echo $siteUrl; ?>entries/<?php echo $form['form_uuid']; ?>">
            	<span class="notranslate"> <?php echo $this->helpers->truncate($_form['Name'],50); ?> </span>
                <?php if($entty_count>0): ?>
                <b title="Hooray!"><span class="notranslate"><?php echo $entty_count; ?></span> New Entries.</b>
                <?php endif; ?>
                
            	</a>
            </h4>
            <span title="Add Categories" onclick="addFormCategories(<?php echo $form['form_id']; ?>); return false;" class="category_select">
            &nbsp;
            </span>
            <span class="themeSelect">
            	<select onchange="changeTheme(<?php echo $form['form_id']; ?>, 'Untitled%20Form', this)">
            		<option value="1" selected="selected">
            			Default Theme
            		</option>
            		<option value="create">
            			-----------
            		</option>
            		<option value="create">
            			New Theme!
            		</option>
            	</select>
            </span>
            <span class="activeCheck">
            	<label for="publicStatus_<?php echo $form['form_id']; ?>">
            		Public
            	</label>
            	<input type="checkbox" id="publicStatus_<?php echo $form['form_id']; ?>" onclick="togglePublicForm(<?php echo $form['form_id']; ?>, 'Untitled%20Form', this)" checked="checked"/>
            </span>
            <div id="expandThis">
    			<div class="actions">
                    <a class="unapprove" href="#" onclick="unApproveForm(this); return false;">UnApprove</a>
                    <a class="approve" href="#" onclick="approveForm(this); return false;">Approve</a>
    				<a class="del" href="#" onclick="deleteForm(this); return false;">Delete</a>
    				<a class="dup" href="#" onclick="duplicateForm(this); return false;">Duplicate</a>
    				<a class="entries" href="#" onclick="viewEntries(this); return false;">Entries</a>
    				<a class="edit" href="#" onclick="editForm(this); return false;">Edit</a>
    				<a class="view" href="<?php echo $siteUrl; ?>forms/<?php echo $form['form_uuid'] ?>" target="_blank">View</a>
                    <a class="view" href="<?php echo $siteUrl; ?>cwa/<?php echo $form['form_uuid'] ?>" target="_blank">CWA</a>
                    <a class="subscribe" href="#" onclick="viewNotifications(this); return false;">Notifications</a>
    				<a class="rules" href="#" onclick="viewRules(this); return false;">Rules</a>
                    <span>
                    <a class="protect" href="#" onclick="showProtectForm(this);return false;">Protect</a>
    				</span>
                    <span>
                    <a target="_blank" class="protect" href="<?php echo $siteUrl; ?>report/auto_builder/<?php echo $form['form_uuid'] ?>">Create Report</a>
    				</span>
    				<span id="paidPlanWarningDiv" class="hide">
    					This feature is only available to our paid plans.
    				</span>
    			</div>
    		</div>
            </li>
            <?php
            $html .= ob_get_contents();
            ob_clean();
            }
            ob_end_clean();
            
            $html_paged = $this->load_pagination($total_rows, $form_limit, $paged);
        } else {
            ob_start();
        ?>
            <li class="notice bigMessage">
        		<h2>
        			<a href="<?php echo $siteUrl; ?>build/"><span class="bigMessageRed">You don't have any forms!</span> <span class="bigMessageGreen">On This Category!</span></a>
        		</h2>
        	</li>
        <?php
            $html .= ob_get_contents();
            ob_end_clean();
        }
        $this->json['success'] = true;
        $this->json['response']['json'] = $json;
        $this->json['response']['paged'] = $html_paged;
        $this->json['response']['html'] = $html;
    }
    function admin_approveform($args) {
        $form_id = $args['formId'];
        $forms_obj = new Models_FormsModel($this->registry);
        $forms_obj->updateForm("`form_approved` = 'Y', `form_status` = 'A'", "`form_id` = '$form_id'");
        if(!$forms_obj->getErrors()) {
            $this->json['success'] = true;
        }
    }
    function admin_unapproveform($args) {
        $form_id = $args['formId'];
        $forms_obj = new Models_FormsModel($this->registry);
        $forms_obj->updateForm("`form_approved` = 'N', `form_status` = 'D'", "`form_id` = '$form_id'");
        if(!$forms_obj->getErrors()) {
            $this->json['success'] = true;
        }
    }
    function admin_duplicate($args) {
        if(!empty($args['formId'])) {
            $forms_obj = new Models_FormsModel($this->registry);
            if($result = $forms_obj->getForm('',$args['formId'])) {
                $form = ($result['form_content']);
                $_form = $this->helpers->json_decode_to_array($form);
                $_form['Name'] = $_form['Name'] . ' Cloned';
                $form_content = $this->helpers->json_encode_then_escape($_form);
                $newFormId = $this->helpers->uuidSecure();
                if(!$forms_obj->insertForm('`form_uuid`,`form_content`', "'$newFormId','$form_content'")) {
                    echo $forms_obj->getErrors();
                    exit;
                } else {
                    $this->json['success'] = true;
                }
            } else {
                echo $forms_obj->getErrors;
                exit;
            }
        }
    }
    function admin_delete($args) {
        $formId = $args['formId'];
        if(!empty($formId)) {
            // init objs
            $forms_obj = new Models_FormsModel($this->registry);
            $reports_obj = new Models_ReportsModel($this->registry);
            $entries_obj = new Models_EntriesModel($this->registry);
            $cached_obj = new Models_CachedModel($this->registry);
            // delete form
            if($forms_obj->deleteForm('', $formId)) {
                // delete form reports
                $reports_obj->deleteReport('', $formId, '', 'form_id');
                // get related entry ids
                $entries_result = $entries_obj->getEntriesMeta("`form_id` = '$formId'");
                $entry_ids = array();
                if($entries_result) foreach($entries_result as $entry) {
                    $entry_ids[] = '\''.$entry['entry_id'].'\'';
                }
                $entry_ids = implode(',',$entry_ids);
                if(empty($entry_ids)) $entry_ids = '\'0\'';
                // delete form entries
                if($entries_obj->deleteEntries("`entry_id` IN ($entry_ids)")) {
                    $entries_obj->deleteEntryMeta('',$formId,'','form_id');
                    // delete form entries caches
                    $cached_obj->deleteCachedEntries("`cache_entry_id` IN ($entry_ids)");
                    $this->json['success'] = true;
                }
            }
            if(!empty($this->registry->mysqli->error)) {
                echo $this->registry->mysqli->error;
                exit;
            }
            
        }
    }
    function admin_protect($args) {
        $forms_obj = new Models_FormsModel($this->registry);
        $password = md5($this->helpers->escape_string($args['password']));
        $formId = $this->helpers->escape_string($args['formId']);
        $forms_obj->updateProtectPassword($password,$formId);
        if(!$forms_obj->getErrors()) {
            $this->json['success'] = true;
        }
    }
    function admin_order($args) {
        $siteUrl = $this->helpers->getUrl();
        $forms_obj = new Models_FormsModel($this->registry);
        $entries_obj = new Models_EntriesModel($this->registry);
        $orderBy = $args['orderBy'];
        $orderDirection = $args['orderDirection'];
        $html = '';
        if($orderBy == 'FormId') $orderBy = 'form_create';
        elseif($orderBy == 'DateUpdated') $orderBy = 'form_update';
        elseif($orderBy == 'EntryCount') $orderBy = 'form_create';
        else $orderBy = 'form_create';
        $forms_result = $forms_obj->getForms('*',"`form_approved` = 'Y'",$orderBy,$orderDirection);
        if($forms_result) {
        $i = 0;
        if($forms_result) foreach($forms_result as $form) {
            ob_start();
            $form_id = $form['form_id'];
            $entty_count = 0;
            $entty_count = $entries_obj->getEntriesNumber($form_id);
            $entty_count = $entty_count - $form['form_flag'];
            if(empty($form['form_content'])) continue;
            $_form = $this->helpers->json_decode_to_array($form['form_content']);
        ?>
        <li id="li<?php echo $form['form_id']; ?>" class="<?php echo ($i==0)?'first':''; $i++; ?>" onmouseover="showActions(this)" onmouseout="hideActions(this)">
        <h4>
        	<a id="link<?php echo $form['form_id']; ?>" href="<?php echo $siteUrl; ?>entries/<?php echo $form['form_uuid']; ?>">
        	<span class="notranslate"> <?php echo $_form['Name']; ?> </span>
            <?php if($entty_count>0): ?>
            <b title="Hooray!"><span class="notranslate"><?php echo $entty_count; ?></span> New Entries.</b>
            <?php endif; ?>
            
        	</a>
        </h4>
        
        <span class="themeSelect">
        	<select onchange="changeTheme(<?php echo $form['form_id']; ?>, 'Untitled%20Form', this)">
        		<option value="1" selected="selected">
        			Default Theme
        		</option>
        		<option value="create">
        			-----------
        		</option>
        		<option value="create">
        			New Theme!
        		</option>
        	</select>
        </span>
        <span class="activeCheck">
        	<label for="publicStatus_<?php echo $form['form_id']; ?>">
        		Public
        	</label>
        	<input type="checkbox" id="publicStatus_<?php echo $form['form_id']; ?>" onclick="togglePublicForm(<?php echo $form['form_id']; ?>, 'Untitled%20Form', this)" checked="checked"/>
        </span>
        <div id="expandThis">
        	<div class="actions">
        		<a class="del" href="#" onclick="deleteForm(this); return false;">Delete</a>
        		<a class="dup" href="#" onclick="duplicateForm(this); return false;">Duplicate</a>
        		<a class="entries" href="#" onclick="viewEntries(this); return false;">Entries</a>
        		<a class="edit" href="#" onclick="editForm(this); return false;">Edit</a>
        		<a class="view" href="<?php echo $siteUrl; ?>forms/<?php echo $form['form_uuid'] ?>" target="_blank">View</a>
        		<a class="view" href="<?php echo $siteUrl; ?>cwa/<?php echo $form['form_uuid'] ?>" target="_blank">CWA</a>
                <a class="subscribe" href="#" onclick="viewNotifications(this); return false;">Notifications</a>
        		<a class="rules" href="#" onclick="viewRules(this); return false;">Rules</a>
        		<span>
        			<a class="protect" href="#" onclick="alert($('paidPlanWarningDiv').innerHTML); return false;">Protect</a>
        		</span>
        		<span id="paidPlanWarningDiv" class="hide">
        			This feature is only available to our paid plans.
        		</span>
        	</div>
        </div>
        </li>
        <?php
        $html .= ob_get_contents();
        ob_clean();
        }
        }
        $this->json['response']['formList'] = $html;
    }
    function admin_savePublicStatus($args) {
        $formId = $this->helpers->escape_string($args['formId']);
        $publicStatus = $args['publicStatus'];
        if($publicStatus == '0') $status = 'D';
        elseif($publicStatus == '1') $status = 'A';
        $forms_obj = new Models_FormsModel($this->registry);
        if($forms_obj->updatePublicStatus($status,$formId)) {
            $this->json['success'] = true;
        }
    }
    
    /* ---------------------------------------------------------------------------- */
    /* ------------------------ SUBJECT BUILDER ----------------------------- */
    /* ---------------------------------------------------------------------------- */
    function subjects_deleteSubject($args) {
        $posts = isset($_POST)?$_POST:array();
        $subjects_obj = new Models_SubjectsModel($this->registry);
        $subject_id = isset($posts['subject_id'])?$posts['subject_id']:'';
        $subject_id = $this->helpers->escape_string($subject_id);
        if(empty($subject_id)) {
            return false;
        }
        
        $subject_result = $subjects_obj->getSubject("`subject_id` = '$subject_id'");
        if(!$subject_result) {
            return false;
        }
        
        if($subjects_obj->deleteSubject("`subject_id` = '$subject_id'")) {
            $subjects_obj->deleteSubjectMeta("`subject_id` = '$subject_id'",'');
            $subjects_obj->deleteSF("`subject_id` = '$subject_id'",'');
            $subjects_obj->deleteSU("`subject_id` = '$subject_id'",'');
            $this->json['success'] = true;
        }
        
    }
    function subjects_getForms($args) {
        $posts = isset($_POST)?$_POST:array();
        $routers = $this->registry->router->getRouters();
        // off magic quotes gpc
        $subjects_obj = new Models_SubjectsModel($this->registry);
        $subject_uuid = $this->helpers->escape_string($routers['id']);
        if(empty($subject_uuid)) {
            return false;
        }
        $subject_result = $subjects_obj->getSubject("`subject_uuid` = '$subject_uuid'");
        if(!$subject_result) {
            return false;
        }
        $forms_obj = new Models_FormsModel($this->registry);
        $form_ids = isset($posts['forms'])?$posts['forms']:'';
        $form_ids = $this->helpers->json_decode_to_array($form_ids);
        $ids_string = implode(',',$form_ids);
        $form_limit = Controllers_subjectsController::$form_limit;
        if($forms_result = $forms_obj->getForms('SQL_CALC_FOUND_ROWS *',"`form_approved` = 'Y' AND `form_id` NOT IN ($ids_string)",'form_create','DESC', $form_limit)) {
            $form_total = $forms_obj->getCalcNumRows();
            $json = array();
            foreach($forms_result as $form) {
                $form_content = $this->helpers->json_decode_to_array($form['form_content']);
                $json[$form['form_id']] = array(
                    'FormId' => $form['form_id'],
                    'Url' => $form['form_uuid'],
                    'Name' => $form_content['Name']
                );
            }
            $this->json['success'] = true;
            $this->json['response']['total'] = $form_total- count($forms_result);
            $this->json['response']['html'] = $this->subjectsGetHTml($forms_result);
            $this->json['response']['json'] = $json;
        }
    }
    function subjectsGetHTml($forms_result) {
        $siteUrl = $this->helpers->getUrl();
        ob_start();
        foreach($forms_result as $form) {
            $form_content = $this->helpers->json_decode_to_array($form['form_content']);
        ?>
        <li id="form_<?php echo $form['form_id']; ?>">
            <a target="_blank" class="form_title" href="<?php echo $siteUrl . 'forms/' .  $form['form_uuid']; ?>"><?php echo $form_content['Name']; ?></a>
            <span class="hold"><input onclick="(this.checked) ? addForm(this) : removeForm(this);" type="checkbox" class="checkbox" /></span>
        </li>
        <?php
        }
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    function subjects_deleteTab($args) {
        $posts = isset($_POST)?$_POST:array();
        $routers = $this->registry->router->getRouters();
        // off magic quotes gpc
        $subjects_obj = new Models_SubjectsModel($this->registry);
        $subject_uuid = $this->helpers->escape_string($routers['id']);
        if(empty($subject_uuid)) {
            return false;
        }
        $subject_result = $subjects_obj->getSubject("`subject_uuid` = '$subject_uuid'");
        if(!$subject_result) {
            return false;
        }
        $tab_key = isset($posts['tab_key'])?$posts['tab_key']:'';
        $tab_key = $this->helpers->escape_string($tab_key);
        $subject_id = $subject_result['subject_id'];
        if($tab_key != 'outline') {
            $tab_key_clean = 'subject_' . $tab_key;
            if($subjects_obj->deleteSubjectMeta("`meta_key` = '$tab_key_clean' AND `subject_id` = '$subject_id'")) {
                $this->json['success'] = true;
                $this->json['response']['tab_key'] = $tab_key;
            }
        }
    }
    function subjects_renameTab($args) {
        $posts = isset($_POST)?$_POST:array();
        $routers = $this->registry->router->getRouters();
        // off magic quotes gpc
        $subjects_obj = new Models_SubjectsModel($this->registry);
        $subject_uuid = $this->helpers->escape_string($routers['id']);
        if(empty($subject_uuid)) {
            return false;
        }
        $subject_result = $subjects_obj->getSubject("`subject_uuid` = '$subject_uuid'");
        if(!$subject_result) {
            return false;
        }
        $tab_key = isset($posts['tab_key'])?$posts['tab_key']:'';
        $tab_name2 = isset($posts['tab_name'])?$posts['tab_name']:'';
        $tab_name2 = trim($tab_name2);
        $tab_name = $this->helpers->unescape_string($tab_name2);
        
        $tab_key = $this->helpers->escape_string($tab_key);
        $tab_name = $this->helpers->escape_string($tab_name);
        
        $subject_id = $subject_result['subject_id'];
        $tab_key_clean = 'subject_' . $tab_key;
        if($subjects_obj->updateSubjectMeta("`key_name` = '$tab_name'","`meta_key` = '$tab_key_clean' AND `subject_id` = '$subject_id'")) {
            $this->json['success'] = true;
            $this->json['response']['tab_key'] = $tab_key;
            $this->json['response']['tab_name'] = $tab_name2;
        }
    }
    function subjects_registerSubject($args) {
        $routers = $this->registry->router->getRouters();
        // off magic quotes gpc
        $subjects_obj = new Models_SubjectsModel($this->registry);
        $users_obj = new Models_UsersModel($this->registry);
        $subject_uuid = $this->helpers->escape_string($routers['id']);
        if(empty($subject_uuid)) {
            return false;
        }
        $subject_result = $subjects_obj->getSubject("`subject_uuid` = '$subject_uuid'");
        if(!$subject_result) {
            return false;
        }
        $subject_id = $subject_result['subject_id'];
        $user_id = $this->helpers->getCurrentUser();
        if($user_id == '1') {
            $this->json['success'] = true;
            return;
        }
        if(!$users_obj->getUser("`user_id` = '$user_id'")) {
            return;
        }
        $subject_settings = $this->helpers->json_decode_to_array($subject_result['subject_settings']);
        if($subject_settings['Price'] == 'Free') {
            $subject_unlocked = 'Y';
        } else {
            $subject_unlocked = 'N';
        }
        $subjects_obj->getSUs('SQL_CALC_FOUND_ROWS *',"`subject_id` = '$subject_id'",'','','1');
        $user_count = $subjects_obj->getCalcNumRows();
        if($subject_settings['LimitUser'] < $user_count) {
            $this->json['response']['message'] = 'User Registration Reach Limited';
            return;
        }
        if(!$subjects_obj->getSU("`user_id` = '$user_id' AND `subject_id` = '$subject_id'")) {
            if($subjects_obj->insertSU('`user_id`,`subject_id`,`subject_unlocked`',"'$user_id','$subject_id','$subject_unlocked'")) {
                $this->json['success'] = true;
                $this->json['response']['status'] = $subject_unlocked;
            } else {
                $this->json['response']['message'] = 'Some Error Occured';
            }
        } else {
            $this->json['response']['message'] = 'Some Error Occured';
        }
    }
    function subjects_saveOutline($args) {
        $posts = isset($_POST)?$_POST:array();
        $routers = $this->registry->router->getRouters();
        // off magic quotes gpc
        
        $subjects_obj = new Models_SubjectsModel($this->registry);
        $forms_obj = new Models_FormsModel($this->registry);
        $subject_uuid = $this->helpers->escape_string($routers['id']);
        if(empty($subject_uuid)) {
            return false;
        }
        $subject_result = $subjects_obj->getSubject("`subject_uuid` = '$subject_uuid'");
        if(!$subject_result) {
            return false;
        }
        $subject_id = $subject_result['subject_id'];
        
        $post_data = isset($posts['data'])?$posts['data']:'';
		
        $post_data = $this->helpers->unescape_string($post_data);
		
        $post_data = $this->helpers->json_decode_to_array($post_data);
        
        if(!empty($post_data)) {
            foreach($post_data as $data) {
                if($forms_obj->getForm('',$data) && !$subjects_obj->getSF("`form_id` = '$data'")) {
                    $subjects_obj->insertSF('`subject_id`,`form_id`',"'$subject_id','$data'");
                }
            }
            $this->json['success'] = true;
        }
    }
    function subjects_deleteOutline($args) {
        $posts = isset($_POST)?$_POST:array();
        $routers = $this->registry->router->getRouters();

        $subjects_obj = new Models_SubjectsModel($this->registry);
        $forms_obj = new Models_FormsModel($this->registry);
        $subject_uuid = $this->helpers->escape_string($routers['id']);
        if(empty($subject_uuid)) {
            return false;
        }
        $subject_result = $subjects_obj->getSubject("`subject_uuid` = '$subject_uuid'");
        if(!$subject_result) {
            return false;
        }
        $subject_id = $subject_result['subject_id'];
        
        $form_id = isset($posts['form_id'])?$posts['form_id']:'';
        
        $form_id = $this->helpers->escape_string($form_id);
        
        if(!empty($form_id)) {
            if($subjects_obj->deleteSF("`subject_id` = '$subject_id' AND `form_id` = '$form_id'")) {
                $this->json['success'] = true;
            }
        }
    }
    function subjects_checkKey($args) {
        $posts = isset($_POST)?$_POST:array();
        
        $routers = $this->registry->router->getRouters();
        // off magic quotes gpc
        
        $subjects_obj = new Models_SubjectsModel($this->registry);
        
        $subject_uuid = $this->helpers->escape_string($routers['id']);
        if(empty($subject_uuid)) {
            return false;
        }
        $subject_result = $subjects_obj->getSubject("`subject_uuid` = '$subject_uuid'");
        if(!$subject_result) {
            return false;
        }
        $subject_id = $subject_result['subject_id'];
        
        $tab_name = isset($posts['key_name'])?$posts['key_name']:'';
        $tab_name = trim($tab_name);
        $tab_name = $this->helpers->unescape_string($tab_name);
        
        $tab_name_clean = strtolower($this->helpers->clean_string($tab_name,'_'));
        
        $meta_key = 'subject_' . $tab_name_clean;
        
        $meta_key_result = $subjects_obj->getSubjectMeta("`subject_id` = '$subject_id' AND `meta_key` = '$meta_key'");
        
        if(!$meta_key_result) {
            $this->json['success'] = true;
            $this->json['response']['key_id'] = $tab_name_clean;
        } 
    }
    function subjects_save($args) {
        $posts = isset($_POST)?$_POST:array();
        // get router
        $routers = $this->registry->router->getRouters();
        // off magic quotes gpc
        //$posts = $this->helpers->unescape_string($posts);
        
        $post_data = isset($posts['data'])?$posts['data']:'';
        
        $settings = isset($posts['settings'])?$posts['settings']:'';
        
        $settings = $this->helpers->json_decode_to_array($settings);
        
        $subject_data = $this->helpers->json_decode_to_array($post_data);
        
        $subjects_obj = new Models_SubjectsModel($this->registry);
        $subject_id = '';
        $create_new = true;
        $subject_uuid = $this->helpers->escape_string($routers['id']);
        if(!empty($subject_uuid)) {
            $create_new = false;
            $subject_result = $subjects_obj->getSubject("`subject_uuid` = '$subject_uuid'");
            if(!$subject_result) {
                return false;
            }
            $subject_id = $subject_result['subject_id'];
        }
        $settings = $this->helpers->json_encode_then_escape($settings);
        foreach($subject_data as $subject_k => $subject_v) {
            if($this->helpers->startsWith($subject_k,'subject_')) {
                $update = $insert = $subject_title = "";
                if(!isset($subject_v['content'])) {
                    $subject_v['content'] = '';
                }
                $subject_v_ = $this->helpers->json_encode_then_escape($subject_v);
                
                if($subject_k == 'subject_content') {
					if(!empty($subject_v['content'])) {
                        $update .= "`subject_content` = '$subject_v_'";
                    }
                    if(isset($subject_v['title'])) {
                        $subject_title = $this->helpers->escape_string($subject_v['title']);
						if(!empty($update)) {
							$update .= ",";
						}
                        $update .= "`subject_name` = '$subject_title'";
                    }
                    if($create_new) {
                        if(empty($subject_title)) {
                            $subject_title = 'Empty Title';
                        }
                        $subject_uuid = $this->helpers->uuidSecure();
                        $subjects_obj->insertSubject('`subject_uuid`, `subject_name`, `subject_content`', "'$subject_uuid','$subject_title','$subject_v_'");
                        $subject_id = $subjects_obj->registry->mysqli->insert_id;
                    } else {
                        if(!empty($update)) $subjects_obj->updateSubject($update, "`subject_id` = '$subject_id'", '1');
                    }
                } else {
                    if(!empty($subject_id)) {
                        // check meta key
                        $meta_key = $this->helpers->escape_string($subject_k);
                        
                        $meta_key_result = $subjects_obj->getSubjectMeta("`subject_id` = '$subject_id' AND `meta_key` = '$meta_key'");
                        // create
                        if(!$meta_key_result) {
                            if(isset($subject_v['tab_name'])) {
                                $tab_name = trim($subject_v['tab_name']);
                                $key_name = 'subject_' . strtolower($this->helpers->clean_string($tab_name,'_'));
                                // create outline meta key here
                                $subjects_obj->insertSubjectMeta('`subject_id`,`meta_key`,`meta_value`,`key_name`',"'$subject_id','$key_name','$subject_v_','$tab_name'");
                            }
                        } 
                        // update
                        else {
                            $meta_value = $meta_key_result['meta_value'];
                            $meta_value = $this->helpers->json_decode_to_array_not_in_mysql($meta_value);
                            foreach($subject_v as $sv_k => $sv_v) {
                                if(!empty($sv_v)) {
                                    $meta_value[$sv_k] = $sv_v;
                                }
                            }
                            $meta_value = $this->helpers->json_encode_then_escape($meta_value);
                            $subjects_obj->updateSubjectMeta("`meta_value` = '$meta_value'", "`subject_id` = '$subject_id' AND `meta_key` = '$meta_key'",'1');
                        }
                    }
                }
            }
        }
        $subjects_obj->updateSubject("`subject_settings` = '$settings'", "`subject_id` = '$subject_id'", '1');
        $this->json['success'] = true;
        $this->json['response']['url'] = $subject_uuid;
        $this->json['response']['id'] = $subject_uuid;
    }
    
    /* ---------------------------------------------------------------------------- */
    /* ------------------------ BUILD ----------------------------- */
    /* ---------------------------------------------------------------------------- */
    function build_deletefield($args) {
        $fields_obj = new Models_FieldsModel($this->registry);
        extract($args);
        $fieldId = $this->helpers->escape_string($fieldId);
        if(empty($fieldId)) return;
        if($fields_obj->deleteField("`field_id` = '$fieldId'")) {
            $this->json['success'] = true;
        }
    }
    function build_savefield($args) {
        $forms_obj = new Models_FormsModel($this->registry);
        $fields_obj = new Models_FieldsModel($this->registry);
        extract($args);
        if(!empty($fields)) {
            $fields = $this->helpers->json_decode_to_array($fields);
            foreach($fields as $kf => $field) {
                if(isset($field['SubFields']) && !empty($field['SubFields'])) {
                    $subF2 = array();
                    foreach($field['SubFields'] as $k => $subF) {
                        if(!isset($subF['ColumnId']) || empty($subF['ColumnId'])) {
                            $field['SubFields'][$k]['ColumnId'] = ''.((!isset($subF2['ColumnId'])?0:$subF2['ColumnId'])+1);
                            $subF['ColumnId'] = ''.((!isset($subF2['ColumnId'])?0:$subF2['ColumnId'])+1);
                        }
                        if(!isset($subF['FieldId']) || empty($subF['FieldId'])) {
                            $field['SubFields'][$k]['FieldId'] = ''.((!isset($subF2['FieldId'])?0:$subF2['FieldId'])+1);
                            $subF['FieldId'] = ''.((!isset($subF2['FieldId'])?0:$subF2['FieldId'])+1);
                        }
                        if(!isset($subF['Price']) || empty($subF['Price'])) {
                            $field['SubFields'][$k]['Price'] = '0';
                            $subF['Price'] = '0';
                        }
                        if(!isset($subF['Title']) || empty($subF['Title'])) {
                            $field['SubFields'][$k]['Title'] = ($subF['ChoicesText']);
                            $subF['Title'] = ($subF['ChoicesText']);
                        }
                        $subF2 = $subF;
                    }
                    $fields[$kf]['SubFields'] = $field['SubFields'];
                }
                if(isset($field['Choices']) && !empty($field['Choices'])) {
                    $subF2 = array();
                    foreach($field['Choices'] as $k => $subF) {
                        if(!isset($subF['ColumnId']) || empty($subF['ColumnId'])) {
                            if(isset($subF2['ColumnId'])) {
                                $field['Choices'][$k]['ColumnId'] =  ''.($subF2['ColumnId']+1);
                                $subF['ColumnId'] = ''.($subF2['ColumnId']+1);
                            }
                        }
                        if(!isset($subF['Price']) || empty($subF['Price'])) {
                            $field['Choices'][$k]['Price'] = '0';
                            $subF['Price'] = '0';
                        }
                        if(!isset($subF['FormId']) || empty($subF['FormId'])) {
                            $field['Choices'][$k]['FormId'] = '0';
                            $subF['FormId'] = '0';
                        }
                        if(!isset($subF['Score']) || empty($subF['Score'])) {
                            $field['Choices'][$k]['Score'] = '0';
                            $subF['Score'] = '0';
                        }
                        if(!isset($subF['ChoiceId']) || empty($subF['ChoiceId'])) {
                            $field['Choices'][$k]['ChoiceId'] = '0';
                            $subF['ChoiceId'] = '0';
                        }
                        if(!isset($subF['StaticId']) || empty($subF['StaticId'])) {
                            $field['Choices'][$k]['StaticId'] = '0';
                            $subF['StaticId'] = '0';
                        }
                        $subF2 = $subF;
                    }
                    $fields[$kf]['Choices'] = $field['Choices'];
                }
                if(isset($field['ExtraFields']) && !empty($field['ExtraFields'])) {
                    foreach($field['ExtraFields'] as $extra_kf => $extra_field) {
                        if(isset($extra_field['SubFields']) && !empty($extra_field['SubFields'])) {
                            $extra_subF2 = array();
                            foreach($extra_field['SubFields'] as $extra_k => $extra_subF) {
                                if(!isset($extra_subF['ColumnId']) || empty($extra_subF['ColumnId'])) {
                                    $extra_field['SubFields'][$extra_k]['ColumnId'] = ''.((!isset($extra_subF2['ColumnId'])?0:$extra_subF2['ColumnId'])+1);
                                    $extra_subF['ColumnId'] = ''.((!isset($extra_subF2['ColumnId'])?0:$extra_subF2['ColumnId'])+1);
                                }
                                if(!isset($extra_subF['FieldId']) || empty($extra_subF['FieldId'])) {
                                    $extra_field['SubFields'][$extra_k]['FieldId'] = ''.((!isset($extra_subF2['FieldId'])?0:$extra_subF2['FieldId'])+1);
                                    $extra_subF['FieldId'] = ''.((!isset($extra_subF2['FieldId'])?0:$extra_subF2['FieldId'])+1);
                                }
                                if(!isset($extra_subF['Price']) || empty($extra_subF['Price'])) {
                                    $extra_field['SubFields'][$extra_k]['Price'] = '0';
                                    $extra_subF['Price'] = '0';
                                }
                                if(!isset($extra_subF['Title']) || empty($extra_subF['Title'])) {
                                    $extra_field['SubFields'][$extra_k]['Title'] = ($extra_subF['ChoicesText']);
                                    $extra_subF['Title'] = ($extra_subF['ChoicesText']);
                                }
                                $extra_subF2 = $extra_subF;
                            }
                            $field['ExtraFields'][$extra_kf]['SubFields'] = $extra_field['SubFields'];
                        }
                        if(isset($extra_field['Choices']) && !empty($extra_field['Choices'])) {
                            $extra_subF2 = array();
                            foreach($extra_field['Choices'] as $extra_k => $extra_subF) {
                                if(!isset($extra_subF['ColumnId']) || empty($extra_subF['ColumnId'])) {
                                    if(isset($extra_subF2['ColumnId'])) {
                                        $extra_field['Choices'][$extra_k]['ColumnId'] =  ''.($extra_subF2['ColumnId']+1);
                                        $extra_subF['ColumnId'] = ''.($extra_subF2['ColumnId']+1);
                                    }
                                }
                                if(!isset($extra_subF['Price']) || empty($extra_subF['Price'])) {
                                    $extra_field['Choices'][$extra_k]['Price'] = '0';
                                    $extra_subF['Price'] = '0';
                                }
                                if(!isset($extra_subF['FormId']) || empty($extra_subF['FormId'])) {
                                    $extra_field['Choices'][$extra_k]['FormId'] = '0';
                                    $extra_subF['FormId'] = '0';
                                }
                                if(!isset($extra_subF['Score']) || empty($extra_subF['Score'])) {
                                    $extra_field['Choices'][$extra_k]['Score'] = '0';
                                    $extra_subF['Score'] = '0';
                                }
                                if(!isset($extra_subF['ChoiceId']) || empty($extra_subF['ChoiceId'])) {
                                    $extra_field['Choices'][$extra_k]['ChoiceId'] = '0';
                                    $extra_subF['ChoiceId'] = '0';
                                }
                                if(!isset($extra_subF['StaticId']) || empty($extra_subF['StaticId'])) {
                                    $extra_field['Choices'][$extra_k]['StaticId'] = '0';
                                    $extra_subF['StaticId'] = '0';
                                }
                                $extra_subF2 = $extra_subF;
                            }
                            $field['ExtraFields'][$extra_kf]['Choices'] = $extra_field['Choices'];
                        }
                    }
                    $fields[$kf]['ExtraFields'] = $field['ExtraFields'];
                }
            }
            /*
            * insert
            */
            $field_content = $this->helpers->json_encode_then_escape($fields);
            $field_name = $this->helpers->escape_string($field_name);
            $field_default = $this->helpers->escape_string($field_default);
            $fieldId = $this->helpers->escape_string($fieldId);
            $field_id = $fieldId;
            $this->json['isUpdate'] = false;
            if($fieldId == 0) {
                if(!$fields_obj->insertField("`field_name`, `field_content`, `field_default`", "'$field_name', '$field_content', '$field_default'")) {
                    echo $fields_obj->getErrors();
                    exit;
                }
                $field_id = $fields_obj->registry->mysqli->insert_id;
                $this->json['success'] = true;
            } else {
            /*
            * update
            */
                if(!$fields_obj->updateField("`field_name` = '$field_name', `field_content` = '$field_content', `field_default` = '$field_default'", "`field_id` = '$fieldId'")) {
                    echo $fields_obj->getErrors();
                    exit;
                }
                $this->json['success'] = true;
                $this->json['isUpdate'] = true;
            }
            
        }
        
        $this->json['response']['field_id'] = $field_id;
        $this->json['response']['field_json'] = json_encode($fields);
    }
    function build_save($args) {
        $forms_obj = new Models_FormsModel($this->registry);
        $formId = '';
        extract($args);
        
        if(isset($byImpoter) && $byImpoter == true) {
            $user_import_id = $this->helpers->getCurrentUser();
            if($user_import_id != '1') {
                $iforms = $forms_obj->getForms('*',"`form_user` = '$user_import_id'");
                if(count($iforms) >= 10) {
                    $this->json['message'] = 'Limit Forms Contribue Allowed';
                    return;
                }
            }
            
        }
        $json = array(
            'success' => false
        );
        if(!empty($form)) {
            $__form = $this->helpers->json_decode_to_array($form);
            if(!$__form) return;
            if(empty($__form['Name']) || empty($__form['Fields'])) {
                $this->json['message'] = 'No Fields Or Form Name Empty';
                return;
            }
            
            foreach($__form['Fields'] as $kf => $field) {
                if(!isset($field['FieldLink']) || empty($field['FieldLink'])) {
                    $rn = rand(1,99) . '_' . rand(1,9) . '-' . $field['ColumnId'] . '_' . rand(1,99);
                    $__form['Fields'][$kf]['FieldLink'] = $this->helpers->encrypt($rn,'dpkey');
                }
                if(isset($field['SubFields']) && !empty($field['SubFields'])) {
                    $subF2 = array();
                    foreach($field['SubFields'] as $k => $subF) {
                        if(!isset($subF['ColumnId']) || empty($subF['ColumnId'])) {
                            $field['SubFields'][$k]['ColumnId'] = ''.((!isset($subF2['ColumnId'])?0:$subF2['ColumnId'])+1);
                            $subF['ColumnId'] = ''.((!isset($subF2['ColumnId'])?0:$subF2['ColumnId'])+1);
                        }
                        if(!isset($subF['FieldId']) || empty($subF['FieldId'])) {
                            $field['SubFields'][$k]['FieldId'] = ''.((!isset($subF2['FieldId'])?0:$subF2['FieldId'])+1);
                            $subF['FieldId'] = ''.((!isset($subF2['FieldId'])?0:$subF2['FieldId'])+1);
                        }
                        if(!isset($subF['Price']) || empty($subF['Price'])) {
                            $field['SubFields'][$k]['Price'] = '0';
                            $subF['Price'] = '0';
                        }
                        if(!isset($subF['Title']) || empty($subF['Title'])) {
                            $field['SubFields'][$k]['Title'] = ($subF['ChoicesText']);
                            $subF['Title'] = ($subF['ChoicesText']);
                        }
                        $subF2 = $subF;
                    }
                    $__form['Fields'][$kf]['SubFields'] = $field['SubFields'];
                }
                if(isset($field['Choices']) && !empty($field['Choices'])) {
                    $subF2 = array();
                    foreach($field['Choices'] as $k => $subF) {
                        if(!isset($subF['ColumnId']) || empty($subF['ColumnId'])) {
                            if(isset($subF2['ColumnId'])) {
                                $field['Choices'][$k]['ColumnId'] =  ''.($subF2['ColumnId']+1);
                                $subF['ColumnId'] = ''.($subF2['ColumnId']+1);
                            }
                        }
                        if(!isset($subF['Price']) || empty($subF['Price'])) {
                            $field['Choices'][$k]['Price'] = '0';
                            $subF['Price'] = '0';
                        }
                        if(!isset($subF['FormId']) || empty($subF['FormId'])) {
                            $field['Choices'][$k]['FormId'] = '0';
                            $subF['FormId'] = '0';
                        }
                        if(!isset($subF['Score']) || empty($subF['Score'])) {
                            $field['Choices'][$k]['Score'] = '0';
                            $subF['Score'] = '0';
                        }
                        if(!isset($subF['ChoiceId']) || empty($subF['ChoiceId'])) {
                            $field['Choices'][$k]['ChoiceId'] = '0';
                            $subF['ChoiceId'] = '0';
                        }
                        if(!isset($subF['StaticId']) || empty($subF['StaticId'])) {
                            $field['Choices'][$k]['StaticId'] = '0';
                            $subF['StaticId'] = '0';
                        }
                        $subF2 = $subF;
                    }
                    $__form['Fields'][$kf]['Choices'] = $field['Choices'];
                }
                if(isset($field['ExtraFields']) && !empty($field['ExtraFields'])) {
                    foreach($field['ExtraFields'] as $extra_kf => $extra_field) {
                        if(isset($extra_field['SubFields']) && !empty($extra_field['SubFields'])) {
                            $extra_subF2 = array();
                            foreach($extra_field['SubFields'] as $extra_k => $extra_subF) {
                                if(!isset($extra_subF['ColumnId']) || empty($extra_subF['ColumnId'])) {
                                    $extra_field['SubFields'][$extra_k]['ColumnId'] = ''.((!isset($extra_subF2['ColumnId'])?0:$extra_subF2['ColumnId'])+1);
                                    $extra_subF['ColumnId'] = ''.((!isset($extra_subF2['ColumnId'])?0:$extra_subF2['ColumnId'])+1);
                                }
                                if(!isset($extra_subF['FieldId']) || empty($extra_subF['FieldId'])) {
                                    $extra_field['SubFields'][$extra_k]['FieldId'] = ''.((!isset($extra_subF2['FieldId'])?0:$extra_subF2['FieldId'])+1);
                                    $extra_subF['FieldId'] = ''.((!isset($extra_subF2['FieldId'])?0:$extra_subF2['FieldId'])+1);
                                }
                                if(!isset($extra_subF['Price']) || empty($extra_subF['Price'])) {
                                    $extra_field['SubFields'][$extra_k]['Price'] = '0';
                                    $extra_subF['Price'] = '0';
                                }
                                if(!isset($extra_subF['Title']) || empty($extra_subF['Title'])) {
                                    $extra_field['SubFields'][$extra_k]['Title'] = ($extra_subF['ChoicesText']);
                                    $extra_subF['Title'] = ($extra_subF['ChoicesText']);
                                }
                                $extra_subF2 = $extra_subF;
                            }
                            $field['ExtraFields'][$extra_kf]['SubFields'] = $extra_field['SubFields'];
                        }
                        if(isset($extra_field['Choices']) && !empty($extra_field['Choices'])) {
                            $extra_subF2 = array();
                            foreach($extra_field['Choices'] as $extra_k => $extra_subF) {
                                if(!isset($extra_subF['ColumnId']) || empty($extra_subF['ColumnId'])) {
                                    if(isset($extra_subF2['ColumnId'])) {
                                        $extra_field['Choices'][$extra_k]['ColumnId'] =  ''.($extra_subF2['ColumnId']+1);
                                        $extra_subF['ColumnId'] = ''.($extra_subF2['ColumnId']+1);
                                    }
                                }
                                if(!isset($extra_subF['Price']) || empty($extra_subF['Price'])) {
                                    $extra_field['Choices'][$extra_k]['Price'] = '0';
                                    $extra_subF['Price'] = '0';
                                }
                                if(!isset($extra_subF['FormId']) || empty($extra_subF['FormId'])) {
                                    $extra_field['Choices'][$extra_k]['FormId'] = '0';
                                    $extra_subF['FormId'] = '0';
                                }
                                if(!isset($extra_subF['Score']) || empty($extra_subF['Score'])) {
                                    $extra_field['Choices'][$extra_k]['Score'] = '0';
                                    $extra_subF['Score'] = '0';
                                }
                                if(!isset($extra_subF['ChoiceId']) || empty($extra_subF['ChoiceId'])) {
                                    $extra_field['Choices'][$extra_k]['ChoiceId'] = '0';
                                    $extra_subF['ChoiceId'] = '0';
                                }
                                if(!isset($extra_subF['StaticId']) || empty($extra_subF['StaticId'])) {
                                    $extra_field['Choices'][$extra_k]['StaticId'] = '0';
                                    $extra_subF['StaticId'] = '0';
                                }
                                $extra_subF2 = $extra_subF;
                            }
                            $field['ExtraFields'][$extra_kf]['Choices'] = $extra_field['Choices'];
                        }
                    }
                    $__form['Fields'][$kf]['ExtraFields'] = $field['ExtraFields'];
                }
            }
            $_form = $this->helpers->json_encode_then_escape($__form);
            $formType = $__form['Formtype'];
            $alt1 = $alt2 = $alt3 = $alt4 = $alt5 = '';
            if($__form['FormId'] == '0') {
                if($formType == 'score') {
                    $alt1 = ",`form_type`";
                    $alt2 = ",'SC'";
                } elseif($formType == 'nopbai') {
                    $alt1 = ",`form_type`";
                    $alt2 = ",'NB'";
                } elseif($formType == 'autoscore') {
                    $alt1 = ",`form_type`";
                    $alt2 = ",'AS'";
                }
                if(isset($byImpoter) && $byImpoter == true)  {
                    $alt4 = ",`form_user`, `form_status`, `form_approved`";
                    $alt5 = ",'$user_import_id', 'D', 'N'";
                }
                $formId = $this->helpers->uuidSecure();
                if(!$forms_obj->insertForm("`form_uuid`,`form_content` $alt1 $alt4", "'$formId','$_form' $alt2 $alt5")) {
                    echo $forms_obj->getErrors();
                    exit;
                }
            } else {
                $formId = $__form['FormId'];
                $formId = $this->helpers->escape_string($formId);
                if($forms_obj->getForm($formId,'')) {
                    $updatedate = $this->helpers->get_datetime();
                    if($formType == 'score') {
                        $alt3 = "`form_type` = 'SC',";
                    } elseif($formType == 'nopbai') {
                        $alt3 = "`form_type` = 'NB',";
                    } elseif($formType == 'autoscore') {
                        $alt3 = "`form_type` = 'AS',";
                    } elseif($formType == 'normal') {
                        $alt3 = "`form_type` = 'NO',";
                    }
                    if(!$forms_obj->updateForm("$alt3 `form_content` = '$_form', `form_update` = '$updatedate'" , "`form_uuid` = '$formId'")) {
                        echo $forms_obj->getErrors();
                    }
                }
            }
            $this->json = array(
                'success' => true,
                'response' => array(
                    'firstForm' => '',
                    'url' => $formId,
                    'id' => $formId
                )
            );
        }
    }
    function build_displayfield($args, $extra = '') {
        extract($args);
        if(!empty($field)) {
            $_field = $this->helpers->json_decode_to_array($field);
            $fieldId = $_field['ColumnId'];
            $type = $_field['Typeof'];
        }
        // header
        $html = $this->getHtmlHead($type,$fieldId,$args['action']);
        // content
        $html .= '<fieldset>';
    	$html .= '<![if !IE | (gte IE 8)]>';
    	$html .= '<legend id="title'.$fieldId.'" class="desc">';
    	$html .= ucfirst($type);
    	$html .= '<span id="req_'.$fieldId.'" class="req"></span>';
    	$html .= '</legend>';
    	$html .= '<![endif]>';
    	$html .= '<!--[if lt IE 8]>';
    	$html .= '<label id="title'.$fieldId.'" class="desc">';
    	$html .= ucfirst($type);
    	$html .= '<span id="req_'.$fieldId.'" class="req"></span>';
    	$html .= '</label>';
    	$html .= '<![endif]-->';
        
        
        if(!empty($field)) {
            if(isset($_field['Choices'])) {
                if($_field['Typeof'] == 'radio') {
                    $html .= '<div>';
                    $html .= '<input id="radioDefault_'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" type="hidden" value="" />';
                    $i = 0;
                    foreach($_field['Choices'] as $choice) {
                        $html .= '<span>';
                        $html .= '<input id="'.$extra.'Field'.$fieldId.'_'.$i.'" name="'.$extra.'Field'.$fieldId.'" type="radio" class="field radio" value="'.$choice['Choice'].'" tabindex="'.$i.'" onchange="handleInput(this);" onmouseup="handleInput(this);" readonly="readonly" disabled="disabled"/>';
                        $html .= '<label class="choice" for="Field'.$fieldId.'_'.$i.'">';
                        $html .= $choice['Choice'];
                        $html .= '</label>';
                        $html .= '</span>';
                        if($choice['Choice'] == 'Other') {
                            $html .= '<input id="'.$extra.'Field'.$fieldId.'_other" name="'.$extra.'Field'.$fieldId.'_other_Other" type="text" class="field text other" value="" onclick="document.getElementById(\'Field'.$fieldId.'_'.$i.'\').checked = \'checked\';" onkeyup="handleInput(this);" onchange="handleInput(this);"
        					tabindex="'.$i.'" />';
                        }
                        $i++;
                    }
                    $html .= '</div>';
                    $_field['DisplayPos'] = 0;
                    array_push($_field['Settings'],'');
                    
                } elseif($_field['Typeof'] == 'likert') {
                    $type = $_field['Typeof'];
                    $validation = $_field['Validation'];
                    $html .= '<table cellspacing="0">';
                    	$html .= '<caption id="title'.$fieldId.'">';
                    		$html .= $_field['Title'];
                    		$html .= '<span id="req_'.$fieldId.'" class="req">';
                    		$html .= '</span>';
                    	$html .= '</caption>';
                    	$html .= '<thead>';
                    		$html .= '<tr>';
                    			$html .= '<th>';
                    				$html .= '&nbsp;';
                    			$html .= '</th>';
                    			foreach($_field['Choices'] as $choice) {
                    			$html .= '<td>';
                    				$html .= $choice['Choice'];
                    			$html .= '</td>';
                                }
                    		$html .= '</tr>';
                    	$html .= '</thead>';
                    	$html .= '<tbody>';
                        $i = 0;
                        foreach($_field['SubFields'] as $subField) {
                                $subFieldId = $subField['ColumnId'];
                        $html .= '<tr class="statement'.$subFieldId.'">';
                                $j=0;
                                foreach($_field['Choices'] as $choice) {
                                if($j==0) {
                                $html .= '<th>';
                                    if($validation == 'na' || empty($validation)) {
                    				$html .= '<label for="Field'.($i+1).'">';
                    					$html .= $subField['ChoicesText'];
                    				$html .= '</label>';
                                    } elseif($validation == 'dc') {
                                        $html .= '<label>';
                        					$html .= $subField['ChoicesText'];
                        				$html .= '</label>';
                                    }
                    			$html .= '</th>';
                                }
                                $html .= '<td title="'.$choice['Choice'].'">';
                                    if($validation == 'na' || empty($validation)) {
                    				$html .= '<input id="'.$extra.'Field'.$subFieldId.'_'.($j + 1).'" name="'.$extra.'Field'.$subFieldId.'" type="radio" tabindex="'.($j + 1).'" value="'.$choice['Choice'].'" onchange="handleInput(this);" disabled="disabled" readonly="readonly"/>';
                    				$html .= '<label for="Field'.$subFieldId.'_'.($j + 1).'">';
                    					$html .= isset($choice['Score'])?$choice['Score']:'';
                    				$html .= '</label>';
                                    } elseif($validation == 'dc')  {
                                        $html .= '<input id="'.$extra.'Field'.$subFieldId.'_'.($j + 1).'" name="'.$extra.'Field'.$subFieldId.'_'.($j + 1).'[]" type="checkbox" tabindex="'.($j + 1).'" value="'.$choice['Choice'].'" onchange="handleInput(this);" disabled="disabled" readonly="readonly"/>';
                                    }
                    			$html .= '</td>';
        
                                
                                $j++;
                                }
                        $html .= '</tr>';
                        $i++;
                        }
                    	$html .= '</tbody>';
                    $html .= '</table>';
                }
            }
            $_json = $_field;
        }
        
        // footer
        $html .= $this->getHtmlFoot($type,$fieldId,$args['action']);
        $this->json['success'] = true;
        $this->json['response']['id'] = $fieldId;
        $this->json['response']['html'] = $this->helpers->trim_string($html);
        $this->json['response']['json'] = json_encode($_json);
    }
    function build_changefield($args) {
        if(!empty($field)) {
            $_field = $this->helpers->json_decode_to_array($field);
            $fieldId = $_field['ColumnId'];
            $type = $_field['Typeof'];
        }
    }
    function build_addextrafield($args) {
        extract($args);
        $duplicate = false;
        if(!empty($args['field'])) {
            $duplicate = true;
            $_json = $this->getJsonDuplicate($args['field'], $fieldId);
        } else {
            $_json = $this->getExtraJson_($type, $fieldId);
        }
        $this->json_ = $_json;
        // header
        if(!$duplicate) $html = $this->getHtmlHead($type,$type,$fieldId,$args['action'],'extra_');
        else $html = $this->getHtmlHead($type,$_json['Title'],$fieldId,$args['action'],'extra_');
        
        // content
        if(!$duplicate) $html .= $this->getDisplayFields($args,'extra_');
        else $html .= $this->getDisplayDuplicateFields($args);
        
        // footer
        $html .= $this->getHtmlFoot($type,$fieldId,$args['action'],'extra_');
        $this->json['success'] = true;
        $this->json['response']['id'] = $fieldId;
        $this->json['response']['html'] = $this->helpers->trim_string($html);
        $this->json['response']['json'] = json_encode($this->json_);
    }
    function build_addfield($args) {
        extract($args);
        $duplicate = false;
        if(!empty($args['field'])) {
            $duplicate = true;
            $_json = $this->getJsonDuplicate($args['field'], $fieldId);
        } else {
            $_json = $this->getJson_($type, $fieldId);
        }
        $this->json_ = $_json;
        
        // header
        if(!$duplicate) $html = $this->getHtmlHead($type,$type,$fieldId,$args['action']);
        else $html = $this->getHtmlHead($type,$_json['Title'],$fieldId,$args['action']);
        
        // content
        if(!$duplicate) $html .= $this->getDisplayFields($args);
        else $html .= $this->getDisplayDuplicateFields($args);
        // footer
        $html .= $this->getHtmlFoot($type,$fieldId,$args['action']);
        $this->json['success'] = true;
        $this->json['response']['id'] = $fieldId;
        $this->json['response']['html'] = $this->helpers->trim_string($html);
        $this->json['response']['json'] = json_encode($this->json_);
    }
    /* ---------------------------------------------------------------------------- */
    /* ------------------------ REPORT BUILDER ----------------------------- */
    /* ---------------------------------------------------------------------------- */
    
    function report_save($args) {
        extract($args);
        $reporst_obj = new Models_ReportsModel($this->registry);
        $forms_obj = new Models_FormsModel($this->registry);
        $reportId = '';
        if(!empty($report)) {
            $__report = $this->helpers->json_decode_to_array($report);
            $form_id = $__report['FormId'];
            if(empty($__report['Graphs'])) {
                $this->json['response']['message'] = 'Your report must have at least one widget.';
                $this->json['response']['lbmarkup'] = $this->helpers->trim_string('<h2><img src="../../images/icons/error.png" alt=""/> Error Encountered</h2><br />
    <p>Something\'s gone wrong. The error is:</p>
    <div class="notice" style="text-align:center"><b>Your report must have at least one widget.</b></div><br />
    <form>
    <ul>
    <li class="buttons">
    	<a href="#" class="button lbOff negative">
    		<img src="../../images/icons/cross.png" alt=""/> Close</a>
    </li>
    </ul>
    </form>');
                return;
            }
            $form_id = ($this->helpers->escape_string($form_id));
            if(!$result = $forms_obj->getForm('',$form_id)) {
                return;
            }
            foreach($__report['Graphs'] as $k => $g) {
                $g['GraphId'] = $k;
                $__report['Graphs'][$k] = $g;
            }
            $_report = $this->helpers->json_encode_then_escape($__report);
            if($__report['ReportId'] == '') {
                $reportId= $this->helpers->uuidSecure();
                
                if(!$reporst_obj->insertReport("`form_id`,`report_uuid`,`report_content`","'$form_id','$reportId','$_report'")) {
                    echo $reporst_obj->getErrors();
                    return;
                }
            } else {
                $reportId = $__report['ReportId'];
                $reportId = $this->helpers->escape_string($reportId);
                if($result = $reporst_obj->getReport($reportId,'')) {
                    $updatedate = $this->helpers->get_datetime();
                    if(!$reporst_obj->updateReport("`form_id` = '$form_id',`report_content` = '$_report', `report_update` = '$updatedate'", "`report_uuid` = '$reportId'")) {
                        echo $reporst_obj->getErrors();
                    }
                }
            }
            $this->json = array(
                'success' => true,
                'response' => array(
                    'url' => $reportId,
                    'reportId' => $reportId
                )
            );
        }
    }
    function report_addWidget($args) {
        extract($args);
        if(empty($newWidgetInfo)) return;
        $newWidgetInfo = $this->helpers->json_decode_to_array($newWidgetInfo);
        $widgetType = $newWidgetInfo['widgetType'];
        $fieldType = $newWidgetInfo['fieldType'];
        $widgetId = $newWidgetInfo['widgetId'];
        $title = $newWidgetInfo['title'];
        $reportId = $newWidgetInfo['reportId'];
        $widget = array(
            'GraphId' => $widgetId,
            'Name' => '',
            'Size' => '',
            'Typeof' => $widgetType,
            'FieldName' => '',
            'ReportId' => $reportId,
            'Description' => '',
            'Format' => '',
            'Detail' => 'both',
            'Color' => $newWidgetInfo['color'],
            'Url' => null,
            'Zone' => $newWidgetInfo['zone'],
            'Position' => $newWidgetInfo['position'],
            'ChoiceId' => null,
            'Width' => null,
            'Height' => null
        );
        ob_start();
        ?>
        <div id="widget_preview_<?php echo $widgetId; ?>" class="wfo_widget fc" onmousedown="pickWidget(this)" title="Click to edit. Drag to reorder."> <table cellspacing="0" class="choices "> <caption> <h4 id="widget_preview_<?php echo $widgetId; ?>_fieldChart_title" class="notranslate"> <?php echo $title; ?> </h4> <span class="fcNav"> <a>&nbsp;</a> </span> </caption> <thead> <tr> <th> Choices </th> <th class="percent"> Percentage </th> <th class="count"> Count </th> </tr> </thead> <tfoot> <tr> <td class="empty"> &nbsp; </td> <th> Total </th> <td class="count"> 100 </td> </tr> <tr> <td class="empty"> &nbsp; </td> <th> <em> Unanswered </em> </th> <td class="count"> <em> 10 </em> </td> </tr> </tfoot> <tbody> <tr class="color1"> <th> <div> &nbsp; </div> </th> <td class="percent"> <div class="bar" style="width:50%"> </div> <var> 50% </var> </td> <td class="count"> 50 </td> </tr> <tr class="color2 alt"> <th> <div> &nbsp; </div> </th> <td class="percent"> <div class="bar" style="width:25%"> </div> <var> 25% </var> </td> <td class="count"> 25 </td> </tr> <tr class="color3"> <th> <div> &nbsp; </div> </th> <td class="percent"> <div class="bar" style="width:10%"> </div> <var> 10% </var> </td> <td class="count"> 10 </td> </tr> <tr class="color4 alt"> <th> <div> &nbsp; </div> </th> <td class="percent"> <div class="bar" style="width:5%"> </div> <var> 5% </var> </td> <td class="count"> 5 </td> </tr> <tr class="color5"> <th> <div> &nbsp; </div> </th> <td class="percent"> <div class="bar" style="width:2%"> </div> <var> 2% </var> </td> <td class="count"> 2 </td> </tr> <tr class="color0 alt"> <th> <div> &nbsp; </div> </th> <td class="percent"> <div class="bar" style="width:8%"> </div> <var> 8% </var> </td> <td class="count"> 8 </td> </tr> </tbody> </table> <div id="wa<?php echo $widgetId; ?>" class="widgetActions"> <img class="waDup" src="../../images/icons/add.png" alt="Duplicate." title="Duplicate" onclick="duplicateWidget(this); return false" /> <img class="waDel" src="../../images/icons/delete.png" alt="Delete." title="Delete" onclick="deleteWidget(this); return false" /> </div> </div>
        <?php
        $html = ob_get_contents();
        ob_clean();
        $this->json['response']['html'] = $this->helpers->trim_string($html);
        $this->json['response']['widget'] = json_encode($widget);
        $this->json['success'] = true;
    }
    function report_updateFieldChartByType($args) {
        extract($args);
        if(empty($widget)) return;
        $widget = $this->helpers->json_decode_to_array($widget);
        $title = $widget['Name'];
        $widgetId = $widget['GraphId'];
        ob_start();
        ?>
        <div id="widget_preview_<?php echo $widgetId; ?>" class="wfo_widget fc" onmousedown="pickWidget(this)" title="Click to edit. Drag to reorder."> <table cellspacing="0" class="choices "> <caption> <h4 id="widget_preview_<?php echo $widgetId; ?>_fieldChart_title" class="notranslate"> <?php echo $title; ?> </h4> <span class="fcNav"> <a>&nbsp;</a> </span> </caption> <thead> <tr> <th> Choices </th> <th class="percent"> Percentage </th> <th class="count"> Count </th> </tr> </thead> <tfoot> <tr> <td class="empty"> &nbsp; </td> <th> Total </th> <td class="count"> 100 </td> </tr> <tr> <td class="empty"> &nbsp; </td> <th> <em> Unanswered </em> </th> <td class="count"> <em> 10 </em> </td> </tr> </tfoot> <tbody> <tr class="color1"> <th> <div> &nbsp; </div> </th> <td class="percent"> <div class="bar" style="width:50%"> </div> <var> 50% </var> </td> <td class="count"> 50 </td> </tr> <tr class="color2 alt"> <th> <div> &nbsp; </div> </th> <td class="percent"> <div class="bar" style="width:25%"> </div> <var> 25% </var> </td> <td class="count"> 25 </td> </tr> <tr class="color3"> <th> <div> &nbsp; </div> </th> <td class="percent"> <div class="bar" style="width:10%"> </div> <var> 10% </var> </td> <td class="count"> 10 </td> </tr> <tr class="color4 alt"> <th> <div> &nbsp; </div> </th> <td class="percent"> <div class="bar" style="width:5%"> </div> <var> 5% </var> </td> <td class="count"> 5 </td> </tr> <tr class="color5"> <th> <div> &nbsp; </div> </th> <td class="percent"> <div class="bar" style="width:2%"> </div> <var> 2% </var> </td> <td class="count"> 2 </td> </tr> <tr class="color0 alt"> <th> <div> &nbsp; </div> </th> <td class="percent"> <div class="bar" style="width:8%"> </div> <var> 8% </var> </td> <td class="count"> 8 </td> </tr> </tbody> </table> <div id="wa<?php echo $widgetId; ?>" class="widgetActions"> <img class="waDup" src="../../images/icons/add.png" alt="Duplicate." title="Duplicate" onclick="duplicateWidget(this); return false" /> <img class="waDel" src="../../images/icons/delete.png" alt="Delete." title="Delete" onclick="deleteWidget(this); return false" /> </div> </div>
        <?php
        $html = ob_get_contents();
        ob_clean();
        $this->json['response']['widgetId'] = $widgetId;
        $this->json['response']['html'] = $html;
        $this->json['success'] = true;
    }
    function report_getForm($args) {
        extract($args);
        $catetory = $this->registry->category;
        $forms_obj = new Models_FormsModel($this->registry);
        
        if(!$form = $forms_obj->getForm('',$form_id)) return;
        
        $formUuid = $form['form_uuid'];
        //$form = stripslashes($form['form_content']);
        $form = ($form['form_content']);
        $_form = $this->helpers->json_decode_to_array($form);
        $_form['FormId'] = $form_id;
        $_form['Url'] = $formUuid;
        foreach($catetory as $cat) {
            array_push($_form['Fields'],$cat);
        }
        $this->json['response']['form'] = json_encode($_form);
        $this->json['success'] = true;
    }
    
    /* ---------------------------------------------------------------------------- */
    /* ------------------------ REPORTS MANAGER ----------------------------- */
    /* ---------------------------------------------------------------------------- */
    function reports_duplicate($args) {
        extract($args);
        if(!empty($reportId)) {
            $reports_obj = new Models_ReportsModel($this->registry);
            if($result = $reports_obj->getReport('',$reportId)) {
                $report = ($result['report_content']);
                $_report = $this->helpers->json_decode_to_array($report);
                $_report['Name'] = $_report['Name'] . ' Cloned';
                $report_content = $this->helpers->json_encode_then_escape($_report);
                $newreportId = $this->helpers->uuidSecure();
                $form_id = $result['form_id'];
                
                if(!$reports_obj->insertReport("`report_uuid`,`form_id`,`report_content`", "'$newreportId','$form_id','$report_content'")) {
                    echo $reports_obj->getErrors();
                    exit;
                } else {
                    $this->json['success'] = true;
                }
            } else {
                echo $reports_obj->getErrors();
                exit;
            }
        }
    }
    function reports_delete($args) {
        extract($args);
        if(!empty($reportId)) {
            $reports_obj = new Models_ReportsModel($this->registry);
            if(!$reports_obj->deleteReport('',$reportId)) {
                echo $reports_obj->getErrors();
                exit;
            } else {
                $this->json['success'] = true;
            }
        }
    }
    function reports_order($args) {
        extract($args);
        $reports_obj = new Models_ReportsModel($this->registry);
        $html = '';
        if($orderBy == 'ReportId') $orderBy = 'report_create';
        if($orderBy == 'DateUpdated') $orderBy = 'report_update';
        if($reports_result = $reports_obj->getReports('',$orderBy,$orderDirection)) {
            $i=0;
            foreach($reports_result as $report) {
                ob_start();
                $report_id = $report['report_id'];
                $reportId = $report['report_uuid'];
                if(empty($report['report_content'])) continue;
                $_report = $this->helpers->json_decode_to_array($report['report_content']);
                
                ?>
                <li value="d_<?php echo $reportId; ?>" id="li<?php echo $report_id; ?>" class="<?php echo ($i==0)?'first':'';$i++; ?>" onmouseover="showActions(this)" onmouseout="hideActions(this)">
                	<h4>
                		<a id="link<?php echo $report_id; ?>" href="../reports/index.php?reportId=<?php echo $reportId; ?>" target="_blank" class="notranslate">
                		<?php echo $_report['Name']; ?>
                		</a>
                	</h4>
                	<span class="themeSelect">
                		<select onchange="changeTheme(<?php echo $report_id; ?>, 'Untitled%20Report', this)">
                			<option value="1">
                				Default Theme
                			</option>
                			<option value="create">
                				-----------
                			</option>
                			<option value="create">
                				New Theme!
                			</option>
                		</select>
                	</span>
                	<span class="activeCheck">
                		<label for="publicStatus_<?php echo $report_id; ?>">
                			Public
                		</label>
                		<input type="checkbox" checked="checked" id="publicStatus_<?php echo $report_id; ?>" onclick="togglePublicReport(<?php echo $report_id; ?>, 'Untitled%20Report', this)" />
                	</span>
                	<div id="expandThis">
                		<div class="actions">
                			<a class="del" onclick="deleteReport(this); return false;" href="#">Delete</a>
                			<a class="dup" onclick="duplicateReport(this); return false;" href="#">Duplicate</a>
                			<a class="view" href="../reports/index.php?reportId=<?php echo $reportId; ?>" target="_blank">View</a>
                			<a class="edit" href="#" onclick="editReport(this); return false;">Edit</a>
                			<a class="export undone" href="#" onclick="exportMe(this);return false;">Export</a>
                			<a class="code undone" href="../widget/manager/index.php?reportId=<?php echo $reportId; ?>" target="_self">Widgets</a>
                			<span>
                				<a class="protect" href="#" onclick="showProtectReport(this);return false;">Protect</a>
                			</span>
                		</div>
                	</div>
                </li>
                <?php
                $html .= ob_get_contents();
                ob_clean();
            }
        }
        $this->json['success'] = true;
        $this->json['response']['markup'] = $this->helpers->trim_string($html);
    }
    function reports_savePublicStatus($args) {
        $reports_obj = new Models_ReportsModel($this->registry);
        $this->common_savePublicStatus($reports_obj,$args,'reportId');
    }
    function reports_protect($args) {
        $reports_obj = new Models_ReportsModel($this->registry);
        $this->common_protect($reports_obj,$args,'reportId');
    }
    /* ---------------------------------------------------------------------------- */
    /* ------------------------ COMMON ----------------------------- */
    /* ---------------------------------------------------------------------------- */
    function common_protect($obj,$args, $id) {
        $password = md5($this->helpers->escape_string($args['password']));
        $commonId = $this->helpers->escape_string($args[$id]);
        $obj->updateProtectPassword($password,$commonId);
        if(!$obj->getErrors()) {
            $this->json['success'] = true;
        }
    }
    function common_savePublicStatus($obj,$args,$id) {
        $commonId = $this->helpers->escape_string($args[$id]);
        $publicStatus = $args['publicStatus'];
        if($publicStatus == '0') $status = 'D';
        elseif($publicStatus == '1') $status = 'A';
        if($obj->updatePublicStatus($status,$commonId)) {
            $this->json['success'] = true;
        }
    }
    
    /* ---------------------------------------------------------------------------- */
    /* ------------------------ CACHES ----------------------------- */
    /* ---------------------------------------------------------------------------- */
    
    function rebuildCache($user,$user_in_group,$user_weights,$users_obj,$caches_obj,$groups_obj) {
        $user_id = $user['user_id'];
        $nb_success = $as_success = $us_success = $nbed_success = $sced_success = array(
            'done' => false
        );
        $user_content = $this->helpers->json_decode_to_array_not_in_mysql($user['user_content']);
        /*
        * Caching Entries
        */
        $entries_obj = new Models_EntriesModel($this->registry);
        $forms_obj = new Models_FormsModel($this->registry);
        $caches_c_obj = new Controllers_cachesController($this->registry);
        $entries_result = $entries_obj->getEntries("`entry_user` = '$user_id' AND `entry_type` = 'NB'");
        $entries_result_private = $entries_obj->getEntries("`entry_user` = '$user_id' AND `entry_type` = 'AS'");
        $entries_result_sc = $entries_obj->getEntries("`entry_user` = '$user_id' AND `entry_type` = 'SC'");
        $entri_sc_ids = array();
        /*
        * Caching Entries NB
        */
        $i = $my_percent = $my_admin_percent = 0;
        if($entries_result) foreach($entries_result as $entry) :
            $i++;
            $entry_id = $entry['entry_id'];
            $entry_uuid = $entry['entry_uuid'];
            $entry_meta_content = $entries_obj->getEntryMeta("`entry_id` = '$entry_id'");
            if(!$entry_meta_content) continue;
            $form_id = $entry_meta_content['form_id'];
            if($form_nb = $forms_obj->getForm('',$form_id)) :
                $form_nb_content = $this->helpers->json_decode_to_array($form_nb['form_content']);
                $form_sc_uuid = $form_nb_content['FormScoreUrl'];
                $fields = $form_nb_content['Fields'];
                $form_uuid = $form_nb['form_uuid'];
                $entries = $this->helpers->json_decode_to_array_not_in_mysql($entry['entry_content']);
                $exerpts = $this->helpers->getExcerptFields($fields,$entries);
                /*
                * Sumary
                */
                $scoring_result = $entries_obj->getEntries("`entry_of` = '$entry_uuid' AND `entry_type` = 'SC'");
                $total = $admin_total = $point = $admin_point = $percent = $admin_percent = $pps = $pointed = $weights = 0;
                if($scoring_result) foreach($scoring_result as $score) {
                    $totaling = $pointing = 0;
                    $score_user_id = $score['entry_user'];
                    $weight = isset($user_weights[$score_user_id])?$user_weights[$score_user_id]:'';
                    if(empty($weight)) $weight = 1;
                    $pps++;
                    $score_scoring = $this->helpers->json_decode_to_array($score['entry_scoring']);
                    if($score['entry_user'] == '1') {
                        foreach($score_scoring['other_fields'] as $of) {
                            if(isset($of['Value']) && is_numeric($of['Value'])) {
                                $admin_total += !empty($of['MaxValue'])?$of['MaxValue']:$of['Value'];
                                $admin_point += $of['Value'];
                            }
                        }
                        $admin_total += $score_scoring['RightFieldCount'];
                        $admin_point += $score_scoring['point'];
                    } else {
                        foreach($score_scoring['other_fields'] as $of) {
                            if(isset($of['Value']) && is_numeric($of['Value'])) {
                                $total += !empty($of['MaxValue'])?$of['MaxValue']:$of['Value'];
                                $totaling += !empty($of['MaxValue'])?$of['MaxValue']:$of['Value'];
                                $point += $of['Value'];
                                $pointing += $of['Value'];
                            }
                        }
                        $total += $score_scoring['RightFieldCount'];
                        $totaling += $score_scoring['RightFieldCount'];
                        $point += $score_scoring['point'];
                        $pointing += $score_scoring['point'];
                    
                        if($totaling != 0) {
                            $pointed += ($pointing/$totaling*100)*$weight;
                            $weights += $weight;
                        }
                        
                    }
                }
                if($weights != 0) $percent = $pointed/$weights;
                if($admin_total != 0) $admin_percent = ($admin_point)/$admin_total*100;
                $my_percent += $percent;
                $my_admin_percent += $admin_percent;
                /*
                * #Summary
                */
            /*
            * Insert Cached To DB
            */
            $cache_form_nb_display = $caches_c_obj->getFormNBDisplayResult($form_nb,$entry,$user,false);
            $cache_content = array(
                    'excerpts' => $exerpts,
                    'scoring' => array(
                        'total' => $total,
                        'point' => $point,
                        'admin_total' => $admin_total,
                        'admin_point' => $admin_point
                    )
            );
            $cache_user_class = array();
            $contents_nb = array(
                'cache_user_id' => $user_id,
                'cache_user_username' => $user['username'],
                'cache_user_email' => $user['user_email'],
                'cache_user_nickh2d' => $user_content['NickH2d'],
                'cache_user_nickskype' => $user_content['NickSkype'],
                'cache_user_fullname' => $user_content['Name'],
                'cache_user_class' => $this->helpers->json_encode_then_escape($cache_user_class),
                'cache_user_content' => $this->helpers->json_encode_then_escape($user_content),
                'cache_entry_id' => $entry['entry_id'],
                'cache_entry_uuid' => $entry['entry_uuid'],
                'cache_entry_public' => $entry['entry_public'],
                'cache_entry_ip' => $entry['entry_ip'],
                'cache_entry_content' => $this->helpers->json_encode_then_escape($entries),
                'cache_entry_complete' => $entry['entry_complete'],
                'cache_entry_t' => $pps,
                'cache_entry_hs' => $percent,
                'cache_entry_tg' => $admin_percent,
                'cache_entry_sys' => 0,
                'cache_entry_sys_t' => 0,
                'cache_form_nb_id' => $form_nb['form_id'],
                'cache_form_nb_uuid' => $form_nb['form_uuid'],
                'cache_form_sc_uuid' => $form_nb_content['FormScoreUrl'],
                'cache_form_nb_type' => $form_nb['form_type'],
                'cache_form_nb_name' => $this->helpers->escape_string($form_nb_content['Name']),
                'cache_form_nb_status' => $form_nb['form_status'],
                'cache_form_nb_theme_id' => $form_nb['theme_id'],
                'cache_form_nb_content' => $this->helpers->json_encode_then_escape($form_nb_content),
                'cache_form_nb_password' => $form_nb['form_password'],
                'cache_form_nb_display_result' => $this->helpers->json_encode_then_escape($cache_form_nb_display),
                'cache_content' => $this->helpers->json_encode_then_escape($cache_content)
            );
            $nb_success = $caches_obj->cache_entries_nb($contents_nb);
            /*
            * #Insert Cached To DB
            */
            endif;
        endforeach;
        if($my_percent!=0) $my_percent = number_format($my_percent/$i,1);
        if($my_admin_percent!=0) $my_admin_percent = number_format($my_admin_percent/$i,1);
        
        /*
        * #Caching Entries NB
        */
        /*
        * Caching Entries AS
        */
        
        if($entries_result_private) foreach($entries_result_private as $entry) :
            $entry_id = $entry['entry_id'];
            $entry_uuid = $entry['entry_uuid'];
            $entry_meta_content = $entries_obj->getEntryMeta("`entry_id` = '$entry_id'");
            if(!$entry_meta_content) continue;
            $form_id = $entry_meta_content['form_id'];
            if($form_nb = $forms_obj->getForm('',$form_id)) :
                $form_nb_content = $this->helpers->json_decode_to_array($form_nb['form_content']);
                $form_sc_uuid = $form_nb_content['FormScoreUrl'];
                $fields = $form_nb_content['Fields'];
                $entries = $this->helpers->json_decode_to_array_not_in_mysql($entry['entry_content']);
                $exerpts = $this->helpers->getExcerptFields($fields,$entries);
                /*
                * System
                */
                $scoring_result = $entries_obj->getEntries("`entry_uuid` = '$entry_uuid' AND `entry_type` = 'AS'");
                $sys_total = $sys_point = $sys_percent = 0;
                if($scoring_result) foreach($scoring_result as $score) {
                    $score_scoring = $this->helpers->json_decode_to_array($score['entry_scoring']);
                    foreach($score_scoring['other_fields'] as $of) {
                        if(isset($of['Value']) && is_numeric($of['Value'])) {
                            $sys_total += !empty($of['MaxValue'])?$of['MaxValue']:$of['Value'];
                            $sys_point += $of['Value'];
                        }
                    }
                    $sys_total += $score_scoring['RightFieldCount'];
                    $sys_point += $score_scoring['point'];
                }
                if($sys_total != 0) $sys_percent = number_format($sys_point/$sys_total*100,1);
                /*
                * #System
                */
                /*
                * Insert Cached To DB
                */
                $cache_form_nb_display = $caches_c_obj->getFormASDisplayResult($form_nb,$entry,$user,false,true);
                $cache_content = array(
                    'excerpts' => $exerpts,
                    'scoring' => array(
                        'sys_total' => $sys_total,
                        'sys_point' => $sys_point
                    )
                );
                $cache_user_class = array();
                $contents_as = array(
                    'cache_user_id' => $user_id,
                    'cache_user_username' => $user['username'],
                    'cache_user_email' => $user['user_email'],
                    'cache_user_nickh2d' => $this->helpers->escape_string($user_content['NickH2d']),
                    'cache_user_nickskype' => $this->helpers->escape_string($user_content['NickSkype']),
                    'cache_user_fullname' => $this->helpers->escape_string($user_content['Name']),
                    'cache_user_class' => $this->helpers->json_encode_then_escape($cache_user_class),
                    'cache_user_content' => $this->helpers->json_encode_then_escape($user_content),
                    'cache_entry_id' => $entry['entry_id'],
                    'cache_entry_uuid' => $entry['entry_uuid'],
                    'cache_entry_public' => $entry['entry_public'],
                    'cache_entry_ip' => $entry['entry_ip'],
                    'cache_entry_content' => $this->helpers->json_encode_then_escape($entries),
                    'cache_entry_complete' => $entry['entry_complete'],
                    'cache_entry_t' => 0,
                    'cache_entry_hs' => 0,
                    'cache_entry_tg' => 0,
                    'cache_entry_sys' => $sys_percent,
                    'cache_entry_sys_t' => 1,
                    'cache_form_nb_id' => $form_nb['form_id'],
                    'cache_form_nb_uuid' => $form_nb['form_uuid'],
                    'cache_form_sc_uuid' => 0,
                    'cache_form_nb_type' => $form_nb['form_type'],
                    'cache_form_nb_name' => $this->helpers->escape_string($form_nb_content['Name']),
                    'cache_form_nb_status' => $form_nb['form_status'],
                    'cache_form_nb_theme_id' => $form_nb['theme_id'],
                    'cache_form_nb_content' => $this->helpers->json_encode_then_escape($form_nb_content),
                    'cache_form_nb_password' => $form_nb['form_password'],
                    'cache_form_nb_display_result' => $this->helpers->json_encode_then_escape($cache_form_nb_display),
                    'cache_content' => $this->helpers->json_encode_then_escape($cache_content)
                );
                $as_success = $caches_obj->cache_entries_as($contents_as);
                /*
                * #Insert Cached To DB
                */
            endif;
        endforeach;
        /*
        * #Caching Entries AS
        */
        
        /*
        * Stats System
        */
        $scoring_result = $entries_obj->getEntries("`entry_user` = '$user_id' AND `entry_type` = 'AS'");
        $sys_total = $sys_percent = $sys_point = $s_pp = 0;
        if($scoring_result) foreach($scoring_result as $score) {
            $s_pp++;
            $score_scoring = $this->helpers->json_decode_to_array($score['entry_scoring']);
            foreach($score_scoring['other_fields'] as $of) {
                if(isset($of['Value']) && is_numeric($of['Value'])) {
                    $sys_total += !empty($of['MaxValue'])?$of['MaxValue']:$of['Value'];
                    $sys_point += $of['Value'];
                }
            }
            $sys_total += $score_scoring['RightFieldCount'];
            $sys_point += $score_scoring['point'];
        }
        if($sys_total != 0) $sys_percent = $sys_point/$sys_total*100;
        $my_sys_percent = $sys_percent;
        $courses = array();
        $cache_user_class = array();
        $users_stats = array(
            'cache_user_id' => $user_id,
            'cache_user_status' => $user['user_status'],
            'cache_user_username' => $user['username'],
            'cache_user_email' => $user['user_email'],
            'cache_user_nickh2d' => $this->helpers->escape_string($user_content['NickH2d']),
            'cache_user_nickskype' => $this->helpers->escape_string($user_content['NickSkype']),
            'cache_user_fullname' => $this->helpers->escape_string($user_content['Name']),
            'cache_user_class' => $this->helpers->json_encode_then_escape($cache_user_class),
            'cache_user_content' => $this->helpers->json_encode_then_escape($user_content),
            'cache_stats_t' => $i,
            'cache_stats_hs' => $my_percent,
            'cache_stats_tg' => $my_admin_percent,
            'cache_stats_sys' => $my_sys_percent,
            'cache_stats_sys_t' => $s_pp,
            'cache_time' => time(),
        );
        $us_success = $caches_obj->cache_users_stats($users_stats);
        /*
        * #Stats System
        */
        /*
        * Caching Entries SCED
        */
        if($entries_result_sc) foreach($entries_result_sc as $entry) :
            $entry_id = $entry['entry_id'];
            $entry_uuid = $entry['entry_uuid'];
            $entry_of = $entry['entry_of'];
            $entri_sc_ids[] = '\''.$entry_of.'\'';
            $entry_orgn = $entries_obj->getEntry($entry_of,'');
            if(!$entry_orgn) continue;
            $user_of_id = $entry_orgn['entry_user'];
            if(in_array($user_of_id, $user_in_group) || $user_id == '1') {
                if($user_of = $users_obj->getUser("`user_id` = '$user_of_id'")) {
                    $user_of_content = $this->helpers->json_decode_to_array($user_of['user_content']);
                } else {
                    continue;
                }
                /*
                * Sumary
                */
                $scoring_result = $entries_obj->getEntries("`entry_of` = '$entry_of' AND `entry_type` = 'SC'");
                $total = $admin_total = $max_point = $point = $admin_point = $percent = $admin_percent = $pps = 0;
                if($scoring_result) foreach($scoring_result as $score) {
                    $pps++;
                    $score_scoring = $this->helpers->json_decode_to_array($score['entry_scoring']);
                    if($score['entry_user'] == '1') {
                        foreach($score_scoring['other_fields'] as $of) {
                            if(isset($of['Value']) && is_numeric($of['Value'])) {
                                $admin_total += !empty($of['MaxValue'])?$of['MaxValue']:$of['Value'];
                                $admin_point += $of['Value'];
                            }
                        }
                        $admin_total += $score_scoring['RightFieldCount'];
                        $admin_point += $score_scoring['point'];
                    } else {
                        foreach($score_scoring['other_fields'] as $of) {
                            if(isset($of['Value']) && is_numeric($of['Value'])) {
                                $total += !empty($of['MaxValue'])?$of['MaxValue']:$of['Value'];
                                $point += $of['Value'];
                            }
                        }
                        $total += $score_scoring['RightFieldCount'];
                        $point += $score_scoring['point'];
                    }
                }
                
                if($total != 0) $percent = $point/$total*100;
                if($admin_total != 0) $admin_percent = $admin_point/$admin_total*100;
                /*
                * #Summary
                */
                /*
                * Insert Cached To DB
                */
                $exerpts = array();
                $cache_content = array(
                    'excerpts' => $exerpts
                );
                $cache_user_class = array();
                
                $contents_sced = array(
                    'cache_user_id' => $user_id,
                    'cache_user_username' => $user['username'],
                    'cache_user_email' => $user['user_email'],
                    'cache_user_nickh2d' => $this->helpers->escape_string($user_content['NickH2d']),
                    'cache_user_nickskype' => $this->helpers->escape_string($user_content['NickSkype']),
                    'cache_user_fullname' => $this->helpers->escape_string($user_content['Name']),
                    'cache_user_class' => $this->helpers->json_encode_then_escape($cache_user_class),
                    'cache_user_content' => $this->helpers->json_encode_then_escape($user_content),
                    
                    'cache_user_of_id' => $user_of_id,
                    'cache_user_of_username' => $user_of['username'],
                    'cache_user_of_email' => $user_of['user_email'],
                    'cache_user_of_nickh2d' => $this->helpers->escape_string($user_of_content['NickH2d']),
                    'cache_user_of_nickskype' => $this->helpers->escape_string($user_of_content['NickSkype']),
                    'cache_user_of_fullname' => $this->helpers->escape_string($user_of_content['Name']),
                    'cache_user_of_class' => isset($user_of_content['Class'])?$user_of_content['Class']:'',
                    'cache_user_of_content' => $this->helpers->json_encode_then_escape($user_of_content),
                    
                    'cache_entry_id' => $entry_orgn['entry_id'],
                    'cache_entry_uuid' => $entry_orgn['entry_uuid'],
                    'cache_entry_public' => $entry_orgn['entry_public'],
                    'cache_entry_ip' => $entry_orgn['entry_ip'],
                    'cache_entry_content' => $this->helpers->json_encode_then_escape($entry_orgn['entry_content']),
                    'cache_entry_complete' => $entry_orgn['entry_complete'],
                    'cache_entry_t' => $pps,
                    'cache_entry_hs' => $percent,
                    'cache_entry_tg' => $admin_percent,
                    'cache_entry_sys' => 0,
                    'cache_entry_sys_t' => 0,
                    'cache_form_nb_id' => 0,
                    'cache_form_nb_uuid' => 0,
                    'cache_form_sc_uuid' => 0,
                    'cache_form_nb_type' => 'NB',
                    'cache_form_nb_name' => 0,
                    'cache_form_nb_status' => 0,
                    'cache_form_nb_theme_id' => 0,
                    'cache_form_nb_content' => 0,
                    'cache_form_nb_password' => 0,
                    'cache_content' => $this->helpers->json_encode_then_escape($cache_content)
                );
                $sced_success = $caches_obj->cache_entries_sced($contents_sced);
            }
            /*
            * #Insert Cached To DB
            */
        endforeach;
        /*
        * #Caching Entries SCED
        */
        /*
        * Caching Entries NBED
        */
        $entri_sc_ids = implode(',',$entri_sc_ids);
        if(empty($entri_sc_ids)) $entri_sc_ids = '\'0\'';
        $entries_result_nbs = $entries_obj->getEntries("`entry_uuid` NOT IN ($entri_sc_ids) AND `entry_type` = 'NB' AND `entry_user` != '$user_id'");
        $caches_obj->deleteCachedNBED($user_id, $entri_sc_ids);
        if($entries_result_nbs) foreach($entries_result_nbs as $entry) :
            $entry_id = $entry['entry_id'];
            $entry_uuid = $entry['entry_uuid'];
            $entry_meta_content = $entries_obj->getEntryMeta("`entry_id` = '$entry_id'");
            if(!$entry_meta_content) continue;
            $user_by_id = $entry['entry_user'];
            if(in_array($user_by_id, $user_in_group) || $user_id == '1') {
                $form_id = $entry_meta_content['form_id'];
                $form_nb = $forms_obj->getForm('',$form_id);
                $form_nb_content = $this->helpers->json_decode_to_array($form_nb['form_content']);
                $form_sc_uuid = $form_nb_content['FormScoreUrl'];
                
                
                $fields = $form_nb_content['Fields'];
                $entries = $this->helpers->json_decode_to_array_not_in_mysql($entry['entry_content']);
                $exerpts = $this->helpers->getExcerptFields($fields,$entries);
                if($user_by = $users_obj->getUser("`user_id` = '$user_by_id'")) {
                    $user_by_content = $this->helpers->json_decode_to_array($user_by['user_content']);
                } else {
                    continue;
                }
                /*
                * Sumary
                */
                $scoring_result = $entries_obj->getEntries("`entry_of` = '$entry_uuid' AND `entry_type` = 'SC'");
                $total = $admin_total = $point = $admin_point = $percent = $admin_percent = $pps = $pointed = $weights = 0;
                if($scoring_result) foreach($scoring_result as $score) {
                    $totaling = $pointing = 0;
                    $score_user_id = $score['entry_user'];
                    if(isset($user_weights[$score_user_id])) $weight = $user_weights[$score_user_id];
                    if(empty($weight)) $weight = 1;
                    $pps++;
                    $score_scoring = $this->helpers->json_decode_to_array($score['entry_scoring']);
                    if($score['entry_user'] == '1') {
                        foreach($score_scoring['other_fields'] as $of) {
                            if(isset($of['Value']) && is_numeric($of['Value'])) {
                                $admin_total += !empty($of['MaxValue'])?$of['MaxValue']:$of['Value'];
                                $admin_point += $of['Value'];
                            }
                        }
                        $admin_total += $score_scoring['RightFieldCount'];
                        $admin_point += $score_scoring['point'];
                    } else {
                        foreach($score_scoring['other_fields'] as $of) {
                            if(isset($of['Value']) && is_numeric($of['Value'])) {
                                $total += !empty($of['MaxValue'])?$of['MaxValue']:$of['Value'];
                                $totaling += !empty($of['MaxValue'])?$of['MaxValue']:$of['Value'];
                                $point += $of['Value'];
                                $pointing += $of['Value'];
                            }
                        }
                        $total += $score_scoring['RightFieldCount'];
                        $totaling += $score_scoring['RightFieldCount'];
                        $point += $score_scoring['point'];
                        $pointing += $score_scoring['point'];
                    
                        if($totaling != 0) {
                            $pointed += ($pointing/$totaling*100)*$weight;
                            $weights += $weight;
                        }
                        
                    }
                }
                if($weights != 0) $percent = $pointed/$weights;
                if($admin_total != 0) $admin_percent = $admin_point/$admin_total*100;
                /*
                * #Summary
                */
                /*
                * Insert Cached To DB
                */
                $cache_content = array(
                    'excerpts' => $exerpts
                );
                $cache_user_class = array();
                
                $contents_nbed = array(
                    'cache_user_id' => $user_id,
                    'cache_user_username' => $user['username'],
                    'cache_user_email' => $user['user_email'],
                    'cache_user_nickh2d' => $this->helpers->escape_string($user_content['NickH2d']),
                    'cache_user_nickskype' => $this->helpers->escape_string($user_content['NickSkype']),
                    'cache_user_fullname' => $this->helpers->escape_string($user_content['Name']),
                    'cache_user_class' => $this->helpers->json_encode_then_escape($cache_user_class),
                    'cache_user_content' => $this->helpers->json_encode_then_escape($user_content),
                    
                    'cache_user_by_id' => $user_by_id,
                    'cache_user_by_username' => $user_by['username'],
                    'cache_user_by_email' => $user_by['user_email'],
                    'cache_user_by_nickh2d' => $this->helpers->escape_string($user_by_content['NickH2d']),
                    'cache_user_by_nickskype' => $this->helpers->escape_string($user_by_content['NickSkype']),
                    'cache_user_by_fullname' => $this->helpers->escape_string($user_by_content['Name']),
                    'cache_user_by_class' => isset($user_by_content['Class'])?$user_by_content['Class']:'',
                    'cache_user_by_content' => $this->helpers->json_encode_then_escape($user_by_content),
                    
                    'cache_entry_id' => $entry['entry_id'],
                    'cache_entry_uuid' => $entry['entry_uuid'],
                    'cache_entry_public' => $entry['entry_public'],
                    'cache_entry_ip' => $entry['entry_ip'],
                    'cache_entry_content' => $this->helpers->json_encode_then_escape($entries),
                    'cache_entry_complete' => $entry['entry_complete'],
                    'cache_entry_t' => $pps,
                    'cache_entry_hs' => $percent,
                    'cache_entry_tg' => $admin_percent,
                    'cache_entry_sys' => 0,
                    'cache_entry_sys_t' => 0,
                    'cache_form_nb_id' => $form_nb['form_id'],
                    'cache_form_nb_uuid' => $form_nb['form_uuid'],
                    'cache_form_sc_uuid' => $form_nb_content['FormScoreUrl'],
                    'cache_form_nb_type' => $form_nb['form_type'],
                    'cache_form_nb_name' => $this->helpers->escape_string($form_nb_content['Name']),
                    'cache_form_nb_status' => $form_nb['form_status'],
                    'cache_form_nb_theme_id' => $form_nb['theme_id'],
                    'cache_form_nb_content' => $this->helpers->json_encode_then_escape($form_nb_content),
                    'cache_form_nb_password' => $form_nb['form_password'],
                    'cache_content' => $this->helpers->json_encode_then_escape($cache_content)
                );
                $nbed_success = $caches_obj->cache_entries_nbed($contents_nbed);
            }
            /*
            * #Insert Cached To DB
            */
        endforeach;
        /*
        * #Caching Entries NBED
        */
        
        $this->json['success'] = true;
        if($nb_success['done'] && $nbed_success['done'] && $sced_success['done'] && $as_success['done'] && $us_success['done']) {
            $this->json['success'] = true;
        }
        $this->json['response'] = array(
            'as' => $as_success,
            'nb' => $nb_success,
            'sced' => $sced_success,
            'nbed' => $sced_success,
            'us' => $us_success
        );
    }
    function common_rebuildCache($user_id) {
        $user_weights = array();
        $users_obj = new Models_UsersModel($this->registry);
        $caches_obj = new Models_CachedModel($this->registry);
        $groups_obj = new Models_GroupsModel($this->registry);
        $weight_results = $caches_obj->getCachedEntries('`cache_stats_hs`,`cache_user_id`','users');
        if($weight_results) foreach($weight_results as $weight_result) {
            $user_weights[$weight_result['cache_user_id']] = $weight_result['cache_stats_hs'];
        }
        $user = $users_obj->getUser("`user_id` = '$user_id'");
        $group_meta_result = $grouped_users = $groups_obj->getGroupsMeta();
        $user_in_group = $group_names = array();
        if($group_meta_result) foreach($group_meta_result as $group_meta) {
            if($group_meta['user_id'] == $user_id) {
                $group_id = $group_meta['group_id'];
                /*
                * Groups
                */
                foreach($grouped_users as $grouped_user) {
                    if($grouped_user['group_id'] == $group_id) {
                        if($grouped_user['user_id'] != '0') {
                            $user_in_group[] = $grouped_user['user_id'];
                        }
                    }
                }
            }
        }
        $this->rebuildCache($user,$user_in_group,$user_weights,$users_obj,$caches_obj,$groups_obj);
    }
    function rebuildCacheFor($user_id, $obj = '') {
        $this->common_rebuildCache($user_id);
        $obj->setAllowNext(true);
    }
    function caches_setCaches($args) {
        $this->common_rebuildCache($args['user_id']);
    }
    function caches_deleteNullCached($args) {
        $caches_obj = new Models_CachedModel($this->registry);
        extract($args);
        $this->json['success'] = $caches_obj->deleteEntriesCachedNotInGroup();
    }
    function caches_emptyCached($args) {
        $caches_obj = new Models_CachedModel($this->registry);
        extract($args);
        $this->json['success'] = $caches_obj->emptyCached();
    }
    /* ---------------------------------------------------------------------------- */
    /* ------------------------ DASHBOARD ----------------------------- */
    /* ---------------------------------------------------------------------------- */
    function dashboard_update($args) {
        $req = $this->registry->req;
        $user_id = $this->helpers->getUserByParam();
        if(!empty($user_id)) {
            $errors = $this->helpers->validateUpdateInfo($req);
            if(empty($errors)) {
                $nickh2d = $req['nickh2d'];
                $nickskype = htmlspecialchars(stripslashes($req['nickskype']));
                $fullname = $req['fullname'];
                $user_content = array(
                    'Name' => $fullname,
                    'NickSkype' => $nickskype,
                    'NickH2d' => $nickh2d
                );
                $users_obj = new Models_UsersModel($this->registry);
                $user_content = $this->helpers->json_encode_then_escape($user_content);
                if($users_obj->updateUser("`user_content` = '$user_content'","`user_id` = '$user_id'")) {
                    $this->json['success'] = true;
                } else {
                    $this->json['response']['message'] = 'Có lỗi khi thay đổi dữ liệu.';
                }
            } else {
                $this->json['response']['message'] = 'Dữ liệu nhập vào sai!';
            }
        }
    }
    function dashboard_changepass($args) {
        $req = $this->registry->req;
        $user_id = $this->helpers->getUserByParam();
        if(!empty($user_id)) {
            $password = $req['password'];
            $confirm = $req['confirm'];
            $isValid = $this->helpers->check_pass($password,$confirm);
            if(($isValid)) {
                $users_obj = new Models_UsersModel($this->registry);
                $password = md5(htmlspecialchars($this->helpers->escape_string($password)));
                if($users_obj->updateUser("`password` = '$password'","`user_id` = '$user_id'")) {
                    $this->json['success'] = 'true';
                } else {
                    $this->json['response']['message'] = 'Có lỗi khi thay đổi password.';
                }
            } else {
                $this->json['response']['message'] = 'Dữ liệu nhập vào sai!';
            }
        }
    }
    function load_pagination($total_rows, $limited, $paged) {
        $response_paged = '';
        ob_start();
        $total_paged = ceil($total_rows/$limited);
        ?>
        <span class="dgNav">
        <a class="firstPage <?php
            if($paged == 1) {
                echo 'disable';
            } else {
                echo 'show';
            }
        ?>" onclick="goToPage(1,this);return false;" href="#" title="First Page">«</a>
        <a class="prevPage <?php
            if($paged == 1) {
                echo 'disable';
            } else {
                echo 'show';
            }
        ?>" onclick="goToPage(<?php echo (($paged-1) > 1)?$paged-1:'1'; ?>,this);return false;" href="#" title="Previous Page">‹</a>
        <span class="dgInfo">
            <span class="startEntry var"><?php echo $paged; ?></span> of <span class="totalEntries var"><?php echo $total_paged; ?></span></span>
        <a class="nextPage <?php
            if(($paged) >= $total_paged) {
                echo 'disable';
            } else {
                echo 'show';
            }
        ?>" onclick="goToPage(<?php echo (($paged+1) <= $total_paged)?$paged+1:$total_paged; ?>,this);return false;" href="#" rel="<?php echo ($paged+1); ?>" title="Next Page">›</a>
        <a class="lastPage <?php
            if(($paged) >= $total_paged) {
                echo 'disable';
            } else {
                echo 'show';
            }
        ?>" onclick="goToPage(<?php echo $total_paged; ?>,this);return false;" href="#" rel="<?php echo ($total_paged); ?>" title="Last Page">»</a>
        </span>
<?php
        $response_paged = ob_get_contents();
        ob_clean();
        return $response_paged;
    }
    function load_section($section,$args) {
        $form_limited = Controllers_dashboardController::$form_limit;
        $siteUrl = $this->helpers->getUrl();
        $section2 = $section;
        extract($args);
        $section = $section2;
        $user_id = $this->helpers->getUserByAdmin();
        if(empty($user_id)) return;
        if($sort_by == 't') {
            $sort_by = 'cache_entry_t';
        }elseif($sort_by == 'hs') {
            $sort_by = 'cache_entry_hs';
        }elseif($sort_by == 'tg') {
            $sort_by = 'cache_entry_tg';
        }elseif($sort_by == 'dc') {
            $sort_by = 'cache_entry_id';
        }elseif($sort_by == 'sys') {
            $sort_by = 'cache_entry_sys';
        }else {
            $sort_by = 'cache_entry_id';
        }
        $response_html = $response_paged = '';
        $cached_obj = new Models_CachedModel($this->registry);
        if(!is_numeric($paged) || $paged <= 0) $paged = 1;
        $_paged = ($paged-1)*$form_limited;
        $entries_result = $cached_obj->getCachedEntries('SQL_CALC_FOUND_ROWS *',$section,"`cache_user_id` = '$user_id'",$sort_by,$sort_dir,"$_paged,$form_limited");
        if(!$entries_result) {
            $entries_result = $cached_obj->getCachedEntries('SQL_CALC_FOUND_ROWS *',$section,"`cache_user_id` = '$user_id'",$sort_by,$sort_dir,"0,$form_limited");
            $paged = 1;
        }
        
        $entries_result_rows = $cached_obj->getCalcNumRows();
        // HTML
        if($entries_result) {
            ob_start();
            foreach($entries_result as $entry) :
                ?>
        		<tr>
        			<td>
                        <?php
                        if($section == 'as') {
                        ?>
                            <?php
                            echo $entry['cache_form_nb_name'];
                            ?>
                        <?php  
                        } elseif($section == 'nb') {
                        ?>
                            <?php echo $entry['cache_form_nb_name']; ?>
                        <?php 
                        } elseif($section == 'sced') {
                        ?>
                            <?php
                            echo '<strong>Bài Làm Của:</strong><br /> '; echo isset($entry['cache_user_of_fullname'])?$entry['cache_user_of_fullname']:'';
                            if($this->options['show_nickh2d']) {
                            echo '<br /><strong>Nick H2d:</strong><br /> '; 
                            echo '<a target="_blank" href="http://hoctudau.com/h2d/index.php?qa=user&qa_1=';echo isset($entry['cache_user_of_nickh2d'])?$entry['cache_user_of_nickh2d']:''; echo '">';echo isset($entry['cache_user_of_nickh2d'])?$entry['cache_user_of_nickh2d']:'';echo '</a>';
                            }
                            ?>
                        <?php     
                        } elseif($section == 'nbed') {
                        ?>
                            <?php
                            echo '<strong>Bài Làm Của:</strong><br /> '; echo isset($entry['cache_user_by_fullname'])?$entry['cache_user_by_fullname']:'';
                            if($this->options['show_nickh2d']) {
                            echo '<br /><strong>Nick H2d:</strong><br /> ';
                            echo '<a target="_blank" href="http://hoctudau.com/h2d/index.php?qa=user&qa_1=';echo isset($entry['cache_user_by_nickh2d'])?$entry['cache_user_by_nickh2d']:''; echo '">';echo isset($entry['cache_user_by_nickh2d'])?$entry['cache_user_by_nickh2d']:'';echo '</a>';
                            }
                            ?>
                        <?php     
                        }
                        ?>
        				
        			</td>
                    <?php
                    if($section == 'as') {
                    ?>
                        <td class="notranslate">
                			<a class="summary" rel="<?php echo $entry['cache_entry_uuid']; ?>" target="_blank" href="<?php echo $siteUrl; ?>summary/<?php echo $entry['cache_entry_uuid']; ?>"><strong>System: </strong><span class="<?php echo ($entry['cache_entry_sys']>80)?'markred':'unmark'; ?>"><?php echo $entry['cache_entry_sys']; ?>%</span></a>
                            <?php
                            $cache_content = $this->helpers->json_decode_to_array_not_in_mysql($entry['cache_content']);
                            foreach($cache_content['excerpts'] as $exerpt) {
                                echo '<div class="excerpts">'.$exerpt['value'] . '<div class="preview">&nbsp;</div></div>';
                            }
                            ?>
                		</td>
                    <?php  
                    } elseif($section == 'nb') {
                    ?>
                        <td class="notranslate">
    						<a target="_blank" href="<?php echo $siteUrl; ?>forms/<?php echo $entry['cache_form_sc_uuid']; ?>/<?php echo $entry['cache_entry_uuid']; ?>">Link Chấm Điểm</a>
    					</td>
                        <td class="notranslate">
    						<a title="xem tổng điểm" class="summary" rel="<?php echo $entry['cache_entry_uuid']; ?>" target="_blank" href="<?php echo $siteUrl; ?>summary/<?php echo $entry['cache_entry_uuid']; ?>"><strong>T: </strong><?php echo $entry['cache_entry_t']; ?> <strong>HS: </strong><span class="<?php echo ($entry['cache_entry_hs']>80)?'markred':'unmark'; ?>"><?php echo $entry['cache_entry_hs']; ?>%</span> | <strong>TG: </strong><span class="<?php echo ($entry['cache_entry_tg']>80)?'markred':'unmark'; ?>"><?php echo $entry['cache_entry_tg']; ?>%</span></a>
                            
                        <?php 
                        $cache_content = $this->helpers->json_decode_to_array_not_in_mysql($entry['cache_content']);
                        foreach($cache_content['excerpts'] as $exerpt) {
                            echo '<div class="excerpts">'.$exerpt['value'] . '<div class="preview">&nbsp;</div></div>';
                        }
                        ?>
    					</td>
                    <?php 
                    } elseif($section == 'sced') {
                    ?>
                        <td class="notranslate">
                			<a class="summary" rel="<?php echo $entry['cache_entry_uuid']; ?>" target="_blank" href="<?php echo $siteUrl; ?>summary/<?php echo $entry['cache_entry_uuid']; ?>"><strong>T: </strong><?php echo $entry['cache_entry_t']; ?> <strong>HS: </strong><span class="<?php echo ($entry['cache_entry_hs']>80)?'markred':'unmark'; ?>"><?php echo $entry['cache_entry_hs']; ?>%</span> | <strong>TG: </strong><span class="<?php echo ($entry['cache_entry_tg']>80)?'markred':'unmark'; ?>"><?php echo $entry['cache_entry_tg']; ?>%</span></a>
                		</td>
                    <?php     
                    } elseif($section == 'nbed') {
                    ?>
                        <td class="notranslate">
                            <?php
                            echo '<strong>Form: </strong>'; echo isset($entry['cache_form_nb_name'])?$entry['cache_form_nb_name']:'';
                            ?><br />
            				<a target="_blank" href="<?php echo $siteUrl; ?>forms/<?php echo $entry['cache_form_sc_uuid']; ?>/<?php echo $entry['cache_entry_uuid']; ?>">Link Chấm Điểm</a>
            			</td>
                        <td class="notranslate">
            				<a  class="summary" rel="<?php echo $entry['cache_entry_uuid']; ?>" target="_blank" href="<?php echo $siteUrl; ?>summary/<?php echo $entry['cache_entry_uuid']; ?>"><strong>T: </strong><?php echo $entry['cache_entry_t']; ?> <strong>HS: </strong><span class="<?php echo ($entry['cache_entry_hs']>80)?'markred':'unmark'; ?>"><?php echo $entry['cache_entry_hs']; ?>%</span> | <strong>TG: </strong><span class="<?php echo ($entry['cache_entry_tg']>80)?'markred':'unmark'; ?>"><?php echo $entry['cache_entry_tg']; ?>%</span></a>
                            <?php
                            $cache_content = $this->helpers->json_decode_to_array_not_in_mysql($entry['cache_content']);
                            foreach($cache_content['excerpts'] as $exerpt) {
                                echo '<div class="excerpts">'.$exerpt['value'] . '<div class="preview">&nbsp;</div></div>';
                            }
                            ?>
            			</td>
                    <?php     
                    }
                    ?>
        			
        		</tr>
                <?php
                $response_html .= ob_get_contents();
                ob_clean();
            endforeach;
            ob_end_clean();
            // END HTML
            // PAGED
            $response_paged = $this->load_pagination($entries_result_rows, $form_limited, $paged);
            // END PAGED
            $this->json['success'] = true;
        }
        $this->json['response']['html'] = $response_html;
        $this->json['response']['paged'] = $response_paged;
    }
    function dashboard_loadSection($args) {
        extract($args);
        $section = $this->helpers->escape_string($section);
        if(!empty($section)) {
            if(in_array($section, array('as','nb','sced','nbed'))) {
                $this->load_section($section,$args);
            }
        }
    }
    function dashboard_get_as($args) {
        $this->load_section('as',$args);
    }
    function dashboard_get_nb($args) {
        $this->load_section('nb',$args);
    }
    function dashboard_get_sced($args) {
        $this->load_section('sced',$args);
    }
    function dashboard_get_nbed($args) {
        $this->load_section('nbed',$args);
    }
    
    /* ---------------------------------------------------------------------------- */
    /* ------------------------ ENTRIES ----------------------------- */
    /* ---------------------------------------------------------------------------- */
    function entries_bulkDelete($args) {
        extract($args);
        $forms_obj = new Models_FormsModel($this->registry);
        $routers = $this->registry->router->getRouters();
        $formId = $routers['id'];
        if(!empty($formId)) {
            if($form_result = $forms_obj->getForm($formId,'')) {
                $entries_obj = new Models_EntriesModel($this->registry);
                $cached_obj = new Models_CachedModel($this->registry);
                $form_id = $form_result['form_id'];
                $entries_result = $entries_obj->getEntriesMeta("`form_id` = '$form_id'");
                $entry_ids = array();
                if($entries_result) foreach($entries_result as $entry) {
                    $entry_ids[] = '\''.$entry['entry_id'].'\'';
                }
                $entry_ids = implode(',',$entry_ids);
                if(empty($entry_ids)) $entry_ids = '\'0\'';
                if($entries_obj->deleteEntries("`entry_id` IN ($entry_ids) OR  `entry_of` IN ($entry_ids)")) {
                    $entries_obj->deleteEntriesMeta("`form_id` = '$form_id'");
                    $cached_obj->deleteCachedEntries("`cache_entry_id` IN ($entry_ids)");
                }
                
            }
            if(!empty($this->registry->mysqli->error)) {
                echo $this->registry->mysqli->error;
                exit;
            }
            $this->json['success'] = true;
        }
    }
    function entries_delete($args) {
        extract($args);
        $_entry = $this->helpers->json_decode_to_array($entry);
        $entryId = $_entry['EntryId'];
        if(!empty($entryId)) {
            $entries_obj = new Models_EntriesModel($this->registry);
            $cached_obj = new Models_CachedModel($this->registry);
            
            if($entries_obj->deleteEntries("`entry_id` = '$entryId' OR `entry_of` = '$entryId'")) {
                $entries_obj->deleteEntriesMeta("`entry_id` = '$entryId'");
                $cached_obj->deleteCachedEntries("`cache_entry_id` = '$entryId'");
            }
            if(!empty($this->registry->mysqli->error)) {
                echo $this->registry->mysqli->error;
                exit;
            }
            $this->json['success'] = true;
        }
    }
    function entries_fetch($args) {
        extract($args);
        $routers = $this->registry->router->getRouters();
        $formId = $routers['id'];
        if(!empty($formId)) {
            $forms_obj = new Models_FormsModel($this->registry);
            $formId = $this->helpers->escape_string($formId);
            if(!$result = $forms_obj->getForm($formId,'')){
                return;
            }
        } else {
            return;
        }
        
        $entries_obj = new Models_EntriesModel($this->registry);
        $form_id = $result['form_id'];
        $form = ($result['form_content']);
        $_form = $this->helpers->json_decode_to_array_not_in_mysql($form);
        if($order == 'EntryId') $order = 'entry_id';
        if($order == 'DateCreated') $order = 'entry_create';
        $entty_count = $entries_obj->getEntriesNumber($form_id);
        $_entry = $entries_obj->fetchAll("SELECT * FROM {$this->db_prefix}entries_meta a, {$this->db_prefix}entries b WHERE a.`form_id` = '$form_id' AND a.`entry_id` = b.`entry_id` ORDER BY b.`$order` $direction LIMIT $page,6 ");
        $_form['EntryCount'] = $entty_count;
        if(isset($_form['Entries'])) {
            $i = 0;
            if($_entry) foreach($_entry as $entry) {
                $_form['Entries'][$i] = array(
                    'EntryId' => $entry['entry_id'],
                    'HitId' => '',
                    'AssignmentId' => '',
                    'Environment' => '',
                    'Status' => '',
                    'PurchaseTotal' => '',
                    'Currency' => '',
                    'TransactionId' => '',
                    'MerchantType' => '',
                    'DateCreated' => $entry['entry_create'],
                    'CreatedBy' => 'datphan',
                    'DateUpdated' => '',
                    'UpdatedBy' => '',
                    'IP' => $entry['entry_ip'],
                    'LastPage' => '1',
                    'CompleteSubmission' => '1'
                );
                
                $posts = ($entry['entry_content']);
                // SIGN: $posts = json_decode_to_array($posts); Once again and its not work? :(
                $posts = $this->helpers->json_decode_to_array_not_in_mysql($posts);
                foreach($posts as $k => $post) {
                    if(substr($k,0,5) == 'Field' && !strpos($k,'Other') && !empty($post)) {
                        $_form['Entries'][$i][$k] = $post;
                    }
                }
                $i++;
            }
            $_form['Entries'] = array_values($_form['Entries']);
        }
        $this->json['response']['response'] = json_encode($_form);
    }
    function entries_display() {
        
    }
    function entries_loadEntry($args) {
        extract($args);
        $forms_c_obj = new Controllers_formsController($this->registry);
        $forms_obj = new Models_FormsModel($this->registry);
        $routers = $this->registry->router->getRouters();
        $formId = $this->helpers->escape_string($routers['id']);
        $result = $forms_obj->getForm($formId,'','form_uuid','');
        $_form = $this->helpers->json_decode_to_array($result['form_content']);
        $fields = $_form['Fields'];
        $html = $forms_c_obj->getDisplayFields($fields,array());
        
        ob_start();
        ?>
        <div class="ltr">
        	<form id="entry_form" class="hoctudau topLabel " onsubmit="submitForm(this);" autocomplete="off" enctype="multipart/form-data" method="post" action="<?php echo $siteUrl; ?>entries/submit/index.php?formId=<?php echo $formId; ?>" target="submit_form_here"
        	novalidate>
        		<div class="info">
        			<div class="var">
        				#
        				<b>
        					<?php echo $result['form_id']; ?>
        				</b>
        			</div>
        			<h2 class="notranslate">
        				<?php echo $_form['Name']; ?>
        			</h2>
        			<div class="notranslate">
        				<?php echo $_form['Description']; ?>
        			</div>
        		</div>
        		<ul>
        			<?php echo $html; ?>
        			<li class="buttons">
        				<input type="hidden" name="currentPage" id="currentPage" value="Q5HuR5uec5GLEba5SfarJEnlRSWxRPx61FTrM93nwuslashMU=" />
        				<input id="saveForm" name="saveForm" class="btTxt submit" type="submit" tabindex="35" value="Save Changes" />
        			</li>
        			<li style="display:none">
        				<input type="hidden" name="EntryId" id="EntryId" value="NOt7p0D69MBcRS61N5mOdaGh2qKuriCJFlhEtRAbMgE=" />
        			</li>
        		</ul>
        	</form>
        </div>
        <?php
        $html = ob_get_contents();
        ob_clean();
        $this->json['response']['markup'] = $html;
    }
    function entries_readOnly($args) {
        extract($args);
        $html = '';
        $siteUrl = $this->helpers->getUrl();
        $cached_obj = new Controllers_cachesController($this->registry);
        $forms_obj = new Models_FormsModel($this->registry);
        $entries_obj = new Models_EntriesModel($this->registry);
        $users_obj = new Models_UsersModel($this->registry);
        $entryId = $this->helpers->escape_string($entryId);
        $form = $forms_obj->getForm($formId,'');
        $_form = ($form['form_content']);
        $_form = $this->helpers->json_decode_to_array($_form);
        $form_id = $form['form_id'];
        $fields = $_form['Fields'];
        $formType = $_form['Formtype'];
        $entry_content = $entries_obj->getEntry('',$entryId);
        $user_id = $entry_content['entry_user'];
        $user = $users_obj->getUser("`user_id` = '$user_id'");
        if($formType == 'autoscore') {
            $html = $cached_obj->getFormASDisplayResult($form,$entry_content,$user, true, false, true);
        } else {
            $html = $cached_obj->getFormNBDisplayResult($form,$entry_content,$user, false, true);
        }
        $datetime = explode(' ',$entry_content['entry_create']);
        $date = $datetime[0];
        $time = $datetime[1];
        ob_start();
        ?>
            <table cellspacing="0" style="width:100%; padding-bottom: 10px;font-family:'Lucida Grande','Lucida Sans Unicode', Tahoma, sans-serif;">
        	<tr>
        		<td class="h2" style="width:90%;font-weight:normal;line-height:30px;font-size:20px;clear:left;color:#000;padding:0 10px 7px 0">
        			<?php echo $_form['Name']; ?>
        		</td>
        		<td style="padding:0 0 7px 0">
        			<div class="var" style="display:block;float:right;font-style:normal;line-height:30px;font-size:20px;padding:0 0 0 10px;color:#999;">
        				#
        				<b style="color:#333;">
        					<?php echo $entry_content['entry_id']; ?>
        				</b>
        				<?php if(!$entry_content['entry_complete']): echo '<p><b style="color:red;font-size:12px;">UnComplete</b></p>'; else: echo '<p><b style="color:green;font-size:12px;">Complete</b></p>'; endif; ?>
        			</div>
        		</td>
        	</tr>
            <?php
            $usered = false;
            if($entry_content['entry_type'] == 'BT') {
                $usered_id = $entry_content['entry_user'];
                if($usered_id !=0) $usered = true;
            }
            if($entry_content['entry_type'] != 'BT' || $usered) {
                $user_id = $entry_content['entry_user'];
                $user_content = $this->helpers->json_decode_to_array($user['user_content']);
                if($entry_content['entry_type'] == 'NB' || $usered) :
            ?>
            <tr>
                <td>Bài Nộp Của: <?php echo $user_content['Name']; ?><br />
                <?php
                if($this->options['show_nickh2d']) {
                ?>
                Nick H2d: 
                <a target="_blank" href="http://hoctudau.com/h2d/index.php?qa=user&qa_1=<?php echo $user_content['NickH2d']; ?>"><?php echo $user_content['NickH2d']; ?></a>
                <?php
                }
                ?><br />
                <?php
                if($this->options['show_nickskype']) {
                ?>
                Nick Skype: 
        <a href="skype:<?php echo $user_content['NickSkype']; ?>?chat">
                <?php echo $user_content['NickSkype']; ?></a>
                <?php
                }
                ?>
                </td>
                <td style="padding:0 0 7px 0"><a target="_blank" href="<?php echo $siteUrl; ?>forms/<?php echo $_form['FormScoreUrl']; ?>/<?php echo $entry_content['entry_uuid']; ?>">Chấm Điểm</a></td>
            </tr>
            <?php 
                elseif($entry_content['entry_type'] == 'SC') :
                $entry_of = $entry_content['entry_of'];
                if($entry_nb_result = $entries_obj->getEntry($entry_of,'','entry_uuid','',"AND `entry_type` = 'NB'")) {
                    $user_nb_id = $entry_nb_result['entry_user'];
                    $user_nb = $users_obj->getUser("`user_id` = '$user_nb_id'");
                    $user_nb_content = $this->helpers->json_decode_to_array($user_nb['user_content']);
                }
            ?>
            <tr>
                <td>
                <div style="float: left; width: 45%">
                Bài Chấm Của: <?php echo $user_content['Name']; ?><br />
                <?php
                if($this->options['show_nickh2d']) {
                ?>
                Nick H2d: <a target="_blank" href="http://hoctudau.com/h2d/index.php?qa=user&qa_1=<?php echo $user_content['NickH2d']; ?>"><?php echo $user_content['NickH2d']; ?></a>
                <?php
                }
                ?>
                <br />
                <?php
                if($this->options['show_nickskype']) {
                ?>
                Nick Skype: 
        <a href="skype:<?php echo $user_content['NickSkype']; ?>?chat">
                <?php echo $user_content['NickSkype']; ?></a>
                <?php
                }
                ?>
                </div>
                <div style="float: left; width: 10%;">
                    <strong>Cho</strong>
                </div>
                <div style="float: right; width: 45%;">
                <?php if($entry_nb_result) { ?>
                
                Bài Nộp Của: <?php echo $user_nb_content['Name']; ?><br />
                <?php
                if($this->options['show_nickh2d']) {
                ?>
                Nick H2d: <a target="_blank" href="http://hoctudau.com/h2d/index.php?qa=user&qa_1=<?php echo $user_nb_content['NickH2d']; ?>"><?php echo $user_nb_content['NickH2d']; ?></a>
                <?php
                }
                ?>
                <br />
                <?php
                if($this->options['show_nickskype']) {
                ?>
                Nick Skype: 
        <a href="skype:<?php echo $user_nb_content['NickSkype']; ?>?chat">
                <?php echo $user_nb_content['NickSkype']; ?></a>
                
                <?php } } ?>
                </div>
                <div style="clear: both;"></div>
                </td>
                <td><a target="_blank" href="<?php echo $siteUrl; ?>summary/<?php echo $entry_content['entry_of']; ?>">Xem Tổng Điểm</a></td>
            </tr>
            <?php
                elseif($entry_content['entry_type'] == 'AS') :
            ?>
            <tr>
                <td>Bài Nộp Của: <?php echo $user_content['Name']; ?><br />
                <?php
                if($this->options['show_nickh2d']) {
                ?>
                Nick H2d: 
                <a target="_blank" href="http://hoctudau.com/h2d/index.php?qa=user&qa_1=<?php echo $user_content['NickH2d']; ?>"><?php echo $user_content['NickH2d']; ?></a>
                <?php
                }
                ?><br />
                <?php
                if($this->options['show_nickskype']) {
                ?>
                Nick Skype: 
        <a href="skype:<?php echo $user_content['NickSkype']; ?>?chat">
                <?php echo $user_content['NickSkype']; ?></a>
                <?php
                }
                ?>
                </td>
                <td style="padding:0 0 7px 0"><a target="_blank" href="<?php echo $siteUrl; ?>summary/<?php echo $entry_content['entry_uuid']; ?>">Xem Điểm</a></td>
            </tr>
            <?php
            endif;    
            } 
            
            ?>
        </table>
        <?php
        echo $html;
        ?>
        <table id="entryInfo" cellspacing="0" style="width:100%;border-top:25px solid #fff; border-bottom:25px solid #fff; font-family:'Lucida Grande','Lucida Sans Unicode', Tahoma, sans-serif;">
        	<tr>
        		<td class="createTD" style="width:33%; padding-right:12px">
        			<div class="created" style="border:1px solid #eee;background:#f5f5f5; text-align:center;">
        				<span style="display:block;font-size:9px;padding:10px 0 0 0;">
        					Created
        				</span>
        				<div id="_dc" style="font-size:13px;font-style:normal;font-weight:bold;padding:0 0 2px 0;color:#222;" class="notranslate">
        					<?php echo $date; ?>
        				</div>
        				<i style="display:block;font-style:normal;font-size:9px;padding:0 0 10px 0;color:#222;" class="notranslate">
        					<?php echo $time; ?>
        				</i>
        				<cite id="_cb" style="background:#dedede;border-top:1px solid #eee;display:block;
        				height:17px;line-height:17px;font-size:9px;font-weight:bold;font-style:normal;text-transform: uppercase;" class="notranslate">
        					...
        				</cite>
        			</div>
        		</td>
        		<td class="ip" style="border:1px solid #eee;background:#f5f5f5;padding:10px 0 0 0; text-align:center;">
        			<div id="_ip" style="font-size:13px;font-style:normal;font-weight:bold;padding:0 0 2px 0;color:#222;">
        				<a href="http://ip-lookup.net/index.php?ip=<?php echo $entry_content['entry_ip']; ?>" target="_blank" class="notranslate"><?php echo $entry_content['entry_ip']; ?></a>
        			</div>
        			<i style="display:block;font-style:normal;font-size:9px;padding:0 0 10px 0;color:#222;">
        				IP Address
        			</i>
        		</td>
        	</tr>
        </table>
        <?php
        $html = ob_get_contents();
        ob_clean();
        $datetime = explode(' ',$entry_content['entry_create']);
        $this->json['response']['response'] = $html;
        $this->json['success'] = true;
    }
    
    /* ---------------------------------------------------------------------------- */
    /* ------------------------ GROUPS ----------------------------- */
    /* ---------------------------------------------------------------------------- */
    function groups_addgroup($args) {
        extract($args);
        $groups_obj = new Models_GroupsModel($this->registry);
        if($groups_obj->insertGroup('`group_name`',"'$group_name'")) {
            $this->json = array(
                'success' => true
            );
        }
    }
    function groups_updategroup($args) {
        extract($args);
        $groups_obj = new Models_GroupsModel($this->registry);
        if($groups_obj->updateGroup("`group_name` = '$group_name'","`group_id` = '$group_id'")) {
            $this->json = array(
                'success' => true
            );
        }
    }
    
    /* ---------------------------------------------------------------------------- */
    /* ------------------------ NOTIFICATIONS ----------------------------- */
    /* ---------------------------------------------------------------------------- */
    function notifications_inbox($args) {
        extract($args);
        $routers = $this->registry->router->getRouters();
        $formId = $this->helpers->escape_string($routers['id']);
        $form = $this->helpers->escape_string($form);
        $forms_obj = new Models_FormsModel($this->registry);
        if($forms_obj->getForm('',$formId)) {
            $forms_obj->updateForm("`form_notify` = '$form'","`form_id` = '$formId'");
        }
        if(!$forms_obj->getErrors()) {
            echo $forms_obj->getErrors();
            return;
        } else {
           $this->json['success'] = 'true';
           $this->json['response']['saved'] = 'true'; 
        }
    }
    
    /* ---------------------------------------------------------------------------- */
    /* ------------------------ ACCOUNTS ----------------------------- */
    /* ---------------------------------------------------------------------------- */
    function accounts_unlockSubject($args) {
        $posts = isset($_POST)?$_POST:array();
        $subjects_obj = new Models_SubjectsModel($this->registry);
        $users_obj = new Models_UsersModel($this->registry);
        
        $subject_id = isset($posts['subject_id'])?$posts['subject_id']:'';
        $subject_id = $this->helpers->escape_string($subject_id);
        if(empty($subject_id)) {
            return false;
        }
        $subject_result = $subjects_obj->getSubject("`subject_id` = '$subject_id'");
        if(!$subject_result) {
            return false;
        }
        $user_id = isset($posts['user_id'])?$posts['user_id']:'';
        $user_id = $this->helpers->escape_string($user_id);

        if(!$users_obj->getUser("`user_id` = '$user_id'")) {
            return;
        }
        if($subjects_obj->updateSU("`subject_unlocked` = 'Y'","`subject_id` = '$subject_id' AND `user_id` = '$user_id'")) {
            $this->json['success'] = true;
        } else {
            $this->json['response']['message'] = 'Some Error Occured';
        }
    }
    function accounts_lockSubject($args) {
        $subjects_obj = new Models_SubjectsModel($this->registry);
        $users_obj = new Models_UsersModel($this->registry);
        $posts = isset($_POST)?$_POST:array();
        $subject_id = isset($posts['subject_id'])?$posts['subject_id']:'';
        $subject_id = $this->helpers->escape_string($subject_id);
        if(empty($subject_id)) {
            return false;
        }
        $subject_result = $subjects_obj->getSubject("`subject_id` = '$subject_id'");
        if(!$subject_result) {
            return false;
        }
        $user_id = isset($posts['user_id'])?$posts['user_id']:'';
        $user_id = $this->helpers->escape_string($user_id);

        if(!$users_obj->getUser("`user_id` = '$user_id'")) {
            return;
        }
        if($subjects_obj->updateSU("`subject_unlocked` = 'N'","`subject_id` = '$subject_id' AND `user_id` = '$user_id'")) {
            $this->json['success'] = true;
        } else {
            $this->json['response']['message'] = 'Some Error Occured';
        }
    }
    function accounts_search($args) {
        $html = '';
        $siteUrl = $this->helpers->getUrl();
        $req = $this->registry->req;
        $searchString = isset($req['searchString'])?$req['searchString']:'';
        $searchString = $this->helpers->escape_string($searchString);
        $user_obj = new Models_UsersModel($this->registry);
        $groups_obj = new Models_GroupsModel($this->registry);
        $users = $user_obj->getUsers('*',"`username` LIKE '%$searchString%' OR `user_email` LIKE '%$searchString%' OR `user_content` LIKE '%$searchString%'");
        if(!$users) {
            $this->json['response']['html'] = $this->registry->mysqli->error;
            return;
        }
        $user_grouped = $groups_obj->fetchAll("SELECT * FROM {$this->db_prefix}groups_meta a, {$this->db_prefix}groups b WHERE a.`group_id` = b.`group_id`");
        $groups_result = $groups_obj->getGroups();
        $i = -1;
        $json_ = array();
        foreach($users as $user) {
        $i++;
        $user_content = $this->helpers->json_decode_to_array_not_in_mysql($user['user_content']);
        $user_id = $user['user_id'];
        $json_[$user_id]['Name'] = $username = $user['username'];
        $json_[$user_id]['Email'] = $email = $user['user_email'];
        $user_status = $user['user_status'];
        $json_[$user_id]['Fullname'] = $user_content['Name'];
        $json_[$user_id]['NickH2d'] = $user_content['NickH2d'];
        $json_[$user_id]['NickSkype'] = $user_content['NickSkype'];
        $json_[$user_id]['IsActivated'] = ($user['user_status']=='A')?'A:':'D:';
        $su_results = $user_obj->fetchAll("SELECT * FROM {$this->db_prefix}subject_user a, {$this->db_prefix}subjects b WHERE a.`subject_id` = b.`subject_id` AND `user_id` = '$user_id'");
        ob_start();
        ?>
        <li class="<?php echo ($i%2==0)?'even2':'even3'; ?> user_info" id="user_<?php echo $user_id; ?>">
            <div class="col col-1">
                <div>
                    <strong>Account:</strong> <a id="link<?php echo $user_id; ?>" class="view" target="_blank" href="<?php echo $siteUrl; ?>dashboard&user_id=<?php echo $user_id; ?>"><?php echo $username; ?></a>
                </div>
                <div>
                    <strong>Email:</strong> <?php echo $email; ?>
                </div>
                <div>
                    <strong>Tên:</strong> <?php echo $user_content['Name']; ?>
                </div>
                <?php
                if($this->options['show_nickh2d']) {
                ?>
                <div>
                    <strong>Nick H2d:</strong> <a target="_blank" href="http://hoctudau.com/h2d/index.php?qa=user&qa_1=<?php echo $user_content['NickH2d']; ?>"><?php echo $user_content['NickH2d']; ?></a>
                </div>
                <?php
                }
                if($this->options['show_nickh2d']) {
                ?>
                <div>
                    <strong>Nick Skype:</strong> <a href="skype:<?php echo $user_content['NickSkype']; ?>?chat"><?php echo $user_content['NickSkype']; ?></a>
                </div>
                <?php
                }
                ?>
            </div>
            <div class="col col-2">
                <?php
                if($su_results) foreach($su_results as $su_result) {
                ?>
                <p>
                    <a id="s_<?php echo $su_result['subject_id']; ?>" title="<?php echo $su_result['subject_name']; ?>" class="subject_title" href="#"><?php echo $su_result['subject_name']; ?></a>
                    <?php
                    if($su_result['subject_unlocked'] == 'N') {
                    ?>
                    <a href="#" title="Click to unlock." class="button unlock" onclick="unlockSubject(this); return false;">UNLOCK</a>
                    <?php
                    } else {
                    ?>
                    <a href="#" title="Click to lock." class="button lock" onclick="lockSubject(this); return false;">LOCKED</a>
                    <?php
                    }
                    ?>
                </p>
                <?php
                }
                ?>
                &nbsp;
            </div>
            <div class="col col-3">
                &nbsp;
            </div>
            <div class="col col-4">
                <div class="group">
                    <form action="#">
                    <ul>
                        <?php
                        $dupl_group_id = array();
                        foreach($user_grouped as $user_group) {
                            if($user_group['user_id'] == $user_id) {
                                $dupl_group_id[] = $user_group['group_id'];
                                ?>
                                <li id="group_<?php echo $user_group['group_id']; ?>">
                                <?php
                                    echo '<strong>'.$user_group['group_name'].'</strong>';
                                ?>
                                
                                <select class="groupname" name="groupname">
                                    <?php
                                    foreach($groups_result as $group) {
                                    if(in_array($group['group_id'],$dupl_group_id)) continue;
                                    
                                    ?>
                                    <option value="<?php echo $group['group_id']; ?>"><?php echo $group['group_name']; ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                    <br /><br />
                                    <?php
                                    
                                    ?>
                                <a style="color: #221989;" onclick="changeGroup(this); return false;" class="addgroup button" href="#">Change Group</a>&nbsp;&nbsp;<a style="color: #FF2A2A;" onclick="deleteGroup(this); return false;" class="button" href="#">Delete Group</a>
                                <?php
                                ?>
                                
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                    <div>
                    <select>
                        <?php
                        foreach($groups_result as $group) {
                            if(in_array($group['group_id'],$dupl_group_id)) continue;
                            ?>
                            <option value="<?php echo $group['group_id']; ?>"><?php echo $group['group_name']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <a onclick="addGroup(this); return false;" class="addgroup button" href="#">Add Group</a>
                    </div>
                    </form>
                </div>
            </div>
            <div class="col col-5">
                <strong><a class="button" rel="<?php echo $user_id; ?>" onclick="deleteAccount(this); return false;" href="#" style="color: red;">Delete</a></strong><br /><br />
                <?php if($user_status == 'A'): ?><a class="button" rel="<?php echo $user_id; ?>" onclick="disableAccount(this); return false;" style="color: green;" href="#">Activated</a><?php else: ?><a class="button" href="#" rel="<?php echo $user_id; ?>" onclick="activeAccount(this); return false;" style="color: red;">Disabled</a><?php endif; ?>
            </div>
        </li>
        <?php
        $html .= ob_get_contents();
        ob_clean();
        }
        $this->json['success'] = true;
        $this->json['response']['html'] = $html;
        $this->json['response']['json'] = $json_;
    }
    function accounts_activate($args) {
        extract($args);
        if($user_id != 1) {
            $user_obj = new Models_UsersModel($this->registry);
            if($user_obj->updateUser("`user_status` = 'A'","`user_id` = '$user_id'")) {
                $this->json['success'] = 'true';
            }
        }
    }
    function accounts_disabled($args) {
        extract($args);
        if($user_id != 1) {
            $user_obj = new Models_UsersModel($this->registry);
            if($user_obj->updateUser("`user_status` = 'D'","`user_id` = '$user_id'")) {
                $this->json['success'] = 'true';
            }
        }
    }
    function accounts_delete($args) {
        extract($args);
        if($user_id != 1) {
            $user_obj = new Models_UsersModel($this->registry);
            if($user_obj->deleteUser("`user_id` = '$user_id'")) {
                $entries_obj = new Models_EntriesModel($this->registry);
                $cached_obj = new Models_CachedModel($this->registry);
                if($entries_obj->deleteEntries("`entry_user` = '$user_id'")) {
                    $cached_obj->deleteCachedEntries("`cache_user_id` = '$user_id'");
                }
            }
            if(empty($this->registry->mysqli->error)) $this->json['success'] = 'true';
            else print_r($this->registry->mysqli->error);
        }
    }
    function accounts_addgroup($args) {
        extract($args);
        if($user_id != 1) {
            $groups_obj = new Models_GroupsModel($this->registry);
            $user_group = $groups_obj->getGroupsMeta("`group_id` = '$group_id'");
            if($user_group && count($user_group) < 5) {
                $exist = $groups_obj->getGroupsMeta("`group_id` = '$group_id' AND `user_id` = '$user_id'");
                if(!$exist) {
                    if($groups_obj->insertGroupMeta("`group_id`,`user_id`","'$group_id','$user_id'")) {
                        $this->json['success'] = true;
                    }
                } else {
                    $this->json['response']['message'] = 'Duplicated Group For This User';
                }
            } else {
                $this->json['response']['message'] = 'Maximum User On This Group Allowed';
            }
        } else {
            $this->json['response']['message'] = 'Admin User Should Not Join Group';
        }
    }
    function accounts_changegroup($args) {
        extract($args);
        if($user_id != 1) {
            $groups_obj = new Models_GroupsModel($this->registry);
            $user_group = $groups_obj->getGroupsMeta("`group_id` = '$group_id'");
            if($user_group && count($user_group) <= 5) {
                $exist = $groups_obj->getGroupsMeta("`group_id` = '$group_id' AND `user_id` = '$user_id'");
                if(!$exist) {
                    if($groups_obj->updateGroupMeta("`group_id` = '$group_id'","`user_id` = '$user_id' AND `group_id` = '$old_group_id'",'1')) {
                        $this->json['success'] = true;
                    }
                } else {
                    $this->json['response']['message'] = 'Duplicated Group For This User';
                }
            } else {
                $this->json['response']['message'] = 'Maximum User On This Group Allowed';
            }
        } else {
            $this->json['response']['message'] = 'Admin User Should Not Join Group';
        }
    }
    function accounts_deletegroup($args) {
        extract($args);
        if($user_id != 1) {
            $groups_obj = new Models_GroupsModel($this->registry);
            if($groups_obj->deleteGroupMeta("`user_id` = '$user_id' AND `group_id` = '$group_id'")) {
                $this->json['success'] = true;
            }
        }
    }
    /* ---------------------------------------------------------------------------- */
    /* ------------------------ LOGS ----------------------------- */
    /* ---------------------------------------------------------------------------- */
    
    // comments on /review and /forms
    function review_loadComments($args) {
        $logs_obj = new Models_FormsLogModel($this->registry);
        $comments_obj = new Models_CommentsModel($this->registry);
        $users_obj = new Models_UsersModel($this->registry);
        extract($args);
        
        $log_id = $this->helpers->escape_string($log_id);
        $c_ids = array();
        
        foreach($comment_ids as $comment_id) {
            $c_ids[] = '\'' . $comment_id . '\'';
        }
        $c_ids = implode(',',$c_ids);
        if(empty($c_ids)) {
            $c_ids = '\'0\'';
        }
        $loadCommentNumber = 3;
        $comments = $comments_obj->getCommentsOfLog($log_id, $c_ids, 'comment_create', 'ASC', $loadCommentNumber);
        $comments_count = $comments_obj->getCalcNumRows();
        $html = '';
        $json = array();
        ob_start();
        foreach($comments as $comment) {
            
            $comment_user = $comment['comment_user'];
            $user_result = $users_obj->getUser("`user_id` = '$comment_user'");
            $user = $this->helpers->json_decode_to_array_not_in_mysql($user_result['user_content']);
            $json[] = array(
                'comment_id' => $comment['comment_id'],
                'comment_text' => $comment['comment_content'],
                'comment_user' => $user['NickH2d']
            );
            
        ?>
            <li id="comment_<?php echo $comment['comment_id']; ?>">
                <div class="avatar" href="#">
                    <a class="image" target="_blank" href="https://vi.gravatar.com/site/signup/" title="Change Avatar">
                        <img width="50" src="<?php echo $this->helpers->get_gravatar($user_result['user_email']); ?>" />
                    </a>
                    <div class="info">
                        <?php
                        if($this->options['show_nickh2d']) {
                        ?>
                        <span class="user_h2d"><?php echo $user['NickH2d']; ?></span><br />
                        <?php
                        }
                        ?>
                        <span class="user_fullname"><?php echo $user['Name']; ?></span>
                    </div>
                    
                </div>
                <div class="comment_content">
                    <?php
                    if($this->helpers->isAdmin()) {
                    ?>
                    <div class="actions"><a class="delete" onclick="deleteComment(this); return false;" href="#">Delete</a></div>
                    <?php
                    }
                    ?>
                    <p><?php echo nl2br($comment['comment_content']); ?></p>
                </div>
            </li>
        <?php
        }
        $html = ob_get_contents();
        ob_end_clean();
        $this->json['response']['html'] = $html;
        $this->json['response']['json'] = $json;
        $this->json['response']['over'] = $comments_count - count($comments);
        if(count($comments) < $loadCommentNumber) $this->json['response']['over'] = false;
        $this->json['success'] = true;
    }
    function review_addComment($args) {
        $args = $this->helpers->unescape_string($args);
        $logs_obj = new Models_FormsLogModel($this->registry);
        $users_obj = new Models_UsersModel($this->registry);
        $comments_obj = new Models_CommentsModel($this->registry);
        extract($args);
        $log_id = $this->helpers->escape_string($log_id);
        $commentText = htmlspecialchars($commentText);
        $commentText2 = $this->helpers->escape_string($commentText);
        $user_id = $this->helpers->getCurrentUser();
        $comment_title = '';
        if($comments_obj->insertComment('`comment_title`, `comment_content`, `comment_user`', "'$comment_title', '$commentText2', '$user_id'")) {
            $comment_id = $comments_obj->registry->mysqli->insert_id;
            $comments_obj->insertCommentMeta('`log_id`, `comment_id`', "'$log_id', '$comment_id'");
            $user_result = $users_obj->getUser("`user_id` = '$user_id'");
            $user = $this->helpers->json_decode_to_array_not_in_mysql($user_result['user_content']);
            $json[] = array(
                'comment_id' => $comment_id,
                'comment_text' => $commentText,
                'comment_user' => $user['NickH2d']
            );
            ob_start();
            ?>
            <li id="comment_<?php echo $comment_id; ?>">
                <div class="avatar" href="#">
                    <a class="image" target="_blank" href="https://vi.gravatar.com/site/signup/" title="Change Avatar">
                        <img width="50" src="<?php echo $this->helpers->get_gravatar($user_result['user_email']); ?>" />
                    </a>
                    <div class="info">
                        <?php
                        if($this->options['show_nickh2d']) {
                        ?>
                        <span class="user_h2d"><?php echo $user['NickH2d']; ?></span><br />
                        <?php
                        }
                        ?>
                        <span class="user_fullname"><?php echo $user['Name']; ?></span>
                    </div>
                    
                </div>
                <div class="comment_content">
                    <?php
                    if($this->helpers->isAdmin()) {
                    ?>
                    <div class="actions"><a class="delete" onclick="deleteComment(this); return false;" href="#">Delete</a></div>
                    <?php
                    }
                    ?>
                    <p><?php echo nl2br($commentText); ?></p>
                </div>
            </li>
            <?php
            $this->json['response']['html'] = ob_get_contents();
            ob_end_clean();
            $this->json['response']['json'] = $json;
            $this->json['success'] = true;
        }
    }
    function review_deleteComment($args) {
        if(!$this->helpers->isAdmin()) return false;
        $logs_obj = new Models_FormsLogModel($this->registry);
        $comments_obj = new Models_CommentsModel($this->registry);
        $users_obj = new Models_UsersModel($this->registry);
        extract($args);
        $log_id = $this->helpers->escape_string($log_id);
        $comment_id = $this->helpers->escape_string($comment_id);
        if($comments_obj->getComment("`comment_id` = '$comment_id'")) {
            if($comments_obj->deleteComment("`comment_id` = '$comment_id'")) {
                $comments_obj->deleteCommentMeta("`comment_id` = '$comment_id'");
                $this->json['success'] = true;
            }  
        }
    }
    function cwa_addWa($args) {
        if(!$this->helpers->isAdmin()) return false;
        $logs_obj = new Models_FormsLogModel($this->registry);
        $forms_obj = new Models_FormsModel($this->registry);
        $args = $this->helpers->unescape_string($args);
        extract($args);
        $routers = $this->registry->router->getRouters();
        $formId = $routers['id'];
        $formId = $this->helpers->escape_string($formId);
        $form_result = $forms_obj->getForm($formId,'');
        if(!$form_result) return;
        $form_id = $form_result['form_id'];
        $fieldId = $this->helpers->escape_string($fieldId);
        $fieldValue2 = $this->helpers->escape_string(trim($fieldValue));
        $log_content = array(
            'FieldId' => $fieldId,
            'Title' => $fieldTitle,
            'Value' => $fieldValue,
            'Answer' => '',
            'AType' => 'awrong'
        );
        $this->json['update'] = false;
        $log_content = $this->helpers->json_encode_then_escape($log_content);
        $log = $logs_obj->getLog("`form_id` = '$form_id' AND `field_id` = '$fieldId' AND `log_subject` = '$fieldValue2'");
        if($log) {
            $entd = $this->helpers->get_datetime();
            $log_id = $log['log_id'];
            $logs_obj->updateLog("`log_hit` = `log_hit` + 1, `log_update` = '$entd', `log_content` = '$log_content', `log_subject` = '$fieldValue2'", "`log_id` = '$log_id'");
            $this->json['success'] = true;
            $this->json['update'] = $log_id;
            $this->json['update_hits'] = $log['log_hit'] + 1;
        } else {
            if($logs_obj->insertLog('`form_id`, `field_id`, `log_subject`, `log_content`', "'$form_id', '$fieldId', '$fieldValue2', '$log_content'")) {
                $log_id = $logs_obj->registry->mysqli->insert_id;
                $user_id = $this->helpers->getCurrentUser();
                $logs_obj->insertLogUser('`user_id`, `log_id`', "'$user_id', '$log_id'");
                $this->json['success'] = true;
                $this->json['response']['json'] = array(
                    $log_id => array(
                        'log_id' => $log_id,
                        'log_value' => $fieldValue
                    )
                );
                $this->json['response']['html'] = '<li id="log_'.$log_id.'"><header class="heading"><span class="value">'.$fieldValue.'</span> ( 1 Hits ) <a class="add_comment" href="#">Add comments</a> <a class="del_wa delete" href="#">Delete Answer</a></header><div class="comments"><ul></ul></div></li>';
            }
        }
    }
    function cwa_deleteLog($args) {
        if(!$this->helpers->isAdmin()) return false;
        $logs_obj = new Models_FormsLogModel($this->registry);
        extract($args);
        $log_id = $this->helpers->escape_string($log_id);
        
        if($logs_obj->deleteLog("`log_id` = '$log_id'",'')) {
            $logs_obj->deleteLogUser("`log_id` = '$log_id'",'');
            $logs_obj->deleteLogMeta("`log_id` = '$log_id'",'');
            $this->json['success'] = true;
        }
    } 
    
    function cwa_loadComments($args) {
        $this->review_loadComments($args);
    }
    function cwa_addComment($args) {
        $this->review_addComment($args);
    }
    function cwa_deleteComment($args) {
        $this->review_deleteComment($args);
    }
    /* ---------------------------------------------------------------------------- */
    /* ------------------------ THEMES ----------------------------- */
    /* ---------------------------------------------------------------------------- */
    function themes_loadThemes() {
        $themes_obj = new Models_ThemesModel($this->registry);
        $theme_resut = $themes_obj->getThemes('','theme_id','ASC');
        $themes = array();
        foreach($theme_resut as $theme) {
            $theme_content = $this->helpers->json_decode_to_array($theme['theme_content']);
            $theme_content['styleId'] = $theme['theme_id'];
            $themes[] = $theme_content;
        }
        $this->json['success'] = 'true';
        $this->json['response']['themes'] = json_encode(array_values($themes));
    }
    function themes_save() {
        $themes_obj = new Models_ThemesModel($this->registry);
        $req = $this->registry->req;
        extract($req);
        $theme = $this->helpers->json_decode_to_array($theme);
        $theme_name = $theme['name'];
        $theme = $this->helpers->json_encode_then_escape($theme);
        if($theme_r = $themes_obj->getTheme($theme_name,'','theme_name','')) {
            $theme_id = $theme_r['theme_id'];
            $themes_obj->updateTheme("`theme_content` = '$theme'","`theme_id` = '$theme_id'");
            $isNewTheme = 'false';
        } else {
            $themes_obj->insertTheme('`theme_name`,`theme_content`',"'$theme_name','$theme'");
            $theme_id = $themes_obj->registry->mysqli->insert_id;
            $isNewTheme = 'true';
        }
        if(!$themes_obj->getErrors()) {
            $_json = array(
                'themeId' => $theme_id,
                'isNewTheme' => $isNewTheme
            );
            $this->json['response'] = ($_json);
            $this->json['success'] = true;
        }
    }
    function themes_rename() {
        $themes_obj = new Models_ThemesModel($this->registry);
        $req = $this->registry->req;
        extract($req);
        $styleId = $this->helpers->escape_string($styleId);
        if( $_theme = $themes_obj->getTheme('',$styleId,'','theme_id')) {
            $theme = $this->helpers->json_decode_to_array($_theme['theme_content']);
            $theme['styleId'] = $styleId;
            $theme['name'] = $name;
            $theme = $this->helpers->json_encode_then_escape($theme);
            $themes_obj->updateTheme("`theme_name` = '$name',`theme_content` = '$theme'","`theme_id` = '$styleId'");
        }
        if(!$themes_obj->getErrors()) {
            $this->json['response'] = '';
            $this->json['success'] = true;
        }
    }
    function themes_delete() {
        $themes_obj = new Models_ThemesModel($this->registry);
        $req = $this->registry->req;
        extract($req);
        $styleId = $this->helpers->escape_string($styleId);
        $themes_obj->deleteTheme('',$styleId,'','theme_id');
        if(!$themes_obj->getErrors()) {
            $this->json['response'] = '';
            $this->json['success'] = true;
        }
    }
    /* ---------------------------------------------------------------------------- */
    /* ------------------------ IMPORTER ----------------------------- */
    /* ---------------------------------------------------------------------------- */
    function importer_addfield($args) {
        if($args['type'] == 'radio') {
            $this->build_addfield($args);
        }
    }
    function importer_displayfield($args) {
        $this->build_displayfield($args);
    }
    function importer_save($args) {
        $form = $this->helpers->json_decode_to_array($args['form']);
        $extends = array(
            'Formtype' => 'autoscore',
            'FormScoreUrl' => '',
            'RedirectMessage' => 'Great! Thanks for filling out my form!',
            'UserSubmitLimit' => '',
            'FormScoreUrl' => '',
            'InputRightAnswerRegex' => false,
            'InputRightAnswer' => '',
            'Password' => '',
            'Redirect' => '',
            'Email' => '',
            'StyleId' => '1',
            'IsPublic' => '0',
            'UniqueIP' => '0',
            'ReplyTo' => '0',
            'ConfirmationFromAddress' => '',
            'ConfirmationSubject' => '',
            'ReceiptSubject' => '',
            'ReceiptReplyTo' => '',
            'FromAddress' => '',
            'ReceiptMessage' => '',
            'ReceiptCopy' => '',
            'ReceiptSubject' => '',
            'ReceiptReplyTo' => '',
            'FromAddress' => '',
            'ReceiptMessage' => '',
            'Language' => 'english',
            'EmailNewEntries' => '1',
            'EmailNewComments' => '0',
            'PhoneNewEntries' => '0',
            'PhoneNewComments' => '0',
            'Mobile' => '0',
            'Carrier' => '0',
            'MerchantEnabled' => '0',
            'StartDate' => '2000-01-01 12:00:00',
            'EndDate' => '2030-01-01 12:00:00',
            'EntryLimit' => '0',
            'PlainText' => '0',
            'UseCaptcha' => '0',
            'BreadCrumbType' => '1',
            'ShowPageTitle' => '1',
            'ShowPageFooter' => '1',
            'PaymentPageTitle' => 'Checkout',
            'RulesEnabled' => '2',
            'GAKey' => '0',
            'NonCriticalIntegrations' => null,
            'EntryCount' => null,
            'CurrentPage' => false,
            'PageCount' => '1',
            'PageBreaks' => array(),
            'EntryComments' => array(),
            'IntegrationList' => array(),
            'NoInstructions' => true,
            'Entries' => array()
        );
        $form = array_merge($form, $extends);
        $args['form'] = json_encode($form);
        $args['byImpoter'] = true;
        $this->build_save($args);
    }
    /* ---------------------------------------------------------------------------- */
    /* ------------------------ DISPLAY ----------------------------- */
    /* ---------------------------------------------------------------------------- */
    function getDisplayDuplicateFields($args, $extra = '') {
        extract($args);
        $html = '';
        $field_json = $args['field'];
        $field = $this->helpers->json_decode_to_array($field_json);
        if($type == 'text') {
            $html .= '<div>';
            $html .= '<input id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" type="text" class="field text medium" value="'.$field['DefaultVal'].'" maxlength="255" tabindex="'.$fieldId.'" onkeyup="handleInput(this); "onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>';
            $html .= '</div>';
        } elseif($type == 'number') {
            $html .= '<div>';
            $html .= '<input id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" type="text" class="field text nospin medium" value="'.$field['DefaultVal'].'" tabindex="'.$fieldId.'" onkeyup="handleInput(this); " onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>';
            $html .= '</div>';
        } elseif($type == 'textarea') {
            $html .= '<div>';
            $html .= '<div class="handle"></div><textarea id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" class="field textarea medium" spellcheck="true" rows="10" cols="50" tabindex="'.$fieldId.'" onkeyup="handleInput(this); " onchange="handleInput(this);" readonly="readonly" disabled="disabled">'.$field['DefaultVal'].'</textarea>';
            $html .= '</div>';
        } elseif($type == 'checkbox') {
    		$html .= '<div>';
            foreach($field['SubFields'] as $subF) {
                $html .= '<span>';
    				$html .= '<input id="'.$extra.'Field" name="'.$extra.'Field" type="checkbox" class="field checkbox" value="'.$subF['ChoicesText'].'" tabindex="1" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>';
    				$html .= '<label class="choice" for="'.$extra.'Field">';
    					$html .= $subF['ChoicesText'];
    				$html .= '</label>';
    			$html .= '</span>';
            }
    		$html .= '</div>';
        } elseif($type == 'radio') {
            $html .= '<div>';
                $html .= '<input id="'.$extra.'radioDefault_'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" type="hidden" value="" />';
            foreach($field['Choices'] as $choiC) {
                $html .= '<span>';
                    $html .= '<input id="'.$extra.'Field'.$fieldId.'_0" name="'.$extra.'Field'.$fieldId.'" type="radio" class="field radio" value="'.$choiC['Choice'].'" tabindex="1" onchange="handleInput(this);" onmouseup="handleInput(this);"';
                    if($choiC['IsDefault']) {
                        $html .= ' checked="checked"';
                    } 
                    $html .= ' readonly="readonly"
                    disabled="disabled"/>';
                    $html .= '<label class="choice" for="Field'.$fieldId.'_0">';
                    $html .= $choiC['Choice'];
                    $html .= '</label>';
                $html .= '</span>';
            }
            $html .= '</div>';
        } elseif($type == 'select') {
            $html .= '<div>';
            $html .= '<select id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" class="field select medium" onclick="handleInput(this);" onkeyup="handleInput(this);" tabindex="'.$fieldId.'" readonly="readonly" disabled="disabled">';
            foreach($field['Choices'] as $choiC) {
    			$html .= '<option value="'.$choiC['Choice'].'"';
                if($choiC['IsDefault']) {
                    $html .= ' selected="selected"';
                }
                $html .= '>';
    				$html .= $choiC['Choice'];
    			$html .= '</option>';
            }
    		$html .= '</select>';
            $html .= '</div>';
        } elseif($type == 'section') {
            $html .= '<section>';
        	$html .= '<h3 id="'.$extra.'title_section">';
        	$html .= 'Section Break';
        	$html .= '</h3>';
        	$html .= '<div id="'.$extra.'instruct_section">';
        	$html .= 'A description of the section goes here.';
        	$html .= '</div>';
            $html .= '</section>';
            $this->json_['DisplayPos'] = '1';
        } elseif($type == 'likert') {
            $html .= '<table cellspacing="0">';
        	$html .= '<caption id="'.$extra.'title'.$fieldId.'">';
        		$html .= 'Evaluate the following statements.';
        		$html .= '<span id="'.$extra.'req_'.$fieldId.'" class="req">';
        		$html .= '</span>';
        	$html .= '</caption>';
        	$html .= '<thead>';
                    $html .= '<th>';
        				$html .= '&nbsp;';
        			$html .= '</th>';
                foreach($field['Choices'] as $choiC) {
        			$html .= '<td>';
        				$html .= $choiC['Choice'];
        			$html .= '</td>';
                }
        		$html .= '</tr>';
        	$html .= '</thead>';
        	$html .= '<tbody>';
            $i=-1;
            foreach($field['SubFields'] as $ks => $subF) {
                $i++;
                $subF['ColumnId'] = ($fieldId+$i);
        		$html .= '<tr class="statement'.($fieldId+$i).'">';
        			$html .= '<th>';
        				$html .= '<label for="'.$extra.'Field'.$fieldId.'">';
        					$html .= $subF['ChoicesText'];
        				$html .= '</label>';
        			$html .= '</th>';
                foreach($field['Choices'] as $kc => $choiC) {
        			$html .= '<td title="'.$choiC['Choice'].'">';
        				$html .= '<input id="'.$extra.'Field'.$fieldId.'_'.($kc+1).'" name="'.$extra.'Field'.$fieldId.'" type="radio" tabindex="'.$kc.'" value="'.$choiC['Choice'].'" onchange="handleInput(this);" disabled="disabled" readonly="readonly"/>';
        				$html .= '<label for="'.$extra.'Field'.$fieldId.'_'.($kc+1).'">';
        					$html .= ($kc+1);
        				$html .= '</label>';
        			$html .= '</td>';
                }
        		$html .= '</tr>';
                
                $this->json_['SubFields'][$ks] = $subF;
            }
        	$html .= '</tbody>';
            $html .= '</table>';
        } elseif($type == 'shortname') {
            $html .= '';
            $html .= '
            <span>
        		<input id="'.$extra.'Field'.($fieldId).'" name="'.$extra.'Field'.($fieldId).'" type="text" class="field text fn" value="" size="8" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
        		<label for="Field'.($fieldId).'">
        			First
        		</label>
        	</span>
        	<span>
        		<input id="'.$extra.'Field'.($fieldId+1).'" name="'.$extra.'Field'.($fieldId+1).'" type="text" class="field text ln" value="" size="14" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
        		<label for="Field'.($fieldId+1).'">
        			Last
        		</label>
        	</span>';
            $html .= '';
            $this->json_['SubFields'] = array(array(
                'Title' => 'First',
                'ColumnId' => ($fieldId),
                'FieldId' => ($fieldId),
                'ChoicesText' => 'First',
                'DefaultVal' => '',
                'Price' => '0'
            ), array(
                'Title' => 'Last',
                'ColumnId' => ''.($fieldId+1),
                'FieldId' => ''.($fieldId+1),
                'ChoicesText' => 'Last',
                'DefaultVal' => '',
                'Price' => '0'
            ));
        } elseif($type == 'address') {
            $html .= '<div>';
            $html .= '<span class="full addr1"> <input id="'.$extra.'Field" name="'.$extra.'Field" type="text" class="field text addr" value="" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="'.$extra.'Field"> Street Address </label> </span> <span class="full addr2"> <input id="'.$extra.'Field" name="'.$extra.'Field" type="text" class="field text addr" value="" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="'.$extra.'Field"> Address Line 2 </label> </span> <span class="left city"> <input id="'.$extra.'Field" name="'.$extra.'Field" type="text" class="field text addr" value="" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="'.$extra.'Field"> City </label> </span> <span class="right state"> <input id="'.$extra.'Field" name="'.$extra.'Field" type="text" class="field text addr" value="" tabindex="4" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="'.$extra.'Field"> State / Province / Region </label> </span> <span class="left zip"> <input id="'.$extra.'Field" name="'.$extra.'Field" type="text" class="field text addr" value="" maxlength="15" tabindex="5" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="'.$extra.'Field"> Postal / Zip Code </label> </span> <span class="right country"> <select id="'.$extra.'Field" name="'.$extra.'Field" class="field select addr" tabindex="6" onclick="handleInput(this);" onkeyup="handleInput(this);" readonly="readonly" disabled="disabled"> <option value="" selected="selected"> </option> <option value="United States"> United States </option> <option value="United Kingdom"> United Kingdom </option> <option value="Australia"> Australia </option> <option value="Canada"> Canada </option> <option value="France"> France </option> <option value="New Zealand"> New Zealand </option> <option value="India"> India </option> <option value="Brazil"> Brazil </option> <option value="----"> ---- </option> <option value="Afghanistan"> Afghanistan </option> <option value="Åland Islands"> Åland Islands </option> <option value="Albania"> Albania </option> <option value="Algeria"> Algeria </option> <option value="American Samoa"> American Samoa </option> <option value="Andorra"> Andorra </option> <option value="Angola"> Angola </option> <option value="Anguilla"> Anguilla </option> <option value="Antarctica"> Antarctica </option> <option value="Antigua and Barbuda"> Antigua and Barbuda </option> <option value="Argentina"> Argentina </option> <option value="Armenia"> Armenia </option> <option value="Aruba"> Aruba </option> <option value="Austria"> Austria </option> <option value="Azerbaijan"> Azerbaijan </option> <option value="Bahamas"> Bahamas </option> <option value="Bahrain"> Bahrain </option> <option value="Bangladesh"> Bangladesh </option> <option value="Barbados"> Barbados </option> <option value="Belarus"> Belarus </option> <option value="Belgium"> Belgium </option> <option value="Belize"> Belize </option> <option value="Benin"> Benin </option> <option value="Bermuda"> Bermuda </option> <option value="Bhutan"> Bhutan </option> <option value="Bolivia"> Bolivia </option> <option value="Bosnia and Herzegovina"> Bosnia and Herzegovina </option> <option value="Botswana"> Botswana </option> <option value="British Indian Ocean Territory"> British Indian Ocean Territory </option> <option value="Brunei Darussalam"> Brunei Darussalam </option> <option value="Bulgaria"> Bulgaria </option> <option value="Burkina Faso"> Burkina Faso </option> <option value="Burundi"> Burundi </option> <option value="Cambodia"> Cambodia </option> <option value="Cameroon"> Cameroon </option> <option value="Cape Verde"> Cape Verde </option> <option value="Cayman Islands"> Cayman Islands </option> <option value="Central African Republic"> Central African Republic </option> <option value="Chad"> Chad </option> <option value="Chile"> Chile </option> <option value="China"> China </option> <option value="Colombia"> Colombia </option> <option value="Comoros"> Comoros </option> <option value="Democratic Republic of the Congo"> Democratic Republic of the Congo </option> <option value="Republic of the Congo"> Republic of the Congo </option> <option value="Cook Islands"> Cook Islands </option> <option value="Costa Rica"> Costa Rica </option> <option value="C&ocirc;te dIvoire"> C&ocirc;te dIvoire </option> <option value="Croatia"> Croatia </option> <option value="Cuba"> Cuba </option> <option value="Cyprus"> Cyprus </option> <option value="Czech Republic"> Czech Republic </option> <option value="Denmark"> Denmark </option> <option value="Djibouti"> Djibouti </option> <option value="Dominica"> Dominica </option> <option value="Dominican Republic"> Dominican Republic </option> <option value="East Timor"> East Timor </option> <option value="Ecuador"> Ecuador </option> <option value="Egypt"> Egypt </option> <option value="El Salvador"> El Salvador </option> <option value="Equatorial Guinea"> Equatorial Guinea </option> <option value="Eritrea"> Eritrea </option> <option value="Estonia"> Estonia </option> <option value="Ethiopia"> Ethiopia </option> <option value="Faroe Islands"> Faroe Islands </option> <option value="Fiji"> Fiji </option> <option value="Finland"> Finland </option> <option value="Gabon"> Gabon </option> <option value="Gambia"> Gambia </option> <option value="Georgia"> Georgia </option> <option value="Germany"> Germany </option> <option value="Ghana"> Ghana </option> <option value="Gibraltar"> Gibraltar </option> <option value="Greece"> Greece </option> <option value="Grenada"> Grenada </option> <option value="Guatemala"> Guatemala </option> <option value="Guinea"> Guinea </option> <option value="Guinea-Bissau"> Guinea-Bissau </option> <option value="Guyana"> Guyana </option> <option value="Haiti"> Haiti </option> <option value="Honduras"> Honduras </option> <option value="Hong Kong"> Hong Kong </option> <option value="Hungary"> Hungary </option> <option value="Iceland"> Iceland </option> <option value="Indonesia"> Indonesia </option> <option value="Iran"> Iran </option> <option value="Iraq"> Iraq </option> <option value="Ireland"> Ireland </option> <option value="Israel"> Israel </option> <option value="Italy"> Italy </option> <option value="Jamaica"> Jamaica </option> <option value="Japan"> Japan </option> <option value="Jordan"> Jordan </option> <option value="Kazakhstan"> Kazakhstan </option> <option value="Kenya"> Kenya </option> <option value="Kiribati"> Kiribati </option> <option value="North Korea"> North Korea </option> <option value="South Korea"> South Korea </option> <option value="Kuwait"> Kuwait </option> <option value="Kyrgyzstan"> Kyrgyzstan </option> <option value="Laos"> Laos </option> <option value="Latvia"> Latvia </option> <option value="Lebanon"> Lebanon </option> <option value="Lesotho"> Lesotho </option> <option value="Liberia"> Liberia </option> <option value="Libya"> Libya </option> <option value="Liechtenstein"> Liechtenstein </option> <option value="Lithuania"> Lithuania </option> <option value="Luxembourg"> Luxembourg </option> <option value="Macedonia"> Macedonia </option> <option value="Madagascar"> Madagascar </option> <option value="Malawi"> Malawi </option> <option value="Malaysia"> Malaysia </option> <option value="Maldives"> Maldives </option> <option value="Mali"> Mali </option> <option value="Malta"> Malta </option> <option value="Marshall Islands"> Marshall Islands </option> <option value="Mauritania"> Mauritania </option> <option value="Mauritius"> Mauritius </option> <option value="Mexico"> Mexico </option> <option value="Micronesia"> Micronesia </option> <option value="Moldova"> Moldova </option> <option value="Monaco"> Monaco </option> <option value="Mongolia"> Mongolia </option> <option value="Montenegro"> Montenegro </option> <option value="Morocco"> Morocco </option> <option value="Mozambique"> Mozambique </option> <option value="Myanmar"> Myanmar </option> <option value="Namibia"> Namibia </option> <option value="Nauru"> Nauru </option> <option value="Nepal"> Nepal </option> <option value="Netherlands"> Netherlands </option> <option value="Netherlands Antilles"> Netherlands Antilles </option> <option value="Nicaragua"> Nicaragua </option> <option value="Niger"> Niger </option> <option value="Nigeria"> Nigeria </option> <option value="Norway"> Norway </option> <option value="Oman"> Oman </option> <option value="Pakistan"> Pakistan </option> <option value="Palau"> Palau </option> <option value="Palestine"> Palestine </option> <option value="Panama"> Panama </option> <option value="Papua New Guinea"> Papua New Guinea </option> <option value="Paraguay"> Paraguay </option> <option value="Peru"> Peru </option> <option value="Philippines"> Philippines </option> <option value="Poland"> Poland </option> <option value="Portugal"> Portugal </option> <option value="Puerto Rico"> Puerto Rico </option> <option value="Qatar"> Qatar </option> <option value="Romania"> Romania </option> <option value="Russia"> Russia </option> <option value="Rwanda"> Rwanda </option> <option value="Saint Kitts and Nevis"> Saint Kitts and Nevis </option> <option value="Saint Lucia"> Saint Lucia </option> <option value="Saint Vincent and the Grenadines"> Saint Vincent and the Grenadines </option> <option value="Samoa"> Samoa </option> <option value="San Marino"> San Marino </option> <option value="Sao Tome and Principe"> Sao Tome and Principe </option> <option value="Saudi Arabia"> Saudi Arabia </option> <option value="Senegal"> Senegal </option> <option value="Serbia and Montenegro"> Serbia and Montenegro </option> <option value="Seychelles"> Seychelles </option> <option value="Sierra Leone"> Sierra Leone </option> <option value="Singapore"> Singapore </option> <option value="Slovakia"> Slovakia </option> <option value="Slovenia"> Slovenia </option> <option value="Solomon Islands"> Solomon Islands </option> <option value="Somalia"> Somalia </option> <option value="South Africa"> South Africa </option> <option value="Spain"> Spain </option> <option value="Sri Lanka"> Sri Lanka </option> <option value="Sudan"> Sudan </option> <option value="Suriname"> Suriname </option> <option value="Swaziland"> Swaziland </option> <option value="Sweden"> Sweden </option> <option value="Switzerland"> Switzerland </option> <option value="Syria"> Syria </option> <option value="Taiwan"> Taiwan </option> <option value="Tajikistan"> Tajikistan </option> <option value="Tanzania"> Tanzania </option> <option value="Thailand"> Thailand </option> <option value="Togo"> Togo </option> <option value="Tonga"> Tonga </option> <option value="Trinidad and Tobago"> Trinidad and Tobago </option> <option value="Tunisia"> Tunisia </option> <option value="Turkey"> Turkey </option> <option value="Turkmenistan"> Turkmenistan </option> <option value="Tuvalu"> Tuvalu </option> <option value="Uganda"> Uganda </option> <option value="Ukraine"> Ukraine </option> <option value="United Arab Emirates"> United Arab Emirates </option> <option value="United States Minor Outlying Islands"> United States Minor Outlying Islands </option> <option value="Uruguay"> Uruguay </option> <option value="Uzbekistan"> Uzbekistan </option> <option value="Vanuatu"> Vanuatu </option> <option value="Vatican City"> Vatican City </option> <option value="Venezuela"> Venezuela </option> <option value="Vietnam"> Vietnam </option> <option value="Virgin Islands, British"> Virgin Islands, British </option> <option value="Virgin Islands, U.S."> Virgin Islands, U.S. </option> <option value="Yemen"> Yemen </option> <option value="Zambia"> Zambia </option> <option value="Zimbabwe"> Zimbabwe </option> </select> <label for="'.$extra.'Field"> Country </label> </span>';
            $html .= '</div>';
            $this->json_['SubFields'] = array(array(
                'Title' => 'Street Address',
                'ColumnId' => $fieldId,
                'FieldId' => $fieldId,
                'ChoicesText' => 'Street Address',
                'DefaultVal' => '',
                'Price' => '0'
            ), array(
                'Title' => 'Address Line 2',
                'ColumnId' => ''.($fieldId+1),
                'FieldId' => ''.($fieldId+1),
                'ChoicesText' => 'Address Line 2',
                'DefaultVal' => '',
                'Price' => '0'
            ), array(
                'Title' => 'City',
                'ColumnId' => ''.($fieldId+2),
                'FieldId' => ''.($fieldId+2),
                'ChoicesText' => 'City',
                'DefaultVal' => '',
                'Price' => '0'
            ), array(
                'Title' => 'State \/ Province \/ Region',
                'ColumnId' => ''.($fieldId+3),
                'FieldId' => ''.($fieldId+3),
                'ChoicesText' => 'State \/ Province \/ Region',
                'DefaultVal' => '',
                'Price' => '0'
            ), array(
                'Title' => 'Postal \/ Zip Code',
                'ColumnId' => ''.($fieldId+4),
                'FieldId' => ''.($fieldId+4),
                'ChoicesText' => 'Postal \/ Zip Code',
                'DefaultVal' => '',
                'Price' => '0'
            ), array(
                'Title' => 'Country',
                'ColumnId' => ''.($fieldId+5),
                'FieldId' => ''.($fieldId+5),
                'ChoicesText' => 'Country',
                'DefaultVal' => '',
                'Price' => '0'
            ));
        } elseif($type == 'date') {
            $html .= '';
            $html .= '
                	<span>
                		<input id="'.$extra.'Field'.$fieldId.'-1" name="'.$extra.'Field'.$fieldId.'-1" type="text" class="field text" value="" size="2" maxlength="2" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                		<label for="Field'.$fieldId.'-1">
                			MM
                		</label>
                	</span>
                	<span class="symbol">
                		/
                	</span>
                	<span>
                		<input id="'.$extra.'Field'.$fieldId.'-2" name="'.$extra.'Field'.$fieldId.'-2" type="text" class="field text" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                		<label for="Field'.$fieldId.'-2">
                			DD
                		</label>
                	</span>
                	<span class="symbol">
                		/
                	</span>
                	<span>
                		<input id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" type="text" class="field text" value="" size="4" maxlength="4" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                		<label for="Field'.$fieldId.'">
                			YYYY
                		</label>
                	</span>
                	<span id="cal'.$fieldId.'">
                		<img id="pick'.$fieldId.'" class="datepicker" src="template/images/icons/calendar.png" alt="Pick a date." />
                	</span>';
            $html .= '';
        } elseif($type == 'email') {
            $html .= '<div>';
            $html .= '<input id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" type="email" spellcheck="false" class="field text medium" value="" maxlength="255" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>';
            $html .= '</div>';
        } elseif($type == 'time') {
            $html .= '';
            $html .= '
                    <span class="hours">
                    	<input id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" type="text" class="field text" value="" size="2" maxlength="2" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                    	<label for="Field'.$fieldId.'">
                    		HH
                    	</label>
                    </span>
                    <span class="symbol minutes">
                    	:
                    </span>
                    <span class="minutes">
                    	<input id="'.$extra.'Field'.$fieldId.'-1" name="'.$extra.'Field'.$fieldId.'-1" type="text" class="field text" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                    	<label for="Field'.$fieldId.'-1">
                    		MM
                    	</label>
                    </span>
                    <span class="symbol seconds">
                    	:
                    </span>
                    <span class="seconds">
                    	<input id="'.$extra.'Field'.$fieldId.'-2" name="'.$extra.'Field'.$fieldId.'-2" type="text" class="field text" value="" size="2" maxlength="2" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                    	<label for="Field'.$fieldId.'-2">
                    		SS
                    	</label>
                    </span>
                    <span class="ampm">
                    	<select id="'.$extra.'Field'.$fieldId.'-3" name="'.$extra.'Field'.$fieldId.'-3" class="field select" style="width:4em" tabindex="4" onclick="handleInput(this);" onkeyup="handleInput(this);" readonly="readonly" disabled="disabled">
                    		<option value="AM" selected="selected">
                    			AM
                    		</option>
                    		<option value="PM">
                    			PM
                    		</option>
                    	</select>
                    	<label for="Field'.$fieldId.'-3">
                    		AM/PM
                    	</label>
                    </span>';
            $html .= '';
        } elseif($type == 'phone') {
            $html .= '';
            $html .= '
                <span>
                	<input id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" type="tel" class="field text" value="" size="3" maxlength="3" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                	<label for="Field'.$fieldId.'">
                		###
                	</label>
                </span>
                <span class="symbol">
                	-
                </span>
                <span>
                	<input id="'.$extra.'Field'.$fieldId.'-1" name="'.$extra.'Field'.$fieldId.'-1" type="tel" class="field text" value="" size="3" maxlength="3" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                	<label for="Field'.$fieldId.'-1">
                		###
                	</label>
                </span>
                <span class="symbol">
                	-
                </span>
                <span>
                	<input id="'.$extra.'Field'.$fieldId.'-2" name="'.$extra.'Field'.$fieldId.'-2" type="tel" class="field text" value="" size="4" maxlength="4" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                	<label for="Field'.$fieldId.'-2">
                		####
                	</label>
                </span>';
            $html .= '';
        } elseif($type == 'url') {
            $html .= '<div>';
            $html .= '<input id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" type="url" class="field text medium" value="" maxlength="255" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>';
            $html .= '</div>';
        } elseif($type == 'money') {
            $html .= '';
            $html .= '
                <span class="symbol">
                	$
                </span>
                <span>
                	<input id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" type="text" class="field text currency nospin" value="" size="10" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                	<label for="Field'.$fieldId.'">
                		Dollars
                	</label>
                </span>
                <span class="symbol radix">
                	.
                </span>
                <span class="cents">
                	<input id="'.$extra.'Field'.$fieldId.'-1" name="'.$extra.'Field'.$fieldId.'-1" type="text" class="field text nospin" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly"
                	disabled="disabled"/>
                	<label for="Field'.$fieldId.'-1">
                		Cents
                	</label>
                </span>';
            $html .= '';
        }
        return $html;
    }
    function getDisplayFields($args, $extra = '') {
        extract($args);
        $html = '';
        if($type == 'text') {
            $html .= '<div>';
            $html .= '<input id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" type="text" class="field text medium" value="" maxlength="255" tabindex="'.$fieldId.'" onkeyup="handleInput(this); "onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>';
            $html .= '</div>';
            $this->json_['Excerpt'] = '0';
        } elseif($type == 'number') {
            $html .= '<div>';
            $html .= '<input id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" type="text" class="field text nospin medium" value="" tabindex="'.$fieldId.'" onkeyup="handleInput(this); " onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>';
            $html .= '</div>';
        } elseif($type == 'textarea') {
            $html .= '<div>';
            $html .= '<div class="handle"></div><textarea id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" class="field textarea medium" spellcheck="true" rows="10" cols="50" tabindex="'.$fieldId.'" onkeyup="handleInput(this); " onchange="handleInput(this);" readonly="readonly" disabled="disabled"></textarea>';
            $html .= '</div>';
        } elseif($type == 'checkbox') {
    		$html .= '<div>';
    			$html .= '<span>';
    				$html .= '<input id="'.$extra.'Field" name="'.$extra.'Field" type="checkbox" class="field checkbox" value="First Choice" tabindex="1" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>';
    				$html .= '<label class="choice" for="'.$extra.'Field">';
    					$html .= 'First Choice';
    				$html .= '</label>';
    			$html .= '</span>';
    			$html .= '<span>';
    				$html .= '<input id="'.$extra.'Field" name="'.$extra.'Field" type="checkbox" class="field checkbox" value="Second Choice" tabindex="2" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>';
    				$html .= '<label class="choice" for="'.$extra.'Field">';
    					$html .= 'Second Choice';
    				$html .= '</label>';
    			$html .= '</span>';
    			$html .= '<span>';
    				$html .= '<input id="'.$extra.'Field" name="'.$extra.'Field" type="checkbox" class="field checkbox" value="Third Choice" tabindex="3" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>';
    				$html .= '<label class="choice" for="'.$extra.'Field">';
    					$html .= 'Third Choice';
    				$html .= '</label>';
    			$html .= '</span>';
    		$html .= '</div>';
            $this->json_['SubFields'] = array(
                array (
                    'Title' => 'First Choice',
                    'ColumnId' => $fieldId,
                    'FieldId' => $fieldId,
                    'ChoicesText' => 'First Choice',
                    'DefaultVal' => '0',
                    'IsRight' => '1',
                    'Price' => '0'
                ), array (
                    'Title' => 'Second Choice',
                    'ColumnId' => ''.($fieldId+1),
                    'FieldId' => ''.($fieldId+1),
                    'ChoicesText' => 'Second Choice',
                    'DefaultVal' => '0',
                    'IsRight' => '1',
                    'Price' => '0'
                ), array (
                    'Title' => 'Third Choice',
                    'ColumnId' => ''.($fieldId+2),
                    'FieldId' => ''.($fieldId+2),
                    'ChoicesText' => 'Third Choice',
                    'DefaultVal' => '0',
                    'IsRight' => '1',
                    'Price' => '0'
                )
            );
        } elseif($type == 'radio') {
            $html .= '<div>';
            $html .= '<input id="'.$extra.'radioDefault_'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" type="hidden" value="" />';
            $html .= '<span>';
            $html .= '<input id="'.$extra.'Field'.$fieldId.'_0" name="'.$extra.'Field'.$fieldId.'" type="radio" class="field radio" value="First Choice" tabindex="1" onchange="handleInput(this);" onmouseup="handleInput(this);" checked="checked" readonly="readonly"
            disabled="disabled"/>';
            $html .= '<label class="choice" for="Field'.$fieldId.'_0">';
            $html .= 'First Choice';
            $html .= '</label>';
            $html .= '</span>';
            $html .= '<span>';
            $html .= '<input id="'.$extra.'Field'.$fieldId.'_1" name="'.$extra.'Field'.$fieldId.'" type="radio" class="field radio" value="Second Choice" tabindex="2" onchange="handleInput(this);" onmouseup="handleInput(this);" readonly="readonly" disabled="disabled"/>';
            $html .= '<label class="choice" for="Field'.$fieldId.'_1">';
            $html .= 'Second Choice';
            $html .= '</label>';
            $html .= '</span>';
            $html .= '<span>';
            $html .= '<input id="'.$extra.'Field'.$fieldId.'_2" name="'.$extra.'Field'.$fieldId.'" type="radio" class="field radio" value="Third Choice" tabindex="3" onchange="handleInput(this);" onmouseup="handleInput(this);" readonly="readonly" disabled="disabled"/>';
            $html .= '<label class="choice" for="Field'.$fieldId.'_2">';
            $html .= 'Third Choice';
            $html .= '</label>';
            $html .= '</span>';
            $html .= '</div>';
            $this->json_['Choices'] = array(
                array(
                    'FormId' => $fieldFormId,
                    'ColumnId' => ''.($fieldId+0),
                    'ChoiceId' => '0',
                    'StaticId' => '0',
                    'Choice' => 'First Choice',
                    'IsDefault' => '1',
                    'IsRight' => '1',
                    'Price' => '0',
                    'Score' => 0
                ), array(
                    'FormId' => $fieldFormId,
                    'ColumnId' => ''.($fieldId+1),
                    'ChoiceId' => '0',
                    'StaticId' => '0',
                    'Choice' => 'Second Choice',
                    'IsDefault' => '0',
                    'IsRight' => '1',
                    'Price' => '0',
                    'Score' => 0
                ), array(
                    'FormId' => $fieldFormId,
                    'ColumnId' => ''.($fieldId+2),
                    'ChoiceId' => '0',
                    'StaticId' => '0',
                    'Choice' => 'Third Choice',
                    'IsDefault' => '0',
                    'IsRight' => '1',
                    'Price' => '0',
                    'Score' => 0
                )
            );
        } elseif($type == 'select') {
            $html .= '<div>';
            $html .= '<select id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" class="field select medium" onclick="handleInput(this);" onkeyup="handleInput(this);" tabindex="'.$fieldId.'" readonly="readonly" disabled="disabled">';
    			$html .= '<option value="" selected="selected">';
    			$html .= '</option>';
    			$html .= '<option value="First Choice">';
    				$html .= 'First Choice';
    			$html .= '</option>';
    			$html .= '<option value="Second Choice">';
    				$html .= 'Second Choice';
    			$html .= '</option>';
    			$html .= '<option value="Third Choice">';
    				$html .= 'Third Choice';
    			$html .= '</option>';
    		$html .= '</select>';
            $html .= '</div>';
            $this->json_['Choices'] = array(
                array(
                    'FormId' => $fieldFormId,
                    'ColumnId' => '',
                    'ChoiceId' => '0',
                    'StaticId' => '0',
                    'Choice' => '',
                    'IsDefault' => '1',
                    'IsRight' => '1',
                    'Price' => '0',
                    'Score' => 0
                ), array(
                    'FormId' => $fieldFormId,
                    'ColumnId' => ''.($fieldId+0),
                    'ChoiceId' => '0',
                    'StaticId' => '0',
                    'Choice' => 'First Choice',
                    'IsDefault' => '0',
                    'IsRight' => '1',
                    'Price' => '0',
                    'Score' => 0
                ), array(
                    'FormId' => $fieldFormId,
                    'ColumnId' => ''.($fieldId+1),
                    'ChoiceId' => '0',
                    'StaticId' => '0',
                    'Choice' => 'Second Choice',
                    'IsDefault' => '0',
                    'IsRight' => '1',
                    'Price' => '0',
                    'Score' => 0
                ), array(
                    'FormId' => $fieldFormId,
                    'ColumnId' => ''.($fieldId+2),
                    'ChoiceId' => '0',
                    'StaticId' => '0',
                    'Choice' => 'Third Choice',
                    'IsDefault' => '0',
                    'IsRight' => '1',
                    'Price' => '0',
                    'Score' => 0
                )
            );
        } elseif($type == 'section') {
            $html .= '<section>';
        	$html .= '<h3 id="'.$extra.'title_section">';
        	$html .= 'Section Break';
        	$html .= '</h3>';
        	$html .= '<div id="'.$extra.'instruct_section">';
        	$html .= 'A description of the section goes here.';
        	$html .= '</div>';
            $html .= '</section>';
            $this->json_['DisplayPos'] = '1';
        } elseif($type == 'likert') {
            $html .= '<table cellspacing="0">';
        	$html .= '<caption id="'.$extra.'title'.$fieldId.'">';
        		$html .= 'Evaluate the following statements.';
        		$html .= '<span id="'.$extra.'req_'.$fieldId.'" class="req">';
        		$html .= '</span>';
        	$html .= '</caption>';
        	$html .= '<thead>';
                    $html .= '<th>';
        				$html .= '&nbsp;';
        			$html .= '</th>';
        			$html .= '<td>';
        				$html .= 'Strongly Disagree';
        			$html .= '</td>';
        			$html .= '<td>';
        				$html .= 'Disagree';
        			$html .= '</td>';
        			$html .= '<td>';
        				$html .= 'Agree';
        			$html .= '</td>';
        			$html .= '<td>';
        				$html .= 'Strongly Agree';
        			$html .= '</td>';
        		$html .= '</tr>';
        	$html .= '</thead>';
        	$html .= '<tbody>';
            $j = 1;
            for($i = 1; $i<=3; $i++) {
        		$html .= '<tr class="statement'.$fieldId.'">';
                    
        			$html .= '<th>';
        				$html .= '<label for="'.$extra.'Field'.$fieldId.'">';
        					$html .= 'Statement One';
        				$html .= '</label>';
        			$html .= '</th>';
        			$html .= '<td title="Strongly Disagree">';
        				$html .= '<input id="'.$extra.'Field'.$fieldId.'_1" name="'.$extra.'Field'.$fieldId.'" type="radio" tabindex="'.$j.'" value="Strongly Disagree" onchange="handleInput(this);" disabled="disabled" readonly="readonly"/>';
        				$html .= '<label for="'.$extra.'Field'.$fieldId.'_1">';
        					$html .= '1';
        				$html .= '</label>';
        			$html .= '</td>';
                    $j++;
        			$html .= '<td title="Disagree">';
        				$html .= '<input id="'.$extra.'Field'.$fieldId.'_2" name="'.$extra.'Field'.$fieldId.'" type="radio" tabindex="'.$j.'" value="Disagree" onchange="handleInput(this);" disabled="disabled" readonly="readonly"/>';
        				$html .= '<label for="'.$extra.'Field'.$fieldId.'_2">';
        					$html .= '2';
        				$html .= '</label>';
        			$html .= '</td>';
                    $j++;
        			$html .= '<td title="Agree">';
        				$html .= '<input id="'.$extra.'Field'.$fieldId.'_3" name="'.$extra.'Field'.$fieldId.'" type="radio" tabindex="'.$j.'" value="Agree" onchange="handleInput(this);" disabled="disabled" readonly="readonly"/>';
        				$html .= '<label for="'.$extra.'Field'.$fieldId.'_3">';
        					$html .= '3';
        				$html .= '</label>';
        			$html .= '</td>';
                    $j++;
        			$html .= '<td title="Strongly Agree">';
        				$html .= '<input id="'.$extra.'Field'.$fieldId.'_4" name="'.$extra.'Field'.$fieldId.'" type="radio" tabindex="'.$j.'" value="Agree" onchange="handleInput(this);" disabled="disabled" readonly="readonly"/>';
        				$html .= '<label for="'.$extra.'Field'.$fieldId.'_4">';
        					$html .= '4';
        				$html .= '</label>';
        			$html .= '</td>';
                    $j++;
        		$html .= '</tr>';
            }
        	$html .= '</tbody>';
            $html .= '</table>';
            $this->json_['Choices'] = array(
                array(
                    'FormId' => $fieldFormId,
                    'ColumnId' => '0',
                    'ChoiceId' => '0',
                    'StaticId' => '0',
                    'Choice' => 'Strongly Disagree',
                    'IsDefault' => '0',
                    'IsRight' => '1',
                    'Price' => '0',
                    'Score' => 1
                ), array(
                    'FormId' => $fieldFormId,
                    'ColumnId' => '1',
                    'ChoiceId' => '0',
                    'StaticId' => '0',
                    'Choice' => 'Disagree',
                    'IsDefault' => '0',
                    'IsRight' => '1',
                    'Price' => '0',
                    'Score' => 2
                ), array(
                    'FormId' => $fieldFormId,
                    'ColumnId' => '2',
                    'ChoiceId' => '0',
                    'StaticId' => '0',
                    'Choice' => 'Agree',
                    'IsDefault' => '0',
                    'IsRight' => '1',
                    'Price' => '0',
                    'Score' => 3
                ), array(
                    'FormId' => $fieldFormId,
                    'ColumnId' => '3',
                    'ChoiceId' => '0',
                    'StaticId' => '0',
                    'Choice' => 'Strongly Agree',
                    'IsDefault' => '0',
                    'IsRight' => '1',
                    'Price' => '0',
                    'Score' => 4
                )
            );
            $this->json_['SubFields'] = array(
                array(
                    'Title' => 'Statement One',
                    'ColumnId' => ($fieldId),
                    'FieldId' => '0',
                    'ChoicesText' => 'Statement One',
                    'DefaultVal' => '0',
                    'Price' => '0'
                ), array(
                    'Title' => 'Statement Two',
                    'ColumnId' => ''.($fieldId+1),
                    'FieldId' => '0',
                    'ChoicesText' => 'Statement Two',
                    'DefaultVal' => '0',
                    'Price' => '0'
                ), array(
                    'Title' => 'Statement Three',
                    'ColumnId' => ''.($fieldId+2),
                    'FieldId' => '0',
                    'ChoicesText' => 'Statement Three',
                    'DefaultVal' => '0',
                    'Price' => '0'
                )
            );
        } elseif($type == 'shortname') {
            $html .= '';
            $html .= '
        <span>
    		<input id="'.$extra.'Field'.($fieldId).'" name="'.$extra.'Field'.($fieldId).'" type="text" class="field text fn" value="" size="8" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
    		<label for="Field'.($fieldId).'">
    			First
    		</label>
    	</span>
    	<span>
    		<input id="'.$extra.'Field'.($fieldId+1).'" name="'.$extra.'Field'.($fieldId+1).'" type="text" class="field text ln" value="" size="14" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
    		<label for="Field'.($fieldId+1).'">
    			Last
    		</label>
    	</span>';
            $html .= '';
            $this->json_['SubFields'] = array(array(
                'Title' => 'First',
                'ColumnId' => ($fieldId),
                'FieldId' => ($fieldId),
                'ChoicesText' => 'First',
                'DefaultVal' => '',
                'Price' => '0'
            ), array(
                'Title' => 'Last',
                'ColumnId' => ''.($fieldId+1),
                'FieldId' => ''.($fieldId+1),
                'ChoicesText' => 'Last',
                'DefaultVal' => '',
                'Price' => '0'
            ));
        } elseif($type == 'address') {
            $html .= '<div>';
            $html .= '<span class="full addr1"> <input id="'.$extra.'Field" name="'.$extra.'Field" type="text" class="field text addr" value="" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="'.$extra.'Field"> Street Address </label> </span> <span class="full addr2"> <input id="'.$extra.'Field" name="'.$extra.'Field" type="text" class="field text addr" value="" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="'.$extra.'Field"> Address Line 2 </label> </span> <span class="left city"> <input id="'.$extra.'Field" name="'.$extra.'Field" type="text" class="field text addr" value="" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="'.$extra.'Field"> City </label> </span> <span class="right state"> <input id="'.$extra.'Field" name="'.$extra.'Field" type="text" class="field text addr" value="" tabindex="4" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="'.$extra.'Field"> State / Province / Region </label> </span> <span class="left zip"> <input id="'.$extra.'Field" name="'.$extra.'Field" type="text" class="field text addr" value="" maxlength="15" tabindex="5" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="'.$extra.'Field"> Postal / Zip Code </label> </span> <span class="right country"> <select id="'.$extra.'Field" name="'.$extra.'Field" class="field select addr" tabindex="6" onclick="handleInput(this);" onkeyup="handleInput(this);" readonly="readonly" disabled="disabled"> <option value="" selected="selected"> </option> <option value="United States"> United States </option> <option value="United Kingdom"> United Kingdom </option> <option value="Australia"> Australia </option> <option value="Canada"> Canada </option> <option value="France"> France </option> <option value="New Zealand"> New Zealand </option> <option value="India"> India </option> <option value="Brazil"> Brazil </option> <option value="----"> ---- </option> <option value="Afghanistan"> Afghanistan </option> <option value="Åland Islands"> Åland Islands </option> <option value="Albania"> Albania </option> <option value="Algeria"> Algeria </option> <option value="American Samoa"> American Samoa </option> <option value="Andorra"> Andorra </option> <option value="Angola"> Angola </option> <option value="Anguilla"> Anguilla </option> <option value="Antarctica"> Antarctica </option> <option value="Antigua and Barbuda"> Antigua and Barbuda </option> <option value="Argentina"> Argentina </option> <option value="Armenia"> Armenia </option> <option value="Aruba"> Aruba </option> <option value="Austria"> Austria </option> <option value="Azerbaijan"> Azerbaijan </option> <option value="Bahamas"> Bahamas </option> <option value="Bahrain"> Bahrain </option> <option value="Bangladesh"> Bangladesh </option> <option value="Barbados"> Barbados </option> <option value="Belarus"> Belarus </option> <option value="Belgium"> Belgium </option> <option value="Belize"> Belize </option> <option value="Benin"> Benin </option> <option value="Bermuda"> Bermuda </option> <option value="Bhutan"> Bhutan </option> <option value="Bolivia"> Bolivia </option> <option value="Bosnia and Herzegovina"> Bosnia and Herzegovina </option> <option value="Botswana"> Botswana </option> <option value="British Indian Ocean Territory"> British Indian Ocean Territory </option> <option value="Brunei Darussalam"> Brunei Darussalam </option> <option value="Bulgaria"> Bulgaria </option> <option value="Burkina Faso"> Burkina Faso </option> <option value="Burundi"> Burundi </option> <option value="Cambodia"> Cambodia </option> <option value="Cameroon"> Cameroon </option> <option value="Cape Verde"> Cape Verde </option> <option value="Cayman Islands"> Cayman Islands </option> <option value="Central African Republic"> Central African Republic </option> <option value="Chad"> Chad </option> <option value="Chile"> Chile </option> <option value="China"> China </option> <option value="Colombia"> Colombia </option> <option value="Comoros"> Comoros </option> <option value="Democratic Republic of the Congo"> Democratic Republic of the Congo </option> <option value="Republic of the Congo"> Republic of the Congo </option> <option value="Cook Islands"> Cook Islands </option> <option value="Costa Rica"> Costa Rica </option> <option value="C&ocirc;te dIvoire"> C&ocirc;te dIvoire </option> <option value="Croatia"> Croatia </option> <option value="Cuba"> Cuba </option> <option value="Cyprus"> Cyprus </option> <option value="Czech Republic"> Czech Republic </option> <option value="Denmark"> Denmark </option> <option value="Djibouti"> Djibouti </option> <option value="Dominica"> Dominica </option> <option value="Dominican Republic"> Dominican Republic </option> <option value="East Timor"> East Timor </option> <option value="Ecuador"> Ecuador </option> <option value="Egypt"> Egypt </option> <option value="El Salvador"> El Salvador </option> <option value="Equatorial Guinea"> Equatorial Guinea </option> <option value="Eritrea"> Eritrea </option> <option value="Estonia"> Estonia </option> <option value="Ethiopia"> Ethiopia </option> <option value="Faroe Islands"> Faroe Islands </option> <option value="Fiji"> Fiji </option> <option value="Finland"> Finland </option> <option value="Gabon"> Gabon </option> <option value="Gambia"> Gambia </option> <option value="Georgia"> Georgia </option> <option value="Germany"> Germany </option> <option value="Ghana"> Ghana </option> <option value="Gibraltar"> Gibraltar </option> <option value="Greece"> Greece </option> <option value="Grenada"> Grenada </option> <option value="Guatemala"> Guatemala </option> <option value="Guinea"> Guinea </option> <option value="Guinea-Bissau"> Guinea-Bissau </option> <option value="Guyana"> Guyana </option> <option value="Haiti"> Haiti </option> <option value="Honduras"> Honduras </option> <option value="Hong Kong"> Hong Kong </option> <option value="Hungary"> Hungary </option> <option value="Iceland"> Iceland </option> <option value="Indonesia"> Indonesia </option> <option value="Iran"> Iran </option> <option value="Iraq"> Iraq </option> <option value="Ireland"> Ireland </option> <option value="Israel"> Israel </option> <option value="Italy"> Italy </option> <option value="Jamaica"> Jamaica </option> <option value="Japan"> Japan </option> <option value="Jordan"> Jordan </option> <option value="Kazakhstan"> Kazakhstan </option> <option value="Kenya"> Kenya </option> <option value="Kiribati"> Kiribati </option> <option value="North Korea"> North Korea </option> <option value="South Korea"> South Korea </option> <option value="Kuwait"> Kuwait </option> <option value="Kyrgyzstan"> Kyrgyzstan </option> <option value="Laos"> Laos </option> <option value="Latvia"> Latvia </option> <option value="Lebanon"> Lebanon </option> <option value="Lesotho"> Lesotho </option> <option value="Liberia"> Liberia </option> <option value="Libya"> Libya </option> <option value="Liechtenstein"> Liechtenstein </option> <option value="Lithuania"> Lithuania </option> <option value="Luxembourg"> Luxembourg </option> <option value="Macedonia"> Macedonia </option> <option value="Madagascar"> Madagascar </option> <option value="Malawi"> Malawi </option> <option value="Malaysia"> Malaysia </option> <option value="Maldives"> Maldives </option> <option value="Mali"> Mali </option> <option value="Malta"> Malta </option> <option value="Marshall Islands"> Marshall Islands </option> <option value="Mauritania"> Mauritania </option> <option value="Mauritius"> Mauritius </option> <option value="Mexico"> Mexico </option> <option value="Micronesia"> Micronesia </option> <option value="Moldova"> Moldova </option> <option value="Monaco"> Monaco </option> <option value="Mongolia"> Mongolia </option> <option value="Montenegro"> Montenegro </option> <option value="Morocco"> Morocco </option> <option value="Mozambique"> Mozambique </option> <option value="Myanmar"> Myanmar </option> <option value="Namibia"> Namibia </option> <option value="Nauru"> Nauru </option> <option value="Nepal"> Nepal </option> <option value="Netherlands"> Netherlands </option> <option value="Netherlands Antilles"> Netherlands Antilles </option> <option value="Nicaragua"> Nicaragua </option> <option value="Niger"> Niger </option> <option value="Nigeria"> Nigeria </option> <option value="Norway"> Norway </option> <option value="Oman"> Oman </option> <option value="Pakistan"> Pakistan </option> <option value="Palau"> Palau </option> <option value="Palestine"> Palestine </option> <option value="Panama"> Panama </option> <option value="Papua New Guinea"> Papua New Guinea </option> <option value="Paraguay"> Paraguay </option> <option value="Peru"> Peru </option> <option value="Philippines"> Philippines </option> <option value="Poland"> Poland </option> <option value="Portugal"> Portugal </option> <option value="Puerto Rico"> Puerto Rico </option> <option value="Qatar"> Qatar </option> <option value="Romania"> Romania </option> <option value="Russia"> Russia </option> <option value="Rwanda"> Rwanda </option> <option value="Saint Kitts and Nevis"> Saint Kitts and Nevis </option> <option value="Saint Lucia"> Saint Lucia </option> <option value="Saint Vincent and the Grenadines"> Saint Vincent and the Grenadines </option> <option value="Samoa"> Samoa </option> <option value="San Marino"> San Marino </option> <option value="Sao Tome and Principe"> Sao Tome and Principe </option> <option value="Saudi Arabia"> Saudi Arabia </option> <option value="Senegal"> Senegal </option> <option value="Serbia and Montenegro"> Serbia and Montenegro </option> <option value="Seychelles"> Seychelles </option> <option value="Sierra Leone"> Sierra Leone </option> <option value="Singapore"> Singapore </option> <option value="Slovakia"> Slovakia </option> <option value="Slovenia"> Slovenia </option> <option value="Solomon Islands"> Solomon Islands </option> <option value="Somalia"> Somalia </option> <option value="South Africa"> South Africa </option> <option value="Spain"> Spain </option> <option value="Sri Lanka"> Sri Lanka </option> <option value="Sudan"> Sudan </option> <option value="Suriname"> Suriname </option> <option value="Swaziland"> Swaziland </option> <option value="Sweden"> Sweden </option> <option value="Switzerland"> Switzerland </option> <option value="Syria"> Syria </option> <option value="Taiwan"> Taiwan </option> <option value="Tajikistan"> Tajikistan </option> <option value="Tanzania"> Tanzania </option> <option value="Thailand"> Thailand </option> <option value="Togo"> Togo </option> <option value="Tonga"> Tonga </option> <option value="Trinidad and Tobago"> Trinidad and Tobago </option> <option value="Tunisia"> Tunisia </option> <option value="Turkey"> Turkey </option> <option value="Turkmenistan"> Turkmenistan </option> <option value="Tuvalu"> Tuvalu </option> <option value="Uganda"> Uganda </option> <option value="Ukraine"> Ukraine </option> <option value="United Arab Emirates"> United Arab Emirates </option> <option value="United States Minor Outlying Islands"> United States Minor Outlying Islands </option> <option value="Uruguay"> Uruguay </option> <option value="Uzbekistan"> Uzbekistan </option> <option value="Vanuatu"> Vanuatu </option> <option value="Vatican City"> Vatican City </option> <option value="Venezuela"> Venezuela </option> <option value="Vietnam"> Vietnam </option> <option value="Virgin Islands, British"> Virgin Islands, British </option> <option value="Virgin Islands, U.S."> Virgin Islands, U.S. </option> <option value="Yemen"> Yemen </option> <option value="Zambia"> Zambia </option> <option value="Zimbabwe"> Zimbabwe </option> </select> <label for="'.$extra.'Field"> Country </label> </span>';
            $html .= '</div>';
            $this->json_['SubFields'] = array(array(
                'Title' => 'Street Address',
                'ColumnId' => $fieldId,
                'FieldId' => $fieldId,
                'ChoicesText' => 'Street Address',
                'DefaultVal' => '',
                'Price' => '0'
            ), array(
                'Title' => 'Address Line 2',
                'ColumnId' => ''.($fieldId+1),
                'FieldId' => ''.($fieldId+1),
                'ChoicesText' => 'Address Line 2',
                'DefaultVal' => '',
                'Price' => '0'
            ), array(
                'Title' => 'City',
                'ColumnId' => ''.($fieldId+2),
                'FieldId' => ''.($fieldId+2),
                'ChoicesText' => 'City',
                'DefaultVal' => '',
                'Price' => '0'
            ), array(
                'Title' => 'State \/ Province \/ Region',
                'ColumnId' => ''.($fieldId+3),
                'FieldId' => ''.($fieldId+3),
                'ChoicesText' => 'State \/ Province \/ Region',
                'DefaultVal' => '',
                'Price' => '0'
            ), array(
                'Title' => 'Postal \/ Zip Code',
                'ColumnId' => ''.($fieldId+4),
                'FieldId' => ''.($fieldId+4),
                'ChoicesText' => 'Postal \/ Zip Code',
                'DefaultVal' => '',
                'Price' => '0'
            ), array(
                'Title' => 'Country',
                'ColumnId' => ''.($fieldId+5),
                'FieldId' => ''.($fieldId+5),
                'ChoicesText' => 'Country',
                'DefaultVal' => '',
                'Price' => '0'
            ));
        } elseif($type == 'date') {
            $html .= '';
            $html .= '
                	<span>
                		<input id="'.$extra.'Field'.$fieldId.'-1" name="'.$extra.'Field'.$fieldId.'-1" type="text" class="field text" value="" size="2" maxlength="2" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                		<label for="Field'.$fieldId.'-1">
                			MM
                		</label>
                	</span>
                	<span class="symbol">
                		/
                	</span>
                	<span>
                		<input id="'.$extra.'Field'.$fieldId.'-2" name="'.$extra.'Field'.$fieldId.'-2" type="text" class="field text" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                		<label for="Field'.$fieldId.'-2">
                			DD
                		</label>
                	</span>
                	<span class="symbol">
                		/
                	</span>
                	<span>
                		<input id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" type="text" class="field text" value="" size="4" maxlength="4" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                		<label for="Field'.$fieldId.'">
                			YYYY
                		</label>
                	</span>
                	<span id="cal'.$fieldId.'">
                		<img id="pick'.$fieldId.'" class="datepicker" src="template/images/icons/calendar.png" alt="Pick a date." />
                	</span>';
            $html .= '';
        } elseif($type == 'email') {
            $html .= '<div>';
            $html .= '<input id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" type="email" spellcheck="false" class="field text medium" value="" maxlength="255" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>';
            $html .= '</div>';
        } elseif($type == 'time') {
            $html .= '';
            $html .= '
                    <span class="hours">
                    	<input id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" type="text" class="field text" value="" size="2" maxlength="2" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                    	<label for="Field'.$fieldId.'">
                    		HH
                    	</label>
                    </span>
                    <span class="symbol minutes">
                    	:
                    </span>
                    <span class="minutes">
                    	<input id="'.$extra.'Field'.$fieldId.'-1" name="'.$extra.'Field'.$fieldId.'-1" type="text" class="field text" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                    	<label for="Field'.$fieldId.'-1">
                    		MM
                    	</label>
                    </span>
                    <span class="symbol seconds">
                    	:
                    </span>
                    <span class="seconds">
                    	<input id="'.$extra.'Field'.$fieldId.'-2" name="'.$extra.'Field'.$fieldId.'-2" type="text" class="field text" value="" size="2" maxlength="2" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                    	<label for="Field'.$fieldId.'-2">
                    		SS
                    	</label>
                    </span>
                    <span class="ampm">
                    	<select id="'.$extra.'Field'.$fieldId.'-3" name="'.$extra.'Field'.$fieldId.'-3" class="field select" style="width:4em" tabindex="4" onclick="handleInput(this);" onkeyup="handleInput(this);" readonly="readonly" disabled="disabled">
                    		<option value="AM" selected="selected">
                    			AM
                    		</option>
                    		<option value="PM">
                    			PM
                    		</option>
                    	</select>
                    	<label for="Field'.$fieldId.'-3">
                    		AM/PM
                    	</label>
                    </span>';
            $html .= '';
        } elseif($type == 'phone') {
            $html .= '';
            $html .= '
                <span>
                	<input id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" type="tel" class="field text" value="" size="3" maxlength="3" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                	<label for="Field'.$fieldId.'">
                		###
                	</label>
                </span>
                <span class="symbol">
                	-
                </span>
                <span>
                	<input id="'.$extra.'Field'.$fieldId.'-1" name="'.$extra.'Field'.$fieldId.'-1" type="tel" class="field text" value="" size="3" maxlength="3" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                	<label for="Field'.$fieldId.'-1">
                		###
                	</label>
                </span>
                <span class="symbol">
                	-
                </span>
                <span>
                	<input id="'.$extra.'Field'.$fieldId.'-2" name="'.$extra.'Field'.$fieldId.'-2" type="tel" class="field text" value="" size="4" maxlength="4" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                	<label for="Field'.$fieldId.'-2">
                		####
                	</label>
                </span>';
            $html .= '';
        } elseif($type == 'url') {
            $html .= '<div>';
            $html .= '<input id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" type="url" class="field text medium" value="" maxlength="255" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>';
            $html .= '</div>';
        } elseif($type == 'money') {
            $html .= '';
            $html .= '
                <span class="symbol">
                	$
                </span>
                <span>
                	<input id="'.$extra.'Field'.$fieldId.'" name="'.$extra.'Field'.$fieldId.'" type="text" class="field text currency nospin" value="" size="10" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                	<label for="Field'.$fieldId.'">
                		Dollars
                	</label>
                </span>
                <span class="symbol radix">
                	.
                </span>
                <span class="cents">
                	<input id="'.$extra.'Field'.$fieldId.'-1" name="'.$extra.'Field'.$fieldId.'-1" type="text" class="field text nospin" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly"
                	disabled="disabled"/>
                	<label for="Field'.$fieldId.'-1">
                		Cents
                	</label>
                </span>';
            $html .= '';
        }
        return $html;
    }
    /* ---------------------------------------------------------------------------- */
    /* ------------------------ JSON ----------------------------- */
    /* ---------------------------------------------------------------------------- */
    function getHtmlHead($type, $title, $fieldId, $action, $extra = '') {
        $html = '';
        $classes = 'notranslate dragable';
        if($type == 'section') {
            $classes .= ' section';
        }
        if($type == 'likert') {
            $classes .= ' col4 likert';
        }
        $html .= '<li id="'.$extra.'foli'.$fieldId.'" title="Click to edit. Drag to reorder." class="'.$classes.'">';
        if(empty($extra)) $html .= '<img src="'.__ABS_PATH . '/template' .'/images/arrow.png" alt="" class="arrow" />';
        if($type == 'checkbox' || $type == 'radio' || $action == 'displayfield') {
            $html .= '<fieldset>';
        	$html .= '<![if !IE | (gte IE 8)]>';
        	$html .= '<legend id="'.$extra.'title'.$fieldId.'" class="desc">';
        	$html .= ucfirst($title);
        	$html .= '<span id="'.$extra.'req_'.$fieldId.'" class="req"></span>';
        	$html .= '</legend>';
        	$html .= '<![endif]>';
        	$html .= '<!--[if lt IE 8]>';
        	$html .= '<label id="'.$extra.'title'.$fieldId.'" class="desc">';
        	$html .= ucfirst($title);
        	$html .= '<span id="'.$extra.'req_'.$fieldId.'" class="req"></span>';
        	$html .= '</label>';
        	$html .= '<![endif]-->';
        } elseif($type == 'section' || $type == 'likert') {
        } else {
            $html .= '<label class="desc" id="'.$extra.'title'.$fieldId.'" for="'.$extra.'Field'.$fieldId.'">';
            $html .= ucfirst($title);
            $html .= '<span id="'.$extra.'req_'.$fieldId.'" class="req">';
            $html .= '</span>';
            $html .= '</label>';
        }
        return $html;
    }
    function getHtmlFoot($type, $fieldId, $action, $extra = '') {
        $html = '';
        if($type == 'checkbox' || $type == 'radio') {
            $html .= '</fieldset>';
        }
        $html .= '<p class="instruct hide" id="'.$extra.'instruct'.$fieldId.'">';
        $html .= '<small>';
        $html .= '</small>';
        $html .= '</p>';
        $html .= '<div id="'.$extra.'fa'.$fieldId.'" class="'.$extra.'fieldActions">';
        if(empty($extra)) $html .= '<img class="faDup" src="'.__ABS_PATH . '/template' .'/images/icons/add.png" alt="Duplicate." title="Duplicate." onclick="duplicateField('.$fieldId.', event); return false;" />';
        $html .= '<img class="faDel" src="'.__ABS_PATH . '/template' .'/images/icons/delete.png" alt="Delete." title="Delete." onclick="removeField('.$fieldId.', event); return false;" />';
        $html .= '</div>';
        $html .= '</li>';
        return $html;
    }
    function getJson_($type, $fieldId) {
        return array(
            'FieldId' => '0',
            'Title' => ucfirst($type),
            'Instructions' => '',
            'RightAnswerDescription' => '',
            'WrongAnswerDescription' => '',
            'InputRightAnswer' => '',
            'Typeof' => $type,
            'Size' => 'medium',
            'IsRequired' => '0',
            'IsUnique' => '0',
            'IsPrivate' => '0',
            'Validation' => '0',
            'Pos' => '0',
            'ColumnId' => $fieldId,
            'FormId' => '0',
            'Price' => '0',
            'ClassNames' => '',
            'IsEncrypted' => '0',
            'RangeType' => 'character',
            'RangeMin' => '0',
            'RangeMax' => '',
            'DefaultVal' => '',
            'ChoicesText' => '0',
            'Settings' => array(),
            'Page' => '',
            'MerchantEnabled' => false,
            'doValidateRequired' => true,
            'ExtraFields' => array()
        );
    }
    function getExtraJson_($type, $fieldId) {
        return array(
            'FieldId' => '0',
            'Title' => ucfirst($type),
            'Instructions' => '',
            'RightAnswerDescription' => '',
            'WrongAnswerDescription' => '',
            'InputRightAnswer' => '',
            'Typeof' => $type,
            'Size' => 'medium',
            'IsRequired' => '0',
            'IsUnique' => '0',
            'IsPrivate' => '0',
            'Validation' => '0',
            'Pos' => '0',
            'ColumnId' => $fieldId,
            'FormId' => '0',
            'Price' => '0',
            'ClassNames' => '',
            'IsEncrypted' => '0',
            'RangeType' => 'character',
            'RangeMin' => '0',
            'RangeMax' => '',
            'DefaultVal' => '',
            'ChoicesText' => '0',
            'Settings' => array(),
            'Page' => '',
            'MerchantEnabled' => false,
            'doValidateRequired' => true
        );
    }
    function getJsonDuplicate($field, $fieldId) {
        $field_ = $this->helpers->json_decode_to_array($field);
        $field_['ColumnId'] = $fieldId;
        if(isset($field_['ExtraFields'])) $field_['ExtraFields'] = array();
        return $field_;
    }
    function setJson($args) {
        foreach($args as $k => $arg) {
            $this->json[$k] = $arg;
        }
    }
    function getJson() {
        return json_encode($this->json);
    }
}