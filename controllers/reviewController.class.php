<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_reviewController extends Applications_BaseController {
    function userOnly($redirect) {
        $this->helpers->userNext($redirect);
    }
    function checkPermission() {
        $this->userOnly($this->helpers->getUrl() . 'auth/login&ref='.$this->helpers->curPageURL());
    }
    function index() {
        $notfound_obj = new Controllers_notfoundController($this->registry);
        $entries_obj = new Models_EntriesModel($this->registry);
        $forms_obj = new Models_FormsModel($this->registry);
        $logs_obj = new Models_FormsLogModel($this->registry);
		$routers = $this->registry->router->getRouters();
        $entryId = $routers['id'];
        $entryId = $this->helpers->escape_string($entryId);
        if(empty($entryId)) {
            $notfound_obj->index();
            return;
        }
        $entry_content = $entries_obj->getEntry($entryId,'','entry_uuid','',"AND `entry_type` = 'AS'");
        if(!$entry_content) {
            $notfound_obj->index();
            return;
        }
        $entry_id = $entry_content['entry_id'];
        $entry = $entries_obj->getEntryMeta("`entry_id` = '$entry_id'");
        $form_id = $entry['form_id'];
        $result = $forms_obj->getForm('',$form_id);
        if(!$result) {
            $notfound_obj->index();
            return;
        }
        $form_uuid = $result['form_uuid'];
        /*
        *
        */
        $caches_obj = new Controllers_cachesController($this->registry);
        $users_obj = new Models_UsersModel($this->registry);
        $comments_obj = new Models_CommentsModel($this->registry);
        $user_id = $entry_content['entry_user'];
        $user_nb = $users_obj->getUser("`user_id` = '$user_id'");
        $display_ = $caches_obj->getFormASDisplayResult($result,$entry_content,$user_nb, true);
        $form_content = $this->helpers->json_decode_to_array($result['form_content']);
        $_fields = $form_content['Fields'];
        $scoring_detail = $this->helpers->scoring_detail($_fields,$entry_content);
        $logs = array();
        foreach($scoring_detail as $sd) {
            $field_id = $sd['FieldId'];
            $log_content = $this->helpers->json_encode_then_escape($sd);
            $log_subject = $sd['Title'];
            if(is_string($sd['Value'])) $log_subject = $sd['Value'];
            $log_subject = $this->helpers->escape_string($log_subject);
            $log = $logs_obj->getLog("`form_id` = '$form_id' AND `field_id` = '$field_id' AND `log_subject` = '$log_subject'");
            if($log) {
                $log_id = $log['log_id'];
                
                $comments = $comments_obj->getCommentsOfLog($log_id, '\'0\'', 'comment_create', 'ASC', '3');
                $comments_count = $comments_obj->getCalcNumRows();
                if($comments) foreach($comments as $k => $comment) {
                    $comment_user = $comment['comment_user'];
                    $comment['user'] = $users_obj->getUser("`user_id` = '$comment_user'");
                    $comments[$k] = $comment;
                }
                $log['comments'] = $comments;
                $log['total_comments'] = $comments_count;
                $logs[] = $log;
            }
        }
        $this->view->form_uuid = $form_uuid;
        $this->view->scoring = $scoring_detail;
        $this->view->logs = $logs;
        $this->view->display_ = $display_;
        $this->view->result = $result;
        $this->view->display_ = $display_;
        $this->view->render('yourscore','only');
    }
    function postReq() {
        $req = $this->registry->req;
        $ajax = new Controllers_AjaxController($this->registry);
        $ajax->index($req);
        echo $ajax->getJson();
    }
}