<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_confirmController extends Applications_BaseController {
    function index() {
        $routers = $this->registry->router->getRouters();
        $notfound_obj = new Controllers_notfoundController($this->registry);
        $entries_obj = new Models_EntriesModel($this->registry);
        $forms_obj = new Models_FormsModel($this->registry);
        $complete = $dupl = false; $_form = array();$message = '';
        $entry_id = isset($_GET['a'])?$_GET['a']:'';
        $formId = $routers['id'];
        if(!empty($entry_id) && strlen($entry_id)>32) {
            $entry_id = str_replace(substr($entry_id,-32),'',$entry_id);
            $entry_id = $this->helpers->escape_string($entry_id);
            $entry_result = $entries_obj->getEntries("`entry_id` = '$entry_id' AND `entry_complete` = '0'",'','','1');
            if($entry_result) {
                $entries_obj->updateEntry("`entry_complete` = '1'","`entry_id` = '$entry_id'",'1');
                $complete = true;
            } else {
                $dupl = true;
            }
        }
        if($dupl) {
            $notfound_obj->index();
            return;
        } else {
        	$formId = $this->helpers->escape_string($formId);
        	$result = $forms_obj->getForm($formId,'');
        	if(empty($formId) && !empty($entry_id)) {
        		$entry_result = $entries_obj->getEntryMeta("`entry_id` = '$entry_id'");
        		$formId = $entry_result['form_id'];
        		$formId = $this->helpers->escape_string($formId);
        		$result = $forms_obj->getForm('',$formId);
        		$message = 'Thanks for complete your submission!';
        	}
            if(!$result) {
                $notfound_obj->index();
                return;
            }
            $_form = $this->helpers->json_decode_to_array($result['form_content']);
            $fields = $_form['Fields'];
            $sendConfirm = $this->helpers->checkSendConfirm($fields);
            if(!empty($sendConfirm) && empty($message)) {
                $message = 'Please Confirm Your Submission By Check Your Email Inbox!';
            }
            $r_message = isset($_form['RedirectMessage'])?$_form['RedirectMessage']:'<h2>Great! Thanks for filling out my form!</h2>';
            
        }
        $this->view->complete = $complete;
        $this->view->message = $message;
        $this->view->r_message = $r_message;
        $this->view->title = 'Confirmation';
		$this->view->render('confirm');
    }
}