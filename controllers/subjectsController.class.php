<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_subjectsController extends Applications_BaseController {
    public static $form_limit = 15;
    public function adminOnly($redirect) {
        $this->helpers->adminNext($redirect);
    }
    function checkPermission() {
        return;
        //$this->adminOnly($this->helpers->getUrl() . 'auth/login/&ref='.$this->helpers->curPageURL());
    }
    function manager() {
        $this->adminOnly($this->helpers->getUrl() . 'auth/login/&ref='.$this->helpers->curPageURL());
        $forms_obj = new Models_FormsModel($this->registry);
        $subjects_obj = new Models_SubjectsModel($this->registry);
        
        $subjects_result = $subjects_obj->getSubjects('*','','subject_create','DESC');
        $forms_result = $forms_obj->getForms('*',"`form_approved` = 'Y'",'','',self::$form_limit);
        
        $subjects_result = ($subjects_result)?$subjects_result:array();
        $forms_result = ($forms_result)?$forms_result:array();
        foreach($subjects_result as $ks => $subject) {
            $subject_id = $subject['subject_id'];
            $subjects_obj->getSUs('SQL_CALC_FOUND_ROWS *',"`subject_id` = '$subject_id'",'','','1');
            $subject['user_count'] = $subjects_obj->getCalcNumRows();
            $subjects_result[$ks] = $subject;
        }
        $this->view->count = count($subjects_result);
        $this->view->form_count = count($forms_result);
        $this->view->subjects_result = $subjects_result;
        $this->view->forms_result = $forms_result;
        
        $this->view->title = 'Subjects Manager';
		$this->view->render('subjects_manager');
    }
    function view() {
        $forms_obj = new Models_FormsModel($this->registry);
        $subjects_obj = new Models_SubjectsModel($this->registry);
        $notfound_obj = new Controllers_notfoundController($this->registry);
        $routers = $this->registry->router->getRouters();
        $subject_uuid = $routers['id'];
        $this->view->subject_uuid = $subject_uuid;
        $form_ids = array();
        if(empty($subject_uuid)) {
            $notfound_obj->index();
            exit();
        }
        $subject_uuid = $this->helpers->escape_string($subject_uuid);
        $subject_result = $subjects_obj->getSubject("`subject_uuid` = '$subject_uuid'");
        
        if(!$subject_result) {
            $notfound_obj->index();
            exit();
        }
        $subject_id = $subject_result['subject_id'];
        $subject_meta_results = $subjects_obj->getSubjectsMeta('*',"`subject_id` = '$subject_id'");
        $sf_results = $subjects_obj->getSFs('*',"`subject_id` = '$subject_id'");
        $sf_results = ($sf_results)?$sf_results:array();
        foreach($sf_results as $sf_result) {
            $form_ids[] = $sf_result['form_id'];
        }
        $ids_string = implode(',',$form_ids);
        
        $settings = $this->helpers->json_decode_to_array($subject_result['subject_settings']);
        $form_order = isset($settings['FormOrder'])?$settings['FormOrder']:array();
        $sf_r = array();
        if(!empty($form_order)) {
            foreach($form_order as $fo) {
                $sf_r[] = $forms_obj->getForm('',$fo);
            }
        } else {
            $sf_r = $forms_obj->getForms('*',"`form_id` IN ($ids_string)",'form_create','DESC');
        }
        $sf_r = ($sf_r)?$sf_r:array();
        $user_id = $this->helpers->getCurrentUser();
        $su_result = $subjects_obj->getSU("`subject_id` = '$subject_id' AND `user_id` = '$user_id'");
        $this->view->su_result = $su_result;
        $this->view->sf_results = $sf_r;
        $this->view->subject_result = $subject_result;
        $this->view->subject_meta_results = $subject_meta_results;
        $this->view->user_id = $user_id;
        $this->view->title = $subject_result['subject_name'];
		$this->view->render('subjects_view');
    }
    function all() {
        $forms_obj = new Models_FormsModel($this->registry);
        $subjects_obj = new Models_SubjectsModel($this->registry);
        
        $subjects_result = $subjects_obj->getSubjects('*','','subject_create','DESC');
        
        $subjects_result = ($subjects_result)?$subjects_result:array();
        
        $this->view->subjects_result = $subjects_result;
        
        $this->view->title = 'Subjects';
		$this->view->render('subjects_all');
    }
    function index() {
        $notfound_obj = new Controllers_notfoundController($this->registry);
        $notfound_obj->index();
    }
    function builder() {
        $this->adminOnly($this->helpers->getUrl() . 'auth/login/&ref='.$this->helpers->curPageURL());
        $forms_obj = new Models_FormsModel($this->registry);
        $subjects_obj = new Models_SubjectsModel($this->registry);
        $notfound_obj = new Controllers_notfoundController($this->registry);
        $routers = $this->registry->router->getRouters();
        $subject_uuid = $routers['id'];
        $this->view->subject_uuid = $subject_uuid;
        $form_ids = array();
        $ids_string = '';
        $user_count = 0;
        if(!empty($subject_uuid)) {
            $subject_uuid = $this->helpers->escape_string($subject_uuid);
            $subject_result = $subjects_obj->getSubject("`subject_uuid` = '$subject_uuid'");
            
            if(!$subject_result) {
                $notfound_obj->index();
                exit();
            }
            $subject_id = $subject_result['subject_id'];
            $subject_meta_results = $subjects_obj->getSubjectsMeta('*',"`subject_id` = '$subject_id'");
            $sf_results = $subjects_obj->getSFs('*',"`subject_id` = '$subject_id'");
            $sf_results = ($sf_results)?$sf_results:array();
            foreach($sf_results as $sf_result) {
                $form_ids[] = $sf_result['form_id'];
            }
            $ids_string = implode(',',$form_ids);
            $sf_results = $forms_obj->getForms('*',"`form_id` IN ($ids_string)",'form_create','ASC');
            $this->view->sf_results = $sf_results;
            $this->view->subject_result = $subject_result;
            $this->view->subject_settings = $this->helpers->json_decode_to_array($subject_result['subject_settings']);
            $this->view->subject_meta_results = $subject_meta_results;
            $subjects_obj->getSUs('SQL_CALC_FOUND_ROWS *',"`subject_id` = '$subject_id'",'','','1');
            $user_count = $subjects_obj->getCalcNumRows();
        }
        if(empty($ids_string)) $ids_string = "'0'";
        $forms_result = $forms_obj->getForms('SQL_CALC_FOUND_ROWS *',"`form_approved` = 'Y' AND `form_id` NOT IN ($ids_string)",'form_create','DESC', self::$form_limit);
        $forms_result = ($forms_result)?$forms_result:array();
        $this->view->forms_result = $forms_result;
        $this->view->rows_limit = self::$form_limit;
        $this->view->total_rows = $forms_obj->getCalcNumRows();
        $this->view->user_count = $user_count;
        $this->view->title = 'Subjects Builder';
		$this->view->render('subjects_builder');
    }
    function postReq() {
        $req = $this->registry->req;
        $ajax = new Controllers_AjaxController($this->registry);
        $ajax->index($req);
        echo $ajax->getJson();
    }
}