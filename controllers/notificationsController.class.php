<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_notificationsController extends Applications_BaseController {
    public function adminOnly($redirect) {
        $this->helpers->adminNext($redirect);
    }
    function checkPermission() {
        $this->adminOnly($this->helpers->getUrl() . 'auth/login/&ref='.$this->helpers->curPageURL());
    }
    function index() {
        $notfound_obj = new Controllers_notfoundController($this->registry);
        $routers = $this->registry->router->getRouters();
        $formId = $routers['id'];
		if(!empty($formId)) {
            $forms_obj = new Models_FormsModel($this->registry);
            $formId = $this->helpers->escape_string($formId);
            if($result = $forms_obj->getForm($formId,'')) {
                $_form = $this->helpers->json_decode_to_array($result['form_content']);
                $_notify = $this->helpers->json_decode_to_array($result['form_notify']);
                $emails = array();
                $fields = $_form['Fields'];
                if(!empty($fields)) {
                    foreach($fields as $field) {
                        if($field['Typeof'] == 'email') {
                            $emails[] = $field;
                        }
                    }
                }
            } else {
                $notfound_obj->index();
                exit();
            }
        } else {
            $notfound_obj->index();
            exit();
        }
        $this->view->result = $result;
        $this->view->emails = $emails;
        $this->view->fields = $fields;
        $this->view->_form = $_form;
        $this->view->_notify = $_notify;
        $this->view->render('notifications');
    }
    function postReq() {
        $req = $this->registry->req;
        $ajax = new Controllers_AjaxController($this->registry);
        $ajax->index($req);
        echo $ajax->getJson();
    }
}