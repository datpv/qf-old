<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Controllers_buildController extends Applications_BaseController {
    public function adminOnly($redirect) {
        $this->helpers->adminNext($redirect);
    }
    function checkPermission() {
        $this->adminOnly($this->helpers->getUrl() . 'auth/login/&ref='.$this->helpers->curPageURL());
    }
    function index() {
        $forms_obj = new Models_FormsModel($this->registry);
        $fields_obj = new Models_FieldsModel($this->registry);
        $routers = $this->registry->router->getRouters();
        $formId = $routers['id'];
        /*
        * Get Form
        */
        $hasFieldClass = '';
        $buttonClass = '';
        $form_result = $forms_obj->getForm($formId,'');
        $form_content = $this->helpers->json_decode_to_array($form_result['form_content']);
        $html = $formScoreUrl = '';
        $formType = isset($_GET['formType'])?$_GET['formType']:'2';
        if(!empty($formId)) {
            $formId = $this->helpers->escape_string($formId);
            if($result = $forms_obj->getForm($formId,'')) {
                $form = $result['form_content'];
                $_form = $this->helpers->json_decode_to_array($form);
                $_form['FormId'] = $formId;
                $_form['Url'] = $formId;
                $formType = isset($_form['Formtype'])?$_form['Formtype']:'normal';
                $formScoreUrl = isset($_form['FormScoreUrl'])?$_form['FormScoreUrl']:'normal';
                if(isset($_form['Fields']['HasntBeenSaved']))
                unset($_form['Fields']['HasntBeenSaved']);
            
            
            $fields = $_form['Fields'];
            $hasFieldClass = '';
            $buttonClass = 'hide';
            if(!empty($fields)) {
                $hasFieldClass = 'hasFields';
                $buttonClass = '';
                $this->helpers->aasort($fields,"Pos");
                foreach($fields as $field) {
                    $extra_fields = isset($field['ExtraFields'])?$field['ExtraFields']:array();
                    $classes = 'notranslate dragable';
                    ob_start();
                    $type = $field['Typeof'];
                    $fieldId = $field['ColumnId'];
                    $fieldTitle = $field['Title'];
                    $settings = $field['Settings'];
                    $fieldInstruction = $field['Instructions'];
                    $subFields = isset($field['SubFields'])?$field['SubFields']:array();
                    $choices = isset($field['Choices'])?$field['Choices']:array();
                    if($type == 'section') {
                        $classes .= ' section';
                    }
                    if($type == 'likert') {
                        $classes .= ' col4 likert';
                    }
                    if(isset($settings['hideNumbers']) || in_array('hideNumbers',$settings)) {
                        $classes .= ' hideNumbers';
                    }
                    if(isset($field['ClassNames'])) {
                        $classes .= ' ' . $field['ClassNames'];
                    }
                    if($type == 'date' || $type == 'eurodate') {
                        $classes .= ' '.$type;
                    } elseif($type == 'time') {
                        $classes .= ' time';
                    } elseif($type == 'phone') {
                        $classes .= ' phone';
                    } elseif($type == 'address') {
                        $classes .= ' complex';
                    }
        /*
        * FORM BUILDER
        */
        
        ?>
        <li id="foli<?php echo $fieldId; ?>" title="Click to edit. Drag to reorder." class="<?php echo $classes; ?>">
        	<img src="template/images/arrow.png" alt="" class="arrow" />
            <?php
            if($type == 'checkbox' || $type == 'radio') {
            ?>
                <fieldset>
            	<![if !IE | (gte IE 8)]>
            	<legend id="title<?php echo $fieldId; ?>" class="desc">
            	<?php echo $fieldTitle; ?>
            	<span id="req_<?php echo $fieldId; ?>" class="req"><?php echo ($field['IsRequired'])?'*':''; ?></span>
            	</legend>
            	<![endif]>
            	<!--[if lt IE 8]>
            	<label id="title<?php echo $fieldId; ?>" class="desc">
            	<?php echo $fieldTitle; ?>
            	<span id="req_<?php echo $fieldId; ?>" class="req"><?php echo ($field['IsRequired'])?'*':''; ?></span>
            	</label>
            	<![endif]-->
            <?php
            } elseif($type == 'section' || $type == 'likert') {
            } else {
            ?>
                <label class="desc" id="title<?php echo $fieldId; ?>" for="Field<?php echo $fieldId; ?>">
                <?php echo $fieldTitle; ?>
                <span id="req_<?php echo $fieldId; ?>" class="req"><?php echo ($field['IsRequired'])?'*':''; ?>
                </span>
                </label>
            <?php
            }
            if($type == 'text' || $type == 'email' || $type == 'url' || $type == 'number') {
            ?>
        	<div>
        		<input id="Field<?php echo $fieldId; ?>" name="Field<?php echo $fieldId; ?>" type="text" class="field text nospin medium" value="<?php echo $field['DefaultVal']; ?>" tabindex="<?php echo $fieldId; ?>" onkeyup="handleInput(this); " onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
        	</div>
            <?php
            } elseif($type == 'shortname') {
            ?>
        	<span>
        		<input id="Field<?php echo $fieldId; ?>_0" name="Field<?php echo $fieldId; ?>" type="text" class="field text fn" value="" size="8" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
        		<label for="Field<?php echo $fieldId; ?>_1">
        			First
        		</label>
        	</span>
        	<span>
        		<input id="Field<?php echo $fieldId; ?>_1" name="Field<?php echo $fieldId; ?>" type="text" class="field text ln" value="" size="14" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
        		<label for="Field<?php echo $fieldId; ?>_1">
        			Last
        		</label>
        	</span>
            <?php
            } elseif($type == 'address') {
            ?>
            <div>
                <span class="full addr1"> <input id="Field" name="Field" type="text" class="field text addr" value="" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="Field"> Street Address </label> </span> <span class="full addr2"> <input id="Field" name="Field" type="text" class="field text addr" value="" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="Field"> Address Line 2 </label> </span> <span class="left city"> <input id="Field" name="Field" type="text" class="field text addr" value="" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="Field"> City </label> </span> <span class="right state"> <input id="Field" name="Field" type="text" class="field text addr" value="" tabindex="4" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="Field"> State / Province / Region </label> </span> <span class="left zip"> <input id="Field" name="Field" type="text" class="field text addr" value="" maxlength="15" tabindex="5" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="Field"> Postal / Zip Code </label> </span> <span class="right country"> <select id="Field" name="Field" class="field select addr" tabindex="6" onclick="handleInput(this);" onkeyup="handleInput(this);" readonly="readonly" disabled="disabled"> <option value="" selected="selected"> </option> <option value="United States"> United States </option> <option value="United Kingdom"> United Kingdom </option> <option value="Australia"> Australia </option> <option value="Canada"> Canada </option> <option value="France"> France </option> <option value="New Zealand"> New Zealand </option> <option value="India"> India </option> <option value="Brazil"> Brazil </option> <option value="----"> ---- </option> <option value="Afghanistan"> Afghanistan </option> <option value="Åland Islands"> Åland Islands </option> <option value="Albania"> Albania </option> <option value="Algeria"> Algeria </option> <option value="American Samoa"> American Samoa </option> <option value="Andorra"> Andorra </option> <option value="Angola"> Angola </option> <option value="Anguilla"> Anguilla </option> <option value="Antarctica"> Antarctica </option> <option value="Antigua and Barbuda"> Antigua and Barbuda </option> <option value="Argentina"> Argentina </option> <option value="Armenia"> Armenia </option> <option value="Aruba"> Aruba </option> <option value="Austria"> Austria </option> <option value="Azerbaijan"> Azerbaijan </option> <option value="Bahamas"> Bahamas </option> <option value="Bahrain"> Bahrain </option> <option value="Bangladesh"> Bangladesh </option> <option value="Barbados"> Barbados </option> <option value="Belarus"> Belarus </option> <option value="Belgium"> Belgium </option> <option value="Belize"> Belize </option> <option value="Benin"> Benin </option> <option value="Bermuda"> Bermuda </option> <option value="Bhutan"> Bhutan </option> <option value="Bolivia"> Bolivia </option> <option value="Bosnia and Herzegovina"> Bosnia and Herzegovina </option> <option value="Botswana"> Botswana </option> <option value="British Indian Ocean Territory"> British Indian Ocean Territory </option> <option value="Brunei Darussalam"> Brunei Darussalam </option> <option value="Bulgaria"> Bulgaria </option> <option value="Burkina Faso"> Burkina Faso </option> <option value="Burundi"> Burundi </option> <option value="Cambodia"> Cambodia </option> <option value="Cameroon"> Cameroon </option> <option value="Cape Verde"> Cape Verde </option> <option value="Cayman Islands"> Cayman Islands </option> <option value="Central African Republic"> Central African Republic </option> <option value="Chad"> Chad </option> <option value="Chile"> Chile </option> <option value="China"> China </option> <option value="Colombia"> Colombia </option> <option value="Comoros"> Comoros </option> <option value="Democratic Republic of the Congo"> Democratic Republic of the Congo </option> <option value="Republic of the Congo"> Republic of the Congo </option> <option value="Cook Islands"> Cook Islands </option> <option value="Costa Rica"> Costa Rica </option> <option value="C&ocirc;te dIvoire"> C&ocirc;te dIvoire </option> <option value="Croatia"> Croatia </option> <option value="Cuba"> Cuba </option> <option value="Cyprus"> Cyprus </option> <option value="Czech Republic"> Czech Republic </option> <option value="Denmark"> Denmark </option> <option value="Djibouti"> Djibouti </option> <option value="Dominica"> Dominica </option> <option value="Dominican Republic"> Dominican Republic </option> <option value="East Timor"> East Timor </option> <option value="Ecuador"> Ecuador </option> <option value="Egypt"> Egypt </option> <option value="El Salvador"> El Salvador </option> <option value="Equatorial Guinea"> Equatorial Guinea </option> <option value="Eritrea"> Eritrea </option> <option value="Estonia"> Estonia </option> <option value="Ethiopia"> Ethiopia </option> <option value="Faroe Islands"> Faroe Islands </option> <option value="Fiji"> Fiji </option> <option value="Finland"> Finland </option> <option value="Gabon"> Gabon </option> <option value="Gambia"> Gambia </option> <option value="Georgia"> Georgia </option> <option value="Germany"> Germany </option> <option value="Ghana"> Ghana </option> <option value="Gibraltar"> Gibraltar </option> <option value="Greece"> Greece </option> <option value="Grenada"> Grenada </option> <option value="Guatemala"> Guatemala </option> <option value="Guinea"> Guinea </option> <option value="Guinea-Bissau"> Guinea-Bissau </option> <option value="Guyana"> Guyana </option> <option value="Haiti"> Haiti </option> <option value="Honduras"> Honduras </option> <option value="Hong Kong"> Hong Kong </option> <option value="Hungary"> Hungary </option> <option value="Iceland"> Iceland </option> <option value="Indonesia"> Indonesia </option> <option value="Iran"> Iran </option> <option value="Iraq"> Iraq </option> <option value="Ireland"> Ireland </option> <option value="Israel"> Israel </option> <option value="Italy"> Italy </option> <option value="Jamaica"> Jamaica </option> <option value="Japan"> Japan </option> <option value="Jordan"> Jordan </option> <option value="Kazakhstan"> Kazakhstan </option> <option value="Kenya"> Kenya </option> <option value="Kiribati"> Kiribati </option> <option value="North Korea"> North Korea </option> <option value="South Korea"> South Korea </option> <option value="Kuwait"> Kuwait </option> <option value="Kyrgyzstan"> Kyrgyzstan </option> <option value="Laos"> Laos </option> <option value="Latvia"> Latvia </option> <option value="Lebanon"> Lebanon </option> <option value="Lesotho"> Lesotho </option> <option value="Liberia"> Liberia </option> <option value="Libya"> Libya </option> <option value="Liechtenstein"> Liechtenstein </option> <option value="Lithuania"> Lithuania </option> <option value="Luxembourg"> Luxembourg </option> <option value="Macedonia"> Macedonia </option> <option value="Madagascar"> Madagascar </option> <option value="Malawi"> Malawi </option> <option value="Malaysia"> Malaysia </option> <option value="Maldives"> Maldives </option> <option value="Mali"> Mali </option> <option value="Malta"> Malta </option> <option value="Marshall Islands"> Marshall Islands </option> <option value="Mauritania"> Mauritania </option> <option value="Mauritius"> Mauritius </option> <option value="Mexico"> Mexico </option> <option value="Micronesia"> Micronesia </option> <option value="Moldova"> Moldova </option> <option value="Monaco"> Monaco </option> <option value="Mongolia"> Mongolia </option> <option value="Montenegro"> Montenegro </option> <option value="Morocco"> Morocco </option> <option value="Mozambique"> Mozambique </option> <option value="Myanmar"> Myanmar </option> <option value="Namibia"> Namibia </option> <option value="Nauru"> Nauru </option> <option value="Nepal"> Nepal </option> <option value="Netherlands"> Netherlands </option> <option value="Netherlands Antilles"> Netherlands Antilles </option> <option value="Nicaragua"> Nicaragua </option> <option value="Niger"> Niger </option> <option value="Nigeria"> Nigeria </option> <option value="Norway"> Norway </option> <option value="Oman"> Oman </option> <option value="Pakistan"> Pakistan </option> <option value="Palau"> Palau </option> <option value="Palestine"> Palestine </option> <option value="Panama"> Panama </option> <option value="Papua New Guinea"> Papua New Guinea </option> <option value="Paraguay"> Paraguay </option> <option value="Peru"> Peru </option> <option value="Philippines"> Philippines </option> <option value="Poland"> Poland </option> <option value="Portugal"> Portugal </option> <option value="Puerto Rico"> Puerto Rico </option> <option value="Qatar"> Qatar </option> <option value="Romania"> Romania </option> <option value="Russia"> Russia </option> <option value="Rwanda"> Rwanda </option> <option value="Saint Kitts and Nevis"> Saint Kitts and Nevis </option> <option value="Saint Lucia"> Saint Lucia </option> <option value="Saint Vincent and the Grenadines"> Saint Vincent and the Grenadines </option> <option value="Samoa"> Samoa </option> <option value="San Marino"> San Marino </option> <option value="Sao Tome and Principe"> Sao Tome and Principe </option> <option value="Saudi Arabia"> Saudi Arabia </option> <option value="Senegal"> Senegal </option> <option value="Serbia and Montenegro"> Serbia and Montenegro </option> <option value="Seychelles"> Seychelles </option> <option value="Sierra Leone"> Sierra Leone </option> <option value="Singapore"> Singapore </option> <option value="Slovakia"> Slovakia </option> <option value="Slovenia"> Slovenia </option> <option value="Solomon Islands"> Solomon Islands </option> <option value="Somalia"> Somalia </option> <option value="South Africa"> South Africa </option> <option value="Spain"> Spain </option> <option value="Sri Lanka"> Sri Lanka </option> <option value="Sudan"> Sudan </option> <option value="Suriname"> Suriname </option> <option value="Swaziland"> Swaziland </option> <option value="Sweden"> Sweden </option> <option value="Switzerland"> Switzerland </option> <option value="Syria"> Syria </option> <option value="Taiwan"> Taiwan </option> <option value="Tajikistan"> Tajikistan </option> <option value="Tanzania"> Tanzania </option> <option value="Thailand"> Thailand </option> <option value="Togo"> Togo </option> <option value="Tonga"> Tonga </option> <option value="Trinidad and Tobago"> Trinidad and Tobago </option> <option value="Tunisia"> Tunisia </option> <option value="Turkey"> Turkey </option> <option value="Turkmenistan"> Turkmenistan </option> <option value="Tuvalu"> Tuvalu </option> <option value="Uganda"> Uganda </option> <option value="Ukraine"> Ukraine </option> <option value="United Arab Emirates"> United Arab Emirates </option> <option value="United States Minor Outlying Islands"> United States Minor Outlying Islands </option> <option value="Uruguay"> Uruguay </option> <option value="Uzbekistan"> Uzbekistan </option> <option value="Vanuatu"> Vanuatu </option> <option value="Vatican City"> Vatican City </option> <option value="Venezuela"> Venezuela </option> <option value="Vietnam"> Vietnam </option> <option value="Virgin Islands, British"> Virgin Islands, British </option> <option value="Virgin Islands, U.S."> Virgin Islands, U.S. </option> <option value="Yemen"> Yemen </option> <option value="Zambia"> Zambia </option> <option value="Zimbabwe"> Zimbabwe </option> </select> <label for="Field"> Country </label> </span>
            </div>
            <?php
            } elseif($type == 'eurodate') {
            ?>
            <span>
        		<input id="Field<?php echo $fieldId; ?>-2" name="Field<?php echo $fieldId; ?>-2" type="text" class="field text" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
        		<label for="Field<?php echo $fieldId; ?>-2">
        			DD
        		</label>
        	</span>
        	<span class="symbol">
        		/
        	</span>
            <span>
        		<input id="Field<?php echo $fieldId; ?>-1" name="Field<?php echo $fieldId; ?>-1" type="text" class="field text" value="" size="2" maxlength="2" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
        		<label for="Field<?php echo $fieldId; ?>-1">
        			MM
        		</label>
        	</span>
        	<span class="symbol">
        		/
        	</span>
        	<span>
        		<input id="Field<?php echo $fieldId; ?>" name="Field<?php echo $fieldId; ?>" type="text" class="field text" value="" size="4" maxlength="4" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
        		<label for="Field<?php echo $fieldId; ?>">
        			YYYY
        		</label>
        	</span>
        	<span id="cal<?php echo $fieldId; ?>">
        		<img id="pick<?php echo $fieldId; ?>" class="datepicker" src="template/images/icons/calendar.png" alt="Pick a date." />
        	</span>
            <?php 
            } elseif($type == 'date') {
            ?>
            <span>
            	<input id="Field<?php echo $fieldId; ?>-1" name="Field<?php echo $fieldId; ?>-1" type="text" class="field text" value="" size="2" maxlength="2" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
            	<label for="Field<?php echo $fieldId; ?>-1">
            		MM
            	</label>
            </span>
            <span class="symbol">
            	/
            </span>
            <span>
            	<input id="Field<?php echo $fieldId; ?>-2" name="Field<?php echo $fieldId; ?>-2" type="text" class="field text" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
            	<label for="Field<?php echo $fieldId; ?>-2">
            		DD
            	</label>
            </span>
            <span class="symbol">
            	/
            </span>
            <span>
            	<input id="Field<?php echo $fieldId; ?>" name="Field<?php echo $fieldId; ?>" type="text" class="field text" value="" size="4" maxlength="4" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
            	<label for="Field<?php echo $fieldId; ?>">
            		YYYY
            	</label>
            </span>
            <span id="cal<?php echo $fieldId; ?>">
            	<img id="pick<?php echo $fieldId; ?>" class="datepicker" src="template/images/icons/calendar.png" alt="Pick a date." />
            </span>
            <?php 
            } elseif($type == 'time') {
            ?>
            <span class="hours">
                        	<input id="Field<?php echo $fieldId; ?>" name="Field<?php echo $fieldId; ?>" type="text" class="field text" value="" size="2" maxlength="2" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                        	<label for="Field<?php echo $fieldId; ?>">
                        		HH
                        	</label>
                        </span>
                        <span class="symbol minutes">
                        	:
                        </span>
                        <span class="minutes">
                        	<input id="Field<?php echo $fieldId; ?>-1" name="Field<?php echo $fieldId; ?>-1" type="text" class="field text" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                        	<label for="Field<?php echo $fieldId; ?>-1">
                        		MM
                        	</label>
                        </span>
                        <span class="symbol seconds">
                        	:
                        </span>
                        <span class="seconds">
                        	<input id="Field<?php echo $fieldId; ?>-2" name="Field<?php echo $fieldId; ?>-2" type="text" class="field text" value="" size="2" maxlength="2" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                        	<label for="Field<?php echo $fieldId; ?>-2">
                        		SS
                        	</label>
                        </span>
                        <span class="ampm">
                        	<select id="Field<?php echo $fieldId; ?>-3" name="Field<?php echo $fieldId; ?>-3" class="field select" style="width:4em" tabindex="4" onclick="handleInput(this);" onkeyup="handleInput(this);" readonly="readonly" disabled="disabled">
                        		<option value="AM" selected="selected">
                        			AM
                        		</option>
                        		<option value="PM">
                        			PM
                        		</option>
                        	</select>
                        	<label for="Field<?php echo $fieldId; ?>-3">
                        		AM/PM
                        	</label>
                        </span>
            <?php
            } elseif($type == 'phone') {
            ?>
            <span>
                    	<input id="Field<?php echo $fieldId; ?>" name="Field<?php echo $fieldId; ?>" type="tel" class="field text" value="" size="3" maxlength="3" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                    	<label for="Field<?php echo $fieldId; ?>">
                    		###
                    	</label>
                    </span>
                    <span class="symbol">
                    	-
                    </span>
                    <span>
                    	<input id="Field<?php echo $fieldId; ?>-1" name="Field<?php echo $fieldId; ?>-1" type="tel" class="field text" value="" size="3" maxlength="3" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                    	<label for="Field<?php echo $fieldId; ?>-1">
                    		###
                    	</label>
                    </span>
                    <span class="symbol">
                    	-
                    </span>
                    <span>
                    	<input id="Field<?php echo $fieldId; ?>-2" name="Field<?php echo $fieldId; ?>-2" type="tel" class="field text" value="" size="4" maxlength="4" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                    	<label for="Field<?php echo $fieldId; ?>-2">
                    		####
                    	</label>
                    </span>
            <?php
            } elseif($type == 'money') {
            ?>
            <span class="symbol">
            	$
            </span>
            <span>
            	<input id="Field<?php echo $fieldId; ?>" name="Field<?php echo $fieldId; ?>" type="text" class="field text currency nospin" value="" size="10" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" />
            	<label for="Field<?php echo $fieldId; ?>">
            		Dollars
            	</label>
            </span>
            <span class="symbol radix">
            	.
            </span>
            <span class="cents">
            	<input id="Field<?php echo $fieldId; ?>-1" name="Field<?php echo $fieldId; ?>-1" type="text" class="field text nospin" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" />
            	<label for="Field<?php echo $fieldId; ?>-1">
            		Cents
            	</label>
            </span>
            <?php
            } elseif($type == 'textarea') {
            ?>
            <div>
        		<div class="handle"></div><textarea id="Field<?php echo $fieldId; ?>" name="Field<?php echo $fieldId; ?>" class="field textarea medium" spellcheck="true" rows="10" cols="50" tabindex="<?php echo $fieldId; ?>" onkeyup="handleInput(this); " onchange="handleInput(this);" readonly="readonly" disabled="disabled"><?php echo $field['DefaultVal']; ?></textarea>
        	</div>
            <?php
            } elseif($type == 'checkbox') {
            ?>
            <div>
            <?php
                
                if(!empty($subFields)) {
                    foreach($subFields as $v) {
            ?>
        		<span>
        			<input id="Field" name="Field" type="checkbox" class="field checkbox" value="<?php echo $v['ChoicesText']; ?>" tabindex="1" onchange="handleInput(this);" readonly="readonly" disabled="disabled" <?php echo $v['DefaultVal']?'checked="checked"':''; ?> />
        			<label class="choice" for="Field">
        				<?php echo $v['ChoicesText']; ?>
        			</label>
        		</span>
            <?php
                    }
                }
            ?>
            </div>
            <?php
            } elseif($type == 'radio') {
            ?>
            <div>
            <input id="radioDefault_<?php echo $fieldId; ?>" name="Field<?php echo $fieldId; ?>" type="hidden" value="" />
            <?php
                
                if(!empty($choices)) {
                    $i = 0;
                    foreach($choices as $v) {
            ?>
                
        		<span>
        			<input id="Field<?php echo $fieldId; ?>_<?php echo $i; ?>" name="Field<?php echo $fieldId; ?>" type="radio" class="field radio" value="<?php echo $v['Choice']; ?>" tabindex="<?php echo $i; ?>" onchange="handleInput(this);" onmouseup="handleInput(this);" <?php echo $v['IsDefault']?'checked="checked"':''; ?> readonly="readonly" disabled="disabled"/>
                    <label class="choice" for="Field<?php echo $fieldId; ?>_<?php echo $i; ?>">
                    <?php echo $v['Choice']; ?>
                    </label>
        		</span>
                <?php if($v['Choice'] == 'Other') { ?>
                    <input id="Field<?php echo $fieldId; ?>_other" name="Field<?php echo $fieldId; ?>_other_Other" type="text" class="field text other" value="" onclick="document.getElementById(\'Field<?php echo $fieldId; ?>_<?php echo $i; ?>\').checked = \'checked\';" onkeyup="handleInput(this);" onchange="handleInput(this);"
        			tabindex="<?php echo $i; ?>" />
                <?php }
                    }
                }
            ?>
            </div>
            <?php
            } elseif($type == 'select') {
            ?>
            <div>
            <select id="Field<?php echo $fieldId; ?>" name="Field<?php echo $fieldId; ?>" class="field select medium" onclick="handleInput(this);" onkeyup="handleInput(this);" tabindex="<?php echo $fieldId; ?>" readonly="readonly" disabled="disabled">
            <?php
                if(!empty($choices)) {
                    foreach($choices as $v) {
            ?>
        			<option <?php echo ($v['IsDefault'])?'selected="selected"':''; ?> value="<?php echo $v['Choice']; ?>">
        				<?php echo $v['Choice']; ?>
        			</option>
            <?php
                    }
                }
            ?>
            </select>
            </div>
            <?php
            } elseif($type == 'section') {
            ?>
            <div>
        		<section>
            	<h3 id="title<?php echo $fieldId; ?>">
            	<?php echo $fieldTitle; ?>
            	</h3>
            	<div id="instruct<?php echo $fieldId; ?>">
            	<?php echo $fieldInstruction; ?>
            	</div>
                </section>
        	</div>
            <?php
            } elseif($type == 'likert') {
                if(!empty($subFields) && !empty($choices)) {
                    $validation = $field['Validation'];
            ?>
            <table cellspacing="0">
        	<caption id="title<?php echo $fieldId; ?>">
        		<?php echo $fieldTitle; ?>
        		<span id="req_<?php echo $fieldId; ?>" class="req"><?php echo ($field['IsRequired'])?'*':''; ?>
        		</span>
        	</caption>
            <thead>
        		<tr>
                    <th>
        				&nbsp;
        			</th>
            <?php
                    foreach($choices as $choice) {
            ?>
        			<td>
        				<?php echo $choice['Choice']; ?>
        			</td>
        		
            <?php
                    }
            ?>
                </tr>
        	</thead>
            <tbody>
            <?php
                $i = 0;
                foreach($subFields as $subField) {
                    $subFieldid = $subField['ColumnId'];
            ?>
            <tr class="statement">
            <?php
                    $j=0;
                    foreach($choices as $choice) {
                    if($j==0) {
            ?>
                    <th>
                        <?php if($validation == 'na' || empty($validation)) : ?>
        				<label for="Field<?php echo $i+1; ?>">
        					<?php echo $subField['ChoicesText']; ?>
        				</label>
                        <?php elseif($validation == 'dc'): ?>
                        <label>
        					<?php echo $subField['ChoicesText']; ?>
        				</label>
                        <?php endif; ?>
        			</th>
            <?php
                    }
            ?>
                    <td title="<?php echo $choice['Choice']; ?>">
                        <?php if($validation == 'na' || empty($validation)) : ?>
        				<input <?php echo ($choice['IsDefault'])?'checked="checked"':''; ?> id="Field<?php echo $subFieldid; ?>_<?php echo $j + 1; ?>" name="Field<?php echo $subFieldid; ?>" type="radio" tabindex="<?php echo $j + 1; ?>" value="Strongly Disagree" onchange="handleInput(this);" disabled="disabled" readonly="readonly"/>
        				<label for="Field<?php echo $subFieldid; ?>_<?php echo $j + 1; ?>">
        					<?php echo isset($choice['Score'])?$choice['Score']:''; ?>
        				</label>
                        <?php elseif($validation == 'dc'): ?>
                        <input <?php echo ($choice['IsDefault'])?'checked="checked"':''; ?> id="Field<?php echo $subFieldid; ?>_<?php echo $j + 1; ?>" name="Field<?php echo $subFieldid; ?>_<?php echo $j + 1; ?>" type="checkbox" tabindex="<?php echo $j + 1; ?>" value="Strongly Disagree" onchange="handleInput(this);" disabled="disabled" readonly="readonly"/>
                        <?php endif; ?>
        			</td>
            <?php
                    
                    $j++;
                    }
            ?>
            </tr>
            <?php
                $i++;
                }
            ?>
            </tbody>
            </table>
            <?php
                }
            }
            ?>
        	<p class="instruct hide" id="instruct<?php echo $fieldId; ?>">
        		<small>
        		</small>
        	</p>
        	<div id="fa<?php echo $fieldId; ?>" class="fieldActions">
        		<img class="faDup" src="template/images/icons/add.png" alt="Duplicate." title="Duplicate." onclick="duplicateField(<?php echo $fieldId; ?>, event); return false;" />
        		<img class="faDel" src="template/images/icons/delete.png" alt="Delete." title="Delete." onclick="removeField(<?php echo $fieldId; ?>, event); return false;" />
        	</div>
            </fieldset>
            <?php
            if(!empty($extra_fields)) {  
            ?>
            <div class="extra_fields">
            <ul>
            <?php
                $extra_hasFieldClass = 'hasFields';
                $extra_buttonClass = '';
                //$this->helpers->aasort($extra_fields,"Pos");
                foreach($extra_fields as $extra_field) {
                    $extra_classes = 'notranslate dragable';
                    $extra_type = $extra_field['Typeof'];
                    $extra_fieldId = $extra_field['ColumnId'];
                    $extra_fieldTitle = $extra_field['Title'];
                    $extra_settings = $extra_field['Settings'];
                    $extra_fieldInstruction = $extra_field['Instructions'];
                    $extra_subFields = isset($extra_field['SubFields'])?$extra_field['SubFields']:array();
                    $extra_choices = isset($extra_field['Choices'])?$extra_field['Choices']:array();
                    if($extra_type == 'section') {
                        $extra_classes .= ' section';
                    }
                    if($extra_type == 'likert') {
                        $extra_classes .= ' col4 likert';
                    }
                    if(isset($extra_settings['hideNumbers']) || in_array('hideNumbers',$extra_settings)) {
                        $extra_classes .= ' hideNumbers';
                    }
                    if(isset($extra_field['ClassNames'])) {
                        $extra_classes .= ' ' . $extra_field['ClassNames'];
                    }
                    if($extra_type == 'date' || $extra_type == 'eurodate') {
                        $extra_classes .= ' '.$extra_type;
                    } elseif($extra_type == 'time') {
                        $extra_classes .= ' time';
                    } elseif($extra_type == 'phone') {
                        $extra_classes .= ' phone';
                    } elseif($extra_type == 'address') {
                        $extra_classes .= ' complex';
                    }
            ?>
                <li id="extra_foli<?php echo $extra_fieldId; ?>" title="Click to edit. Drag to reorder." class="<?php echo $extra_classes; ?>">
                    <?php
                    if($extra_type == 'checkbox' || $extra_type == 'radio') {
                    ?>
                        <fieldset>
                    	<![if !IE | (gte IE 8)]>
                    	<legend id="extra_title<?php echo $extra_fieldId; ?>" class="desc">
                    	<?php echo $extra_fieldTitle; ?>
                    	<span id="extra_req_<?php echo $extra_fieldId; ?>" class="req"><?php echo ($extra_field['IsRequired'])?'*':''; ?></span>
                    	</legend>
                    	<![endif]>
                    	<!--[if lt IE 8]>
                    	<label id="extra_title<?php echo $extra_fieldId; ?>" class="desc">
                    	<?php echo $extra_fieldTitle; ?>
                    	<span id="extra_req_<?php echo $extra_fieldId; ?>" class="req"><?php echo ($extra_field['IsRequired'])?'*':''; ?></span>
                    	</label>
                    	<![endif]-->
                    <?php
                    } elseif($extra_type == 'section' || $extra_type == 'likert') {
                    } else {
                    ?>
                        <label class="desc" id="extra_title<?php echo $extra_fieldId; ?>" for="extra_Field<?php echo $extra_fieldId; ?>">
                        <?php echo $extra_fieldTitle; ?>
                        <span id="extra_req_<?php echo $extra_fieldId; ?>" class="req"><?php echo ($extra_field['IsRequired'])?'*':''; ?>
                        </span>
                        </label>
                    <?php
                    }
                    if($extra_type == 'text' || $extra_type == 'email' || $extra_type == 'url' || $extra_type == 'number') {
                    ?>
                	<div>
                		<input id="extra_Field<?php echo $extra_fieldId; ?>" name="extra_Field<?php echo $extra_fieldId; ?>" type="text" class="field text nospin medium" value="<?php echo $extra_field['DefaultVal']; ?>" tabindex="<?php echo $extra_fieldId; ?>" onkeyup="handleInput(this); " onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                	</div>
                    <?php
                    } elseif($extra_type == 'shortname') {
                    ?>
                	<span>
                		<input id="extra_Field<?php echo $extra_fieldId; ?>_0" name="extra_Field<?php echo $extra_fieldId; ?>" type="text" class="field text fn" value="" size="8" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                		<label for="extra_Field<?php echo $extra_fieldId; ?>_1">
                			First
                		</label>
                	</span>
                	<span>
                		<input id="extra_Field<?php echo $extra_fieldId; ?>_1" name="extra_Field<?php echo $extra_fieldId; ?>" type="text" class="field text ln" value="" size="14" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                		<label for="extra_Field<?php echo $extra_fieldId; ?>_1">
                			Last
                		</label>
                	</span>
                    <?php
                    } elseif($extra_type == 'address') {
                    ?>
                    <div>
                        <span class="full addr1"> <input id="extra_Field" name="extra_Field" type="text" class="field text addr" value="" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="extra_Field"> Street Address </label> </span> <span class="full addr2"> <input id="extra_Field" name="extra_Field" type="text" class="field text addr" value="" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="extra_Field"> Address Line 2 </label> </span> <span class="left city"> <input id="extra_Field" name="extra_Field" type="text" class="field text addr" value="" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="extra_Field"> City </label> </span> <span class="right state"> <input id="extra_Field" name="extra_Field" type="text" class="field text addr" value="" tabindex="4" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="extra_Field"> State / Province / Region </label> </span> <span class="left zip"> <input id="extra_Field" name="extra_Field" type="text" class="field text addr" value="" maxlength="15" tabindex="5" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/> <label for="extra_Field"> Postal / Zip Code </label> </span> <span class="right country"> <select id="extra_Field" name="extra_Field" class="field select addr" tabindex="6" onclick="handleInput(this);" onkeyup="handleInput(this);" readonly="readonly" disabled="disabled"> <option value="" selected="selected"> </option> <option value="United States"> United States </option> <option value="United Kingdom"> United Kingdom </option> <option value="Australia"> Australia </option> <option value="Canada"> Canada </option> <option value="France"> France </option> <option value="New Zealand"> New Zealand </option> <option value="India"> India </option> <option value="Brazil"> Brazil </option> <option value="----"> ---- </option> <option value="Afghanistan"> Afghanistan </option> <option value="Åland Islands"> Åland Islands </option> <option value="Albania"> Albania </option> <option value="Algeria"> Algeria </option> <option value="American Samoa"> American Samoa </option> <option value="Andorra"> Andorra </option> <option value="Angola"> Angola </option> <option value="Anguilla"> Anguilla </option> <option value="Antarctica"> Antarctica </option> <option value="Antigua and Barbuda"> Antigua and Barbuda </option> <option value="Argentina"> Argentina </option> <option value="Armenia"> Armenia </option> <option value="Aruba"> Aruba </option> <option value="Austria"> Austria </option> <option value="Azerbaijan"> Azerbaijan </option> <option value="Bahamas"> Bahamas </option> <option value="Bahrain"> Bahrain </option> <option value="Bangladesh"> Bangladesh </option> <option value="Barbados"> Barbados </option> <option value="Belarus"> Belarus </option> <option value="Belgium"> Belgium </option> <option value="Belize"> Belize </option> <option value="Benin"> Benin </option> <option value="Bermuda"> Bermuda </option> <option value="Bhutan"> Bhutan </option> <option value="Bolivia"> Bolivia </option> <option value="Bosnia and Herzegovina"> Bosnia and Herzegovina </option> <option value="Botswana"> Botswana </option> <option value="British Indian Ocean Territory"> British Indian Ocean Territory </option> <option value="Brunei Darussalam"> Brunei Darussalam </option> <option value="Bulgaria"> Bulgaria </option> <option value="Burkina Faso"> Burkina Faso </option> <option value="Burundi"> Burundi </option> <option value="Cambodia"> Cambodia </option> <option value="Cameroon"> Cameroon </option> <option value="Cape Verde"> Cape Verde </option> <option value="Cayman Islands"> Cayman Islands </option> <option value="Central African Republic"> Central African Republic </option> <option value="Chad"> Chad </option> <option value="Chile"> Chile </option> <option value="China"> China </option> <option value="Colombia"> Colombia </option> <option value="Comoros"> Comoros </option> <option value="Democratic Republic of the Congo"> Democratic Republic of the Congo </option> <option value="Republic of the Congo"> Republic of the Congo </option> <option value="Cook Islands"> Cook Islands </option> <option value="Costa Rica"> Costa Rica </option> <option value="C&ocirc;te dIvoire"> C&ocirc;te dIvoire </option> <option value="Croatia"> Croatia </option> <option value="Cuba"> Cuba </option> <option value="Cyprus"> Cyprus </option> <option value="Czech Republic"> Czech Republic </option> <option value="Denmark"> Denmark </option> <option value="Djibouti"> Djibouti </option> <option value="Dominica"> Dominica </option> <option value="Dominican Republic"> Dominican Republic </option> <option value="East Timor"> East Timor </option> <option value="Ecuador"> Ecuador </option> <option value="Egypt"> Egypt </option> <option value="El Salvador"> El Salvador </option> <option value="Equatorial Guinea"> Equatorial Guinea </option> <option value="Eritrea"> Eritrea </option> <option value="Estonia"> Estonia </option> <option value="Ethiopia"> Ethiopia </option> <option value="Faroe Islands"> Faroe Islands </option> <option value="Fiji"> Fiji </option> <option value="Finland"> Finland </option> <option value="Gabon"> Gabon </option> <option value="Gambia"> Gambia </option> <option value="Georgia"> Georgia </option> <option value="Germany"> Germany </option> <option value="Ghana"> Ghana </option> <option value="Gibraltar"> Gibraltar </option> <option value="Greece"> Greece </option> <option value="Grenada"> Grenada </option> <option value="Guatemala"> Guatemala </option> <option value="Guinea"> Guinea </option> <option value="Guinea-Bissau"> Guinea-Bissau </option> <option value="Guyana"> Guyana </option> <option value="Haiti"> Haiti </option> <option value="Honduras"> Honduras </option> <option value="Hong Kong"> Hong Kong </option> <option value="Hungary"> Hungary </option> <option value="Iceland"> Iceland </option> <option value="Indonesia"> Indonesia </option> <option value="Iran"> Iran </option> <option value="Iraq"> Iraq </option> <option value="Ireland"> Ireland </option> <option value="Israel"> Israel </option> <option value="Italy"> Italy </option> <option value="Jamaica"> Jamaica </option> <option value="Japan"> Japan </option> <option value="Jordan"> Jordan </option> <option value="Kazakhstan"> Kazakhstan </option> <option value="Kenya"> Kenya </option> <option value="Kiribati"> Kiribati </option> <option value="North Korea"> North Korea </option> <option value="South Korea"> South Korea </option> <option value="Kuwait"> Kuwait </option> <option value="Kyrgyzstan"> Kyrgyzstan </option> <option value="Laos"> Laos </option> <option value="Latvia"> Latvia </option> <option value="Lebanon"> Lebanon </option> <option value="Lesotho"> Lesotho </option> <option value="Liberia"> Liberia </option> <option value="Libya"> Libya </option> <option value="Liechtenstein"> Liechtenstein </option> <option value="Lithuania"> Lithuania </option> <option value="Luxembourg"> Luxembourg </option> <option value="Macedonia"> Macedonia </option> <option value="Madagascar"> Madagascar </option> <option value="Malawi"> Malawi </option> <option value="Malaysia"> Malaysia </option> <option value="Maldives"> Maldives </option> <option value="Mali"> Mali </option> <option value="Malta"> Malta </option> <option value="Marshall Islands"> Marshall Islands </option> <option value="Mauritania"> Mauritania </option> <option value="Mauritius"> Mauritius </option> <option value="Mexico"> Mexico </option> <option value="Micronesia"> Micronesia </option> <option value="Moldova"> Moldova </option> <option value="Monaco"> Monaco </option> <option value="Mongolia"> Mongolia </option> <option value="Montenegro"> Montenegro </option> <option value="Morocco"> Morocco </option> <option value="Mozambique"> Mozambique </option> <option value="Myanmar"> Myanmar </option> <option value="Namibia"> Namibia </option> <option value="Nauru"> Nauru </option> <option value="Nepal"> Nepal </option> <option value="Netherlands"> Netherlands </option> <option value="Netherlands Antilles"> Netherlands Antilles </option> <option value="Nicaragua"> Nicaragua </option> <option value="Niger"> Niger </option> <option value="Nigeria"> Nigeria </option> <option value="Norway"> Norway </option> <option value="Oman"> Oman </option> <option value="Pakistan"> Pakistan </option> <option value="Palau"> Palau </option> <option value="Palestine"> Palestine </option> <option value="Panama"> Panama </option> <option value="Papua New Guinea"> Papua New Guinea </option> <option value="Paraguay"> Paraguay </option> <option value="Peru"> Peru </option> <option value="Philippines"> Philippines </option> <option value="Poland"> Poland </option> <option value="Portugal"> Portugal </option> <option value="Puerto Rico"> Puerto Rico </option> <option value="Qatar"> Qatar </option> <option value="Romania"> Romania </option> <option value="Russia"> Russia </option> <option value="Rwanda"> Rwanda </option> <option value="Saint Kitts and Nevis"> Saint Kitts and Nevis </option> <option value="Saint Lucia"> Saint Lucia </option> <option value="Saint Vincent and the Grenadines"> Saint Vincent and the Grenadines </option> <option value="Samoa"> Samoa </option> <option value="San Marino"> San Marino </option> <option value="Sao Tome and Principe"> Sao Tome and Principe </option> <option value="Saudi Arabia"> Saudi Arabia </option> <option value="Senegal"> Senegal </option> <option value="Serbia and Montenegro"> Serbia and Montenegro </option> <option value="Seychelles"> Seychelles </option> <option value="Sierra Leone"> Sierra Leone </option> <option value="Singapore"> Singapore </option> <option value="Slovakia"> Slovakia </option> <option value="Slovenia"> Slovenia </option> <option value="Solomon Islands"> Solomon Islands </option> <option value="Somalia"> Somalia </option> <option value="South Africa"> South Africa </option> <option value="Spain"> Spain </option> <option value="Sri Lanka"> Sri Lanka </option> <option value="Sudan"> Sudan </option> <option value="Suriname"> Suriname </option> <option value="Swaziland"> Swaziland </option> <option value="Sweden"> Sweden </option> <option value="Switzerland"> Switzerland </option> <option value="Syria"> Syria </option> <option value="Taiwan"> Taiwan </option> <option value="Tajikistan"> Tajikistan </option> <option value="Tanzania"> Tanzania </option> <option value="Thailand"> Thailand </option> <option value="Togo"> Togo </option> <option value="Tonga"> Tonga </option> <option value="Trinidad and Tobago"> Trinidad and Tobago </option> <option value="Tunisia"> Tunisia </option> <option value="Turkey"> Turkey </option> <option value="Turkmenistan"> Turkmenistan </option> <option value="Tuvalu"> Tuvalu </option> <option value="Uganda"> Uganda </option> <option value="Ukraine"> Ukraine </option> <option value="United Arab Emirates"> United Arab Emirates </option> <option value="United States Minor Outlying Islands"> United States Minor Outlying Islands </option> <option value="Uruguay"> Uruguay </option> <option value="Uzbekistan"> Uzbekistan </option> <option value="Vanuatu"> Vanuatu </option> <option value="Vatican City"> Vatican City </option> <option value="Venezuela"> Venezuela </option> <option value="Vietnam"> Vietnam </option> <option value="Virgin Islands, British"> Virgin Islands, British </option> <option value="Virgin Islands, U.S."> Virgin Islands, U.S. </option> <option value="Yemen"> Yemen </option> <option value="Zambia"> Zambia </option> <option value="Zimbabwe"> Zimbabwe </option> </select> <label for="extra_Field"> Country </label> </span>
                    </div>
                    <?php
                    } elseif($extra_type == 'eurodate') {
                    ?>
                    <span>
                		<input id="extra_Field<?php echo $extra_fieldId; ?>-2" name="extra_Field<?php echo $extra_fieldId; ?>-2" type="text" class="field text" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                		<label for="extra_Field<?php echo $extra_fieldId; ?>-2">
                			DD
                		</label>
                	</span>
                	<span class="symbol">
                		/
                	</span>
                    <span>
                		<input id="extra_Field<?php echo $extra_fieldId; ?>-1" name="extra_Field<?php echo $extra_fieldId; ?>-1" type="text" class="field text" value="" size="2" maxlength="2" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                		<label for="extra_Field<?php echo $extra_fieldId; ?>-1">
                			MM
                		</label>
                	</span>
                	<span class="symbol">
                		/
                	</span>
                	<span>
                		<input id="extra_Field<?php echo $extra_fieldId; ?>" name="extra_Field<?php echo $extra_fieldId; ?>" type="text" class="field text" value="" size="4" maxlength="4" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                		<label for="extra_Field<?php echo $extra_fieldId; ?>">
                			YYYY
                		</label>
                	</span>
                	<span id="extra_cal<?php echo $extra_fieldId; ?>">
                		<img id="extra_pick<?php echo $extra_fieldId; ?>" class="datepicker" src="template/images/icons/calendar.png" alt="Pick a date." />
                	</span>
                    <?php 
                    } elseif($extra_type == 'date') {
                    ?>
                    <span>
                    	<input id="extra_Field<?php echo $extra_fieldId; ?>-1" name="extra_Field<?php echo $extra_fieldId; ?>-1" type="text" class="field text" value="" size="2" maxlength="2" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                    	<label for="extra_Field<?php echo $extra_fieldId; ?>-1">
                    		MM
                    	</label>
                    </span>
                    <span class="symbol">
                    	/
                    </span>
                    <span>
                    	<input id="extra_Field<?php echo $extra_fieldId; ?>-2" name="extra_Field<?php echo $extra_fieldId; ?>-2" type="text" class="field text" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                    	<label for="extra_Field<?php echo $extra_fieldId; ?>-2">
                    		DD
                    	</label>
                    </span>
                    <span class="symbol">
                    	/
                    </span>
                    <span>
                    	<input id="extra_Field<?php echo $extra_fieldId; ?>" name="extra_Field<?php echo $extra_fieldId; ?>" type="text" class="field text" value="" size="4" maxlength="4" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                    	<label for="extra_Field<?php echo $extra_fieldId; ?>">
                    		YYYY
                    	</label>
                    </span>
                    <span id="extra_cal<?php echo $extra_fieldId; ?>">
                    	<img id="extra_pick<?php echo $extra_fieldId; ?>" class="datepicker" src="template/images/icons/calendar.png" alt="Pick a date." />
                    </span>
                    <?php 
                    } elseif($extra_type == 'time') {
                    ?>
                    <span class="hours">
                                	<input id="extra_Field<?php echo $extra_fieldId; ?>" name="extra_Field<?php echo $extra_fieldId; ?>" type="text" class="field text" value="" size="2" maxlength="2" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                                	<label for="extra_Field<?php echo $extra_fieldId; ?>">
                                		HH
                                	</label>
                                </span>
                                <span class="symbol minutes">
                                	:
                                </span>
                                <span class="minutes">
                                	<input id="extra_Field<?php echo $extra_fieldId; ?>-1" name="extra_Field<?php echo $extra_fieldId; ?>-1" type="text" class="field text" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                                	<label for="extra_Field<?php echo $extra_fieldId; ?>-1">
                                		MM
                                	</label>
                                </span>
                                <span class="symbol seconds">
                                	:
                                </span>
                                <span class="seconds">
                                	<input id="extra_Field<?php echo $extra_fieldId; ?>-2" name="extra_Field<?php echo $extra_fieldId; ?>-2" type="text" class="field text" value="" size="2" maxlength="2" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                                	<label for="extra_Field<?php echo $extra_fieldId; ?>-2">
                                		SS
                                	</label>
                                </span>
                                <span class="ampm">
                                	<select id="extra_Field<?php echo $extra_fieldId; ?>-3" name="extra_Field<?php echo $extra_fieldId; ?>-3" class="field select" style="width:4em" tabindex="4" onclick="handleInput(this);" onkeyup="handleInput(this);" readonly="readonly" disabled="disabled">
                                		<option value="AM" selected="selected">
                                			AM
                                		</option>
                                		<option value="PM">
                                			PM
                                		</option>
                                	</select>
                                	<label for="extra_Field<?php echo $extra_fieldId; ?>-3">
                                		AM/PM
                                	</label>
                                </span>
                    <?php
                    } elseif($extra_type == 'phone') {
                    ?>
                    <span>
                            	<input id="extra_Field<?php echo $extra_fieldId; ?>" name="extra_Field<?php echo $extra_fieldId; ?>" type="tel" class="field text" value="" size="3" maxlength="3" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                            	<label for="extra_Field<?php echo $extra_fieldId; ?>">
                            		###
                            	</label>
                            </span>
                            <span class="symbol">
                            	-
                            </span>
                            <span>
                            	<input id="extra_Field<?php echo $extra_fieldId; ?>-1" name="extra_Field<?php echo $extra_fieldId; ?>-1" type="tel" class="field text" value="" size="3" maxlength="3" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                            	<label for="extra_Field<?php echo $extra_fieldId; ?>-1">
                            		###
                            	</label>
                            </span>
                            <span class="symbol">
                            	-
                            </span>
                            <span>
                            	<input id="extra_Field<?php echo $extra_fieldId; ?>-2" name="extra_Field<?php echo $extra_fieldId; ?>-2" type="tel" class="field text" value="" size="4" maxlength="4" tabindex="3" onkeyup="handleInput(this);" onchange="handleInput(this);" readonly="readonly" disabled="disabled"/>
                            	<label for="extra_Field<?php echo $extra_fieldId; ?>-2">
                            		####
                            	</label>
                            </span>
                    <?php
                    } elseif($extra_type == 'money') {
                    ?>
                    <span class="symbol">
                    	$extra_
                    </span>
                    <span>
                    	<input id="extra_Field<?php echo $extra_fieldId; ?>" name="extra_Field<?php echo $extra_fieldId; ?>" type="text" class="field text currency nospin" value="" size="10" tabindex="1" onkeyup="handleInput(this);" onchange="handleInput(this);" />
                    	<label for="extra_Field<?php echo $extra_fieldId; ?>">
                    		Dollars
                    	</label>
                    </span>
                    <span class="symbol radix">
                    	.
                    </span>
                    <span class="cents">
                    	<input id="extra_Field<?php echo $extra_fieldId; ?>-1" name="extra_Field<?php echo $extra_fieldId; ?>-1" type="text" class="field text nospin" value="" size="2" maxlength="2" tabindex="2" onkeyup="handleInput(this);" onchange="handleInput(this);" />
                    	<label for="extra_Field<?php echo $extra_fieldId; ?>-1">
                    		Cents
                    	</label>
                    </span>
                    <?php
                    } elseif($extra_type == 'textarea') {
                    ?>
                    <div>
                		<div class="handle"></div><textarea id="extra_Field<?php echo $extra_fieldId; ?>" name="extra_Field<?php echo $extra_fieldId; ?>" class="field textarea medium" spellcheck="true" rows="10" cols="50" tabindex="<?php echo $extra_fieldId; ?>" onkeyup="handleInput(this); " onchange="handleInput(this);" readonly="readonly" disabled="disabled"><?php echo $extra_field['DefaultVal']; ?></textarea>
                	</div>
                    <?php
                    } elseif($extra_type == 'checkbox') {
                    ?>
                    <div>
                    <?php
                        
                        if(!empty($extra_subFields)) {
                            foreach($extra_subFields as $extra_v) {
                    ?>
                		<span>
                			<input id="extra_Field" name="extra_Field" type="checkbox" class="field checkbox" value="<?php echo $extra_v['ChoicesText']; ?>" tabindex="1" onchange="handleInput(this);" readonly="readonly" disabled="disabled" <?php echo $extra_v['DefaultVal']?'checked="checked"':''; ?> />
                			<label class="choice" for="extra_Field">
                				<?php echo $extra_v['ChoicesText']; ?>
                			</label>
                		</span>
                    <?php
                            }
                        }
                    ?>
                    </div>
                    <?php
                    } elseif($extra_type == 'radio') {
                    ?>
                    <div>
                    <input id="extra_radioDefault_<?php echo $extra_fieldId; ?>" name="extra_Field<?php echo $extra_fieldId; ?>" type="hidden" value="" />
                    <?php
                        
                        if(!empty($extra_choices)) {
                            $extra_i = 0;
                            foreach($extra_choices as $extra_v) {
                    ?>
                        
                		<span>
                			<input id="extra_Field<?php echo $extra_fieldId; ?>_<?php echo $extra_i; ?>" name="extra_Field<?php echo $extra_fieldId; ?>" type="radio" class="field radio" value="<?php echo $extra_v['Choice']; ?>" tabindex="<?php echo $extra_i; ?>" onchange="handleInput(this);" onmouseup="handleInput(this);" <?php echo $extra_v['IsDefault']?'checked="checked"':''; ?> readonly="readonly" disabled="disabled"/>
                            <label class="choice" for="extra_Field<?php echo $extra_fieldId; ?>_<?php echo $extra_i; ?>">
                            <?php echo $extra_v['Choice']; ?>
                            </label>
                		</span>
                        <?php if($extra_v['Choice'] == 'Other') { ?>
                            <input id="extra_Field<?php echo $extra_fieldId; ?>_other" name="extra_Field<?php echo $extra_fieldId; ?>_other_Other" type="text" class="field text other" value="" onclick="document.getElementById(\'Field<?php echo $extra_fieldId; ?>_<?php echo $extra_i; ?>\').checked = \'checked\';" onkeyup="handleInput(this);" onchange="handleInput(this);"
                			tabindex="<?php echo $extra_i; ?>" />
                        <?php }
                            }
                        }
                    ?>
                    </div>
                    <?php
                    } elseif($extra_type == 'select') {
                    ?>
                    <div>
                    <select id="extra_Field<?php echo $extra_fieldId; ?>" name="extra_Field<?php echo $extra_fieldId; ?>" class="field select medium" onclick="handleInput(this);" onkeyup="handleInput(this);" tabindex="<?php echo $extra_fieldId; ?>" readonly="readonly" disabled="disabled">
                    <?php
                        if(!empty($extra_choices)) {
                            foreach($extra_choices as $extra_v) {
                    ?>
                			<option <?php echo ($extra_v['IsDefault'])?'selected="selected"':''; ?> value="<?php echo $extra_v['Choice']; ?>">
                				<?php echo $extra_v['Choice']; ?>
                			</option>
                    <?php
                            }
                        }
                    ?>
                    </select>
                    </div>
                    <?php
                    } elseif($extra_type == 'section') {
                    ?>
                    <div>
                		<section>
                    	<h3 id="extra_title<?php echo $extra_fieldId; ?>">
                    	<?php echo $extra_fieldTitle; ?>
                    	</h3>
                    	<div id="extra_instruct<?php echo $extra_fieldId; ?>">
                    	<?php echo $extra_fieldInstruction; ?>
                    	</div>
                        </section>
                	</div>
                    <?php
                    } elseif($extra_type == 'likert') {
                        if(!empty($extra_subFields) && !empty($extra_choices)) {
                            $extra_validation = $extra_field['Validation'];
                    ?>
                    <table cellspacing="0">
                	<caption id="extra_title<?php echo $extra_fieldId; ?>">
                		<?php echo $extra_fieldTitle; ?>
                		<span id="extra_req_<?php echo $extra_fieldId; ?>" class="req"><?php echo ($extra_field['IsRequired'])?'*':''; ?>
                		</span>
                	</caption>
                    <thead>
                		<tr>
                            <th>
                				&nbsp;
                			</th>
                    <?php
                            foreach($extra_choices as $extra_choice) {
                    ?>
                			<td>
                				<?php echo $extra_choice['Choice']; ?>
                			</td>
                		
                    <?php
                            }
                    ?>
                        </tr>
                	</thead>
                    <tbody>
                    <?php
                        $extra_i = 0;
                        foreach($extra_subFields as $extra_subField) {
                            $extra_subFieldid = $extra_subField['ColumnId'];
                    ?>
                    <tr class="statement">
                    <?php
                            $extra_j=0;
                            foreach($extra_choices as $extra_choice) {
                            if($extra_j==0) {
                    ?>
                            <th>
                                <?php if($extra_validation == 'na' || empty($extra_validation)) : ?>
                				<label for="extra_Field<?php echo $extra_i+1; ?>">
                					<?php echo $extra_subField['ChoicesText']; ?>
                				</label>
                                <?php elseif($extra_validation == 'dc'): ?>
                                <label>
                					<?php echo $extra_subField['ChoicesText']; ?>
                				</label>
                                <?php endif; ?>
                			</th>
                    <?php
                            }
                    ?>
                            <td title="<?php echo $extra_choice['Choice']; ?>">
                                <?php if($extra_validation == 'na' || empty($extra_validation)) : ?>
                				<input <?php echo ($extra_choice['IsDefault'])?'checked="checked"':''; ?> id="extra_Field<?php echo $extra_subFieldid; ?>_<?php echo $extra_j + 1; ?>" name="extra_Field<?php echo $extra_subFieldid; ?>" type="radio" tabindex="<?php echo $extra_j + 1; ?>" value="Strongly Disagree" onchange="handleInput(this);" disabled="disabled" readonly="readonly"/>
                				<label for="extra_Field<?php echo $extra_subFieldid; ?>_<?php echo $extra_j + 1; ?>">
                					<?php echo isset($extra_choice['Score'])?$extra_choice['Score']:''; ?>
                				</label>
                                <?php elseif($extra_validation == 'dc'): ?>
                                <input <?php echo ($extra_choice['IsDefault'])?'checked="checked"':''; ?> id="extra_Field<?php echo $extra_subFieldid; ?>_<?php echo $extra_j + 1; ?>" name="extra_Field<?php echo $extra_subFieldid; ?>_<?php echo $extra_j + 1; ?>" type="checkbox" tabindex="<?php echo $extra_j + 1; ?>" value="Strongly Disagree" onchange="handleInput(this);" disabled="disabled" readonly="readonly"/>
                                <?php endif; ?>
                			</td>
                    <?php
                            
                            $extra_j++;
                            }
                    ?>
                    </tr>
                    <?php
                        $extra_i++;
                        }
                    ?>
                    </tbody>
                    </table>
                    <?php
                        }
                    }
                    ?>
                	<p class="instruct hide" id="extra_instruct<?php echo $extra_fieldId; ?>">
                		<small>
                		</small>
                	</p>
                	<div id="extra_fa<?php echo $extra_fieldId; ?>" class="extra_fieldActions">
                		<!--<img class="faDup" src="template/images/icons/add.png" alt="Duplicate." title="Duplicate." onclick="duplicateField(<?php echo $extra_fieldId; ?>, event); return false;" />-->
                		<img class="faDel" src="template/images/icons/delete.png" alt="Delete." title="Delete." onclick="removeField(<?php echo $extra_fieldId; ?>, event); return false;" />
                	</div>
                    </fieldset>
                </li>
            <?php
                }
            ?>
            </ul>
            </div>
            <?php
            }
            ?>
        </li>
        <?php
        
        $html .= ob_get_contents();
        ob_clean();
        /*
        * END FORM BUILDER
        */
                }
            }
            }
        }
        /*
        *
        */
        $this->view->fields = $fields_obj->getFields('*','','','','30');
        $forms_result = $forms_obj->getForms('*',"`form_type` = 'SC'");
        $this->view->formType = $formType;
        $this->view->buttonClass = $buttonClass;
        $this->view->forms_result = $forms_result;
        $this->view->formScoreUrl = $formScoreUrl;
        $this->view->hasFieldClass = $hasFieldClass;
        $this->view->html = $html;
        if(isset($_form))$this->view->_form = $_form;
        $this->view->title = 'Form Builder';
		$this->view->render('form_builder');
        
    }
    function postReq() {
        $req = $this->registry->req;
        $ajax = new Controllers_AjaxController($this->registry);
        $ajax->index($req);
        echo $ajax->getJson();
    }
}