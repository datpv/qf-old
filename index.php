<?php
    $x = microtime(true);
    //error_reporting(0);
    include ('config.php');
    include ('includes/init.php');
    // disallow cache
    session_start();
    header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
    header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
    // load controller, action, .. and run
    $registry->query_count = 0;
    $registry->smtp = $smtp;
    $registry->category = $catetory;
    $registry->timeload = $x;
    $registry->router->setPath(__SITE_PATH . '/controllers');
    // run
    $registry->router->loader();
    
//echo microtime(true) - $x;