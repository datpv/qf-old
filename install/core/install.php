<?php
function fn_put_contents($location, $content, $base_dir = '', $file_perm = DEFAULT_FILE_PERMISSIONS)
{
	$result = '';
	$path = $base_dir . $location;

	if (!empty($base_dir) && !fn_check_path($path)) {
		return false;
	}

	// Location is regular file
	$result = @file_put_contents($path, $content);
	if ($result !== false) {
		@chmod($path, $file_perm);
	}
	
	return $result; 
}
function fn_check_path($path)
{
	$real_path = realpath($path);
    
	return str_replace('\\', '/', $real_path) == $path ? true : false;
}

$mode = isset($_POST['mode'])?$_POST['mode']:'prepare';
session_start();
if ($mode == 'prepare') {
    $next_mode = 'database';
    
}
if ($mode == 'database') {
    
    $db_host = isset($_POST['db_host'])?$_POST['db_host']:'localhost';
    $db_user = isset($_POST['db_user'])?$_POST['db_user']:'root';
    $db_pass = isset($_POST['db_pass'])?$_POST['db_pass']:'';
    $db_name = isset($_POST['db_name'])?$_POST['db_name']:'';
    $db_prefix = isset($_POST['db_prefix'])?$_POST['db_prefix']:'';
    $old_db_prefix = isset($_POST['old_db_prefix'])?$_POST['old_db_prefix']:'';
    
    $db_admin_user = isset($_POST['db_admin_user'])?$_POST['db_admin_user']:'';
    $db_admin_pass = isset($_POST['db_admin_pass'])?$_POST['db_admin_pass']:'';
    $db_admin_number = isset($_POST['db_admin_number'])?$_POST['db_admin_number']:'';
    $db_admin_email = isset($_POST['db_admin_email'])?$_POST['db_admin_email']:'';
    
    $option_show_nickh2d = isset($_POST['option_show_nickh2d'])?$_POST['option_show_nickh2d']:'off';
    $option_show_nickskype = isset($_POST['option_show_nickskype'])?$_POST['option_show_nickskype']:'off';
    if($option_show_nickh2d == 'on') {
        $option_show_nickh2d = 'true';
    } else {
        $option_show_nickh2d = 'false';
    }
    if($option_show_nickskype == 'on') {
        $option_show_nickskype = 'true';
    } else {
        $option_show_nickskype = 'false';
    }
    $smtp_host = isset($_POST['smtp_host'])?$_POST['smtp_host']:'';
    $smtp_port = isset($_POST['smtp_port'])?$_POST['smtp_port']:'';
    $smtp_user = isset($_POST['smtp_user'])?$_POST['smtp_user']:'';
    $smtp_pass = isset($_POST['smtp_pass'])?$_POST['smtp_pass']:'';
    
    $mysqli = new mysqli($db_host, $db_user, $db_pass);
    if ($mysqli->connect_errno) {
        echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
        exit();
    }
    if(!$mysqli->query("CREATE DATABASE IF NOT EXISTS $db_name")) {
        echo '<br>DATABASE: '.$mysqli->error.'<br>';
        exit();
    }
    $mysqli->select_db($db_name) or die('Count not select db');
    $mysqli->query("SET NAMES 'utf8'");
    $mysqli->query("SET CHARACTER SET utf8");
    /*
    * Install By File
    */
    if ($_FILES['db_file']['error'] == UPLOAD_ERR_OK               //checks for errors
      && is_uploaded_file($_FILES['db_file']['tmp_name'])) { //checks that file is uploaded
        
    } else {
        exit('file upload Error'); 
    }
    $file = $_FILES['db_file']['tmp_name'];
		
	if (!file_exists($file)) { 
		exit('Could not load sql file: ' . $file); 
	}
	
	$lines = file($file);
	
	if ($lines) {
		$sql = '';

		foreach($lines as $line) {
			if ($line && (substr($line, 0, 2) != '--') && (substr($line, 0, 1) != '#')) {
				$sql .= $line;

				if (preg_match('/;\s*$/', $line)) {
					$sql = str_replace("DROP TABLE IF EXISTS `" . $old_db_prefix, "DROP TABLE IF EXISTS `" . $db_prefix, $sql);
					$sql = str_replace("CREATE TABLE IF NOT EXISTS `" . $old_db_prefix, "CREATE TABLE `" . $db_prefix, $sql);
					$sql = str_replace("INSERT INTO `" . $old_db_prefix, "INSERT INTO `" . $db_prefix, $sql);
					$mysqli->query($sql);
                    if(!empty($mysqli->error)) {
                        echo $mysqli->error;
                        exit();
                    }
					$sql = '';
				}
			}
		}
	}
    $mysqli->query("INSERT INTO `{$db_prefix}themes` (`theme_id`, `theme_name`, `theme_content`) VALUES (1, 'Default Theme', '{\"styleId\":1,\"name\":\"Create New\",\"css\":\"Default, ..\\\\\\/images\\\\\\/htdlogo.png, inherit, normal, 160%, #000000, inherit, normal, 95%, #444444, inherit, normal, 110%, #000000, inherit, normal, 85%, #444444, inherit, bold, 95%, #444444, inherit, normal, 100%, #333333, inherit, normal, 80%, #444444, #EEEEEE, none, #DEDEDE, none, #FFFFFF, #FFFFFF url(..\\\\\\/images\\\\\\/fieldbg.gif) repeat-x top, #FFF7C0, #F5F5F5, 1px, solid, #CCCCCC, 1px, dotted, #CCCCCC, 1px, solid, #E6E6E6, visible, 40\",\"buttonType\":\"text\",\"buttonValue\":\"Submit\",\"customCss\":\"\",\"integration\":null,\"formStyle\":\"0\"}')");
    $mysqli->query("TRUNCATE TABLE `{$db_prefix}users`");
    
    $db_admin_pass = md5($db_admin_pass);
    
    $mysqli->query("INSERT INTO `{$db_prefix}users` (`user_id`, `username`, `password`, `user_email`, `user_content`, `user_type`, `user_status`) VALUES
        (1, '$db_admin_user', '$db_admin_pass', '$db_admin_email', '{\"Name\":\"Admin\",\"NickH2d\":\"\",\"NickSkype\":\"\"}', 'A', 'A')");
    /*
    *
    */
    
    $config_contents = $default = file_get_contents($basedir . '/install/config.install.php');
	if (!empty($config_contents)) {
		if (strstr($config_contents, '$config[\'db_host\'] =')) {
			$config_contents = preg_replace('/^\$config\[\'db_host\'\] =.*;/mi', "\$config['db_host'] = '" . addslashes($db_host) . "';", $config_contents);
		}
        if (strstr($config_contents, '$config[\'db_user\'] =')) {
			$config_contents = preg_replace('/^\$config\[\'db_user\'\] =.*;/mi', "\$config['db_user'] = '" . addslashes($db_user) . "';", $config_contents);
		}
        if (strstr($config_contents, '$config[\'db_pass\'] =')) {
			$config_contents = preg_replace('/^\$config\[\'db_pass\'\] =.*;/mi', "\$config['db_pass'] = '" . addslashes($db_pass) . "';", $config_contents);
		}
        if (strstr($config_contents, '$config[\'db_name\'] =')) {
			$config_contents = preg_replace('/^\$config\[\'db_name\'\] =.*;/mi', "\$config['db_name'] = '" . addslashes($db_name) . "';", $config_contents);
		}
        if (strstr($config_contents, '$config[\'db_prefix\'] =')) {
			$config_contents = preg_replace('/^\$config\[\'db_prefix\'\] =.*;/mi', "\$config['db_prefix'] = '" . addslashes($db_prefix) . "';", $config_contents);
		}
    }
    if (fn_put_contents('/install/config.install.php', $config_contents, $basedir) == 0) {
        fn_put_contents('/install/config.install.php', $default, $basedir);
        echo 'can\' write file config.install.php';
		exit();
	}
    
    $mysqli_contents = $default = file_get_contents($basedir . '/config.php');
	if (!empty($mysqli_contents)) {
		if (strstr($mysqli_contents, '$db[\'db_host\'] =')) {
			$mysqli_contents = preg_replace('/^\$db\[\'db_host\'\] =.*;/mi', "\$db['db_host'] = '" . addslashes($db_host) . "';", $mysqli_contents);
		}
        if (strstr($mysqli_contents, '$db[\'db_user\'] =')) {
			$mysqli_contents = preg_replace('/^\$db\[\'db_user\'\] =.*;/mi', "\$db['db_user'] = '" . addslashes($db_user) . "';", $mysqli_contents);
		}
        if (strstr($mysqli_contents, '$db[\'db_pass\'] =')) {
			$mysqli_contents = preg_replace('/^\$db\[\'db_pass\'\] =.*;/mi', "\$db['db_pass'] = '" . addslashes($db_pass) . "';", $mysqli_contents);
		}
        if (strstr($mysqli_contents, '$db[\'db_name\'] =')) {
			$mysqli_contents = preg_replace('/^\$db\[\'db_name\'\] =.*;/mi', "\$db['db_name'] = '" . addslashes($db_name) . "';", $mysqli_contents);
		}
        if (strstr($mysqli_contents, '$db[\'db_prefix\'] =')) {
			$mysqli_contents = preg_replace('/^\$db\[\'db_prefix\'\] =.*;/mi', "\$db['db_prefix'] = '" . addslashes($db_prefix) . "';", $mysqli_contents);
		}
        
        if (strstr($mysqli_contents, '$smtp[\'host\'] =')) {
			$mysqli_contents = preg_replace('/^\$smtp\[\'host\'\] =.*;/mi', "\$smtp['host'] = '" . addslashes($smtp_host) . "';", $mysqli_contents);
		}
        if (strstr($mysqli_contents, '$smtp[\'username\'] =')) {
			$mysqli_contents = preg_replace('/^\$smtp\[\'username\'\] =.*;/mi', "\$smtp['username'] = '" . addslashes($smtp_user) . "';", $mysqli_contents);
		}
        if (strstr($mysqli_contents, '$smtp[\'password\'] =')) {
			$mysqli_contents = preg_replace('/^\$smtp\[\'password\'\] =.*;/mi', "\$smtp['password'] = '" . addslashes($smtp_pass) . "';", $mysqli_contents);
		}
        if (strstr($mysqli_contents, '$smtp[\'port\'] =')) {
			$mysqli_contents = preg_replace('/^\$smtp\[\'port\'\] =.*;/mi', "\$smtp['port'] = '" . addslashes($smtp_port) . "';", $mysqli_contents);
		}
        
        if (strstr($mysqli_contents, '$options[\'show_nickh2d\'] =')) {
			$mysqli_contents = preg_replace('/^\$options\[\'show_nickh2d\'\] =.*;/mi', "\$options['show_nickh2d'] = " . addslashes($option_show_nickh2d) . ";", $mysqli_contents);
		}
        if (strstr($mysqli_contents, '$options[\'show_nickskype\'] =')) {
			$mysqli_contents = preg_replace('/^\$options\[\'show_nickskype\'\] =.*;/mi', "\$options['show_nickskype'] = " . addslashes($option_show_nickskype) . ";", $mysqli_contents);
		}
    }
    if (fn_put_contents('/config.php', $mysqli_contents, $basedir) == 0) {
        fn_put_contents('/config.php', $default, $basedir);
        echo 'can\'t write file /config.php';
		exit();
	}
    $mess = 'database '.$db_name.' created';
	$next_mode = 'summary';

}


// Summary page
if ($mode == 'summary') {
    include('config.install.php');
	$mysqli = new mysqli($config['db_host'], $config['db_user'], $config['db_pass'], $config['db_name']);
    if ($mysqli->connect_errno) {
        echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }
	//
//	$install_content = $default = file_get_contents($basedir . '/index.php');
//    if (!empty($install_content)) {
//    	if (strstr($install_content, '$install[\'install\'] =')) {
//    		$install_content = preg_replace('/^\$install\[\'install\'\] =.*;/mi', "\$install['install'] = 'No';", $install_content);
//    	}
//    }
//    if (fn_put_contents('/index.php', $install_content, $basedir) == 0) {
//        fn_put_contents('/index.php', $default, $basedir);
//        echo 'can\' write file /index.php';
//		exit();
//	}
    $mess = 'write config files successfully';
    $next_mode = 'finish';
}
if ($mode == 'finish') {
    // unset cookies
    if (isset($_SERVER['HTTP_COOKIE'])) {
        $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
        if(!empty($cookies))
        foreach($cookies as $cookie) {
            $parts = explode('=', $cookie);
            $name = trim($parts[0]);
            setcookie($name, '', time()-1000);
            setcookie($name, '', time()-1000, '/');
        }
    }
    header('location: ../index.php');
    exit;
}
?>
