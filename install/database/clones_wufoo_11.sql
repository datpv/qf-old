-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 29, 2013 at 03:48 PM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `clones_wufoo_11`
--

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_cached_entries_as`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_cached_entries_as` (
  `cache_id` int(11) NOT NULL AUTO_INCREMENT,
  `cache_user_id` int(11) NOT NULL DEFAULT '0',
  `cache_user_username` varchar(255) NOT NULL,
  `cache_user_email` varchar(255) NOT NULL,
  `cache_user_nickh2d` varchar(255) NOT NULL,
  `cache_user_nickskype` varchar(255) NOT NULL,
  `cache_user_fullname` varchar(255) NOT NULL,
  `cache_user_class` varchar(255) NOT NULL,
  `cache_user_content` text NOT NULL,
  `cache_entry_id` int(11) NOT NULL,
  `cache_entry_uuid` varchar(36) NOT NULL DEFAULT '0',
  `cache_entry_public` varchar(2) NOT NULL DEFAULT 'AO',
  `cache_entry_ip` varchar(40) NOT NULL DEFAULT '0',
  `cache_entry_content` text NOT NULL,
  `cache_entry_complete` int(1) NOT NULL DEFAULT '1',
  `cache_entry_t` int(11) NOT NULL DEFAULT '0',
  `cache_entry_hs` int(4) NOT NULL DEFAULT '0',
  `cache_entry_tg` int(4) NOT NULL DEFAULT '0',
  `cache_entry_sys` int(4) NOT NULL DEFAULT '0',
  `cache_entry_sys_t` int(11) NOT NULL DEFAULT '0',
  `cache_form_nb_id` int(11) NOT NULL,
  `cache_form_nb_uuid` varchar(36) NOT NULL DEFAULT '0',
  `cache_form_sc_uuid` varchar(36) NOT NULL DEFAULT '0',
  `cache_form_nb_type` varchar(2) NOT NULL DEFAULT 'NO',
  `cache_form_nb_name` varchar(255) NOT NULL,
  `cache_form_nb_status` varchar(1) NOT NULL DEFAULT 'A',
  `cache_form_nb_theme_id` int(11) NOT NULL DEFAULT '1',
  `cache_form_nb_content` text NOT NULL,
  `cache_form_nb_password` varchar(255) NOT NULL,
  `cache_form_nb_display_result` text NOT NULL,
  `cache_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cache_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cache_content` text NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_cached_entries_nb`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_cached_entries_nb` (
  `cache_id` int(11) NOT NULL AUTO_INCREMENT,
  `cache_user_id` int(11) NOT NULL DEFAULT '0',
  `cache_user_username` varchar(255) NOT NULL,
  `cache_user_email` varchar(255) NOT NULL,
  `cache_user_nickh2d` varchar(255) NOT NULL,
  `cache_user_nickskype` varchar(255) NOT NULL,
  `cache_user_fullname` varchar(255) NOT NULL,
  `cache_user_class` varchar(255) NOT NULL,
  `cache_user_content` text NOT NULL,
  `cache_entry_id` int(11) NOT NULL,
  `cache_entry_uuid` varchar(36) NOT NULL DEFAULT '0',
  `cache_entry_public` varchar(2) NOT NULL DEFAULT 'AO',
  `cache_entry_ip` varchar(40) NOT NULL DEFAULT '0',
  `cache_entry_content` text NOT NULL,
  `cache_entry_complete` int(1) NOT NULL DEFAULT '1',
  `cache_entry_t` int(11) NOT NULL DEFAULT '0',
  `cache_entry_hs` int(4) NOT NULL DEFAULT '0',
  `cache_entry_tg` int(4) NOT NULL DEFAULT '0',
  `cache_entry_sys` int(4) NOT NULL DEFAULT '0',
  `cache_entry_sys_t` int(11) NOT NULL DEFAULT '0',
  `cache_form_nb_id` int(11) NOT NULL,
  `cache_form_nb_uuid` varchar(36) NOT NULL DEFAULT '0',
  `cache_form_sc_uuid` varchar(36) NOT NULL DEFAULT '0',
  `cache_form_nb_type` varchar(2) NOT NULL DEFAULT 'NO',
  `cache_form_nb_name` varchar(255) NOT NULL,
  `cache_form_nb_status` varchar(1) NOT NULL DEFAULT 'A',
  `cache_form_nb_theme_id` int(11) NOT NULL DEFAULT '1',
  `cache_form_nb_content` text NOT NULL,
  `cache_form_nb_display_result` text NOT NULL,
  `cache_form_nb_password` varchar(255) NOT NULL,
  `cache_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cache_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cache_content` text NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=347 ;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_cached_entries_nbed`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_cached_entries_nbed` (
  `cache_id` int(11) NOT NULL AUTO_INCREMENT,
  `cache_user_id` int(11) NOT NULL DEFAULT '0',
  `cache_user_username` varchar(255) NOT NULL,
  `cache_user_email` varchar(255) NOT NULL,
  `cache_user_nickh2d` varchar(255) NOT NULL,
  `cache_user_nickskype` varchar(255) NOT NULL,
  `cache_user_fullname` varchar(255) NOT NULL,
  `cache_user_class` varchar(255) NOT NULL,
  `cache_user_content` text NOT NULL,
  `cache_user_by_id` int(11) NOT NULL DEFAULT '0',
  `cache_user_by_username` varchar(255) NOT NULL,
  `cache_user_by_email` varchar(255) NOT NULL,
  `cache_user_by_nickh2d` varchar(255) NOT NULL,
  `cache_user_by_nickskype` varchar(255) NOT NULL,
  `cache_user_by_fullname` varchar(255) NOT NULL,
  `cache_user_by_class` varchar(255) NOT NULL,
  `cache_user_by_content` text NOT NULL,
  `cache_entry_id` int(11) NOT NULL,
  `cache_entry_uuid` varchar(36) NOT NULL DEFAULT '0',
  `cache_entry_public` varchar(2) NOT NULL DEFAULT 'AO',
  `cache_entry_ip` varchar(40) NOT NULL DEFAULT '0',
  `cache_entry_content` text NOT NULL,
  `cache_entry_complete` int(1) NOT NULL DEFAULT '1',
  `cache_entry_t` int(11) NOT NULL DEFAULT '0',
  `cache_entry_hs` int(4) NOT NULL DEFAULT '0',
  `cache_entry_tg` int(4) NOT NULL DEFAULT '0',
  `cache_entry_sys` int(4) NOT NULL DEFAULT '0',
  `cache_entry_sys_t` int(11) NOT NULL DEFAULT '0',
  `cache_form_nb_id` int(11) NOT NULL,
  `cache_form_nb_uuid` varchar(36) NOT NULL DEFAULT '0',
  `cache_form_sc_uuid` varchar(36) NOT NULL DEFAULT '0',
  `cache_form_nb_type` varchar(2) NOT NULL DEFAULT 'NO',
  `cache_form_nb_name` varchar(255) NOT NULL,
  `cache_form_nb_status` varchar(1) NOT NULL DEFAULT 'A',
  `cache_form_nb_theme_id` int(11) NOT NULL DEFAULT '1',
  `cache_form_nb_content` text NOT NULL,
  `cache_form_nb_password` varchar(255) NOT NULL,
  `cache_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cache_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cache_content` text NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=823 ;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_cached_entries_sced`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_cached_entries_sced` (
  `cache_id` int(11) NOT NULL AUTO_INCREMENT,
  `cache_user_id` int(11) NOT NULL DEFAULT '0',
  `cache_user_username` varchar(255) NOT NULL,
  `cache_user_email` varchar(255) NOT NULL,
  `cache_user_nickh2d` varchar(255) NOT NULL,
  `cache_user_nickskype` varchar(255) NOT NULL,
  `cache_user_fullname` varchar(255) NOT NULL,
  `cache_user_class` varchar(255) NOT NULL,
  `cache_user_content` text NOT NULL,
  `cache_user_of_id` int(11) NOT NULL DEFAULT '0',
  `cache_user_of_username` varchar(255) NOT NULL,
  `cache_user_of_email` varchar(255) NOT NULL,
  `cache_user_of_nickh2d` varchar(255) NOT NULL,
  `cache_user_of_nickskype` varchar(255) NOT NULL,
  `cache_user_of_fullname` varchar(255) NOT NULL,
  `cache_user_of_class` varchar(255) NOT NULL,
  `cache_user_of_content` text NOT NULL,
  `cache_entry_id` int(11) NOT NULL,
  `cache_entry_uuid` varchar(36) NOT NULL DEFAULT '0',
  `cache_entry_public` varchar(2) NOT NULL DEFAULT 'AO',
  `cache_entry_ip` varchar(40) NOT NULL DEFAULT '0',
  `cache_entry_content` text NOT NULL,
  `cache_entry_complete` int(1) NOT NULL DEFAULT '1',
  `cache_entry_t` int(11) NOT NULL DEFAULT '0',
  `cache_entry_hs` int(4) NOT NULL DEFAULT '0',
  `cache_entry_tg` int(4) NOT NULL DEFAULT '0',
  `cache_entry_sys` int(4) NOT NULL DEFAULT '0',
  `cache_entry_sys_t` int(11) NOT NULL DEFAULT '0',
  `cache_form_nb_id` int(11) NOT NULL,
  `cache_form_nb_uuid` varchar(36) NOT NULL DEFAULT '0',
  `cache_form_sc_uuid` varchar(36) NOT NULL DEFAULT '0',
  `cache_form_nb_type` varchar(2) NOT NULL DEFAULT 'NO',
  `cache_form_nb_name` varchar(255) NOT NULL,
  `cache_form_nb_status` varchar(1) NOT NULL DEFAULT 'A',
  `cache_form_nb_theme_id` int(11) NOT NULL DEFAULT '1',
  `cache_form_nb_content` text NOT NULL,
  `cache_form_nb_password` varchar(255) NOT NULL,
  `cache_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cache_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cache_content` text NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=293 ;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_cached_users_stats`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_cached_users_stats` (
  `cache_id` int(11) NOT NULL AUTO_INCREMENT,
  `cache_stats_t` int(11) NOT NULL DEFAULT '0',
  `cache_stats_hs` int(4) NOT NULL DEFAULT '0',
  `cache_stats_tg` int(4) NOT NULL DEFAULT '0',
  `cache_stats_sys` int(4) NOT NULL DEFAULT '0',
  `cache_stats_sys_t` int(11) NOT NULL DEFAULT '0',
  `cache_user_id` int(11) NOT NULL,
  `cache_user_status` varchar(1) NOT NULL DEFAULT 'D',
  `cache_user_username` varchar(255) NOT NULL,
  `cache_user_email` varchar(255) NOT NULL,
  `cache_user_nickh2d` varchar(255) NOT NULL,
  `cache_user_nickskype` varchar(255) NOT NULL,
  `cache_user_fullname` varchar(255) NOT NULL,
  `cache_user_class` varchar(255) NOT NULL,
  `cache_user_content` text NOT NULL,
  `cache_time` int(11) NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=142 ;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_categories`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) NOT NULL,
  `category_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category_parent` int(11) NOT NULL DEFAULT '0',
  `category_public` varchar(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_categories_meta`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_categories_meta` (
  `category_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_comments`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_reply` int(11) NOT NULL,
  `comment_title` varchar(255) NOT NULL,
  `comment_content` text NOT NULL,
  `comment_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comment_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_user` int(11) NOT NULL DEFAULT '0',
  `comment_status` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=59 ;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_courses`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_courses` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(255) NOT NULL,
  `course_description` varchar(500) NOT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_courses_meta`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_courses_meta` (
  `group_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_entries`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_entries` (
  `entry_id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_uuid` varchar(36) NOT NULL DEFAULT '0',
  `entry_type` varchar(2) NOT NULL DEFAULT 'BT',
  `entry_user` int(11) NOT NULL DEFAULT '0',
  `entry_of` varchar(36) NOT NULL DEFAULT '0',
  `entry_scoring` text NOT NULL,
  `entry_public` varchar(2) NOT NULL DEFAULT 'AO',
  `entry_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `entry_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `entry_ip` varchar(24) NOT NULL DEFAULT '0',
  `entry_content` text NOT NULL,
  `entry_complete` int(1) NOT NULL DEFAULT '1',
  `entry_flag` int(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=420 ;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_entries_meta`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_entries_meta` (
  `entry_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  PRIMARY KEY (`entry_id`,`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_fields`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_fields` (
  `field_id` int(11) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(255) NOT NULL,
  `field_content` text NOT NULL,
  `field_default` int(1) NOT NULL DEFAULT '0',
  `field_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`field_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_forms`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_forms` (
  `form_id` int(11) NOT NULL AUTO_INCREMENT,
  `theme_id` int(11) NOT NULL DEFAULT '1',
  `form_uuid` varchar(36) NOT NULL,
  `form_type` varchar(2) NOT NULL DEFAULT 'NO',
  `form_content` text NOT NULL,
  `form_notify` text NOT NULL,
  `form_status` varchar(1) NOT NULL DEFAULT 'A',
  `form_flag` int(11) NOT NULL DEFAULT '0',
  `form_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `form_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `form_password` varchar(32) NOT NULL,
  `form_user` int(11) NOT NULL DEFAULT '1',
  `form_approved` varchar(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`form_id`),
  KEY `theme_id` (`theme_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=496 ;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_forms_log`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_forms_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `field_id` int(11) unsigned NOT NULL,
  `log_subject` varchar(255) NOT NULL,
  `log_content` text NOT NULL,
  `log_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `log_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log_hit` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=146 ;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_forms_log_meta`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_forms_log_meta` (
  `log_id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_forms_log_user`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_forms_log_user` (
  `log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_groups`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_groups_meta`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_groups_meta` (
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_reports`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_reports` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `report_uuid` varchar(36) NOT NULL,
  `form_id` int(11) NOT NULL,
  `report_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `report_content` text NOT NULL,
  `report_status` varchar(1) NOT NULL DEFAULT 'A',
  `report_password` varchar(32) NOT NULL,
  PRIMARY KEY (`report_id`),
  KEY `form_id` (`form_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_rules`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_rules` (
  `rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_type` varchar(30) NOT NULL,
  `rule_content` text NOT NULL,
  `rule_status` varchar(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_themes`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_themes` (
  `theme_id` int(11) NOT NULL AUTO_INCREMENT,
  `theme_name` varchar(100) NOT NULL DEFAULT 'Default Theme',
  `theme_content` text NOT NULL,
  PRIMARY KEY (`theme_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `wufoo_11_themes`
--

INSERT INTO `wufoo_11_themes` (`theme_id`, `theme_name`, `theme_content`) VALUES
(1, 'Default Theme', '{"styleId":1,"name":"Create New","css":"Default, ..\\/images\\/htdlogo.png, inherit, normal, 160%, #000000, inherit, normal, 95%, #444444, inherit, normal, 110%, #000000, inherit, normal, 85%, #444444, inherit, bold, 95%, #444444, inherit, normal, 100%, #333333, inherit, normal, 80%, #444444, #EEEEEE, none, #DEDEDE, none, #FFFFFF, #FFFFFF url(..\\/images\\/fieldbg.gif) repeat-x top, #FFF7C0, #F5F5F5, 1px, solid, #CCCCCC, 1px, dotted, #CCCCCC, 1px, solid, #E6E6E6, visible, 40","buttonType":"text","buttonValue":"Submit","customCss":"","integration":null,"formStyle":"0"}');

-- --------------------------------------------------------

--
-- Table structure for table `wufoo_11_users`
--

CREATE TABLE IF NOT EXISTS `wufoo_11_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_content` text NOT NULL,
  `user_type` varchar(1) NOT NULL DEFAULT 'C',
  `user_status` varchar(1) NOT NULL DEFAULT 'D',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `wufoo_11_users`
--

INSERT INTO `wufoo_11_users` (`user_id`, `username`, `password`, `user_email`, `user_content`, `user_type`, `user_status`) VALUES
(1, 'admin', '123456', 'admin@localhost.com', '{"Name":"Admin","NickH2d":"","NickSkype":""}', 'A', 'A');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
