<?php
include('path.php');
define('DEFAULT_FILE_PERMISSIONS', 0666);
define('DEFAULT_DIR_PERMISSIONS', 0777);
include('core/install.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Install</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" />
</head>
	<body>
		<form enctype="multipart/form-data" name="installform" action="index.php" method="post">
    		<input type="hidden" name="mode" value="<?php echo !empty($next_mode) ? $next_mode : '';?>" />
            <?php if($mode == 'prepare') { ?>
            <div class="sep">
                <div><label for="db_host">DB host: </label><input value="localhost" type="text" name="db_host" id="db_host" /></div>
        		<div><label for="db_user">DB user: </label><input value="root" type="text" name="db_user" id="db_user" /></div>
                <div><label for="db_pass">DB password: </label><input type="password" name="db_pass" id="db_pass" /></div>
                <div><label for="db_name">DB name: </label><input type="text" name="db_name" id="db_name" /></div>
                <div><label for="db_prefix">DB new prefix: </label><input type="text" name="db_prefix" id="db_prefix" value="new_prefix_" /></div>
                <div><label for="old_db_prefix">DB old prefix: </label><input type="text" name="old_db_prefix" id="old_db_prefix" value="old_prefix_" /></div>
                
                <div><label for="db_prefix">DB content: </label><input type="file" name="db_file" id="db_file" /></div>
            </div>
            <div class="sep">
                <div><label for="db_admin_user">Admin username: </label><input type="text" name="db_admin_user" id="db_admin_user" value="admin" /></div>
                <div><label for="db_admin_pass">Admin password: </label><input type="text" name="db_admin_pass" id="db_admin_pass" value="123456" /></div>
                <div><label for="db_admin_email">Admin email: </label><input type="text" name="db_admin_email" id="db_admin_email" value="admin@localhost.com" /></div>
                <div><label for="db_admin_number">Admin phone: </label><input type="text" name="db_admin_number" id="db_admin_number" value="0123456789" /></div>
            </div>
            <div class="sep">
                <div><label for="option_show_nickh2d">Show Nick H2d: </label><input type="checkbox" name="option_show_nickh2d" id="option_show_nickh2d" checked="checked" /></div>
                <div><label for="option_show_nickskype">Show Nick Skype: </label><input type="checkbox" name="option_show_nickskype" id="option_show_nickskype" checked="checked" /></div>
            </div>
            <div class="sep">
                <div><label for="smtp_host">SMTP host: </label><input value="smtp.gmail.com" type="text" name="smtp_host" id="smtp_host" /></div>
                <div><label for="smtp_port">SMTP port: </label><input value="465" type="text" name="smtp_port" id="smtp_port" /></div>
        		<div><label for="smtp_user">SMTP user: </label><input value="" type="text" name="smtp_user" id="smtp_user" /></div>
                <div><label for="smtp_pass">SMTP password: </label><input type="text" name="smtp_pass" id="smtp_pass" /></div>
            </div>
            <?php } elseif($mode == 'database') {
                echo $mess;
            } elseif($mode == 'summary') {
                echo $mess;
            } ?>
            <input value="done" type="submit" />
		</form>
	</body>
</html>
