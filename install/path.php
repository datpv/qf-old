<?php
$_s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
$_sp = strtolower($_SERVER["SERVER_PROTOCOL"]);
$_protocol = substr($_sp, 0, strpos($_sp, "/")) . $_s;
$_port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
$_host = (isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST']))? $_SERVER['HTTP_HOST']:$_SERVER['SERVER_NAME'];
$_url = $_protocol . "://" . $_host . $_port . $_SERVER['REQUEST_URI'];
$_path = str_replace('index.php','',parse_url($_url, PHP_URL_PATH));
$_path = str_replace(array('/install/','/install'),'',$_path);
$_path = rtrim($_path,"/");
$_fullpath = $_protocol . "://" . $_host . $_port . str_replace('/install/index.php','',parse_url($_url, PHP_URL_PATH));
$_fullpath_ser = $_protocol . "s://" . $_host . $_port . str_replace('/install/index.php','',parse_url($_url, PHP_URL_PATH));
$basedir = rtrim($_SERVER['DOCUMENT_ROOT'],"/") . $_path;