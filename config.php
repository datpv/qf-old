<?php
$db['db_host'] = 'localhost';
$db['db_name'] = 'clones_wufoo_1';
$db['db_user'] = 'root';
$db['db_pass'] = '';
$db['db_prefix'] = '';
$smtp['host'] = 'smtp.gmail.com';
$smtp['port'] = '465';
$smtp['username'] = '';
$smtp['password'] = '';
$options['show_nickh2d'] = false;
$options['show_nickskype'] = false;
    
    define('DB_PREFIX', $db['db_prefix']);
    define('__SITE_PATH', realpath(dirname(__FILE__)));
    define('__ABS_PATH', 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['SCRIPT_NAME']));

    define('GETOVER','');
    
    $catetory = array(array(
        'Category' => 'Integration',
        'Typeof' => 'Amazon',
        'SubFields' => array(array(
            'Typeof' => 'text',
            'ColumnId' => 'HitId',
            'ChoicesText' => 'MTurk HitId',
            'DefaultVal' => '0',
            'Price' => '0'
        ), array(
            'Typeof' => 'text',
            'ColumnId' => 'AssignmentId',
            'ChoicesText' => 'MTurk AssignmentId',
            'DefaultVal' => '0',
            'Price' => '0'
        ), array(
            'Typeof' => 'text',
            'ColumnId' => 'Environment',
            'ChoicesText' => 'MTurk Environment',
            'DefaultVal' => '0',
            'Price' => '0'
        )),
        'IsAltField' => true
    ), array(
        'Category' => 'Integration',
        'Typeof' => 'Payments',
        'SubFields' => array(array(
            'Typeof' => 'text',
            'ColumnId' => 'Status',
            'ChoicesText' => 'Payment Status',
            'DefaultVal' => '0',
            'Price' => '0'
        ), array(
            'Typeof' => 'text',
            'ColumnId' => 'PurchaseTotal',
            'ChoicesText' => 'Payment Total',
            'DefaultVal' => '0',
            'Price' => '0'
        ), array(
            'Typeof' => 'text',
            'ColumnId' => 'Currency',
            'ChoicesText' => 'Payment Currency',
            'DefaultVal' => '0',
            'Price' => '0'
        ), array(
            'Typeof' => 'text',
            'ColumnId' => 'TransactionId',
            'ChoicesText' => 'Payment Confirmation',
            'DefaultVal' => '0',
            'Price' => '0'
        ), array(
            'Typeof' => 'text',
            'ColumnId' => 'MerchantType',
            'ChoicesText' => 'Payment Merchant',
            'DefaultVal' => '0',
            'Price' => '0'
        )),
        'IsAltField' => true
    ), array(
        'Category' => 'System',
        'Typeof' => 'System',
        'SubFields' => array(array(
            'Typeof' => 'text',
            'ColumnId' => 'EntryId',
            'ChoicesText' => 'Entry Id',
            'DefaultVal' => '0',
            'Price' => '0'
        ), array(
            'Typeof' => 'date',
            'ColumnId' => 'DateCreated',
            'ChoicesText' => 'Date Created',
            'DefaultVal' => '0',
            'Price' => '0'
        ), array(
            'Typeof' => 'text',
            'ColumnId' => 'CreatedBy',
            'ChoicesText' => 'Created By',
            'DefaultVal' => '0',
            'Price' => '0'
        ), array(
            'Typeof' => 'date',
            'ColumnId' => 'DateUpdated',
            'ChoicesText' => 'Last Updated',
            'DefaultVal' => '0',
            'Price' => '0'
        ), array(
            'Typeof' => 'text',
            'ColumnId' => 'UpdatedBy',
            'ChoicesText' => 'Updated By',
            'DefaultVal' => '0',
            'Price' => '0'
        )),
        'IsAltField' => true
    ), array(
        'Category' => 'Integration',
        'Typeof' => 'IPAddresses',
        'SubFields' => array(array(
            'Typeof' => 'text',
            'ColumnId' => 'IP',
            'ChoicesText' => 'IP Address',
            'DefaultVal' => '0',
            'Price' => '0'
        ), array(
            'Typeof' => 'text',
            'ColumnId' => 'LastPage',
            'ChoicesText' => 'Last Page Accessed',
            'DefaultVal' => '0',
            'Price' => '0'
        ), array(
            'Typeof' => 'text',
            'ColumnId' => 'CompleteSubmission',
            'ChoicesText' => 'Completion Status',
            'DefaultVal' => '0',
            'Price' => '0'
        )),
        'IsAltField' => true
    ));