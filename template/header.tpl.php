<?php
include('getover.php');
$routers = $this->registry->router->getRouters();
$siteUrl = $this->helpers->getUrl();
?><!DOCTYPE html>
<html class="firefox en">
	<head>
		<title>
			<?php echo isset($title)?$title:'Hoctudau Quiz Form'; ?> - Project Form Builder, Quiz Manager By Hoctudau.com
		</title>
		<!-- Meta -->
		<meta charset="utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <?php
        if($routers['controller'] == 'index') {
        ?>
        
        <meta name="robots" content="index, follow" />
        <meta name="author" content="Hoctudau Team" />
        <?php
        if(isset($meta_keyword)) {
        ?>
        
        <meta name="keywords" content="<?php echo $meta_keyword; ?>" />
        <?php    
        } else {
        ?>
        
        <meta name="keywords" content="hoctudau quiz form, quiz form, scoring manager, student manager, online" />
        <?php
        }
        if(isset($meta_description)) {
        ?>
        
        <meta name="description" content="<?php echo $meta_description; ?>" />
        <?php    
        } else {
        ?>
        
        <meta name="description" content="Hoctudau Quiz Form, Student Scoring Manager" />
        <?php    
        }
        } else {
        ?>
        
        <meta name="robots" content="noindex, nofollow" />
        <?php
        }
        ?>
        
        <base href="<?php echo __ABS_PATH; ?>/" />
        
		<!-- CSS -->
        <link rel="stylesheet" href="template/stylesheets/base.css" />
        <?php
        $body_id = 'public'; $body_class = $routers['controller'];
        if($routers['controller'] == 'index') {
            $body_id = 'home';
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/global/css/index.814.css" />
        <link rel="stylesheet" href="template/stylesheets/landing/css/index.814.css" />
        <?php
        } elseif($routers['controller'] == 'auth' && $routers['action'] == 'login' || isset($pass_require)) {
            $body_id = 'public'; $body_class = 'login';
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/admin/css/index.740.css" />
        <link rel="stylesheet" href="template/stylesheets/system/login/css/index.740.css" />
        <?php
        } elseif($routers['controller'] == 'auth' && $routers['action'] == 'signup') {
            $body_id = 'creation';
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/public/forms/css/index.768.css" />
        <link rel="stylesheet" href="template/stylesheets/signup/creation/css/index.768.css" />
        <?php
        }  elseif($routers['controller'] == 'forms' && !isset($pass_require)) {
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/admin/css/index.740.css" />
        <link rel="stylesheet" href="template/stylesheets/public/forms/css/index.740.css" />
        <?php
        } elseif($routers['controller'] == 'report' && $routers['action'] == 'builder') {
            $body_id = 'report'; $body_class = 'step0 dragtrick zoom';
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/admin/css/index.740.css" />
		<link rel="stylesheet" href="template/stylesheets/builder/report/css/index.740.css" />
        <?php
        } elseif($routers['controller'] == 'report' && $routers['action'] == 'auto_builder') {
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/admin/css/index.740.css" />
        <link rel="stylesheet" href="template/stylesheets/builder/report/css/index.740.css" />
        <?php
        } elseif($routers['controller'] == 'subjects' && ($routers['action'] == 'manager' || $routers['action'] == 'index') ) {
            $body_id = 'subjects'; $body_class = '';
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/admin/css/index.740.css" />
		<link rel="stylesheet" href="template/stylesheets/manager/subjects/css/index.css" />
        <?php
        } elseif($routers['controller'] == 'subjects' && ($routers['action'] == 'builder') ) {
            $body_id = 'subjects'; $body_class = '';
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/admin/css/index.724.css" />
        <link rel="stylesheet" href="template/stylesheets/builder/subject/css/calendar.css" />
        <link rel="stylesheet" href="template/stylesheets/builder/subject/css/subject.css" />
        <link rel="stylesheet" href="template/stylesheets/builder/subject/css/sample.css" />
        <?php
        } elseif($routers['controller'] == 'subjects' && ($routers['action'] == 'view' || $routers['action'] == 'all') ) {
            $body_id = 'subjects'; $body_class = '';
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/admin/css/index.724.css" />
        <link rel="stylesheet" href="template/stylesheets/public/subjects/css/subject.css" />
        <?php
        } elseif($routers['controller'] == 'reports' && $routers['action'] == 'index' && !isset($report_detail)) {
            $body_id = 'reports'; $body_class = '';
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/admin/css/index.740.css" />
		<link rel="stylesheet" href="template/stylesheets/manager/reports/css/index.740.css" />
        <?php
        } elseif($routers['controller'] == 'reports' && isset($report_detail)) {
            $body_id = 'report'; $body_class = 'hoctudau '.$_report['Layout'];
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/public/reports/css/index.740.css" />
        <?php
        } elseif($routers['controller'] == 'review' || $routers['controller'] == 'summary') {
            $body_id = 'report'; $body_class = 'hoctudau layout1';
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/admin/css/index.740.css" />
        <link rel="stylesheet" href="template/stylesheets/public/reports/css/index.740.css" />
        <?php
        } elseif($routers['controller'] == 'statistic') {
            $body_id = 'report'; $body_class = 'hoctudau layout2';
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/admin/css/index.740.css" />
        <link rel="stylesheet" href="template/stylesheets/public/reports/css/index.740.css" />
        <?php
        } elseif($routers['controller'] == 'dashboard' || $routers['controller'] == 'caches' || $routers['controller'] == 'groups' || $routers['controller'] == 'courses') {
        $body_id = 'account';
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/admin/css/index.768.css" />
        <link rel="stylesheet" href="template/stylesheets/manager/account/css/index.768.css" />
        <?php
        } elseif($routers['controller'] == 'admin') {
            $body_id = 'forms';
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/admin/css/index.724.css" />
        <link rel="stylesheet" href="template/stylesheets/manager/forms/css/index.724.css" />
        <?php
        } elseif($routers['controller'] == 'build') {
            $body_id = 'build'; $body_class = '';
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/admin/css/index.724.css" />
        <link rel="stylesheet" href="template/stylesheets/builder/form/css/index.724.css" />
        <?php
        } elseif($routers['controller'] == 'entries') {
            $body_class = 'first';
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/admin/css/index.740.css" />
        <link rel="stylesheet" href="template/stylesheets/manager/entries/css/index.740.css" />
        <?php
        } elseif($routers['controller'] == 'notifications') {
            $body_id = 'notify';
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/admin/css/index.744.css" />
        <link rel="stylesheet" href="template/stylesheets/settings/notification/css/index.744.css" />
        <?php
        } elseif($routers['controller'] == 'confirm') {
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/admin/css/index.740.css" />
        <link rel="stylesheet" href="template/stylesheets/public/forms/css/index.740.css" />
        <?php
        } elseif($routers['controller'] == 'notfound') {
            $body_id = 'error';
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/admin/css/index.css" />
		<link rel="stylesheet" href="template/stylesheets/system/errors/css/index.css" />
        <?php
        } elseif($routers['controller'] == 'accounts' || $routers['controller'] == 'database') {
            $body_id = 'account';
        ?>
        
        <link rel="stylesheet" href="template/stylesheets/admin/css/index.768.css" />
		<link rel="stylesheet" href="template/stylesheets/manager/account/css/index.768.css" />
        <link rel="stylesheet" href="template/stylesheets/manager/account/css/custom.css" />
        <?php
        } elseif($routers['controller'] == 'themes') {
            $body_id = 'themes'; $body_classes = 'themes';
        ?>
        
        <link href="template/stylesheets/admin/css/index.740.css" rel="stylesheet" />
		<link href="template/stylesheets/builder/theme/css/index.740.css" rel="stylesheet" />
		<!--[if !IE]><!-->
		<link href="template/stylesheets/builder/theme/css/fonts.740.css" rel="stylesheet" />
		<!--<![endif]-->
        <?php
        } elseif($routers['controller'] == 'importer') {
            $body_id = 'importer'; $body_classes = 'importer';
        ?>
        
        <link href="template/stylesheets/admin/css/index.724.css" rel="stylesheet" />
		<link href="template/stylesheets/builder/form/css/index.724.css" rel="stylesheet" />
        <?php
        } elseif($routers['controller'] == 'cwa') {
        ?>
        
        <link href="template/stylesheets/public/reports/css/index.740.css" rel="stylesheet" />
        <link href="template/stylesheets/global/css/index.991.css" rel="stylesheet" />
        <?php
        }
        ?>
		
		<!--[if lt IE 9]>
			<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
    <?php
 
    ?>
	<body id="<?php echo $body_id ?>" class="<?php echo $body_class; ?>">
        <?php
        if($routers['controller'] == 'report' && $routers['action'] == 'builder') {
        ?>
        <?php
        }
        if($routers['action'] != 'login' && $routers['controller'] != 'forms' && $routers['controller'] != 'notfound' && $routers['controller'] != 'confirm' && $routers['controller'] != 'cwa') {
        ?>
		<div id="container" class="<?php echo isset($hasFieldClass)?$hasFieldClass:''; ?>">
			<?php if($routers['controller'] != 'statistic' && $routers['controller'] != 'summary' && !isset($report_detail) && $routers['controller'] != 'auth') include('nav.tpl.php'); ?>
        <?php
        }
        ?>