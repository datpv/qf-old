<?php
include('getover.php');
?>
</div>
<?php if($routers['controller'] != 'auth' && $routers['controller'] != 'report') { ?>
<footer id="footer">
    <p>
		<a href="http://hoctudau.com/">Hoctudau.com</a>
		&middot; Đạo Tạo Lập Trình Web - Thiết Kế Web Trực Tuyến
	</p>
	<div class="links">
        <a href="http://hoctudau.com/info/">Hoctudau Info</a>
		&middot;
		<a href="http://hoctudau.com/h2d/">Hoctudau H2D</a>
		&middot;
		<a href="http://hoctudau.com/collabedit/">Hoctudau Collabedit</a>
	</div>
    <?php
    if($this->helpers->isAdmin()) {
    ?>
    <div>Time Load: <?php echo number_format(microtime(true) - $this->registry->timeload, 3) . ' seconds'; ?></div>
    <?php
    }
    ?>
</footer>
<!--footer-->
<?php } ?>
<div id="status">
    <div id="y" style="left: 0px; top: 0px; display: none;">
        <div id="statusText">Loading Data ...</div>
    </div>
</div>
<noscript>
	<style type="text/css">
        body, html{height:100%; overflow:hidden}
        .graph, .map{height:0 !important}
        select{visibility:hidden}
	</style>
	<div id="overlay">
	</div>
	<div id="lightbox" class="done error">
		<div id="lbContent">
			<div class="prompt">
				<h2>
					Please Turn On JavaScript To Use This Software
				</h2>
				<p>
					Read
					<a target="_blank" href="http://www.google.com/search?hl=en&q=how+to+enable+javascript">How to enable JavaScript</a>
					or
					<br />
					<a href="http://www.google.com/search?hl=en&safe=off&q=download+browser" target="_blank">download</a>
					a browser that allows you to run JavaScript.
				</p>
			</div>
		</div>
	</div>
</noscript>
<!-- JavaScript -->
<?php
if($routers['controller'] == 'index') {
    $body_id = 'account';
?>
<script src="template/scripts/global/dynamic-1-7.768.js"></script>
<?php
} elseif($routers['controller'] == 'auth' && $routers['action'] == 'login' || isset($pass_require)) {
?>
<script src="template/scripts/global/dynamic-1-7.740.js"></script>
<script src="template/scripts/signup/dynamic.740.js"></script>
<?php
} elseif($routers['controller'] == 'auth' && $routers['action'] == 'signup') {
?>
<script src="template/scripts/global/dynamic-1-7.768.js"></script>
<script src="template/scripts/account/creation/dynamic.768.js"></script>
<?php
} elseif($routers['controller'] == 'forms' && !isset($pass_require)) {
?>
<script>
__RULES = [];
__ENTRY = <?php echo isset($entry)?$entry:'[]'; ?>;
__PRICES = null;
</script>
<script src="template/scripts/global/dynamic-1-7.724.js"></script>
<script src="template/scripts/public/custom.js?language=english"></script>
<!--<script src="template/scripts/public/dynamic.740.js?language=english"></script>-->


<?php
}elseif($routers['controller'] == 'report' && $routers['action'] == 'builder') {
?>
<!-- JavaScript -->
<script type="text/javascript">
__REPORT = <?php if(isset($_report) && !empty($_report)): echo json_encode($_report); else: ?>{"ReportId":"","Name":"Untitled Report","IsPublic":"0","Url":"","Description":"This is my report. View it in all its glory!","FormId":<?php echo $result['form_id']; ?>,"MatchType":"all","HasGrid":0,"IsEditable":0,"PageSize":10,"ShowColumns":"","StyleId":"","DateCreated":"0","DateUpdated":"0","AllowExport":"0","Layout":"layout1","Conditions":[],"Graphs":<?php $grp = array();
if(!empty($form_orgn['Fields'])) {
    $ki = 0;
    foreach($form_orgn['Fields'] as $kfo => $fo) {
        
        if($fo['Typeof'] == 'likert') {
            foreach($fo['SubFields'] as $skfo => $sfo) {
                $grp[] = array(
                    'GraphId' => $ki,
                    'Name' => $sfo['Title'],
                    'Size' => '',
                    'Typeof' => 'fieldChart',
                    'FieldName' => 'Field'.$sfo['ColumnId'],
                    'ReportId' => '',
                    'Description' => '',
                    'Format' => '',
                    'Detail' => 'both',
                    'Color' => '',
                    'Url' => null,
                    'Zone' => '1',
                    'Position' => $ki,
                    'ChoiceId' => null,
                    'Width' => null,
                    'Height' => null
                );
                $ki++;
            }
        } else {
            $grp[] = array(
                'GraphId' => $kfo,
                'Name' => $fo['Title'],
                'Size' => '',
                'Typeof' => 'fieldChart',
                'FieldName' => 'Field'.$fo['ColumnId'],
                'ReportId' => '',
                'Description' => '',
                'Format' => '',
                'Detail' => 'both',
                'Color' => '',
                'Url' => null,
                'Zone' => '1',
                'Position' => $ki,
                'ChoiceId' => null,
                'Width' => null,
                'Height' => null
            );
        }
        
        $ki++;
    }   
}
echo json_encode($grp);
?>,"Widgets":[]}<?php endif; ?>;
__FORM = <?php echo json_encode($_form); ?>;
</script>
<script src="template/scripts/global/dynamic-1-7.740.js"></script>
<script src="template/scripts/reports/builder/dynamic.740.js?language=english"></script>
<?php
} elseif($routers['controller'] == 'accounts') {
?>

<script type="text/javascript">
__USERS = {<?php 
    $i = 0;
    $count = count($users);
    foreach($users as $user) {
        if(empty($user['user_content'])) continue;
        $_user = $this->helpers->json_decode_to_array($user['user_content']);
        ?>"<?php echo $user['user_id']; ?>":{"FormId":"<?php echo $user['user_id']; ?>","Url":"<?php echo $user['user_id']; ?>","Fullname":<?php echo json_encode($_user['Name']); ?>,"Nickh2d":<?php echo json_encode($_user['NickH2d']); ?>,"Nickskype":<?php echo json_encode($_user['NickSkype']); ?>,"Email":<?php echo json_encode($user['user_email']); ?>,"IsActivated":"<?php echo $user['user_status'].':'; ?>","GroupId":"","GroupName":"","Name":<?php echo json_encode($user['username']); ?>}<?php if($i<$count-1) echo ','; $i++;
    }
?>};
</script>
<script src="template/scripts/global/dynamic-1-7.768.js"></script>
<script src="template/scripts/account/manager/custom.js"></script>
<?php
}  elseif($routers['controller'] == 'report' && $routers['action'] == 'auto_builder') {
?>

<?php
} elseif($routers['controller'] == 'reports' && $routers['action'] == 'index' && !isset($report_detail)) {
?>
<script type="text/javascript">		
var __REPORTS = {<?php
    if($count > 0) {
        $i = 0;
        foreach($reports_result as $report) {
            if(empty($report['report_content'])) continue;
            $_report = $this->helpers->json_decode_to_array($report['report_content']);
            ?>"<?php echo $report['report_id']; ?>":{"ReportId":"<?php echo $report['report_id']; ?>","Url":"<?php echo $report['report_uuid']; ?>","Name":<?php echo json_encode($_report['Name']); ?>}<?php if($i<$count-1) echo ','; $i++;
        }
    }
?>};
</script>
<script src="template/scripts/global/dynamic-1-7.740.js"></script>
<script src="template/scripts/reports/control/dynamic.740.js"></script>
<?php
} elseif($routers['controller'] == 'reports' && isset($report_detail)) {
?>
<!-- JavaScript -->
<script type="text/javascript">
    REPORT= "";
    FORM= "";
</script>
<script src="template/scripts/global/dynamic-1-7.740.js"></script>
<script src="template/scripts/reports/view/dynamic.740.js"></script>
<?php
} elseif($routers['controller'] == 'statistic') {
?>
<script src="template/scripts/global/dynamic-1-7.768.js"></script>
<script src="template/scripts/public/stats.js"></script>
<?php
} elseif($routers['controller'] == 'summary') {
?>
<?php
} elseif($routers['controller'] == 'review') {
?>
<script src="template/scripts/global/dynamic-1-7.740.js"></script>
<script src="template/scripts/account/manager/dynamic.js"></script>
<?php
} elseif($routers['controller'] == 'dashboard') {
?>
<script src="template/scripts/global/dynamic-1-7.768.js"></script>
<script src="template/scripts/account/manager/dynamic.js"></script>
<?php
} elseif($routers['controller'] == 'caches') {
?>
<script src="template/scripts/global/dynamic-1-7.768.js"></script>
<script src="template/scripts/account/manager/caches.js"></script>
<?php
} elseif($routers['controller'] == 'groups') {
?>
<script src="template/scripts/global/dynamic-1-7.768.js"></script>
<script src="template/scripts/account/manager/groups.js"></script>
<?php
} elseif($routers['controller'] == 'courses') {
?>
<script src="template/scripts/global/dynamic-1-7.768.js"></script>
<script src="template/scripts/account/manager/groups.js"></script>
<?php
} elseif($routers['controller'] == 'admin') {
?>
<script type="text/javascript">
var __FORMS = {<?php
if($count > 0) {
    $i = 0;
    foreach($forms_result as $form) {
    if(empty($form['form_content'])) continue;
    $_form = $this->helpers->json_decode_to_array($form['form_content']);
    ?>"<?php echo $form['form_id']; ?>":{"FormId":"<?php echo $form['form_id']; ?>","Url":"<?php echo $form['form_uuid']; ?>","Name":<?php echo json_encode($_form['Name']); ?>}<?php if($i<$count-1) echo ','; $i++;
    }
}
?>};
</script>
<script src="template/scripts/global/dynamic-1-7.724.js"></script>
<script src="template/scripts/admin/dynamic.724.js"></script>
<?php
} elseif($routers['controller'] == 'build') {
?>
<script>
var FORM_JSON = <?php
if(isset($_form)) {
echo json_encode($_form); ?>;<?php
} else {
?>
{"FormId":"0","Name":"Untitled Form","Description":"This is my form. Please fill it out. It's awesome!","Formtype":"<?php if($formType=='1') echo 'score'; elseif($formType=='2') echo 'normal'; elseif($formType=='3') echo 'nopbai'; elseif($formType=='4') echo 'autoscore'; ?>","UserSubmitLimit":"","AjaxEnable":false,"AjaxShowWhatJustType":true,"AjaxLiveResponse":false,"FormScoreUrl":"","InputRightAnswerRegex":false,"InputRightAnswer":"","Password":"","RedirectMessage":"Great! Thanks for filling out my form!","Redirect":"","Url":"0","Email":"","StyleId":"1","IsPublic":"1","UniqueIP":"0","OffsetHeight":"0","ReplyTo":"0","ConfirmationFromAddress":"","ConfirmationSubject":"","ReceiptSubject":"0","ReceiptReplyTo":"","FromAddress":"","ReceiptMessage":"","ReceiptCopy":"","Language":"english","EmailNewEntries":"1","EmailNewComments":"0","PhoneNewEntries":"0","PhoneNewComments":"0","Mobile":"0","Carrier":"0","MerchantEnabled":"0","LabelAlign":"topLabel","StartDate":"2000-01-01 12:00:00","EndDate":"2030-01-01 12:00:00","EntryLimit":"0","PlainText":"0","UseCaptcha":"0","DateCreated":"0","DateUpdated":"0","BreadCrumbType":"1","ShowPageTitle":"1","ShowPageFooter":"1","PaymentPageTitle":"Checkout","RulesEnabled":"2","GAKey":"0","NonCriticalIntegrations":null,"EntryCount":null,"CurrentPage":false,"PageCount":1,"PageBreaks":[],"Fields":[],"EntryComments":[],"IntegrationList":[],"NoInstructions":true, "Entries":[]};
<?php
}
?>
var CLIENT_JSON = {"Profile":{"ProfileId": "916127", "Username": "datphan", "PlanId": "1", "AccountCreated": "2013-01-22", "User":{"UserId": "969766", "User": "datphan"},"Plan":{"PlanId": "1", "Upload": "0", "Price": "0.00", "Secure": "0", "Storage": "0", "FieldCount": "100", "EntriesCount": "", "FormsCount": "", "ReportsCount": "", "UsersCount": "1", "Paypal": "0", "PlanName": "Gratis"}}};
var FIELDS_JSON = {<?php
    if(!empty($fields)) {
        $count = count($fields);
        $i = 0;
        foreach($fields as $field) {?>"<?php echo $field['field_id']; ?>":{"field_name":<?php echo json_encode($field['field_name']); ?>,"field_default":<?php echo ($field['field_default']); ?>,"field_content":<?php echo ($field['field_content']); ?>}<?php if($i<$count-1) echo ','; $i++;
        }
    }?>};
</script>
<script src="template/scripts/global/dynamic-1-7.724.js"></script>
<script src="template/scripts/builder/dynamic.724.js?language=english"></script>
<?php
} elseif($routers['controller'] == 'entries') {
?>
<script type="text/javascript">

var FORM = <?php echo json_encode($_form); ?>;
var LOADED_ENTRY = '';
var LOADED_PAGE = '0';
var __RULES = [];

</script>
<script src="template/scripts/global/dynamic-1-7.740.js"></script>
<script src="template/scripts/entries/dynamic.740.js?language=english"></script>
<?php
} elseif($routers['controller'] == 'notifications') {
?>
<script type="text/javascript">
var FORM_NAME = '<?php echo isset($_form['Name'])?$_form['Name']:''; ?>';
var FORM_ID = '<?php echo isset($result['form_id'])?$result['form_id']:''; ?>';
var INTEGRATIONS = [];
</script>
<script src="template/scripts/global/dynamic-1-7.744.js"></script>
<script src="template/scripts/notifications/dynamic.744.js"></script>
<?php
} elseif($routers['controller'] == 'rules') {
?>
<script type="text/javascript">
__RULES = {"FieldRules":[],"PageRules":[],"FormRules":[]};
__FORM = <?php echo json_encode($_form); ?>;
__CHOICES = <?php echo json_encode($choices); ?>;
</script>
<script src="template/scripts/global/dynamic-1-7.766.js"></script>
<script src="template/scripts/rules/dynamic.766.js?language=english"></script>
<?php
} elseif($routers['controller'] == 'scoring') {
?>
<script src="template/scripts/global/dynamic-1-7.768.js"></script>
<script src="template/scripts/users/dynamic.999.js"></script>
<?php
} elseif($routers['controller'] == 'themes') {
?>
<script type="text/javascript">
	
LEVEL = '1';
__DEFAULT_THEME = {"themeName":"Default","logoUrl":"\/images\/htdlogo.png","ftFormTitleFamily":"inherit","ftFormTitleStyle":"normal","ftFormTitleSize":"160%","ftFormTitleColor":"#000000","ftFormDescriptionFamily":"inherit","ftFormDescriptionStyle":"normal","ftFormDescriptionSize":"95%","ftFormDescriptionColor":"#444444","ftSectionTitleFamily":"inherit","ftSectionTitleStyle":"normal","ftSectionTitleSize":"110%","ftSectionTitleColor":"#000000","ftSectionTextFamily":"inherit","ftSectionTextStyle":"normal","ftSectionTextSize":"85%","ftSectionTextColor":"#444444","ftFieldTitleFamily":"inherit","ftFieldTitleStyle":"bold","ftFieldTitleSize":"95%","ftFieldTitleColor":"#444444","ftFieldTextFamily":"inherit","ftFieldTextStyle":"normal","ftFieldTextSize":"100%","ftFieldTextColor":"#333333","ftInstructFamily":"inherit","ftInstructStyle":"normal","ftInstructSize":"80%","ftInstructColor":"#444444","bgHtmlColor":"#EEEEEE","bgHtmlPattern":"none","bgLogoColor":"#DEDEDE","bgLogoPattern":"none","bgFormColor":"#FFFFFF","bgFieldColor":"#FFFFFF url(..\/images\/fieldbg.gif) repeat-x top","bgHighlightColor":"#FFF7C0","bgInstructColor":"#F5F5F5","brFormThickness":"1px","brFormStyle":"solid","brFormColor":"#CCCCCC","brDividerThickness":"1px","brDividerStyle":"dotted","brDividerColor":"#CCCCCC","brInstructThickness":"1px","brInstructStyle":"solid","brInstructColor":"#E6E6E6","dropShadow":"visible","logoHeight":"40"};
__SELECTED_THEME_ID = 1;
__FONTS = {"Standard":{"Default":{"URL":"#","Designer":"","Group":1,"Page":1,"Styles":["normal","bold","italic"],"FontFamily":"inherit"},"Arial":{"URL":"#","Designer":"","Group":1,"Page":1,"Styles":["normal","bold","italic"],"FontFamily":"Arial, sans-serif"},"Courier New":{"URL":"#","Designer":"","Group":1,"Page":1,"Styles":["normal","bold","italic"],"FontFamily":"'Courier New', Courier, monospace"},"Georgia":{"URL":"#","Designer":"","Group":1,"Page":1,"Styles":["normal","bold","italic"],"FontFamily":"Georgia, serif"},"Palatino Linotype":{"URL":"#","Designer":"","Group":1,"Page":1,"Styles":["normal","bold","italic"],"FontFamily":"'Palatino Linotype', 'Book Antiqua', Palatino, serif"},"Tahoma":{"URL":"#","Designer":"","Group":1,"Page":1,"Styles":["normal","bold","italic"],"FontFamily":"Tahoma, Geneva, sans-serif"},"Trebuchet MS":{"URL":"#","Designer":"","Group":1,"Page":1,"Styles":["normal","bold","italic"],"FontFamily":"'Trebuchet MS', Helvetica, sans-serif"},"Times New Roman":{"URL":"#","Designer":"","Group":1,"Page":1,"Styles":["normal","bold","italic"],"FontFamily":"'Times New Roman', serif"},"Verdana":{"URL":"#","Designer":"","Group":1,"Page":1,"Styles":["normal","bold","italic"],"FontFamily":"Verdana, sans-serif"},"Arial Black":{"URL":"#","Designer":"","Group":1,"Page":2,"Styles":["normal","bold","italic"],"FontFamily":"'Arial Black', Gadget, sans-serif"},"Comic Sans MS":{"URL":"#","Designer":"","Group":1,"Page":2,"Styles":["normal","bold","italic"],"FontFamily":"'Comic Sans MS', cursive"}},"LocalI":{"Aller":{"Styles":["normal","bold","italic"],"Page":"1","Group":"1","Designer":"Dalton Maag Ltd","URL":"http:\/\/www.daltonmaag.com","FontFamily":"'Aller'"},"Allerdisplay":{"Styles":["normal"],"Page":"1","Group":"1","Designer":"Dalton Maag Ltd","URL":"http:\/\/www.daltonmaag.com","FontFamily":"'Allerdisplay'"},"Amadeus":{"Styles":["normal"],"Page":"1","Group":"1","Designer":"Bright Ideas","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Amadeus","FontFamily":"'Amadeus'"},"Angelina":{"Styles":["normal"],"Page":"1","Group":"1","Designer":"Anja Denali","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Angelina","FontFamily":"'Angelina'"},"Anivers":{"Styles":["normal"],"Page":"1","Group":"1","Designer":"Jos Buivenga","URL":"http:\/\/www.josbuivenga.demon.nl\/anivers.html","FontFamily":"'Anivers'"},"Ballpark Weiner":{"Styles":["normal"],"Page":"1","Group":"1","Designer":"Mickey Rossi","URL":"http:\/\/www.subflux.com","FontFamily":"'Ballpark Weiner'"},"Bebas":{"Styles":["normal"],"Page":"1","Group":"1","Designer":"Ryoichi Tsunekawa","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Bebas","FontFamily":"'Bebas'"},"Black Casper":{"Styles":["normal"],"Page":"1","Group":"1","Designer":"AllencHIU cHIU","URL":"http:\/\/www.fontsquirrel.com\/fonts\/BlackCasper","FontFamily":"'Black Casper'"},"Black Jack":{"Styles":["normal"],"Page":"1","Group":"1","Designer":"Typadelic","URL":"http:\/\/www.typadelic.com\/","FontFamily":"'Black Jack'"},"Black Rose":{"Styles":["normal"],"Page":"2","Group":"1","Designer":"Bright Ideas","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Black-Rose","FontFamily":"'Black Rose'"},"Boycott":{"Styles":["normal"],"Page":"2","Group":"1","Designer":"Ryoichi Tsunekawa","URL":"http:\/\/flat-it.com\/","FontFamily":"'Boycott'"},"Brock Script":{"Styles":["normal"],"Page":"2","Group":"1","Designer":"Dieter Steffmann","URL":"http:\/\/www.steffmann.de","FontFamily":"'Brock Script'"},"Broken15":{"Styles":["normal"],"Page":"2","Group":"1","Designer":"Eduardo Recife","URL":"http:\/\/www.misprintedtype.com","FontFamily":"'Broken15'"},"Burnstown Dam":{"Styles":["normal"],"Page":"2","Group":"1","Designer":"Ray Larabie","URL":"http:\/\/www.larabiefonts.com\/","FontFamily":"'Burnstown Dam'"},"Calluna":{"Styles":["normal"],"Page":"2","Group":"1","Designer":"Jos Buivenga","URL":"http:\/\/www.josbuivenga.demon.nl\/calluna.html","FontFamily":"'Calluna'"},"Candela":{"Styles":["bold","book","italic"],"Page":"2","Group":"1","Designer":"La Tipomatika","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Candela","FontFamily":"'Candela-Bold'"},"Carousel":{"Styles":["normal"],"Page":"2","Group":"1","Designer":"Bright Ideas","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Carousel","FontFamily":"'Carousel'"},"Carrington":{"Styles":["normal"],"Page":"2","Group":"1","Designer":"Bright Ideas","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Carrington","FontFamily":"'Carrington'"},"Caviar Dreams":{"Styles":["normal","bold","italic"],"Page":"3","Group":"1","Designer":"Nymphont","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Caviar-Dreams","FontFamily":"'Caviar Dreams'"},"Chantelli Antiqua":{"Styles":["normal"],"Page":"3","Group":"1","Designer":"Bernd Montag","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Chantelli-Antiqua","FontFamily":"'Chantelli Antiqua'"},"Chopin Script":{"Styles":["normal"],"Page":"3","Group":"1","Designer":"Dieter Steffmann","URL":"http:\/\/www.steffmann.de","FontFamily":"'Chopin Script'"},"Chunk":{"Styles":["normal"],"Page":"3","Group":"1","Designer":"Meredith Mandel","URL":"http:\/\/www.theleagueofmoveabletype.com\/fonts\/4-chunk","FontFamily":"'Chunk'"},"Cicle":{"Styles":["fina","fina_italic","gordita","gordita_italic","semi","semi_italic","shadow"],"Page":"3","Group":"1","Designer":"","URL":"#","FontFamily":"'Cicle-Fina'"},"Cloister":{"Styles":["black"],"Page":"3","Group":"1","Designer":"Dieter Steffman","URL":"http:\/\/www.steffmann.de","FontFamily":"'Cloister-Black'"},"College":{"Styles":["normal","bold"],"Page":"3","Group":"1","Designer":"Matthew Welch","URL":"http:\/\/www.fontsquirrel.com\/fonts\/College","FontFamily":"'College'"},"Comic Zine":{"Styles":["normal"],"Page":"3","Group":"1","Designer":"Jess Latham","URL":"http:\/\/www.bvfonts.com","FontFamily":"'Comic Zine'"},"Commando":{"Styles":["normal"],"Page":"3","Group":"1","Designer":"DefaultError","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Commando","FontFamily":"'Commando'"},"Deftone":{"Styles":["normal"],"Page":"4","Group":"1","Designer":"Ray Larabie","URL":"http:\/\/www.larabiefonts.com\/","FontFamily":"'Deftone'"},"Downcome":{"Styles":["normal"],"Page":"4","Group":"1","Designer":"Eduardo Recife","URL":"http:\/\/www.misprintedtype.com","FontFamily":"'Downcome'"},"Droid":{"Styles":["normal"],"Page":"4","Group":"1","Designer":"Ray Larabie","URL":"http:\/\/www.larabiefonts.com\/","FontFamily":"'Droid'"},"Edmunds":{"Styles":["normal"],"Page":"4","Group":"1","Designer":"Ray Larabie","URL":"http:\/\/www.larabiefonts.com\/","FontFamily":"'Edmunds'"},"Elliot Six":{"Styles":["normal"],"Page":"4","Group":"1","Designer":"","URL":"#","FontFamily":"'Elliot Six'"},"Fertigo Pro":{"Styles":["normal"],"Page":"4","Group":"1","Designer":"Jos Buivenga","URL":"http:\/\/www.josbuivenga.demon.nl\/fertigo.html","FontFamily":"'Fertigo Pro'"},"Folks":{"Styles":["normal","bold","heavy","light"],"Page":"4","Group":"1","Designer":"Manfred Klein","URL":"http:\/\/manfred-klein.ina-mar.com\/","FontFamily":"'Folks'"},"Fontin":{"Styles":["normal","bold","italic","smallcaps"],"Page":"4","Group":"1","Designer":"Jos Buivenga","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Fontin","FontFamily":"'Fontin'"},"Fontin Sans":{"Styles":["normal","bold","cyrillic","cyrillic_bold","italic"],"Page":"4","Group":"1","Designer":"Jos Buivenga","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Fontin-Sans","FontFamily":"'Fontin Sans'"},"Forelle":{"Styles":["normal"],"Page":"5","Group":"1","Designer":"Dieter Steffman","URL":"http:\/\/www.steffmann.de","FontFamily":"'Forelle'"},"Gabrielle":{"Styles":["normal"],"Page":"5","Group":"1","Designer":"Dieter Steffmann","URL":"http:\/\/www.steffmann.de","FontFamily":"'Gabrielle'"},"Gesso":{"Styles":["normal"],"Page":"5","Group":"1","Designer":"Ryoichi Tsunekawa","URL":"http:\/\/flat-it.com\/","FontFamily":"'Gesso'"},"Good Dog":{"Styles":["normal"],"Page":"5","Group":"1","Designer":"Fonthead Design","URL":"http:\/\/www.fonthead.com\/","FontFamily":"'Good Dog'"},"Gothic Ultra":{"Styles":["normal"],"Page":"5","Group":"1","Designer":"Jess Latham","URL":"http:\/\/www.bvfonts.com","FontFamily":"'Gothic Ultra'"},"Goudy Book Letter 1911":{"Styles":["normal"],"Page":"5","Group":"1","Designer":"Barry Schwartz","URL":"http:\/\/www.theleagueofmoveabletype.com\/fonts\/8-goudy-bookletter-1911","FontFamily":"'Goudy Book Letter 1911'"},"Goudy Twenty":{"Styles":["normal"],"Page":"5","Group":"1","Designer":"Dieter Steffmann","URL":"http:\/\/www.steffmann.de","FontFamily":"'Goudy Twenty'"},"Green Fuz":{"Styles":["normal"],"Page":"5","Group":"1","Designer":"Ray Larabie","URL":"http:\/\/www.larabiefonts.com\/","FontFamily":"'Green Fuz'"},"Griffin":{"Styles":["normal"],"Page":"5","Group":"1","Designer":"Bright Ideas","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Griffin","FontFamily":"'Griffin'"},"Griffos Font":{"Styles":["normal","smallcaps"],"Page":"6","Group":"1","Designer":"Manfred Klein","URL":"http:\/\/manfred-klein.ina-mar.com\/","FontFamily":"'Griffos Font'"},"Hat Check":{"Styles":["normal"],"Page":"6","Group":"1","Designer":"","URL":"#","FontFamily":"'Hat Check'"},"Hattori Hanzo":{"Styles":["normal","italic"],"Page":"6","Group":"1","Designer":"Roman Shamin","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Hattori-Hanzo","FontFamily":"'Hattori Hanzo'"},"Hill House":{"Styles":["normal"],"Page":"6","Group":"1","Designer":"Jon Hicks","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Hill-House","FontFamily":"'Hill House'"},"Holla Script":{"Styles":["normal"],"Page":"6","Group":"1","Designer":"Dieter Steffmann","URL":"http:\/\/www.steffmann.de","FontFamily":"'Holla Script'"},"Honey Script":{"Styles":["light"],"Page":"6","Group":"1","Designer":"","URL":"#","FontFamily":"'Honey Script-Light'"},"House Slant":{"Styles":["normal"],"Page":"6","Group":"1","Designer":"House Industries","URL":"http:\/\/www.fontsquirrel.com\/fonts\/HouseSampler-HouseSlant","FontFamily":"'House Slant'"},"Idolwild":{"Styles":["normal"],"Page":"6","Group":"1","Designer":"Jakob Fischer","URL":"http:\/\/www.pizzadude.dk","FontFamily":"'Idolwild'"},"Inconsolata":{"Styles":["normal"],"Page":"6","Group":"1","Designer":"Raph Levien","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Inconsolata","FontFamily":"'Inconsolata'"},"Josefin":{"Styles":["normal"],"Page":"7","Group":"1","Designer":"Typemade","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Josefin","FontFamily":"'Josefin'"},"Journal":{"Styles":["normal"],"Page":"7","Group":"1","Designer":"Fontourist","URL":"http:\/\/www.fontourist.com\/","FontFamily":"'Journal'"},"Junction":{"Styles":["normal"],"Page":"7","Group":"1","Designer":"Caroline Hadilaksono","URL":"http:\/\/www.theleagueofmoveabletype.com\/fonts\/1-junction","FontFamily":"'Junction'"},"Laffayette Comic Pro":{"Styles":["normal"],"Page":"7","Group":"1","Designer":"","URL":"#","FontFamily":"'Laffayette Comic Pro'"},"League Gothic":{"Styles":["normal"],"Page":"7","Group":"1","Designer":"The League of Moveable Type","URL":"http:\/\/www.theleagueofmoveabletype.com\/fonts\/7-league-gothic","FontFamily":"'League Gothic'"},"Learning Curve":{"Styles":["normal","dashed"],"Page":"7","Group":"1","Designer":"","URL":"#","FontFamily":"'Learning Curve'"},"Lemon Chicken":{"Styles":["normal"],"Page":"7","Group":"1","Designer":"Crack a Jack Studios","URL":"http:\/\/www.fontsquirrel.com\/fonts\/LemonChicken","FontFamily":"'Lemon Chicken'"},"Little Trouble Girl":{"Styles":["normal"],"Page":"7","Group":"1","Designer":"Jess Latham","URL":"http:\/\/www.bvfonts.com","FontFamily":"'Little Trouble Girl'"},"Lobster":{"Styles":["normal"],"Page":"7","Group":"1","Designer":"Pablo Impallari","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Lobster","FontFamily":"'Lobster'"}},"LocalII":{"Mano Negra":{"Styles":["normal","bold"],"Page":"1","Group":"2","Designer":"Christophe F\u00e9ray","URL":"http:\/\/www.wcfonts.com","FontFamily":"'Mano Negra'"},"Marketing Script":{"Styles":["normal"],"Page":"1","Group":"2","Designer":"Dieter Steffmann","URL":"http:\/\/www.steffmann.de","FontFamily":"'Marketing Script'"},"Molengo":{"Styles":["normal"],"Page":"1","Group":"2","Designer":"Denis Jacquerye","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Molengo","FontFamily":"'Molengo'"},"Museo":{"Styles":["300","500","700","sans_500","sans_500_italic","slab_500","slab_500_italic"],"Page":"1","Group":"2","Designer":"","URL":"#","FontFamily":"'Museo-300'"},"Neo Retro Draw":{"Styles":["normal"],"Page":"1","Group":"2","Designer":"Font Boutique","URL":"http:\/\/www.fontboutique.de","FontFamily":"'Neo Retro Draw'"},"New Athena Unicode":{"Styles":["normal"],"Page":"1","Group":"2","Designer":"American Philological Association","URL":"http:\/\/www.fontsquirrel.com\/fonts\/New-Athena-Unicode","FontFamily":"'New Athena Unicode'"},"New Cicle":{"Styles":["fina","fina_italic","gordita","gordita_italic","semi","semi_italic"],"Page":"1","Group":"2","Designer":"","URL":"#","FontFamily":"'New Cicle-Fina'"},"Operating Instructions":{"Styles":["normal"],"Page":"1","Group":"2","Designer":"Jakob Fischer","URL":"http:\/\/www.pizzadude.dk","FontFamily":"'Operating Instructions'"},"Orbitron":{"Styles":["black","bold","light","medium"],"Page":"1","Group":"2","Designer":"Matt Mclnerney","URL":"http:\/\/www.theleagueofmoveabletype.com\/fonts\/12-orbitron","FontFamily":"'Orbitron-Black'"},"Patagonia":{"Styles":["normal"],"Page":"2","Group":"2","Designer":"Kingdom of Awesome","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Patagonia","FontFamily":"'Patagonia'"},"Pinewood":{"Styles":["normal"],"Page":"2","Group":"2","Designer":"Dieter Steffmann","URL":"http:\/\/www.steffmann.de","FontFamily":"'Pinewood'"},"Porcelain":{"Styles":["normal"],"Page":"2","Group":"2","Designer":"Eduardo Recife","URL":"http:\/\/www.misprintedtype.com","FontFamily":"'Porcelain'"},"Powell Antique":{"Styles":["normal"],"Page":"2","Group":"2","Designer":"Dieter Steffman","URL":"http:\/\/www.steffmann.de","FontFamily":"'Powell Antique'"},"Pricedown":{"Styles":["black"],"Page":"2","Group":"2","Designer":"","URL":"#","FontFamily":"'Pricedown-Black'"},"Print Clearly":{"Styles":["normal","bold","dashed"],"Page":"2","Group":"2","Designer":"Jess Latham","URL":"http:\/\/www.bvfonts.com","FontFamily":"'Print Clearly'"},"Prociono":{"Styles":["normal"],"Page":"2","Group":"2","Designer":"Barry Schwartz","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Prociono","FontFamily":"'Prociono'"},"Pupcat":{"Styles":["normal"],"Page":"2","Group":"2","Designer":"Ray Larabie","URL":"http:\/\/www.larabiefonts.com\/","FontFamily":"'Pupcat'"},"Pusab":{"Styles":["normal"],"Page":"2","Group":"2","Designer":"Ryoichi Tsunekawa","URL":"http:\/\/flat-it.com\/","FontFamily":"'Pusab'"},"Quicksand":{"Styles":["bold","bold_oblique","book","book_oblique","dash","light","light_oblique"],"Page":"3","Group":"2","Designer":"Andrew Paglinawan","URL":"http:\/\/www.andrewpaglinawan.com","FontFamily":"'Quicksand-Bold'"},"Rabiohead":{"Styles":["normal"],"Page":"3","Group":"2","Designer":"","URL":"#","FontFamily":"'Rabiohead'"},"Radioland":{"Styles":["normal","slim"],"Page":"3","Group":"2","Designer":"Jakob Fischer","URL":"http:\/\/www.pizzadude.dk","FontFamily":"'Radioland'"},"Raleway Thin":{"Styles":["normal"],"Page":"3","Group":"2","Designer":"Matt Mclnerney","URL":"http:\/\/www.theleagueofmoveabletype.com\/fonts\/14-raleway","FontFamily":"'Raleway Thin'"},"Riesling":{"Styles":["normal"],"Page":"3","Group":"2","Designer":"Bright Ideas","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Riesling","FontFamily":"'Riesling'"},"Rothenburg":{"Styles":["normal"],"Page":"3","Group":"2","Designer":"Alex Winterbottom","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Rothenburg-Decorative","FontFamily":"'Rothenburg'"},"Rough Trad":{"Styles":["normal"],"Page":"3","Group":"2","Designer":"Christophe F\u00e9ray","URL":"http:\/\/www.wcfonts.com","FontFamily":"'Rough Trad'"},"Sansation":{"Styles":["normal","bold","light"],"Page":"3","Group":"2","Designer":"Bernd Montag","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Sansation","FontFamily":"'Sansation'"},"Santana":{"Styles":["normal","black","bold","condensed","condensed_black"],"Page":"3","Group":"2","Designer":"Manfred Klein","URL":"http:\/\/manfred-klein.ina-mar.com\/","FontFamily":"'Santana'"},"Selfish":{"Styles":["normal"],"Page":"4","Group":"2","Designer":"Eduardo Recife","URL":"http:\/\/www.misprintedtype.com","FontFamily":"'Selfish'"},"Shardee":{"Styles":["normal"],"Page":"4","Group":"2","Designer":"Bright Ideas","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Shardee","FontFamily":"'Shardee'"},"Shortcut":{"Styles":["normal"],"Page":"4","Group":"2","Designer":"Eduardo Recife","URL":"http:\/\/www.misprintedtype.com","FontFamily":"'Shortcut'"},"Silkscreen":{"Styles":["normal","bold"],"Page":"4","Group":"2","Designer":"Jason Kottke","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Silkscreen","FontFamily":"'Silkscreen'"},"Simply Mono":{"Styles":["normal","dirty","oblique"],"Page":"4","Group":"2","Designer":"La Tipomatika","URL":"http:\/\/www.dafont.com\/simply-mono.font","FontFamily":"'Simply Mono'"},"Sniglet":{"Styles":["normal"],"Page":"4","Group":"2","Designer":"League of Moveable Type","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Sniglet","FontFamily":"'Sniglet'"},"Sorts Mill Goudy":{"Styles":["normal","italic"],"Page":"4","Group":"2","Designer":"Barry Schwartz","URL":"http:\/\/www.theleagueofmoveabletype.com\/fonts\/6-sorts-mill-goudy","FontFamily":"'Sorts Mill Goudy'"},"Spin Cycle":{"Styles":["normal","3d"],"Page":"4","Group":"2","Designer":"Jess Latham","URL":"http:\/\/www.bvfonts.com","FontFamily":"'Spin Cycle'"},"Synthetique":{"Styles":["normal"],"Page":"4","Group":"2","Designer":"Jess Latham","URL":"http:\/\/www.bvfonts.com","FontFamily":"'Synthetique'"},"Tallys 15":{"Styles":["normal"],"Page":"5","Group":"2","Designer":"Jos Buivenga","URL":"http:\/\/www.josbuivenga.demon.nl\/tallys.html","FontFamily":"'Tallys 15'"},"Teen":{"Styles":["normal","bold","italic","light"],"Page":"5","Group":"2","Designer":"Ray Larabie","URL":"http:\/\/www.larabiefonts.com\/","FontFamily":"'Teen'"},"Tribeca":{"Styles":["normal"],"Page":"5","Group":"2","Designer":"Dieter Steffmann","URL":"http:\/\/www.steffmann.de","FontFamily":"'Tribeca'"},"Typo Slabserif":{"Styles":["light"],"Page":"5","Group":"2","Designer":"Manfred Klein","URL":"http:\/\/manfred-klein.ina-mar.com\/","FontFamily":"'Typo Slabserif-Light'"},"Ugly Qua":{"Styles":["normal","italic"],"Page":"5","Group":"2","Designer":"Manfred Klein","URL":"http:\/\/manfred-klein.ina-mar.com\/","FontFamily":"'Ugly Qua'"},"Unibody 8":{"Styles":["normal","black","bold","italic","smallcaps"],"Page":"5","Group":"2","Designer":"Underware","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Unibody-8","FontFamily":"'Unibody 8'"},"Vitamin":{"Styles":["normal"],"Page":"5","Group":"2","Designer":"Jakob Fischer","URL":"http:\/\/www.pizzadude.dk","FontFamily":"'Vitamin'"},"Vollkorn":{"Styles":["normal","bold"],"Page":"5","Group":"2","Designer":"Friedrich Althausen","URL":"http:\/\/www.grafikfritze.de\/","FontFamily":"'Vollkorn'"},"Windsong":{"Styles":["normal"],"Page":"5","Group":"2","Designer":"Bright Ideas","URL":"http:\/\/www.fontsquirrel.com\/fonts\/Windsong","FontFamily":"'Windsong'"},"Winterthur Condensed":{"Styles":["normal","oblique"],"Page":"6","Group":"2","Designer":"Manfred Klein","URL":"http:\/\/manfred-klein.ina-mar.com\/","FontFamily":"'Winterthur Condensed'"}}};
__BTN_TEXT = "Submit";

</script>
<script src="template/scripts/global/dynamic-1-7.740.js"></script>
<script src="template/scripts/themes/dynamic.740.js"></script>
<?php
} elseif($routers['controller'] == 'importer') {
?>
<script>
var __FORM = {"FormId":"0","Name":"Untitled Form","Description":"This is my form. Please fill it out. It's awesome!","OffsetHeight":"0","LabelAlign":"topLabel","DateCreated":"0","DateUpdated":"0","Fields":[]};
var __USER = {"Profile":{"ProfileId": "111", "Username": "abcxyz", "PlanId": "1", "AccountCreated": "1997-01-22", "User":{"UserId": "111", "User": "abcxyz"},"Plan":{"PlanId": "1", "Upload": "0", "Price": "0.00", "Secure": "0", "Storage": "0", "FieldCount": "50", "EntriesCount": "", "FormsCount": "", "ReportsCount": "", "UsersCount": "1", "Paypal": "0", "PlanName": "HoctudauBasic"}}};
</script>
<script src="template/scripts/global/dynamic-1-7.724.js"></script>
<script src="template/scripts/public/importer.js"></script>
<?php
} elseif($routers['controller'] == 'cwa') {
?>

<script>
__LOGS = {<?php
    $i = 0;
    $count = count($logs3);
    foreach($logs3 as $kl => $logs) {
        foreach($logs as $key => $log) {
            $log_comments = array();
            if(isset($log['comments']) && !empty($log['comments'])) foreach($log['comments'] as $lcs) {
                $user = $this->helpers->json_decode_to_array_not_in_mysql($lcs['user']['user_content']);
                $log_comments[] = array(
                    'comment_id' => $lcs['comment_id'],
                    'comment_text' => $lcs['comment_content'],
                    'comment_user' => $user['NickH2d']
                );
            }
            ?>"<?php echo $log['log_id']; ?>":{"log_id":"<?php echo $log['log_id']; ?>","log_value":<?php echo json_encode($log['log_subject']); ?>,"log_comments":<?php echo json_encode($log_comments); ?>}<?php if($i<$count) echo ','; 
        }
        $i++;
    }
?>};
</script>
<script src="template/scripts/global/dynamic-1-7.724.js"></script>
<script src="template/scripts/public/reviewer.js"></script>
<?php
} elseif($routers['controller'] == 'subjects' && ($routers['action'] == 'manager')) {
?>
<script src="template/scripts/global/dynamic-1-7.740.js"></script>
<script src="template/scripts/subjects/manager/admin.js"></script>
<?php
} elseif($routers['controller'] == 'subjects' && ($routers['action'] == 'builder')) {
?>
<script>
var SUBJ_NEW = <?php echo empty($subject_uuid)?'1':'0'; ?>;
var __SUBJECT = <?php 
    if(!empty($subject_settings)) {
        echo json_encode($subject_settings);
    } else {
    ?>{"ExpireDate":"0000-00-00","Price":"Free","LimitUser":"0","ThumbnailUrl":"http:\/\/hoctudau.com\/wp-content\/themes\/bueno\/images\/logo.png","FormOrder":[]}<?php 
    }
?>;
var __FORMS = {<?php
$count = count($forms_result);
if($count > 0) {
    $i = 0;
    foreach($forms_result as $form) {
    if(empty($form['form_content'])) continue;
    $_form = $this->helpers->json_decode_to_array($form['form_content']);
    ?>"<?php echo $form['form_id']; ?>":{"FormId":"<?php echo $form['form_id']; ?>","Url":"<?php echo $form['form_uuid']; ?>","Name":<?php echo json_encode($_form['Name']); ?>}<?php if($i<$count-1) echo ','; $i++;
    }
}
?>};
var __SFORMS = {<?php
if(!empty($sf_results)) {
$count = count($sf_results);
if($count > 0) {
    $i = 0;
    foreach($sf_results as $form) {
    if(empty($form['form_content'])) continue;
    $_form = $this->helpers->json_decode_to_array($form['form_content']);
    ?>"<?php echo $form['form_id']; ?>":{"FormId":"<?php echo $form['form_id']; ?>","Url":"<?php echo $form['form_uuid']; ?>","Name":<?php echo json_encode($_form['Name']); ?>}<?php if($i<$count-1) echo ','; $i++;
    }
}
}
?>};
</script>
<script src="template/scripts/global/dynamic-1-7.740.js"></script>
<script src="template/scripts/ckeditor/ckeditor.js"></script>
<script>
CKEDITOR.on( 'instanceCreated', function( event ) {
	var editor = event.editor,
		element = editor.element;

	// Customize editors for headers and tag list.
	// These editors don't need features like smileys, templates, iframes etc.
	if ( element.is( 'h1', 'h2', 'h3' ) || element.getAttribute( 'id' ) == 'taglist' ) {
		// Customize the editor configurations on "configLoaded" event,
		// which is fired after the configuration file loading and
		// execution. This makes it possible to change the
		// configurations before the editor initialization takes place.
		editor.on( 'configLoaded', function() {

			// Remove unnecessary plugins to make the editor simpler.
			editor.config.removePlugins = 'colorbutton,find,flash,font,' +
				'forms,iframe,image,newpage,removeformat,' +
				'smiley,specialchar,stylescombo,templates';

			// Rearrange the layout of the toolbar.
			editor.config.toolbarGroups = [
				{ name: 'editing',		groups: [ 'basicstyles', 'links' ] },
				{ name: 'undo' },
				{ name: 'clipboard',	groups: [ 'selection', 'clipboard' ] },
				{ name: 'about' }
			];
		});
	}
});
</script>
<script src="template/scripts/subjects/manager/calendar.js"></script>
<script src="template/scripts/subjects/manager/custom.js"></script>

<?php
} elseif($routers['controller'] == 'subjects' && ($routers['action'] == 'view')) {
?>
<script src="template/scripts/global/dynamic-1-7.740.js"></script>
<script src="template/scripts/subjects/public/dynamic.js"></script>

<?php
}
?>
</body>
</html>