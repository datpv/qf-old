<?php include('getover.php'); ?>
<div id="nav">
	<h1 class="logo">
		<a href="<?php echo $this->helpers->getUrl(); ?>index/">Hoctudau</a>
	</h1>
	<ul id="menu" class="clearfix">
        <li class="<?php echo ($routers['controller']=='dashboard')?'act':'act2'; ?>">
			<a href="<?php echo $this->helpers->getUrl(); ?>dashboard/" title="dashboard Settings">Dashboard</a>
		</li>
        <li class="<?php echo ($routers['controller']=='importer')?'imp':''; ?>">
			<a href="<?php echo $this->helpers->getUrl(); ?>importer/" title="Student Contribute Forms">Importer</a>
		</li>
		<li class="lgo">
            <?php
            if($this->helpers->isAdmin() || $this->helpers->isUser()) {
            ?>
            <a href="lightboxes/logout.php" class="lbOn" title="Logout!">Logout</a>
            <?php   
            } else {
            ?>
            <a href="<?php echo $this->helpers->getUrl(); ?>auth/login" class="" title="Login!">Login</a>
            <?php  
            }
            ?>
		</li>
	</ul>
    <?php if(isset($_SESSION['admin_user'])) : ?>
    <ul id="admin_menu" class="clearfix">
        <li class="frm">
			<a href="<?php echo $this->helpers->getUrl(); ?>admin/" title="Form Manager">Forms</a>
		</li>
		<li class="rpt">
			<a href="<?php echo $this->helpers->getUrl(); ?>reports/" title="Report Manager">Reports</a>
		</li>
		<li class="thm">
			<a href="<?php echo $this->helpers->getUrl(); ?>themes/" title="Theme Designer">Themes</a>
		</li>
        <li class="subj">
			<a href="<?php echo $this->helpers->getUrl(); ?>subjects/manager" title="Subjects Mananger">Subjects</a>
		</li>
        <li class="<?php echo ($routers['controller']=='groups')?'ad_m':''; ?>">
			<a href="<?php echo $this->helpers->getUrl(); ?>groups/" title="Groups Mananger">Groups</a>
		</li>
        <li class="<?php echo ($routers['controller']=='caches')?'ad_m':''; ?>">
			<a href="<?php echo $this->helpers->getUrl(); ?>caches/" title="Caches Mananger">Caches</a>
		</li>
        <li class="<?php echo ($routers['controller']=='database')?'ad_m':''; ?>">
			<a href="<?php echo $this->helpers->getUrl(); ?>database/" title="Database Mananger">Database</a>
		</li>
        <li class="<?php echo ($routers['controller']=='accounts')?'ad_m':''; ?>">
			<a href="<?php echo $this->helpers->getUrl(); ?>accounts/" title="Accounts Mananger">Accounts</a>
		</li>
    </ul>
	<?php endif; ?>
</div>
<!--nav-->