var Interface = Class.create({
    initialize: function () {
        getPlatform();
    },
    hideStatus: function () {
        hideStatus();
    },
    toggleFormPublicStatus: function (formId, publicStatus) {
        if (publicStatus == 1) {
            this.showButtons(formId);
        } else {
            this.hideButtons(formId);
        }
    },
    showButtons: function (formId) {
        $('li' + formId).removeClassName('notActive');
    },
    hideButtons: function (formId) {
        $('li' + formId).addClassName('notActive');
    },
    duplicateAnimation: function () {
        var formList = Array();
        $$('.group li').each(function (num, index) {
            formList[index] = num.id.replace('li', '') * 1;
        });
        maxFormID = formList.max();
        el = $('li' + maxFormID).down();
        var initialColor = el.getStyle('backgroundColor');
        var hexInitialColor = new RGBColor(initialColor);
        el.style.backgroundColor = '#99D247';
        new Effect.Highlight(el, {
            startcolor: '#99D247',
            endcolor: hexInitialColor.toHex(),
            restorecolor: initialColor,
            duration: 2,
            queue: 'end'
        });
    },
    showSearching: function (searchTerm) {
        $('searchTerm').innerHTML = searchTerm;
        $('searching').removeClassName('hide');
    },
    hideSearching: function () {
        if (!$('searching').hasClassName('hide')) $('searching').addClassName('hide');
    },
    showNoResults: function () {
        this.hideSearching();
        $('noResults').removeClassName('hide');
    },
    hideNoResults: function () {
        if (!$('noResults').hasClassName('hide')) $('noResults').addClassName('hide');
    },
    toggleSelectedSort: function (sort) {
        $('sort').childElements().invoke('removeClassName', 'selected');
        $(sort).addClassName('selected');
    }
});
var FormEngineObject = Class.create({
    initialize: function () {}, //<sl:translate>
    deleteFormStatus: 'Deleting form ...',
    duplicatingFormStatus: 'Duplicating form ...',
    savingPublicStatusStatus: 'Toggling the public setting of your form ...',
    saveThemeStatus: 'Changing the theme for this form ...',
    sortFormsStatus: 'Resorting your forms ...',
    displayGraphStatus: 'Processing data and drawing graph ...', //</sl:translate>
    deleteForm: function (formId, formName) {
        $('statusText').innerHTML = this.deleteFormStatus;
        var myAjax = new Ajax.Request('index.php?req=admin/', {
            method: 'post',
            parameters: 'action=delete&formId=' + formId + '&formName=' + encodeURIComponent(formName),
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishDeletingForm(ret);
            }.bind(this)
        });
    },
    duplicateForm: function (formId, formName) {
        $('statusText').innerHTML = this.duplicatingFormStatus;
        var myAjax = new Ajax.Request('index.php?req=admin/', {
            method: 'post',
            parameters: 'action=duplicate&formId=' + formId + '&formName=' + encodeURIComponent(formName),
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishDuplicatingForm(ret);
            }.bind(this)
        });
    },
    savePublicStatus: function (formId, formName, publicStatus) {
        $('statusText').innerHTML = this.savingPublicStatusStatus;
        var myAjax = new Ajax.Request('index.php?req=admin/', {
            method: 'post',
            parameters: 'action=savePublicStatus&formId=' + formId + '&formName=' + encodeURIComponent(formName) + '&publicStatus=' + publicStatus,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishSavingPublicStatus(ret);
            }.bind(this)
        });
    },
    savePassword: function (formId, formName, password) {
        var myAjax = new Ajax.Request('index.php?req=admin/', {
            method: 'post',
            parameters: 'action=protect&formId=' + formId + '&formName=' + encodeURIComponent(formName) + '&password=' + encodeURIComponent(password),
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishProtectForm(ret);
            }.bind(this)
        });
    },
    saveTheme: function (formId, formName, styleId, formUrl) {
        $('statusText').innerHTML = this.saveThemeStatus;
        var myAjax = new Ajax.Request('index.php?req=themes/attach/', {
            method: 'post',
            parameters: 'formId=' + formId + '&formName=' + formName + '&styleId=' + styleId + '&formUrl=' + formUrl,
            onComplete: function (response) {}
        });
    },
    sortForms: function (sortBy) {
        $('statusText').innerHTML = this.sortFormsStatus;
        var myAjax = new Ajax.Request('index.php?req=admin/', {
            method: 'post',
            parameters: 'action=order&orderBy=' + sortBy + '&orderDirection=DESC',
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishFilteringForm(ret);
            }.bind(this)
        });
    },
    formMatchesFilter: function (form, filter) {
        var username = form.Name || '';
        var fullname = form.Fullname || '';
        var nickskype = form.Nickskype || '';
        var nickh2d = form.Nickh2d || '';
        var email = form.Email || '';
        var isactive = form.IsActivated || '';
        if (username.toLowerCase().include(filter.toLowerCase())) return true;
        else if (fullname.toLowerCase().include(filter.toLowerCase())) return true;
        else if (nickskype.toLowerCase().include(filter.toLowerCase())) return true;
        else if (nickh2d.toLowerCase().include(filter.toLowerCase())) return true;
        else if (email.toLowerCase().include(filter.toLowerCase())) return true;
        else if (isactive.toLowerCase().include(filter.toLowerCase())) return true;
        else return false;
    }
});
var FormsObject = Class.create({
    initialize: function (Forms) {
        this.data = Forms;
    },
    getForm: function (formId) {
        if (this.data[formId]) return this.data[formId];
        else return false;
    }
});
var FormManager = Class.create({
    FormEngine: new FormEngineObject(),
    UI: '',
    Forms: '',
    sortSetting: 'DateCreated',
    filterSetting: '',
    g_type: 'week',
    initialize: function () {
        if ($('expandThis')) {
            this.actionMarkup = $('expandThis').innerHTML;
        }
        this.UI = new Interface();
        this.Forms = new FormsObject(__USERS);
        this.UI.hideStatus();
        this.setSortDefaults();
        if (document.URL.indexOf('highlight') != -1) {
            this.UI.duplicateAnimation();
        }
        if ($('searchBox')) $('searchBox').focus();
        if (this.readCookie('stats') && this.readCookie('stats') != 'Off') this.loadDefaultStats();
    },
    attachEvents: function () {
        $$('#groupList-1 li').each(function (num, index) {
            num.observe('mouseover', showActions.bind(num));
            num.observe('mouseout', hideActions.bind(num));
        });
    },
    setSortDefaults: function () {
        var filter = this.readCookie('formManagerFilter');
        if (filter && filter != '') {
            this.filterSetting = filter
            //this.runFilter();
        }
    },
    showActions: function (ctrl) {
        var ctrl = $(ctrl);
        if (ctrl.down) {
            this.activeForm = ctrl.id;
            if (!ctrl.down('.actions')) {
                ctrl.insert({
                    bottom: this.actionMarkup
                });
                var linkToEntries = $(ctrl.id.replace('li', 'link')).href;
                ctrl.down('.view').href = linkToEntries;
            }
            ctrl.addClassName('hover');
        }
    },
    hideActions: function (ctrl) {
        var ctrl = $(ctrl);
        if (ctrl.down) {
            ctrl.removeClassName('hover');
        }
    },
    editForm: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=build/' + form.Url + '';
    },
    viewEntries: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=entries/' + form.Url + '';
    },
    viewCode: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=code/' + form.Url + '';
    },
    viewPayment: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=merchant/' + form.Url + '';
    },
    viewRules: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=rules/' + form.Url + '';
    },
    viewAnalytics: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=analytics/' + form.Url + '';
    },
    viewNotifications: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=notifications/' + form.Url + '';
    },
    deleteForm: function (formId) {
        var form = this.Forms.getForm(formId);
        Lightbox.showUrl('lightboxes/Form.Delete.php', function () {
            $('deleteFormId').value = form.FormId;
            $('deleteFormName').innerHTML = form.Name;
        }, 'error');
        return false;
    },
    continueDeletingForm: function () {
        var form = this.Forms.getForm($F('deleteFormId'));
        this.FormEngine.deleteForm(form.FormId, form.Name);
    },
    successfulFormDeletion: function () {
        window.location.reload(true);
        return;
    },
    failedFormDeletion: function (ret) {
        Lightbox.showContent(ret.response.lbmarkup);
    },
    duplicateForm: function (formId) {
        var form = this.Forms.getForm(formId);
        this.FormEngine.duplicateForm(form.FormId, form.Name);
    },
    successfulFormDuplication: function (formId) {
        window.location.reload(true);
        return;
    },
    failedFormDuplication: function (ret) {
        Lightbox.showContent(ret.response.lbmarkup);
    },
    savePublicStatus: function (formId, formName, ctrl) {
        publicStatus = (ctrl.checked) ? 1 : 0;
        this.UI.toggleFormPublicStatus(formId, publicStatus);
        this.FormEngine.savePublicStatus(formId, formName, publicStatus);
    },
    failedSavePublicStatus: function (ret) {
        var ctrl = $('publicStatus_' + ret.response.formId);
        ctrl.checked = !ctrl.checked;
        var publicStatus = (ctrl.checked) ? 1 : 0;
        this.UI.toggleFormPublicStatus(ret.response.formId, publicStatus);
        Lightbox.showContent(ret.response.lbmarkup);
    },
    showProtectForm: function (formId) {
        var form = this.Forms.getForm(formId);
        Lightbox.showUrl('lightboxes/Form.Protect.php?formId=' + form.FormId, function () {
            $('protect_formId').value = form.FormId;
        });
        return false;
    },
    protectForm: function () {
        var form = this.Forms.getForm($('protect_formId').value);
        this.FormEngine.savePassword(form.FormId, form.Name, $('protect_password').value);
    },
    successfulProtectForm: function (ret) {
        Lightbox.close();
    },
    failedProtectForm: function (ret) {
        $('protect_error').innerHTML = ret.response.errorMessage;
        Element.addClassName('protect_error', 'error');
        Element.addClassName('protect_parent', 'error');
    },
    changeTheme: function (formId, formName, ctrl) {
        var form = this.Forms.getForm(formId);
        if (ctrl.options[ctrl.selectedIndex].value == 'create') {
            document.location = 'index.php?req=themes/';
            return false;
        }
        this.FormEngine.saveTheme(formId, formName, ctrl.options[ctrl.selectedIndex].value, form.Url);
    },
    sortBy: function (sort) {
        if (sort != this.sortSetting) {
            this.sortSetting = sort;
            this.UI.toggleSelectedSort(this.sortSetting);
            this.FormEngine.sortForms(this.sortSetting);
            this.createCookie('formManagerOrder', sort);
        }
    },
    filterBy: function (filter) {
        if (filter != this.filterSetting) {
            this.filterSetting = filter;
            this.runFilter();
            //this.createCookie('formManagerFilter', filter);
        }
    },
    runFilter: function () {
        this.UI.showSearching(this.filterSetting);
        var zebra = 'alt';
        var matches = false;
        var numberOfMatches = 0;
        $('groupList-1').childElements().each(function (num, index) {
            var form = this.Forms.data[num.id.replace('user_', '')];
            
            if (this.FormEngine.formMatchesFilter(form, this.filterSetting)) {
                matches = true;
                numberOfMatches++;
                $('user_' + form.FormId).removeClassName('hide');
                if (num != 0) {
                    if (numberOfMatches == 1) {
                        this.showActions($('user_' + form.FormId));
                        $('user_' + form.FormId).removeClassName('hover');
                        if (!$('user_' + form.FormId).hasClassName('first')) $('user_' + form.FormId).addClassName('first');
                    } else {
                        this.hideActions($('user_' + form.FormId));
                        $('user_' + form.FormId).removeClassName('first');
                    }
                }
                if (zebra == 'alt') {
                    $('user_' + form.FormId).addClassName('alt');
                    zebra = '';
                } else {
                    $('user_' + form.FormId).removeClassName('alt');
                    zebra = 'alt';
                }
            } else $('user_' + form.FormId).addClassName('hide');
        }.bind(this));
        if (!this.filterSetting) {
            this.UI.hideSearching();
        }
        if (!matches) this.UI.showNoResults();
        else this.UI.hideNoResults();
    },
    failedFilterForm: function (ret) {
        Lightbox.showContent(ret.response.lbmarkup);
    },
    successfulFilterForm: function (ret) {
        $('groupList-1').innerHTML = ret.response.formList;
        this.runFilter();
        this.attachEvents();
    },
    loadDefaultStats: function () {
        vals = this.readCookie('stats').split('|');
        found = false;
        for (var i = 0; i < $('allForms').options.length; i++) {
            if ($('allForms').options[i].value == vals[1]) {
                $('allForms').selectedIndex = i;
                found = true;
                break;
            }
        }
        if (!found) $('allForms').selectedIndex = 0;
        Effect.BlindDown('stats', {
            duration: .4,
            afterFinish: function () {
                this.changeType(vals[2], true);
            }.bind(this)
        });
    },
    changeType: function (newType, first) {
        if (newType && newType != this.g_type || first == true) {
            this.g_type = newType;
            Element.removeClassName('todayGraph', 'selected');
            Element.removeClassName('weekGraph', 'selected');
            Element.removeClassName('monthGraph', 'selected');
            Element.removeClassName('yearGraph', 'selected');
            Element.addClassName(newType + 'Graph', 'selected');
            this.refreshGraph();
        }
    },
    refreshGraph: function () {
        cookieVal = 'on|' + $('allForms').options[$('allForms').selectedIndex].value + '|' + this.g_type;
        this.createCookie('stats', cookieVal);
        this.FormEngine.displayGraph($('allForms').options[$('allForms').selectedIndex].value, this.g_type);
    },
    toggleGraph: function () {
        if (!$('stats').offsetHeight || $('stats').offsetHeight == 0) this.showGraph();
        else this.hideGraph();
    },
    showGraph: function () {
        Effect.BlindDown('stats', {
            duration: .4,
            afterFinish: function () {
                this.refreshGraph();
            }.bind(this)
        });
    },
    hideGraph: function () {
        this.createCookie('stats', 'Off');
        $('formGraph').innerHTML = '';
        Effect.BlindUp('stats', {
            duration: .4
        });
    },
    createCookie: function (name, value) {
        createCookie(name, value);
    },
    readCookie: function (name) {
        return readCookie(name);
    }
});
var GroupEngineObject = Class.create({
    addGroup: function() {
        var url = new String(document.location);;
        var group_name = $('group_name').value;
        if(group_name.length == 0) return;
        var pars = 'action=addgroup&group_name='+group_name;
        var myAjax = new Ajax.Request(url, {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == true) {
                    location.reload(true);
                }
            }
        });
        return;
    },
    updateGroup: function($group) {
        var url = new String(document.location);;
        var group_name = $group.up().previous().previous().select('input')[0].value;
        var group_id = $group.up().up().id.replace('group_','');
        if(group_name.length == 0) return;
        var pars = 'action=updategroup&group_name='+group_name+'&group_id='+group_id;
        var myAjax = new Ajax.Request(url, {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == true) {
                    $group.up().up().style.backgroundColor = '#FFE14F';
                    setTimeout(function() {
                        $group.up().up().style.backgroundColor = '';
                    },1500);
                } else {
                    $group.up().up().style.backgroundColor = '#FF8989';
                    setTimeout(function() {
                        $group.up().up().style.backgroundColor = '';
                    },1500);
                }
            }
        });
        return;
    },
    changeGroup: function() {
        var url = new String(document.location);;
        var group_name = $('group_name').value;
        if(group_name.length == 0) return;
        var pars = 'action=changegroup&group_name='+group_name;
        var myAjax = new Ajax.Request(url, {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == true) {
                    location.reload(true);
                }
            }
        });
        return;
    },
});
var CourseEngineObject = Class.create({
    addCourse: function() {
        var url = new String(document.location);;
        var course_name = $('course_name').value;
        var course_description = $('course_description').value;
        if(course_name.length == 0) return;
        var pars = 'action=addcourse&course_name='+course_name+'&course_description='+course_description;
        var myAjax = new Ajax.Request(url, {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == true) {
                    location.reload(true);
                }
            }
        });
        return;
    },
    updateCourse: function(course) {
        var url = new String(document.location);;
        var course_name = course.up().previous('.name').down('.course_name').value;
        var course_description = course.up().previous('.name').down('.course_desciption').value;
        var course_id = course.up().up().id.replace('course_','');
        if(course_name.length == 0) return;
        var pars = 'action=updatecourse&course_name='+course_name+'&course_id='+course_id+'&course_description='+course_description;
        var myAjax = new Ajax.Request(url, {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == true) {
                    course.up().up().style.backgroundColor = '#FFE14F';
                    setTimeout(function() {
                        course.up().up().style.backgroundColor = '';
                    },1500);
                } else {
                    course.up().up().style.backgroundColor = '#FF8989';
                    setTimeout(function() {
                        $group.up().up().style.backgroundColor = '';
                    },1500);
                }
            }
        });
        return;
    },
    addGroupToCourse: function(group) {
        var group_id = group.up().down('.groups').value;
        var group_name = group.up().down('.groups').select('option[value="' + group_id + '"]')[0].innerHTML
        var course_id = group.up().up().up().up().id.replace('course_','');
        var pars='action=addgrouptocourse&group_id='+encodeURIComponent(group_id)+'&course_id='+course_id;
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == true) {
                    var newobj = document.createElement('li');
                    newobj.innerHTML = '<strong>'+group_name+'</strong>';
                    console.log(newobj);
                    group.up().previous('ul').appendChild(newobj);
                } else {
                    alert(result.response.message);
                }
            }
        });
    },
    deleteGroupCourse: function(group, group_id) {
        var course_id = group.up().up().up().up().up().id.replace('course_','');
        var pars='action=deletegroupcourse&group_id='+encodeURIComponent(group_id)+'&course_id='+course_id;
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == true) {
                    group.up().remove();
                } else {
                    alert(result.response.message);
                }
            }
        });
    }
});
var __GROUP,__COURSE;
Event.observe(window, 'load', init, false);
function init() {
    __GROUP = new GroupEngineObject();
    __COURSE = new CourseEngineObject();
}
function addGroup() {
    __GROUP.addGroup();
}
function changeGroup($group) {
    __GROUP.changeGroup($group);;
}
function updateGroup($group) {
    __GROUP.updateGroup($group);
}
function deleteGroup($group) {
    __GROUP.deleteGroup($group);
}
function addCourse() {
    __COURSE.addCourse();
}
function updateCourse(course) {
    __COURSE.updateCourse(course);
}
function addGroupToCourse(group) {
    __COURSE.addGroupToCourse(group);
}
function deleteGroupCourse(group, group_id) {
    __COURSE.deleteGroupCourse(group, group_id);
}