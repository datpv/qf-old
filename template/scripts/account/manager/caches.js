var AccountEngineObject = Class.create({
    rebuildCache: function($user_id) {
        var url = new String(document.location);
        var pars='action=setCaches&user_id='+encodeURIComponent($user_id);
        var myAjax = new Ajax.Request(url, {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                var color = '#FFE14F';
                if(result.success == true) {
                    $('user_'+$user_id).select('.nb p')[0].innerHTML = 'update: '+result.response.nb.up+' - insert: '+result.response.nb.ins;
                    $('user_'+$user_id).select('.nb')[0].style.backgroundColor = (result.response.nb.done)?color:'#FF8989';
                    setTimeout(function() {
                        $('user_'+$user_id).select('.nb')[0].style.backgroundColor = '';
                    },1500);
                    
                    $('user_'+$user_id).select('.as p')[0].innerHTML = 'update: '+result.response.as.up+' - insert: '+result.response.as.ins;
                    $('user_'+$user_id).select('.as')[0].style.backgroundColor = (result.response.as.done)?color:'#FF8989';;
                    setTimeout(function() {
                        $('user_'+$user_id).select('.as')[0].style.backgroundColor = '';
                    },1500);
                    
                    
                    $('user_'+$user_id).select('.sced p')[0].innerHTML = 'update: '+result.response.sced.up+' - insert: '+result.response.sced.ins;
                    $('user_'+$user_id).select('.sced')[0].style.backgroundColor = (result.response.sced.done)?color:'#FF8989';;
                    setTimeout(function() {
                        $('user_'+$user_id).select('.sced')[0].style.backgroundColor = '';
                    },1500);
                    
                    $('user_'+$user_id).select('.nbed p')[0].innerHTML = 'update: '+result.response.nbed.up+' - insert: '+result.response.nbed.ins;
                    $('user_'+$user_id).select('.nbed')[0].style.backgroundColor = (result.response.nbed.done)?color:'#FF8989';;
                    setTimeout(function() {
                        $('user_'+$user_id).select('.nbed')[0].style.backgroundColor = '';
                    },1500);
                } else {
                    $('user_'+$user_id).style.backgroundColor = '#FF8989';
                    setTimeout(function() {
                        $('user_'+$user_id).style.backgroundColor = '';
                    },1500);
                }
            }
        });
        return;
    },
    rebuildAllCaches: function() {
        var $users = $$('a.rebuild');
        $users.each(function($user) {
            var $user_id = $user.rel;
            this.rebuildCache($user_id);
        });
        return;
    },
    deleteNullCached: function() {
        var url = new String(document.location);
        var pars='action=deleteNullCached';
        var color = '#FFE14F';
        var myAjax = new Ajax.Request(url, {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success) {
                    $('deleteNullCached').up().style.backgroundColor = color;
                        setTimeout(function() {
                           $('deleteNullCached').up().style.backgroundColor = '';
                        },1500);
                } else {
                    $('deleteNullCached').up().style.backgroundColor = '#FF8989';
                    setTimeout(function() {
                        $('deleteNullCached').up().style.backgroundColor = '';
                    },1500);
                }
            }
        });
    },
    emptyCached: function() {
        var url = new String(document.location);
        var pars='action=emptyCached';
        var color = '#FFE14F';
        var myAjax = new Ajax.Request(url, {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success) {
                    $('emptyCached').up().style.backgroundColor = color;
                        setTimeout(function() {
                           $('emptyCached').up().style.backgroundColor = '';
                        },1500);
                } else {
                    $('emptyCached').up().style.backgroundColor = '#FF8989';
                    setTimeout(function() {
                        $('emptyCached').up().style.backgroundColor = '';
                    },1500);
                }
            }
        });
    }
});
var EntriesEngineObject = Class.create({
});
var __MANAGER,__ENTRY;
Event.observe(window, 'load', init, false);
function init() {
    __MANAGER = new AccountEngineObject();
    __ENTRY = new EntriesEngineObject();
    var $users = $$('a.rebuild');
    $users.each(function($user) {
        $user.observe('click', function(event) {
            var $user_id = this.rel;
            rebuildCache($user_id);
            event.stop() 
        });
    });
}
function rebuildCache($user_id) {
    __MANAGER.rebuildCache($user_id);
}
function rebuildAllCaches() {
    __MANAGER.rebuildAllCaches();
}
function deleteNullCached() {
    __MANAGER.deleteNullCached();
}
function emptyCached() {
    __MANAGER.emptyCached();
}