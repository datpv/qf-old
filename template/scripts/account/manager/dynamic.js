var AccountEngineObject = Class.create({
    updateAccount: function($account) {
        var url = new String(document.location);
        var pars =  $('user_info').serialize(true);
        pars.action = 'update';
        var myAjax = new Ajax.Request(url, {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == 'false') {
                    alert(result.response.message);
                }
            }
        });
        return;
    },
    changePassword: function() {
        var password=$F('password');
        var confirm=$F('confirm');
        var userId=$F('userId');
        var url = new String(document.location);
        var pars='action=changepass&password='+encodeURIComponent(password)+'&confirm='+encodeURIComponent(confirm);
        var myAjax = new Ajax.Request(url, {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == 'false') {
                    alert(result.response.message);
                } else {
                    Lightbox.close();
                }
            }
        });
        return;
    },
    addTabing: function(tabs) {
        $$('a[rel=nbed_container]')[0].addClassName('active');
        $('nbed_container').removeClassName('hide');
        tabs.each(function(tab) {
            tab.observe('click', function(event) {
                $$('.fullTable').invoke('addClassName','hide');
                tabs.invoke('removeClassName','active')
                tab.addClassName('active');
                var rel = tab.rel;
                $(rel).removeClassName('hide');
                event.stop();
            });
          
        });
    },
    showChangeGroupLightbox: function(ctrl) {
        
    },
    showJoinGroupLightbox: function(ctrl) {
        
    }
});
var EntriesEngineObject = Class.create({
    loadSection: function($section,sortby,direction) {
        var pars='action=loadSection&section='+encodeURIComponent($section.id)+'&sortby='+sortby+'&direction='+direction;
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success) {
                    if(result.response.paged != '') {
                        $section.up().up().next().down().innerHTML = result.response.paged;
                    }
                    if(result.response.html != '') {
                        $section.up().next().down('tbody').innerHTML = result.response.html;
                    }
                    this.addHovers($section.up().next().select('tbody .excerpts a'));
                }
            }
        });
        return;
    },
    goToPage: function($paged,$page) {
        var $upPage = $page.up().up();
        var action = $upPage.up().id;
        var sortby = 'dc';
        var direction = 'asc';
        var custom_sort = $upPage.up().previous().select('.sorts .button.active');
        var custom_dir = $upPage.up().previous().select('.sorts select[name="direction"]');
        if(custom_sort.length > 0) {
            sortby = custom_sort['0'].rel;
        }
        if(custom_dir.length > 0) {
            direction = custom_dir['0'].value;
        }
        var pars='action='+action+'&paged='+encodeURIComponent($paged)+'&sortby='+sortby+'&direction='+direction;
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success) {
                    $upPage.innerHTML = result.response.paged;
                    $upPage.up().previous().down('tbody').innerHTML = result.response.html;
                    var summaries = $upPage.up().previous().select('tbody a.summary');
                    var count = summaries.length;
                    if($$('.nocache').length > 0) this.loadSummary(summaries,count);
                    this.addHovers($upPage.up().previous().select('tbody .excerpts a'));
                }
            }
        });
        return;
    },
    loadSummary: function(summaries,count) {
        var action = 'summary';
        var entry = summaries.shift();
        var entryId = entry.rel;
        var pars='action='+action+'&entryId='+encodeURIComponent(entryId);
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                count = summaries.length;
                if(result.success) {
                    entry.innerHTML = result.response.html;
                    if(count > 0) {
                        this.loadSummary(summaries,count);
                    }
                    
                }
            }
        });
    },
    loadStatistic: function() {
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: 'action=statistic',
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == 'true') {
                    $('statistic').innerHTML = result.response.html;
                }
            }
        });
    },
    addHovers: function(links) {
        links.each(function(link) {
            link.observe('mouseover', function(event) {
                var src = link.href;
                var preview = this.up().select('.preview')['0'];
                var newobj=document.createElement('img');
                newobj.src = src;
                newobj.style.width = '100px';
                preview.appendChild(newobj);
            }).observe('mouseout', function(event) {
                this.up().select('.preview')['0'].innerHTML = '';
            });
          
        });
    },
    addActions: function() {
        var sorts = $$('.sorts a.button');
        sorts.each(function(sort) {
            sort.observe('click', function(event) {
                Cookie.set(this.up().id+'_sort',this.rel);
                $(this.up().id).select('a.button').invoke('removeClassName','active');
                this.addClassName('active');
                loadSection($(this.up().id),this.rel,$(this.up().id).down('.direction').value);
                event.stop();
            });
        });
        var dirs = $$('.sorts .direction');
        dirs.each(function(dir) {
            dir.observe('change', function(event) {
                Cookie.set(this.up().id+'_dir',this.value);
                event.stop() 
            });
        });
    }
});
var __MANAGER,__ENTRY;
Event.observe(window, 'load', init, false);
function init() {
    __MANAGER = new AccountEngineObject();
    __ENTRY = new EntriesEngineObject();
    setTimeout(function() {
        var summaries = $$('.nocache a.summary');
        var count = summaries.length;
        if(count>0)
        loadSummary(summaries,count);
    },1000);
    if($$('.nocache').length > 0) loadStatistic();
    addTabing($$('.tab li>a'));
    addHovers($$('.excerpts a'));
    addActions();
}
function changePassword() {
    __MANAGER.changePassword();
}
function updateAccount($account) {
    __MANAGER.updateAccount($account);
}
function showChangePasswordLightbox(){
    Lightbox.showUrl('lightboxes/Account.Password.php',function(){});
}
function showChangeGroupLightbox(ctrl) {
    Lightbox.showUrl('lightboxes/Account.Groups.Change.php',function(){});
}
function showJoinGroupLightbox(ctrl) {
    Lightbox.showUrl('lightboxes/Account.Groups.Join.php',function(){});
}
function goToPage($paged,$page) {
    __ENTRY.goToPage($paged,$page);
}
function loadSummary(summaries,count) {
    __ENTRY.loadSummary(summaries,count);
}
function loadStatistic() {
    __ENTRY.loadStatistic();
}
function addTabing(tabs) {
    __MANAGER.addTabing(tabs);
}
function addHovers(links) {
    __ENTRY.addHovers(links);
}
function addActions() {
    __ENTRY.addActions();
}
function loadSection($section,sortby,direction) {
    __ENTRY.loadSection($section,sortby,direction);
}