var Interface = Class.create({
    initialize: function () {
        getPlatform();
    },
    hideStatus: function () {
        hideStatus();
    },
    toggleFormPublicStatus: function (formId, publicStatus) {
        if (publicStatus == 1) {
            this.showButtons(formId);
        } else {
            this.hideButtons(formId);
        }
    },
    showButtons: function (formId) {
        $('li' + formId).removeClassName('notActive');
    },
    hideButtons: function (formId) {
        $('li' + formId).addClassName('notActive');
    },
    duplicateAnimation: function () {
        var formList = Array();
        $$('.group li').each(function (num, index) {
            formList[index] = num.id.replace('li', '') * 1;
        });
        maxFormID = formList.max();
        el = $('li' + maxFormID).down();
        var initialColor = el.getStyle('backgroundColor');
        var hexInitialColor = new RGBColor(initialColor);
        el.style.backgroundColor = '#99D247';
        new Effect.Highlight(el, {
            startcolor: '#99D247',
            endcolor: hexInitialColor.toHex(),
            restorecolor: initialColor,
            duration: 2,
            queue: 'end'
        });
    },
    showSearching: function (searchTerm) {
        $('searchTerm').innerHTML = searchTerm;
        $('searching').removeClassName('hide');
    },
    hideSearching: function () {
        if (!$('searching').hasClassName('hide')) $('searching').addClassName('hide');
    },
    showNoResults: function () {
        this.hideSearching();
        $('noResults').removeClassName('hide');
    },
    hideNoResults: function () {
        if (!$('noResults').hasClassName('hide')) $('noResults').addClassName('hide');
    },
    toggleSelectedSort: function (sort) {
        $('sort').childElements().invoke('removeClassName', 'selected');
        $(sort).addClassName('selected');
    }
});
var FormEngineObject = Class.create({
    initialize: function () {}, //<sl:translate>
    deleteFormStatus: 'Deleting form ...',
    duplicatingFormStatus: 'Duplicating form ...',
    savingPublicStatusStatus: 'Toggling the public setting of your form ...',
    saveThemeStatus: 'Changing the theme for this form ...',
    sortFormsStatus: 'Resorting your forms ...',
    displayGraphStatus: 'Processing data and drawing graph ...', //</sl:translate>
    deleteForm: function (formId, formName) {
        $('statusText').innerHTML = this.deleteFormStatus;
        var myAjax = new Ajax.Request('index.php?req=admin/', {
            method: 'post',
            parameters: 'action=delete&formId=' + formId + '&formName=' + encodeURIComponent(formName),
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishDeletingForm(ret);
            }.bind(this)
        });
    },
    duplicateForm: function (formId, formName) {
        $('statusText').innerHTML = this.duplicatingFormStatus;
        var myAjax = new Ajax.Request('index.php?req=admin/', {
            method: 'post',
            parameters: 'action=duplicate&formId=' + formId + '&formName=' + encodeURIComponent(formName),
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishDuplicatingForm(ret);
            }.bind(this)
        });
    },
    savePublicStatus: function (formId, formName, publicStatus) {
        $('statusText').innerHTML = this.savingPublicStatusStatus;
        var myAjax = new Ajax.Request('index.php?req=admin/', {
            method: 'post',
            parameters: 'action=savePublicStatus&formId=' + formId + '&formName=' + encodeURIComponent(formName) + '&publicStatus=' + publicStatus,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishSavingPublicStatus(ret);
            }.bind(this)
        });
    },
    savePassword: function (formId, formName, password) {
        var myAjax = new Ajax.Request('index.php?req=admin/', {
            method: 'post',
            parameters: 'action=protect&formId=' + formId + '&formName=' + encodeURIComponent(formName) + '&password=' + encodeURIComponent(password),
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishProtectForm(ret);
            }.bind(this)
        });
    },
    saveTheme: function (formId, formName, styleId, formUrl) {
        $('statusText').innerHTML = this.saveThemeStatus;
        var myAjax = new Ajax.Request('index.php?req=themes/attach/', {
            method: 'post',
            parameters: 'formId=' + formId + '&formName=' + formName + '&styleId=' + styleId + '&formUrl=' + formUrl,
            onComplete: function (response) {}
        });
    },
    sortForms: function (sortBy) {
        $('statusText').innerHTML = this.sortFormsStatus;
        var myAjax = new Ajax.Request('index.php?req=admin/', {
            method: 'post',
            parameters: 'action=order&orderBy=' + sortBy + '&orderDirection=DESC',
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishFilteringForm(ret);
            }.bind(this)
        });
    },
    formMatchesFilter: function (form, filter) {
        var username = form.Name || '';
        var fullname = form.Fullname || '';
        var nickskype = form.Nickskype || '';
        var nickh2d = form.Nickh2d || '';
        var email = form.Email || '';
        var isactive = form.IsActivated || '';
        var group_id = form.GroupId || '';
        var group_name = form.GroupName || '';
        if (username.toLowerCase().include(filter.toLowerCase())) return true;
        else if (fullname.toLowerCase().include(filter.toLowerCase())) return true;
        else if (nickskype.toLowerCase().include(filter.toLowerCase())) return true;
        else if (nickh2d.toLowerCase().include(filter.toLowerCase())) return true;
        else if (email.toLowerCase().include(filter.toLowerCase())) return true;
        else if (isactive.toLowerCase().include(filter.toLowerCase())) return true;
        else if (group_id.toLowerCase().include(filter.toLowerCase())) return true;
        else if (group_name.toLowerCase().include(filter.toLowerCase())) return true;
        else return false;
    }
});
var FormsObject = Class.create({
    initialize: function (Forms) {
        this.data = Forms;
    },
    getForm: function (formId) {
        if (this.data[formId]) return this.data[formId];
        else return false;
    }
});
var FormManager = Class.create({
    FormEngine: new FormEngineObject(),
    UI: '',
    Forms: '',
    sortSetting: 'DateCreated',
    filterSetting: '',
    searchedString:'',
    g_type: 'week',
    initialize: function () {
        if ($('expandThis')) {
            this.actionMarkup = $('expandThis').innerHTML;
        }
        this.UI = new Interface();
        this.Forms = new FormsObject(__USERS);
        this.UI.hideStatus();
        this.setSortDefaults();
        if (document.URL.indexOf('highlight') != -1) {
            this.UI.duplicateAnimation();
        }
        if ($('searchBox')) $('searchBox').focus();
        if (this.readCookie('stats') && this.readCookie('stats') != 'Off') this.loadDefaultStats();
    },
    attachEvents: function () {
        $$('#groupList-1 li').each(function (num, index) {
            num.observe('mouseover', showActions.bind(num));
            num.observe('mouseout', hideActions.bind(num));
        });
    },
    setSortDefaults: function () {
//        var filter = this.readCookie('userManagerFilter');
//        $('searchBox').value = filter;
//        if (filter && filter != '') {
//            this.filterSetting = filter
//            this.runFilter();
//        }
    },
    showActions: function (ctrl) {
        var ctrl = $(ctrl);
        if (ctrl.down) {
            this.activeForm = ctrl.id;
            if (!ctrl.down('.actions')) {
                ctrl.insert({
                    bottom: this.actionMarkup
                });
                var linkToEntries = $(ctrl.down('.col-1').down('.account_name')).href;
                ctrl.down('.view').href = linkToEntries;
            }
            ctrl.addClassName('hover');
        }
    },
    hideActions: function (ctrl) {
        var ctrl = $(ctrl);
        if (ctrl.down) {
            ctrl.removeClassName('hover');
        }
    },
    editForm: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=build/' + form.Url + '';
    },
    viewEntries: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=entries/' + form.Url + '';
    },
    viewCode: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=code/' + form.Url + '';
    },
    viewPayment: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=merchant/' + form.Url + '';
    },
    viewRules: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=rules/' + form.Url + '';
    },
    viewAnalytics: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=analytics/' + form.Url + '';
    },
    viewNotifications: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=notifications/' + form.Url + '';
    },
    deleteForm: function (formId) {
        var form = this.Forms.getForm(formId);
        Lightbox.showUrl('lightboxes/Form.Delete.php', function () {
            $('deleteFormId').value = form.FormId;
            $('deleteFormName').innerHTML = form.Name;
        }, 'error');
        return false;
    },
    continueDeletingForm: function () {
        var form = this.Forms.getForm($F('deleteFormId'));
        this.FormEngine.deleteForm(form.FormId, form.Name);
    },
    successfulFormDeletion: function () {
        window.location.reload(true);
        return;
    },
    failedFormDeletion: function (ret) {
        Lightbox.showContent(ret.response.lbmarkup);
    },
    duplicateForm: function (formId) {
        var form = this.Forms.getForm(formId);
        this.FormEngine.duplicateForm(form.FormId, form.Name);
    },
    successfulFormDuplication: function (formId) {
        window.location.reload(true);
        return;
    },
    failedFormDuplication: function (ret) {
        Lightbox.showContent(ret.response.lbmarkup);
    },
    savePublicStatus: function (formId, formName, ctrl) {
        publicStatus = (ctrl.checked) ? 1 : 0;
        this.UI.toggleFormPublicStatus(formId, publicStatus);
        this.FormEngine.savePublicStatus(formId, formName, publicStatus);
    },
    failedSavePublicStatus: function (ret) {
        var ctrl = $('publicStatus_' + ret.response.formId);
        ctrl.checked = !ctrl.checked;
        var publicStatus = (ctrl.checked) ? 1 : 0;
        this.UI.toggleFormPublicStatus(ret.response.formId, publicStatus);
        Lightbox.showContent(ret.response.lbmarkup);
    },
    showProtectForm: function (formId) {
        var form = this.Forms.getForm(formId);
        Lightbox.showUrl('lightboxes/Form.Protect.php?formId=' + form.FormId, function () {
            $('protect_formId').value = form.FormId;
        });
        return false;
    },
    protectForm: function () {
        var form = this.Forms.getForm($('protect_formId').value);
        this.FormEngine.savePassword(form.FormId, form.Name, $('protect_password').value);
    },
    successfulProtectForm: function (ret) {
        Lightbox.close();
    },
    failedProtectForm: function (ret) {
        $('protect_error').innerHTML = ret.response.errorMessage;
        Element.addClassName('protect_error', 'error');
        Element.addClassName('protect_parent', 'error');
    },
    changeTheme: function (formId, formName, ctrl) {
        var form = this.Forms.getForm(formId);
        if (ctrl.options[ctrl.selectedIndex].value == 'create') {
            document.location = 'index.php?req=themes/';
            return false;
        }
        this.FormEngine.saveTheme(formId, formName, ctrl.options[ctrl.selectedIndex].value, form.Url);
    },
    sortBy: function (sort) {
        if (sort != this.sortSetting) {
            this.sortSetting = sort;
            this.UI.toggleSelectedSort(this.sortSetting);
            this.FormEngine.sortForms(this.sortSetting);
            this.createCookie('formManagerOrder', sort);
        }
    },
    filterBy: function (filter) {
        if (filter != this.filterSetting) {
            this.filterSetting = filter;
            this.runFilter();
            this.createCookie('userManagerFilter', filter);
        }
    },
    ajaxSearch: function (search) {
        if (search != this.searchedString) {
            this.searchedString = search;
            this.runSearch();
            this.createCookie('userManagerFilter', search);
        }
    },
    runSearch: function() {
        this.UI.showSearching(this.searchedString);
        var zebra = 'alt';
        var matches = false;
        var numberOfMatches = 0;
        $('groupList-1').childElements().each(function (num, index) {
            var form = this.Forms.data[num.id.replace('user_', '')];
            if (this.FormEngine.formMatchesFilter(form, this.searchedString)) {
                matches = true;
                numberOfMatches++;
                num.removeClassName('hide');
                if (num != 0) {
                    if (numberOfMatches == 1) {
                        this.showActions(num);
                        num.removeClassName('hover');
                        if (!num.hasClassName('first')) num.addClassName('first');
                    } else {
                        this.hideActions(num);
                        num.removeClassName('first');
                    }
                }
                if (zebra == 'alt') {
                    num.addClassName('alt');
                    zebra = '';
                } else {
                    num.removeClassName('alt');
                    zebra = 'alt';
                }
            } else {
                num.addClassName('hide');
            }
        }.bind(this));
        
        if (!this.searchedString) {
            this.UI.hideSearching();
        }
        if (!matches) {
            var ui = this.UI;
            var searchString = this.searchedString;
            var pars='action=search&searchString='+encodeURIComponent(searchString);
            var myAjax = new Ajax.Request(new String(document.location), {
                parameters: pars,
                onComplete: function (response) {
                    var result = response.responseText.evalJSON();
                    if(result.success == true) {
                        $('groupList-1').insert(result.response.html);
                        updateUsersJson(result.response.json);
                    } else if(result.success == false) {
                        ui.showNoResults();
                    }
                },
                onError: function() {
                    ui.showNoResults();
                }
            });
        } else {
            this.UI.hideNoResults();
        }
    },
    runFilter: function () {
        this.UI.showSearching(this.filterSetting);
        var zebra = 'alt';
        var matches = false;
        var numberOfMatches = 0;
        $('groupList-1').childElements().each(function (num, index) {
            var form = this.Forms.data[num.id.replace('user_', '')];
            
            if (this.FormEngine.formMatchesFilter(form, this.filterSetting)) {
                matches = true;
                numberOfMatches++;
                $('user_' + form.FormId).removeClassName('hide');
                if (num != 0) {
                    if (numberOfMatches == 1) {
                        this.showActions($('user_' + form.FormId));
                        $('user_' + form.FormId).removeClassName('hover');
                        if (!$('user_' + form.FormId).hasClassName('first')) $('user_' + form.FormId).addClassName('first');
                    } else {
                        this.hideActions($('user_' + form.FormId));
                        $('user_' + form.FormId).removeClassName('first');
                    }
                }
                if (zebra == 'alt') {
                    $('user_' + form.FormId).addClassName('alt');
                    zebra = '';
                } else {
                    $('user_' + form.FormId).removeClassName('alt');
                    zebra = 'alt';
                }
            } else $('user_' + form.FormId).addClassName('hide');
        }.bind(this));
        if (!this.filterSetting) {
            this.UI.hideSearching();
        }
        if (!matches) this.UI.showNoResults();
        else this.UI.hideNoResults();
    },
    failedFilterForm: function (ret) {
        Lightbox.showContent(ret.response.lbmarkup);
    },
    successfulFilterForm: function (ret) {
        $('groupList-1').innerHTML = ret.response.formList;
        this.runFilter();
        this.attachEvents();
    },
    loadDefaultStats: function () {
        vals = this.readCookie('stats').split('|');
        found = false;
        for (var i = 0; i < $('allForms').options.length; i++) {
            if ($('allForms').options[i].value == vals[1]) {
                $('allForms').selectedIndex = i;
                found = true;
                break;
            }
        }
        if (!found) $('allForms').selectedIndex = 0;
        Effect.BlindDown('stats', {
            duration: .4,
            afterFinish: function () {
                this.changeType(vals[2], true);
            }.bind(this)
        });
    },
    changeType: function (newType, first) {
        if (newType && newType != this.g_type || first == true) {
            this.g_type = newType;
            Element.removeClassName('todayGraph', 'selected');
            Element.removeClassName('weekGraph', 'selected');
            Element.removeClassName('monthGraph', 'selected');
            Element.removeClassName('yearGraph', 'selected');
            Element.addClassName(newType + 'Graph', 'selected');
            this.refreshGraph();
        }
    },
    refreshGraph: function () {
        cookieVal = 'on|' + $('allForms').options[$('allForms').selectedIndex].value + '|' + this.g_type;
        this.createCookie('stats', cookieVal);
        this.FormEngine.displayGraph($('allForms').options[$('allForms').selectedIndex].value, this.g_type);
    },
    toggleGraph: function () {
        if (!$('stats').offsetHeight || $('stats').offsetHeight == 0) this.showGraph();
        else this.hideGraph();
    },
    showGraph: function () {
        Effect.BlindDown('stats', {
            duration: .4,
            afterFinish: function () {
                this.refreshGraph();
            }.bind(this)
        });
    },
    hideGraph: function () {
        this.createCookie('stats', 'Off');
        $('formGraph').innerHTML = '';
        Effect.BlindUp('stats', {
            duration: .4
        });
    },
    createCookie: function (name, value) {
        createCookie(name, value);
    },
    readCookie: function (name) {
        return readCookie(name);
    }
});
var AccountEngineObject = Class.create({
    activeAccount: function($account) {
        var url = 'index.php?req=accounts/';
        var pars = 'action=activate&user_id='+$account.rel;
        var myAjax = new Ajax.Request(url, {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == 'true') {
                    $account.style.color = 'green';
                    $account.innerHTML = 'Activated';
                    $account.parentNode.parentNode.style.backgroundColor = '#FFE14F';
                    setTimeout(function() {
                        $account.parentNode.parentNode.style.backgroundColor = '';
                    },1500);
                } else {
                    $account.parentNode.parentNode.style.backgroundColor = '#FF8989';
                    setTimeout(function() {
                        $account.parentNode.parentNode.style.backgroundColor = '';
                    },1500);
                }
            }
        });
        return;
    },
    disableAccount: function($account) {
        var url = 'index.php?req=accounts/';
        var pars = 'action=disabled&user_id='+$account.rel;
        var myAjax = new Ajax.Request(url, {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == 'true') {
                    $account.style.color = 'red';
                    $account.innerHTML = 'Disabled';
                    $account.parentNode.parentNode.style.backgroundColor = '#FFE14F';
                    setTimeout(function() {
                        $account.parentNode.parentNode.style.backgroundColor = '';
                    },1500);
                } else {
                    $account.parentNode.parentNode.style.backgroundColor = '#FF8989';
                    setTimeout(function() {
                        $account.parentNode.parentNode.style.backgroundColor = '';
                    },1500);
                }
            }
        });
        return;
    },
    deleteAccount: function($account) {
        var url = 'index.php?req=accounts/';
        var pars = 'action=delete&user_id='+$account.rel;
        var isConfirm = window.confirm('Delete This Will Delete All Data Associated With');
        if(isConfirm) {
        var myAjax = new Ajax.Request(url, {
            
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == 'true') {
                    $account.parentNode.parentNode.parentNode.style.backgroundColor = '#FFE14F';
                    setTimeout(function() {
                        $account.parentNode.parentNode.parentNode.remove();
                    },1500);
                } else {
                    $account.parentNode.parentNode.parentNode.style.backgroundColor = '#FF8989';
                    setTimeout(function() {
                        $account.parentNode.parentNode.parentNode.style.backgroundColor = '';
                    },1500);
                }
            }
        });
        }
    },
    loadStatistic: function(stats,count) {
        return;
        var action = 'summary';
        var user = stats.shift();
        var user_id = user.rel;
        var pars='action=statistic&user_id='+encodeURIComponent(user_id);
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                count = stats.length;
                if(result.success == 'true') {
                    user.innerHTML = result.response.html;
                    if(count > 0) {
                        this.loadStatistic(stats,count);
                    }
                    
                }
            }
        });
    },
    addGroup: function($group) {
        var group_id = $group.up().down('select').value;
        var group_name = $group.up().down('select').select('option[value="' + group_id + '"]')[0].innerHTML
        var user_id = $group.up().up().up().up().up().id.replace('user_','');
        var pars='action=addgroup&group_id='+encodeURIComponent(group_id)+'&user_id='+user_id;
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == true) {
                    var newobj = document.createElement('li');
                    newobj.innerHTML = '<strong>'+group_name+'</strong>';
                    console.log(newobj);
                    $group.up().previous('ul').appendChild(newobj);
                } else {
                    alert(result.response.message);
                }
            }
        });
    },
    changeGroup: function($group) {
        var group_id = $group.up().down('.groupname').value;
        var old_group_id = $group.up().id.replace('group_','');
        var group_name = $group.up().down('.groupname').select('option[value="' + group_id + '"]')[0].innerHTML
        var user_id = $group.up().up().up().up().up().up().id.replace('user_','');
        var pars='action=changegroup&group_id='+encodeURIComponent(group_id)+'&user_id='+user_id+'&old_group='+old_group_id;
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == true) {
                    $group.up().down('strong').innerHTML = group_name;
                } else {
                    alert(result.response.message);
                }
            }
        });
    },
    deleteGroup: function($group) {
        var old_group_id = $group.up().id.replace('group_','');
        var user_id = $group.up().up().up().up().up().up().id.replace('user_','');
        var pars='action=deletegroup&group_id='+encodeURIComponent(old_group_id)+'&user_id='+user_id;
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == true) {
                    $group.up().remove();
                } else {
                    alert(result.response.message);
                }
            }
        });
    },
    lockSubject: function(ctrl) {
        var user_id = $(ctrl).up('li').id.replace('user_','');
        var subject_id = $(ctrl).up('p').down('.subject_title').id.replace('s_','');
        var pars='action=lockSubject&subject_id='+subject_id+'&user_id='+user_id;
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success) {
                    $(ctrl).up('p').insert('<a onclick="unlockSubject(this); return false;" class="button unlock" title="Click to unlock." href="#">UNLOCK</a>');
                    $(ctrl).remove();
                } else {
                    alert(result.response.message);
                }
            }
        });
    },
    unlockSubject: function(ctrl) {
        var user_id = $(ctrl).up('li').id.replace('user_','');
        var subject_id = $(ctrl).up('p').down('.subject_title').id.replace('s_','');
        var pars='action=unlockSubject&subject_id='+subject_id+'&user_id='+user_id;
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == true) {
                    $(ctrl).up('p').insert('<a onclick="lockSubject(this); return false;" class="button lock" title="Click to lock." href="#">LOCKED</a>');
                    $(ctrl).remove();
                } else {
                    alert(result.response.message);
                }
            }
        });
    }
});
var __MANAGER,__FM;
var searchBox;
Event.observe(window, 'load', init, false);
function init() {
    __MANAGER = new AccountEngineObject();
    __FM = new FormManager();
    $('account').removeAttribute('class');
    __FM.setSortDefaults();
    var searchTimeout;
    searchBox = document.getElementById('searchBox');
    ajaxSearch();
    searchBox.onkeypress = function () {
        if (searchTimeout != undefined) clearTimeout(searchTimeout);
        searchTimeout = setTimeout(ajaxSearch, 250);
    };
}
function loadStats() {
    var stats = $$('.nocache .stats');
    var count = stats.length;
    if(count>0) loadStatistic(stats,count);
    __MANAGER.loadStatistic(stats,count);
}
function activeAccount($account) {
    __MANAGER.activeAccount($account);
}
function disableAccount($account) {
    __MANAGER.disableAccount($account);;
}
function deleteAccount($account) {
    __MANAGER.deleteAccount($account);
}
function loadStatistic(stats,count) {
    __MANAGER.loadStatistic(stats,count);
}
function addGroup($group) {
    __MANAGER.addGroup($group);
}
function changeGroup($group) {
    __MANAGER.changeGroup($group);
}
function deleteGroup($group) {
    __MANAGER.deleteGroup($group);
}
function filterBy(filter){__FM.filterBy(filter);}
function finishFilteringForm(ret){if(ret.success=='false')__FM.failedFilterForm(ret);else __FM.successfulFilterForm(ret);}


function updateUsersJson(json) {
    var forms = merge_options(__USERS,json);
    __USERS = forms;
    __FM.Forms = new FormsObject(forms);
    
}
function merge_options(obj1,obj2){
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
}
function ajaxSearch(){
    search = searchBox.value;
    __FM.ajaxSearch(search);
}
function lockSubject(ctrl) {
    __MANAGER.lockSubject(ctrl);
}
function unlockSubject(ctrl) {
    __MANAGER.unlockSubject(ctrl);
}