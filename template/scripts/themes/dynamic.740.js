var Base = Class.create({
    initialize: function () {},
    _initBase: function () {
        this._extendObjectClass();
        this._extendStringClass();
        this._extendElementClass();
        this._extendAjaxClass();
        this._makeFunctionsPublic();
    },
    _extendObjectClass: function () {
        Object.extend(Object, {
            isEmpty: function (text) {
                return (typeof (text) == 'undefined' || text == null || text == '') ? true : false;
            }
        });
    },
    _extendStringClass: function () {
        String.prototype.equals = function (str, isCaseSensitive) {
            isCaseSensitive = isCaseSensitive || false;
            var safeThis = this.strip();
            safeThis = (isCaseSensitive) ? safeThis : safeThis.toLowerCase();
            safeStr = str || '';
            safeStr = safeStr.strip();
            safeStr = (isCaseSensitive) ? safeStr : safeStr.toLowerCase();
            return (safeThis == safeStr);
        };
    },
    _extendElementClass: function () {
        Object.extend(Element, {
            selectOption: function (selectElement, selected) {
                var foundSelectedOption = false;
                var children = (selectElement) ? selectElement.childElements() : [];
                children.each(function (option) {
                    if (option.childElements().length == 0) {
                        option.selected = false;
                        if (option.value == selected) {
                            option.selected = true;
                            foundSelectedOption = true;
                        }
                    } else {
                        option.childElements().each(function (subOption) {
                            subOption.selected = false;
                            if (subOption.value == selected) {
                                subOption.selected = true;
                                foundSelectedOption = true;
                            }
                        }.bind(this));
                    }
                }.bind(this));
                if (!foundSelectedOption) {
                    if (children.first().childElements().length == 0) {
                        children.first().selected = true;
                    } else {
                        children.first().childElements().first().selected = true;
                    }
                }
            }
        });
    },
    _extendAjaxClass: function () {
        Object.extend(Ajax, {
            htdPost: function (msg, url, parameters, onSuccess, onFail) {
                if ($('statusText') && msg) {
                    $('statusText').innerHTML = (msg.endsWith('...')) ? msg : msg + ' ...';
                }
                url = (url == '') ? new String(document.location) : url;
                onFail = (Object.isEmpty(onFail)) ? Ajax.simplePostFail : onFail;
                onSuccess = (Object.isEmpty(onSuccess)) ? Ajax.simplePostSuccess : onSuccess;
                var myAjax = new Ajax.Request(url, {
                    method: 'post',
                    parameters: parameters,
                    onComplete: function (r) {
                        ret = r.responseText.evalJSON();
                        if (ret.success == 'false') onFail(ret);
                        else onSuccess(ret);
                    }.bind(this)
                });
            },
            simplePostFail: function (ret) {
                Lightbox.showContent(ret.response.lbmarkup);
            },
            simplePostSuccess: function (ret) {}
        });
    },
    _makeFunctionsPublic: function () {
        for (var key in this) {
            if (this._isFunctionPublic(key)) {
                window[key] = this[key].bind(this);
            }
        }
    },
    _isFunctionPublic: function (funcName) {
        var isPublicFunction = false;
        if (typeof (this[funcName]) == 'function') {
            if (!funcName.startsWith('_')) {
                if (['initialize', 'constructor'].indexOf(funcName) < 0) {
                    isPublicFunction = true;
                }
            }
        }
        return isPublicFunction;
    }
});

function RGBColor(color_string) {
    this.ok = false;
    if (color_string.charAt(0) == '#') {
        color_string = color_string.substr(1, 6);
    }
    color_string = color_string.replace(/ /g, '');
    color_string = color_string.toLowerCase();
    var simple_colors = {
        antiquewhite: 'faebd7',
        aqua: '00ffff',
        aquamarine: '7fffd4',
        azure: 'f0ffff',
        beige: 'f5f5dc',
        black: '000000',
        blue: '0000ff',
        blueviolet: '8a2be2',
        brown: 'a52a2a',
        cadetblue: '5f9ea0',
        chartreuse: '7fff00',
        chocolate: 'd2691e',
        coral: 'ff7f50',
        cornflowerblue: '6495ed',
        crimson: 'dc143c',
        cyan: '00ffff',
        darkblue: '00008b',
        darkgray: 'a9a9a9',
        darkgreen: '006400',
        darkorange: 'ff8c00',
        darkred: '8b0000',
        darkviolet: '9400d3',
        floralwhite: 'fffaf0',
        forestgreen: '228b22',
        fuchsia: 'ff00ff',
        gold: 'ffd700',
        goldenrod: 'daa520',
        gray: '808080',
        green: '008000',
        greenyellow: 'adff2f',
        hotpink: 'ff69b4',
        indianred: 'cd5c5c',
        indigo: '4b0082',
        ivory: 'fffff0',
        khaki: 'f0e68c',
        lavender: 'e6e6fa',
        lawngreen: '7cfc00',
        lightblue: 'add8e6',
        lightgrey: 'd3d3d3',
        lightgreen: '90ee90',
        lightpink: 'ffb6c1',
        lightyellow: 'ffffe0',
        lime: '00ff00',
        limegreen: '32cd32',
        linen: 'faf0e6',
        magenta: 'ff00ff',
        maroon: '800000',
        midnightblue: '191970',
        moccasin: 'ffe4b5',
        navy: '000080',
        olive: '808000',
        orange: 'ffa500',
        orangered: 'ff4500',
        palegreen: '98fb98',
        pink: 'ffc0cb',
        plum: 'dda0dd',
        powderblue: 'b0e0e6',
        purple: '800080',
        red: 'ff0000',
        royalblue: '4169e1',
        salmon: 'fa8072',
        sandybrown: 'f4a460',
        seagreen: '2e8b57',
        sienna: 'a0522d',
        silver: 'c0c0c0',
        skyblue: '87ceeb',
        slateblue: '6a5acd',
        slategray: '708090',
        snow: 'fffafa',
        steelblue: '4682b4',
        tan: 'd2b48c',
        teal: '008080',
        tomato: 'ff6347',
        turquoise: '40e0d0',
        violet: 'ee82ee',
        violetred: 'd02090',
        white: 'ffffff',
        yellow: 'ffff00',
        yellowgreen: '9acd32'
    };
    for (var key in simple_colors) {
        if (color_string == key) {
            color_string = simple_colors[key];
        }
    }
    var color_defs = [{
        re: /^rgb\((\d{1,3}),\s*(\d{1,3}),\s*(\d{1,3})\)$/,
        example: ['rgb(123, 234, 45)', 'rgb(255,234,245)'],
        process: function (bits) {
            return [parseInt(bits[1]), parseInt(bits[2]), parseInt(bits[3])];
        }
    }, {
        re: /^(\w{2})(\w{2})(\w{2})$/,
        example: ['#00ff00', '336699'],
        process: function (bits) {
            return [parseInt(bits[1], 16), parseInt(bits[2], 16), parseInt(bits[3], 16)];
        }
    }, {
        re: /^(\w{1})(\w{1})(\w{1})$/,
        example: ['#fb0', 'f0f'],
        process: function (bits) {
            return [parseInt(bits[1] + bits[1], 16), parseInt(bits[2] + bits[2], 16), parseInt(bits[3] + bits[3], 16)];
        }
    }];
    for (var i = 0; i < color_defs.length; i++) {
        var re = color_defs[i].re;
        var processor = color_defs[i].process;
        var bits = re.exec(color_string);
        if (bits) {
            channels = processor(bits);
            this.r = channels[0];
            this.g = channels[1];
            this.b = channels[2];
            this.ok = true;
        }
    }
    this.r = (this.r < 0 || isNaN(this.r)) ? 0 : ((this.r > 255) ? 255 : this.r);
    this.g = (this.g < 0 || isNaN(this.g)) ? 0 : ((this.g > 255) ? 255 : this.g);
    this.b = (this.b < 0 || isNaN(this.b)) ? 0 : ((this.b > 255) ? 255 : this.b);
    this.toRGB = function () {
        return 'rgb(' + this.r + ', ' + this.g + ', ' + this.b + ')';
    }
    this.toHex = function () {
        var r = this.r.toString(16);
        var g = this.g.toString(16);
        var b = this.b.toString(16);
        if (r.length == 1) r = '0' + r;
        if (g.length == 1) g = '0' + g;
        if (b.length == 1) b = '0' + b;
        return '#' + r + g + b;
    }
}
var Interface = Class.create({
    textSample: 1, //<sl:translate>
    pickTypoWeapon: "Pick a typographic weapon from below.",
    alphabet: "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz",
    quickBrownFox: "The quick brown fox jumps over the lazy dog.",
    wolfZombies: "Wolf zombies quickly spot the jinxed grave.",
    wizardsJob: "A wizard’s job is to vex chumps quickly in fog.",
    watchJeopardy: "Watch ‘Jeopardy!’, Alex Trebek’s fun TV quiz game.",
    myGirlWove: "My girl wove six dozen plaid jackets before she quit.",
    fewBlackTaxis: "Few black taxis drive up major roads on quiet hazy nights.",
    sixBoys: "Six boys guzzled cheap raw plum vodka quite joyfully.", //</sl:translate>
    initialize: function () {},
    hideStatus: function () {
        hideStatus();
    },
    pickFont: function (el) {
        this.updateBigSample(el);
        this.fontPickerSelect(el);
    },
    updateBigSample: function (el) {
        $('bigSample').style.fontFamily = el.down().style.fontFamily;
        if ($('bigSample').innerHTML.strip() == this.pickTypoWeapon) {
            this.rotateText();
        }
    },
    fontPickerSelect: function (el) {
        this.clearPickedFonts();
        el.addClassName('selected');
    },
    clearPickedFonts: function () {
        $$('.previews li').invoke('removeClassName', 'selected');
    },
    rotateText: function () {
        if ($('bigSample').innerHTML == this.pickTypoWeapon) {
            this.textSample = 1;
        } else if ($('bigSample').innerHTML == this.alphabet) {
            this.textSample = 2;
        }
        switch (this.textSample) {
            case 1:
                $('bigSample').innerHTML = this.alphabet;
                break;
            case 2:
                $('bigSample').innerHTML = this.quickBrownFox;
                break;
            case 3:
                $('bigSample').innerHTML = this.wolfZombies;
                break;
            case 4:
                $('bigSample').innerHTML = this.wizardsJob;
                break;
            case 5:
                $('bigSample').innerHTML = this.watchJeopardy;
                break;
            case 6:
                $('bigSample').innerHTML = this.myGirlWove;
                break;
            case 7:
                $('bigSample').innerHTML = this.fewBlackTaxis;
                break;
            case 8:
                $('bigSample').innerHTML = this.sixBoys;
                this.textSample = 0;
                break;
        }
        this.textSample++;
    }
});
var ThemeDesignerEngine = Class.create({ //<sl:translate>
    loadThemesStatus: 'Loading themes',
    renameThemeStatus: 'Renaming theme',
    deleteThemeStatus: 'Deleting theme',
    saveThemeStatus: 'Saving theme',
    fetchPageStatus: 'Next Page',
    loadLocalFontsStatus: 'Loading fonts',
    getKitStatus: 'Loading Typekit kit', //</sl:translate>
    initialize: function () {},
    loadThemes: function () {
        Ajax.htdPost(this.loadThemesStatus, 'index.php?req=themes/loadThemes/', '', successfulLoadThemes);
    },
    renameTheme: function (themeId, themeName, oldName) {
        var parameters = 'styleId=' + themeId + '&name=' + themeName + '&oldName=' + oldName;
        Ajax.htdPost(this.renameThemeStatus, 'index.php?req=themes/rename/', parameters, successfulRenameTheme);
    },
    deleteTheme: function (themeId, themeName) {
        var parameters = 'name=' + themeName + '&styleId=' + themeId;
        Ajax.htdPost(this.deleteThemeStatus, 'index.php?req=themes/delete/', parameters, successfulDeleteTheme);
    },
    saveTheme: function (theme) {
        var parameters = 'theme=' + encodeURIComponent(Object.toJSON(theme));
        Ajax.htdPost(this.saveThemeStatus, 'index.php?req=themes/save/', parameters, successfulSave);
    },
    fetchPage: function (type, page, font) {
        var parameters = 'action=fetchPage&type=' + type + '&page=' + page + '&font=' + font;
        Ajax.htdPost(this.fetchPageStatus, 'lightboxes/Theme.Font.php', parameters, successfulFetchPage);
    },
    loadLocalFonts: function (fontsToLoad) {
        var parameters = 'fontsToLoad=' + encodeURIComponent(Object.toJSON(fontsToLoad));
        Ajax.htdPost(this.loadLocalFontsStatus, 'index.php?req=themes/loadFonts/', parameters, successfulLoadLocalFonts);
    },
    getKit: function (kitId, onSuccess) {
        var parameters = 'kitId=' + kitId;
        Ajax.htdPost(this.getKitStatus, 'index.php?req=themes/getKit/', parameters, onSuccess);
    },
    getKitHTML: function (integration, onSuccess) {
        var parameters = 'integration=' + encodeURIComponent(Object.toJSON(integration));
        Ajax.htdPost(this.getKitStatus, 'index.php?req=themes/getKitHTML/', parameters, onSuccess);
    }
});
var ThemeMenu = Class.create({
    themeDesigner: '',
    themes: '',
    preview: '',
    submenu_class_names: new Array('showTypos'),
    menu_class_names: new Array('showLogoMenu', 'showTypoMenu', 'showBackMenu', 'showBordMenu', 'showShadMenu', 'showButtMenu', 'showAdvMenu'),
    initialize: function () {
        this.wireColorPickerEvents();
        this.wirePatternPickerEvents();
    },
    wireColorPickerEvents: function () {
        var initialColor = new RGBColor($('colorPicker').style.backgroundColor)
        $('color').value = initialColor.toHex();
        $$('#colorList li').each(function (color) {
            color.observe('click', function (event) {
                var el = Event.element(event);
                el = (el.tagName.toUpperCase() == 'SPAN') ? el.up() : el;
                this.selectColor(el.style.backgroundColor);
            }.bind(this));
        }.bind(this));
        $('color').onkeyup = function () {
            typeColor(this.value)
        };
    },
    typeColor: function (string) {
        typedColor = new RGBColor(string);
        if (typedColor.ok) {
            this.selectColor(string);
            $('color').value = string;
        }
    },
    wirePatternPickerEvents: function () {
        $('patternColor').observe('click', function (event) {
            this.selectSolidColor(Event.element(event));
        }.bind(this));
        $('customPattern').observe('click', function (event) {
            this.selectPattern(Event.element(event));
        }.bind(this));
        $$('#patternList li.p').each(function (pattern) {
            pattern.observe('click', function (event) {
                this.selectPattern(Event.element(event));
            }.bind(this));
        }.bind(this));
    },
    select: function (num) {
        $('themeManager').className = this.menu_class_names[(num - 1)];
        this.clearSecondaryMenu();
        this.clearSelectedPattern();
        
        switch (num) {
            case '1':
                this.themeDesigner.selectedProperty = 'logoUrl';
                var ret = this.preselect(this.themes.activeTheme['logoUrl'], 'logoMenu');
                if (ret == 1) this.selectLogo('custom', true);
                break;
            case '5':
                this.preselect(this.themes.activeTheme['dropShadow'], 'shadowMenu');
                break;
        }
    },
    selectLogo: function (str, beenSaved) {
        this.themeDesigner.selectedProperty = 'logoUrl';
        $('themeManager').removeClassName('showCustom');
        if (str != 'custom') {
            this.themes.activeTheme[this.themeDesigner.selectedProperty] = str;
            this.themes.activeTheme['logoHeight'] = 40;
            this.preview.apply(this.themeDesigner.selectedProperty, str);
            this.preview.apply('logoHeight', 40);
        } else {
            $('themeManager').addClassName('showCustom');
            $('custom').value = 'http://';
            $('logoHeight').value = 40;
            if (beenSaved) {
                $('custom').value = this.themes.activeTheme['logoUrl'];
                $('logoHeight').value = this.themes.activeTheme['logoHeight'];
            }
            this.selectCustom();
        }
    },
    selectColor: function (color) {
        if (color != '') {
            color = color || '';
            color = color.replace(' url(/images/fieldbg.gif) repeat-x top', '');
            if (color == "transparent" || color == "none") {
                $('color').value = "NONE";
                $('colorPreview').style.backgroundImage = 'url(/images/themes/patterns/trans.png)';
            } else {
                color = new RGBColor(color);
                color = color.toHex();
                $('color').value = color;
                $('colorPreview').style.background = color;
            }
            $('patternList').style.backgroundColor = color;
            if (this.themeDesigner.selectedProperty == 'bgField') {
                if (color != 'transparent') {
                    color += ' url(/images/fieldbg.gif) repeat-x top';
                }
            }
            this.themes.activeTheme[this.themeDesigner.selectedProperty + 'Color'] = color;
            this.preview.apply(this.themeDesigner.selectedProperty + 'Color', color);
        }
    },
    selectCustom: function () {
        var customValue = $('custom').value;
        var heightValue = $('logoHeight').value;
        if (this.themeDesigner.selectedProperty == 'logoUrl' && (customValue == '' || customValue == 'http://')) {
            customValue = '/images/themes/logos/none.png';
        }
        if (this.themeDesigner.selectedProperty != 'logoUrl') {
            var pattern = 'url(' + customValue + ')';
            this.themes.activeTheme[this.themeDesigner.selectedProperty + 'Pattern'] = pattern;
            this.preview.apply(this.themeDesigner.selectedProperty + 'Pattern', pattern);
        } else {
            this.themes.activeTheme[this.themeDesigner.selectedProperty] = customValue;
            this.themes.activeTheme['logoHeight'] = heightValue;
            this.preview.apply(this.themeDesigner.selectedProperty, customValue);
            this.preview.apply('logoHeight', heightValue);
        }
    },
    clearMenus: function (all) {
        var themeManager = $('themeManager');
        this.submenu_class_names.each(function (num) {
            themeManager.removeClassName(num);
        });
        if (all == 'all') {
            this.menu_class_names.each(function (num) {
                themeManager.removeClassName(num);
            });
            $('propertiesMenu').selectedIndex = -1;
            themeManager.removeClassName('showBords');
            themeManager.removeClassName('showBacks');
            themeManager.removeClassName('showPattern');
        }
        themeManager.removeClassName('showCustom');
    },
    clearSecondaryMenu: function () {
        $('typographyMenu').selectedIndex = "-1";
        $('backgroundsMenu').selectedIndex = "-1";
        $('bordersMenu').selectedIndex = "-1";
    },
    selectTheme: function (themeId) {
        this.applyButtons(themeId);
        this.showActionButtons(themeId);
        this.clearMenus('all');
    },
    applyButtons: function (themeId) {
        bType = this.themes.get('buttonType', 'styleId', themeId);
        bText = this.themes.get('buttonValue', 'styleId', themeId);
        if (bType == 'image') {
            $('butLink').checked = true;
            $('buttonImage').value = bText;
        } else {
            $('butText').checked = true;
            $('buttonText').value = (bText == '' || bText == 'Submit') ? __BTN_TEXT : bText;
        }
        this.changeButtonType();
    },
    changeButtonType: function () {
        if ($('butLink').checked) {
            $('textInput').addClassName('hide');
            $('linkInput').removeClassName('hide');
        } else {
            $('linkInput').addClassName('hide');
            $('textInput').removeClassName('hide');
        }
        this.updateButton();
    },
    updateButton: function () {
        if ($('butLink').checked) {
            $('saveForm').addClassName('hide');
            $('saveFormImage').removeClassName('hide');
            $('saveFormImage').src = $F('buttonImage');
        } else {
            $('saveFormImage').addClassName('hide');
            $('saveForm').removeClassName('hide');
            $('saveForm').value = $F('buttonText');
        }
    },
    showActionButtons: function (themeId) {
        if (themeId == 1) {
            $('otherButtons').removeClassName('show');
            if (LEVEL != '1') $('saveBtn').addClassName('hide');
        } else {
            $('otherButtons').addClassName('show');
            $('saveBtn').removeClassName('hide');
        }
    },
    showTypographyMenu: function (str) {
        this.clearMenus();
        $('themeManager').addClassName('showTypos');
    },
    backgrounds: function (str) {
        this.clearMenus();
        this.clearSelectedPattern();
        $('themeManager').addClassName('showBacks');
        this.themeDesigner.selectedProperty = str;
        if (str == 'bgHtml' || str == 'bgLogo') {
            this.showPatterns(str);
        } else {
            this.hidePatterns();
        }
    },
    borders: function (str) {
        this.clearMenus();
        $('themeManager').addClassName('showBords');
        this.themeDesigner.selectedProperty = str;
    },
    selectSolidColor: function (el) {
        this.clearSelectedPattern();
        Element.addClassName(el, 'selected');
        $('themeManager').removeClassName('showCustom');
        this.themes.activeTheme[this.themeDesigner.selectedProperty + 'Pattern'] = 'none';
        this.preview.apply(this.themeDesigner.selectedProperty + 'Pattern', 'none');
        this.selectColor(this.themes.activeTheme[this.themeDesigner.selectedProperty + 'Color']);
    },
    selectPattern: function (el) {
        this.clearSelectedPattern();
        Element.addClassName(el, 'selected');
        var pattern = el.title;
        if (pattern == '') {
            $('themeManager').addClassName('showCustom');
            $('custom').value = this.getCustomBackgroundURL();
            this.selectCustom();
        } else {
            $('themeManager').removeClassName('showCustom');
            this.themes.activeTheme[this.themeDesigner.selectedProperty + 'Pattern'] = pattern;
            this.preview.apply(this.themeDesigner.selectedProperty + 'Pattern', pattern);
        }
    },
    getCustomBackgroundURL: function () {
        var url = 'http://';
        if (this.themeDesigner.selectedProperty == 'bgHtml' || this.themeDesigner.selectedProperty == 'bgLogo') {
            url = this.themes.activeTheme[this.themeDesigner.selectedProperty + 'Pattern'];
            url = (url == 'none' || url == 'url()') ? '' : url;
            url = url || '';
            url = (url == '') ? 'http://' : url.substring(4, url.length - 1);
        }
        return url;
    },
    clearSelectedPattern: function () {
        $$('#patternList li').invoke('removeClassName', 'selected');
    },
    showPatterns: function (str) {
        var selected = false;
        $('themeManager').addClassName('showPattern');
        var patterns = $('patternList').getElementsByTagName('li');
        for (var i = 0; i < patterns.length; i++) {
            inline_style = patterns[i].readAttribute('style');
            if (inline_style && inline_style.replace('background:', '') == this.themes.activeTheme[str + 'Pattern']) {
                Element.addClassName(patterns[i], 'selected');
                selected = true;
            }
        }
        return selected;
    },
    hidePatterns: function () {
        $('themeManager').removeClassName('showPattern');
    },
    loadThemeMenu: function (collection) {
        var offset = 0;
        collection.each(function (num, index) {
            $('themeMenu').options[index + offset] = new Option(num.name, num.styleId);
            if (num.styleId == 1) {
                var option = new Option('-----------', 'none');
                option.setAttribute('disabled', 'disabled');
                $('themeMenu').options[index + 1] = option;
                offset = 1;
            }
        });
    },
    preselect: function (value, menu) {
        var dropdown = document.forms[0][menu];
        var options = dropdown.options;
        for (var i = 0; i < options.length; i++) {
            if (options[i].value == 'custom') {
                dropdown.selectedIndex = i;
            }
            if (options[i].value == value) {
                dropdown.selectedIndex = i;
                break;
            }
        }
        return dropdown.selectedIndex;
    }
});
var ThemePreview = Class.create({
    themeDesigner: '',
    fontPicker: '',
    themes: '',
    morphable: 'height font-size color background background-color border-width border-color',
    ftFormTitleFamily: {
        'property': 'fontFamily',
        'container': '',
        'selector': ['.info h2', '.footer p', '.footer p a']
    },
    ftFormTitleStyle: {
        'property': 'fontStyle',
        'container': '',
        'selector': ['.info h2']
    },
    ftFormTitleSize: {
        'property': 'font-size',
        'container': '',
        'selector': ['.info h2']
    },
    ftFormTitleColor: {
        'property': 'color',
        'container': '',
        'selector': ['.info h2', '.footer p', '.footer p a']
    },
    ftFormDescriptionFamily: {
        'property': 'fontFamily',
        'container': '',
        'selector': ['.info div']
    },
    ftFormDescriptionStyle: {
        'property': 'fontStyle',
        'container': '',
        'selector': ['.info div']
    },
    ftFormDescriptionSize: {
        'property': 'font-size',
        'container': '',
        'selector': ['.info div']
    },
    ftFormDescriptionColor: {
        'property': 'color',
        'container': '',
        'selector': ['.info div']
    },
    ftSectionTitleFamily: {
        'property': 'fontFamily',
        'container': '',
        'selector': ['.section h3']
    },
    ftSectionTitleStyle: {
        'property': 'fontStyle',
        'container': '',
        'selector': ['.section h3']
    },
    ftSectionTitleSize: {
        'property': 'font-size',
        'container': '',
        'selector': ['.section h3']
    },
    ftSectionTitleColor: {
        'property': 'color',
        'container': '',
        'selector': ['.section h3']
    },
    ftSectionTextFamily: {
        'property': 'fontFamily',
        'container': '',
        'selector': ['.section div']
    },
    ftSectionTextStyle: {
        'property': 'fontStyle',
        'container': '',
        'selector': ['.section div']
    },
    ftSectionTextSize: {
        'property': 'font-size',
        'container': '',
        'selector': ['.section div']
    },
    ftSectionTextColor: {
        'property': 'color',
        'container': '',
        'selector': ['.section div']
    },
    ftFieldTitleFamily: {
        'property': 'fontFamily',
        'container': '',
        'selector': ['label.desc', 'li div p']
    },
    ftFieldTitleStyle: {
        'property': 'fontStyle',
        'container': '',
        'selector': ['label.desc', 'li div p']
    },
    ftFieldTitleSize: {
        'property': 'font-size',
        'container': '',
        'selector': ['label.desc']
    },
    ftFieldTitleColor: {
        'property': 'color',
        'container': '',
        'selector': ['label.desc', 'li div p']
    },
    ftFieldTextFamily: {
        'property': 'fontFamily',
        'container': '',
        'selector': ['input.text', 'textarea.textarea', 'select.select', 'label.choice']
    },
    ftFieldTextStyle: {
        'property': 'fontStyle',
        'container': '',
        'selector': ['input.text', 'textarea.textarea', 'select.select', 'label.choice']
    },
    ftFieldTextSize: {
        'property': 'font-size',
        'container': '',
        'selector': ['input.text', 'textarea.textarea', 'select.select', 'label.choice']
    },
    ftFieldTextColor: {
        'property': 'color',
        'container': '',
        'selector': ['input.text', 'textarea.textarea', 'select.select', 'label.choice']
    },
    ftInstructFamily: {
        'property': 'fontFamily',
        'container': '',
        'selector': ['p.instruct']
    },
    ftInstructStyle: {
        'property': 'fontStyle',
        'container': '',
        'selector': ['p.instruct']
    },
    ftInstructSize: {
        'property': 'font-size',
        'container': '',
        'selector': ['p.instruct small']
    },
    ftInstructColor: {
        'property': 'color',
        'container': '',
        'selector': ['p.instruct small']
    },
    bgHtmlColor: {
        'property': 'background-color',
        'container': 'none',
        'selector': ['div#preview']
    },
    bgHtmlPattern: {
        'property': 'backgroundImage',
        'container': 'none',
        'selector': ['div#preview']
    },
    bgLogoColor: {
        'property': 'background-color',
        'container': '',
        'selector': ['h1#nav2']
    },
    bgLogoPattern: {
        'property': 'backgroundImage',
        'container': '',
        'selector': ['h1#nav2']
    },
    bgFormColor: {
        'property': 'background-color',
        'container': 'none',
        'selector': ['div#container2']
    },
    bgFieldColor: {
        'property': 'background',
        'container': '',
        'selector': ['input.text', 'textarea.textarea']
    },
    bgHighlightColor: {
        'property': 'background-color',
        'container': '',
        'selector': ['li.focused']
    },
    bgInstructColor: {
        'property': 'background-color',
        'container': '',
        'selector': ['p.instruct']
    },
    brFormThickness: {
        'property': 'border-width',
        'container': 'none',
        'selector': ['div#container2']
    },
    brFormStyle: {
        'property': 'borderStyle',
        'container': 'none',
        'selector': ['div#container2']
    },
    brFormColor: {
        'property': 'border-color',
        'container': 'none',
        'selector': ['div#container2']
    },
    brDividerThickness: {
        'property': 'border-width',
        'container': '',
        'selector': ['header.info']
    },
    brDividerStyle: {
        'property': 'borderBottomStyle',
        'container': '',
        'selector': ['header.info']
    },
    brDividerColor: {
        'property': 'border-color',
        'container': '',
        'selector': ['header.info']
    },
    brInstructThickness: {
        'property': 'border-width',
        'container': '',
        'selector': ['p.instruct']
    },
    brInstructStyle: {
        'property': 'borderStyle',
        'container': '',
        'selector': ['p.instruct']
    },
    brInstructColor: {
        'property': 'border-color',
        'container': '',
        'selector': ['p.instruct']
    },
    logoUrl: {
        'property': 'backgroundImage',
        'container': '',
        'selector': ['h1#nav2 a']
    },
    logoHeight: {
        'property': 'height',
        'container': '',
        'selector': ['h1#nav2 a']
    },
    applyTheme: function (theme, themeId) {
        this.applyCustomCSS(themeId);
        this.applyThemeProperties(theme, themeId);
    },
    applyCustomCSS: function (themeId) {
        var customCSS = this.themes.get('customCss', 'styleId', themeId);
        this.themes.customCSS = (customCSS.startsWith('http')) ? '' : customCSS;
        $('cssURL').value = (customCSS.startsWith('http') || customCSS.startsWith('//')) ? customCSS : 'http://';
    },
    applyThemeProperties: function (theme, themeId) {
        this.themes.properties.each(function (prop, index) {
            this.loadSelectedFont(prop, theme);
            this.apply(prop, theme[prop]);
        }.bind(this));
    },
    loadSelectedFont: function (prop, theme) {
        if (prop.startsWith('ft') && prop.endsWith('Style')) {
            var propFam = prop.replace('Style', 'Family');
            this.fontPicker.setFontObject(theme[propFam], theme[prop]);
        } else if (prop.startsWith('ft') && prop.endsWith('Family')) {
            var propStyle = prop.replace('Family', 'Style');
            this.fontPicker.setFontObject(theme[prop], theme[propStyle]);
        }
    },
    apply: function (property, value) {
        if (this[property]) {
            if (property == 'logoHeight') {
                if (value == '') value = '40px';
                else value += 'px';
                this.changeCSS('h1#nav a', 'minHeight', 0);
                this.applyChangesToCSS(this[property], value);
            } else if (property == 'logoUrl') {
                value = 'url(' + value + ')';
                this.applyChangesToCSS(this[property], value);
            } else if (property.startsWith('ft') && (property.endsWith('Family') || property.endsWith('Style'))) {
                this.applyFontProperty(property, value);
            } else {
                this.applyChangesToCSS(this[property], value);
            }
        }
    },
    applyChangesToCSS: function (style, value) {
        style.selector.each(function (num) {
            this.changeCSS(num, style.property, value, style.container);
        }.bind(this));
    },
    applyFontProperty: function (property, value) {
        if (property.endsWith('Family')) {
            fontName = value;
            style = this.fontPicker.fontObject.style;
        } else {
            fontName = this.fontPicker.fontObject.name;
            style = value;
        }
        var fontObject = this.fontPicker.getFontObject(fontName, style);
        var familyProperty = property.replace('Style', 'Family');
        var styleProperty = property.replace('Family', 'Style');
        var weightProperty = styleProperty.replace('Style', 'Weight');
        var style = this[familyProperty];
        style.selector.each(function (selectorValue) {
            
            this.changeCSS(selectorValue, 'fontFamily', fontObject.family, style.container);
            this.changeCSS(selectorValue, 'fontStyle', fontObject.style, style.container);
            this.changeCSS(selectorValue, 'fontWeight', fontObject.weight, style.container);
        }.bind(this));
    },
    changeCSS: function (selector, property, value, container) {
        var cssElements = (container != 'none') ? $$('#preview ' + selector) : $$(selector);
        for (var i = 0; i < cssElements.length; i++) {
            if (value == 'transparent' && (property == 'background' || property == 'background-color')) {
                cssElements[i].style.background = value;
            } else if (this.morphable.match(property) && value != 'transparent' && !this.themeDesigner.loading) {
                cssElements[i].morph(property + ':' + value, {
                    duration: .2
                });
            } else {
                cssElements[i].style[property] = value;
            }
        }
    }
});
var ThemeDuplicate = {
    createNewThemeName: function (themeName, themes) {
        var ctr = 1;
        var copyPattern = /Copy$/;
        var copyPlusNumberPattern = /Copy \d+$/;
        var newThemeName = themeName;
        while (true) {
            var endsInCopy = newThemeName.match(copyPattern);
            var endsInCopyPlusNumber = newThemeName.match(copyPlusNumberPattern);
            if (endsInCopy) {
                newThemeName += ' 2';
            } else if (endsInCopyPlusNumber) {
                var nameArray = newThemeName.split(' ');
                var firstHalfOfThemeName = '';
                for (var i = 0; i < nameArray.length - 1; i++) {
                    firstHalfOfThemeName += ' ' + nameArray[i];
                }
                var newCount = new Number(newThemeName.match(/\d+$/)) + 1;
                newThemeName = firstHalfOfThemeName.strip() + ' ' + newCount;
            } else {
                if (newThemeName.length > 40) {
                    newThemeName = newThemeName.substring(0, 30) + '...';
                }
                newThemeName += ' Copy';
            }
            if (this.isThemeNameUnique(newThemeName, themes)) {
                return newThemeName;
            }
        }
    },
    isThemeNameUnique: function (themeName, themes) {
        var nameIsUnique = true;
        for (var i = 0; i < themes.length; i++) {
            if (themeName == themes[i]['name']) {
                nameIsUnique = false;
                break;
            }
        }
        return nameIsUnique;
    }
};
var Themes = Class.create({
    properties: '',
    activeTheme: '',
    savedTheme: '',
    themeDesigner: '',
    themeMenu: '',
    engine: '',
    customCSS: '',
    collection: '', //<sl:translate>
    themeNameIsRequired: 'A name for your theme is required.',
    createNewTxt: 'Create New', //</sl:translate>
    initialize: function () {
        this.properties = Object.keys(__DEFAULT_THEME);
        
    },
    setCollection: function (themes) {
        this.collection = themes;
        for (var i = 0; i < this.collection.length; i++) {
            if (this.collection[i].styleId == 1) {
                this.collection[i].name = this.createNewTxt;
            }
            if (typeof (this.collection[i].integration) == 'string') {
                this.collection[i].integration = this.collection[i].integration.evalJSON();
            }
            
        }
    },
    save: function (themeName) {
        if (themeName.replace(/^\s*|\s*$/g, "") == '') {
            this.showSaveErrors(this.themeNameIsRequired);
        } else {
            Lightbox.close();
            this.savedTheme = this.generateThemeObject(themeName, 0);
            this.engine.saveTheme(this.savedTheme);
        }
    },
    showSaveErrors: function (error) {
        $('theme').up().addClassName('error');
        $('themeError').addClassName('error');
        $('themeError').innerHTML = error;
    },
    finishSaving: function (ret) {
        if (typeof (this.savedTheme.integration) == 'string') {
            this.savedTheme.integration = this.savedTheme.integration.evalJSON();
        }
        if (ret.response.isNewTheme == 'true') {
            this.savedTheme.styleId = ret.response.themeId;
            this.collection.push(this.savedTheme);
            var themeMenuEL = $('themeMenu');
            themeMenuEL.options[themeMenuEL.options.length] = new Option(this.savedTheme.name, this.savedTheme.styleId);
            themeMenuEL.options.selectedIndex = themeMenuEL.options.length - 1;
            this.themeMenu.showActionButtons(this.savedTheme.styleId);
        } else {
            this.collection.each(function (theme, index) {
                if (theme['styleId'] == ret.response.themeId) {
                    this.collection[index] = this.savedTheme;
                }
            }.bind(this));
        }
        if (this.preview_mode) this.loadPreview();
    },
    update: function (themeId, themeName) {
        this.savedTheme = this.generateThemeObject(themeName, themeId);
        this.engine.saveTheme(this.savedTheme);
    },
    generateThemeObject: function (themeName, themeId) {
        var obj = {
            'name': themeName,
            'css': this.generateCssString(),
            'styleId': themeId,
            'formStyle': '',
            'buttonType': this.getButtonType(),
            'buttonValue': this.getButtonValue(),
            'customCss': this.getCustomCSS(),
            'integration': this.getIntegration()
        }
        return obj;
    },
    generateCssString: function () {
        var css = '';
        this.properties.each(function (prop, index) {
            value = (this.activeTheme[prop]) ? this.activeTheme[prop] : '';
            css = css + value;
            if (index < this.properties.length - 1) css = css + ',';
        }.bind(this));
        
        return css;
    },
    getCustomCSS: function () {
        var value = $('cssURL').value;
        if (value == 'http://' && this.customCSS != '') {
            return this.customCSS;
        } else {
            return value;
        }
    },
    getIntegration: function () {
        return Object.toJSON(this.activeTheme['Integration']);
    },
    getButtonType: function () {
        if ($('butLink').checked) return 'image';
        else return 'text';
    },
    getButtonValue: function () {
        if ($('butLink').checked) return $F('buttonImage');
        else return $F('buttonText');
    },
    getThemeAsHash: function (themeId) {
        var themeValues = this.getThemeValues(themeId);
        var assocTheme = this.buildAssocTheme(themeValues);
        return assocTheme;
    },
    setActiveTheme: function (themeId) {
        this.activeTheme = this.getThemeAsHash(themeId);
        return this.activeTheme;
    },
    getThemeValues: function (themeId) {
        var themeValues = this.get('css', 'styleId', themeId).split(',');
        themeValues = this.addNewAttributesToLegacyThemes(themeValues);
        return themeValues;
    },
    addNewAttributesToLegacyThemes: function (theme) {
        if (theme.length < 49) {
            var index = 0;
            for (var attribute in __DEFAULT_THEME) {
                if (attribute.startsWith('ft') && attribute.endsWith('Style')) {
                    var value = __DEFAULT_THEME[attribute];
                    theme.splice(index, 0, value);
                }
                index += 1;
            }
        }
        return theme;
    },
    buildAssocTheme: function (themeValues) {
        var assocTheme = {};
        this.properties.each(function (prop, index) {
            assocTheme[prop] = (themeValues[index]) ? themeValues[index].strip() : '';
        }.bind(this));
        return assocTheme;
    },
    preview: function (themeId, name) {
        this.preview_mode = true;
        if (themeId == '1') {
            Lightbox.pageUrl('lightboxes/style-name.php');
        } else {
            this.update(themeId, name);
            Lightbox.close();
        }
    },
    loadPreview: function () {
        var themeId = this.themeDesigner.getSelectedThemeId();
        window.open("/themes/preview/" + themeId + "/", "Preview");
        this.preview_mode = false;
    },
    get: function (lookingFor, condition, value) {
        var theme = this.collection.find(function (currentTheme) {
            return currentTheme[condition] == value;
        });
        var result = theme[lookingFor];
        result = result || '';
        return result;
    }
});
var FontPicker = Class.create({
    engine: '',
    themes: '',
    preview: '',
    interface: '',
    themeDesigner: '',
    fontObject: '',
    typekitChangeHTML: '',
    typekitHTMLCache: [],
    fontPageCache: {
        'Standard': [],
        'LocalI': [],
        'LocalII': [],
        'Typekit': []
    },
    loadedFonts: ['inherit', 'Default', 'Arial', 'Courier New', 'Georgia', 'Palatino Linotype', 'Tahoma', 'Trebuchet MS', 'Times New Roman', 'Verdana', 'Arial Black', 'Comic Sans MS'],
    fontStyleOptionsHTML: '<label for="formatMenuDD" class="desc">Style</label>' + '<select size="7" class="select" id="formatMenuDD" name="formatMenuDD" ' + ' onchange="selectFormat($F(this))">#{options}</select>',
    changeTheme: function (themeId, themeAsHash) {
        this.fontObject = new Object();
        this.loadTypekitFontsForSelectedTheme(themeId);
        this.loadLocalFontsForSelectedTheme(themeAsHash);
    },
    selectTypography: function (formTypographyAttribute) {
        this.fontObject = this.getActiveThemeFontObject(formTypographyAttribute);
        this.updateFontBox(this.fontObject);
        this.updateStyleDropdown(this.fontObject);
        this.themeMenu.preselect(this.fontObject.altStyle, 'formatMenuDD');
        this.themeMenu.preselect(this.getActiveValue(formTypographyAttribute + 'Size'), 'sizeMenu');
        this.themeMenu.selectColor(this.getActiveValue(formTypographyAttribute + 'Color'));
        this.themeMenu.showTypographyMenu(formTypographyAttribute);
    },
    getActiveThemeFontObject: function (formTypographyAttribute) {
        var family = this.getActiveValue(formTypographyAttribute + 'Family');
        var style = this.getActiveValue(formTypographyAttribute + 'Style');
        return this.getFontObject(family, style);
    },
    updateStyleDropdown: function (fontObject) {
        var template = new Template(this.fontStyleOptionsHTML);
        var variables = {
            options: this.getFontStyleOptions(fontObject)
        };
        $('formatMenu').innerHTML = template.evaluate(variables);
    },
    getFontStyleOptions: function (fontObject) {
        var options = '';
        for (var i = 0; i < fontObject.styles.length; i++) {
            var style = fontObject.styles[i];
            var selected = this.getStyleSelectedHTML(style, fontObject.altStyle);
            options += '<option value="' + style.capitalize() + '" '
            options += selected + '>' + this.getFormattedStyle(style) + '</option>';
        }
        return options;
    },
    getFormattedStyle: function (rawStyle) {
        var style = '';
        var styleArray = rawStyle.split('_');
        for (var i = 0; i < styleArray.length; i++) {
            style += ' ' + styleArray[i].capitalize();
        }
        return style.strip();
    },
    getStyleSelectedHTML: function (style, selectedStyle) {
        return (style.equals(selectedStyle)) ? 'selected="selected"' : '';
    },
    showFontPickerLB: function () {
        var family = this.getSelectedPropValue('Family');
        var style = this.getSelectedPropValue('Style');
        var fontObject = this.getFontObject(family, style);
        this.loadLocalFonts(fontObject.type, fontObject.page);
        Lightbox.showUrl(this.getFontPickerLBURL(fontObject), this.postShowFontPickerLB.bind(this), 'modal');
    },
    postShowFontPickerLB: function () {
        if (Object.isEmpty(this.getCachedPageHTML('Typekit'))) {
            var integration = this.getActiveValue('Integration');
            if (!Object.isEmpty(integration)) {
                this.engine.getKitHTML(integration, this.successfulGetKitHTML.bind(this));
            }
        } else {
            this.addTypekitHTML(this.getCachedPageHTML('Typekit'));
        }
        if (this.fontObject.type.equals('Typekit')) {
            this.showTypekitFonts();
        }
        if (Prototype.Browser.IE) {
            if ($('getKitDetailsBtn')) {
                $('getKitDetailsBtn').observe('click', this.getKit.bind(this));
                $('TypekitForm').onsubmit = function () {
                    return false;
                };
            }
        } else {
            if ($('TypekitForm')) {
                $('TypekitForm').observe('submit', this.getKit.bind(this));
            }
        }
        this.highlightSelectedFont(this.fontObject);
    },
    successfulGetKitHTML: function (ret) {
        this.addTypekitHTML(ret.response.html);
        this.highlightSelectedFont(this.fontObject);
    },
    getFontPickerLBURL: function (fontObject) {
        var URL = 'lightboxes/Theme.Font.php?'
        URL += 'font=' + fontObject.name;
        URL += '&page=' + fontObject.page;
        URL += '&style=' + fontObject.style;
        URL += '&fontPickerClassname=' + fontObject.fontPickerClassname;
        return URL;
    },
    highlightSelectedFont: function (fontObject) {
        this.interface.clearPickedFonts();
        $$('#' + fontObject.type + 'Fonts li h3').find(function (el) {
            if (el.innerHTML.equals(fontObject.name)) {
                this.interface.pickFont(el.up());
                return true;
            }
        }.bind(this));
    },
    selectFontGroup: function (type, fontPickerClassname) {
        if (!$('fontPicker').hasClassName(fontPickerClassname)) {
            $('fontPicker').className = this.getNextFontPickerClassname(type, fontPickerClassname);
            this.fetchPage(type, 1);
        }
    },
    getNextFontPickerClassname: function (type, fontPickerClassname) {
        if (!Object.isEmpty(this.getActiveKitId()) && fontPickerClassname == 'typFonts showKitForm') {
            fontPickerClassname = 'typFonts showKitFonts';
        }
        return fontPickerClassname;
    },
    updateFontBox: function (fontObject) {
        $('selectFontType').innerHTML = fontObject.name;
        $('selectFontExample').style.fontFamily = fontObject.family;
        $('selectFontExample').style.fontWeight = fontObject.weight;
        $('selectFontExample').style.fontStyle = fontObject.style;
    },
    pickFont: function (el) {
        this.fontObject = this.getFontObject(el.down().innerHTML);
    },
    saveFont: function () {
        this.setActiveValue(this.themeDesigner.selectedProperty + 'Family', this.fontObject.name);
        this.setActiveValue(this.themeDesigner.selectedProperty + 'Style', this.fontObject.altStyle);
        this.updateFontBox(this.fontObject);
        this.updateStyleDropdown(this.fontObject);
        this.preview.apply(this.themeDesigner.selectedProperty + 'Family', this.fontObject.name);
    },
    cancelSaveFont: function () {
        this.fontObject = this.getFontObject(this.getSelectedPropValue('Family'), this.getSelectedPropValue('Style'));
    },
    fetchPage: function (type, page) {
        var pageHTML = this.getCachedPageHTML(type, page);
        if (Object.isEmpty(pageHTML)) {
            if (type != 'Typekit') {
                this.loadLocalFonts(type, page);
                this.engine.fetchPage(type, page, this.fontObject.family);
            } else {
                var integration = this.getActiveValue('Integration');
                if (!Object.isEmpty(integration)) {
                    this.engine.getKitHTML(this.getActiveValue('Integration'), this.successfulGetExistingKitHTML.bind(this));
                }
            }
        } else {
            this.showPage(type, page, pageHTML);
            this.highlightSelectedFont(this.fontObject);
        }
    },
    getCachedPageHTML: function (type, page) {
        if (type == 'Typekit') {
            return this.fontPageCache[type][this.getActiveKitId()];
        } else {
            return this.fontPageCache[type][page];
        }
    },
    setCachedPageHTML: function (type, page, html) {
        if (type == 'Typekit') {
            this.fontPageCache[type][this.getActiveKitId()] = this.typekitChangeHTML + html;
        } else {
            this.fontPageCache[type][page] = html;
        }
    },
    getActiveKitId: function () {
        var kitId = '';
        if (this.themes.activeTheme.Integration) {
            kitId = this.themes.activeTheme.Integration.Typekit.KitId;
        }
        return kitId;
    },
    successfulGetExistingKitHTML: function (ret) {
        this.successfulGetKitHTML(ret);
        this.showTypekitFonts();
    },
    successfulFetchPage: function (ret) {
        this.setCachedPageHTML(ret.response.type, ret.response.page, ret.response.html);
        this.showPage(ret.response.type, ret.response.page, ret.response.html)
    },
    showPage: function (type, page, html) {
        if (type == 'Typekit') {
            this.showTypekitFonts();
        } else {
            $(type + 'Fonts').innerHTML = html;
            $$('#' + type + 'FontPaging a').each(function (link, index) {
                if (page == (index + 1)) {
                    link.addClassName('selected');
                } else {
                    link.removeClassName('selected');
                }
            });
        }
    },
    getFontObject: function (fontName, style) {
        var details = this.getFontDetailsObject(this.getProperName(fontName));
        var fontObject = {
            'name': details.Name,
            'type': details.Type,
            'family': details.FontFamily,
            'page': details.Page,
            'group': details.Group,
            'styles': details.Styles,
            'fontPickerClassname': this.getFontPickerClassname(details.Type)
        };
        this.setStyleAndWeight(style, fontObject);
        return fontObject;
    },
    getFontDetailsObject: function (fontName) {
        var fontDetails;
        ['Standard', 'Typekit', 'LocalI', 'LocalII'].find(function (type) {
            fontDetails = this.getFontDetails(fontName, type);
            if (fontDetails) return true;
        }.bind(this));
        if (!fontDetails) {
            fontDetails = __FONTS['Standard']['Default'];
        }
        return fontDetails;
    },
    getProperName: function (fontName) {
        return (fontName == 'inherit') ? 'Default' : fontName;
    },
    getFontDetails: function (fontName, type) {
        var fontDetails = false;
        var fonts = __FONTS[type];
        for (var name in fonts) {
            if (fontName.equals(name)) {
                fontDetails = fonts[name];
                fontDetails.Name = name;
                fontDetails.Type = type;
                break;
            }
        }
        return fontDetails;
    },
    getFontPickerClassname: function (type) {
        var fontTypeToClassNames = {
            'Standard': 'deFonts',
            'LocalI': 'lclFonts1',
            'LocalII': 'lclFonts2',
            'Typekit': 'typFonts showKitForm'
        };
        return fontTypeToClassNames[type];
    },
    getStyleFromFamily: function (family, styles) {
        var familyArray = family.split(',');
        var cleanFont = familyArray[0];
        cleanFont = this.removeParens(cleanFont);
        cleanFont = cleanFont.strip();
        var cleanFontArray = cleanFont.split('-');
        var style = (Object.isEmpty(cleanFontArray[1])) ? '' : cleanFontArray[1];
        style = (style.equals('')) ? styles[0] : style;
        return style.toLowerCase();
    },
    removeParens: function (text) {
        text = text.gsub("'", "");
        text = text.gsub('"', '');
        return text;
    },
    setStyleAndWeight: function (style, fontObject) {
        style = style || '';
        style = (style.equals('')) ? 'normal' : style;
        style = style.toLowerCase().strip();
        if (fontObject.type == 'Standard') {
            if (style != 'normal' && style != 'italic' && style != 'bold') style = 'normal';
            fontObject.style = (style == 'bold') ? 'normal' : style;
            fontObject.weight = (style == 'bold') ? 'bold' : 'normal';
            fontObject.altStyle = (style == 'bold') ? 'bold' : fontObject.style;
        } else if (fontObject.type == 'LocalI' || fontObject.type == 'LocalII') {
            style = (style == 'normal') ? '' : style;
            if (Object.isEmpty(style)) {
                style = this.getStyleFromFamily(fontObject.family, fontObject.styles);
            }
            var altStyle;
            if (style != 'normal') {
                fontObject.family = "'" + fontObject.name + '-' + style.capitalize() + "'";
                fontObject.altStyle = style.capitalize();
            } else {
                fontObject.altStyle = 'normal';
            }
            fontObject.style = 'normal';
            fontObject.weight = 'normal';
        } else if (fontObject.type == 'Typekit') {
            if (Object.isEmpty(style) || style.equals('normal')) {
                var styleArray = fontObject.styles[0].split(' ');
                fontObject.style = styleArray[0];
                fontObject.weight = styleArray[1];
                fontObject.altStyle = fontObject.styles[0];
            } else {
                var styleArray = style.split(' ');
                fontObject.style = styleArray[0];
                fontObject.weight = styleArray[1];
                fontObject.altStyle = style;
            }
        }
    },
    getKit: function (event) {
        Event.stop(event);
        this.showTypekitFonts();
        this.engine.getKit($('kitid').value, this.successfulGetKit.bind(this));
    },
    successfulGetKit: function (ret) {
        this.addTypekitHTML(ret.response.html);
        this.addThemeTypekitValues(ret.response.kit.evalJSON());
    },
    changeKit: function () {
        $('TypekitFonts').innerHTML = '<li class="loader"><img src="/images/loader2.gif" alt="Loading..." /></li>';
        this.showTypekitForm();
    },
    showTypekitForm: function () {
        $('fontPicker').className = 'typFonts showKitForm';
        document.TypekitForm.kitid.focus();
    },
    showTypekitFonts: function () {
        $('fontPicker').className = 'typFonts showKitFonts';
    },
    addThemeTypekitValues: function (integration) {
        this.cleanupTypekitFontNames(integration);
        this.removeExistingTypekitFonts(__FONTS['Typekit'], integration.Typekit.fonts);
        this.addTypekitJS(integration.Typekit.KitId);
        this.addTypekitFontsToLoadedFonts(integration);
        __FONTS['Typekit'] = integration.Typekit.fonts;
        this.themes.activeTheme.Integration = integration;
    },
    cleanupTypekitFontNames: function (integration) {
        var cleanFonts = {};
        for (var font in integration.Typekit.fonts) {
            cleanFonts[font.strip()] = integration.Typekit.fonts[font];
        }
        integration.Typekit.fonts = cleanFonts;
    },
    removeExistingTypekitFonts: function (existingTypekitFonts, newTypekitFonts) {
        if (!Object.isEmpty(existingTypekitFonts)) {
            var keys = $H(existingTypekitFonts).keys();
            var newKeys = $H(newTypekitFonts).keys();
            for (var i = 0; i < keys.length; i++) {
                this.loadedFonts = this.loadedFonts.without(keys[i]);
            }
            for (var prop in this.themes.activeTheme) {
                if (prop.startsWith('ft') && prop.endsWith('Family')) {
                    var value = this.themes.activeTheme[prop];
                    if (keys.indexOf(value) > -1 && newKeys.indexOf(value) == -1) {
                        this.setActiveValue(prop, 'inherit');
                        var styleProp = prop.replace('Family', 'Style');
                        this.setActiveValue(styleProp, 'normal');
                    }
                }
            }
        }
    },
    addTypekitFontsToLoadedFonts: function (integration) {
        var fonts = integration['Typekit']['fonts'];
        for (var f in fonts) {
            this.loadedFonts.push(f);
        }
    },
    addTypekitHTML: function (html) {
        this.setCachedPageHTML('Typekit', '', html);
        $('TypekitFonts').innerHTML = this.getCachedPageHTML('Typekit');
    },
    loadTypekitFontsForSelectedTheme: function (themeId) {
        var integration = this.themes.get('integration', 'styleId', themeId);
        if (!Object.isEmpty(integration)) {
            this.addThemeTypekitValues(integration);
        }
    },
    loadLocalFontsForSelectedTheme: function (assocTheme) {
        var fontsToLoad = new Array();
        this.themes.properties.each(function (prop, index) {
            if (prop.startsWith('ft') && prop.endsWith('Family')) {
                fontsToLoad.push(assocTheme[prop]);
            }
        }.bind(this));
        if (Prototype.Browser.IE) {
            this.loadNecessaryFonts(fontsToLoad, true);
        }
    },
    loadLocalFonts: function (type, page) {
        if (type == 'LocalI' || type == 'LocalII') {
            if (Prototype.Browser.IE) {
                var fontsToLoad = this.getFontsOnPage(type, page);
                this.loadNecessaryFonts(fontsToLoad, false);
            }
        }
    },
    loadKit: function (kitId) {
        this.addTypekitJS(kitId);
    },
    addTypekitJS: function (kitId) {
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = document.location.protocol + '//use.typekit.com/' + kitId + '.js';
        head.appendChild(script);
        script.onreadystatechange = function () {
            if (document.readyState == 'complete') {
                try {
                    Typekit.load();
                } catch (e) {}
            }
        }.bind(this);
        script.onload = function () {
            try {
                Typekit.load();
            } catch (e) {}
        }
    },
    getFontsOnPage: function (type, page) {
        var fontsOnPage = new Array();
        for (var font in __FONTS[type]) {
            if (page == __FONTS[type][font]['Page']) {
                fontsOnPage.push(font);
            }
        }
        return fontsOnPage;
    },
    loadNecessaryFonts: function (fontsToLoad, fontsFromMultiplePages) {
        var fontsOnAllPages = fontsToLoad || [];
        if (fontsFromMultiplePages) {
            for (var i = 0; i < fontsToLoad.length; i++) {
                var fontObject = this.getFontObject(fontsToLoad[i]);
                fontsOnAllPages = fontsToLoad.concat(this.getFontsOnPage(fontObject.type, fontObject.page));
            }
            fontsOnAllPages = fontsOnAllPages.uniq();
        }
        for (var i = 0; i < this.loadedFonts.length; i++) {
            fontsOnAllPages = fontsOnAllPages.without(this.loadedFonts[i]);
        }
        if (fontsOnAllPages.length > 0) {
            this.engine.loadLocalFonts(fontsOnAllPages);
        }
    },
    successfulLoadLocalFonts: function (ret) {
        var style = '';
        for (var font in ret.response) {
            this.loadedFonts.push(font);
            style += ret.response[font];
        }
        this.addStyleToIE(style);
    },
    addStyleToIE: function (style) {
        var styleElement = styleElement = document.createStyleSheet();
        styleElement.cssText = style;
    },
    setFontObject: function (fontName, style) {
        this.fontObject = this.getFontObject(fontName, style);
    },
    setFontObjectStyle: function (style) {
        this.fontObject = this.getFontObject(this.fontObject.name, style);
    },
    getSelectedPropValue: function (key) {
        return this.getActiveValue(this.themeDesigner.selectedProperty + key);
    },
    getActiveValue: function (key) {
        return this.themes.activeTheme[key];
    },
    setActiveValue: function (key, value) {
        this.themes.activeTheme[key] = value;
    }
});
var ThemeDesigner = Class.create(Base, {
    loading: false,
    selectedProperty: '',
    engine: '',
    themes: '',
    preview: '',
    themeMenu: '',
    interface: '',
    fontPicker: '', //<sl:translate>
    areYouSureYouWantToDelete: 'Are you sure that you want to delete the \"#{themeNameKey}\" theme?', //</sl:translate>
    initialize: function () {
        this.loading = true;
        this._initBase();
        this._wireObjects();
        this._wireEvents();
        this.engine.loadThemes();
        this.interface.hideStatus();
    },
    _wireObjects: function () {
        this.interface = new Interface();
        this.engine = new ThemeDesignerEngine();
        this.preview = new ThemePreview();
        this.themeMenu = new ThemeMenu();
        this.themes = new Themes();
        this.fontPicker = new FontPicker();
        this.preview.themeDesigner = this;
        this.preview.themes = this.themes;
        this.preview.fontPicker = this.fontPicker;
        this.themeMenu.themeDesigner = this;
        this.themeMenu.themes = this.themes;
        this.themeMenu.preview = this.preview;
        this.themes.themeDesigner = this;
        this.themes.engine = this.engine;
        this.themes.themeMenu = this.themeMenu;
        this.fontPicker.themeDesigner = this;
        this.fontPicker.themes = this.themes;
        this.fontPicker.preview = this.preview;
        this.fontPicker.themeMenu = this.themeMenu;
        this.fontPicker.interface = this.interface;
        this.fontPicker.engine = this.engine;
    },
    _wireEvents: function () {
        Event.observe($('buttonText'), 'keyup', this.updateButton.bind(this), false);
        Event.observe($('buttonImage'), 'keyup', this.updateButton.bind(this), false);
        Event.observe($('buttonText'), 'blur', this.updateButton.bind(this), false);
        Event.observe($('buttonImage'), 'blur', this.updateButton.bind(this), false);
    },
    successfulLoadThemes: function (ret) {
        this.themes.setCollection(ret.response.themes.evalJSON());
        this.themeMenu.loadThemeMenu(this.themes.collection);
        this.selectDefaultTheme();
        this.loading = false;
    },
    selectDefaultTheme: function () {
        var selectThemeId = (__SELECTED_THEME_ID > 0) ? __SELECTED_THEME_ID : 1;
        Element.selectOption($('themeMenu'), selectThemeId);
        this.selectTheme(selectThemeId);
    },
    selectTheme: function (themeId) {
        this.themes.setActiveTheme(themeId);
        this.themeMenu.selectTheme(themeId);
        this.fontPicker.changeTheme(themeId, this.themes.activeTheme);
        this.preview.applyTheme(this.themes.activeTheme, themeId);
    },
    successfulLoadLocalFonts: function (ret) {
        this.fontPicker.successfulLoadLocalFonts(ret);
    },
    saveNewTheme: function (themeName) {
        this.themes.save(themeName);
    },
    saveTheme: function () {
        var themeId = this.getSelectedThemeId();
        if (themeId != '1') {
            this.updateTheme(themeId);
        } else {
            Lightbox.showUrl('lightboxes/style-name.php');
        }
    },
    selectProperties: function (num) {
        this.themeMenu.select(num);
    },
    selectTypography: function (str) {
        this.selectedProperty = str;
        this.fontPicker.selectTypography(str);
    },
    selectBackgrounds: function (str) {
        this.selectedProperty = str;
        this.themeMenu.selectColor(this.themes.activeTheme[str + 'Color']);
        this.themeMenu.backgrounds(str);
    },
    selectBorders: function (str) {
        this.themeMenu.preselect(this.themes.activeTheme[str + 'Thickness'], 'thickMenu');
        this.themeMenu.preselect(this.themes.activeTheme[str + 'Style'], 'styleMenu');
        this.selectedProperty = str;
        this.themeMenu.selectColor(this.themes.activeTheme[str + 'Color']);
        this.themeMenu.borders(str);
    },
    selectFont: function (str) {
        this.themes.activeTheme[this.selectedProperty + 'Family'] = str;
        this.preview.apply(this.selectedProperty + 'Family', str);
    },
    selectFormat: function (str) {
        this.fontPicker.setFontObjectStyle(str);
        this.themes.activeTheme[this.selectedProperty + 'Style'] = str;
        this.preview.apply(this.selectedProperty + 'Style', str);
        this.fontPicker.updateFontBox(this.fontPicker.fontObject);
    },
    selectSize: function (str) {
        this.themes.activeTheme[this.selectedProperty + 'Size'] = str;
        this.preview.apply(this.selectedProperty + 'Size', str);
    },
    selectThickness: function (str) {
        this.themes.activeTheme[this.selectedProperty + 'Thickness'] = str;
        this.preview.apply(this.selectedProperty + 'Thickness', str);
    },
    selectStyle: function (str) {
        this.themes.activeTheme[this.selectedProperty + 'Style'] = str;
        this.preview.apply(this.selectedProperty + 'Style', str);
    },
    selectShadow: function (str) {
        this.selectedProperty = 'dropShadow';
        this.themes.activeTheme[this.selectedProperty] = str;
        this.preview.apply(this.selectedProperty, str);
        $('container2').toggleClassName('noshadow');
    },
    typeColor: function (string) {
        this.themeMenu.typeColor(string);
    },
    selectCustom: function () {
        this.themeMenu.selectCustom();
    },
    selectLogo: function (str, beenSaved) {
        this.themeMenu.selectLogo(str, beenSaved);
    },
    changeButtonType: function () {
        this.themeMenu.changeButtonType();
    },
    updateButton: function () {
        this.themeMenu.updateButton();
    },
    deleteTheme: function () {
        var themeId = this.getSelectedThemeId();
        var themeName = this.getSelectedThemeName();
        var tpl = new Template(this.areYouSureYouWantToDelete);
        var msg = tpl.evaluate({
            themeNameKey: themeName
        });
        msg = msg.replace(/&quot;/g, '"');
        if (confirm(msg)) {
            this.engine.deleteTheme(themeId, themeName);
        }
    },
    successfulDeleteTheme: function (ret) {
        $('themeMenu').options[$('themeMenu').selectedIndex] = null;
        this.selectTheme(1);
    },
    renameTheme: function (theme) {
        Lightbox.showUrl('lightboxes/style-rename.php', function () {
            $('themeName').value = this.getSelectedThemeName();
            $('themeId').value = this.getSelectedThemeId();
        });
    },
    saveRename: function (themeName, themeId) {
        var oldName = this.getSelectedThemeName();
        this.engine.renameTheme(themeId, themeName, oldName);
        this.getSelectedOption().innerHTML = themeName;
        Lightbox.close();
    },
    successfulRenameTheme: function (ret) {},
    successfulSave: function (ret) {
        this.themes.finishSaving(ret);
    },
    previewTheme: function () {
        this.themes.preview(this.getSelectedThemeId(), this.getSelectedThemeName());
    },
    updateTheme: function (themeId) {
        var themeName = this.getSelectedThemeName();
        this.themes.update(themeId, themeName);
    },
    updateCustomCSS: function (value) {
        this.themes.customCSS = value;
        $('cssURL').value = 'http://';
    },
    finishLoadingCustomCSS: function () {
        $('customCSS').value = this.themes.customCSS;
    },
    duplicateTheme: function () {
        var name = this.getSelectedThemeName();
        var newThemeName = ThemeDuplicate.createNewThemeName(name, this.themes.collection);
        this.themes.save(newThemeName);
    },
    getSelectedThemeName: function () {
        return this.getSelectedOption().innerHTML;
    },
    getSelectedThemeId: function () {
        return this.getSelectedOption().value;
    },
    getSelectedOption: function () {
        return $('themeMenu').options[$('themeMenu').selectedIndex];
    },
    showFontPickerLB: function () {
        this.fontPicker.showFontPickerLB();
    },
    selectFontGroup: function (type, fontPickerClassname) {
        this.fontPicker.selectFontGroup(type, fontPickerClassname);
    },
    cancelSaveFont: function () {
        this.fontPicker.cancelSaveFont();
    },
    pickFont: function (el) {
        el = $(el);
        this.interface.pickFont(el);
        this.fontPicker.pickFont(el);
    },
    saveFont: function () {
        this.fontPicker.saveFont();
    },
    fetchPage: function (type, page) {
        this.fontPicker.fetchPage(type, page);
    },
    successfulFetchPage: function (ret) {
        this.fontPicker.successfulFetchPage(ret);
    },
    rotateText: function () {
        this.interface.rotateText();
    },
    changeKit: function () {
        this.fontPicker.changeKit();
    }
});
Event.observe(window, 'load', function () {
    new ThemeDesigner();
});