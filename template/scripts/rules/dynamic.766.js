Calendar = function (firstDayOfWeek, dateStr, onSelected, onClose) {
    this.activeDiv = null;
    this.currentDateEl = null;
    this.getDateStatus = null;
    this.getDateToolTip = null;
    this.getDateText = null;
    this.timeout = null;
    this.onSelected = onSelected || null;
    this.onClose = onClose || null;
    this.dragging = false;
    this.hidden = false;
    this.minYear = 1970;
    this.maxYear = 2050;
    this.dateFormat = Calendar._TT["DEF_DATE_FORMAT"];
    this.ttDateFormat = Calendar._TT["TT_DATE_FORMAT"];
    this.isPopup = true;
    this.weekNumbers = true;
    this.firstDayOfWeek = typeof firstDayOfWeek == "number" ? firstDayOfWeek : Calendar._FD;
    this.showsOtherMonths = false;
    this.dateStr = dateStr;
    this.ar_days = null;
    this.showsTime = false;
    this.time24 = true;
    this.yearStep = 2;
    this.hiliteToday = true;
    this.multiple = null;
    this.table = null;
    this.element = null;
    this.tbody = null;
    this.firstdayname = null;
    this.monthsCombo = null;
    this.yearsCombo = null;
    this.hilitedMonth = null;
    this.activeMonth = null;
    this.hilitedYear = null;
    this.activeYear = null;
    this.dateClicked = false;
    if (typeof Calendar._SDN == "undefined") {
        if (typeof Calendar._SDN_len == "undefined") Calendar._SDN_len = 3;
        var ar = new Array();
        for (var i = 8; i > 0;) {
            ar[--i] = Calendar._DN[i].substr(0, Calendar._SDN_len);
        }
        Calendar._SDN = ar;
        if (typeof Calendar._SMN_len == "undefined") Calendar._SMN_len = 3;
        ar = new Array();
        for (var i = 12; i > 0;) {
            ar[--i] = Calendar._MN[i].substr(0, Calendar._SMN_len);
        }
        Calendar._SMN = ar;
    }
};
Calendar._C = null;
Calendar.is_ie = (/msie/i.test(navigator.userAgent) && !/opera/i.test(navigator.userAgent));
Calendar.is_ie5 = (Calendar.is_ie && /msie 5\.0/i.test(navigator.userAgent));
Calendar.is_opera = /opera/i.test(navigator.userAgent);
Calendar.is_khtml = /Konqueror|Safari|KHTML/i.test(navigator.userAgent);
Calendar.getAbsolutePos = function (el) {
    var SL = 0,
        ST = 0;
    var is_div = /^div$/i.test(el.tagName);
    if (is_div && el.scrollLeft) SL = el.scrollLeft;
    if (is_div && el.scrollTop) ST = el.scrollTop;
    var r = {
        x: el.offsetLeft - SL,
        y: el.offsetTop - ST
    };
    if (el.offsetParent) {
        var tmp = this.getAbsolutePos(el.offsetParent);
        r.x += tmp.x;
        r.y += tmp.y;
    }
    return r;
};
Calendar.isRelated = function (el, evt) {
    var related = evt.relatedTarget;
    if (!related) {
        var type = evt.type;
        if (type == "mouseover") {
            related = evt.fromElement;
        } else if (type == "mouseout") {
            related = evt.toElement;
        }
    }
    while (related) {
        if (related == el) {
            return true;
        }
        related = related.parentNode;
    }
    return false;
};
Calendar.removeClass = function (el, className) {
    if (!(el && el.className)) {
        return;
    }
    var cls = el.className.split(" ");
    var ar = new Array();
    for (var i = cls.length; i > 0;) {
        if (cls[--i] != className) {
            ar[ar.length] = cls[i];
        }
    }
    el.className = ar.join(" ");
};
Calendar.addClass = function (el, className) {
    Calendar.removeClass(el, className);
    el.className += " " + className;
};
Calendar.getElement = function (ev) {
    var f = Calendar.is_ie ? window.event.srcElement : ev.currentTarget;
    while (f.nodeType != 1 || /^div$/i.test(f.tagName))
    f = f.parentNode;
    return f;
};
Calendar.getTargetElement = function (ev) {
    var f = Calendar.is_ie ? window.event.srcElement : ev.target;
    while (f.nodeType != 1)
    f = f.parentNode;
    return f;
};
Calendar.stopEvent = function (ev) {
    ev || (ev = window.event);
    if (Calendar.is_ie) {
        ev.cancelBubble = true;
        ev.returnValue = false;
    } else {
        ev.preventDefault();
        ev.stopPropagation();
    }
    return false;
};
Calendar.addEvent = function (el, evname, func) {
    if (el.attachEvent) {
        el.attachEvent("on" + evname, func);
    } else if (el.addEventListener) {
        el.addEventListener(evname, func, true);
    } else {
        el["on" + evname] = func;
    }
};
Calendar.removeEvent = function (el, evname, func) {
    if (el.detachEvent) {
        el.detachEvent("on" + evname, func);
    } else if (el.removeEventListener) {
        el.removeEventListener(evname, func, true);
    } else {
        el["on" + evname] = null;
    }
};
Calendar.createElement = function (type, parent) {
    var el = null;
    if (document.createElementNS) {
        el = document.createElementNS("http://www.w3.org/1999/xhtml", type);
    } else {
        el = document.createElement(type);
    }
    if (typeof parent != "undefined") {
        parent.appendChild(el);
    }
    return el;
};
Calendar._add_evs = function (el) {
    with(Calendar) {
        addEvent(el, "mouseover", dayMouseOver);
        addEvent(el, "mousedown", dayMouseDown);
        addEvent(el, "mouseout", dayMouseOut);
        if (is_ie) {
            addEvent(el, "dblclick", dayMouseDblClick);
            el.setAttribute("unselectable", true);
        }
    }
};
Calendar.findMonth = function (el) {
    if (typeof el.month != "undefined") {
        return el;
    } else if (typeof el.parentNode.month != "undefined") {
        return el.parentNode;
    }
    return null;
};
Calendar.findYear = function (el) {
    if (typeof el.year != "undefined") {
        return el;
    } else if (typeof el.parentNode.year != "undefined") {
        return el.parentNode;
    }
    return null;
};
Calendar.showMonthsCombo = function () {
    var cal = Calendar._C;
    if (!cal) {
        return false;
    }
    var cal = cal;
    var cd = cal.activeDiv;
    var mc = cal.monthsCombo;
    if (cal.hilitedMonth) {
        Calendar.removeClass(cal.hilitedMonth, "hilite");
    }
    if (cal.activeMonth) {
        Calendar.removeClass(cal.activeMonth, "active");
    }
    var mon = cal.monthsCombo.getElementsByTagName("div")[cal.date.getMonth()];
    Calendar.addClass(mon, "active");
    cal.activeMonth = mon;
    var s = mc.style;
    s.display = "block";
    if (cd.navtype < 0) s.left = cd.offsetLeft + "px";
    else {
        var mcw = mc.offsetWidth;
        if (typeof mcw == "undefined") mcw = 50;
        s.left = (cd.offsetLeft + cd.offsetWidth - mcw) + "px";
    }
    s.top = (cd.offsetTop + cd.offsetHeight) + "px";
};
Calendar.showYearsCombo = function (fwd) {
    var cal = Calendar._C;
    if (!cal) {
        return false;
    }
    var cal = cal;
    var cd = cal.activeDiv;
    var yc = cal.yearsCombo;
    if (cal.hilitedYear) {
        Calendar.removeClass(cal.hilitedYear, "hilite");
    }
    if (cal.activeYear) {
        Calendar.removeClass(cal.activeYear, "active");
    }
    cal.activeYear = null;
    var Y = cal.date.getFullYear() + (fwd ? 1 : -1);
    var yr = yc.firstChild;
    var show = false;
    for (var i = 12; i > 0; --i) {
        if (Y >= cal.minYear && Y <= cal.maxYear) {
            yr.innerHTML = Y;
            yr.year = Y;
            yr.style.display = "block";
            show = true;
        } else {
            yr.style.display = "none";
        }
        yr = yr.nextSibling;
        Y += fwd ? cal.yearStep : -cal.yearStep;
    }
    if (show) {
        var s = yc.style;
        s.display = "block";
        if (cd.navtype < 0) s.left = cd.offsetLeft + "px";
        else {
            var ycw = yc.offsetWidth;
            if (typeof ycw == "undefined") ycw = 50;
            s.left = (cd.offsetLeft + cd.offsetWidth - ycw) + "px";
        }
        s.top = (cd.offsetTop + cd.offsetHeight) + "px";
    }
};
Calendar.tableMouseUp = function (ev) {
    var cal = Calendar._C;
    if (!cal) {
        return false;
    }
    if (cal.timeout) {
        clearTimeout(cal.timeout);
    }
    var el = cal.activeDiv;
    if (!el) {
        return false;
    }
    var target = Calendar.getTargetElement(ev);
    ev || (ev = window.event);
    Calendar.removeClass(el, "active");
    if (target == el || target.parentNode == el) {
        Calendar.cellClick(el, ev);
    }
    var mon = Calendar.findMonth(target);
    var date = null;
    if (mon) {
        date = new Date(cal.date);
        if (mon.month != date.getMonth()) {
            date.setMonth(mon.month);
            cal.setDate(date);
            cal.dateClicked = false;
            cal.callHandler();
        }
    } else {
        var year = Calendar.findYear(target);
        if (year) {
            date = new Date(cal.date);
            if (year.year != date.getFullYear()) {
                date.setFullYear(year.year);
                cal.setDate(date);
                cal.dateClicked = false;
                cal.callHandler();
            }
        }
    }
    with(Calendar) {
        removeEvent(document, "mouseup", tableMouseUp);
        removeEvent(document, "mouseover", tableMouseOver);
        removeEvent(document, "mousemove", tableMouseOver);
        cal._hideCombos();
        _C = null;
        return stopEvent(ev);
    }
};
Calendar.tableMouseOver = function (ev) {
    var cal = Calendar._C;
    if (!cal) {
        return;
    }
    var el = cal.activeDiv;
    var target = Calendar.getTargetElement(ev);
    if (target == el || target.parentNode == el) {
        Calendar.addClass(el, "hilite active");
        Calendar.addClass(el.parentNode, "rowhilite");
    } else {
        if (typeof el.navtype == "undefined" || (el.navtype != 50 && (el.navtype == 0 || Math.abs(el.navtype) > 2))) Calendar.removeClass(el, "active");
        Calendar.removeClass(el, "hilite");
        Calendar.removeClass(el.parentNode, "rowhilite");
    }
    ev || (ev = window.event);
    if (el.navtype == 50 && target != el) {
        var pos = Calendar.getAbsolutePos(el);
        var w = el.offsetWidth;
        var x = ev.clientX;
        var dx;
        var decrease = true;
        if (x > pos.x + w) {
            dx = x - pos.x - w;
            decrease = false;
        } else dx = pos.x - x;
        if (dx < 0) dx = 0;
        var range = el._range;
        var current = el._current;
        var count = Math.floor(dx / 10) % range.length;
        for (var i = range.length; --i >= 0;)
        if (range[i] == current) break;
        while (count-- > 0)
        if (decrease) {
            if (--i < 0) i = range.length - 1;
        } else if (++i >= range.length) i = 0;
        var newval = range[i];
        el.innerHTML = newval;
        cal.onUpdateTime();
    }
    var mon = Calendar.findMonth(target);
    if (mon) {
        if (mon.month != cal.date.getMonth()) {
            if (cal.hilitedMonth) {
                Calendar.removeClass(cal.hilitedMonth, "hilite");
            }
            Calendar.addClass(mon, "hilite");
            cal.hilitedMonth = mon;
        } else if (cal.hilitedMonth) {
            Calendar.removeClass(cal.hilitedMonth, "hilite");
        }
    } else {
        if (cal.hilitedMonth) {
            Calendar.removeClass(cal.hilitedMonth, "hilite");
        }
        var year = Calendar.findYear(target);
        if (year) {
            if (year.year != cal.date.getFullYear()) {
                if (cal.hilitedYear) {
                    Calendar.removeClass(cal.hilitedYear, "hilite");
                }
                Calendar.addClass(year, "hilite");
                cal.hilitedYear = year;
            } else if (cal.hilitedYear) {
                Calendar.removeClass(cal.hilitedYear, "hilite");
            }
        } else if (cal.hilitedYear) {
            Calendar.removeClass(cal.hilitedYear, "hilite");
        }
    }
    return Calendar.stopEvent(ev);
};
Calendar.tableMouseDown = function (ev) {
    if (Calendar.getTargetElement(ev) == Calendar.getElement(ev)) {
        return Calendar.stopEvent(ev);
    }
};
Calendar.calDragIt = function (ev) {
    var cal = Calendar._C;
    if (!(cal && cal.dragging)) {
        return false;
    }
    var posX;
    var posY;
    if (Calendar.is_ie) {
        posY = window.event.clientY + document.body.scrollTop;
        posX = window.event.clientX + document.body.scrollLeft;
    } else {
        posX = ev.pageX;
        posY = ev.pageY;
    }
    cal.hideShowCovered();
    var st = cal.element.style;
    st.left = (posX - cal.xOffs) + "px";
    st.top = (posY - cal.yOffs) + "px";
    return Calendar.stopEvent(ev);
};
Calendar.calDragEnd = function (ev) {
    var cal = Calendar._C;
    if (!cal) {
        return false;
    }
    cal.dragging = false;
    with(Calendar) {
        removeEvent(document, "mousemove", calDragIt);
        removeEvent(document, "mouseup", calDragEnd);
        tableMouseUp(ev);
    }
    cal.hideShowCovered();
};
Calendar.dayMouseDown = function (ev) {
    var el = Calendar.getElement(ev);
    if (el.disabled) {
        return false;
    }
    var cal = el.calendar;
    cal.activeDiv = el;
    Calendar._C = cal;
    if (el.navtype != 300) with(Calendar) {
        if (el.navtype == 50) {
            el._current = el.innerHTML;
            addEvent(document, "mousemove", tableMouseOver);
        } else addEvent(document, Calendar.is_ie5 ? "mousemove" : "mouseover", tableMouseOver);
        addClass(el, "hilite active");
        addEvent(document, "mouseup", tableMouseUp);
    } else if (cal.isPopup) {
        cal._dragStart(ev);
    }
    if (el.navtype == -1 || el.navtype == 1) {
        if (cal.timeout) clearTimeout(cal.timeout);
        cal.timeout = setTimeout("Calendar.showMonthsCombo()", 250);
    } else if (el.navtype == -2 || el.navtype == 2) {
        if (cal.timeout) clearTimeout(cal.timeout);
        cal.timeout = setTimeout((el.navtype > 0) ? "Calendar.showYearsCombo(true)" : "Calendar.showYearsCombo(false)", 250);
    } else {
        cal.timeout = null;
    }
    return Calendar.stopEvent(ev);
};
Calendar.dayMouseDblClick = function (ev) {
    Calendar.cellClick(Calendar.getElement(ev), ev || window.event);
    if (Calendar.is_ie) {
        document.selection.empty();
    }
};
Calendar.dayMouseOver = function (ev) {
    var el = Calendar.getElement(ev);
    if (Calendar.isRelated(el, ev) || Calendar._C || el.disabled) {
        return false;
    }
    if (el.ttip) {
        if (el.ttip.substr(0, 1) == "_") {
            el.ttip = el.caldate.print(el.calendar.ttDateFormat) + el.ttip.substr(1);
        }
        el.calendar.tooltips.innerHTML = el.ttip;
    }
    if (el.navtype != 300) {
        Calendar.addClass(el, "hilite");
        if (el.caldate) {
            Calendar.addClass(el.parentNode, "rowhilite");
        }
    }
    return Calendar.stopEvent(ev);
};
Calendar.dayMouseOut = function (ev) {
    with(Calendar) {
        var el = getElement(ev);
        if (isRelated(el, ev) || _C || el.disabled) return false;
        removeClass(el, "hilite");
        if (el.caldate) removeClass(el.parentNode, "rowhilite");
        if (el.calendar) el.calendar.tooltips.innerHTML = _TT["SEL_DATE"];
        return stopEvent(ev);
    }
};
Calendar.cellClick = function (el, ev) {
    var cal = el.calendar;
    var closing = false;
    var newdate = false;
    var date = null;
    if (typeof el.navtype == "undefined") {
        if (cal.currentDateEl) {
            Calendar.removeClass(cal.currentDateEl, "selected");
            Calendar.addClass(el, "selected");
            closing = (cal.currentDateEl == el);
            if (!closing) {
                cal.currentDateEl = el;
            }
        }
        cal.date.setDateOnly(el.caldate);
        date = cal.date;
        var other_month = !(cal.dateClicked = !el.otherMonth);
        if (!other_month && !cal.currentDateEl) cal._toggleMultipleDate(new Date(date));
        else newdate = !el.disabled;
        if (other_month) cal._init(cal.firstDayOfWeek, date);
    } else {
        if (el.navtype == 200) {
            Calendar.removeClass(el, "hilite");
            cal.callCloseHandler();
            return;
        }
        date = new Date(cal.date);
        if (el.navtype == 0) date.setDateOnly(new Date());
        cal.dateClicked = false;
        var year = date.getFullYear();
        var mon = date.getMonth();

        function setMonth(m) {
            var day = date.getDate();
            var max = date.getMonthDays(m);
            if (day > max) {
                date.setDate(max);
            }
            date.setMonth(m);
        };
        switch (el.navtype) {
            case 400:
                Calendar.removeClass(el, "hilite");
                var text = Calendar._TT["ABOUT"];
                if (typeof text != "undefined") {
                    text += cal.showsTime ? Calendar._TT["ABOUT_TIME"] : "";
                } else {
                    text = "Help and about box text is not translated into this language.\n" + "If you know this language and you feel generous please update\n" + "the corresponding file in \"lang\" subdir to match calendar-en.js\n" + "and send it back to <mihai_bazon@yahoo.com> to get it into the distribution  ;-)\n\n" + "Thank you!\n" + "http://dynarch.com/mishoo/calendar.epl\n";
                }
                alert(text);
                return;
            case -2:
                if (year > cal.minYear) {
                    date.setFullYear(year - 1);
                }
                break;
            case -1:
                if (mon > 0) {
                    setMonth(mon - 1);
                } else if (year-- > cal.minYear) {
                    date.setFullYear(year);
                    setMonth(11);
                }
                break;
            case 1:
                if (mon < 11) {
                    setMonth(mon + 1);
                } else if (year < cal.maxYear) {
                    date.setFullYear(year + 1);
                    setMonth(0);
                }
                break;
            case 2:
                if (year < cal.maxYear) {
                    date.setFullYear(year + 1);
                }
                break;
            case 100:
                cal.setFirstDayOfWeek(el.fdow);
                return;
            case 50:
                var range = el._range;
                var current = el.innerHTML;
                for (var i = range.length; --i >= 0;)
                if (range[i] == current) break;
                if (ev && ev.shiftKey) {
                    if (--i < 0) i = range.length - 1;
                } else if (++i >= range.length) i = 0;
                var newval = range[i];
                el.innerHTML = newval;
                cal.onUpdateTime();
                return;
            case 0:
                if ((typeof cal.getDateStatus == "function") && cal.getDateStatus(date, date.getFullYear(), date.getMonth(), date.getDate())) {
                    return false;
                }
                break;
        }
        if (!date.equalsTo(cal.date)) {
            cal.setDate(date);
            newdate = true;
        } else if (el.navtype == 0) newdate = closing = true;
    }
    if (newdate) {
        ev && cal.callHandler();
    }
    if (closing) {
        Calendar.removeClass(el, "hilite");
        ev && cal.callCloseHandler();
    }
};
Calendar.prototype.create = function (_par) {
    var parent = null;
    if (!_par) {
        parent = document.getElementsByTagName("body")[0];
        this.isPopup = true;
    } else {
        parent = _par;
        this.isPopup = false;
    }
    this.date = this.dateStr ? new Date(this.dateStr) : new Date();
    var table = Calendar.createElement("table");
    this.table = table;
    table.cellSpacing = 0;
    table.cellPadding = 0;
    table.calendar = this;
    Calendar.addEvent(table, "mousedown", Calendar.tableMouseDown);
    var div = Calendar.createElement("div");
    this.element = div;
    div.className = "calendar";
    if (this.isPopup) {
        div.style.position = "absolute";
        div.style.display = "none";
    }
    div.appendChild(table);
    var thead = Calendar.createElement("thead", table);
    var cell = null;
    var row = null;
    var cal = this;
    var hh = function (text, cs, navtype, clsnm) {
        cell = Calendar.createElement("td", row);
        cell.colSpan = cs;
        cell.className = "button";
        if (navtype != 0 && Math.abs(navtype) <= 2) cell.className += " nav";
        if (clsnm) cell.className += " " + clsnm;
        Calendar._add_evs(cell);
        cell.calendar = cal;
        cell.navtype = navtype;
        cell.innerHTML = "<div unselectable='on'>" + text + "</div>";
        return cell;
    };
    row = Calendar.createElement("tr", thead);
    var title_length = 6;
    (this.isPopup) && --title_length;
    (this.weekNumbers) && ++title_length;
    hh("?", 1, 400, 'question').ttip = Calendar._TT["INFO"];
    this.title = hh("", title_length, 300);
    this.title.className = "title";
    if (this.isPopup) {
        this.title.ttip = Calendar._TT["DRAG_TO_MOVE"];
        this.title.style.cursor = "move";
        hh("&#x00d7;", 1, 200, 'close').ttip = Calendar._TT["CLOSE"];
    }
    row = Calendar.createElement("tr", thead);
    row.className = "headrow";
    this._nav_py = hh("&#x00ab;", 1, -2);
    this._nav_py.ttip = Calendar._TT["PREV_YEAR"];
    this._nav_pm = hh("&#x2039;", 1, -1);
    this._nav_pm.ttip = Calendar._TT["PREV_MONTH"];
    this._nav_now = hh(Calendar._TT["TODAY"], this.weekNumbers ? 4 : 3, 0);
    this._nav_now.ttip = Calendar._TT["GO_TODAY"];
    this._nav_nm = hh("&#x203a;", 1, 1);
    this._nav_nm.ttip = Calendar._TT["NEXT_MONTH"];
    this._nav_ny = hh("&#x00bb;", 1, 2);
    this._nav_ny.ttip = Calendar._TT["NEXT_YEAR"];
    row = Calendar.createElement("tr", thead);
    row.className = "daynames";
    if (this.weekNumbers) {
        cell = Calendar.createElement("td", row);
        cell.className = "name wn";
        cell.innerHTML = Calendar._TT["WK"];
    }
    for (var i = 7; i > 0; --i) {
        cell = Calendar.createElement("td", row);
        if (!i) {
            cell.navtype = 100;
            cell.calendar = this;
            Calendar._add_evs(cell);
        }
    }
    this.firstdayname = (this.weekNumbers) ? row.firstChild.nextSibling : row.firstChild;
    this._displayWeekdays();
    var tbody = Calendar.createElement("tbody", table);
    this.tbody = tbody;
    for (i = 6; i > 0; --i) {
        row = Calendar.createElement("tr", tbody);
        if (this.weekNumbers) {
            cell = Calendar.createElement("td", row);
        }
        for (var j = 7; j > 0; --j) {
            cell = Calendar.createElement("td", row);
            cell.calendar = this;
            Calendar._add_evs(cell);
        }
    }
    if (this.showsTime) {
        row = Calendar.createElement("tr", tbody);
        row.className = "time";
        cell = Calendar.createElement("td", row);
        cell.className = "time";
        cell.colSpan = 2;
        cell.innerHTML = Calendar._TT["TIME"] || "&nbsp;";
        cell = Calendar.createElement("td", row);
        cell.className = "time";
        cell.colSpan = this.weekNumbers ? 4 : 3;
        (function () {
            function makeTimePart(className, init, range_start, range_end) {
                var part = Calendar.createElement("span", cell);
                part.className = className;
                part.innerHTML = init;
                part.calendar = cal;
                part.ttip = Calendar._TT["TIME_PART"];
                part.navtype = 50;
                part._range = [];
                if (typeof range_start != "number") part._range = range_start;
                else {
                    for (var i = range_start; i <= range_end; ++i) {
                        var txt;
                        if (i < 10 && range_end >= 10) txt = '0' + i;
                        else txt = '' + i;
                        part._range[part._range.length] = txt;
                    }
                }
                Calendar._add_evs(part);
                return part;
            };
            var hrs = cal.date.getHours();
            var mins = cal.date.getMinutes();
            var t12 = !cal.time24;
            var pm = (hrs > 12);
            if (t12 && pm) hrs -= 12;
            var H = makeTimePart("hour", hrs, t12 ? 1 : 0, t12 ? 12 : 23);
            var span = Calendar.createElement("span", cell);
            span.innerHTML = ":";
            span.className = "colon";
            var M = makeTimePart("minute", mins, 0, 59);
            var AP = null;
            cell = Calendar.createElement("td", row);
            cell.className = "time";
            cell.colSpan = 2;
            if (t12) AP = makeTimePart("ampm", pm ? "pm" : "am", ["am", "pm"]);
            else cell.innerHTML = "&nbsp;";
            cal.onSetTime = function () {
                var pm, hrs = this.date.getHours(),
                    mins = this.date.getMinutes();
                if (t12) {
                    pm = (hrs >= 12);
                    if (pm) hrs -= 12;
                    if (hrs == 0) hrs = 12;
                    AP.innerHTML = pm ? "pm" : "am";
                }
                H.innerHTML = (hrs < 10) ? ("0" + hrs) : hrs;
                M.innerHTML = (mins < 10) ? ("0" + mins) : mins;
            };
            cal.onUpdateTime = function () {
                var date = this.date;
                var h = parseInt(H.innerHTML, 10);
                if (t12) {
                    if (/pm/i.test(AP.innerHTML) && h < 12) h += 12;
                    else if (/am/i.test(AP.innerHTML) && h == 12) h = 0;
                }
                var d = date.getDate();
                var m = date.getMonth();
                var y = date.getFullYear();
                date.setHours(h);
                date.setMinutes(parseInt(M.innerHTML, 10));
                date.setFullYear(y);
                date.setMonth(m);
                date.setDate(d);
                this.dateClicked = false;
                this.callHandler();
            };
        })();
    } else {
        this.onSetTime = this.onUpdateTime = function () {};
    }
    var tfoot = Calendar.createElement("tfoot", table);
    row = Calendar.createElement("tr", tfoot);
    row.className = "footrow";
    cell = hh(Calendar._TT["SEL_DATE"], this.weekNumbers ? 8 : 7, 300);
    cell.className = "ttip";
    if (this.isPopup) {
        cell.ttip = Calendar._TT["DRAG_TO_MOVE"];
        cell.style.cursor = "move";
    }
    this.tooltips = cell;
    div = Calendar.createElement("div", this.element);
    this.monthsCombo = div;
    div.className = "combo";
    for (i = 0; i < Calendar._MN.length; ++i) {
        var mn = Calendar.createElement("div");
        mn.className = Calendar.is_ie ? "label-IEfix" : "label";
        mn.month = i;
        mn.innerHTML = Calendar._SMN[i];
        div.appendChild(mn);
    }
    div = Calendar.createElement("div", this.element);
    this.yearsCombo = div;
    div.className = "combo";
    for (i = 12; i > 0; --i) {
        var yr = Calendar.createElement("div");
        yr.className = Calendar.is_ie ? "label-IEfix" : "label";
        div.appendChild(yr);
    }
    this._init(this.firstDayOfWeek, this.date);
    parent.appendChild(this.element);
};
Calendar.prototype._init = function (firstDayOfWeek, date) {
    var today = new Date(),
        TY = today.getFullYear(),
        TM = today.getMonth(),
        TD = today.getDate();
    this.table.style.visibility = "hidden";
    var year = date.getFullYear();
    if (year < this.minYear) {
        year = this.minYear;
        date.setFullYear(year);
    } else if (year > this.maxYear) {
        year = this.maxYear;
        date.setFullYear(year);
    }
    this.firstDayOfWeek = firstDayOfWeek;
    this.date = new Date(date);
    var month = date.getMonth();
    var mday = date.getDate();
    var no_days = date.getMonthDays();
    date.setDate(1);
    var day1 = (date.getDay() - this.firstDayOfWeek) % 7;
    if (day1 < 0) day1 += 7;
    if (day1 < 0) {
        day1 = Math.abs(day1);
    } else {
        if (day1 > 0) day1 = 0 - day1;
    }
    date.setDate(day1);
    date.setDate(date.getDate() + 1);
    var row = this.tbody.firstChild;
    var MN = Calendar._SMN[month];
    var ar_days = this.ar_days = new Array();
    var weekend = Calendar._TT["WEEKEND"];
    var dates = this.multiple ? (this.datesCells = {}) : null;
    for (var i = 0; i < 6; ++i, row = row.nextSibling) {
        var cell = row.firstChild;
        if (this.weekNumbers) {
            cell.className = "day wn";
            cell.innerHTML = date.getWeekNumber();
            cell = cell.nextSibling;
        }
        row.className = "daysrow";
        var hasdays = false,
            iday, dpos = ar_days[i] = [];
        for (var j = 0; j < 7; ++j, cell = cell.nextSibling, date.setDate(iday + 1)) {
            iday = date.getDate();
            var wday = date.getDay();
            cell.className = "day";
            cell.pos = i << 4 | j;
            dpos[j] = cell;
            var current_month = (date.getMonth() == month);
            if (!current_month) {
                if (this.showsOtherMonths) {
                    cell.className += " othermonth";
                    cell.otherMonth = true;
                } else {
                    cell.className = "emptycell";
                    cell.innerHTML = "&nbsp;";
                    cell.disabled = true;
                    continue;
                }
            } else {
                cell.otherMonth = false;
                hasdays = true;
            }
            cell.disabled = false;
            cell.innerHTML = this.getDateText ? this.getDateText(date, iday) : iday;
            if (dates) dates[date.print("%Y%m%d")] = cell;
            if (this.getDateStatus) {
                var status = this.getDateStatus(date, year, month, iday);
                if (this.getDateToolTip) {
                    var toolTip = this.getDateToolTip(date, year, month, iday);
                    if (toolTip) cell.title = toolTip;
                }
                if (status === true) {
                    cell.className += " disabled";
                    cell.disabled = true;
                } else {
                    if (/disabled/i.test(status)) cell.disabled = true;
                    cell.className += " " + status;
                }
            }
            if (!cell.disabled) {
                cell.caldate = new Date(date);
                cell.ttip = "_";
                if (!this.multiple && current_month && iday == mday && this.hiliteToday) {
                    cell.className += " selected";
                    this.currentDateEl = cell;
                }
                if (date.getFullYear() == TY && date.getMonth() == TM && iday == TD) {
                    cell.className += " today";
                    cell.ttip += Calendar._TT["PART_TODAY"];
                }
                if (weekend.indexOf(wday.toString()) != -1) cell.className += cell.otherMonth ? " oweekend" : " weekend";
            }
        }
        if (!(hasdays || this.showsOtherMonths)) row.className = "emptyrow";
    }
    this.title.innerHTML = Calendar._MN[month] + " " + year;
    this.onSetTime();
    this.table.style.visibility = "visible";
    this._initMultipleDates();
};
Calendar.prototype._initMultipleDates = function () {
    if (this.multiple) {
        for (var i in this.multiple) {
            var cell = this.datesCells[i];
            var d = this.multiple[i];
            if (!d) continue;
            if (cell) cell.className += " selected";
        }
    }
};
Calendar.prototype._toggleMultipleDate = function (date) {
    if (this.multiple) {
        var ds = date.print("%Y%m%d");
        var cell = this.datesCells[ds];
        if (cell) {
            var d = this.multiple[ds];
            if (!d) {
                Calendar.addClass(cell, "selected");
                this.multiple[ds] = date;
            } else {
                Calendar.removeClass(cell, "selected");
                delete this.multiple[ds];
            }
        }
    }
};
Calendar.prototype.setDateToolTipHandler = function (unaryFunction) {
    this.getDateToolTip = unaryFunction;
};
Calendar.prototype.setDate = function (date) {
    if (!date.equalsTo(this.date)) {
        this._init(this.firstDayOfWeek, date);
    }
};
Calendar.prototype.refresh = function () {
    this._init(this.firstDayOfWeek, this.date);
};
Calendar.prototype.setFirstDayOfWeek = function (firstDayOfWeek) {
    this._init(firstDayOfWeek, this.date);
    this._displayWeekdays();
};
Calendar.prototype.setDateStatusHandler = Calendar.prototype.setDisabledHandler = function (unaryFunction) {
    this.getDateStatus = unaryFunction;
};
Calendar.prototype.setRange = function (a, z) {
    this.minYear = a;
    this.maxYear = z;
};
Calendar.prototype.callHandler = function () {
    if (this.onSelected) {
        this.onSelected(this, this.date.print(this.dateFormat));
    }
};
Calendar.prototype.callCloseHandler = function () {
    if (this.onClose) {
        this.onClose(this);
    }
    this.hideShowCovered();
};
Calendar.prototype.destroy = function () {
    var el = this.element.parentNode;
    el.removeChild(this.element);
    Calendar._C = null;
    window._dynarch_popupCalendar = null;
};
Calendar.prototype.reparent = function (new_parent) {
    var el = this.element;
    el.parentNode.removeChild(el);
    new_parent.appendChild(el);
};
Calendar._checkCalendar = function (ev) {
    var calendar = window._dynarch_popupCalendar;
    if (!calendar) {
        return false;
    }
    var el = Calendar.is_ie ? Calendar.getElement(ev) : Calendar.getTargetElement(ev);
    for (; el != null && el != calendar.element; el = el.parentNode);
    if (el == null) {
        window._dynarch_popupCalendar.callCloseHandler();
        return Calendar.stopEvent(ev);
    }
};
Calendar.prototype.show = function () {
    var rows = this.table.getElementsByTagName("tr");
    for (var i = rows.length; i > 0;) {
        var row = rows[--i];
        Calendar.removeClass(row, "rowhilite");
        var cells = row.getElementsByTagName("td");
        for (var j = cells.length; j > 0;) {
            var cell = cells[--j];
            Calendar.removeClass(cell, "hilite");
            Calendar.removeClass(cell, "active");
        }
    }
    this.element.style.display = "block";
    this.hidden = false;
    if (this.isPopup) {
        window._dynarch_popupCalendar = this;
        Calendar.addEvent(document, "mousedown", Calendar._checkCalendar);
    }
    this.hideShowCovered();
};
Calendar.prototype.hide = function () {
    if (this.isPopup) {
        Calendar.removeEvent(document, "mousedown", Calendar._checkCalendar);
    }
    this.element.style.display = "none";
    this.hidden = true;
    this.hideShowCovered();
};
Calendar.prototype.showAt = function (x, y) {
    var s = this.element.style;
    s.left = x + "px";
    s.top = y + "px";
    this.show();
};
Calendar.prototype.showAtElement = function (el, opts) {
    var self = this;
    var p = Calendar.getAbsolutePos(el);
    if (!opts || typeof opts != "string") {
        this.showAt(p.x, p.y + el.offsetHeight);
        return true;
    }

    function fixPosition(box) {
        if (box.x < 0) box.x = 0;
        if (box.y < 0) box.y = 0;
        var cp = document.createElement("div");
        var s = cp.style;
        s.position = "absolute";
        s.right = s.bottom = s.width = s.height = "0px";
        document.body.appendChild(cp);
        var br = Calendar.getAbsolutePos(cp);
        document.body.removeChild(cp);
        br.y += window.scrollY;
        br.x += window.scrollX;
        var tmp = box.x + box.width - br.x;
        if (tmp > 0) box.x -= tmp;
        tmp = box.y + box.height - br.y;
        if (tmp > 0) box.y -= tmp;
    };
    Calendar.continuation_for_khtml_browser = function () {
        var w = self.element.offsetWidth;
        var h = self.element.offsetHeight;
        self.element.style.display = "none";
        var valign = opts.substr(0, 1);
        var halign = "l";
        if (opts.length > 1) {
            halign = opts.substr(1, 1);
        }
        switch (valign) {
            case "T":
                p.y -= h;
                break;
            case "B":
                p.y += el.offsetHeight;
                break;
            case "C":
                p.y += (el.offsetHeight - h) / 2;
                break;
            case "t":
                p.y += el.offsetHeight - h;
                break;
            case "b":
                break;
        }
        switch (halign) {
            case "L":
                p.x -= w;
                break;
            case "R":
                p.x += el.offsetWidth;
                break;
            case "C":
                p.x += (el.offsetWidth - w) / 2;
                break;
            case "l":
                p.x += el.offsetWidth - w;
                break;
            case "r":
                break;
        }
        p.width = w;
        p.height = h + 40;
        self.monthsCombo.style.display = "none";
        fixPosition(p);
        self.showAt(p.x, p.y);
    };
    if (Calendar.is_khtml) setTimeout("Calendar.continuation_for_khtml_browser()", 10);
    else Calendar.continuation_for_khtml_browser();
};
Calendar.prototype.setDateFormat = function (str) {
    this.dateFormat = str;
};
Calendar.prototype.setTtDateFormat = function (str) {
    this.ttDateFormat = str;
};
Calendar.prototype.parseDate = function (str, fmt) {
    if (!fmt) fmt = this.dateFormat;
    this.setDate(Date.parseDate(str, fmt));
};
Calendar.prototype.hideShowCovered = function () {
    if (!Calendar.is_ie && !Calendar.is_opera) return;

    function getVisib(obj) {
        var value = obj.style.visibility;
        if (!value) {
            if (document.defaultView && typeof (document.defaultView.getComputedStyle) == "function") {
                if (!Calendar.is_khtml) value = document.defaultView.getComputedStyle(obj, "").getPropertyValue("visibility");
                else value = '';
            } else if (obj.currentStyle) {
                value = obj.currentStyle.visibility;
            } else value = '';
        }
        return value;
    };
    var tags = new Array("applet", "iframe", "select");
    var el = this.element;
    var p = Calendar.getAbsolutePos(el);
    var EX1 = p.x;
    var EX2 = el.offsetWidth + EX1;
    var EY1 = p.y;
    var EY2 = el.offsetHeight + EY1;
    for (var k = tags.length; k > 0;) {
        var ar = document.getElementsByTagName(tags[--k]);
        var cc = null;
        for (var i = ar.length; i > 0;) {
            cc = ar[--i];
            p = Calendar.getAbsolutePos(cc);
            var CX1 = p.x;
            var CX2 = cc.offsetWidth + CX1;
            var CY1 = p.y;
            var CY2 = cc.offsetHeight + CY1;
            if (this.hidden || (CX1 > EX2) || (CX2 < EX1) || (CY1 > EY2) || (CY2 < EY1)) {
                if (!cc.__msh_save_visibility) {
                    cc.__msh_save_visibility = getVisib(cc);
                }
                cc.style.visibility = cc.__msh_save_visibility;
            } else {
                if (!cc.__msh_save_visibility) {
                    cc.__msh_save_visibility = getVisib(cc);
                }
                cc.style.visibility = "hidden";
            }
        }
    }
};
Calendar.prototype._displayWeekdays = function () {
    var fdow = this.firstDayOfWeek;
    var cell = this.firstdayname;
    var weekend = Calendar._TT["WEEKEND"];
    for (var i = 0; i < 7; ++i) {
        cell.className = "day name";
        var realday = (i + fdow) % 7;
        if (i) {
            cell.ttip = Calendar._TT["DAY_FIRST"].replace("%s", Calendar._DN[realday]);
            cell.navtype = 100;
            cell.calendar = this;
            cell.fdow = realday;
            Calendar._add_evs(cell);
        }
        if (weekend.indexOf(realday.toString()) != -1) {
            Calendar.addClass(cell, "weekend");
        }
        cell.innerHTML = Calendar._SDN[(i + fdow) % 7];
        cell = cell.nextSibling;
    }
};
Calendar.prototype._hideCombos = function () {
    this.monthsCombo.style.display = "none";
    this.yearsCombo.style.display = "none";
};
Calendar.prototype._dragStart = function (ev) {
    if (this.dragging) {
        return;
    }
    this.dragging = true;
    var posX;
    var posY;
    if (Calendar.is_ie) {
        posY = window.event.clientY + document.body.scrollTop;
        posX = window.event.clientX + document.body.scrollLeft;
    } else {
        posY = ev.clientY + window.scrollY;
        posX = ev.clientX + window.scrollX;
    }
    var st = this.element.style;
    this.xOffs = posX - parseInt(st.left);
    this.yOffs = posY - parseInt(st.top);
    with(Calendar) {
        addEvent(document, "mousemove", calDragIt);
        addEvent(document, "mouseup", calDragEnd);
    }
};
Date._MD = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
Date.SECOND = 1000;
Date.MINUTE = 60 * Date.SECOND;
Date.HOUR = 60 * Date.MINUTE;
Date.DAY = 24 * Date.HOUR;
Date.WEEK = 7 * Date.DAY;
Date.parseDate = function (str, fmt) {
    var today = new Date();
    var y = 0;
    var m = -1;
    var d = 0;
    var a = str.split(/\W+/);
    var b = fmt.match(/%./g);
    var i = 0,
        j = 0;
    var hr = 0;
    var min = 0;
    for (i = 0; i < a.length; ++i) {
        if (!a[i]) continue;
        switch (b[i]) {
            case "%d":
            case "%e":
                d = parseInt(a[i], 10);
                break;
            case "%m":
                m = parseInt(a[i], 10) - 1;
                break;
            case "%Y":
            case "%y":
                y = parseInt(a[i], 10);
                (y < 100) && (y += (y > 29) ? 1900 : 2000);
                break;
            case "%b":
            case "%B":
                for (j = 0; j < 12; ++j) {
                    if (Calendar._MN[j].substr(0, a[i].length).toLowerCase() == a[i].toLowerCase()) {
                        m = j;
                        break;
                    }
                }
                break;
            case "%H":
            case "%I":
            case "%k":
            case "%l":
                hr = parseInt(a[i], 10);
                break;
            case "%P":
            case "%p":
                if (/pm/i.test(a[i]) && hr < 12) hr += 12;
                else if (/am/i.test(a[i]) && hr >= 12) hr -= 12;
                break;
            case "%M":
                min = parseInt(a[i], 10);
                break;
        }
    }
    if (isNaN(y)) y = today.getFullYear();
    if (isNaN(m)) m = today.getMonth();
    if (isNaN(d)) d = today.getDate();
    if (isNaN(hr)) hr = today.getHours();
    if (isNaN(min)) min = today.getMinutes();
    if (y != 0 && m != -1 && d != 0) return new Date(y, m, d, hr, min, 0);
    y = 0;
    m = -1;
    d = 0;
    for (i = 0; i < a.length; ++i) {
        if (a[i].search(/[a-zA-Z]+/) != -1) {
            var t = -1;
            for (j = 0; j < 12; ++j) {
                if (Calendar._MN[j].substr(0, a[i].length).toLowerCase() == a[i].toLowerCase()) {
                    t = j;
                    break;
                }
            }
            if (t != -1) {
                if (m != -1) {
                    d = m + 1;
                }
                m = t;
            }
        } else if (parseInt(a[i], 10) <= 12 && m == -1) {
            m = a[i] - 1;
        } else if (parseInt(a[i], 10) > 31 && y == 0) {
            y = parseInt(a[i], 10);
            (y < 100) && (y += (y > 29) ? 1900 : 2000);
        } else if (d == 0) {
            d = a[i];
        }
    }
    if (y == 0) y = today.getFullYear();
    if (m != -1 && d != 0) return new Date(y, m, d, hr, min, 0);
    return today;
};
Date.prototype.getMonthDays = function (month) {
    var year = this.getFullYear();
    if (typeof month == "undefined") {
        month = this.getMonth();
    }
    if (((0 == (year % 4)) && ((0 != (year % 100)) || (0 == (year % 400)))) && month == 1) {
        return 29;
    } else {
        return Date._MD[month];
    }
};
Date.prototype.getDayOfYear = function () {
    var now = new Date(this.getFullYear(), this.getMonth(), this.getDate(), 0, 0, 0);
    var then = new Date(this.getFullYear(), 0, 0, 0, 0, 0);
    var time = now - then;
    return Math.floor(time / Date.DAY);
};
Date.prototype.getWeekNumber = function () {
    var d = new Date(this.getFullYear(), this.getMonth(), this.getDate(), 0, 0, 0);
    var DoW = d.getDay();
    d.setDate(d.getDate() - (DoW + 6) % 7 + 3);
    var ms = d.valueOf();
    d.setMonth(0);
    d.setDate(4);
    return Math.round((ms - d.valueOf()) / (7 * 864e5)) + 1;
};
Date.prototype.equalsTo = function (date) {
    return ((this.getFullYear() == date.getFullYear()) && (this.getMonth() == date.getMonth()) && (this.getDate() == date.getDate()) && (this.getHours() == date.getHours()) && (this.getMinutes() == date.getMinutes()));
};
Date.prototype.setDateOnly = function (date) {
    var tmp = new Date(date);
    this.setDate(1);
    this.setFullYear(tmp.getFullYear());
    this.setMonth(tmp.getMonth());
    this.setDate(tmp.getDate());
};
Date.prototype.print = function (str) {
    var m = this.getMonth();
    var d = this.getDate();
    var y = this.getFullYear();
    var wn = this.getWeekNumber();
    var w = this.getDay();
    var s = {};
    var hr = this.getHours();
    var pm = (hr >= 12);
    var ir = (pm) ? (hr - 12) : hr;
    var dy = this.getDayOfYear();
    if (ir == 0) ir = 12;
    var min = this.getMinutes();
    var sec = this.getSeconds();
    s["%a"] = Calendar._SDN[w];
    s["%A"] = Calendar._DN[w];
    s["%b"] = Calendar._SMN[m];
    s["%B"] = Calendar._MN[m];
    s["%C"] = 1 + Math.floor(y / 100);
    s["%d"] = (d < 10) ? ("0" + d) : d;
    s["%e"] = d;
    s["%H"] = (hr < 10) ? ("0" + hr) : hr;
    s["%I"] = (ir < 10) ? ("0" + ir) : ir;
    s["%j"] = (dy < 100) ? ((dy < 10) ? ("00" + dy) : ("0" + dy)) : dy;
    s["%k"] = hr;
    s["%l"] = ir;
    s["%m"] = (m < 9) ? ("0" + (1 + m)) : (1 + m);
    s["%M"] = (min < 10) ? ("0" + min) : min;
    s["%n"] = "\n";
    s["%p"] = pm ? "PM" : "AM";
    s["%P"] = pm ? "pm" : "am";
    s["%s"] = Math.floor(this.getTime() / 1000);
    s["%S"] = (sec < 10) ? ("0" + sec) : sec;
    s["%t"] = "\t";
    s["%U"] = s["%W"] = s["%V"] = (wn < 10) ? ("0" + wn) : wn;
    s["%u"] = w + 1;
    s["%w"] = w;
    s["%y"] = ('' + y).substr(2, 2);
    s["%Y"] = y;
    s["%%"] = "%";
    var re = /%./g;
    if (!Calendar.is_ie5 && !Calendar.is_khtml) return str.replace(re, function (par) {
        return s[par] || par;
    });
    var a = str.match(re);
    for (var i = 0; i < a.length; i++) {
        var tmp = s[a[i]];
        if (tmp) {
            re = new RegExp(a[i], 'g');
            str = str.replace(re, tmp);
        }
    }
    return str;
};
Date.prototype.__msh_oldSetFullYear = Date.prototype.setFullYear;
Date.prototype.setFullYear = function (y) {
    var d = new Date(this);
    d.__msh_oldSetFullYear(y);
    if (d.getMonth() != this.getMonth()) this.setDate(28);
    this.__msh_oldSetFullYear(y);
};
window._dynarch_popupCalendar = null;

function selectDate(cal) {
    var p = cal.params;
    var update = (cal.dateClicked || p.electric);
    year = p.inputField.id;
    day = year + '-2';
    month = year + '-1';
    document.getElementById(month).value = cal.date.print('%m');
    document.getElementById(day).value = cal.date.print('%e');
    document.getElementById(year).value = cal.date.print('%Y');
}

function selectEuroDate(cal) {
    var p = cal.params;
    var update = (cal.dateClicked || p.electric);
    year = p.inputField.id;
    day = year + '-1';
    month = year + '-2';
    document.getElementById(month).value = cal.date.print('%m');
    document.getElementById(day).value = cal.date.print('%e');
    document.getElementById(year).value = cal.date.print('%Y');
}
Calendar._DN = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
Calendar._SDN = new Array("S", "M", "T", "W", "T", "F", "S", "S");
Calendar._FD = 0;
Calendar._MN = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
Calendar._SMN = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
Calendar._TT = {};
Calendar._TT["INFO"] = "About the Calendar";
Calendar._TT["ABOUT"] = "DHTML Date/Time Selector\n" + "(c) dynarch.com 2002-2005 / Author: Mihai Bazon\n" + "For latest version visit:\n http://www.dynarch.com/projects/calendar/\n" + "Distributed under GNU LGPL.\n See http://gnu.org/licenses/lgpl.html for details." + "\n\n" + "Date selection:\n" + "- Use the \xab, \xbb buttons to select year\n" + "- Use the " + String.fromCharCode(0x2039) + ", " + String.fromCharCode(0x203a) + " buttons to select month\n" + "- Hold mouse button for faster selection.";
Calendar._TT["ABOUT_TIME"] = "\n\n" + "Time selection:\n" + "- Click on any of the time parts to increase it\n" + "- or Shift-click to decrease it\n" + "- or click and drag for faster selection.";
Calendar._TT["PREV_YEAR"] = "Prev. Year (Hold for Menu)";
Calendar._TT["PREV_MONTH"] = "Prev. Month (Hold for Menu)";
Calendar._TT["GO_TODAY"] = "Go to Today";
Calendar._TT["NEXT_MONTH"] = "Next Month (Hold for Menu)";
Calendar._TT["NEXT_YEAR"] = "Next Year (Hold for Menu)";
Calendar._TT["SEL_DATE"] = "Select Date";
Calendar._TT["DRAG_TO_MOVE"] = "Drag to Move";
Calendar._TT["PART_TODAY"] = " (Today)";
Calendar._TT["DAY_FIRST"] = "Display %ss first";
Calendar._TT["WEEKEND"] = "0,6";
Calendar._TT["CLOSE"] = "Close Calendar";
Calendar._TT["TODAY"] = "Today";
Calendar._TT["TIME_PART"] = "(Shift-)Click or Drag to Change Value";
Calendar._TT["DEF_DATE_FORMAT"] = "%Y-%m-%d";
Calendar._TT["TT_DATE_FORMAT"] = "%b %e, %Y";
Calendar._TT["WK"] = "wk";
Calendar._TT["TIME"] = "Time:";
Calendar.setup = function (params) {
    function param_default(pname, def) {
        if (typeof params[pname] == "undefined") {
            params[pname] = def;
        }
    };
    param_default("inputField", null);
    param_default("displayArea", null);
    param_default("button", null);
    param_default("eventName", "click");
    param_default("ifFormat", "%Y/%m/%d");
    param_default("daFormat", "%Y/%m/%d");
    param_default("singleClick", true);
    param_default("disableFunc", null);
    param_default("dateStatusFunc", params["disableFunc"]);
    param_default("dateText", null);
    param_default("firstDay", null);
    param_default("align", "BR");
    param_default("range", [1900, 2999]);
    param_default("weekNumbers", false);
    param_default("flat", null);
    param_default("flatCallback", null);
    param_default("onSelect", null);
    param_default("onClose", null);
    param_default("onUpdate", null);
    param_default("date", null);
    param_default("showsTime", false);
    param_default("timeFormat", "24");
    param_default("electric", true);
    param_default("step", 2);
    param_default("position", null);
    param_default("cache", false);
    param_default("showOthers", false);
    param_default("multiple", null);
    var tmp = ["inputField", "displayArea", "button"];
    for (var i in tmp) {
        if (typeof params[tmp[i]] == "string") {
            params[tmp[i]] = document.getElementById(params[tmp[i]]);
        }
    }
    if (!(params.flat || params.multiple || params.inputField || params.displayArea || params.button)) {
        alert("Calendar.setup:\n  Nothing to setup (no fields found).  Please check your code");
        return false;
    }

    function onSelect(cal) {
        var p = cal.params;
        var update = (cal.dateClicked || p.electric);
        if (update && p.inputField) {
            p.inputField.value = cal.date.print(p.ifFormat);
            if (typeof p.inputField.onchange == "function") p.inputField.onchange();
        }
        if (update && p.displayArea) p.displayArea.innerHTML = cal.date.print(p.daFormat);
        if (update && typeof p.onUpdate == "function") p.onUpdate(cal);
        if (update && p.flat) {
            if (typeof p.flatCallback == "function") p.flatCallback(cal);
        }
        if (update && p.singleClick && cal.dateClicked) cal.callCloseHandler();
    };
    if (params.flat != null) {
        if (typeof params.flat == "string") params.flat = document.getElementById(params.flat);
        if (!params.flat) {
            alert("Calendar.setup:\n  Flat specified but can't find parent.");
            return false;
        }
        var cal = new Calendar(params.firstDay, params.date, params.onSelect || onSelect);
        cal.showsOtherMonths = params.showOthers;
        cal.showsTime = params.showsTime;
        cal.time24 = (params.timeFormat == "24");
        cal.params = params;
        cal.weekNumbers = params.weekNumbers;
        cal.setRange(params.range[0], params.range[1]);
        cal.setDateStatusHandler(params.dateStatusFunc);
        cal.getDateText = params.dateText;
        if (params.ifFormat) {
            cal.setDateFormat(params.ifFormat);
        }
        if (params.inputField && typeof params.inputField.value == "string") {
            cal.parseDate(params.inputField.value);
        }
        cal.create(params.flat);
        cal.show();
        return false;
    }
    var triggerEl = params.button || params.displayArea || params.inputField;
    triggerEl["on" + params.eventName] = function () {
        var dateEl = params.inputField || params.displayArea;
        var dateFmt = params.inputField ? params.ifFormat : params.daFormat;
        var mustCreate = false;
        var cal = window.calendar;
        if (dateEl) params.date = Date.parseDate(dateEl.value || dateEl.innerHTML, dateFmt);
        if (!(cal && params.cache)) {
            window.calendar = cal = new Calendar(params.firstDay, params.date, params.onSelect || onSelect, params.onClose || function (cal) {
                cal.hide();
            });
            cal.showsTime = params.showsTime;
            cal.time24 = (params.timeFormat == "24");
            cal.weekNumbers = params.weekNumbers;
            mustCreate = true;
        } else {
            if (params.date) cal.setDate(params.date);
            cal.hide();
        }
        if (params.multiple) {
            cal.multiple = {};
            for (var i = params.multiple.length; --i >= 0;) {
                var d = params.multiple[i];
                var ds = d.print("%Y%m%d");
                cal.multiple[ds] = d;
            }
        }
        cal.showsOtherMonths = params.showOthers;
        cal.yearStep = params.step;
        cal.setRange(params.range[0], params.range[1]);
        cal.params = params;
        cal.setDateStatusHandler(params.dateStatusFunc);
        cal.getDateText = params.dateText;
        cal.setDateFormat(dateFmt);
        if (mustCreate) cal.create();
        cal.refresh();
        if (!params.position) cal.showAtElement(params.button || params.displayArea || params.inputField, params.align);
        else cal.showAt(params.position[0], params.position[1]);
        return false;
    };
    return cal;
};
var Util = {
    getRev: function (el) {
        if (typeof el == 'string') el = $(el);
        return el.rev.replace('dd', '');
    },
    cleanValue: function (value) {
        value = value || '';
        value = value.strip();
        value = value.unescapeHTML();
        return value;
    },
    deCapitalize: function (string) {
        return string.charAt(0).toLowerCase() + string.substring(1);
    },
    isEmpty: function (text) {
        return (typeof (text) == 'undefined' || text == null || text == '' || text == 0) ? true : false;
    },
    deepCopy: function (object) {
        var jsonified = Object.toJSON(object);
        return jsonified.evalJSON();
    }
};
var DropDown = Class.create({
    activeDropdown: '',
    activeDropdownList: '',
    activeSelectedChoice: '',
    observers: [], //<sl:translate>
    specifyDropdownList: "You need to specify a dropdown list and default choice.", //</sl:translate>
    showDropdown: function (e, ctrl) {
        var dropdownListID = this.getDropdownListID(ctrl);
        this.activeDropdown = ctrl;
        this.activeDropdownList = $(dropdownListID);
        this.hideActiveDropdowns();
        this.clearCurrentChoiceInActiveDropdown(dropdownListID);
        this.modifyDropdownBasedOnType(ctrl);
        this.selectDropdownChoice(dropdownListID, ctrl);
        this.showDropdownList(e, dropdownListID, ctrl);
    },
    getDropdownListID: function (ctrl) {
        var matched_text = ctrl.className.match(/dropdown_list_(\w)+/);
        var dropdownListID = matched_text[0].replace('dropdown_list_', '');
        return dropdownListID;
    },
    hideActiveDropdowns: function () {
        $$('ul.ddList').invoke('hide');
    },
    clearCurrentChoiceInActiveDropdown: function (dropdownListID) {
        $$('#' + dropdownListID + ' a').invoke('removeClassName', 'selected');
    },
    modifyDropdownBasedOnType: function (ctrl) {
        if (ctrl.type) {
            this.activeDropdownList.className = "ddList " + ctrl.type
        } else {
            this.activeDropdownList.className = "ddList";
        }
    },
    selectDropdownChoice: function (dropdownListID, ctrl) {
        if (dropdownListID && ctrl.rev) {
            var choices = $$('#' + dropdownListID + ' .' + ctrl.rev + ' a');
            if (choices.length > 0) choices[0].addClassName('selected');
        } else {
            alert(this.specifyDropdownList);
        }
    },
    showDropdownList: function (e, dropdownListID, ctrl) {
        mX = Event.pointerX(e);
        mY = Event.pointerY(e);
        if ((mX + 286) > document.viewport.getWidth()) {
            mX = document.viewport.getWidth() - 300;
        }
        $(dropdownListID).style.top = mY + 'px';
        $(dropdownListID).style.left = mX + 'px';
        document.getElementsByTagName('html')[0].className = 'prepIE';
        clickAway = Builder.node('div', {
            id: 'clickAway',
            onclick: 'hideDropdownList();'
        });
        document.getElementsByTagName('body')[0].appendChild(clickAway);
        $(dropdownListID).show();
    },
    selectChoice: function (el) {
        this.activeDropdown.rev = el.parentNode.className.split(" ")[0];
        this.activeDropdown.innerHTML = this.getDropDownInnerHTML(el);
        this.activeSelectedChoice = el;
        this.notifyObservers();
        this.hideDropdownList();
    },
    getDropDownInnerHTML: function (el) {
        var html = '';
        if (el.childElements().length == 2) {
            var dropdownListID = this.getDropdownListID(this.activeDropdown);
            var str_length = (dropdownListID == 'Choices') ? 24 : 28;
            var html = (el.rel) ? el.rel : el.down().innerHTML;
            html = html.strip().truncate(str_length);
            html = html + '<b>' + el.down().next().innerHTML + '</b>';
        } else {
            return el.innerHTML;
        }
        return html;
    },
    hideDropdownList: function () {
        this.activeDropdownList.style.display = 'none';
        $('clickAway').remove();
    },
    registerObserver: function (observer) {
        this.observers.push(observer);
    },
    notifyObservers: function () {
        this.observers.each(function (observer) {
            observer.updateDropdown(this.activeDropdown);
        }.bind(this));
    }
});
var __DD = new DropDown();

function showDropdown(e, el) {
    if (!e) var e = window.event;
    Event.extend(e);
    Element.extend(el);
    __DD.showDropdown(e, el);
}

function selectChoice(el) {
    Element.extend(el);
    __DD.selectChoice(el);
}

function hideDropdownList() {
    __DD.hideDropdownList();
}

function registerObserver(observer) {
    __DD.registerObserver(observer);
}

function notifyObservers() {
    __DD.notifyObservers();
}
var Interface = Class.create({
    hideStatus: function () {
        hideStatus();
    }
});
var RulesBase = Class.create({
    dateFilters: {
        'dd7': 'is on',
        'dd8': 'd',
        'dd9': 'd',
        'dd10': 'd'
    },
    timeFilters: {
        'dd14': 'is at',
        'dd15': 'd',
        'dd16': 'd'
    },
    numberFilters: {
        'dd11': 'is equal to',
        'dd12': 'n',
        'dd13': 'n'
    },
    checkboxFilters: {
        'dd17': '<em class="positive">is</em>',
        'dd18': 's'
    },
    standardFilters: {
        'dd1': '<em class="positive">is</em>',
        'dd2': 's',
        'dd3': 's',
        'dd4': 's',
        'dd5': 's',
        'dd6': 's'
    },
    initialize: function () {},
    initDateTypeConditions: function (type) {
        $$(type + ' img').each(function (img) {
            this.setUpCalendar(img);
        }.bind(this));
        initToolTips();
    },
    setUpCalendar: function (dateImg) {
        var buttonId = dateImg.identify();
        var inputFieldId = dateImg.previous().identify();
        var dateFormat = (dateImg.alt == 'eurodate') ? '%d/%m/%Y' : "%m/%d/%Y";
        Calendar.setup({
            inputField: inputFieldId,
            button: buttonId,
            ifFormat: dateFormat
        });
    },
    updateRuleCounter: function (nums) {
        nums.each(function (div, index) {
            div.innerHTML = index + 1;
        }.bind(this));
    },
    choiceValueSelected: function (ruleType, rule, conditionId) {
        var liEl = $(ruleType + '_' + rule.RuleId);
        liEl.removeClassName('choiceError');
        var choiceEl = $(ruleType + 's_' + rule.RuleId + '_Condition_' + conditionId + '_ChoiceValue');
        choiceEl.removeClassName('choiceWarning');
    },
    matchTypeChange: function (ruleType, rule) {
        var rev = (rule.MatchType == 'any') ? 'ddOr' : 'ddAnd';
        var text = (rule.MatchType == 'any') ? '<em>OR</em>' : '<em>AND</em>';
        rule.Conditions.each(function (condition) {
            var link = $(ruleType + '_' + rule.RuleId + '_Condition_' + condition.ConditionId + '_MatchType');
            if (link) {
                link.rev = rev;
                link.innerHTML = text;
            }
        }.bind(this));
    },
    addCondition: function (ruleType, rule, conditionId, html) {
        $(ruleType + '_' + rule.RuleId + '_Condition_' + conditionId + '_Condition').insert({
            after: html
        });
        this.initDateTypeConditions('#' + Util.deCapitalize(ruleType));
    },
    deleteCondition: function (ruleType, ruleId, conditionId) {
        $(ruleType + '_' + ruleId + '_Condition_' + conditionId + '_Condition').remove();
    },
    fieldNameChange: function (ruleType, rule, value, conditionId) {
        var fieldNameEl = $(ruleType + '_' + rule.RuleId + '_Condition_' + conditionId + '_FieldName');
        var columnId = Util.getRev(fieldNameEl);
        var filterEl = $(ruleType + '_' + rule.RuleId + '_Condition_' + conditionId + '_Filter');
        var field = this.rulesObject.getFieldById(columnId);
        var fieldTypeof = this.getFieldTypeof(field);
        var spanEl = $(ruleType + '_' + rule.RuleId + '_Condition_' + conditionId + '_Value').up();
        spanEl.removeClassName('dateField');
        if (field.ColumnId == 'EntryId' || this.treatAsInteger(fieldTypeof)) {
            filterEl.type = 'int';
            this.forceFilterToBeIntegerValue(filterEl);
        } else if (fieldTypeof == 'time') {
            filterEl.type = 'time';
            this.forceFilterToBeTimeValue(filterEl);
        } else if (fieldTypeof == 'checkbox') {
            filterEl.type = 'check';
            this.forceFilterToBeCheckboxValue(filterEl);
        } else if (fieldTypeof == 'date' || fieldTypeof == 'eurodate' || field.ColumnId == 'DateCreated') {
            filterEl.type = 'date';
            spanEl.addClassName('dateField');
            var imgEl = $(ruleType + '_' + rule.RuleId + '_Condition_' + conditionId + '_Value').next();
            imgEl.alt = (fieldTypeof == 'eurodate') ? 'eurodate' : 'date';
            initToolTips();
            this.setUpCalendar(imgEl);
            this.forceFilterToBeDateValue(filterEl);
        } else {
            filterEl.type = '';
            this.forceFilterToBeStandardValue(filterEl);
        }
        var filterValue = Util.getRev(filterEl);
        this.showCorrectValueType(ruleType, rule, columnId, conditionId, field, filterValue);
    },
    getFieldTypeof: function (field) {
        return (field.Typeof) ? field.Typeof : field.parent.Typeof;
    },
    treatAsInteger: function (type) {
        return (type == 'number' || type == 'money') ? true : false;
    },
    forceFilterToBeIntegerValue: function (filterEl) {
        if (Util.isEmpty(this.numberFilters[filterEl.rev])) {
            filterEl.rev = 'dd11';
            filterEl.innerHTML = this.numberFilters['dd11'];
            updateDropdown(filterEl);
        }
    },
    forceFilterToBeDateValue: function (filterEl) {
        if (Util.isEmpty(this.dateFilters[filterEl.rev])) {
            filterEl.rev = 'dd7';
            filterEl.innerHTML = this.dateFilters['dd7'];
            updateDropdown(filterEl);
        }
    },
    forceFilterToBeTimeValue: function (filterEl) {
        if (Util.isEmpty(this.timeFilters[filterEl.rev])) {
            filterEl.rev = 'dd14';
            filterEl.innerHTML = this.timeFilters['dd14'];
            updateDropdown(filterEl);
        }
    },
    forceFilterToBeCheckboxValue: function (filterEl) {
        if (Util.isEmpty(this.checkboxFilters[filterEl.rev])) {
            filterEl.rev = 'dd17';
            filterEl.innerHTML = this.checkboxFilters['dd17'];
            updateDropdown(filterEl);
        }
    },
    forceFilterToBeStandardValue: function (filterEl) {
        if (Util.isEmpty(this.standardFilters[filterEl.rev])) {
            filterEl.rev = 'dd1';
            filterEl.innerHTML = this.standardFilters['dd1'];
            updateDropdown(filterEl);
        }
    },
    showCorrectValueType: function (ruleType, rule, columnId, conditionId, field, filterValue) {
        var valueEl = $(ruleType + '_' + rule.RuleId + '_Condition_' + conditionId + '_Value');
        var choiceEl = $(ruleType + '_' + rule.RuleId + '_Condition_' + conditionId + '_ChoiceValue');
        var choiceCheckboxEl = $(ruleType + '_' + rule.RuleId + '_Condition_' + conditionId + '_ChoiceCheckboxValue');
        if (this.isChoiceDropdown(field, filterValue)) {
            if (field.parent && field.parent.Typeof == 'checkbox') {
                valueEl.addClassName('hide');
                choiceEl.addClassName('hide');
                choiceCheckboxEl.removeClassName('hide');
            } else {
                valueEl.addClassName('hide');
                choiceCheckboxEl.addClassName('hide');
                choiceEl.removeClassName('hide');
            }
            var columnId = choiceEl.type.replace('field', '');
            if (columnId != field.ColumnId) {
                var choice = this.choiceHash[field.ColumnId];
                var subId = false;
                for (i in choice) {
                    if (!subId) {
                        subId = i;
                        break;
                    }
                }
                var choiceObj = this.choiceHash[field.ColumnId][subId];
                choiceEl.type = 'field' + field.ColumnId;
                choiceEl.rev = 'dd' + subId;
                choiceEl.innerHTML = choiceObj.Text + ' <b>' + choiceObj.Typeof + '</b>';
            }
            updateDropdown(choiceEl);
        } else {
            choiceEl.addClassName('hide');
            choiceCheckboxEl.addClassName('hide');
            valueEl.removeClassName('hide');
            rb_update(valueEl);
        }
    },
    isChoiceDropdown: function (field, filterValue) {
        var isChoicesType = this.isChoicesType(field);
        var isChoiceFilterValue = (filterValue == 1 || filterValue == 2 || filterValue == 17 || filterValue == 18);
        return (isChoicesType && isChoiceFilterValue);
    },
    isChoicesType: function (field) {
        if (Util.isEmpty(this.choiceHash[field.ColumnId])) {
            return false;
        } else {
            return true;
        }
    }
});
var RulesObject = Class.create({
    form: '',
    rules: [],
    choiceHash: '',
    ruleIdCounter: 1,
    conditionIdCounter: 1,
    filterValueHash: {
        1: 'is',
        2: 'is not',
        3: 'contains',
        4: 'does not contain',
        5: 'begins with',
        6: 'ends with',
        7: 'is on',
        8: 'is before',
        9: 'is after',
        10: 'is between',
        11: 'is equal to',
        12: 'is greater than',
        13: 'is less than',
        14: 'is at',
        15: 'is before',
        16: 'is after',
        17: 'is',
        18: 'is not'
    }, //<sl:translate>
    formRuleSettingMsg: 'Success! Thanks for filling out my form!', //</sl:translate>
    initialize: function (form, rules) {
        this.form = form;
        this.rules = rules;
        this.choiceHash = __CHOICES;
    },
    addRule: function (ruleType, rule) {
        this.rules[ruleType].push(rule);
    },
    duplicateCondition: function (ruleType, rule, conditionId) {
        var condition = this.getCondition(rule, conditionId);
        var newCondition = Util.deepCopy(condition);
        newCondition.ConditionId = this.nextConditionId();
        var newRule = Util.deepCopy(rule);
        newRule.Conditions = new Array(newCondition);
        return newRule;
    },
    getCondition: function (rule, conditionId) {
        return rule.Conditions.find(function (condition) {
            return condition.ConditionId == conditionId;
        }.bind(this));
    },
    addCondition: function (ruleType, rule) {
        var savedRule = this.getRule(ruleType, rule.RuleId);
        savedRule.Conditions.push(rule.Conditions[0]);
    },
    deleteCondition: function (ruleType, ruleId, conditionId) {
        var rule = this.getRule(ruleType, ruleId);
        var conditionToBeDeleted = this.getCondition(rule, conditionId);
        rule.Conditions = rule.Conditions.without(conditionToBeDeleted);
    },
    duplicateRule: function (ruleType, ruleId) {
        var rule = this.getRule(ruleType, ruleId);
        var newRule = Util.deepCopy(rule);
        newRule.RuleId = this.nextRuleId();
        newRule.Conditions.each(function (condition) {
            condition.ConditionId = this.nextConditionId();
        }.bind(this));
        return newRule;
    },
    getRule: function (ruleType, ruleId) {
        return this.rules[ruleType].find(function (rule) {
            return (rule.RuleId == ruleId);
        }.bind(this));
    },
    getFieldById: function (columnId) {
        var foundField;
        this.form.Fields.each(function (field) {
            if (Object.isArray(field.SubFields)) {
                field.SubFields.each(function (subField) {
                    if (subField.ColumnId == columnId) {
                        foundField = subField;
                        foundField.parent = field;
                    }
                }.bind(this));
            } else {
                if (field.ColumnId == columnId) foundField = field;
            }
        }.bind(this));
        return foundField;
    },
    getFieldTypeof: function (columnId) {
        var fieldTypeof;
        this.form.Fields.each(function (field) {
            if (Object.isArray(field.SubFields)) {
                field.SubFields.each(function (subField) {
                    if (subField.ColumnId == columnId) {
                        fieldTypeof = subField.Typeof || field.Typeof;
                    }
                }.bind(this));
            } else {
                if (field.ColumnId == columnId) fieldTypeof = field.Typeof;
            }
        }.bind(this));
        return fieldTypeof;
    },
    deleteRule: function (ruleId, type) {
        var ruleToBeDeleted = this.rules[type].find(function (rule, index) {
            return (rule.RuleId == ruleId);
        }.bind(this));
        this.rules[type] = this.rules[type].without(ruleToBeDeleted);
    },
    updateRule: function (ruleType, rule, attribute, value, conditionId, conditionAttribute) {
        if (attribute == 'Type' || attribute == 'Setting') {
            var type = (attribute == 'Type') ? value : rule.Type;
            this.updateRuleOfType(type, rule);
        } else if (conditionAttribute == 'MatchType') {
            rule.MatchType = (value.toLowerCase() == 'or') ? 'any' : 'all';
        } else if (attribute == 'Condition') {
            if (['FieldName', 'Filter', 'Value', 'ChoiceValue'].indexOf(conditionAttribute) > -1) {
                this.updateRuleCondition(ruleType, rule, value, conditionId, conditionAttribute);
            }
        }
    },
    updateRuleOfType: function (type, rule) {
        if (type == 'ShowMessage') {
            this.updateShowMessageRule(rule);
        } else if (type == 'Redirect') {
            this.updateRedirectRule(rule);
        } else if (type == 'SendEmail') {
            this.updateSendEmailRule(rule);
        } else if (type == 'SkipToPage') {
            this.updateSkipToPageRule(rule);
        } else if (type == 'Show' || type == 'Hide') {
            this.updateShowHideRule(rule);
        }
    },
    updateShowMessageRule: function (rule) {
        rule.Type = 'ShowMessage';
        rule.Setting = {
            Message: $('FormRules_' + rule.RuleId + '_Setting_ShowMessage').value
        };
    },
    updateRedirectRule: function (rule) {
        rule.Type = 'Redirect';
        rule.Setting = {
            Redirect: $('FormRules_' + rule.RuleId + '_Setting_Redirect').value
        };
    },
    updateSendEmailRule: function (rule) {
        rule.Type = 'SendEmail';
        if ('confirmation' == this.getSendEmailType(rule)) {
            rule.Setting = {
                'Type': 'SendConfirmationEmail',
                'EmailField': this.getConfirmationEmailField(rule),
                'Name': $('FormRules_' + rule.RuleId + '_Setting_ConfirmationEmail_Name').value.strip(),
                'Subject': $('FormRules_' + rule.RuleId + '_Setting_ConfirmationEmail_Subject').value.strip(),
                'Message': $('FormRules_' + rule.RuleId + '_Setting_ConfirmationEmail_Message').value.strip(),
                'ReplyTo': $('FormRules_' + rule.RuleId + '_Setting_ConfirmationEmail_ReplyTo').value.strip(),
                'IncludeEntry': this.getConfirmationEmailIncludeEntry(rule)
            };
        } else {
            rule.Setting = {
                'Type': 'SendNotificationEmail',
                'Value': $('FormRules_' + rule.RuleId + '_Setting_SendEmail_Value_Text').value.strip()
            };
        }
    },
    getSendEmailType: function (rule) {
        return $('FormRules_' + rule.RuleId + '_Setting_SendEmail_Type').innerHTML.strip();
    },
    getConfirmationEmailField: function (rule) {
        var el = $('FormRules_' + rule.RuleId + '_Setting_ConfirmationEmail_Value_Dropdown');
        return Util.getRev(el);
    },
    getConfirmationEmailIncludeEntry: function (rule) {
        var el = $('FormRules_' + rule.RuleId + '_Setting_ConfirmationEmail_IncludeEntry');
        return el.checked ? '1' : '0';
    },
    updateSkipToPageRule: function (rule) {
        rule.Type = 'SkipToPage';
        rule.Setting = new Object();
        rule.Setting.Page = Util.getRev('PageRules_' + rule.RuleId + '_Setting_Page');
    },
    updateShowHideRule: function (rule) {
        rule.Type = Util.getRev('FieldRules_' + rule.RuleId + '_Type_ShowHide');
        rule.Setting = new Object();
        rule.Setting.FieldName = Util.getRev('FieldRules_' + rule.RuleId + '_Setting');
        this.updateShowHideConditionFieldTypes(rule);
    },
    updateShowHideConditionFieldTypes: function (rule) {
        rule.Setting.FieldTypes = new Object();
        rule.Conditions.each(function (condition) {
            rule.Setting.FieldTypes[condition.FieldName] = this.getFieldTypeof(condition.FieldName);
        }.bind(this));
    },
    updateRuleCondition: function (ruleType, rule, value, conditionId, conditionAttribute) {
        var condition = rule.Conditions.find(function (condition) {
            return (condition.ConditionId == conditionId);
        }.bind(this));
        if (conditionAttribute == 'FieldName') {
            value = Util.getRev(ruleType + '_' + rule.RuleId + '_Condition_' + conditionId + '_FieldName');
        } else if (conditionAttribute == 'Filter') {
            var filterIndex = Util.getRev(ruleType + '_' + rule.RuleId + '_Condition_' + conditionId + '_Filter');
            value = this.filterValueHash[filterIndex];
        } else if (conditionAttribute == 'Value') {
            value = this.formatValue(condition.FieldName, value);
        } else if (conditionAttribute == 'ChoiceValue') {
            conditionAttribute = 'Value';
            value = this.getChoiceValue(ruleType, rule, value, conditionId, conditionAttribute);
        }
        condition[conditionAttribute] = Util.cleanValue(value);
        if (conditionAttribute == 'FieldName' && ruleType == 'FieldRules') {
            this.updateShowHideConditionFieldTypes(rule);
        }
    },
    getChoiceValue: function (ruleType, rule, value, conditionId, conditionAttribute) {
        var value = '';
        var choiceValueEl = $(ruleType + '_' + rule.RuleId + '_Condition_' + conditionId + '_ChoiceValue');
        try {
            var columnId = choiceValueEl.type.replace('field', '');
            var subId = choiceValueEl.rev.replace('dd', '');
            value = __CHOICES[columnId][subId]['Text'];
        } catch (e) {}
        if (value == '') {
            var link = __DD.activeSelectedChoice;
            var lowerHTML = link.innerHTML.toLowerCase();
            value = link.innerHTML.substring(0, lowerHTML.indexOf('<b>'));
        }
        return value;
    },
    formatValue: function (columnId, value) {
        var fieldTypeof = this.getFieldTypeof(columnId);
        if (fieldTypeof == 'phone') {
            var valueArray = value.split('-');
            value = '';
            for (var i = 0; i < valueArray.length; i++) {
                value += valueArray[i];
            }
        }
        return value;
    },
    getInstanceOfFormRule: function () {
        var formRule = new Object();
        formRule.RuleId = this.nextRuleId();
        formRule.Type = 'ShowMessage';
        formRule.Setting = new Object();
        formRule.Setting.Message = this.formRuleSettingMsg;
        formRule.FormId = this.form.FormId;
        formRule.MatchType = 'any';
        formRule.Conditions = new Array();
        formRule.Conditions[0] = new Object();
        formRule.Conditions[0].ConditionId = this.nextConditionId();
        var firstField = this.getFirstField(['section', 'file']);
        formRule.Conditions[0].FieldName = firstField.ColumnId;
        formRule.Conditions[0].Filter = this.getFilterByField(firstField);
        formRule.Conditions[0].Value = this.getValueTypeByField(firstField);
        formRule.Conditions[0].ReportId = this.form.FormId;
        return formRule;
    },
    getInstanceOfPageRule: function () {
        var formRule = new Object();
        formRule.RuleId = this.nextRuleId();
        formRule.Type = 'SkipToPage';
        formRule.Setting = new Object();
        formRule.Setting.Page = 1;
        formRule.FormId = this.form.FormId;
        formRule.MatchType = 'any';
        formRule.Conditions = new Array();
        formRule.Conditions[0] = new Object();
        formRule.Conditions[0].ConditionId = this.nextConditionId();
        var firstField = this.getFirstField(['section', 'file']);
        formRule.Conditions[0].FieldName = firstField.ColumnId;
        formRule.Conditions[0].Filter = this.getFilterByField(firstField);
        formRule.Conditions[0].Value = this.getValueTypeByField(firstField);
        formRule.Conditions[0].ReportId = this.form.FormId;
        return formRule;
    },
    getInstanceOfFieldRule: function () {
        var formRule = new Object();
        formRule.RuleId = this.nextRuleId();
        formRule.Type = 'Show';
        formRule.Setting = new Object();
        var settingFirstField = this.getFirstField();
        formRule.Setting.FieldName = settingFirstField.ColumnId;
        formRule.Setting.FieldTypes = new Object();
        formRule.FormId = this.form.FormId;
        formRule.MatchType = 'any';
        formRule.Conditions = new Array();
        formRule.Conditions[0] = new Object();
        formRule.Conditions[0].ConditionId = this.nextConditionId();
        var firstField = this.getFirstField(['section', 'file']);
        formRule.Conditions[0].FieldName = firstField.ColumnId;
        formRule.Conditions[0].Filter = this.getFilterByField(firstField);
        formRule.Conditions[0].Value = this.getValueTypeByField(firstField);
        formRule.Conditions[0].ReportId = this.form.FormId;
        this.updateShowHideConditionFieldTypes(formRule);
        return formRule;
    },
    getFirstField: function (ignoreList, pageRequirement) {
        ignoreList = ignoreList || [];
        pageRequirement = pageRequirement || 0;
        var field = this.form.Fields.find(function (field) {
            if (ignoreList.indexOf(field.Typeof) == -1) {
                if (pageRequirement > 0) {
                    if (field.Page == pageRequirement) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return true;
                }
            }
        }.bind(this));
        return field;
    },
    getFilterByField: function (field) {
        if (field.ColumnId == 'EntryId' || this.treatAsInteger(field.Typeof)) {
            return 'is equal to';
        } else if (field.Typeof == 'time') {
            return 'is at';
        } else if (field.Typeof == 'date' || field.Typeof == 'eurodate' || field.ColumnId == 'DateCreated') {
            return 'is on';
        } else {
            return 'is';
        }
    },
    treatAsInteger: function (type) {
        return (type == 'number' || type == 'money') ? true : false;
    },
    getValueTypeByField: function (field) {
        if (this.isChoicesType(field)) {
            var choice = this.choiceHash[field.ColumnId];
            var subId = false;
            for (i in choice) {
                if (!subId) {
                    subId = i;
                    break;
                }
            }
            var choiceObj = this.choiceHash[field.ColumnId][subId];
            return choiceObj.Text;
        } else {
            return '';
        }
    },
    isChoicesType: function (field) {
        if (Util.isEmpty(this.choiceHash[field.ColumnId])) {
            return false;
        } else {
            return true;
        }
    },
    nextRuleId: function () {
        var ruleId = 'TempRule' + this.ruleIdCounter;
        this.ruleIdCounter = this.ruleIdCounter + 1;
        return ruleId;
    },
    nextConditionId: function () {
        var conditionId = 'TempCondition' + this.conditionIdCounter;
        this.conditionIdCounter = this.conditionIdCounter + 1;
        return conditionId;
    }
});
var RuleBuilderEngine = Class.create({
    URL: '',
    formId: '',
    formName: '', //<sl:translate>
    addRuleStatus: 'Adding rule ...',
    addingConditionStatus: 'Adding condition ...',
    saveRulesEnabledStatus: 'Saving Rules Enabled ...',
    saveStatus: 'Saving Rules ...', //</sl:translate>
    initialize: function (formId, formURL, formName) {
        this.URL = 'index.php?req=rules/' + formURL + '';
        this.formId = formId;
        this.formName = formName;
    },
    addRule: function (ruleId, rule, ruleType) {
        $('statusText').innerHTML = this.addRuleStatus;
        var myAjax = new Ajax.Request(this.URL, {
            method: 'post',
            parameters: 'action=addRule&ruleId=' + ruleId + '&ruleType=' + ruleType + '&rule=' + encodeURIComponent(Object.toJSON(rule)),
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishAddRule(ret);
            }.bind(this)
        });
    },
    addCondition: function (conditionId, rule, ruleType) {
        $('statusText').innerHTML = this.addingConditionStatus;
        var myAjax = new Ajax.Request(this.URL, {
            method: 'post',
            parameters: 'action=addCondition&conditionId=' + conditionId + '&ruleType=' + ruleType + '&rule=' + encodeURIComponent(Object.toJSON(rule)),
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishAddCondition(ret);
            }.bind(this)
        });
    },
    saveRulesEnabled: function (rulesEnabled) {
        $('statusText').innerHTML = this.saveRulesEnabledStatus;
        var myAjax = new Ajax.Request(this.URL, {
            method: 'post',
            parameters: 'action=saveRulesEnabled&rulesEnabled=' + rulesEnabled + '&formId=' + this.formId,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishSaveRulesEnabled(ret);
            }.bind(this)
        });
    },
    save: function (rules, ruleType) {
        $('statusText').innerHTML = this.saveStatus;
        var myAjax = new Ajax.Request(this.URL, {
            method: 'post',
            parameters: 'action=save&ruleType=' + ruleType + '&formId=' + this.formId + '&formName=' + encodeURIComponent(this.formName) + '&rules=' + encodeURIComponent(Object.toJSON(rules)),
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishSave(ret);
            }.bind(this)
        });
    }
});
var FieldRules = Class.create(RulesBase, {
    rulesObject: '',
    choiceHash: '',
    initialize: function (rulesObject) {
        this.rulesObject = rulesObject;
        this.choiceHash = __CHOICES;
        this.initDateTypeConditions('#fieldRules');
    },
    addRule: function (ruleId, html) {
        $('fieldRulesWarning').addClassName('hide');
        $('fieldRulesTop').removeClassName('hide');
        if (Util.isEmpty(ruleId)) $('fieldRules').insert({
            bottom: html
        });
        else $('FieldRule_' + ruleId).insert({
            after: html
        });
        this.updateRuleCounter($$('#fieldRules li a.index'));
        this.initDateTypeConditions('#fieldRules');
    },
    update: function (rule, attribute, value, conditionId, conditionAttribute) {
        if (conditionAttribute == 'FieldName' || conditionAttribute == 'Filter') {
            this.fieldNameChange('FieldRules', rule, value, conditionId);
        } else if (conditionAttribute == 'ChoiceValue') {
            this.choiceValueSelected('FieldRule', rule, conditionId);
        } else if (conditionAttribute == 'MatchType') {
            this.matchTypeChange('FieldRules', rule);
        }
    },
    deleteRule: function (ruleId) {
        $('FieldRule_' + ruleId).addClassName('hide');
        $('FieldRule_' + ruleId).remove();
        if ($$('#fieldRules li').length < 1) {
            $('fieldRules').up().addClassName('hide');
            $('fieldRulesWarning').removeClassName('hide');
        }
        this.updateRuleCounter($$('#fieldRules li a.index'));
    }
});
var PageRules = Class.create(RulesBase, {
    rulesObject: '',
    choiceHash: '',
    initialize: function (rulesObject) {
        this.rulesObject = rulesObject;
        this.choiceHash = __CHOICES;
        this.initDateTypeConditions('#pageRules');
    },
    addRule: function (ruleId, html) {
        $('pageRulesWarning').addClassName('hide');
        $('pageRulesTop').removeClassName('hide');
        if (Util.isEmpty(ruleId)) $('pageRules').insert({
            bottom: html
        });
        else $('PageRule_' + ruleId).insert({
            after: html
        });
        this.updateRuleCounter($$('#pageRules li a.index'));
        this.initDateTypeConditions('#pageRules');
    },
    update: function (rule, attribute, value, conditionId, conditionAttribute) {
        if (conditionAttribute == 'FieldName' || conditionAttribute == 'Filter') {
            this.fieldNameChange('PageRules', rule, value, conditionId);
        } else if (conditionAttribute == 'ChoiceValue') {
            this.choiceValueSelected('PageRule', rule, conditionId);
        } else if (conditionAttribute == 'MatchType') {
            this.matchTypeChange('PageRules', rule);
        }
    },
    deleteRule: function (ruleId) {
        $('PageRule_' + ruleId).addClassName('hide');
        $('PageRule_' + ruleId).remove();
        if ($$('#pageRules li').length < 1) {
            $('pageRules').up().addClassName('hide');
            $('pageRulesWarning').removeClassName('hide');
        }
        this.updateRuleCounter($$('#pageRules li a.index'));
    }
});
var FormRules = Class.create(RulesBase, {
    rulesObject: '',
    choiceHash: '',
    initialize: function (rulesObject) {
        this.rulesObject = rulesObject;
        this.choiceHash = __CHOICES;
        this.initDateTypeConditions('#formRules');
    },
    addRule: function (ruleId, html) {
        $('formRulesWarning').addClassName('hide');
        $('formRulesTop').removeClassName('hide');
        if (Util.isEmpty(ruleId)) $('formRules').insert({
            bottom: html
        });
        else $('FormRule_' + ruleId).insert({
            after: html
        });
        this.updateRuleCounter($$('#formRules li a.index'));
        this.initDateTypeConditions('#formRules');
    },
    update: function (rule, attribute, value, conditionId, conditionAttribute) {
        if (attribute == 'Type') {
            this.selectFormRuleType(rule.RuleId, value);
        } else if (attribute == 'Setting') {
            this.settingChange(rule, value, conditionId);
        } else if (conditionAttribute == 'FieldName' || conditionAttribute == 'Filter') {
            this.fieldNameChange('FormRules', rule, value, conditionId);
        } else if (conditionAttribute == 'ChoiceValue') {
            this.choiceValueSelected('FormRule', rule, conditionId);
        } else if (conditionAttribute == 'MatchType') {
            this.matchTypeChange('FormRules', rule);
        }
    },
    settingChange: function (rule, value, conditionId) {
        if (value == 'SendEmailConfirmation' || conditionId == 'ConfirmationEmail') {
            $('FormRules_' + rule.RuleId + '_Setting_ConfirmationEmail_Value_Dropdown').removeClassName('hide');
            $('FormRules_' + rule.RuleId + '_Setting_ConfirmationEmail_Attributes').removeClassName('hide');
            $('FormRules_' + rule.RuleId + '_Setting_SendEmail_Value_Text').addClassName('hide');
            $('FormRules_' + rule.RuleId + '_Setting_SendEmail_Value_Label').addClassName('hide');
        } else {
            $('FormRules_' + rule.RuleId + '_Setting_ConfirmationEmail_Value_Dropdown').addClassName('hide');
            $('FormRules_' + rule.RuleId + '_Setting_ConfirmationEmail_Attributes').addClassName('hide');
            $('FormRules_' + rule.RuleId + '_Setting_SendEmail_Value_Text').removeClassName('hide');
            $('FormRules_' + rule.RuleId + '_Setting_SendEmail_Value_Label').removeClassName('hide');
        }
    },
    selectFormRuleType: function (ruleId, type) {
        if (type == 'ShowMessage') {
            $('FormRule_' + ruleId).className = 'threeColumns sM';
            $('FormRules_' + ruleId + '_Setting_ShowMessage_Div').removeClassName('hide');
            $('FormRules_' + ruleId + '_Setting_Redirect_Div').addClassName('hide');
            $('FormRules_' + ruleId + '_Setting_SendEmail_Div').addClassName('hide');
        } else if (type == 'Redirect') {
            $('FormRule_' + ruleId).className = 'threeColumns rW';
            $('FormRules_' + ruleId + '_Setting_Redirect_Div').removeClassName('hide');
            $('FormRules_' + ruleId + '_Setting_ShowMessage_Div').addClassName('hide');
            $('FormRules_' + ruleId + '_Setting_SendEmail_Div').addClassName('hide');
        } else {
            $('FormRule_' + ruleId).className = 'threeColumns sE';
            $('FormRules_' + ruleId + '_Setting_SendEmail_Div').removeClassName('hide');
            $('FormRules_' + ruleId + '_Setting_Redirect_Div').addClassName('hide');
            $('FormRules_' + ruleId + '_Setting_ShowMessage_Div').addClassName('hide');
        }
    },
    deleteRule: function (ruleId) {
        $('FormRule_' + ruleId).addClassName('hide');
        $('FormRule_' + ruleId).remove();
        if ($$('#formRules li').length < 1) {
            $('formRules').up().addClassName('hide');
            $('formRulesWarning').removeClassName('hide');
        }
        this.updateRuleCounter($$('#formRules li a.index'));
    }
});
var RuleBuilder = Class.create({
    interface: new Interface(),
    ruleBuilderEngine: '',
    fieldRules: '',
    pageRules: '',
    formRules: '',
    rulesObject: '',
    form: '',
    accountType: '',
    addingRule: false, //<sl:translate>
    oneRuleLeft: 'You have <strong class="notranslate">1</strong> rule left for this form.',
    fewRulesLeft: 'You have <strong class="notranslate">#{leftKey}</strong> rules left for this form.',
    noValidFieldsWarning: 'Your form has no valid fields. Add other types of fields to your form to create rules.', //</sl:translate>
    initialize: function () {
        $('footer_rules_left') ? this.accountType = 1 : this.accountType = 2;
        this.form = __FORM;
        this.ruleBuilderEngine = new RuleBuilderEngine(this.form.FormId, this.form.Url, this.form.Name);
        this.rulesObject = new RulesObject(this.form, __RULES);
        this.fieldRules = new FieldRules(this.rulesObject);
        this.pageRules = new PageRules(this.rulesObject);
        this.formRules = new FormRules(this.rulesObject);
        registerObserver(this);
        this.updateRulesLeft();
        this.interface.hideStatus();
    },
    selectTab: function (el) {
        Element.extend(el);
        $('logic').className = el.up().id;
        this.updateRulesLeft();
    },
    updateRulesLeft: function (ruleType) {
        if (this.accountType == 1) {
            $('footer_rules_left').removeClassName('hide');
            var count = this.rulesObject.rules['FieldRules'].length;
            count = count + this.rulesObject.rules['PageRules'].length;
            var left = 99999 - count;
            if (left == 1) {
                $('footer_rules_left').innerHTML = this.oneRuleLeft;
            } else {
                var tpl = new Template(this.fewRulesLeft);
                var msg = tpl.evaluate({
                    leftKey: left
                });
                $('footer_rules_left').innerHTML = msg;
            }
        }
    },
    updateDropdown: function (el) {
        var value = Util.getRev(el);
        this.updateRule(el, value);
    },
    update: function (el) {
        var value = el.value;
        this.updateRule(el, value);
    },
    updateRule: function (el, value) {
        var idArray = el.id.split('_');
        var ruleType = idArray[0];
        var ruleId = idArray[1];
        var attribute = idArray[2];
        var conditionId = idArray[3];
        var conditionAttribute = idArray[4];
        var rule = this.rulesObject.getRule(ruleType, ruleId);
        this.rulesObject.updateRule(ruleType, rule, attribute, value, conditionId, conditionAttribute);
        this.updateRuleType(ruleType, rule, attribute, value, conditionId, conditionAttribute);
    },
    updateRuleType: function (ruleType, rule, attribute, value, conditionId, conditionAttribute) {
        this.getUIHandler(ruleType).update(rule, attribute, value, conditionId, conditionAttribute);
    },
    getUIHandler: function (ruleType) {
        if (ruleType == 'FormRules') {
            return this.formRules;
        } else if (ruleType == 'PageRules') {
            return this.pageRules;
        } else if (ruleType == 'FieldRules') {
            return this.fieldRules;
        }
    },
    saveRulesEnabled: function () {
        var rulesEnabled = ($('RulesEnabled').checked) ? '1' : '0';
        this.ruleBuilderEngine.saveRulesEnabled(rulesEnabled);
    },
    failedSaveRulesEnabled: function (ret) {
        Lightbox.showContent(ret.response.lbmarkup);
    },
    successfulSaveRulesEnabled: function (ret) {},
    save: function (ruleType) {
        var rules = this.rulesObject.rules[ruleType];
        this.ruleBuilderEngine.save(rules, ruleType);
        if (this.form.RulesEnabled == 2) {
            this.saveRulesEnabled();
        }
    },
    failedSave: function (ret) {
        Lightbox.showContent(ret.response.lbmarkup);
    },
    successfulSave: function (ret) {},
    saveWhenZeroRulesLeft: function (ruleType) {
        if (this.rulesObject.rules[ruleType].length == 0) {
            if (this.accountType == 1 && ruleType == 'FormRules') {
                return;
            }
            this.save(ruleType);
        }
    },
    failedAddRule: function (ret) {
        this.addingRule = false;
        Lightbox.showContent(ret.response.lbmarkup);
    },
    successfulAddRule: function (ret) {
        this.addingRule = false;
        if (ret.response.ruleType == 'FormRule') {
            this.formRules.addRule(ret.response.ruleId, ret.response.html);
            this.rulesObject.addRule('FormRules', ret.response.rule.evalJSON());
        } else if (ret.response.ruleType == 'PageRule') {
            this.pageRules.addRule(ret.response.ruleId, ret.response.html);
            this.rulesObject.addRule('PageRules', ret.response.rule.evalJSON());
            this.updateRulesLeft('PageRules');
        } else if (ret.response.ruleType == 'FieldRule') {
            this.fieldRules.addRule(ret.response.ruleId, ret.response.html);
            this.rulesObject.addRule('FieldRules', ret.response.rule.evalJSON());
            this.updateRulesLeft('FieldRules');
        }
    },
    allowRuleAddition: function (ruleType) {
        if (this.addingRule) {
            return false;
        }
        var hasValidFields = this.form.Fields.find(function (field) {
            return (field.Typeof != 'file' && typeof (field.Category) == 'undefined');
        });
        if (!hasValidFields) {
            alert(this.noValidFieldsWarning);
            return false;
        }
        if (this.accountType == 1) {
            if (ruleType == 'FormRules') {
                var count = this.rulesObject.rules['FormRules'].length;
                if (count > 99999) {
                    Lightbox.showUrl('lightboxes/Rules.Maximum.Free.php', function () {}, 'error');
                    return false;
                } else {
                    return true;
                }
            } else {
                var count = this.rulesObject.rules['FieldRules'].length;
                count = count + this.rulesObject.rules['PageRules'].length;
                if (count > 99999) {
                    Lightbox.showUrl('lightboxes/Rules.Maximum.Free.php', function () {}, 'error');
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            var count = this.rulesObject.rules[ruleType].length;
            if (count > 99999) {
                Lightbox.showUrl('lightboxes/Rules.Maximum.Paid.php', function () {}, 'error');
                return false;
            } else {
                return true;
            }
        }
    },
    duplicateCondition: function (ruleType, ruleId, conditionId) {
        var rule = this.rulesObject.getRule(ruleType, ruleId);
        if (this.allowConditionAddition(rule)) {
            var singleConditionRule = this.rulesObject.duplicateCondition(ruleType, rule, conditionId);
            this.ruleBuilderEngine.addCondition(conditionId, singleConditionRule, ruleType);
        }
    },
    allowConditionAddition: function (rule) {
        if (rule.Conditions.length > 49) {
            Lightbox.showUrl('lightboxes/Rules.Maximum.Conditions.php', function () {}, 'error');
            return false;
        } else {
            return true;
        }
    },
    failedAddCondition: function (ret) {
        Lightbox.showContent(ret.response.lbmarkup);
    },
    successfulAddCondition: function (ret) {
        var rule = ret.response.rule.evalJSON();
        this.rulesObject.addCondition(ret.response.ruleType, rule);
        var uiHandler = this.getUIHandler(ret.response.ruleType);
        uiHandler.addCondition(ret.response.ruleType, rule, ret.response.conditionId, ret.response.html);
    },
    deleteCondition: function (ruleType, ruleId, conditionId) {
        this.rulesObject.deleteCondition(ruleType, ruleId, conditionId);
        this.getUIHandler(ruleType).deleteCondition(ruleType, ruleId, conditionId);
    },
    addFormRule: function () {
        if (this.allowRuleAddition('FormRules')) {
            this.addingRule = true;
            var formRule = this.rulesObject.getInstanceOfFormRule();
            this.ruleBuilderEngine.addRule(0, formRule, 'FormRule');
        }
    },
    duplicateFormRule: function (el, ruleId) {
        if (this.allowRuleAddition('FormRules')) {
            this.addingRule = true;
            var formRule = this.rulesObject.duplicateRule('FormRules', ruleId);
            this.ruleBuilderEngine.addRule(ruleId, formRule, 'FormRule');
        }
    },
    deleteFormRule: function (el, ruleId) {
        this.rulesObject.deleteRule(ruleId, 'FormRules');
        this.formRules.deleteRule(ruleId);
        this.saveWhenZeroRulesLeft('FormRules');
    },
    saveFormRules: function () {
        this.save('FormRules');
    },
    addPageRule: function () {
        if (this.allowRuleAddition('PageRules')) {
            this.addingRule = true;
            var pageRule = this.rulesObject.getInstanceOfPageRule();
            this.ruleBuilderEngine.addRule(0, pageRule, 'PageRule');
        }
    },
    duplicatePageRule: function (el, ruleId) {
        if (this.allowRuleAddition('PageRules')) {
            this.addingRule = true;
            var pageRule = this.rulesObject.duplicateRule('PageRules', ruleId);
            this.ruleBuilderEngine.addRule(ruleId, pageRule, 'PageRule');
        }
    },
    deletePageRule: function (el, ruleId) {
        this.rulesObject.deleteRule(ruleId, 'PageRules');
        this.pageRules.deleteRule(ruleId);
        this.updateRulesLeft('PageRules');
        this.saveWhenZeroRulesLeft('PageRules');
    },
    savePageRules: function (el, ruleId) {
        this.save('PageRules');
    },
    addFieldRule: function () {
        if (this.allowRuleAddition('FieldRules')) {
            this.addingRule = true;
            var fieldRule = this.rulesObject.getInstanceOfFieldRule();
            this.ruleBuilderEngine.addRule(0, fieldRule, 'FieldRule');
        }
    },
    duplicateFieldRule: function (el, ruleId) {
        if (this.allowRuleAddition('FieldRules')) {
            this.addingRule = true;
            var fieldRule = this.rulesObject.duplicateRule('FieldRules', ruleId);
            this.ruleBuilderEngine.addRule(ruleId, fieldRule, 'FieldRule');
        }
    },
    deleteFieldRule: function (el, ruleId) {
        this.rulesObject.deleteRule(ruleId, 'FieldRules');
        this.fieldRules.deleteRule(ruleId);
        this.saveWhenZeroRulesLeft('FieldRules');
    },
    saveFieldRules: function (el, ruleId) {
        this.save('FieldRules');
        this.showRuleConflicts();
    },
    showRuleConflicts: function () {
        var fieldRules = this.rulesObject.rules['FieldRules'];
        var conflicts = new Object();
        for (var i = 0; i < fieldRules.length; i++) {
            var rule = fieldRules[i];
            if (Object.isUndefined(conflicts[rule.Setting.FieldName])) {
                conflicts[rule.Setting.FieldName] = new Array(rule.RuleId);
            } else {
                conflictsArray = conflicts[rule.Setting.FieldName];
                conflictsArray.push(rule.RuleId);
            }
        }
        var keys = Object.keys(conflicts);
        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            var conflictsArray = conflicts[key];
            for (var j = 0; j < conflictsArray.length; j++) {
                var ruleId = conflictsArray[j];
                var li = $('FieldRule_' + ruleId);
                if (conflictsArray.length > 1) {
                    li.addClassName('conflict');
                } else {
                    li.removeClassName('conflict');
                }
            }
        }
    }
});
var __RB;
Event.observe(window, 'load', init, false);

function init() {
    __RB = new RuleBuilder();
}

function selectTab(el) {
    __RB.selectTab(el);
}

function selectFormRuleType(conditionNum, type) {
    __RB.selectFormRuleType(conditionNum, type);
}

function rb_update(el) {
    Element.extend(el);
    __RB.update(el);
}

function finishAddRule(ret) {
    if (ret.success == 'false') __RB.failedAddRule(ret);
    else __RB.successfulAddRule(ret);
}

function finishAddCondition(ret) {
    if (ret.success == 'false') __RB.failedAddCondition(ret);
    else __RB.successfulAddCondition(ret);
}

function updateDropdown(el) {
    __RB.updateDropdown(el);
}

function saveRulesEnabled() {
    __RB.saveRulesEnabled();
}

function finishSaveRulesEnabled(ret) {
    if (ret.success == 'false') __RB.failedSaveRulesEnabled(ret);
    else __RB.successfulSaveRulesEnabled(ret);
}

function finishSave(ret) {
    if (ret.success == 'false') __RB.failedSave(ret);
    else __RB.successfulSave(ret);
}

function addFormRule() {
    __RB.addFormRule();
}

function duplicateFormRule(el, ruleId) {
    __RB.duplicateFormRule(el, ruleId);
}

function deleteFormRule(el, ruleId) {
    __RB.deleteFormRule(el, ruleId);
}

function saveFormRules() {
    __RB.saveFormRules();
}

function addPageRule() {
    __RB.addPageRule();
}

function duplicatePageRule(el, ruleId) {
    __RB.duplicatePageRule(el, ruleId);
}

function deletePageRule(el, ruleId) {
    __RB.deletePageRule(el, ruleId);
}

function savePageRules() {
    __RB.savePageRules();
}

function duplicateCondition(ruleType, ruleId, conditionId) {
    __RB.duplicateCondition(ruleType, ruleId, conditionId);
}

function deleteCondition(ruleType, ruleId, conditionId) {
    __RB.deleteCondition(ruleType, ruleId, conditionId);
}

function addFieldRule() {
    __RB.addFieldRule();
}

function duplicateFieldRule(el, ruleId) {
    __RB.duplicateFieldRule(el, ruleId);
}

function deleteFieldRule(el, ruleId) {
    __RB.deleteFieldRule(el, ruleId);
}

function saveFieldRules() {
    __RB.saveFieldRules();
}