var AccountEngineObject = Class.create({
    updateAccount: function($account) {
        var url = new String(document.location);
        var pars =  $('user_info').serialize(true);
        pars.action = 'update';
        var myAjax = new Ajax.Request(url, {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == 'false') {
                    alert(result.response.message);
                }
            }
        });
        return;
    },
    changePassword: function() {
        var password=$F('password');
        var confirm=$F('confirm');
        var userId=$F('userId');
        var url = new String(document.location);
        var pars='action=changepass&password='+encodeURIComponent(password)+'&confirm='+encodeURIComponent(confirm);
        var myAjax = new Ajax.Request(url, {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == 'false') {
                    alert(result.response.message);
                } else {
                    Lightbox.close();
                }
            }
        });
        return;
    },
    selectUser: function(user, user_id) {
        var pars='action=selectUser&user_id='+encodeURIComponent(user_id);
        var loaded_user = $('block_user_'+user_id);
        $('userList').select('li').invoke('removeClassName','selected');
        if(!loaded_user) {
            var myAjax = new Ajax.Request(new String(document.location), {
                parameters: pars,
                onComplete: function (response) {
                    var result = response.responseText.evalJSON();
                    if(result.success) {
                        $('entries').down().down('tbody').innerHTML = result.response.html;
                        $(user).className = 'selected';
                        buildLoadedUser(user_id,result.response.html);
                        addHovers($$('#entries .excerpts a'));
                    }
                }
            });
        } else {
            $(user).className = 'selected';
            $('entries').down().down('tbody').innerHTML = loaded_user.innerHTML;
        }
    },
    selectGroup: function(group, group_id) {
        var pars='action=selectGroup&group_id='+encodeURIComponent(group_id);
        var loaded_group = $('block_group_'+group_id);
        $('groupList').select('li').invoke('removeClassName','selected');
        if(!loaded_group) {
            var myAjax = new Ajax.Request(new String(document.location), {
                parameters: pars,
                onComplete: function (response) {
                    var result = response.responseText.evalJSON();
                    if(result.success) {
                        $('userList').innerHTML = result.response.html;
                        $(group).className = 'selected';
                        buildLoadedGroup(group_id,result.response.html);
                    }
                }
            });
        } else {
            $(group).className = 'selected';
            $('userList').innerHTML = loaded_group.innerHTML;
        }
    },
    buildLoadedGroup: function(group_id,html) {
        var group_loaded = $('loaded_group');
        var group = document.createElement('div');
        group.id = 'block_group_'+group_id;
        group.innerHTML = html;
        group_loaded.appendChild(group);
    },
    buildLoadedUser: function(user_id,html) {
        var user_loaded = $('loaded_user');
        var user = document.createElement('table');
        user.id = 'block_user_'+user_id;
        user.innerHTML = html;
        user_loaded.appendChild(user);
    }
});
var EntriesEngineObject = Class.create({
    goToPage: function($paged,$page) {
        var $upPage = $page.up().up();
        var action = $upPage.up().id;
        var sortby = 'dc';
        var direction = 'asc';
        //var custom_sort = $upPage.up().previous().select('.sorts .button.active');
        //var custom_dir = $upPage.up().previous().select('.sorts select[name="direction"]');
        //if(custom_sort.length > 0) {
//            sortby = custom_sort['0'].rel;
//        }
//        if(custom_dir.length > 0) {
//            direction = custom_dir['0'].value;
//        }
        var pars='action='+action+'&paged='+encodeURIComponent($paged);//+'&sortby='+sortby+'&direction='+direction;
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success) {
                    $upPage.innerHTML = result.response.paged;
                    $upPage.up().previous().down('ul').innerHTML = result.response.html;
                    //var summaries = $upPage.up().previous().select('tbody a.summary');
                    //var count = summaries.length;
                    //if($$('.nocache').length > 0) this.loadSummary(summaries,count);
                    //this.addHovers($upPage.up().previous().select('tbody .excerpts a'));
                }
            }
        });
        return;
    },
    addHovers: function(links) {
        links.each(function(link) {
            link.observe('mouseover', function(event) {
                var src = link.href;
                var preview = this.up().select('.preview')['0'];
                var newobj=document.createElement('img');
                newobj.src = src;
                newobj.style.width = '100px';
                preview.appendChild(newobj);
            }).observe('mouseout', function(event) {
                this.up().select('.preview')['0'].innerHTML = '';
            });
          
        });
    }
});
var __MANAGER,__ENTRY;
Event.observe(window, 'load', init, false);
function init() {
    __MANAGER = new AccountEngineObject();
    __ENTRY = new EntriesEngineObject();
    if($$('.excerpts a').length > 0) addHovers($$('.excerpts a'));
}
function selectUser($user, $user_id) {
    __MANAGER.selectUser($user, $user_id);
}
function selectGroup($user, $user_id) {
    __MANAGER.selectGroup($user, $user_id);
}
function buildLoadedGroup(group_id, html) {
    __MANAGER.buildLoadedGroup(group_id, html);
}
function buildLoadedUser(user_id, html) {
    __MANAGER.buildLoadedUser(user_id, html);
}
function goToPage($paged,$page) {
    __ENTRY.goToPage($paged,$page);
}
function addHovers(links) {
    __ENTRY.addHovers(links);
}