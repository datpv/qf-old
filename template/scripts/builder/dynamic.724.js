var Cookie = Class.create({
    data: {},
    options: {
        expires: 1,
        domain: '.' + document.domain.split('.')[document.domain.split('.').length - 2] + '.' + document.domain.split('.')[document.domain.split('.').length - 1],
        path: "",
        secure: false
    },
    init: function (options, data) {
        this.options = Object.extend(this.options, options || {});
        var payload = this.retrieve();
        if (payload) {
            this.data = payload.evalJSON();
        } else {
            this.data = data || {};
        }
        this.store();
    },
    getData: function (key) {
        return this.data[key];
    },
    setData: function (key, value) {
        this.data[key] = value;
        this.store();
    },
    removeData: function (key) {
        delete this.data[key];
        this.store();
    },
    retrieve: function () {
        var start = document.cookie.indexOf(this.options.name + "=");
        if (start == -1) {
            return null;
        }
        if (this.options.name != document.cookie.substr(start, this.options.name.length)) {
            return null;
        }
        var len = start + this.options.name.length + 1;
        var end = document.cookie.indexOf(';', len);
        if (end == -1) {
            end = document.cookie.length;
        }
        return unescape(document.cookie.substring(len, end));
    },
    store: function () {
        var expires = '';
        if (this.options.expires) {
            var today = new Date();
            expires = this.options.expires * 86400000;
            expires = ';expires=' + new Date(today.getTime() + expires);
        }
        document.cookie = this.options.name + '=' + escape(Object.toJSON(this.data)) + this.getOptions() + expires;
    },
    erase: function () {
        document.cookie = this.options.name + '=' + this.getOptions() + ';expires=Thu, 01-Jan-1970 00:00:01 GMT';
    },
    getOptions: function () {
        return (this.options.path ? ';path=' + this.options.path : '') + (this.options.domain ? ';domain=' + this.options.domain : '') + (this.options.secure ? ';secure' : '');
    },
    getIFrameDefault: function () {
        return {
            name: 'wuIFrame',
            expires: 30,
            path: '/'
        };
    }
});
var DragDropObject = Class.create({
    dragging: false,
    dragpos: -1,
    columns: new Array('col1', 'col2', 'col3', 'col4'),
    initialize: function () {
        Sortable.SERIALIZE_RULE = /^[^_\-][foli](.*)$/;
        this.grantFieldDragAbility();
        for (var i = 0; i < this.columns.length; i++) {
            this.grantMenuDragAbility(this.columns[i]);
        }
    },
    grantFieldDragAbility: function () {
        Sortable.create("formFields", {
            dropOnEmpty: false,
            containment: ["formFields", "col1", "col2", "col3", "col4"],
            constraint: "vertical",
            scroll: window,
            only: 'dragable',
            onUpdate: reorderField
        });
    },
    grantMenuDragAbility: function (col) {
        Sortable.create(col, {
            dropOnEmpty: false,
            containment: [],
            constraint: false,
            scroll: window,
            onUpdate: addFieldAfterDraggingComplete
        });
    },
    returnButtonToAddFieldMenu: function (dragDropBox) {
        var menu = this.findMenuButtonBelongsTo(dragDropBox);
        dragDropBox.removeClassName('hide');
        var elPos = dragDropBox.className.replace("dragfld ", "").replace(" bigger", "").replace(" biggest", "").replace(" wide", "");
        if (menu.down('li', elPos)) {
            menu.down('li', elPos).insert({
                before: dragDropBox
            });
        } else menu.appendChild(dragDropBox);
    },
    findMenuButtonBelongsTo: function (button) {
        if (button.id && button.id.startsWith('drag1')) return $('col1');
        else if (button.id && button.id.startsWith('drag2')) return $('col2');
        else if (button.id && button.id.startsWith('drag3')) return $('col3');
        else return $('col4');
    }
});
var ReorderObject = Class.create({
    Form: '',
    FieldProperties: '',
    FieldPreview: '',
    FormEngine: '',
    position: 0,
    displayPosition: 0,
    extraPosition: 0,
    extraDisplayPosition: 0,
    currentPage: 1,
    needsRefresh: false,
    initialize: function (Form, FieldPreview, FieldProperties, FormEngine) {
        this.Form = Form;
        this.FieldProperties = FieldProperties;
        this.FieldPreview = FieldPreview;
        this.FormEngine = FormEngine;
    },
    /*
    * ExtraFields: Position
    */
    updateExtraPositionsByHtml: function (field) {
        field.ExtraFields.each(function (item, index) {
            if (this.isExtraField($('extra_foli'+item.ColumnId))) {
                this.updateExtraPosition(item, field.ColumnId);
                this.updateExtraDisplayPosition(item, field.ColumnId);
            }
        }.bind(this));
    },
    /*
    * ExtraFields: End Position
    */
    updatePositionsByHtml: function (activeField) {
        var fieldList = this.prepFieldList();
        fieldList.each(function (item, index) {
            if (this.isField(item)) {
                var Field = this.Form.selectFieldByColumnId(item.id.replace('foli', ''));
                if (Field) {
                    this.handlePageReorder(Field);
                    this.updatePosition(Field, activeField);
                    this.updateDisplayPosition(Field);
                    this.updateCurrentPage(Field);
                    this.FieldPreview.updateAfterReorder(Field, this.Form);
                }
            }
        }.bind(this));
        var ret = this.needsRefresh;
        this.resetCounters();
        return ret;
    },
    findInsertPositionByHtml: function (insertAfterElement) {
        var ret = 0;
        if (insertAfterElement == 'top') ret = 0;
        else if ($(insertAfterElement)) {
            ret = this.loopThroughFieldsToFindPos(insertAfterElement);
        } else {
            ret = this.Form.FormJson.Fields.length;
            if (this.Form.FormJson.PageCount > 1) ret = ret - 1;
        }
        return ret;
    },
    loopThroughFieldsToFindPos: function (insertAfterElement) {
        var ret = 0;
        var fieldList = this.prepFieldList();
        fieldList.each(function (item, index) {
            if (this.isField(item)) {
                ret++;
                if (item == $(insertAfterElement)) throw $break;
            }
        }.bind(this));
        return ret;
    },
    prepFieldList: function () {
        var allFields = $('formFields');
        allFields.cleanWhitespace();
        return $A(allFields.childNodes);
    },
    isField: function (item) {
        var ret = false;
        if (item.id.startsWith('foli')) ret = true;
        return ret;
    },
    /*
    * ExtraFields: Position
    */
    isExtraField: function (item) {
        var ret = false;
        if (item.id.startsWith('extra_foli')) ret = true;
        return ret;
    },
    updateExtraPosition: function (Field, activeField) {
        Field.Pos = this.extraPosition;
        this.FieldProperties.extraPosition(Field.Pos);
        this.extraPosition++;
    },
    updateExtraDisplayPosition: function (Field, activeField) {
        if (Field.Typeof != 'section' && Field.Typeof != 'page') {
            Field.DisplayPos = this.ExtraDisplayPosition;
            this.ExtraDisplayPosition++;
        }
    },
    /*
    * ExtraFields: End Position
    */
    updatePosition: function (Field, activeField) {
        Field.Pos = this.position;
        if (activeField == Field.ColumnId) this.FieldProperties.position(Field.Pos);
        this.position++;
    },
    updateDisplayPosition: function (Field) {
        if (Field.Typeof != 'section' && Field.Typeof != 'page') {
            Field.DisplayPos = this.displayPosition;
            this.displayPosition++;
        }
    },
    updateCurrentPage: function (Field) {
        Field.Page = this.currentPage;
        if (Field.Typeof == 'page') this.currentPage += 1;
    },
    handlePageReorder: function (Field) {
        if (Field.Typeof == 'page' && Field.Page != this.currentPage) {
            this.FormEngine.removeField(this.Form, Field);
            this.FormEngine.addField(this.Form, Field, this.position);
            this.needsRefresh = true;
        }
    },
    resetCounters: function () {
        this.position = 0;
        this.displayPosition = 0;
        this.currentPage = 1;
        this.needsRefresh = false;
    }
});
var BulkAddObject = Class.create({ //<sl:translate>
    pre_gender: new Array('Male', 'Female', 'Prefer Not to Answer'),
    pre_age: new Array('Under 18', '18-24', '25-34', '35-44', '45-54', '55-64', '65 or Above', 'Prefer Not to Answer'),
    pre_employment: new Array('Employed Full-Time', 'Employed Part-Time', 'Self-employed', 'Not employed, but looking for work', 'Not employed and not looking for work', 'Homemaker', 'Retired', 'Student', 'Prefer Not to Answer'),
    pre_income: new Array('Under $20,000', '$20,000 - $30,000', '$30,000 - $40,000', '$40,000 - $50,000', '$50,000 - $75,000', '$75,000 - $100,000', '$100,000 - $150,000', '$150,000 or more', 'Prefer Not to Answer'),
    pre_education: new Array('Some High School', 'High School Graduate or Equivalent', 'Trade or Vocational Degree', 'Some College', 'Associate Degree', 'Bachelor\'s Degree', 'Graduate of Professional Degree', 'Prefer Not to Answer'),
    pre_marital: new Array('Single, Never Married', 'Married', 'Living with Partner', 'Separated', 'Divorced', 'Widowed', 'Prefer Not to Answer'),
    pre_race: new Array('White / Caucasian', 'Spanish / Hispanic / Latino', 'Black / African American', 'Asian', 'Pacific Islander', 'Native American', 'Other', 'Prefer Not to Answer'),
    pre_timezones: new Array('(-12:00) International Date Line', '(-11:00) Midway Island, Samoa', '(-10:00) Hawaii', '(-9:00) Alaska', '(-8:00) Pacific', '(-7:00) Mountain', '(-6:00) Central', '(-5:00) Eastern', '(-4:30) Venezuela', '(-4:00) Atlantic', '(-3:30) Newfoundland', '(-3:00) Brasilia, Buenos Aires, Georgetown', '(-2:00) Mid-Atlantic', '(-1:00) Azores, Cape Verde Isles', '(0:00) Dublin, London, Monrovia, Casablanca', '(+1:00) Paris, Madrid, Athens, Berlin, Rome', '(+2:00) Eastern Europe, Harare, Pretoria, Israel', '(+3:00) Moscow, Baghdad, Kuwait, Nairobi', '(+3:30) Tehran', '(+4:00) Abu Dhabi, Muscat, Tbilisi, Kazan', '(+4:30) Kabul', '(+5:00) Islamabad, Karachi, Ekaterinburg', '(+5:30) Bombay, Calcutta, Madras, New Delhi', '(+6:00) Almaty, Dhaka', '(+7:00) Bangkok, Jakarta, Hanoi', '(+8:00) Beijing, Hong Kong, Perth, Singapore', '(+9:00) Tokyo, Osaka, Sapporo, Seoul, Yakutsk', '(+9:30) Adelaide, Darwin', '(+10:00) Sydney, Guam, Port Moresby, Vladivostok', '(+11:00) Magadan, Solomon Isles, New Caledonia', '(+12:00) Fiji, Marshall Isles, Wellington, Auckland'),
    pre_months: new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'),
    pre_days: new Array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'),
    pre_states: new Array('Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'),
    pre_continents: new Array('Africa', 'Antarctica', 'Asia', 'Australia', 'Europe', 'North America', 'South America'),
    pre_howoften: new Array('Everyday', 'Once a week', '2 to 3 times a month', 'Once a month', 'Less than once a month'),
    pre_howlong: new Array('Less than a month', '1-6 months', '1-3 years', 'Over 3 Years', 'Never used'),
    pre_satisfied: new Array('Very Satisfied', 'Satisfied', 'Neutral', 'Unsatisfied', 'Very Unsatisfied', 'N/A'),
    pre_important: new Array('Very Important', 'Important', 'Neutral', 'Somewhat Important', 'Not at all Important', 'N/A'),
    pre_agree: new Array('Strongly Agree', 'Agree', 'Neutral', 'Disagree', 'Strongly Disagree', 'N/A'),
    pre_compare: new Array('Much Better', 'Somewhat Better', 'About the Same', 'Somewhat Worse', 'Much Worse', 'Don\'t Know'),
    pre_wouldyou: new Array('Definitely', 'Probably', 'Not Sure', 'Probably Not', 'Definitely Not'),
    pre_agree2: new Array('Strongly Disagree', 'Disagree', 'Neutral', 'Agree', 'Strongly Agree'),
    pre_compare2: new Array('Much Worse', 'Somewhat Worse', 'About the Same', 'Somewhat Better', 'Much Better'),
    pre_important2: new Array('Not at all Important', 'Somewhat Important', 'Neutral', 'Important', 'Very Important'),
    pre_satisfied2: new Array('Very Unsatisfied', 'Unsatisfied', 'Neutral', 'Satisfied', 'Very Satisfied'),
    pre_wouldyou2: new Array('Definitely Not', 'Probably Not', 'Not Sure', 'Probably', 'Definitely'),
    pre_ten: new Array('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'),
    pre_football: new Array('Arizona Cardinals', 'Atlanta Falcons', 'Baltimore Ravens', 'Buffalo Bills', 'Carolina Panthers', 'Chicago Bears', 'Cincinnati Bengals', 'Cleveland Browns', 'Dallas Cowboys', 'Denver Broncos', 'Detroit Lions', 'Green Bay Packers', 'Houston Texans', 'Indianapolis Colts', 'Jacksonville Jaguars', 'Kansas City Chiefs', 'Miami Dolphins', 'Minnesota Vikings', 'New England Patriots', 'New Orleans Saints', 'New York Giants', 'New York Jets', 'Oakland Raiders', 'Philadelphia Eagles', 'Pittsburgh Steelers', 'San Diego Chargers', 'San Francisco 49ers', 'Seattle Seahawks', 'St. Louis Rams', 'Tampa Bay Buccaneers', 'Tennessee Titans', 'Washington Redskins'), //</sl:translate>
    initialize: function () {},
    getSet: function (category) {
        return this[category].join('\n');
    }
});
var __BULKADD;

function initBulkAdd() {
    __BULKADD = new BulkAddObject();
}

function getBulkSet(category) {
    return __BULKADD.getSet(category);
}
var TabInterface = Class.create({
    activeAnimation: false,
    initialize: function () {},
    firstTab: function () {
        $('stage').className = "afi";
    },
    secondTab: function () {
        $('stage').className = "cfi";
    },
    thirdTab: function () {
        $('stage').className = "cfo";
    },
    showSidebar: function (name) {
        this.hideSidebars();
        this.toggleDisplay(name, 'block');
    },
    hideSidebars: function () {
        this.toggleDisplay('addFields', 'none');
        this.toggleDisplay('fieldProperties', 'none');
        this.toggleDisplay('formProperties', 'none');
    },
    toggleDisplay: function (element, visible) {
        $(element).style.display = visible;
    },
    showFields: function (pulse) {
        if (pulse == true && $('addFields').style.display == 'block' && !this.activeAnimation) {
            this.activeAnimation = true;
            if (Prototype.Browser.IE) {
                new Effect.Highlight('addFields', {
                    startcolor: '#FFF7C0',
                    endcolor: '#FDFCE9',
                    afterFinish: function () {
                        this.activeAnimation = false;
                    }.bind(this)
                });
            } else {
                new Effect.Shake('shake', 3, {
                    afterFinish: function () {
                        this.activeAnimation = false;
                    }.bind(this)
                });
                setTimeout(function () {
                    this.activeAnimation = false
                }.bind(this), 500);
            }
        } else {
            this.showSidebar('addFields');
            this.firstTab();
        }
    },
    showFieldProperties: function (columnId) {
        this.showSidebar('fieldProperties');
        this.secondTab();
        if (!columnId || columnId < 0) {
            $('noFieldSelected').style.display = 'block';
            $('fieldPos').style.display = 'none';
            $('allProps').style.display = 'none';
        }
    },
    /*
    * ExtraField: Show
    */
    showExtraFieldProperties: function (columnId) {
        this.showSidebar('fieldProperties');
        this.secondTab();
        if (!columnId || columnId < 0) {
            $('noFieldSelected').style.display = 'block';
            $('fieldPos').style.display = 'none';
            $('allProps').style.display = 'none';
        }
    }
    /*
    * ExtraField: End Show
    */
});
var FormPropertiesObject = Class.create({ //<sl:translate>
    noEmailsOptionText: 'No Email Fields Found', //</sl:translate>
    initialize: function () {
        this.initCalendars();
    },
    populate: function (Form, Profile) {
        $('formProperties').removeClassName('paging');
        this.Profile = Profile;
        this.Form = Form;
        this.name();
        this.description();
        this.formtype();
        this.labelAlign();
        this.captcha();
        this.receipt();
        this.extra_options();
        this.ip();
        this.redirect();
        this.limit();
        this.usersubmitlimit();
        this.setDates();
        this.language();
    },
    populateSingle: function (Form, Profile, attribute) {
        this.Profile = Profile;
        this.Form = Form;
        this[attribute]();
    },
    usersubmitlimit: function () {
        $('formUserSubmitLimit').value = this.Form.FormJson.UserSubmitLimit || 1;
    },
    name: function () {
        $('formName').value = this.Form.FormJson.Name;
    },
    description: function () {
        var desc = this.Form.FormJson.Description;
        if (desc != '' && desc != null) desc = desc.replace(/\<br \/\>/g, "\n");
        $('formDescription').value = desc;
    },
    formtype: function() {
        var formtype = this.Form.FormJson.Formtype;
        $$('.formtype').each(function(key, el) {
            if(key.value == formtype) key.checked = true;
        });
    },
    labelAlign: function () {
        lab = $('formLabelAlign');
        lab.selectedIndex = 1;
        for (var i = 0; i < lab.options.length; i++) {
            if (this.Form.FormJson.LabelAlign == lab.options[i].value) {
                lab.selectedIndex = i;
            }
        }
    },
    captcha: function () {
        cap = $('formCaptcha');
        cap.selectedIndex = 1;
        for (var i = 0; i < cap.options.length; i++) {
            if (this.Form.FormJson.UseCaptcha == cap.options[i].value) {
                cap.selectedIndex = i;
            }
        }
    },
    limit: function () {
        var limit = this.Form.FormJson.EntryLimit;
        if (limit == 0) limit = '';
        $('formEntryLimit').value = limit;
    },
    email: function () {
        fieldVal = ACTIVE_FORM.data['email'];
        $('formEmailVal').value = fieldVal;
        this.setReplyTo();
        $('formEmail').onclick = function () {
            if (Element.hasClassName('formEmailDiv', 'hide')) {
                Element.removeClassName('formEmailDiv', 'hide');
                updateForm(tempEmail, 'email');
                $('formEmailVal').value = tempEmail;
                Field.activate($('formEmailVal'));
            } else {
                Element.addClassName('formEmailDiv', 'hide');
                tempEmail = $('formEmailVal').value;
                updateForm('', 'email');
            }
        }
        if (fieldVal == '') {
            $('formEmail').checked = false;
            Element.addClassName('formEmailDiv', 'hide');
        } else {
            $('formEmail').checked = true;
            Element.removeClassName('formEmailDiv', 'hide');
        }
    },
    receipt: function () {
        var sendToDropDown = $('formSendTo');
        sendToDropDown.options.length = 0;
        sendToDropDown.options[0] = new Option(' ', ' ');
        $('formReceiptDiv').addClassName('hide');
        $('formReceipt').checked = false;
        this.Form.FormJson.Fields.each(function (Field, index) {
            if (Field.Typeof == 'email') {
                var option = new Option(Field.Title, Field.ColumnId);
                option.className = 'notranslate';
                sendToDropDown.options[sendToDropDown.options.length] = option;
                if (Field.Validation == 'email') {
                    sendToDropDown.selectedIndex = sendToDropDown.options.length - 1;
                    $('formReceipt').checked = true;
                    var replyto = '';
                    if (this.Form.FormJson.ReceiptReplyTo != null) replyto = this.Form.FormJson.ReceiptReplyTo;
                    $('receiptReplyTo').value = replyto;
                    $('formReceiptDiv').removeClassName('hide');
                }
            }
        }.bind(this));
        if (sendToDropDown.options.length == 1) {
            sendToDropDown.options[0] = new Option(this.noEmailsOptionText, '');
            sendToDropDown.disabled = true;
        } else sendToDropDown.disabled = false;
    },
    displayReceiptLightbox: function () {
        Lightbox.showUrl('lightboxes/FormBuilder.Receipt.php', finishLoadingReceiptLightbox);
    },
    populateReceiptLightbox: function (Form) {
        var from = '';
        if (Form.FormJson.FromAddress != null) from = Form.FormJson.FromAddress;
        var message = '';
        if (Form.FormJson.ReceiptMessage != null) message = Form.FormJson.ReceiptMessage;
        $('fromAddress').value = from;
        $('receiptMessage').value = message;
        if (Form.FormJson.ReceiptCopy == '1') $('receiptCopy').checked = true;
        else $('receiptCopy').checked = false;
    },
    language: function () {
        lang = $('formLanguage');
        lang.selectedIndex = 3;
        for (var i = 0; i < lang.options.length; i++) {
            if (this.Form.FormJson.Language == lang.options[i].value) {
                lang.selectedIndex = i;
            }
        }
    },
    password: function () {
        var fieldVal = '';
        if (this.Form.FormJson.Password != null) fieldVal = this.Form.FormJson.Password;
        $('formPasswordVal').value = fieldVal;
        if (!fieldVal || fieldVal == '' || this.Profile.PlanId == '1') {
            $('formPassword').checked = false;
            Element.addClassName('formPasswordDiv', 'hide');
        } else {
            $('formPassword').checked = true;
            Element.removeClassName('formPasswordDiv', 'hide');
        }
    },
    ip: function () {
        if (this.Form.FormJson.UniqueIP == 1) $('formIp').checked = true;
        else $('formIp').checked = false;
    },
    extra_options: function () {
        // user login
        if (this.Form.FormJson.UserLogin == 1) $('formUserLogin').checked = true;
        else $('formUserLogin').checked = false;
        // ajax submit
        if (this.Form.FormJson.AjaxEnable == 1) $('formAjaxEnable').checked = true;
        else $('formAjaxEnable').checked = false;
        // ajax show what just typed
        if (this.Form.FormJson.AjaxShowWhatJustType == 1) $('formAjaxShowWhatJustType').checked = true;
        else $('formAjaxShowWhatJustType').checked = false;
        // live response
        if (this.Form.FormJson.AjaxLiveResponse == 1) $('formAjaxLiveResponse').checked = true;
        else $('formAjaxLiveResponse').checked = false;
    },
    redirect: function () {
        if (this.Form.FormJson.Redirect != '' && this.Form.FormJson.Redirect != null) {
            Element.addClassName($('formMessageVal'), 'hide');
            Element.removeClassName($('formRedirectVal'), 'hide');
            $('formRedirect').checked = true;
            $('formRedirectVal').value = this.Form.FormJson.Redirect;
        } else {
            Element.addClassName($('formRedirectVal'), 'hide');
            Element.removeClassName($('formMessageVal'), 'hide');
            $('formMessage').checked = true;
            var redirectMsg = this.Form.FormJson.RedirectMessage;
            if (redirectMsg != '' && redirectMsg != null) redirectMsg = redirectMsg.replace(/\<br \/\>/g, "\n");
            $('formMessageVal').value = redirectMsg;
        }
    },
    setDates: function () {
        var starting = this.Form.FormJson.StartDate;
        var ending = this.Form.FormJson.EndDate;
        if (starting != '2000-01-01 12:00:00' || ending != '2030-01-01 12:00:00') {
            $('formSchedule').checked = true;
            $('listStartDate').removeClassName('hide');
            $('listEndDate').removeClassName('hide');
            temp = starting.split(' ');
            sDate = temp[0];
            sTime = temp[1];
            this.setStartDate(sDate);
            this.setStartTime(sTime);
            temp = ending.split(' ');
            sDate = temp[0];
            sTime = temp[1];
            this.setEndDate(sDate);
            this.setEndTime(sTime);
        }
    },
    setStartDate: function (sDate) {
        sDateArray = sDate.split('-');
        $('startDate-1').value = sDateArray[1];
        $('startDate-2').value = sDateArray[2];
        $('startDate').value = sDateArray[0];
        if (sDate == '2000-01-01') {
            $('startDate-1').value = '';
            $('startDate-2').value = '';
            $('startDate').value = '';
        }
    },
    setStartTime: function (sTime) {
        sTimeArray = sTime.split(':');
        hours = sTimeArray[0];
        minutes = sTimeArray[1];
        ampm = 'AM';
        if (hours >= 12) {
            ampm = 'PM';
            hours = hours - 12;
        }
        this.dropdownSelection('startTime-0', hours);
        this.dropdownSelection('startTime-1', minutes);
        this.dropdownSelection('startTime-2', ampm);
    },
    setEndDate: function (sDate) {
        sDateArray = sDate.split('-');
        $('endDate-1').value = sDateArray[1];
        $('endDate-2').value = sDateArray[2];
        $('endDate').value = sDateArray[0];
        if (sDate == '2030-01-01') {
            $('endDate-1').value = '';
            $('endDate-2').value = '';
            $('endDate').value = '';
        }
    },
    setEndTime: function (sTime) {
        sTimeArray = sTime.split(':');
        hours = sTimeArray[0];
        minutes = sTimeArray[1];
        ampm = 'AM';
        if (hours >= 12) {
            ampm = 'PM';
            hours = hours - 12;
        }
        this.dropdownSelection('endTime-0', hours);
        this.dropdownSelection('endTime-1', minutes);
        this.dropdownSelection('endTime-2', ampm);
    },
    dropdownSelection: function (dropdown, value) {
        var drop = $(dropdown);
        for (var i = 0; i < drop.length; i++) {
            if (drop.options[i].value == value || '0' + drop.options[i].value == value) {
                drop.selectedIndex = i;
            }
        }
    },
    initCalendars: function () {
        Calendar.setup({
            inputField: "startDate",
            displayArea: "calStart",
            button: "pickStart",
            ifFormat: "%B %e, %Y",
            onSelect: selectFormActivity
        });
        Calendar.setup({
            inputField: "endDate",
            displayArea: "calEnd",
            button: "pickEnd",
            ifFormat: "%B %e, %Y",
            onSelect: selectFormActivity
        });
    }
});
var FieldPropertiesObject = Class.create({
    show: '',
    Form: '', //<sl:translate>
    fieldTypeCanNoLongerBeSavedWarning: 'Since this field has already been saved to your account, the field type can no longer be changed.',
    fieldOnceEncryptedWarning: 'Once a field has been encrypted it cannot be changed.',
    radioMarkupText: '<li><input name="rightAnswered" class="" type="checkbox" onclick="changeRightDefault(this)" #{selectedRightAnswered} title="Make this choice right answered." /><input name="allChoices" class="" type="radio" onclick="changeRadioDefault(this)" #{selectedAttribute} title="Make this choice pre-selected." /> <input class="text" type="text" maxlength="150" autocomplete="off" value="#{choice}" onkeyup="updateChoice(this);" /> <img src="template/images/icons/add.png" onclick="addRadio(this)" class="notranslate" alt="Add" title="Add another choice."/> <img src="template/images/icons/delete.png" class="notranslate" onclick="deleteRadio(this)" alt="Delete" title="Delete this choice."/></li>',
    highlightRadioMarkupText: '<a href="#" onclick="toolTipToggle(this, event); return false;" class="tooltip" title="About Other Field" rel="This is the label for the alternative text input for your multiple choice field. It allows your user to enter their own value if none of the other choices are appropriate. Note : If this choice is selected, but no value is entered by the user, this label will be submitted instead.">(?)</a>',
    linkTipMarkupText: '<a href="#" onclick="toolTipToggle(this, event); return false;" class="tooltip" title="About Required Drop Downs" rel="This is the default drop down choice. When you make a drop down field required, you are essentially telling the user they have to make a choice. If they leave the drop down on the default choice (the one that is selected when the page loads),Hoctudau will consider that as not making a choice and will throw an error if not changed.">(?)</a>',
    selectMarkupText: '<li #{defaultClassKey}><input name="rightAnswered" class="" type="checkbox" onclick="changeRightDefault(this)" #{selectedRightAnswered} title="Make this choice right answered." />#{linkTipKey}<input name="allChoices" class="" type="radio" onclick="changeSelectDefault(this)" #{selectedKey} title="Make this choice pre-selected." /> <input class="text" type="text" maxlength="150" autocomplete="off" value="#{choiceKey}" onkeyup="updateChoice(this);" /> <img src="template/images/icons/add.png" class="notranslate" onclick="addSelect(this)" alt="Add" title="Add another choice."/> <img src="template/images/icons/delete.png" class="notranslate" onclick="deleteSelect(this)" alt="Delete" title="Delete this choice."/></li>',
    choiceToHighlightMarkupText: '<a href="#" onclick="toolTipToggle(this, event); return false;" class="tooltip" title="About Required Dropdowns" rel="This is the default down choice. When you make a drop down field required, you are essentially telling the user they have to make a choice. So, if they leave the drop down on the default choice (the one that is selected when the page loads),Hoctudau will consider that as not making a choice and throw an error.">(?)</a>',
    checkboxMarkupText: '<li><input name="rightAnswered" class="" type="checkbox" onclick="changeRightDefault(this)" #{selectedRightAnswered} title="Make this choice right answered." /><input name="allChoices" class="" type="checkbox" onclick="changeCheckboxDefault(this)" #{selectedKey} title="Make this choice pre-selected." /> <input class="text" type="text" maxlength="150" autocomplete="off" value="#{choiceKey}" onkeyup="updateChoice(this);" /> <img src="template/images/icons/add.png" class="notranslate" onclick="addCheckbox(this)" alt="Add" title="Add another choice."/>#{deleteKey}</li>',
    checkboxMarkupDeleteText: '<img src="template/images/icons/delete.png" class="notranslate" onclick="deleteCheckbox(this)" alt="Delete" title="Delete this choice."/>',
    columnMarkupText: '<li><input name="rightAnswered" class="" type="checkbox" onclick="changeRightDefault(this)" #{selectedRightAnswered} title="Make this choice right answered." /><input name="allChoices" type="radio" #{selectedKey} onclick="changeLikertDefault(this);" title="Make this column pre-selected." /> <input class="text" type="text" value="#{choiceKey}" onkeyup="updateChoice(this);" /> <img src="template/images/icons/add.png" class="notranslate" alt="Add" onclick="addLikertChoice(this);" title="Add another choice."/>#{deleteKey}</li>',
    columnMarkupDeleteText: '<img src="template/images/icons/delete.png" class="notranslate" alt="Delete" onclick="deleteLikertChoice(this);" title="Delete this column."/> ',
    highlightLikertColumnMarkupText: '<a href="#" onclick="toolTipToggle(this, event); return false;" class="tooltip" title="About Likert Non Applicable" rel="This choice represents a null selection, and will have a score of 0 assigned to it. It is ideal for situations where you wish to make a likert field required, but you still want to allow users to opt out of certain questions.">(?)</a>',
    statementMarkupText: '<li><input class="text statement" type="text" value="#{choiceKey}" onkeyup="updateChoice(this);" /> <img src="template/images/icons/add.png" class="notranslate" alt="Add" onclick="addLikertStatement(this);" title="Add another choice."/>#{deleteKey}</li>',
    statementMarkupDeleteText: '<img src="template/images/icons/delete.png" class="notranslate" alt="Delete" onclick="deleteLikertStatement(this)" title="Delete this choice."/> ',
    charactersOptionText: 'Characters',
    wordsOptionText: 'Words',
    valueOptionText: 'Value',
    digitsOptionText: 'Digits', //</sl:translate>
    initialize: function (Form) {
        this.Form = Form;
        this.show = new Array();
        this.show['text'] = new Array('types', 'size', 'displayInputAnswer', 'displayAnswer', 'required', 'excerpt', 'unique', 'isPrivate', 'textDefault', 'encryption', 'range', 'instructions');
        this.show['textarea'] = new Array('types', 'size', 'required', 'excerpt', 'unique', 'isPrivate', 'textDefault', 'range', 'instructions');
        this.show['select'] = new Array('types', 'size', 'choices', 'displayAnswer', 'required', 'excerpt', 'isPrivate', 'populate', 'instructions');
        this.show['radio'] = new Array('types', 'layout', 'choices', 'displayInputAnswer', 'displayAnswer', 'required', 'excerpt', 'isPrivate', 'randomize', 'other', 'populate', 'instructions');
        this.show['checkbox'] = new Array('types', 'layout', 'choices', 'displayAnswer', 'required', 'isPrivate', 'instructions');
        this.show['likert'] = new Array('types', 'required', 'isPrivate', 'likert', 'numbers', 'na');
        this.show['name'] = new Array('types', 'required', 'isPrivate', 'name', 'instructions');
        this.show['shortname'] = new Array('types', 'required', 'isPrivate', 'name', 'instructions');
        this.show['date'] = new Array('types', 'required', 'unique', 'isPrivate', 'date', 'dateDefault', 'instructions');
        this.show['eurodate'] = new Array('types', 'required', 'unique', 'isPrivate', 'date', 'euroDateDefault', 'instructions');
        this.show['time'] = new Array('types', 'required', 'unique', 'isPrivate', 'instructions');
        this.show['phone'] = new Array('types', 'required', 'unique', 'isPrivate', 'phone', 'phoneDefault', 'instructions');
        this.show['europhone'] = new Array('types', 'required', 'unique', 'isPrivate', 'phone', 'textDefault', 'instructions');
        this.show['address'] = new Array('types', 'required', 'isPrivate', 'addressDefault', 'instructions');
        this.show['money'] = new Array('types', 'required', 'unique', 'isPrivate', 'moneyDefault', 'currency', 'range', 'instructions');
        this.show['linked'] = new Array('types', 'required', 'unique', 'isPrivate', 'instructions');
        this.show['url'] = new Array('types', 'size', 'required', 'excerpt', 'unique', 'isPrivate', 'textDefault', 'instructions');
        this.show['email'] = new Array('types', 'size', 'required', 'excerpt', 'unique', 'isPrivate', 'textDefault', 'instructions');
        this.show['number'] = new Array('types', 'size', 'required', 'excerpt', 'unique', 'isPrivate', 'textDefault', 'range', 'instructions');
        this.show['rating'] = new Array('types', 'option', 'instructions');
        this.show['tags'] = new Array('types', 'option', 'instructions');
        this.show['file'] = new Array('types', 'required', 'isPrivate', 'instructions');
        this.show['password'] = new Array('types', 'option', 'instructions');
        this.show['math'] = new Array('types', 'option', 'instructions');
        this.show['section'] = new Array('isPrivate', 'instructions');
        this.show['page'] = new Array('buttons');
    },
    display: function (Field) {
        this.Field = Field;
        this.hideAll();
        this.title();
        for (var x = 0; x < this.show[this.Field.Typeof].length; x++) {
            this[this.show[this.Field.Typeof][x]]();
        }
        this.position(Field.DisplayPos);
        this.ClassNames();
        this.listButtons();
        $('fieldPos').style.display = 'block';
        $('allProps').style.display = 'block';
    },
    displayExtra: function (Field) {
        this.Field = Field;
        this.hideAll();
        this.title();
        $('listFieldLink').style.display = 'none';
        for (var x = 0; x < this.show[this.Field.Typeof].length; x++) {
            if(typeof this['extra_' + this.show[this.Field.Typeof][x]] == 'function') {
                this['extra_' + this.show[this.Field.Typeof][x]]();
            } else {
                this[this.show[this.Field.Typeof][x]]();
            }
        }
        this.position(Field.Pos);
        this.ClassNames();
        this.listButtons();
        $('fieldPos').style.display = 'block';
        $('allProps').style.display = 'block';
    },
    hideAll: function () {
        $('listFieldLink').style.display = 'none';
        $('listSecurity').style.display = 'none';
        $('listType').style.display = 'none';
        $('listInstructions').style.display = 'none';
        $('listPopulate').style.display = 'none';
        $('listTextDefault').style.display = 'none';
        $('listPhoneDefault').style.display = 'none';
        $('listDateDefault').style.display = 'none';
        $('listPriceDefault').style.display = 'none';
        $('listEuroDateDefault').style.display = 'none';
        $('listAddressDefault').style.display = 'none';
        $('listDateFormat').style.display = 'none';
        $('listPhoneFormat').style.display = 'none';
        $('listMoneyFormat').style.display = 'none';
        $('listSize').style.display = 'none';
        $('listNameFormat').style.display = 'none';
        $('listChoices').style.display = 'none';
        $('listOptions').style.display = 'none';
        $('listInputAnswer').style.display = 'none';
        $('listDisplayAnswer').style.display = 'none';
        $('listRange').style.display = 'none';
        $('listNextButtons').style.display = 'none';
        $('listPreviousButtons').style.display = 'none';
        $('listClassNames').style.display = 'none';
        $('listLikert').style.display = 'none';
        $('listLayout').style.display = 'none';
        $('noFieldSelected').style.display = 'none';
        $('listButtons').style.display = 'none';
        $('fieldPos').style.display = 'none';
        $('allProps').style.display = 'none';
    },
    onFieldUpdate: function (Field, parameter) {
        switch (parameter) {
            case 'IsRequired':
                if (Field.Typeof == 'select') this.highlightDefaultIfRequired(Field);
                break;
            case 'IsDefault':
                if (Field.Typeof == 'select') this.highlightDefaultIfRequired(Field);
                break;
        }
    },
    listButtons: function () {
        if ($('fa' + this.Field.ColumnId)) {
            $('listButtons').style.display = 'block';
            $('listButtonsDuplicate').onclick = function (event) {
                duplicateField(this.Field.ColumnId, event);
                return false;
            }.bind(this);
            $('listButtonsDelete').onclick = function (event) {
                removeField(this.Field.ColumnId, event);
                return false;
            }.bind(this);
        }
    },
    position: function (val) {
        if (this.Field.Typeof == 'page' || this.Field.Typeof == 'section') $('fieldPos').addClassName('hide');
        else {
            $('fieldPos').removeClassName('hide');
            $('fieldPos').innerHTML = (parseInt(val) + 1) + '.';
        }
    },
    /*
    * ExtraFields: Position
    */
    extraPosition: function (val) {
        if (this.Field.Typeof == 'page' || this.Field.Typeof == 'section') $('fieldPos').addClassName('hide');
        else {
            $('fieldPos').removeClassName('hide');
            $('fieldPos').innerHTML = (parseInt(val) + 1) + '.';
        }
    },
    /*
    * ExtraFields: End Position
    */
    title: function () {
        if( this.Field ) if (this.Field.Typeof == 'page') $('listTitle').style.display = 'none';
        else $('listTitle').style.display = 'block';
        $('fieldTitle').value = this.Field.Title;
        var url = new String(document.location);
        $('listFieldLink').style.display = 'block';
        if(this.Field.FieldLink) $('fieldLink').value = url.replace('req=build','req=forms') + '&f=' + this.Field.FieldLink + '/';
        
    },
    types: function () {
        $('listType').style.display = 'block';
        var func = this.typeNotify;
        var type = this.Field.Typeof;
        if (this.Field.HasntBeenSaved) {
            $('fieldType').disabled = false;
            Event.stopObserving('fieldType', 'click', func);
        } else {
            $('fieldType').disabled = true;
            Event.observe('fieldType', 'click', func, false);
        }
        if (type == 'name') type = 'shortname';
        if (type == 'europhone') type = 'phone';
        if (type == 'eurodate') type = 'date';
        for (var i = 0; i < $('fieldType').options.length; i++) {
            if ($('fieldType').options[i].value == type) {
                $('fieldType').options[i].selected = true;
                break;
            }
        }
    },
    typeNotify: function () {
        alert(this.fieldTypeCanNoLongerBeSavedWarning);
    },
    size: function () {
        $('listSize').style.display = 'block';
        for (var i = 0; i < $('fieldSize').options.length; i++) {
            if ($('fieldSize').options[i].value == this.Field.Size) {
                $('fieldSize').options[i].selected = true;
                break;
            }
        }
    },
    layout: function () {
        $('listLayout').style.display = 'block';
        var setting = '';
        if (this.Field.Settings.indexOf('notStacked') != -1) setting = 'notStacked';
        if (this.Field.Settings.indexOf('twoColumns') != -1) setting = 'twoColumns';
        if (this.Field.Settings.indexOf('threeColumns') != -1) setting = 'threeColumns';
        for (var i = 0; i < $('fieldLayout').options.length; i++) {
            if ($('fieldLayout').options[i].value == setting) {
                $('fieldLayout').options[i].selected = true;
                break;
            }
        }
    },
    required: function () {
        $('listOptions').style.display = 'block';
        (this.Field.IsRequired == '1') ? $('fieldRequired').checked = true : $('fieldRequired').checked = false;
        var ftype = this.Field.Typeof;
        (ftype == 'checkbox' || ftype == 'radio' || ftype == 'select' || ftype == 'name' || ftype == 'shortname' || ftype == 'address' || ftype == 'likert') ? this.toggleUnique('hide') : this.toggleUnique('show');
        (ftype == 'text' && CLIENT_JSON.Profile.PlanId >= 3) ? this.toggleEncrypted('show') : this.toggleEncrypted('hide');
        (ftype == 'text' || ftype == 'radio' || ftype == 'select' || ftype == 'number' || ftype == 'url') ? this.toggleExerpt('show') : this.toggleExerpt('hide');
        (ftype == 'radio') ? this.toggleRandomize('show') : this.toggleRandomize('hide');
        (ftype == 'radio') ? this.toggleOther('show') : this.toggleOther('hide');
        
        
        (ftype == 'likert') ? this.toggleNumbers('show') : this.toggleNumbers('hide');
        (ftype == 'likert') ? this.toggleNA('show') : this.toggleNA('hide');
        (ftype == 'likert') ? this.toggleDC('show') : this.toggleDC('hide');
    },
    toggleExerpt: function (view) {
        excerpts = document.getElementsByClassName('fieldExcerptToggle');
        for (var i = 0; i < excerpts.length; i++) {
            (view == 'hide') ? excerpts[i].addClassName('hide') : excerpts[i].removeClassName('hide');
        }
    },
    displayInputAnswer: function () {
        $('listInputAnswer').style.display = 'block';
        $('fieldInputRightAnswer').value = this.Field.InputRightAnswer;
        $('fieldInputRightAnswerRegex').checked = this.Field.InputRightAnswerRegex || false;
    },
    displayAnswer: function () {
        $('listDisplayAnswer').style.display = 'block';
        $('fieldDisplayRightAnswer').value = this.Field.RightAnswerDescription;
        $('fieldDisplayWrongAnswer').value = this.Field.WrongAnswerDescription;
    },
    toggleRandomize: function (view) {
        var randoms = document.getElementsByClassName('fieldRandomToggle');
        for (var i = 0; i < randoms.length; i++) {
            (view == 'hide') ? randoms[i].addClassName('hide') : randoms[i].removeClassName('hide');
        }
    },
    toggleOther: function (view) {
        if ($('fieldOther')) {
            var others = document.getElementsByClassName('fieldOtherToggle');
            for (var i = 0; i < others.length; i++) {
                (view == 'hide') ? others[i].addClassName('hide') : others[i].removeClassName('hide');
            }
        }
    },
    toggleNumbers: function (view) {
        var numbs = document.getElementsByClassName('fieldNumbersToggle');
        for (var i = 0; i < numbs.length; i++) {
            (view == 'hide') ? numbs[i].addClassName('hide') : numbs[i].removeClassName('hide');
        }
    },
    toggleNA: function (view) {
        var nonapp = document.getElementsByClassName('fieldNAToggle');
        for (var i = 0; i < nonapp.length; i++) {
            (view == 'hide') ? nonapp[i].addClassName('hide') : nonapp[i].removeClassName('hide');
        }
    },
    toggleDC: function (view) {
        var nonapp = document.getElementsByClassName('fieldDCToggle');
        for (var i = 0; i < nonapp.length; i++) {
            (view == 'hide') ? nonapp[i].addClassName('hide') : nonapp[i].removeClassName('hide');
        }
    },
    randomize: function () {
        (this.Field.Validation == 'rand') ? $('fieldRandom').checked = true : $('fieldRandom').checked = false;
    },
    numbers: function () {
        (this.Field.Settings.indexOf('hideNumbers') != -1) ? $('fieldNumbers').checked = true : $('fieldNumbers').checked = false;
    },
    buttons: function () {
        if (this.Form.instancesOfField('page') != this.Field.Page) {
            $('listNextButtons').style.display = 'block';
            this.displayNextPage();
        } else $('listNextButtons').style.display = 'none';
        if (this.Field.Page != 1) {
            $('listPreviousButtons').style.display = 'block';
            this.displayPreviousPage();
        } else $('listPreviousButtons').style.display = 'none';
    },
    displayNextPage: function () {
        var btn = this.Field.ChoicesText;
        if (btn.include('http') && btn.include('.')) {
            $('listNextButtons').removeClassName('asTxt');
            $('listNextButtons').addClassName('asImg');
            $('nextButtonLink').checked = true;
            $('nextButtonImg').value = btn;
            $('nextButtonsText').value = 'Next Page';
        } else {
            $('listNextButtons').removeClassName('asImg');
            $('listNextButtons').addClassName('asTxt');
            $('nextButtonString').checked = true;
            $('nextButtonsText').value = btn;
            $('nextButtonImg').value = 'http://';
        }
    },
    displayPreviousPage: function () {
        $('previousButtonsText').value = this.Field.DefaultVal;
    },
    other: function () {
        if ($('fieldOther')) {
            (this.Field.Settings.indexOf('other') != -1) ? $('fieldOther').checked = true : $('fieldOther').checked = false;
        }
    },
    na: function () {
        (this.Field.Validation == 'na') ? $('fieldNA').checked = true : $('fieldNA').checked = false;
        (this.Field.Validation == 'dc') ? $('fieldDC').checked = true : $('fieldDC').checked = false;
    },
    toggleUnique: function (view) {
        uniques = document.getElementsByClassName('fieldUniqueToggle');
        for (var i = 0; i < uniques.length; i++) {
            (view == 'hide') ? uniques[i].addClassName('hide') : uniques[i].removeClassName('hide');
        }
    },
    unique: function () {
        (this.Field.IsUnique == '1') ? $('fieldUnique').checked = true : $('fieldUnique').checked = false;
        
    },
    excerpt: function () {
        (this.Field.Excerpt == '1') ? $('fieldExcerpt').checked = true : $('fieldExcerpt').checked = false;
    },
    isPrivate: function () {
        $('listSecurity').style.display = 'block';
        (this.Field.IsPrivate == '1') ? $('fieldPrivate').checked = true : $('fieldPublic').checked = true;
    },
    encryption: function () {
        (this.Field.IsEncrypted == '1') ? $('fieldEncrypted').checked = true : $('fieldEncrypted').checked = false;
        var func = this.encryptNotify;
        if (this.Field.HasntBeenSaved) {
            $('fieldEncrypted').disabled = false;
            Event.stopObserving('fieldEncrypted', 'click', func);
        } else {
            $('fieldEncrypted').disabled = true;
            Event.observe('fieldEncrypted', 'click', func, false);
        }
    },
    encryptNotify: function () {
        alert(this.fieldOnceEncryptedWarning);
    },
    toggleEncrypted: function (view) {
        encrypts = document.getElementsByClassName('fieldEncryptedToggle');
        for (var i = 0; i < encrypts.length; i++) {
            (view == 'hide') ? encrypts[i].addClassName('hide') : encrypts[i].removeClassName('hide');
        }
    },
    range: function () {
        $('listRange').style.display = 'block';
        this.determineRangeDropDownTypes();
        var minRange = '0';
        var maxRange = '';
        if (parseInt(this.Field.RangeMin) > 0) minRange = this.Field.RangeMin;
        if (parseInt(this.Field.RangeMax) > 0 && parseInt(this.Field.RangeMax) >= minRange) maxRange = this.Field.RangeMax;
        $('fieldRangeMin').value = minRange;
        $('fieldRangeMax').value = maxRange;
        for (var i = 0; i < $('fieldRangeType').options.length; i++) {
            if ($('fieldRangeType').options[i].value == this.Field.RangeType) {
                $('fieldRangeType').options[i].selected = true;
                break;
            }
        }
    },
    determineRangeDropDownTypes: function () {
        var el = $('fieldRangeType');
        el.options.length = 0;
        if (this.Field.Typeof == 'text' || this.Field.Typeof == 'textarea') {
            el.options[0] = new Option(this.charactersOptionText, 'character');
            el.options[1] = new Option(this.wordsOptionText, 'word');
        }
        if (this.Field.Typeof == 'number') {
            el.options[0] = new Option(this.valueOptionText, 'value');
            el.options[1] = new Option(this.digitsOptionText, 'digit');
        }
        if (this.Field.Typeof == 'money') {
            el.options[0] = new Option(this.valueOptionText, 'value');
        }
    },
    populate: function () {
        $('listPopulate').style.display = 'block';
    },
    textDefault: function () {
        $('listTextDefault').style.display = 'block';
        $('fieldDefault').value = this.Field.DefaultVal;
    },
    phoneDefault: function () {
        $('listPhoneDefault').style.display = 'block';
        fieldVal = this.Field.DefaultVal;
        $('fieldPhoneDefault1').value = fieldVal.substring(0, 3);
        $('fieldPhoneDefault2').value = fieldVal.substring(3, 6);
        $('fieldPhoneDefault3').value = fieldVal.substring(6, 10);
    },
    moneyDefault: function () {
        $('listPriceDefault').style.display = 'block';
        $('fieldPriceDefault').value = this.Field.DefaultVal;
    },
    addressDefault: function () {
        $('listAddressDefault').style.display = 'block';
        fieldVal = this.Field.SubFields[5].DefaultVal;
        countries = $('fieldCountries');
        for (var i = 0; i < countries.options.length; i++) {
            if (countries.options[i].value == fieldVal) countries.selectedIndex = i;
        }
    },
    dateDefault: function () {
        $('listDateDefault').style.display = 'block';
        $('fieldDateDefault').value = this.Field.DefaultVal;
    },
    euroDateDefault: function () {
        $('listEuroDateDefault').style.display = 'block';
        $('fieldEuroDateDefault').value = this.Field.DefaultVal;
    },
    date: function () {
        $('listDateFormat').style.display = 'block';
        (this.Field.Typeof == 'date') ? $('fieldDateAmerican').selected = true : $('fieldDateEuro').selected = true;
    },
    name: function () {
        $('listNameFormat').style.display = 'block';
        if (this.Field.HasntBeenSaved) $('nameFormat').disabled = false;
        else $('nameFormat').disabled = true;
        (this.Field.Typeof == 'shortname') ? $('fieldNameNormal').selected = true : $('fieldNameExtended').selected = true;
    },
    phone: function () {
        $('listPhoneFormat').style.display = 'block';
        (this.Field.Typeof == 'phone') ? $('fieldPhoneAmerican').selected = true : $('fieldPhoneEuro').selected = true;
    },
    currency: function () {
        var validation = this.Field.Validation;
        $('listMoneyFormat').style.display = 'block';
        if (validation == 'euro') $('fieldMoneyEuro').selected = true;
        else if (validation == 'pound') $('fieldMoneyPound').selected = true;
        else if (validation == 'yen') $('fieldMoneyYen').selected = true;
        else if (validation == 'pesos') $('fieldMoneyPesos').selected = true;
        else $('fieldMoneyAmerican').selected = true;
    },
    instructions: function () {
        $('listInstructions').style.display = 'block';
        $('fieldInstructions').value = this.Field.Instructions;
    },
    ClassNames: function () {
        $('listClassNames').style.display = 'block';
        $('fieldClassNames').value = this.Field.ClassNames;
    },
    choices: function () {
        $('listChoices').style.display = 'block';
        $('fieldChoices').innerHTML = '';
        var choicesMarkup = '';
        if (this.Field.Typeof == 'checkbox') {
            this.Field.SubFields.each(function (num, index) {
                choicesMarkup += this[this.Field.Typeof + 'Markup'](num, index);
            }.bind(this));
        } else {
            this.Field.Choices.each(function (num, index) {
                choicesMarkup += this[this.Field.Typeof + 'Markup'](num, index);
            }.bind(this));
        }
        $('fieldChoices').innerHTML = choicesMarkup;
        if (this.Field.Typeof == 'radio' && this.Field.Settings.indexOf('other') != -1) {
            var list = $('fieldChoices');
            var listEl = $('fieldChoices').down('li', $('fieldChoices').childElements().length - 1);
            this.highlightRadioOther(listEl);
            list.down('li', list.childElements().length - 1).down('img').remove();
        }
    },
    radioMarkup: function (choice, index) {
        var selected, selectedR = '';
        if (choice.IsDefault == '1') selected = 'checked="checked"';
        if (choice.IsRight == '1') selectedR = 'checked="checked"';
        var choiceValue = choice.Choice.gsub("\"", "&quot;");
        var tpl = new Template(this.radioMarkupText);
        return tpl.evaluate({
            selectedAttribute: selected,
            choice: choiceValue,
            selectedRightAnswered: selectedR
        });
    },
    highlightRadioOther: function (choiceToHighlight) {
        if (!choiceToHighlight.hasClassName('dropReq')) {
            choiceToHighlight.addClassName('dropReq');
            choiceToHighlight.insert({
                top: this.highlightRadioMarkupText
            });
        }
    },
    insertChoice: function (ctrl, value, type) {
        var choice = {
            "Choice": value,
            "IsDefault": "0",
            "IsRight": "1"
        }
        if (type == 'checkbox' || type == 'statement') {
            choice = {
                "ChoicesText": value,
                "DefaultVal": "0",
                "IsRight": "1"
            }
        }
        var markup = this[type + 'Markup'](choice, 1);
        ctrl.insert({
            after: markup
        });
    },
    deleteChoice: function (ctrl) {
        Element.remove(ctrl);
    },
    selectMarkup: function (choice, index) {
        var selected,selectedR = '';
        var defaultClass = '';
        var linkTip = '';
        var choiceValue = choice.Choice.gsub("\"", "&quot;");
        if (choice.IsRight == '1') selectedR = 'checked="checked"';
        if (choice.IsDefault == '1') {
            selected = 'checked="checked"';
            if (this.Field.IsRequired == '1') {
                defaultClass = 'class="dropReq"';
                linkTip = this.linkTipMarkupText;
            }
        }
        var tpl = new Template(this.selectMarkupText);
        return tpl.evaluate({
            defaultClassKey: defaultClass,
            linkTipKey: linkTip,
            selectedKey: selected,
            choiceKey: choiceValue,
            selectedRightAnswered: selectedR
        });
    },
    highlightDefaultIfRequired: function (Field) {
        this.unhighlightOldDefaultChoice();
        if (Field.Typeof == 'select' && Field.IsRequired == '1') {
            var choiceToHighlight = '';
            Field.Choices.each(function (num, index) {
                if (num.IsDefault == '1') {
                    choiceToHighlight = $('fieldChoices').down('li', index);
                }
            });
            if (choiceToHighlight != '') {
                if (!choiceToHighlight.hasClassName('dropReq')) {
                    choiceToHighlight.addClassName('dropReq');
                    choiceToHighlight.insert({
                        top: this.choiceToHighlightMarkupText
                    });
                }
            }
        }
    },
    unhighlightOldDefaultChoice: function () {
        var oldReq = $$('.dropReq')[0];
        if (oldReq) {
            var link = oldReq.down('a');
            if (link) link.remove();
            oldReq.removeClassName('dropReq');
        }
    },
    checkboxMarkup: function (choice, index) {
        var selected = selectedR = '';
        if (choice.DefaultVal == '1') selected = 'checked="checked"';
        if (choice.IsRight == '1') selectedR = 'checked="checked"';
        var choiceValue = choice.ChoicesText.gsub("\"", "&quot;");
        var deleteValue = (index != 0) ? this.checkboxMarkupDeleteText : '';
        var tpl = new Template(this.checkboxMarkupText);
        return tpl.evaluate({
            selectedKey: selected,
            choiceKey: choiceValue,
            deleteKey: deleteValue,
            selectedRightAnswered: selectedR
        });
    },
    likert: function () {
        $('listLikert').style.display = 'block';
        this.likertColumns();
        this.likertStatements();
    },
    likertColumns: function () {
        $('likertColumns').innerHTML = '';
        var markup = '';
        this.Field.Choices.each(function (num, index) {
            markup += this.columnMarkup(num, index);
        }.bind(this));
        $('likertColumns').innerHTML = markup;
        if (this.Field.Validation == 'na') {
            var list = $('likertColumns');
            var listEl = $('likertColumns').down('li', $('likertColumns').childElements().length - 1);
            this.highlightLikertColumn(listEl);
            list.down('li', list.childElements().length - 1).down('img').remove();
        }
    },
    columnMarkup: function (choice, index) {
        var selected = selectedR = '';
        if (choice.IsDefault == '1') selected = 'checked="checked"';
        if (choice.IsRight == '1') selectedR = 'checked="checked"';
        var choiceValue = choice.Choice.gsub("\"", "&quot;");
        var deleteValue = (index != 0) ? this.columnMarkupDeleteText : '';
        var tpl = new Template(this.columnMarkupText);
        return tpl.evaluate({
            selectedKey: selected,
            choiceKey: choiceValue,
            deleteKey: deleteValue,
            selectedRightAnswered: selectedR
        });
    },
    highlightLikertColumn: function (choiceToHighlight) {
        if (!choiceToHighlight.hasClassName('dropReq')) {
            choiceToHighlight.addClassName('dropReq');
            choiceToHighlight.insert({
                top: this.highlightLikertColumnMarkupText
            });
        }
    },
    likertStatements: function () {
        $('likertStatements').innerHTML = '';
        var markup = '';
        this.Field.SubFields.each(function (num, index) {
            markup += this.statementMarkup(num, index);
        }.bind(this));
        $('likertStatements').innerHTML = markup;
    },
    statementMarkup: function (choice, index) {
        var choiceValue = choice.ChoicesText.gsub("\"", "&quot;");
        var deleteValue = (index != 0) ? this.statementMarkupDeleteText : '';
        var tpl = new Template(this.statementMarkupText);
        return tpl.evaluate({
            choiceKey: choiceValue,
            deleteKey: deleteValue
        });
    }
});
var PagePropertiesObject = Class.create({
    titleMarkup: '<li><b>#{sequence}.</b> <input class="text" type="text" value="#{title}" ' + 'onkeyup="updateProperties(this.value, \'Title\', #{columnId})" ' + 'onmouseup="updateProperties(this.value, \'Title\', #{columnId})"/></li>',
    initialize: function () {},
    populate: function (Form, Profile) {
        $('formProperties').addClassName('paging');
        this.Profile = Profile;
        this.Form = Form;
        this.selectBreadCrumb();
        this.selectShowPageTitle();
        this.selectShowPageFooter();
        this.displayPageTitles();
    },
    selectBreadCrumb: function () {
        $('breadcrumbs' + this.Form.FormJson.BreadCrumbType).checked = true;
    },
    selectShowPageTitle: function () {
        if (this.Form.FormJson.ShowPageTitle == 1) $('showPageTitles').checked = true;
        else $('showPageTitles').checked = false;
    },
    selectShowPageFooter: function () {
        if (this.Form.FormJson.ShowPageFooter == 1) $('pageFooter').checked = false;
        else $('pageFooter').checked = true;
    },
    displayPageTitles: function () {
        var markup = '';
        $('pageTitles').childElements().each(function (num) {
            if (num.className != 'paymentPageTitle') num.remove();
        });
        if (this.Form.FormJson.MerchantEnabled == '1') {
            $('paymentTitle').value = this.Form.FormJson.PaymentPageTitle;
            $('pageTitles').addClassName('paymentEnabled');
        }
        this.Form.FormJson.Fields.each(function (field, index) {
            var pageTemplate = new Template(this.titleMarkup);
            if (field.Typeof == 'page') {
                var tempPage = {
                    title: field.Title.gsub("\"", "&quot;"),
                    sequence: field.Page,
                    columnId: field.ColumnId
                };
                markup += pageTemplate.evaluate(tempPage);
            }
        }.bind(this));
        $('pageTitles').insert({
            top: markup
        });
    }
});
var FormPreviewObject = Class.create({
    initialize: function () {},
    modify: function (value, parameter, Form, Field) {
        if (this[parameter]) {
            this[parameter](value, Form, Field);
        }
    },
    Name: function (value, Form, Field) {
        if ($('fname')) $('fname').innerHTML = Form.FormJson.Name;
    },
    Description: function (value, Form, Field) {
        if ($('fdescription')) $('fdescription').innerHTML = Form.FormJson.Description.replace(/\n/g, "<br />");
    },
    Language: function (value, Form, Field) {
        if (value == 'preview') {
            $('formLanguage').selectedIndex = 10;
            window.open('http://hoctudau.com', '', 'width=700,height=500,toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes');
        }
        if (value == 'arabic' || value == 'persian' || value == 'hebrew') {
            $('main').addClassName('rtl');
            $('fieldProperties').addClassName('rtl-lang');
            $('formProperties').addClassName('rtl-lang');
        } else {
            $('main').removeClassName('rtl');
            $('fieldProperties').removeClassName('rtl-lang')
            $('formProperties').removeClassName('rtl-lang');
        }
    },
    LabelAlign: function (value, Form, Field) {
        $('formPreview').className = 'hoctudau page1 ' + Form.FormJson.LabelAlign;
    },
    EntryLimit: function (value, Form, Field) {
        if (Form.FormJson.EntryLimit == 0) $('formEntryLimit').value = '';
    },
    StartDate: function (value, Form, Field) {},
    EndDate: function (value, Form, Field) {},
    PageHeader: function (value, Form, Field) {
        var circle = $('pagecircle' + Field.Page);
        if (circle) {
            circle.innerHTML = value;
        }
    },
    replaceHeader: function (markup) {
        $('pageHeader').innerHTML = markup;
    },
    PaymentPageTitle: function (value, Form, Field) {
        var pc = Form.instancesOfField('page') + 1;
        $('pagecircle' + pc).innerHTML = value;
    },
    toggleShowPageHeaderText: function (param) {}
});
var AccountProfile = Class.create({
    data: '', //<sl:translate>
    upgradeRequiredTitle: 'Upgrade Required',
    upgradeRequiredFileMsg: 'In order to add a file upload field, you must upgrade yourHoctudau  subscription. To do so, click on the "Account" link at the top of this page.',
    upgradeRequiredFieldsMsg: 'In order to add more fields, you must upgrade yourHoctudau subscription. To do so, click on the "Account" link at the top of this page.',
    fieldLimitTitle: 'Field Limit',
    fieldLimitMsg: 'You have reached the maximum amount of fields (100) available for a form.', //</sl:translate>
    initialize: function () {
        this.data = CLIENT_JSON.Profile;
        this.initUnlock();
    },
    initUnlock: function () {
        Event.observe(window, 'unload', this.unlock.bind(this), false);
    },
    unlock: function () {},
    permissionToAddField: function (type, fieldCount, changing) {
        if (type == 'file' && this.data.ProfileId == '0') {
            Lightbox.rawUrl("lightboxes/Demo.FileUpload.php");
            return false;
        } else if (type == 'file' && this.data.Plan.Upload == '0') {
            Lightbox.rawUrl("lightboxes/FormBuilder.FileUpload.php");
            return false;
        }
        if (fieldCount >= this.data.Plan.FieldCount && !changing) {
            if (this.data.Plan.FieldCount == '100') {
                lightbox(this.fieldLimitTitle, this.fieldLimitMsg);
            } else if (this.data.ProfileId == '0') {
                Lightbox.rawUrl("lightboxes/Demo.FieldLimit.php");
            } else {
                Lightbox.rawUrl("lightboxes/FormBuilder.FieldLimit.php");
            }
            return false;
        }
        return true;
    },
    isDemoAccount: function () {
        if (this.data.ProfileId == 'demo' || this.data.ProfileId == '0') return true;
        else return false;
    },
    permissionToRedirect: function () {
        if (this.data.ProfileId == '0') {
            Lightbox.rawUrl("lightboxes/Demo.RedirectURL.php");
            return false;
        } else if (this.data.Plan.PlanId <= 1) {
            Lightbox.rawUrl("lightboxes/FormBuilder.RedirectURL.php");
            return false;
        }
        return true;
    },
    getTargetURL: function () {
        if (this.isDemoAccount()) return '/form-builder/';
        else return 'index.php?req=build/';
    }
});
var FormObject = Class.create({
    data: '',
    firstInstruction: true, //<sl:translate>
    encryptedFieldsWarning: 'You may only have up to 5 encrypted fields.',
    paragraphFieldsWarningTitle: 'Size Restriction',
    paragraphFieldsWarning: 'Due to storage space restrictions, a form may have no more than 10 Paragraph fields. The best option is to try and structure certain fields as Single Line text fields.', //</sl:translate>
    initialize: function () {
        this.FormJson = FORM_JSON;
    },
    set: function (value, parameter) {
        switch (parameter) {
            case 'EntryLimit':
                if (value > 0 && value < 99999999999) value = value;
                else value = 0;
                break;
            case 'Language':
                if (value == 'preview') value = 'english';
            case 'StartDate':
                if (value == 'live') {
                    var d = $F('startDate') + '-' + $F('startDate-1') + '-' + $F('startDate-2');
                    if (d == '--') d = '2000-01-01';
                    var hour = $F('startTime-0');
                    if ($F('startTime-2') == 'PM') hour = parseInt(hour) + 12;
                    var t = hour + ':' + $F('startTime-1') + ':00';
                    value = d + ' ' + t;
                }
                break;
            case 'EndDate':
                if (value == 'live') {
                    var d = $F('endDate') + '-' + $F('endDate-1') + '-' + $F('endDate-2');
                    if (d == '--') d = '2030-01-01';
                    var hour = $F('endTime-0');
                    if ($F('endTime-2') == 'PM') hour = parseInt(hour) + 12;
                    var t = hour + ':' + $F('endTime-1') + ':00';
                    value = d + ' ' + t;
                }
                break;
            case 'UseCaptcha':
                addCaptchaToPreview(value);
                break;
            case 'ShowPageTitle':
                if (value == 1) {
                    $('pageHeader').removeClassName('nopagelabel');
                    $('pageTitles').removeClassName('hide');
                } else {
                    $('pageHeader').addClassName('nopagelabel');
                    $('pageTitles').addClassName('hide');
                }
                break;
            case 'ShowPageFooter':
                if (value == 1) $('formFields').removeClassName('hideMarkers');
                else $('formFields').addClassName('hideMarkers');
                break;
            default:
                break;
        }
        this.FormJson[parameter] = value;
    },
    /*
    * ExtraFields: Set
    */
    setExtraField: function (value, parameter, columnId, extraColumnId) {
        var ret = false;
        switch (parameter) {
            case 'Validation':
                if (value == 'email') {
                    this.eraseValidationIfEmail();
                }
                break;
            case 'Instructions':
                if (this.firstInstruction) {
                    this.firstInstruction = false;
                    $('main').removeClassName('noI');
                }
                break;
        }
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.ColumnId == columnId) {
                if(Field.ExtraFields) Field.ExtraFields.each(function (ExtraField, extra_index) {
                    if (ExtraField.ColumnId == extraColumnId) {
                        ExtraField[parameter] = value;
                        switch (parameter) {
                            case 'RangeMin':
                                ExtraField.RangeType = $F('fieldRangeType');
                                if (!(parseInt(value) >= 0)) {
                                    $('fieldRangeMin').value = '';
                                    ExtraField.RangeMin = 0;
                                }
                                break;
                            case 'RangeMax':
                                ExtraField.RangeType = $F('fieldRangeType');
                                if (!(parseInt(value) > 0)) {
                                    $('fieldRangeMax').value = '';
                                    ExtraField.RangeMax = 0;
                                }
                                break;
                            case 'Validation':
                                if (value == 'email') {
                                    ExtraField[parameter] = value;
                                }
                                break;
                            case 'DefaultVal':
                                if (ExtraField.Typeof == 'address') {
                                    ExtraField.DefaultVal = '';
                                    ExtraField.SubFields[5].DefaultVal = value;
                                }
                                break;
                            case 'IsEncrypted':
                                var countOfEncryptedFields = this.instancesOfProperty('IsEncrypted', '1');
                                if (countOfEncryptedFields == 1 && value == '1') Lightbox.showUrl('lightboxes/build-encrypt.php');
                                if (countOfEncryptedFields > 5) {
                                    ExtraField.IsEncrypted = '0';
                                    $('fieldEncrypted').checked = false;
                                    alert(this.encryptedFieldsWarning);
                                }
                                break;
                            default:
                                break;
                        }
                        ret = ExtraField;
                    }
                });
                
            }
        }.bind(this));
        return ret;
    },
    setExtraChoice: function (value, parameter, columnId, extraColumnId, position, ctrl) {
        var ret = false;
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.ColumnId == columnId) {
                if(Field.ExtraFields) Field.ExtraFields.each(function (ExtraField, extra_index) {
                    if (ExtraField.ColumnId == extraColumnId) {
                        if (ExtraField.Typeof == 'checkbox') {
                            if (ExtraField.SubFields[position]) {
                                ExtraField.SubFields[position]['ChoicesText'] = value;
                            }
                        } else if (ExtraField.Typeof == 'likert') {
                            if (ctrl.up('ul').id == 'extra_likertStatements') {
                                if (ExtraField.SubFields[position]) {
                                    ExtraField.SubFields[position]['ChoicesText'] = value;
                                }
                            } else {
                                if (ExtraField.Choices[position]) {
                                    ExtraField.Choices[position][parameter] = value;
                                }
                            }
                        } else {
                            if (ExtraField.Choices[position]) {
                                ExtraField.Choices[position][parameter] = value;
                            }
                        }
                        ret = ExtraField;
                    }
                });
                
            }
        }.bind(this));
        return ret;
    },
    /*
    * ExtraFields: End Set
    */
    setField: function (value, parameter, columnId) {
        var ret = false;
        switch (parameter) {
            case 'Validation':
                if (value == 'email') {
                    this.eraseValidationIfEmail();
                }
                break;
            case 'Instructions':
                if (this.firstInstruction) {
                    this.firstInstruction = false;
                    $('main').removeClassName('noI');
                }
                break;
        }
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.ColumnId == columnId) {
                Field[parameter] = value;
                switch (parameter) {
                    case 'RangeMin':
                        Field.RangeType = $F('fieldRangeType');
                        if (!(parseInt(value) >= 0)) {
                            $('fieldRangeMin').value = '';
                            Field.RangeMin = 0;
                        }
                        break;
                    case 'RangeMax':
                        Field.RangeType = $F('fieldRangeType');
                        if (!(parseInt(value) > 0)) {
                            $('fieldRangeMax').value = '';
                            Field.RangeMax = 0;
                        }
                        break;
                    case 'Validation':
                        if (value == 'email') {
                            Field[parameter] = value;
                        }
                        break;
                    case 'DefaultVal':
                        if (Field.Typeof == 'address') {
                            Field.DefaultVal = '';
                            Field.SubFields[5].DefaultVal = value;
                        }
                        break;
                    case 'IsEncrypted':
                        var countOfEncryptedFields = this.instancesOfProperty('IsEncrypted', '1');
                        if (countOfEncryptedFields == 1 && value == '1') Lightbox.showUrl('lightboxes/build-encrypt.php');
                        if (countOfEncryptedFields > 5) {
                            Field.IsEncrypted = '0';
                            $('fieldEncrypted').checked = false;
                            alert(this.encryptedFieldsWarning);
                        }
                        break;
                    default:
                        break;
                }
                ret = Field;
            }
        }.bind(this));
        return ret;
    },
    setChoice: function (value, parameter, columnId, position, ctrl) {
        var ret = false;
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.ColumnId == columnId) {
                if (Field.Typeof == 'checkbox') {
                    if (Field.SubFields[position]) {
                        Field.SubFields[position]['ChoicesText'] = value;
                    }
                } else if (Field.Typeof == 'likert') {
                    if (ctrl.up('ul').id == 'likertStatements') {
                        if (Field.SubFields[position]) {
                            Field.SubFields[position]['ChoicesText'] = value;
                        }
                    } else {
                        if (Field.Choices[position]) {
                            Field.Choices[position][parameter] = value;
                        }
                    }
                } else {
                    if (Field.Choices[position]) {
                        Field.Choices[position][parameter] = value;
                    }
                }
                ret = Field;
            }
        }.bind(this));
        return ret;
    },
    appendToClassNames: function (setting, columnId) {
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.ColumnId == columnId) {
                Field.ClassNames = Field.ClassNames + ' ' + setting;
            }
        }.bind(this));
    },
    removeFromClassNames: function (setting, columnId) {
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.ColumnId == columnId) {
                Field.ClassNames = Field.ClassNames.gsub(setting, '');
            }
        }.bind(this));
    },
    addSetting: function (setting, columnId) {
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.ColumnId == columnId) {
                Field.Settings.push(setting);
            }
        }.bind(this));
    },
    removeSetting: function (setting, columnId) {
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.ColumnId == columnId) {
                Field.Settings = Field.Settings.without(setting);
            }
        }.bind(this));
    },
    eraseValidationIfEmail: function () {
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.Typeof == 'email' && Field.Validation == 'email') {
                Field.Validation = '';
            }
        });
    },
    permissionToAddField: function (type) {
        if (type == 'textarea') {
            if (this.instancesOfField(type) >= 10) {
                lightbox(this.paragraphFieldsWarningTitle, this.paragraphFieldsWarning);
                return false;
            }
        }
        return true;
    },
    instancesOfField: function (type) {
        var tc = 0;
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.Typeof == type) tc++;
        });
        return tc;
    },
    instancesOfProperty: function (property, value) {
        var tc = 0;
        this.FormJson.Fields.each(function (Field, index) {
            if (Field[property] == value) tc++;
        });
        return tc;
    },
    selectFieldByColumnId: function (columnId) {
        var ret = this.FormJson.Fields.find(function (Field) {
            return Field.ColumnId == columnId;
        });
        return ret;
    },
    /*
    * ExtraFields: Select
    */
    selectExtraFieldByColumnId: function (f_id, columnId) {
        var ret = this.FormJson.Fields.find(function (Field) {
            return Field.ColumnId == f_id;
        });
        var ret2 = false;
        if(ret.ExtraFields) {
            ret2 = ret.ExtraFields.find(function (Field) {
            return Field.ColumnId == columnId;
        });
        }
        
        return ret2;
    },
    /*
    * ExtraFields: End Select
    */
    
    pageHasNoFields: function (columnId) {
        var nofieldsonpage = true;
        var prev = $('foli' + columnId).previous();
        if (prev && prev.title == 'This page has no fields!') prev = prev.previous();
        if (prev && !prev.hasClassName('buttons') && prev.id != 'pageHeader') nofieldsonpage = false;
        return nofieldsonpage;
    },
    calculateFormHeight: function () {
        var heights = [];
        var maxHeight = 1;
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.Typeof != 'page') {
                var offset = $('foli' + Field.ColumnId).offsetHeight;
                if (!heights[Field.Page]) {
                    heights[Field.Page] = offset;
                } else heights[Field.Page] += offset;
            }
        });
        heights.each(function (num, index) {
            if (num > maxHeight) maxHeight = num;
        });
        return maxHeight + 200;
    }
});
var FormEngineObject = Class.create({
    target: 'index.php?req=build/',
    initialize: function (Profile) {
        this.Profile = Profile;
        this.target = this.Profile.getTargetURL();
        Event.observe('wuform', 'mousedown', selectForm, false);
        Event.observe($('saveForm'), 'click', saveForm, false);
        $('saveForm').onclick = function () {
            return false;
        }
    },
    save: function (Form) {
        var myAjax = new Ajax.Request(this.getSaveURL(), {
            method: 'post',
            parameters: 'action=save&form=' + encodeURIComponent(Object.toJSON(Form.FormJson)),
            onComplete: function (r) {
                var ret = r.responseText.evalJSON();
                Event.observe($('saveForm'), 'click', saveForm, false);
                finishSaving(ret);
            }.bind(this)
        });
    },
    saveDemo: function (Form) {
        $('formData').value = Object.toJSON(Form.FormJson);
        $('demoSave').submit();
    },
    getSaveURL: function () {
        var url = document.URL.split('/');
        var urlToUse = this.target;
        if (url[url.length - 2] != 'build' && url[url.length - 2] != 'form-builder') urlToUse += url[url.length - 2] + '/';
        return urlToUse;
    },
    fetchPageHeader: function (Form) {
        var myAjax = new Ajax.Request(this.target, {
            method: 'post',
            parameters: 'action=pageheader&form=' + encodeURIComponent(Object.toJSON(Form.FormJson)),
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishPageHeaders(ret);
            }.bind(this)
        });
    },
    addField: function (Form, json, pos) {
        json.HasntBeenSaved = true;
        if (!pos || pos < 0) pos = 0
        if (json.Typeof == 'checkbox' || json.Typeof == 'likert') {
            json.SubFields.each(function (num, index) {
                num.HasntBeenSaved = true;
            });
        }
        if (Form.FormJson.PageCount > 1) {
            Form.FormJson.Fields.splice(pos, 0, json);
        } else {
            Form.FormJson.Fields.push(json);
        }
    },
    removeField: function (Form, Field) {
        var i = -1;
        Form.FormJson.Fields.each(function (F, index) {
            if (F.ColumnId == Field.ColumnId) {
                i = index;
            }
        });
        if (i >= 0) Form.FormJson.Fields.splice(i, 1);
    },
    /*
    * Extra Fields: Add, Remove
    */
    removeExtraField: function (Form, Field, activeField) {
        var i = -1;
        Form.FormJson.Fields.each(function (F, index) {
            if(F.ColumnId == activeField) {
                F.ExtraFields.each(function (EF, extra_index) {
                    if (EF.ColumnId == Field.ColumnId) {
                        i = extra_index;
                    }
                });
                
                if (i >= 0) F.ExtraFields.splice(i, 1);
            }
            
        });
        
    },
    addExtraField: function (Form, field, json, pos) {
        if (!pos || pos < 0) pos = 0
        if (json.Typeof == 'checkbox' || json.Typeof == 'likert') {
            json.SubFields.each(function (num, index) {
                num.HasntBeenSaved = true;
            });
        }
        Form.FormJson.Fields.each(function (F, index) {
            if (F.ColumnId == field.ColumnId) {
                if(!F.ExtraFields) {
                    F['ExtraFields'] = [];
                }
                F.ExtraFields.push(json);
            }
        });
    },
    /*
    * Extra Fields: End Add
    */
    addChoice: function (Field, Choice, pos) {
        Field.Choices.splice((pos + 1), 0, Choice);
    },
    removeChoice: function (Field, pos) {
        var choice = Field.Choices[pos];
        var defChoice = false;
        if (choice.IsDefault == 1) {
            defChoice = true;
        }
        Field.Choices.splice(pos, 1);
        if (defChoice && Field.Typeof == 'select') this.setDefaultChoice(Field, 0);
    },
    addSubField: function (Field, SubField, pos) {
        SubField.HasntBeenSaved = true;
        Field.SubFields.splice((pos + 1), 0, SubField);
    },
    removeSubField: function (Field, pos) {
        Field.SubFields.splice(pos, 1);
    },
    setDefaultChoice: function (Field, pos, forceDefault) {
        Field.Choices.each(function (choice, index) {
            if (index == pos && (choice.IsDefault == 0 || forceDefault || Field.Typeof == 'select')) choice.IsDefault = 1;
            else if (index == pos && choice.IsDefault == 1) choice.IsDefault = 0;
            else if (Field.Typeof != 'checkbox') choice.IsDefault = 0;
        });
    },
    setDefaultRightChoice: function (Field, pos, forceDefault) {
        Field.Choices.each(function (choice, index) {
            if (index == pos && choice.IsRight == 0) choice.IsRight = 1;
            else if (index == pos && choice.IsRight == 1) choice.IsRight = 0;
        });
    },
    setDefaultCheckboxChoice: function (Field, pos) {
        Field.SubFields.each(function (subfield, index) {
            if (index == pos && subfield.DefaultVal == 0) subfield.DefaultVal = 1;
            else if (index == pos && subfield.DefaultVal == 1) subfield.DefaultVal = 0;
        });
    },
    setDefaultRightCheckboxChoice: function (Field, pos, forceDefault) {
        Field.SubFields.each(function (subfield, index) {
            if (index == pos && subfield.IsRight == 0) subfield.IsRight = 1;
            else if (index == pos && subfield.IsRight == 1) subfield.IsRight = 0;
        });
    },
    incrementPageCount: function (Form) {
        Form.FormJson.PageCount += 1;
    },
    decrementPageCount: function (Form) {
        if (Form.FormJson.PageCount > 1) Form.FormJson.PageCount -= 1;
    }
});
var FieldObject = Class.create({
    initialize: function () {}
});
var FieldEngineObject = Class.create({
    lastColumnId: 1,
    lastExtraColumnId: 1,
    attributes: {
        "text": {
            "FieldCount": 1
        },
        "email": {
            "FieldCount": 1
        },
        "url": {
            "FieldCount": 1
        },
        "number": {
            "FieldCount": 1
        },
        "textarea": {
            "FieldCount": 1
        },
        "file": {
            "FieldCount": 1
        },
        "section": {
            "FieldCount": 1
        },
        "page": {
            "FieldCount": 1
        },
        "time": {
            "FieldCount": 1
        },
        "money": {
            "FieldCount": 1
        },
        "phone": {
            "FieldCount": 1
        },
        "europhone": {
            "FieldCount": 1
        },
        "date": {
            "FieldCount": 1
        },
        "eurodate": {
            "FieldCount": 1
        },
        "shortname": {
            "FieldCount": 2
        },
        "name": {
            "FieldCount": 4
        },
        "address": {
            "FieldCount": 6
        },
        "select": {
            "FieldCount": 1
        },
        "radio": {
            "FieldCount": 1
        },
        "checkbox": {
            "FieldCount": 100
        },
        "likert": {
            "FieldCount": 100
        }
    },
    target: 'index.php?req=build/',
    initialize: function (Form, Profile) {
        this.lastColumnId = 1;
        this.lastExtraColumnId = 1;
        this.formUrl = Form.FormJson.Url;
        this.Form = Form;
        this.Profile = Profile;
        this.target = this.Profile.getTargetURL();
        Form.FormJson.Fields.each(function (Field, index) {
            this.updateLastColumnId(Field);
            this.updateLastExtraColumnId('', Field);
            this.attachLoadEvents(Field.ColumnId);
            if(Field.ExtraFields) {
                Field.ExtraFields.each(function (ExtraField, extra_index) {
                    this.attachLoadExtraEvents(ExtraField.ColumnId);
                }.bind(this));
            }
        }.bind(this));
    },
    attachLoadExtraEvents: function (id) {
        obj = $('extra_foli' + id);
        if (obj) {
            Event.observe(obj, 'click', selectExtraField.bind(obj), false);
            Event.observe(obj, 'mousedown', function () {
                this.style.cursor = 'move';
            }.bind(obj), false);
            Event.observe(obj, 'mouseup', function () {
                this.style.cursor = 'pointer';
            }.bind(obj), false);
        }
    },
    attachLoadEvents: function (id) {
        obj = $('foli' + id);
        if (obj) {
            Event.observe(obj, 'click', selectField.bind(obj), false);
            Event.observe(obj, 'mousedown', function () {
                this.style.cursor = 'move';
            }.bind(obj), false);
            Event.observe(obj, 'mouseup', function () {
                this.style.cursor = 'pointer';
            }.bind(obj), false);
        }
    },
    updateLastColumnId: function (field) {
        var newMax = parseInt(field.ColumnId) + this.attributes[field.Typeof].FieldCount;
        if (newMax >= this.lastColumnId) this.lastColumnId = newMax + 1;
    },
    generateNewColumnId: function (type) {
        ret = this.lastColumnId;
        this.lastColumnId += this.attributes[type].FieldCount;
        return ret;
    },
    addField: function (type, insertAfterElement, dragDropBox, language) {
        var columnId = this.generateNewColumnId(type);
        var myAjax = new Ajax.Request(this.target, {
            method: 'post',
            parameters: 'action=addfield&type=' + type + '&fieldId=' + columnId + '&lang=' + language,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                if (type != 'page') finishAddingField(ret, insertAfterElement, dragDropBox);
                else finishAddingPage(ret, insertAfterElement, dragDropBox);
            }.bind(this)
        });
    },
    /*
    * ExtraFields: ColumnId, Add Extra Field
    */
    updateLastExtraColumnId: function (type, field) {
        if(field.ExtraFields) field.ExtraFields.each(function (ExtraField, index) {
            var newMax = parseInt(ExtraField.ColumnId) + this.attributes[ExtraField.Typeof].FieldCount;
            if (newMax >= this.lastExtraColumnId) this.lastExtraColumnId = newMax + 1;
        }.bind(this));
    
    },
    generateNewExtraColumnId: function (type, field) {
        this.updateLastExtraColumnId(type, field);
        ret = this.lastExtraColumnId;
        this.lastExtraColumnId += this.attributes[type].FieldCount;
        return ret;
    },
    addExtraField: function (type, insertAfterElement, dragDropBox, language, field) {
        var columnId = this.generateNewExtraColumnId(type, field);
        var myAjax = new Ajax.Request(this.target, {
            method: 'post',
            parameters: 'action=addextrafield&type=' + type + '&fieldId=' + columnId + '&lang=' + language,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishAddingExtraField(ret, insertAfterElement, dragDropBox);
            }.bind(this)
        });
    },
    /*
    * ExtraFields: End ColumnId, Add Extra Field
    */
    duplicateExtraField: function (field, field_parent, language) {
        var columnId = this.generateNewExtraColumnId(field.Typeof, field_parent);
        var myAjax = new Ajax.Request(this.target, {
            method: 'post',
            parameters: 'action=addextrafield&type=' + field.Typeof + '&fieldId=' + columnId + '&field=' + encodeURIComponent(Object.toJSON(field)) + '&lang=' + language,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishAddedExtraField(ret);
            }
        });
    },
    duplicateField: function (field, language) {
        var columnId = this.generateNewColumnId(field.Typeof);
        var myAjax = new Ajax.Request(this.target, {
            method: 'post',
            parameters: 'action=addfield&type=' + field.Typeof + '&fieldId=' + columnId + '&field=' + encodeURIComponent(Object.toJSON(field)) + '&lang=' + language,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                if (field.Typeof != 'page') finishAddingField(ret, 'foli' + field.ColumnId);
                else finishAddingPage(ret, 'foli' + field.ColumnId);
            }
        });
    },
    changeField: function (field, newType, sameDbColumn, language) {
        var columnId = field.ColumnId;
        if (!sameDbColumn) columnId = this.generateNewColumnId(newType);
        var myAjax = new Ajax.Request(this.target, {
            method: 'post',
            parameters: 'action=changefield&type=' + newType + '&fieldId=' + columnId + '&field=' + encodeURIComponent(Object.toJSON(field)) + '&lang=' + language,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishChangingField(ret, field.ColumnId, sameDbColumn);
            }
        });
    },
    deleteField: function (field) {
        var myAjax = new Ajax.Request(this.target, {
            method: 'post',
            parameters: 'action=delete&field=' + encodeURIComponent(Object.toJSON(field)) + '&url=' + this.formUrl,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishDeletingField(ret);
            }
        });
    },
    deleteCheckboxChoice: function (field, ctrl) {
        var myAjax = new Ajax.Request(this.target, {
            method: 'post',
            parameters: 'action=delete&field=' + encodeURIComponent(Object.toJSON(field)) + '&url=' + this.formUrl,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishDeletingCheckbox(ret, ctrl);
            }
        });
    },
    deleteLikertStatement: function (field, ctrl) {
        var myAjax = new Ajax.Request(this.target, {
            method: 'post',
            parameters: 'action=delete&field=' + encodeURIComponent(Object.toJSON(field)) + '&url=' + this.formUrl,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishDeletingLikertStatement(ret, ctrl);
            }
        });
    },
    displayField: function (field) {
        var myAjax = new Ajax.Request(this.target, {
            method: 'post',
            parameters: 'action=displayfield&field=' + encodeURIComponent(Object.toJSON(field)),
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishDisplayingField(ret);
            }
        });
    }
});
var FieldPreviewObject = Class.create({
    target: 'index.php?req=build/', //<sl:translate>
    noFieldsInPageMarkupText: '<div id="nfip#{columnIdKey}" class="nofieldsonpage notice" title="This page has no fields!">#{html}</div>', //</sl:translate>
    initialize: function (Profile) {
        this.Profile = Profile;
        this.target = this.Profile.getTargetURL();
    },
    /*
    * ExtraFields: ColumnId, Add Extra Field
    */
    addExtraFieldPreview: function (field, markup, insertAfterElement) {
        var this_field = $('formFields').down('#foli'+field.ColumnId);
        if(!this_field.down('.extra_fields')) {
            this_field.insert('<div class="extra_fields"><ul></ul></div>');
        }
        this_field.down('.extra_fields ul').insert(markup);
    },
    addExtraField: function (field, markup, insertAfterElement) {
        var this_field = $('formFields').down('#foli'+field.ColumnId);
        if(!this_field.down('.extra_fields')) {
            this_field.insert('<div class="extra_fields"><ul></ul></div>');
        }
        this_field.down('.extra_fields ul').insert(markup);
    },
    
    /*
    * ExtraFields: End ColumnId, Add Extra Field
    */
    addField: function (markup, insertAfterElement) {
        if (insertAfterElement == 'top') $('formFields').insert({
            top: markup
        });
        else if (insertAfterElement == 'dragdropbox') {
            var switchMe = $('formFields').select('.dragfld')[0];
            switchMe.addClassName('hide');
            switchMe.insert({
                before: markup
            });
        } else if ($(insertAfterElement)) $(insertAfterElement).insert({
            after: markup
        });
        else {
            var lastChild = $('formFields').lastChild;
            if (!lastChild || lastChild.hasClassName('dragable') || lastChild.hasClassName('paging-context')) {
                $('formFields').insert({
                    bottom: markup
                });
            } else lastChild.previous('li').insert({
                after: markup
            });
        }
    },
    modify: function (value, parameter, Field, type) {
        if (this[parameter]) {
            if(type == 'extra_') {
                this[parameter](value, Field, 'extra_');
            } else {
                this[parameter](value, Field, '');
            }
            
        }
    },
    Title: function (value, Field, extra) {
        if (Field.Title.length > 100) {
            if (!$('fieldTitle').hasClassName('expand')) $('fieldTitle').addClassName('expand');
        } else $('fieldTitle').removeClassName('expand');
        if ($(extra + 'title' + Field.ColumnId)) {
            var el = $(extra + 'title' + Field.ColumnId);
            value = Field.Title + '<span class="req" id="req_' + Field.ColumnId + '"></span>';
            if (el) el.innerHTML = value.replace(/\n/g, "<br />");
            if (Field.IsRequired == 1) this.IsRequired(1, Field);
        }
    },
    Instructions: function (value, Field, extra) {
        el = $(extra + 'instruct' + Field.ColumnId);
        if (el) {
            if (Field.Instructions != '') el.removeClassName('hide');
            else el.addClassName('hide');
            if (Field.Typeof != 'section') el.down().innerHTML = Field.Instructions.replace(/\n/g, "<br />");
            else el.innerHTML = Field.Instructions.replace(/\n/g, "<br />");
        }
    },
    Size: function (value, Field, extra) {
        $(extra + 'Field' + Field.ColumnId).removeClassName('small').removeClassName('medium').removeClassName('large');
        $(extra + 'Field' + Field.ColumnId).addClassName(Field.Size);
    },
    IsRequired: function (value, Field, extra) {
        var el = $(extra + 'req_' + Field.ColumnId);
        (el && value == '1') ? el.innerHTML = ' *' : el.innerHTML = '';
    },
    IsPrivate: function (value, Field, extra) {
        (Field.IsPrivate == 1) ? Element.addClassName(extra + 'foli' + Field.ColumnId, 'private') : Element.removeClassName(extra + 'foli' + Field.ColumnId, 'private');
    },
    Validation: function (value, Field) {
        if (Field.Typeof == 'money') {
            updateFieldDisplay();
        }
    },
    DefaultVal: function (value, Field, extra) {
        switch (Field.Typeof) {
            case 'date':
                break;
            case 'eurodate':
                break;
            case 'phone':
                this.defaultPhone(value, Field, extra);
                break;
            case 'address':
                this.defaultAddress(value, Field, extra);
                break;
            case 'page':
                this.defaultPage(value, Field, extra);
                break;
            default:
                this.defaultText(value, Field, extra);
                break;
        }
    },
    defaultText: function (value, Field, extra) {
        $(extra + 'Field' + Field.ColumnId).value = value;
    },
    defaultPhone: function (value, Field, extra) {
        $(extra + 'Field' + Field.ColumnId).value = value.substring(0, 3);;
        $(extra + 'Field' + Field.ColumnId + '-1').value = value.substring(3, 6);;
        $(extra + 'Field' + Field.ColumnId + '-2').value = value.substring(6, 10);;
    },
    defaultAddress: function (value, Field, extra) {
        var countryDropDown = $(extra + 'foli' + Field.ColumnId).down('select');
        for (var i = 0; i < countryDropDown.options.length; i++) {
            if (countryDropDown.options[i].value == value) countryDropDown.selectedIndex = i;
        }
    },
    defaultPage: function (value, Field, extra) {
        $('previousPageButton' + Field.ColumnId).innerHTML = Field.DefaultVal;
    },
    defaultDate: function (value, Field, extra) {
        if (value != '') {
            var myAjax = new Ajax.Request(this.target, {
                method: 'post',
                parameters: 'action=strtotime&time=' + value,
                onComplete: function (r) {
                    ret = r.responseText.evalJSON();
                    defaultVal = ret.response.date.split('/');
                    $(extra + 'Field' + Field.ColumnId + '-2').value = defaultVal[1];
                    $(extra + 'Field' + Field.ColumnId + '-1').value = defaultVal[0];
                    $(extra + 'Field' + Field.ColumnId).value = defaultVal[2];
                    if (Field.Typeof == 'eurodate') {
                        $(extra + 'Field' + Field.ColumnId + '-2').value = defaultVal[0];
                        $(extra + 'Field' + Field.ColumnId + '-1').value = defaultVal[1];
                    }
                }
            });
        } else {
            $(extra + 'Field' + Field.ColumnId + '-2').value = '';
            $(extra + 'Field' + Field.ColumnId + '-1').value = '';
            $(extra + 'Field' + Field.ColumnId).value = '';
        }
    },
    defaultPrice: function (value, Field, extra) {
        var values = value.split('.');
        var dollars = values[0];
        var cents = values[1];
        $(extra + 'Field' + Field.ColumnId).value = dollars;
        if (cents) $(extra + 'Field' + Field.ColumnId + '-1').value = cents;
        else $(extra + 'Field' + Field.ColumnId + '-1').value = '';
    },
    ChoicesText: function (value, Field, extra) {
        if (Field.Typeof == 'page') {
            if ($(extra + 'listNextButtons').hasClassName('asImg')) {
                $(extra + 'nextPageButtonImg' + Field.ColumnId).src = value;
            } else $(extra + 'nextPageButton' + Field.ColumnId).value = value;
        }
    },
    choiceDefault: function (field, position, ctrl, extra) {
        switch (field.Typeof) {
            case 'radio':
                this.defaultRadio(field, position, ctrl, extra);
                break;
            case 'select':
                this.defaultSelect(field, position, ctrl, extra);
                break;
            case 'checkbox':
                this.defaultCheckbox(field, position, ctrl, extra);
                break;
            case 'likert':
                this.defaultLikert(field, position, ctrl, extra);
                break;
        }
    },
    defaultRadio: function (field, position, ctrl, extra) {
        var i = $(extra + 'foli' + field.ColumnId).down('input', (position + 1));
        if (i.checked) {
            i.checked = false;
            ctrl.checked = false;
        } else i.checked = true;
    },
    defaultLikert: function (field, position, ctrl, extra) {
        var el = $(extra + 'foli' + field.ColumnId);
        var body = el.down('tbody');
        body.childElements().each(function (num, index) {
            var i = num.down('td', position).down('input');
            if (i.checked) {
                i.checked = false;
                ctrl.checked = false;
            } else i.checked = true;
        });
    },
    defaultSelect: function (field, position, ctrl, extra) {
        $(extra + 'Field' + field.ColumnId).selectedIndex = position;
    },
    defaultCheckbox: function (field, position, ctrl, extra) {
        var i = $(extra + 'foli' + field.ColumnId).down('input', position);
        if (i.checked) i.checked = false;
        else i.checked = true;
    },
    removeField: function (Field, extra) {
        if(!extra) extra = '';
        var ctrl = $(extra + 'foli' + Field.ColumnId);
        if ($(extra + 'nfip' + Field.ColumnId)) {
            $(extra + 'nfip' + Field.ColumnId).remove();
        }
        Effect.Fade(ctrl, {
            duration: .5,
            afterFinish: function () {
                ctrl.remove();
                __BUILD.updatePositionsByHtml();
            }.bind(this)
        });
    },
    removeFieldWithoutAnimation: function (Field) {
        $('foli' + Field.ColumnId).remove();
    },
    updateAfterReorder: function (Field, Form) {
        switch (Field.Typeof) {
            case 'section':
                this.SectionLine(Field);
                break;
            case 'page':
                this.updatePageCounter(Field, Form);
                this.removeNoFieldsInPage(Form, Field);
                break;
            default:
                break;
        }
    },
    SectionLine: function (Field) {
        if (Field.Typeof == 'section') {
            var el = $('foli' + Field.ColumnId);
            var previousEl = el.previous('li');
            if (previousEl) previousEl = previousEl.down('div');
            if (Field.Pos == 0) el.addClassName('first');
            else if (previousEl && previousEl.hasClassName('marker')) el.addClassName('first');
            else el.removeClassName('first');
        }
    },
    /*
    * ExtraFields: Choices
    */
    modifyExtraChoice: function (Field, value, pos, ctrl) {
        var extra = 'extra_';
        switch (Field.Typeof) {
            case 'radio':
                if (value == '') value = '&nbsp;'
                var el = $(extra + 'foli' + Field.ColumnId).down('div');
                el.down('label', pos).innerHTML = value;
                break;
            case 'select':
                var el = $(extra + 'Field' + Field.ColumnId);
                el.down(pos).innerHTML = value;
                break;
            case 'checkbox':
                if (value == '') value = '&nbsp;';
                var el = $(extra + 'foli' + Field.ColumnId).down('div');
                el.down('label', pos).innerHTML = value;
                break;
            case 'likert':
                if (ctrl.up('ul').id == 'likertColumns') {
                    var el = $(extra + 'foli' + Field.ColumnId).down('thead');
                    el.down('td', pos).innerHTML = value;
                } else {
                    var el = $(extra + 'foli' + Field.ColumnId).down('tbody');
                    el.down('tr', pos).down('th').down('label').innerHTML = value;
                }
                break;
        }
    },
    /*
    * ExtraFields: End Choices
    */
    modifyChoice: function (Field, value, pos, ctrl) {
        switch (Field.Typeof) {
            case 'radio':
                if (value == '') value = '&nbsp;'
                var el = $('foli' + Field.ColumnId).down('div');
                el.down('label', pos).innerHTML = value;
                break;
            case 'select':
                var el = $('Field' + Field.ColumnId);
                el.down(pos).innerHTML = value;
                break;
            case 'checkbox':
                if (value == '') value = '&nbsp;';
                var el = $('foli' + Field.ColumnId).down('div');
                el.down('label', pos).innerHTML = value;
                break;
            case 'likert':
                if (ctrl.up('ul').id == 'likertColumns') {
                    var el = $('foli' + Field.ColumnId).down('thead');
                    el.down('td', pos).innerHTML = value;
                } else {
                    var el = $('foli' + Field.ColumnId).down('tbody');
                    el.down('tr', pos).down('th').down('label').innerHTML = value;
                }
                break;
        }
    },
    addChoice: function (Field, pos, value, extra) {
        switch (Field.Typeof) {
            case 'radio':
                this.addRadioChoice(Field, pos, value, extra);
                break;
            case 'select':
                this.addSelectChoice(Field, pos, value, extra);
                break;
            case 'checkbox':
                this.addCheckboxChoice(Field, pos, extra);
                break;
            case 'likert':
                this.addLikertChoice(Field, pos, extra);
                break;
        }
    },
    deleteChoice: function (Field, pos, extra) {
        switch (Field.Typeof) {
            case 'radio':
                this.deleteRadioChoice(Field, pos, extra);
                break;
            case 'select':
                this.deleteSelectChoice(Field, pos, extra);
                break;
            case 'checkbox':
                this.deleteCheckboxChoice(Field, pos, extra);
                break;
            case 'likert':
                this.deleteLikertChoice(Field, pos, extra);
                break;
        }
    },
    addRadioChoice: function (Field, pos, value, extra) {
        if (value == '') value = '&nbsp;';
        var el = $(extra + 'foli' + Field.ColumnId).down('div');
        var markup = '<span><input disabled="disabled" id="' + extra + 'Field' + Field.ColumnId + '_' + (pos + 1) + '"' + ' name="' + extra + 'Field' + Field.ColumnId + '"' + ' class="field radio" type="radio" />' + '<label class="choice" for="' + extra + 'Field' + Field.ColumnId + '_' + (pos + 1) + '">' + value + '</label></span>';
        el.down().next('span', pos).insert({
            after: markup
        });
    },
    deleteRadioChoice: function (Field, pos, extra) {
        var el = $(extra + 'foli' + Field.ColumnId).down('div');
        var i = el.down('span', pos);
        Element.remove(i);
    },
    addLikertChoice: function (Field, pos, extra) {
        var el = $(extra + 'foli' + Field.ColumnId);
        var tr = el.down('tr', pos + 1);
        var newEl = tr.cloneNode(true);
        var uniquename = pos + (Math.floor(Math.random() * 1000000 + 1));
        newEl.childElements().each(function (child, index) {
            if (child.down() && child.down('input')) {
                var radio = child.down('input');
                radio.id = radio.id + '_' + index;
                radio.name = radio.name + '_' + uniquename;
            }
        });
        tr.insert({
            after: newEl
        });
        el.down('tr', pos + 2).down('th').down('label').innerHTML = '&nbsp;';
        this.highlightLikertStatements(Field);
    },
    highlightLikertStatements: function (Field, extra) {
        var el = $(extra + 'foli' + Field.ColumnId);
        var body = el.down('tbody');
        var highlight = '';
        body.childElements().each(function (tr, index) {
            tr.className = highlight;
            if (highlight == '') highlight = 'alt';
            else highlight = '';
        });
    },
    deleteLikertChoice: function (Field, pos, extra) {
        var el = $(extra + 'foli' + Field.ColumnId);
        var tr = el.down('tr', pos + 1);
        tr.remove();
    },
    addSelectChoice: function (Field, pos, value, extra) {
        if (Prototype.Browser.IE) {
            var optn = document.createElement("OPTION");
            optn.text = value;
            optn.value = value;
            $(extra + 'Field' + Field.ColumnId).options.add(optn, pos + 1);
        } else $(extra + 'Field' + Field.ColumnId).down(pos).insert({
            after: '<option value="' + value + '">' + value + '</option>'
        });
    },
    deleteSelectChoice: function (Field, pos, extra) {
        $(extra + 'Field' + Field.ColumnId).down(pos).remove();
    },
    addCheckboxChoice: function (Field, pos, extra) {
        var lastCheckbox = $(extra + 'foli' + Field.ColumnId).down('span', (pos + 1));
        var markup = '<span><input disabled="disabled" id="' + extra + 'Field' + Field.ColumnId + '_' + (pos + 1) + '"' + ' name="' + extra + 'Field' + Field.ColumnId + '"' + ' class="field checkbox" type="checkbox" />' + '<label class="choice" for="' + extra + 'Field' + Field.ColumnId + '_' + (pos + 1) + '">&nbsp;</label></span>';
        lastCheckbox.insert({
            after: markup
        });
    },
    deleteCheckboxChoice: function (Field, pos, extra) {
        var el = $(extra + 'foli' + Field.ColumnId).down('div');
        var i = el.down('span', pos);
        Element.remove(i);
    },
    addNoFieldsInPage: function (columnId, Form) {
        if ($('nfip' + columnId)) $('nfip' + columnId).remove();
        var tpl = new Template(this.noFieldsInPageMarkupText);
        var markup = tpl.evaluate({
            columnIdKey: columnId,
            html: $('nofieldsonpage').innerHTML
        });
        var location = 'top';
        if ($('foli' + columnId).previous()) location = $('foli' + columnId).previous();
        this.addField(markup, location);
    },
    removeNoFieldsInPage: function (Form, Field) {
        if (!Form.pageHasNoFields(Field.ColumnId)) {
            if ($('nfip' + Field.ColumnId)) {
                $('nfip' + Field.ColumnId).remove();
            }
        } else {
            this.addNoFieldsInPage(Field.ColumnId, Form);
        }
    },
    updatePageCounter: function (Field, Form) {
        var pages = Form.instancesOfField('page');
        var prevbutton = $$('#foli' + Field.ColumnId + ' .hasprev')[0];
        var nextbutton = $$('#foli' + Field.ColumnId + ' .hasnext')[0];
        if (Field.ChoicesText.include('http')) nextbutton = $$('#foli' + Field.ColumnId + ' .hasnext')[1];
        $('pagecurrent' + Field.ColumnId).innerHTML = Field.Page;
        $('pagetotal' + Field.ColumnId).innerHTML = pages;
        if (Field.Page == 1) prevbutton.addClassName('hide');
        else prevbutton.removeClassName('hide');
        if (Field.Page == pages) nextbutton.addClassName('hide');
        else nextbutton.removeClassName('hide');
    },
    removeDragable: function (ColumnId) {
        $('foli' + ColumnId).removeClassName('dragable');
        $('indicator' + ColumnId).remove();
        $('fa' + ColumnId).remove();
    },
    showPageHeader: function () {
        $('pageHeader').removeClassName('hide');
    },
    hidePageHeader: function () {
        $('pageHeader').addClassName('hide');
    },
    togglePagingButton: function (ColumnId, category, newtype) {
        category = category.toLowerCase();
        if (newtype == 'asImg') {
            $(category + 'PageButton' + ColumnId).addClassName('hide');
            $(category + 'PageButtonImg' + ColumnId).removeClassName('hide');
        } else {
            $(category + 'PageButtonImg' + ColumnId).addClassName('hide');
            $(category + 'PageButton' + ColumnId).removeClassName('hide');
        }
    },
    showPageJump: function (Form) {
        this.adjustPageJump(1, Form)
        $('pagejump').removeClassName('hide');
    },
    hidePageJump: function () {
        $('pagejump').addClassName('hide');
    },
    adjustPageJump: function (pageId, Form) {
        var pages = Form.instancesOfField('page');
        if (pages == pageId) $('jumpnext').style.visibility = 'hidden';
        else $('jumpnext').style.visibility = 'visible';
        if (pageId == 1) $('jumpprev').style.visibility = 'hidden';
        else $('jumpprev').style.visibility = 'visible';
        $('jumptarget').value = pageId;
    }
});
var Interface = Class.create({
    scrolling: false,
    afterScrollTime: 0,
    scrollTime: -1,
    initialize: function () {
        this.initButtons();
    },
    hideStatus: function () {
        hideStatus();
    },
    initButtons: function () {
        this.adjustButtons();
        Event.observe(window, 'scroll', this.adjustButtons.bind(this), false);
        Event.observe(window, 'resize', this.adjustButtons.bind(this), false);
        Event.observe(window, 'scroll', this.afterScroll.bind(this), false);
    },
    showSaveButton: function () {
        Element.removeClassName('formButtons', 'hide');
    },
    hideSaveButton: function () {
        Element.addClassName('formButtons', 'hide');
    },
    showNoFields: function () {
        Element.removeClassName('container', 'hasFields');
    },
    hideNoFields: function () {
        Element.addClassName('container', 'hasFields');
    },
    highlightField: function (id) {
        if (id >= 0) {
            Element.addClassName('foli' + id, 'editing focused');
            $('foli' + id).style.zIndex = "500";
        }
        this.unhighlightForm();
        this.unhighlightPageSettings();
    },
    unhighlightField: function (id) {
        if (id >= 0) {
            Element.removeClassName('foli' + id, 'editing');
            Element.removeClassName('foli' + id, 'focused');
            if ($('foli' + id)) {
                $('foli' + id).style.zIndex = "100";
            }
        }
    },
    /*
    * ExtraFields
    */
    
    highlightExtraField: function (id) {
        if (id >= 0) {
            if ($('extra_foli' + id)) {
                $('extra_foli' + id).addClassName('extra_editing extra_focused');
                $('extra_foli' + id).style.zIndex = "500";
            }
        }
        this.unhighlightForm();
        this.unhighlightPageSettings();
    },
    unhighlightExtraField: function (id) {
        if (id >= 0) {
            if ($$('.extra_fields li')) {
                $$('.extra_fields li').invoke('removeClassName', 'extra_editing');
                $$('.extra_fields li').invoke('removeClassName', 'extra_focused');
                //$$('.extra_fields li').invoke('style.zIndex', '100');
            }
        }
    },
    /*
    * ExtraFields: End
    */
    highlightForm: function () {
        Element.addClassName('wuform', 'editing');
        new Effect.ScrollTo('container', {
            duration: .2
        });
    },
    unhighlightForm: function () {
        Element.removeClassName('wuform', 'editing');
    },
    highlightPageSettings: function () {
        $('pageHeader').addClassName('editing focused');
        new Effect.ScrollTo('container', {
            duration: .2
        });
    },
    unhighlightPageSettings: function () {
        $('pageHeader').removeClassName('editing');
        $('pageHeader').removeClassName('focused');
    },
    showFormActivity: function () {
        $('listStartDate').removeClassName('hide');
        $('listEndDate').removeClassName('hide');
    },
    hideFormActivity: function () {
        $('listStartDate').addClassName('hide');
        $('listEndDate').addClassName('hide');
    },
    showFormPassword: function () {
        Element.removeClassName('formPasswordDiv', 'hide');
    },
    hideFormPassword: function () {
        Element.addClassName('formPasswordDiv', 'hide');
    },
    showFormConfirmationMessage: function () {
        Element.removeClassName('formMessageVal', 'hide');
        Element.addClassName('formRedirectVal', 'hide');
    },
    showFormRedirectUrl: function () {
        Element.addClassName('formMessageVal', 'hide');
        Element.removeClassName('formRedirectVal', 'hide');
    },
    showConfirmationEmailToUser: function () {
        $('formReceiptDiv').removeClassName('hide');
    },
    hideConfirmationEmailToUser: function () {
        $('formReceiptDiv').addClassName('hide');
    },
    snapFieldProperties: function (list) {
        if (list.innerHTML) {
            propsPos = $(list).cumulativeOffset()[1] - 150;
            if (propsPos < 0) propsPos = 0;
            $('fieldProperties').style.marginTop = propsPos + 'px';
        }
    },
    /*
    * ExtraFields
    */
    snapExtraFieldProperties: function (list) {
        if (list.innerHTML) {
            propsPos = $(list).cumulativeOffset()[1] - 150;
            if (propsPos < 0) propsPos = 0;
            $('fieldProperties').style.marginTop = propsPos + 'px';
        }
    },
    /*
    * ExtraFields: End
    */
    adjustButtons: function () {
        var scrollCovered = document.viewport.getScrollOffsets()['top'] + document.viewport.getHeight();
        if (document.viewport.getHeight() <= $('build').offsetHeight && scrollCovered <= $('container').offsetHeight * .96) {
            $('container').addClassName('tooLong')
        } else {
            $('container').removeClassName('tooLong');
        }
    },
    adjustAdd: function () {
        if (document.viewport.getHeight() >= 550 && document.viewport.getScrollOffsets()['top'] >= document.viewport.getHeight() * .20) {
            offset = (document.viewport.getHeight() / 2) - ($('addFields').offsetHeight / 2) - 75;
        } else {
            offset = 0;
        }
        var value = document.viewport.getScrollOffsets()['top'] + offset + 'px';
        $('addFields').morph('margin-top:' + value, {
            duration: .5
        });
    },
    afterScroll: function () {
        if (!this.scrolling) {
            this.scrolling = true;
            this.scrollTime = new Date().getTime();
            setTimeout(this.scrollCheck.bind(this), 100);
        }
    },
    scrollCheck: function () {
        if (this.scrollTime == this.afterScrollTime) {
            this.scrolling = false;
            this.afterScrollTime = 0;
            this.scrollTime = -1;
            this.adjustAdd();
        } else {
            this.afterScrollTime = this.scrollTime;
            setTimeout(arguments.callee.bind(this), 100);
        }
    },
    showDemoMessage: function () {}
});
var FieldsObject = Class.create({
    fieldsData: '',
    lastFieldId: 0,
    activeField: 0,
    activeDetailField: 0,
    Field: '',
    extra: 'extra_',
    fields: '',
    fieldTypeCanNoLongerBeSavedWarning: 'Since this field has already been saved to your account, the field type can no longer be changed.',
    fieldOnceEncryptedWarning: 'Once a field has been encrypted it cannot be changed.',
    radioMarkupText: '<li><input name="extra_rightAnswered" class="" type="checkbox" onclick="extra_changeRightDefault(this)" #{selectedRightAnswered} title="Make this choice right answered." /><input name="extra_allChoices" class="" type="radio" onclick="extra_changeRadioDefault(this)" #{selectedAttribute} title="Make this choice pre-selected." /> <input class="text" type="text" maxlength="150" autocomplete="off" value="#{choice}" onkeyup="extra_updateChoice(this);" /> <img src="template/images/icons/add.png" onclick="extra_addRadio(this)" class="notranslate" alt="Add" title="Add another choice."/> <img src="template/images/icons/delete.png" class="notranslate" onclick="extra_deleteRadio(this)" alt="Delete" title="Delete this choice."/></li>',
    highlightRadioMarkupText: '<a href="#" onclick="extra_toolTipToggle(this, event); return false;" class="tooltip" title="About Other Field" rel="This is the label for the alternative text input for your multiple choice field. It allows your user to enter their own value if none of the other choices are appropriate. Note : If this choice is selected, but no value is entered by the user, this label will be submitted instead.">(?)</a>',
    linkTipMarkupText: '<a href="#" onclick="extra_toolTipToggle(this, event); return false;" class="tooltip" title="About Required Drop Downs" rel="This is the default drop down choice. When you make a drop down field required, you are essentially telling the user they have to make a choice. If they leave the drop down on the default choice (the one that is selected when the page loads),Hoctudau will consider that as not making a choice and will throw an error if not changed.">(?)</a>',
    selectMarkupText: '<li #{defaultClassKey}><input name="extra_rightAnswered" class="" type="checkbox" onclick="extra_changeRightDefault(this)" #{selectedRightAnswered} title="Make this choice right answered." />#{linkTipKey}<input name="extra_allChoices" class="" type="radio" onclick="extra_changeSelectDefault(this)" #{selectedKey} title="Make this choice pre-selected." /> <input class="text" type="text" maxlength="150" autocomplete="off" value="#{choiceKey}" onkeyup="extra_updateChoice(this);" /> <img src="template/images/icons/add.png" class="notranslate" onclick="extra_addSelect(this)" alt="Add" title="Add another choice."/> <img src="template/images/icons/delete.png" class="notranslate" onclick="extra_deleteSelect(this)" alt="Delete" title="Delete this choice."/></li>',
    choiceToHighlightMarkupText: '<a href="#" onclick="extra_toolTipToggle(this, event); return false;" class="tooltip" title="About Required Dropdowns" rel="This is the default down choice. When you make a drop down field required, you are essentially telling the user they have to make a choice. So, if they leave the drop down on the default choice (the one that is selected when the page loads),Hoctudau will consider that as not making a choice and throw an error.">(?)</a>',
    checkboxMarkupText: '<li><input name="extra_rightAnswered" class="" type="checkbox" onclick="extra_changeRightDefault(this)" #{selectedRightAnswered} title="Make this choice right answered." /><input name="extra_allChoices" class="" type="checkbox" onclick="extra_changeCheckboxDefault(this)" #{selectedKey} title="Make this choice pre-selected." /> <input class="text" type="text" maxlength="150" autocomplete="off" value="#{choiceKey}" onkeyup="extra_updateChoice(this);" /> <img src="template/images/icons/add.png" class="notranslate" onclick="extra_addCheckbox(this)" alt="Add" title="Add another choice."/>#{deleteKey}</li>',
    checkboxMarkupDeleteText: '<img src="template/images/icons/delete.png" class="notranslate" onclick="extra_deleteCheckbox(this)" alt="Delete" title="Delete this choice."/>',
    columnMarkupText: '<li><input name="extra_rightAnswered" class="" type="checkbox" onclick="extra_changeRightDefault(this)" #{selectedRightAnswered} title="Make this choice right answered." /><input name="extra_allChoices" type="radio" #{selectedKey} onclick="extra_changeLikertDefault(this);" title="Make this column pre-selected." /> <input class="text" type="text" value="#{choiceKey}" onkeyup="extra_updateChoice(this);" /> <img src="template/images/icons/add.png" class="notranslate" alt="Add" onclick="extra_addLikertChoice(this);" title="Add another choice."/>#{deleteKey}</li>',
    columnMarkupDeleteText: '<img src="template/images/icons/delete.png" class="notranslate" alt="Delete" onclick="extra_deleteLikertChoice(this);" title="Delete this column."/> ',
    highlightLikertColumnMarkupText: '<a href="#" onclick="extra_toolTipToggle(this, event); return false;" class="tooltip" title="About Likert Non Applicable" rel="This choice represents a null selection, and will have a score of 0 assigned to it. It is ideal for situations where you wish to make a likert field required, but you still want to allow users to opt out of certain questions.">(?)</a>',
    statementMarkupText: '<li><input class="text statement" type="text" value="#{choiceKey}" onkeyup="extra_updateChoice(this);" /> <img src="template/images/icons/add.png" class="notranslate" alt="Add" onclick="extra_addLikertStatement(this);" title="Add another choice."/>#{deleteKey}</li>',
    statementMarkupDeleteText: '<img src="template/images/icons/delete.png" class="notranslate" alt="Delete" onclick="extra_deleteLikertStatement(this)" title="Delete this choice."/> ',
    charactersOptionText: 'Characters',
    wordsOptionText: 'Words',
    valueOptionText: 'Value',
    digitsOptionText: 'Digits', //</sl:translate>
    initialize: function () {
        this.fieldsData = FIELDS_JSON;
        this.show = new Array();
        this.show['text'] = new Array('types', 'size', 'displayInputAnswer', 'displayAnswer', 'required', 'excerpt', 'unique', 'isPrivate', 'textDefault', 'encryption', 'range', 'instructions');
        this.show['textarea'] = new Array('types', 'size', 'required', 'excerpt', 'unique', 'isPrivate', 'textDefault', 'range', 'instructions');
        this.show['select'] = new Array('types', 'size', 'choices', 'displayAnswer', 'required', 'excerpt', 'isPrivate', 'populate', 'instructions');
        this.show['radio'] = new Array('types', 'layout', 'choices', 'displayInputAnswer', 'displayAnswer', 'required', 'excerpt', 'isPrivate', 'randomize', 'other', 'populate', 'instructions');
        this.show['checkbox'] = new Array('types', 'layout', 'choices', 'displayAnswer', 'required', 'isPrivate', 'instructions');
        this.show['likert'] = new Array('types', 'required', 'isPrivate', 'likert', 'numbers', 'na');
        this.show['name'] = new Array('types', 'required', 'isPrivate', 'name', 'instructions');
        this.show['shortname'] = new Array('types', 'required', 'isPrivate', 'name', 'instructions');
        this.show['date'] = new Array('types', 'required', 'unique', 'isPrivate', 'date', 'dateDefault', 'instructions');
        this.show['eurodate'] = new Array('types', 'required', 'unique', 'isPrivate', 'date', 'euroDateDefault', 'instructions');
        this.show['time'] = new Array('types', 'required', 'unique', 'isPrivate', 'instructions');
        this.show['phone'] = new Array('types', 'required', 'unique', 'isPrivate', 'phone', 'phoneDefault', 'instructions');
        this.show['europhone'] = new Array('types', 'required', 'unique', 'isPrivate', 'phone', 'textDefault', 'instructions');
        this.show['address'] = new Array('types', 'required', 'isPrivate', 'addressDefault', 'instructions');
        this.show['money'] = new Array('types', 'required', 'unique', 'isPrivate', 'moneyDefault', 'currency', 'range', 'instructions');
        this.show['linked'] = new Array('types', 'required', 'unique', 'isPrivate', 'instructions');
        this.show['url'] = new Array('types', 'size', 'required', 'excerpt', 'unique', 'isPrivate', 'textDefault', 'instructions');
        this.show['email'] = new Array('types', 'size', 'required', 'excerpt', 'unique', 'isPrivate', 'textDefault', 'instructions');
        this.show['number'] = new Array('types', 'size', 'required', 'excerpt', 'unique', 'isPrivate', 'textDefault', 'range', 'instructions');
        this.show['rating'] = new Array('types', 'option', 'instructions');
        this.show['tags'] = new Array('types', 'option', 'instructions');
        this.show['file'] = new Array('types', 'required', 'isPrivate', 'instructions');
        this.show['password'] = new Array('types', 'option', 'instructions');
        this.show['math'] = new Array('types', 'option', 'instructions');
        this.show['section'] = new Array('isPrivate', 'instructions');
        this.show['page'] = new Array('buttons');
    },
    /*
    * -----------------
    */
    display: function (Field) {
        if(Field.field_json) this.Field = Field.field_json;
        else this.Field = Field;
        extra = this.extra;
        $(extra + 'fieldProperties').style.display = 'block';
        this.hideAll();
        this.title();
        for (var x = 0; x < this.show[this.Field.Typeof].length; x++) {
            if(typeof this[extra + this.show[this.Field.Typeof][x]] == 'function') {
                this[extra + this.show[this.Field.Typeof][x]]();
            } else {
                this[this.show[this.Field.Typeof][x]]();
            }
        }
    },
    hideAll: function () {
        var extra = this.extra;
        $(extra + 'listSecurity').style.display = 'none';
        $(extra + 'listType').style.display = 'none';
        $(extra + 'listInstructions').style.display = 'none';
        $(extra + 'listPopulate').style.display = 'none';
        $(extra + 'listTextDefault').style.display = 'none';
        $(extra + 'listPhoneDefault').style.display = 'none';
        $(extra + 'listDateDefault').style.display = 'none';
        $(extra + 'listPriceDefault').style.display = 'none';
        $(extra + 'listEuroDateDefault').style.display = 'none';
        $(extra + 'listAddressDefault').style.display = 'none';
        $(extra + 'listDateFormat').style.display = 'none';
        $(extra + 'listPhoneFormat').style.display = 'none';
        $(extra + 'listMoneyFormat').style.display = 'none';
        $(extra + 'listSize').style.display = 'none';
        $(extra + 'listNameFormat').style.display = 'none';
        $(extra + 'listChoices').style.display = 'none';
        $(extra + 'listOptions').style.display = 'none';
        $(extra + 'listInputAnswer').style.display = 'none';
        $(extra + 'listDisplayAnswer').style.display = 'none';
        $(extra + 'listRange').style.display = 'none';
        $(extra + 'listNextButtons').style.display = 'none';
        $(extra + 'listPreviousButtons').style.display = 'none';
        $(extra + 'listClassNames').style.display = 'none';
        $(extra + 'listLikert').style.display = 'none';
        $(extra + 'listLayout').style.display = 'none';
        $(extra + 'listButtons').style.display = 'none';
        $(extra + 'fieldPos').style.display = 'none';
    },
    title: function () {
        if( this.Field ) if (this.Field.Typeof == 'page') $(this.extra + 'listTitle').style.display = 'none';
        else $(this.extra + 'listTitle').style.display = 'block';
        $(this.extra + 'fieldTitle').value = this.Field.Title;
    },
    types: function () {
        $(this.extra + 'listType').style.display = 'block';
        var func = this.typeNotify;
        var type = this.Field.Typeof;
        if (this.Field.HasntBeenSaved) {
            $(this.extra + 'fieldType').disabled = false;
            Event.stopObserving('fieldType', 'click', func);
        } else {
            $(this.extra + 'fieldType').disabled = true;
            Event.observe('fieldType', 'click', func, false);
        }
        if (type == 'name') type = 'shortname';
        if (type == 'europhone') type = 'phone';
        if (type == 'eurodate') type = 'date';
        for (var i = 0; i < $(this.extra + 'fieldType').options.length; i++) {
            if ($(this.extra + 'fieldType').options[i].value == type) {
                $(this.extra + 'fieldType').options[i].selected = true;
                break;
            }
        }
    },
    typeNotify: function () {
        alert(this.fieldTypeCanNoLongerBeSavedWarning);
    },
    size: function () {
        $(this.extra + 'listSize').style.display = 'block';
        for (var i = 0; i < $('fieldSize').options.length; i++) {
            if ($(this.extra + 'fieldSize').options[i].value == this.Field.Size) {
                $(this.extra + 'fieldSize').options[i].selected = true;
                break;
            }
        }
    },
    layout: function () {
        $(this.extra + 'listLayout').style.display = 'block';
        var setting = '';
        if (this.Field.Settings.indexOf('notStacked') != -1) setting = 'notStacked';
        if (this.Field.Settings.indexOf('twoColumns') != -1) setting = 'twoColumns';
        if (this.Field.Settings.indexOf('threeColumns') != -1) setting = 'threeColumns';
        for (var i = 0; i < $(this.extra + 'fieldLayout').options.length; i++) {
            if ($(this.extra + 'fieldLayout').options[i].value == setting) {
                $(this.extra + 'fieldLayout').options[i].selected = true;
                break;
            }
        }
    },
    required: function () {
        $(this.extra + 'listOptions').style.display = 'block';
        (this.Field.IsRequired == '1') ? $(this.extra + 'fieldRequired').checked = true : $(this.extra + 'fieldRequired').checked = false;
        var ftype = this.Field.Typeof;
        (ftype == 'checkbox' || ftype == 'radio' || ftype == 'select' || ftype == 'name' || ftype == 'shortname' || ftype == 'address' || ftype == 'likert') ? this.toggleUnique('hide') : this.toggleUnique('show');
        (ftype == 'text' && CLIENT_JSON.Profile.PlanId >= 3) ? this.toggleEncrypted('show') : this.toggleEncrypted('hide');
        (ftype == 'text' || ftype == 'radio' || ftype == 'select' || ftype == 'number' || ftype == 'url') ? this.toggleExerpt('show') : this.toggleExerpt('hide');
        (ftype == 'radio') ? this.toggleRandomize('show') : this.toggleRandomize('hide');
        (ftype == 'radio') ? this.toggleOther('show') : this.toggleOther('hide');
        
        
        (ftype == 'likert') ? this.toggleNumbers('show') : this.toggleNumbers('hide');
        (ftype == 'likert') ? this.toggleNA('show') : this.toggleNA('hide');
        (ftype == 'likert') ? this.toggleDC('show') : this.toggleDC('hide');
    },
    toggleExerpt: function (view) {
        excerpts = document.getElementsByClassName('fieldExcerptToggle');
        for (var i = 0; i < excerpts.length; i++) {
            (view == 'hide') ? excerpts[i].addClassName('hide') : excerpts[i].removeClassName('hide');
        }
    },
    displayInputAnswer: function () {
        $(this.extra + 'listInputAnswer').style.display = 'block';
        $(this.extra + 'fieldInputRightAnswer').value = this.Field.InputRightAnswer;
        $(this.extra + 'fieldInputRightAnswerRegex').checked = this.Field.InputRightAnswerRegex || false;
    },
    displayAnswer: function () {
        $(this.extra + 'listDisplayAnswer').style.display = 'block';
        $(this.extra + 'fieldDisplayRightAnswer').value = this.Field.RightAnswerDescription;
        $(this.extra + 'fieldDisplayWrongAnswer').value = this.Field.WrongAnswerDescription;
    },
    toggleRandomize: function (view) {
        var randoms = document.getElementsByClassName('fieldRandomToggle');
        for (var i = 0; i < randoms.length; i++) {
            (view == 'hide') ? randoms[i].addClassName('hide') : randoms[i].removeClassName('hide');
        }
    },
    toggleOther: function (view) {
        if ($('fieldOther')) {
            var others = document.getElementsByClassName('fieldOtherToggle');
            for (var i = 0; i < others.length; i++) {
                (view == 'hide') ? others[i].addClassName('hide') : others[i].removeClassName('hide');
            }
        }
    },
    toggleNumbers: function (view) {
        var numbs = document.getElementsByClassName('fieldNumbersToggle');
        for (var i = 0; i < numbs.length; i++) {
            (view == 'hide') ? numbs[i].addClassName('hide') : numbs[i].removeClassName('hide');
        }
    },
    toggleNA: function (view) {
        var nonapp = document.getElementsByClassName('fieldNAToggle');
        for (var i = 0; i < nonapp.length; i++) {
            (view == 'hide') ? nonapp[i].addClassName('hide') : nonapp[i].removeClassName('hide');
        }
    },
    toggleDC: function (view) {
        var nonapp = document.getElementsByClassName('fieldDCToggle');
        for (var i = 0; i < nonapp.length; i++) {
            (view == 'hide') ? nonapp[i].addClassName('hide') : nonapp[i].removeClassName('hide');
        }
    },
    randomize: function () {
        (this.Field.Validation == 'rand') ? $(this.extra + 'fieldRandom').checked = true : $(this.extra + 'fieldRandom').checked = false;
    },
    numbers: function () {
        (this.Field.Settings.indexOf('hideNumbers') != -1) ? $(this.extra + 'fieldNumbers').checked = true : $(this.extra + 'fieldNumbers').checked = false;
    },
    buttons: function () {
        if (this.Form.instancesOfField('page') != this.Field.Page) {
            $(this.extra + 'listNextButtons').style.display = 'block';
            this.displayNextPage();
        } else $(this.extra + 'listNextButtons').style.display = 'none';
        if (this.Field.Page != 1) {
            $(this.extra + 'listPreviousButtons').style.display = 'block';
            this.displayPreviousPage();
        } else $(this.extra + 'listPreviousButtons').style.display = 'none';
    },
    displayNextPage: function () {
        var btn = this.Field.ChoicesText;
        if (btn.include('http') && btn.include('.')) {
            $(this.extra + 'listNextButtons').removeClassName('asTxt');
            $(this.extra + 'listNextButtons').addClassName('asImg');
            $(this.extra + 'nextButtonLink').checked = true;
            $(this.extra + 'nextButtonImg').value = btn;
            $(this.extra + 'nextButtonsText').value = 'Next Page';
        } else {
            $(this.extra + 'listNextButtons').removeClassName('asImg');
            $(this.extra + 'listNextButtons').addClassName('asTxt');
            $(this.extra + 'nextButtonString').checked = true;
            $(this.extra + 'nextButtonsText').value = btn;
            $(this.extra + 'nextButtonImg').value = 'http://';
        }
    },
    displayPreviousPage: function () {
        $(this.extra + 'previousButtonsText').value = this.Field.DefaultVal;
    },
    other: function () {
        if ($(this.extra + 'fieldOther')) {
            (this.Field.Settings.indexOf('other') != -1) ? $(this.extra + 'fieldOther').checked = true : $(this.extra + 'fieldOther').checked = false;
        }
    },
    na: function () {
        (this.Field.Validation == 'na') ? $(this.extra + 'fieldNA').checked = true : $(this.extra + 'fieldNA').checked = false;
        (this.Field.Validation == 'dc') ? $(this.extra + 'fieldDC').checked = true : $(this.extra + 'fieldDC').checked = false;
    },
    toggleUnique: function (view) {
        uniques = document.getElementsByClassName('fieldUniqueToggle');
        for (var i = 0; i < uniques.length; i++) {
            (view == 'hide') ? uniques[i].addClassName('hide') : uniques[i].removeClassName('hide');
        }
    },
    unique: function () {
        (this.Field.IsUnique == '1') ? $(this.extra + 'fieldUnique').checked = true : $(this.extra + 'fieldUnique').checked = false;
        
    },
    excerpt: function () {
        (this.Field.Excerpt == '1') ? $(this.extra + 'fieldExcerpt').checked = true : $(this.extra + 'fieldExcerpt').checked = false;
    },
    isPrivate: function () {
        $(this.extra + 'listSecurity').style.display = 'block';
        (this.Field.IsPrivate == '1') ? $(this.extra + 'fieldPrivate').checked = true : $(this.extra + 'fieldPublic').checked = true;
    },
    encryption: function () {
        (this.Field.IsEncrypted == '1') ? $(this.extra + 'fieldEncrypted').checked = true : $(this.extra + 'fieldEncrypted').checked = false;
        var func = this.encryptNotify;
        if (this.Field.HasntBeenSaved) {
            $(this.extra + 'fieldEncrypted').disabled = false;
            Event.stopObserving(this.extra + 'fieldEncrypted', 'click', func);
        } else {
            $(this.extra + 'fieldEncrypted').disabled = true;
            Event.observe(this.extra + 'fieldEncrypted', 'click', func, false);
        }
    },
    encryptNotify: function () {
        alert(this.fieldOnceEncryptedWarning);
    },
    toggleEncrypted: function (view) {
        encrypts = document.getElementsByClassName('fieldEncryptedToggle');
        for (var i = 0; i < encrypts.length; i++) {
            (view == 'hide') ? encrypts[i].addClassName('hide') : encrypts[i].removeClassName('hide');
        }
    },
    range: function () {
        $(this.extra + 'listRange').style.display = 'block';
        this.determineRangeDropDownTypes();
        var minRange = '0';
        var maxRange = '';
        if (parseInt(this.Field.RangeMin) > 0) minRange = this.Field.RangeMin;
        if (parseInt(this.Field.RangeMax) > 0 && parseInt(this.Field.RangeMax) >= minRange) maxRange = this.Field.RangeMax;
        $(this.extra + 'fieldRangeMin').value = minRange;
        $(this.extra + 'fieldRangeMax').value = maxRange;
        for (var i = 0; i < $(this.extra + 'fieldRangeType').options.length; i++) {
            if ($(this.extra + 'fieldRangeType').options[i].value == this.Field.RangeType) {
                $(this.extra + 'fieldRangeType').options[i].selected = true;
                break;
            }
        }
    },
    determineRangeDropDownTypes: function () {
        var el = $(this.extra + 'fieldRangeType');
        el.options.length = 0;
        if (this.Field.Typeof == 'text' || this.Field.Typeof == 'textarea') {
            el.options[0] = new Option(this.charactersOptionText, 'character');
            el.options[1] = new Option(this.wordsOptionText, 'word');
        }
        if (this.Field.Typeof == 'number') {
            el.options[0] = new Option(this.valueOptionText, 'value');
            el.options[1] = new Option(this.digitsOptionText, 'digit');
        }
        if (this.Field.Typeof == 'money') {
            el.options[0] = new Option(this.valueOptionText, 'value');
        }
    },
    populate: function () {
        $(this.extra + 'listPopulate').style.display = 'block';
    },
    textDefault: function () {
        $(this.extra + 'listTextDefault').style.display = 'block';
        $(this.extra + 'fieldDefault').value = this.Field.DefaultVal;
    },
    phoneDefault: function () {
        $(this.extra + 'listPhoneDefault').style.display = 'block';
        fieldVal = this.Field.DefaultVal;
        $(this.extra + 'fieldPhoneDefault1').value = fieldVal.substring(0, 3);
        $(this.extra + 'fieldPhoneDefault2').value = fieldVal.substring(3, 6);
        $(this.extra + 'fieldPhoneDefault3').value = fieldVal.substring(6, 10);
    },
    moneyDefault: function () {
        $(this.extra + 'listPriceDefault').style.display = 'block';
        $(this.extra + 'fieldPriceDefault').value = this.Field.DefaultVal;
    },
    addressDefault: function () {
        $(this.extra + 'listAddressDefault').style.display = 'block';
        fieldVal = this.Field.SubFields[5].DefaultVal;
        countries = $('fieldCountries');
        for (var i = 0; i < countries.options.length; i++) {
            if (countries.options[i].value == fieldVal) countries.selectedIndex = i;
        }
    },
    dateDefault: function () {
        $(this.extra + 'listDateDefault').style.display = 'block';
        $(this.extra + 'fieldDateDefault').value = this.Field.DefaultVal;
    },
    euroDateDefault: function () {
        $(this.extra + 'listEuroDateDefault').style.display = 'block';
        $(this.extra + 'fieldEuroDateDefault').value = this.Field.DefaultVal;
    },
    date: function () {
        $(this.extra + 'listDateFormat').style.display = 'block';
        (this.Field.Typeof == 'date') ? $(this.extra + 'fieldDateAmerican').selected = true : $(this.extra + 'fieldDateEuro').selected = true;
    },
    name: function () {
        $(this.extra + 'listNameFormat').style.display = 'block';
        if (this.Field.HasntBeenSaved) $(this.extra + 'nameFormat').disabled = false;
        else $(this.extra + 'nameFormat').disabled = true;
        (this.Field.Typeof == 'shortname') ? $(this.extra + 'fieldNameNormal').selected = true : $(this.extra + 'fieldNameExtended').selected = true;
    },
    phone: function () {
        $(this.extra + 'listPhoneFormat').style.display = 'block';
        (this.Field.Typeof == 'phone') ? $(this.extra + 'fieldPhoneAmerican').selected = true : $(this.extra + 'fieldPhoneEuro').selected = true;
    },
    currency: function () {
        var validation = this.Field.Validation;
        $(this.extra + 'listMoneyFormat').style.display = 'block';
        if (validation == 'euro') $(this.extra + 'fieldMoneyEuro').selected = true;
        else if (validation == 'pound') $(this.extra + 'fieldMoneyPound').selected = true;
        else if (validation == 'yen') $(this.extra + 'fieldMoneyYen').selected = true;
        else if (validation == 'pesos') $(this.extra + 'fieldMoneyPesos').selected = true;
        else $(this.extra + 'fieldMoneyAmerican').selected = true;
    },
    instructions: function () {
        $(this.extra + 'listInstructions').style.display = 'block';
        $(this.extra + 'fieldInstructions').value = this.Field.Instructions;
    },
    ClassNames: function () {
        $(this.extra + 'listClassNames').style.display = 'block';
        $(this.extra + 'fieldClassNames').value = this.Field.ClassNames;
    },
    choices: function () {
        $(this.extra + 'listChoices').style.display = 'block';
        $(this.extra + 'fieldChoices').innerHTML = '';
        var choicesMarkup = '';
        if (this.Field.Typeof == 'checkbox') {
            this.Field.SubFields.each(function (num, index) {
                choicesMarkup += this[this.Field.Typeof + 'Markup'](num, index);
            }.bind(this));
        } else {
            this.Field.Choices.each(function (num, index) {
                choicesMarkup += this[this.Field.Typeof + 'Markup'](num, index);
            }.bind(this));
        }
        $(this.extra + 'fieldChoices').innerHTML = choicesMarkup;
        if (this.Field.Typeof == 'radio' && this.Field.Settings.indexOf('other') != -1) {
            var list = $(this.extra + 'fieldChoices');
            var listEl = $(this.extra + 'fieldChoices').down('li', $(this.extra + 'fieldChoices').childElements().length - 1);
            this.highlightRadioOther(listEl);
            list.down('li', list.childElements().length - 1).down('img').remove();
        }
    },
    radioMarkup: function (choice, index) {
        var selected, selectedR = '';
        if (choice.IsDefault == '1') selected = 'checked="checked"';
        if (choice.IsRight == '1') selectedR = 'checked="checked"';
        var choiceValue = choice.Choice.gsub("\"", "&quot;");
        var tpl = new Template(this.radioMarkupText);
        return tpl.evaluate({
            selectedAttribute: selected,
            choice: choiceValue,
            selectedRightAnswered: selectedR
        });
    },
    highlightRadioOther: function (choiceToHighlight) {
        if (!choiceToHighlight.hasClassName('dropReq')) {
            choiceToHighlight.addClassName('dropReq');
            choiceToHighlight.insert({
                top: this.highlightRadioMarkupText
            });
        }
    },
    insertChoice: function (ctrl, value, type) {
        var choice = {
            "Choice": value,
            "IsDefault": "0",
            "IsRight": "1"
        }
        if (type == 'checkbox' || type == 'statement') {
            choice = {
                "ChoicesText": value,
                "DefaultVal": "0",
                "IsRight": "1"
            }
        }
        var markup = this[type + 'Markup'](choice, 1);
        ctrl.insert({
            after: markup
        });
    },
    deleteChoice: function (ctrl) {
        Element.remove(ctrl);
    },
    selectMarkup: function (choice, index) {
        var selected,selectedR = '';
        var defaultClass = '';
        var linkTip = '';
        var choiceValue = choice.Choice.gsub("\"", "&quot;");
        if (choice.IsRight == '1') selectedR = 'checked="checked"';
        if (choice.IsDefault == '1') {
            selected = 'checked="checked"';
            if (this.Field.IsRequired == '1') {
                defaultClass = 'class="dropReq"';
                linkTip = this.linkTipMarkupText;
            }
        }
        var tpl = new Template(this.selectMarkupText);
        return tpl.evaluate({
            defaultClassKey: defaultClass,
            linkTipKey: linkTip,
            selectedKey: selected,
            choiceKey: choiceValue,
            selectedRightAnswered: selectedR
        });
    },
    highlightDefaultIfRequired: function (Field) {
        this.unhighlightOldDefaultChoice();
        if (Field.Typeof == 'select' && Field.IsRequired == '1') {
            var choiceToHighlight = '';
            Field.Choices.each(function (num, index) {
                if (num.IsDefault == '1') {
                    choiceToHighlight = $(this.extra + 'fieldChoices').down('li', index);
                }
            });
            if (choiceToHighlight != '') {
                if (!choiceToHighlight.hasClassName('dropReq')) {
                    choiceToHighlight.addClassName('dropReq');
                    choiceToHighlight.insert({
                        top: this.choiceToHighlightMarkupText
                    });
                }
            }
        }
    },
    unhighlightOldDefaultChoice: function () {
        var oldReq = $$('.dropReq')[0];
        if (oldReq) {
            var link = oldReq.down('a');
            if (link) link.remove();
            oldReq.removeClassName('dropReq');
        }
    },
    checkboxMarkup: function (choice, index) {
        var selected = selectedR = '';
        if (choice.DefaultVal == '1') selected = 'checked="checked"';
        if (choice.IsRight == '1') selectedR = 'checked="checked"';
        var choiceValue = choice.ChoicesText.gsub("\"", "&quot;");
        var deleteValue = (index != 0) ? this.checkboxMarkupDeleteText : '';
        var tpl = new Template(this.checkboxMarkupText);
        return tpl.evaluate({
            selectedKey: selected,
            choiceKey: choiceValue,
            deleteKey: deleteValue,
            selectedRightAnswered: selectedR
        });
    },
    likert: function () {
        $(this.extra + 'listLikert').style.display = 'block';
        this.likertColumns();
        this.likertStatements();
    },
    likertColumns: function () {
        $(this.extra + 'likertColumns').innerHTML = '';
        var markup = '';
        this.Field.Choices.each(function (num, index) {
            markup += this.columnMarkup(num, index);
        }.bind(this));
        $(this.extra + 'likertColumns').innerHTML = markup;
        if (this.Field.Validation == 'na') {
            var list = $(this.extra + 'likertColumns');
            var listEl = $(this.extra + 'likertColumns').down('li', $(this.extra + 'likertColumns').childElements().length - 1);
            this.highlightLikertColumn(listEl);
            list.down('li', list.childElements().length - 1).down('img').remove();
        }
    },
    columnMarkup: function (choice, index) {
        var selected = selectedR = '';
        if (choice.IsDefault == '1') selected = 'checked="checked"';
        if (choice.IsRight == '1') selectedR = 'checked="checked"';
        var choiceValue = choice.Choice.gsub("\"", "&quot;");
        var deleteValue = (index != 0) ? this.columnMarkupDeleteText : '';
        var tpl = new Template(this.columnMarkupText);
        return tpl.evaluate({
            selectedKey: selected,
            choiceKey: choiceValue,
            deleteKey: deleteValue,
            selectedRightAnswered: selectedR
        });
    },
    highlightLikertColumn: function (choiceToHighlight) {
        if (!choiceToHighlight.hasClassName('dropReq')) {
            choiceToHighlight.addClassName('dropReq');
            choiceToHighlight.insert({
                top: this.highlightLikertColumnMarkupText
            });
        }
    },
    likertStatements: function () {
        $(this.extra + 'likertStatements').innerHTML = '';
        var markup = '';
        this.Field.SubFields.each(function (num, index) {
            markup += this.statementMarkup(num, index);
        }.bind(this));
        $(this.extra + 'likertStatements').innerHTML = markup;
    },
    statementMarkup: function (choice, index) {
        var choiceValue = choice.ChoicesText.gsub("\"", "&quot;");
        var deleteValue = (index != 0) ? this.statementMarkupDeleteText : '';
        var tpl = new Template(this.statementMarkupText);
        return tpl.evaluate({
            choiceKey: choiceValue,
            deleteKey: deleteValue
        });
    },
    /*
    * -----------------
    */
    setField: function (value, parameter, columnId) {
        var extra = 'extra_';
        var ret = false;
        this.fieldsData[this.activeField].field_content.each(function (Field, index) {
            if(index == this.activeDetailField) {
                Field[parameter] = value;
                switch (parameter) {
                    case 'RangeMin':
                        Field.RangeType = $F(extra + 'fieldRangeType');
                        if (!(parseInt(value) >= 0)) {
                            $(extra + 'fieldRangeMin').value = '';
                            Field.RangeMin = 0;
                        }
                        break;
                    case 'RangeMax':
                        Field.RangeType = $F(extra + 'fieldRangeType');
                        if (!(parseInt(value) > 0)) {
                            $(extra + 'fieldRangeMax').value = '';
                            Field.RangeMax = 0;
                        }
                        break;
                    case 'Validation':
                        if (value == 'email') {
                            Field[parameter] = value;
                        }
                        break;
                    case 'DefaultVal':
                        if (Field.Typeof == 'address') {
                            Field.DefaultVal = '';
                            Field.SubFields[5].DefaultVal = value;
                        }
                        break;
                    case 'IsEncrypted':
                        var countOfEncryptedFields = this.instancesOfProperty('IsEncrypted', '1');
                        if (countOfEncryptedFields == 1 && value == '1') Lightbox.showUrl('lightboxes/build-encrypt.php');
                        if (countOfEncryptedFields > 5) {
                            Field.IsEncrypted = '0';
                            $(extra + 'fieldEncrypted').checked = false;
                            alert(this.encryptedFieldsWarning);
                        }
                        break;
                    default:
                        break;
                }
                ret = Field;
            }
        }.bind(this));
        return ret;
    },
    setChoice: function (value, parameter, columnId, position, ctrl) {
        var ret = false;
        var extra = 'extra_';
        this.fieldsData[this.activeField].field_content.each(function (Field, index) {
            if(index == this.activeDetailField) {
                if (Field.Typeof == 'checkbox') {
                    if (Field.SubFields[position]) {
                        Field.SubFields[position]['ChoicesText'] = value;
                    }
                } else if (Field.Typeof == 'likert') {
                    if (ctrl.up('ul').id == (extra + 'likertStatements')) {
                        if (Field.SubFields[position]) {
                            Field.SubFields[position]['ChoicesText'] = value;
                        }
                    } else {
                        if (Field.Choices[position]) {
                            Field.Choices[position][parameter] = value;
                        }
                    }
                } else {
                    if (Field.Choices[position]) {
                        Field.Choices[position][parameter] = value;
                    }
                }
                ret = Field;
            }
        }.bind(this));
        return ret;
    },
    appendToClassNames: function (setting, columnId) {
        this.fieldsData[this.activeField].field_content.each(function (Field, index) {
            if(index == this.activeDetailField) {
                Field.ClassNames = Field.ClassNames + ' ' + setting;
            }
        }.bind(this));
    },
    removeFromClassNames: function (setting, columnId) {
        this.fieldsData[this.activeField].field_content.each(function (Field, index) {
            if(index == this.activeDetailField) {
                Field.ClassNames = Field.ClassNames.gsub(setting, '');
            }
        }.bind(this));
    },
    addSetting: function (setting, columnId) {
        this.fieldsData[this.activeField].field_content.each(function (Field, index) {
            if(index == this.activeDetailField) {
                Field.Settings.push(setting);
            }
        }.bind(this));
    },
    removeSetting: function (setting, columnId) {
        this.fieldsData[this.activeField].field_content.each(function (Field, index) {
            if(index == this.activeDetailField) {
                Field.Settings = Field.Settings.without(setting);
            }
        }.bind(this));
    },
    eraseValidationIfEmail: function () {
        this.fieldsData[this.activeField].field_content.each(function (Field, index) {
            if (Field.Typeof == 'email' && Field.Validation == 'email') {
                Field.Validation = '';
            }
        });
    },
    permissionToAddField: function (type) {
        if (type == 'textarea') {
            if (this.instancesOfField(type) >= 10) {
                lightbox(this.paragraphFieldsWarningTitle, this.paragraphFieldsWarning);
                return false;
            }
        }
        return true;
    },
    instancesOfField: function (type) {
        var tc = 0;
        this.fieldsData[this.activeField].field_content.each(function (Field, index) {
            if (Field.Typeof == type) tc++;
        });
        return tc;
    },
    instancesOfProperty: function (property, value) {
        var tc = 0;
        this.fieldsData[this.activeField].field_content.each(function (Field, index) {
            if (Field[property] == value) tc++;
        });
        return tc;
    },
    /*
    * -----------------
    */
    addField: function(newField) {
        if(!this.fieldsData[this.activeField]) {
           this.fieldsData[this.activeField] = {
                'field_name': 'Tmp Field',
                'field_default': 0,
                'field_content': []
           };
        }
        var field_content = '';
        if(newField.field_json) field_content = newField.field_json;
        else field_content = newField;
        this.fieldsData[this.activeField].field_content.push(field_content);
    },
    removeField: function (FieldId) {
        var i = -1;
        this.fieldsData[this.activeField].field_content.each(function (F, index) {
            if (index == FieldId) {
                i = index;
            }
        });
        if (i >= 0) this.fieldsData[this.activeField].field_content.splice(i, 1);
    },
    addChoice: function (Field, Choice, pos) {
        Field.Choices.splice((pos + 1), 0, Choice);
    },
    removeChoice: function (Field, pos) {
        var choice = Field.Choices[pos];
        var defChoice = false;
        if (choice.IsDefault == 1) {
            defChoice = true;
        }
        Field.Choices.splice(pos, 1);
        if (defChoice && Field.Typeof == 'select') this.setDefaultChoice(Field, 0);
    },
    addSubField: function (Field, SubField, pos) {
        SubField.HasntBeenSaved = true;
        Field.SubFields.splice((pos + 1), 0, SubField);
    },
    removeSubField: function (Field, pos) {
        Field.SubFields.splice(pos, 1);
    },
    setDefaultChoice: function (Field, pos, forceDefault) {
        Field.Choices.each(function (choice, index) {
            if (index == pos && (choice.IsDefault == 0 || forceDefault || Field.Typeof == 'select')) choice.IsDefault = 1;
            else if (index == pos && choice.IsDefault == 1) choice.IsDefault = 0;
            else if (Field.Typeof != 'checkbox') choice.IsDefault = 0;
        });
    },
    setDefaultRightChoice: function (Field, pos, forceDefault) {
        Field.Choices.each(function (choice, index) {
            if (index == pos && choice.IsRight == 0) choice.IsRight = 1;
            else if (index == pos && choice.IsRight == 1) choice.IsRight = 0;
        });
    },
    setDefaultCheckboxChoice: function (Field, pos) {
        Field.SubFields.each(function (subfield, index) {
            if (index == pos && subfield.DefaultVal == 0) subfield.DefaultVal = 1;
            else if (index == pos && subfield.DefaultVal == 1) subfield.DefaultVal = 0;
        });
    },
    setDefaultRightCheckboxChoice: function (Field, pos, forceDefault) {
        Field.SubFields.each(function (subfield, index) {
            if (index == pos && subfield.IsRight == 0) subfield.IsRight = 1;
            else if (index == pos && subfield.IsRight == 1) subfield.IsRight = 0;
        });
    },
    onFieldUpdate: function (Field, parameter) {
        switch (parameter) {
            case 'IsRequired':
                if (Field.Typeof == 'select') this.highlightDefaultIfRequired(Field);
                break;
            case 'IsDefault':
                if (Field.Typeof == 'select') this.highlightDefaultIfRequired(Field);
                break;
        }
    },
    /*
    * -----------------
    */
    selectFieldByColumnId: function (columnId) {
        var ret = this.FormJson.Fields.find(function (Field) {
            return Field.ColumnId == columnId;
        });
        return ret;
    },
    clearEmptyData: function() {
        if(this.fieldsData['0']) {
           this.fieldsData['0'] = {
                'field_name': 'Tmp Field',
                'field_default': 0,
                'field_content': []
           };
        }
    },
    selectField: function(field_id) {
        this.showNoFields();
        this.activeField = field_id;
        $$('#extraFields .choiceMenu ul li').invoke('removeClassName', 'active');
        $('extra_fieldGroup' + this.activeField).addClassName('active');
    },
    selectFieldDetail: function(field_id) {
        this.activeDetailField = field_id;
        this.displayField();
        $$('#listOfFields .choiceMenu ul li').invoke('removeClassName', 'active');
        $('extra_fieldSingle' + this.activeDetailField).addClassName('active');
    },
    selectFieldByColumnId: function(field_id) {
        var field = false;
        this.fieldsData[this.activeField].field_content.each(function(a,b) {
            if(field_id == b) {
                field = a;
                return true;
            }
        });
        return field;
    },
    displayField: function() {
        var field = this.getCurrentSelectField();
        if(!field) {
            this.showNoFields();
            return false;
        }
        this.display(field);
    },
    showNoFields: function() {
        $('extra_fieldProperties').style.display = 'none';
    },
    getCurrentSelectField: function() {
        var activeDetailField  = this.activeDetailField;
        var field = false;
        if(this.fieldsData[this.activeField]) this.fieldsData[this.activeField].field_content.each(function(a,b) {
            if(activeDetailField == b) {
                field = a;
                return true;
            }
        });
        return field;
    },
    
    clearFieldAfterDelete: function(FieldId) {
        $('extra_fieldSingle'+FieldId).remove();
        this.showNoFields();
    },
    getSavedFields: function() {
        var html = '';
        for(var i in this.fieldsData) {
            if(this.fieldsData[i]) {
                var fieldData = this.fieldsData[i];
                var defaultField = '';
                (fieldData.field_default == '1') ? defaultField = 'default' : defaultField = '';
                html += '<li id="extra_fieldGroup'+i+'" class="' + defaultField + '"><a onclick="selectFieldGroup(this); return false; " href="#">'+fieldData.field_name+'</a><a class="delete" onclick="deleteFieldGroup(this); return false;">X</a></li>';
            }
        }
        this.fields = html;
        $('extraFields').down('.choiceMenu ul').insert(this.fields);
    },
    getSavedField: function() {
        var activeDetailField  = this.activeDetailField;
        var field = false;
        var html = '';
        if(this.fieldsData[this.activeField]) {
            $('group_rename_input').value = this.fieldsData[this.activeField].field_name;
            $('group_default_input').checked = this.fieldsData[this.activeField].field_default;
            this.fieldsData[this.activeField].field_content.each(function(a,b) {
                html += '<li id="extra_fieldSingle'+b+'" class="active"><a onclick="selectFieldSingle(this); return false;" href="#">'+(a.Title)+'</a><a class="delete" onclick="deleteFieldSingle(this); return false;">X</a></li>';
            });
        }
        $('listOfFields').down('.choiceMenu ul').innerHTML = html; 
    },
    addExtraDefaultFields: function(ctrl) {
        $(ctrl).addClassName('loadding');
        var fieldsData = this.fieldsData;
        for(var j in fieldsData) {
            if (fieldsData.hasOwnProperty(j)) {
                if(fieldsData[j].field_default == '1') {
                    addFieldToForm2(j);
                }
            }
        }
        $(ctrl).removeClassName('loadding');
    },
    renameGroup: function(field_name) {
        if(this.fieldsData[this.activeField]) {
            this.fieldsData[this.activeField].field_name = field_name;
            $('extra_fieldGroup'+this.activeField).down('a').innerHTML = field_name;
        }
    },
    defaultGroup: function(checkVal) {
        if(this.fieldsData[this.activeField]) {
            this.fieldsData[this.activeField].field_default = checkVal;
            if(checkVal == '1') {
                $('extra_fieldGroup' + this.activeField).addClassName('default');
            } else {
                $('extra_fieldGroup' + this.activeField).removeClassName('default');
            }
            
        }
    },
    saveField: function() {
        if(!this.fieldsData[this.activeField]) return;
        var fields = this.fieldsData[this.activeField].field_content;
        var field_name = this.fieldsData[this.activeField].field_name;
        var field_default = this.fieldsData[this.activeField].field_default;
        var myAjax = new Ajax.Request(new String(document.location), {
            method: 'post',
            parameters: 'action=savefield&fields=' + Object.toJSON(fields) + '&fieldId=' + this.activeField + '&field_name=' + field_name + '&field_default=' + field_default,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                if(!ret.isUpdate) {
                    ret.field_name = field_name;
                    finishSaveField(ret);
                }
                
            }.bind(this)
        });
    },
    updateField: function(field_id) {
        if(this.activeField == 0) {
            this.activeField = field_id;
            var $this = this;
            var field_name = this.fieldsData['0'].field_name;
            this.fieldsData['0'].field_content.each(function(a,b) {
                var field_json = ''
                if(a.field_json) {
                    field_json = a.field_json;
                } else {
                    field_json = a;
                }
                addFieldToFields(field_json);
            });
            this.clearEmptyData();
            this.fieldsData[this.activeField].field_name = field_name;
        }
        this.activeField = field_id;
    },
    addSavedFields: function(field_name) {
        var html = '<li id="extra_fieldGroup'+this.activeField+'" class=""><a onclick="selectFieldGroup(this); return false; " href="#">'+field_name+'</a><a onclick="deleteFieldGroup(this); return false;"" class="delete">X</a></li>';
        $('extraFields').down('.choiceMenu ul').insert(html);
        this.fields += html;
        $('extra_fieldGroup0').down('a').innerHTML = 'New Field Group';
        selectFieldGroup($('extra_fieldGroup' + this.activeField).down('a'));
    },
    addSavedField: function(field) {
        var lastFieldColumn = $('listOfFields').down('.choiceMenu ul').select('li').length;
        var html = '<li class="active" id="extra_fieldSingle'+lastFieldColumn+'"><a href="#" onclick="selectFieldSingle(this); return false;">'+(field.Title)+'</a><a class="delete" onclick="deleteFieldSingle(this); return false;">X</a></li>';
        $('listOfFields').down('.choiceMenu ul').insert(html);
        selectFieldSingle($('extra_fieldSingle' + lastFieldColumn).down('a'));
    },
    deleteFieldGroup: function(field_id) {
        var myAjax = new Ajax.Request(new String(document.location), {
            method: 'post',
            parameters: 'action=deletefield&fieldId=' + field_id,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                if(ret.success) {
                    finishDeleteFieldGroup(field_id);
                }
            }.bind(this)
        });
    },
    deleteFieldSingle: function(field_id) {
        var i = -1;
        this.fieldsData[this.activeField].field_content.each(function (F, index) {
            if (index == field_id) {
                i = index;
            }
        });
        if (i >= 0) {
            this.fieldsData[this.activeField].field_content.splice(i, 1);
            selectFieldGroup($('extra_fieldGroup' + this.activeField).down('a'));
        }
    },
    finishDeleteFieldGroup: function (field_id) {
        var fieldsData = this.fieldsData;
        var activeField = this.activeField;
        for(var j in fieldsData) {
            if (fieldsData.hasOwnProperty(j)) {
                if(field_id == j) {
                    delete fieldsData[j];
                    $('extra_fieldGroup' + field_id).remove();
                    if(activeField == field_id) {
                        $('listOfFields').down('.choiceMenu ul').innerHTML = '';
                    }
                }
            }
        }
    }
});
var FormBuilder = Class.create({
    UI: new Interface(),
    Tabs: new TabInterface(),
    Profile: new AccountProfile(),
    Form: new FormObject(),
    Fields: new FieldsObject(),
    FormEngine: '',
    FormPreview: new FormPreviewObject(),
    Field: new FieldObject(),
    WuCookie: new Cookie(),
    Reorder: '',
    FieldPreview: '',
    FieldProperties: '',
    FieldEngine: '',
    FormProperties: '',
    PageProperties: '',
    DragDrop: '',
    hasntSaved: false,
    runningExtra: false,
    activeField: '', //<sl:translate>
    activeExtraField: '', //<sl:translate>
    choicesRestrictedWarning: 'Choices are restricted to 300 per field.',
    youHaveNotSavedWarning: "YOU HAVEN'T SAVED YET AND\nYOU'LL BE ABANDONING YOUR CHANGES.",
    fieldMaxOptionsWarning: 'This field is allowed a maximum of 95 options.',
    fieldMaxColumnsWarning: 'This field is allowed a maximum of 10 columns.',
    notAllChoicesWereAddedError: 'Not all choices were added since the maximum of 10 choices was exceeded.',
    deletingWillDeleteDataWarning: 'Deleting this choice will also delete all data associated with it. This cannot be undone. Are you sure you want to delete this choice?',
    savingFormDataStatus: 'Saving form data ...',
    unableToDeleteFieldTitle: 'Unable to delete this field!',
    unableToDeleteField: 'You must have at least 1 field in your form.',
    saveFormTxt: 'Save Form',
    savingTxt: 'Saving',
    addingNewFieldTxt: 'Adding a new field ...', //</sl:translate>
    initialize: function () {
        this.FormEngine = new FormEngineObject(this.Profile);
        this.FieldEngine = new FieldEngineObject(this.Form, this.Profile);
        this.FieldProperties = new FieldPropertiesObject(this.Form), this.FormProperties = new FormPropertiesObject();
        this.PageProperties = new PagePropertiesObject();
        this.FieldPreview = new FieldPreviewObject(this.Profile);
        this.DragDrop = new DragDropObject();
        this.Reorder = new ReorderObject(this.Form, this.FieldPreview, this.FieldProperties, this.FormEngine)
        window.onbeforeunload = this.wannaLeave.bind(this);
        this.UI.hideStatus();
        this.updatePositionsByHtml();
    },
    wannaLeave: function () {
        unlockSession()
        if (this.hasntSaved) {
            return this.youHaveNotSavedWarning;
        }
    },
    selectForm: function () {
        this.UI.unhighlightField(this.activeField);
        this.UI.unhighlightPageSettings();
        this.UI.highlightForm();
        this.FormProperties.populate(this.Form, this.Profile);
        this.Tabs.showSidebar('formProperties');
        this.Tabs.thirdTab();
        this.activeField = -1;
        this.activeExtraField = -1;
        this.UI.adjustButtons();
    },
    selectPageSettings: function () {
        this.UI.unhighlightField(this.activeField);
        this.UI.unhighlightForm();
        this.UI.highlightPageSettings();
        this.PageProperties.populate(this.Form, this.Profile);
        this.Tabs.showSidebar('formProperties');
        this.Tabs.thirdTab();
        this.activeField = -1;
        this.activeExtraField = -1;
        this.UI.adjustButtons();
    },
    updateForm: function (value, parameter) {
        this.Form.set(value, parameter);
        this.FormPreview.modify(value, parameter, this.Form);
        if (parameter == 'BreadCrumbType') this.updatePageHeader();
        this.hasntSaved = true;
    },
    saveForm: function () {
        $('statusText').innerHTML = this.savingFormDataStatus;
        $('saveForm').innerHTML = '<img src="template/images/icons/arrow_refresh.png" /> ' + this.savingTxt;
        this.Form.set(this.Form.calculateFormHeight(), 'OffsetHeight');
        this.FormEngine.save(this.Form);
    },
    resetSaveFormAppearance: function () {
        $('saveForm').innerHTML = '<img src="template/images/icons/tick.png" /> ' + this.saveFormTxt;
    },
    processSaveSuccess: function (ret) {
        this.hasntSaved = false;
        if (ret.success == true) Lightbox.showUrl('lightboxes/FormBuilder.Wdywtdn.php?url=' + ret.response.url + '&id=' + ret.response.id, false, 'modal');
        else if (ret.success == false) {
            if(ret.message) alert(ret.message);
            else alert('some error occured!');
        }
    },
    processSaveDemoSuccess: function (ret) {
        this.hasntSaved = false;
        $('formData').value = Object.toJSON(this.Form.FormJson);
        $('demoSave').submit();
    },
    processSaveError: function (ret) {
        this.resetSaveFormAppearance();
        Lightbox.showContent(ret.response.lbmarkup);
    },
    showFormActivity: function () {
        this.UI.showFormActivity();
    },
    hideFormActivity: function () {
        this.UI.hideFormActivity();
        this.Form.set('2000-01-01 12:00:00', 'StartDate');
        this.Form.set('2030-01-01 12:00:00', 'EndDate');
    },
    showFormPassword: function () {
        this.UI.showFormPassword();
    },
    hideFormPassword: function () {
        this.UI.hideFormPassword();
        this.Form.set('', 'Password');
        $('formPasswordVal').value = '';
    },
    toggleFormRedirect: function (setting) {
        if (setting == 'url') this.showFormRedirectUrl();
        else this.showFormConfirmationMessage();
    },
    showFormRedirectUrl: function () {
        if (this.Profile.permissionToRedirect()) {
            this.Form.set($F('formRedirectVal'), 'Redirect');
            this.UI.showFormRedirectUrl();
        } else {
            $('formMessage').checked = true;
        }
    },
    showFormConfirmationMessage: function () {
        this.Form.set('', 'Redirect');
        this.UI.showFormConfirmationMessage();
    },
    addCaptchaToPreview: function (useCaptcha) {
        if (useCaptcha == 2) $('forceCaptcha').removeClassName('hide');
        else $('forceCaptcha').addClassName('hide');
    },
    showConfirmationEmailToUser: function () {
        this.UI.showConfirmationEmailToUser();
    },
    hideConfirmationEmailToUser: function () {
        this.Form.eraseValidationIfEmail();
        $('formSendTo').selectedIndex = 0;
        this.UI.hideConfirmationEmailToUser();
    },
    displayReceiptLightbox: function () {
        this.FormProperties.displayReceiptLightbox();
    },
    finishLoadingReceiptLightbox: function () {
        if (this.Form.FormJson.FormId > 0) {
            $('receiptApi').href = 'https://' + document.domain + '/api/code/' + this.Form.FormJson.FormId + '/';
        }
        this.FormProperties.populateReceiptLightbox(this.Form);
        this.UI.hideStatus();
    },
    updatePageHeader: function () {
        this.FormEngine.fetchPageHeader(this.Form);
    },
    finishPageHeaders: function (ret) {
        this.FormPreview.replaceHeader(ret.response.markup);
    },
    jumpToPage: function (direction) {
        if (direction == 'next') this.jumpToNextPage();
        else this.jumpToPreviousPage();
    },
    jumpToNextPage: function () {
        var pageId = $('jumptarget').value;
        if (pageId >= 1) {
            pageId = parseInt(pageId) + 1;
            var pageObj = $('formFields').down('li.page', (pageId - 2));
            if (pageObj) {
                var target = pageObj.next();
                Effect.ScrollTo(target, {
                    'offset': -35,
                    'duration': 0.3
                });
            }
        }
        this.FieldPreview.adjustPageJump(pageId, this.Form);
    },
    jumpToPreviousPage: function () {
        var pageId = $('jumptarget').value;
        if (pageId >= 2) {
            pageId = parseInt(pageId) - 1;
            var pageObj = $('formFields').down('li.page', (pageId - 2));
            if (pageObj) {
                var target = pageObj.next();
                Effect.ScrollTo(target, {
                    'offset': -35,
                    'duration': 0.3
                });
            }
        }
        if (pageId == 1) {
            target = $('formFields').next();
            Effect.ScrollTo(target, {
                'duration': 0.3
            });
        }
        this.FieldPreview.adjustPageJump(pageId, this.Form);
    },
    showFields: function (pulse) {
        this.UI.unhighlightField(this.activeField);
        this.UI.unhighlightForm();
        this.UI.unhighlightPageSettings();
        this.Tabs.showFields(pulse);
        this.UI.adjustButtons();
        this.activeField = -1;
        this.activeExtraField = -1;
    },
    selectField: function (ctrl) {
        
        var columnId = -1;
        if (ctrl.id) columnId = ctrl.id.replace('foli', '');
        else {
            if (this.Form.FormJson.Fields[0]) columnId = this.Form.FormJson.Fields[0].ColumnId;
        }
        if (columnId == this.activeField) {
            this.runningExtra = false;
            return;
        }
        if (columnId != this.activeField || columnId == -1) {
            this.UI.unhighlightField(this.activeField);
            this.UI.unhighlightExtraField(this.activeField);
            if (columnId >= 0) this.activeField = columnId;
            this.Tabs.showFieldProperties(columnId);
            this.UI.highlightField(columnId);
            if (columnId >= 0) this.FieldProperties.display(this.Form.selectFieldByColumnId(columnId));
            this.UI.snapFieldProperties(ctrl);
            this.UI.adjustButtons();
        }
        this.activeExtraField = -1;
        console.log('selectField');
    },
    addField: function (type, insertAfterElement, dragDropBox) {
        if (this.Profile.permissionToAddField(type, this.Form.FormJson.Fields.length, false) && this.Form.permissionToAddField(type)) {
            $('statusText').innerHTML = this.addingNewFieldTxt;
            this.FieldEngine.addField(type, insertAfterElement, dragDropBox, this.Form.FormJson.Language);
            this.UI.hideNoFields();
            this.UI.showSaveButton();
            this.UI.adjustButtons();
            this.hasntSaved = true;
        }
    },
    /*
    * ExtraField: Add
    */
    autoCountField: function() {
        console.log(this.Form.FormJson.Fields.length);
        $('formUserSubmitLimit').value = this.Form.FormJson.Fields.length;
        $('formUserSubmitLimit').up().style.backgroundColor = '#1BFF55';
        updateForm(this.Form.FormJson.Fields.length, 'UserSubmitLimit');
        setTimeout(function() {
            $('formUserSubmitLimit').up().style.backgroundColor = '';
        },1500)
    },
    addFieldToFields: function(newField) {
        this.Fields.addField(newField);
    },
    saveField: function() {
        this.Fields.saveField(); 
    },
    extra_selectField: function(ctrl) {
        this.Fields.selectField(ctrl);  
    },
    showExtraFields: function (pulse) {
        this.UI.unhighlightField(this.activeField);
        this.UI.unhighlightForm();
        this.UI.unhighlightPageSettings();
        this.Tabs.showFields(pulse);
        this.UI.adjustButtons();
        this.activeField = -1;
        this.activeExtraField = -1;
    },
    selectFieldGroup: function(field_id) {
        this.Fields.selectField(field_id);
        this.Fields.getSavedField();
    },
    selectFieldSingle: function(field_id) {
        this.Fields.selectFieldDetail(field_id);
    },
    deleteFieldGroup: function(field_id) {
        this.Fields.deleteFieldGroup(field_id);
    },
    deleteFieldSingle: function(field_id) {
        this.Fields.deleteFieldSingle(field_id);
    },
    finishDeleteFieldGroup: function(field_id) {
        this.Fields.finishDeleteFieldGroup(field_id);
    },
    deletetFieldSingle: function(field_id) {
        this.Fields.deletetFieldSingle(field_id);
    },
    selectExtraField: function (ctrl) {
        console.log('selectExtraField');
        this.runningExtra = true;
        var columnId = -1;
        if (ctrl.id) columnId = ctrl.id.replace('extra_foli', '');
        else {
            if (this.Form.FormJson.Fields[0]) columnId = this.Form.FormJson.Fields[0].ColumnId;
        }
        this.UI.unhighlightExtraField(this.activeExtraField);
        this.activeExtraField = columnId;
        this.Tabs.showExtraFieldProperties(this.activeExtraField);
        this.UI.highlightExtraField(this.activeExtraField);
        if (this.activeExtraField >= 0) this.FieldProperties.displayExtra(this.Form.selectExtraFieldByColumnId(this.activeField, this.activeExtraField));
        this.UI.snapExtraFieldProperties(ctrl);
        this.UI.adjustButtons();
    },
    selectExtraFieldPreview: function (ctrl) {
        console.log('selectExtraField');
        this.runningExtra = true;
        var columnId = -1;
        if (ctrl.id) columnId = ctrl.id.replace('extra_foli', '');
        else {
            if (this.Form.FormJson.Fields[0]) columnId = this.Form.FormJson.Fields[0].ColumnId;
        }
        this.activeExtraField = columnId;
        this.UI.unhighlightExtraField(this.activeField, this.activeExtraField);
        this.Tabs.showExtraFieldProperties(this.activeField, this.activeExtraField);
        this.UI.highlightExtraField(this.activeField, this.activeExtraField);
        if (this.activeExtraField >= 0) this.FieldProperties.displayExtra(this.Form.selectExtraFieldByColumnId(this.activeField, this.activeExtraField));
        this.UI.snapExtraFieldProperties(ctrl);
        this.UI.adjustButtons();
    },
    addExtraField: function (type, insertAfterElement, dragDropBox) {
        if (this.Profile.permissionToAddField(type, this.Form.FormJson.Fields.length, false) && this.Form.permissionToAddField(type)) {
            $('statusText').innerHTML = this.addingNewFieldTxt;
            var field = this.Form.selectFieldByColumnId(this.activeField);
            this.FieldEngine.addExtraField(type, insertAfterElement, dragDropBox, this.Form.FormJson.Language, field);
            this.UI.hideNoFields();
            this.UI.showSaveButton();
            this.UI.adjustButtons();
            this.hasntSaved = true;
        }
    },
    addExtraFieldPreview: function (type, insertAfterElement, dragDropBox) {
        if (this.Profile.permissionToAddField(type, this.Form.FormJson.Fields.length, false) && this.Form.permissionToAddField(type)) {
            $('statusText').innerHTML = this.addingNewFieldTxt;
            var field = this.Form.selectFieldByColumnId(this.activeField);
            this.FieldEngine.addExtraField(type, insertAfterElement, dragDropBox, this.Form.FormJson.Language, field);
            this.UI.hideNoFields();
            this.UI.showSaveButton();
            this.UI.adjustButtons();
            this.hasntSaved = true;
        }
    },
    addFieldToForm2: function (group_id) {
        var field = this.Form.selectFieldByColumnId(this.activeField);
        var activeField = group_id;
        if(this.Fields.fieldsData[activeField]) {
            var FieldEngine = this.FieldEngine;
            this.Fields.fieldsData[activeField].field_content.each(function(k, v) {
                var extra_field = k;
                if(k.field_json) extra_field = k.field_json;
                FieldEngine.duplicateExtraField(extra_field, field, '');
            });
        }
    },
    addFieldToForm: function (ctrl) {
        var field = this.Form.selectFieldByColumnId(this.activeField);
        var activeField = this.Fields.activeField;
        if(this.Fields.fieldsData[activeField]) {
            var FieldEngine = this.FieldEngine;
            this.Fields.fieldsData[activeField].field_content.each(function(k, v) {
                var extra_field = k;
                if(k.field_json) extra_field = k.field_json;
                FieldEngine.duplicateExtraField(extra_field, field, '');
            });
        }
        this.Fields.clearEmptyData();
    },
    addGroupToForm: function (ctrl) {
        var field = this.Form.selectFieldByColumnId(this.activeField);
        var activeField = this.Fields.activeField;
        if(this.Fields.fieldsData[activeField]) {
            var FieldEngine = this.FieldEngine;
            this.Fields.fieldsData[activeField].field_content.each(function(k, v) {
                var extra_field = k;
                if(k.field_json) extra_field = k.field_json;
                FieldEngine.duplicateField(extra_field, '');
            });
        }
        this.Fields.clearEmptyData();
    },
    processAddedExtraSuccess: function (ret) {
        var newField = {
            'field_id': ret.response.id,
            'field_json': ret.response.json.evalJSON(),
            'field_html': ret.response.html
        }
        var field = this.Form.selectFieldByColumnId(this.activeField);
        this.FormEngine.addExtraField(this.Form, field, newField.field_json, 1);
        this.FieldPreview.addExtraField(field, newField.field_html, '');
        Event.observe($('extra_foli' + newField.field_id), 'click', selectExtraField.bind($('extra_foli' + newField.field_id)), false);
        field = this.Form.selectFieldByColumnId(this.activeField);
        this.updateExtraPositionsByHtml(field);
    },
    processAddExtraSuccess: function (ret, insertAfterElement, nodrag, dragDropBox) {
        var newField = {
            'field_id': ret.response.id,
            'field_json': ret.response.json.evalJSON(),
            'field_html': ret.response.html
        }
        this.Fields.addField(newField);
        this.Fields.addSavedField(newField.field_json);
    },
    processAddExtraError: function (ret) {
        Lightbox.showContent(ret.response.lbmarkup);
    },
    processSaveFieldSuccess: function (ret) {
        this.Fields.updateField(ret.response.field_id);
        this.Fields.addSavedFields(ret.field_name);
    },
    processSaveFieldError: function (ret) {
        Lightbox.showContent(ret.response.lbmarkup);
    },
    /*
    * ExtraField: End Add
    */
    processAddSuccess: function (ret, insertAfterElement, nodrag, dragDropBox) {
        var insertType = insertAfterElement;
        if (dragDropBox) insertType = 'dragdropbox';
        this.FieldPreview.addField(ret.response.html, insertType);
        if (dragDropBox) this.DragDrop.returnButtonToAddFieldMenu(dragDropBox);
        this.FormEngine.addField(this.Form, ret.response.json.evalJSON(), this.findInsertPosition(insertAfterElement));
        this.updatePositionsByHtml();
        if (!nodrag) this.DragDrop.grantFieldDragAbility();
        Event.observe($('foli' + ret.response.id), 'click', selectField.bind($('foli' + ret.response.id)), false);
    },
    findInsertPosition: function (insertAfterElement) {
        return this.Reorder.findInsertPositionByHtml(insertAfterElement);
    },
    finishAddingPage: function (ret, insertAfterElement, dragDropBox) {
        var nodrag = false;
        var pages = this.Form.instancesOfField('page');
        if (pages == 1) nodrag = true;
        if (pages >= 1) {
            this.FormEngine.incrementPageCount(this.Form);
        }
        this.processAddSuccess(ret, insertAfterElement, nodrag, dragDropBox);
        if (pages == 0) {
            this.addField('page');
            this.FieldPreview.showPageHeader();
        }
        if (pages == 1) {
            this.FieldPreview.showPageJump(this.Form);
            this.FieldPreview.removeDragable(ret.response.id);
        }
    },
    processAddError: function (ret) {
        Lightbox.showContent(ret.response.lbmarkup);
    },
    addFieldAfterDraggingComplete: function () {
        var insertAfter = 'top';
        var dragDropBox = $('formFields').select('.dragfld')[0];
        var fieldType = dragDropBox.id.replace('drag1_', '').replace('drag2_', '').replace('drag3_', '').replace('drag4_', '');
        if (dragDropBox.previous()) insertAfter = dragDropBox.previous().id;
        this.addField(fieldType, insertAfter, dragDropBox);
    },
    duplicateField: function (columnId, e) {
        var fieldToDuplicate = this.Form.selectFieldByColumnId(columnId);
        if (this.Profile.permissionToAddField(fieldToDuplicate.Typeof, this.Form.FormJson.Fields.length) && this.Form.permissionToAddField(fieldToDuplicate.Typeof)) {
            $('statusText').innerHTML = this.addingNewFieldTxt;
            this.FieldEngine.duplicateField(fieldToDuplicate, this.Form.FormJson.Language);
        }
    },
    deleteField: function (columnId, e) {
        if(this.activeExtraField > 0) {
            if (!e) var e = window.event;
            e.cancelBubble = true;
            if (e.stopPropagation) e.stopPropagation();
            var fieldToDelete = this.Form.selectExtraFieldByColumnId(this.activeField, columnId);
            this.finishDeletingField({
                'ColumnId': columnId
            }, e);
            return;
        }
        if (!e) var e = window.event;
        e.cancelBubble = true;
        if (e.stopPropagation) e.stopPropagation();
        if (this.Form.FormJson.Fields.length > 1 || this.Form.FormJson.FormId < 1) {
            var fieldToDelete = this.Form.selectFieldByColumnId(columnId);
            if (fieldToDelete.HasntBeenSaved || fieldToDelete.Typeof == 'page') {
                this.finishDeletingField({
                    'ColumnId': columnId
                }, e);
            } else {
                Lightbox.showUrl('lightboxes/Field.Delete.php', function () {
                    $('deleteFieldId').value = fieldToDelete.ColumnId;
                }, 'error');
            }
        } else {
            lightbox(this.unableToDeleteFieldTitle, this.unableToDeleteField);
        }
    },
    continueDeletingField: function () {
        var fieldToDelete = this.Form.selectFieldByColumnId($F('deleteFieldId'));
        Lightbox.close();
        this.FieldEngine.deleteField(fieldToDelete);
    },
    finishDeletingField: function (ret, e) {
        var extra = '';
        if(this.activeExtraField > 0) {
            extra = 'extra_';
            var field = this.Form.selectExtraFieldByColumnId(this.activeField, ret.ColumnId);
            this.FieldPreview.removeField(field, extra);
            this.FormEngine.removeExtraField(this.Form, field, this.activeField);
            this.handlePageDeletion(field, e);
            this.Tabs.showFieldProperties(-1);
            this.activeExtraField = -1;
            return;
        }
        var field = this.Form.selectFieldByColumnId(ret.ColumnId);
        this.FieldPreview.removeField(field);
        this.FormEngine.removeField(this.Form, field);
        this.handlePageDeletion(field, e);
        this.Tabs.showFieldProperties(-1);
        this.activeField = -1;
        if (this.Form.FormJson.Fields.length <= 0) {
            this.UI.showNoFields();
            this.UI.hideSaveButton();
        }
        this.UI.adjustButtons();
    },
    handlePageDeletion: function (field, e) {
        var instances = this.Form.instancesOfField('page');
        if (field.Typeof == 'page' && instances >= 1) {
            this.FormEngine.decrementPageCount(this.Form);
            this.checkAndDeleteLastPage(instances, e);
        }
    },
    checkAndDeleteLastPage: function (instances, e) {
        if (instances == 1) {
            var id = $('formFields').lastChild.id;
            id = id.replace('foli', '');
            this.deleteField(id, e);
            this.FieldPreview.hidePageHeader();
            this.FieldPreview.hidePageJump();
        }
    },
    processDeleteError: function (ret) {
        Lightbox.showContent(ret.response.lbmarkup);
    },
    updatePositionsByHtml: function () {
        var refresh = this.Reorder.updatePositionsByHtml(this.activeField);
        if (refresh) this.updatePageHeader();
    },
    /*
    * ExtraFields: Position
    */
    updateExtraPositionsByHtml: function (field) {
        this.Reorder.updateExtraPositionsByHtml(field);
    },
    /*
    * ExtraFields: End Position
    */
    updateField: function (value, parameter, columnId) {
        if(this.activeExtraField > 0) {
            var Field = this.Form.setExtraField(value, parameter, this.activeField, this.activeExtraField);
            if (!this.updatePageBreak(value, parameter, Field)) {
                this.FieldPreview.modify(value, parameter, Field, 'extra_');
                this.FieldProperties.onFieldUpdate(Field, parameter);
            }
            this.hasntSaved = true;
            return;
        }
        if (!columnId) columnId = this.activeField;
        var Field = this.Form.setField(value, parameter, columnId);
        if (!this.updatePageBreak(value, parameter, Field)) {
            this.FieldPreview.modify(value, parameter, Field);
            this.FieldProperties.onFieldUpdate(Field, parameter);
        }
        this.hasntSaved = true;
    },
    updatePageBreak: function (value, parameter, Field) {
        var ret = false;
        if (Field.Typeof == 'page') {
            if (parameter == 'Title') {
                this.FormPreview.modify(value, 'PageHeader', this.Form, Field);
                ret = true;
            }
        }
        return ret;
    },
    removeEncryption: function () {
        this.updateField('0', 'IsEncrypted');
        $('fieldEncrypted').checked = false;
        Lightbox.close();
    },
    defaultDate: function (value) {
        var field = this.Form.selectFieldByColumnId(this.activeField);
        this.FieldPreview.defaultDate(value, field);
    },
    defaultPrice: function (value) {
        var field = this.Form.selectFieldByColumnId(this.activeField);
        this.FieldPreview.defaultPrice(value, field);
    },
    updateFieldDisplay: function () {
        var fieldToChange = this.Form.selectFieldByColumnId(this.activeField);
        this.FieldEngine.displayField(fieldToChange);
    },
    finishDisplayingField: function (ret) {
        var insertAfter = $('foli' + ret.response.id).previous();
        if (!insertAfter) insertAfter = 'top';
        else insertAfter = insertAfter.id;
        $('foli' + ret.response.id).remove();
        this.FieldPreview.addField(ret.response.html, insertAfter);
        this.DragDrop.grantFieldDragAbility();
        Event.observe($('foli' + ret.response.id), 'click', selectField.bind($('foli' + ret.response.id)), false);
        this.UI.highlightField(ret.response.id);
    },
    changeFieldType: function (newType, sameDbColumn) {
        if (this.Profile.permissionToAddField(newType, this.Form.FormJson.Fields.length, true) && this.Form.permissionToAddField(newType)) {
            var fieldToChange = this.Form.selectFieldByColumnId(this.activeField);
            this.FieldEngine.changeField(fieldToChange, newType, sameDbColumn, this.Form.FormJson.Language);
            this.hasntSaved = true;
        }
    },
    finishChangingField: function (ret, originalField, sameDbColumn) {
        var originalField = this.Form.selectFieldByColumnId(originalField);
        this.FormEngine.removeField(this.Form, originalField);
        this.processAddSuccess(ret, 'foli' + originalField.ColumnId);
        this.FieldPreview.removeFieldWithoutAnimation(originalField);
        this.updatePositionsByHtml();
        this.activeField = -1;
        if (sameDbColumn && !originalField.HasntBeenSaved) {
            var newField = this.Form.selectFieldByColumnId(ret.response.id);
            newField.HasntBeenSaved = false;
        }
        this.selectField($('foli' + ret.response.id));
        Event.observe($('foli' + ret.response.id), 'click', selectField.bind($('foli' + ret.response.id)), false);
    },
    toggleChoicesLayout: function (setting) {
        var settings = new Array('notStacked', 'twoColumns', 'threeColumns');
        for (var i = 0; i < settings.length; i++) {
            this.Form.removeSetting(settings[i], this.activeField);
            $('foli' + this.activeField).removeClassName(settings[i]);
        }
        this.Form.addSetting(setting, this.activeField);
        $('foli' + this.activeField).addClassName(setting);
    },
    showButtonType: function (category, newtype) {
        var oldtype = 'asTxt';
        if (newtype == 'asTxt') oldtype = 'asImg';
        $('list' + category + 'Buttons').removeClassName(oldtype);
        $('list' + category + 'Buttons').addClassName(newtype);
        if (newtype == 'asTxt' && category == 'Next') updateProperties($F('nextButtonsText'), 'ChoicesText');
        if (newtype == 'asImg' && category == 'Next') updateProperties($F('nextButtonImg'), 'ChoicesText');
        this.FieldPreview.togglePagingButton(this.activeField, category, newtype);
    },
    updateChoice: function (ctrl) {
        if(this.activeExtraField > 0) {
            var position = $(ctrl).up().previousSiblings().length;
            var value = $F(ctrl);
            var fieldBeingUpdated = this.Form.setExtraChoice(value, 'Choice', this.activeField, this.activeExtraField, position, ctrl);
            this.FieldPreview.modifyExtraChoice(fieldBeingUpdated, value, position, ctrl);
            this.hasntSaved = true;
            return;
        }
        var position = $(ctrl).up().previousSiblings().length;
        var value = $F(ctrl);
        var fieldBeingUpdated = this.Form.setChoice(value, 'Choice', this.activeField, position, ctrl);
        this.FieldPreview.modifyChoice(fieldBeingUpdated, value, position, ctrl);
        this.hasntSaved = true;
    },
    addChoice: function (ctrl, value, noalert) {
        var extra = '';
        if(this.activeExtraField > 0) {
            extra = 'extra_';
            var field = this.Form.selectExtraFieldByColumnId(this.activeField, this.activeExtraField);
        } else {
            var field = this.Form.selectFieldByColumnId(this.activeField);
        }
        var position = $(ctrl).up().previousSiblings().length;
        if (!value) value = '';
        if (field.Choices.length < 300) {
            this.FieldProperties.insertChoice(ctrl.up(), value, field.Typeof);
            if (field.Typeof == 'checkbox') this.FormEngine.addSubField(field, {
                "Title": "",
                "ChoicesText": value,
                "DefaultVal": "0",
                "IsRight": "1"
            }, position, extra);
            else this.FormEngine.addChoice(field, {
                "Choice": value,
                "IsDefault": "0",
                "IsRight": "1"
            }, position, extra);
            this.FieldPreview.addChoice(field, position, value, extra);
        } else if (!noalert) alert(this.choicesRestrictedWarning);
    },
    addSubField: function (ctrl, value) {
        var extra = '';
        if(this.activeExtraField > 0) {
            extra = 'extra_';
            var field = this.Form.selectExtraFieldByColumnId(this.activeField, this.activeExtraField);
        } else {
            var field = this.Form.selectFieldByColumnId(this.activeField);
        }
        var position = $(ctrl).up().previousSiblings().length;
        if (!value) value = '';
        var ftype = field.Typeof;
        if (field.Typeof == 'likert') ftype = 'statement';
        if (field.SubFields.length < 95) {
            this.FieldProperties.insertChoice(ctrl.up(), value, ftype);
            this.FormEngine.addSubField(field, {
                "Title": "",
                "ChoicesText": value,
                "DefaultVal": "0",
                "IsRight": "1"
            }, position, extra);
            this.FieldPreview.addChoice(field, position, value, extra);
        } else {
            alert(this.fieldMaxOptionsWarning);
        }
    },
    deleteSubField: function (ctrl, value) {
        var extra = '';
        if(this.activeExtraField > 0) {
            extra = 'extra_';
            var field = this.Form.selectExtraFieldByColumnId(this.activeField, this.activeExtraField);
        } else {
            var field = this.Form.selectFieldByColumnId(this.activeField);
        }
        var position = $(ctrl).up().previousSiblings().length;
        if (position == 0 && field.SubFields && field.SubFields.length == 1) {
            Element.previous(ctrl, 'input').value = '';
            this.updateChoice(Element.previous(ctrl, 'input'));
        } else {
            this.FieldProperties.deleteChoice(ctrl.parentNode);
            this.FieldPreview.deleteChoice(field, position, extra);
            this.FormEngine.removeSubField(field, position);
            if (field.Typeof == 'likert') this.FieldPreview.highlightLikertStatements(field);
        }
    },
    addLikertChoice: function (ctrl, value) {
        var extra = '';
        if(this.activeExtraField > 0) {
            extra = 'extra_';
            var field = this.Form.selectExtraFieldByColumnId(this.activeField, this.activeExtraField);
        } else {
            var field = this.Form.selectFieldByColumnId(this.activeField);
        }
        if($('fieldDC').checked == true) {
            $('fieldDC').checked = false;
        }
        var position = $(ctrl).up().previousSiblings().length;
        if (!value) value = '';
        if (field.Choices.length < 10 || (field.Choices.length < 11 && field.Validation == 'na')) {
            this.FieldProperties.insertChoice(ctrl.up(), value, 'column');
            this.FormEngine.addChoice(field, {
                "Choice": value,
                "IsDefault": "0",
                "IsRight": "1"
            }, position, extra);
            this.FieldEngine.displayField(field);
        } else {
            alert(this.fieldMaxColumnsWarning);
        }
    },
    deleteLikertChoice: function (ctrl, value) {
        var extra = '';
        if(this.activeExtraField > 0) {
            extra = 'extra_';
            var field = this.Form.selectExtraFieldByColumnId(this.activeField, this.activeExtraField);
        } else {
            var field = this.Form.selectFieldByColumnId(this.activeField);
        }
        if($('fieldDC').checked == true) {
            $('fieldDC').checked = false;
        }
        var position = $(ctrl).up().previousSiblings().length;
        if (position == 0 && field.Choices && field.Choices.length == 1) {
            Element.previous(ctrl, 'input').value = '';
            this.updateChoice(Element.previous(ctrl, 'input'));
        } else {
            
            var toggleNa = false;
            if (!$(ctrl).up().next()) toggleNa = true;
            this.FieldProperties.deleteChoice(ctrl.parentNode);
            this.FormEngine.removeChoice(field, position, extra);
            this.FieldEngine.displayField(field);
            
            if (toggleNa && field.Validation == 'na') {
                $('fieldNA').checked = false;
                this.updateField(field.ColumnId, 'Validation');
            }
        }
    },
    addLikertChoice2: function (ctrl, value) {
        var extra = '';
        if(this.activeExtraField > 0) {
            extra = 'extra_';
            var field = this.Form.selectExtraFieldByColumnId(this.activeField, this.activeExtraField);
        } else {
            var field = this.Form.selectFieldByColumnId(this.activeField);
        }
        var position = $(ctrl).up().previousSiblings().length;
        if (!value) value = '';
        if (field.Choices.length < 10 || (field.Choices.length < 11 && field.Validation == 'dc')) {
            //this.FieldProperties.insertChoice(ctrl.up(), value, 'column');
            if($('fieldNA').checked == true) {
                $('fieldNA').checked = false;
                this.FieldProperties.deleteChoice(ctrl.parentNode);
                this.FormEngine.removeChoice(field, position);  
            }
            this.FieldEngine.displayField(field, extra);
        } else {
            alert(this.fieldMaxColumnsWarning);
        }
    },
    deleteLikertChoice2: function (ctrl, value) {
        var extra = '';
        if(this.activeExtraField > 0) {
            extra = 'extra_';
            var field = this.Form.selectExtraFieldByColumnId(this.activeField, this.activeExtraField);
        } else {
            var field = this.Form.selectFieldByColumnId(this.activeField);
        }
        var position = $(ctrl).up().previousSiblings().length;
        if($('fieldNA').checked == true) {
            $('fieldNA').checked = false;
            this.FieldProperties.deleteChoice(ctrl.parentNode);
            this.FormEngine.removeChoice(field, position);  
        }
        if (position == 0 && field.Choices && field.Choices.length == 1) {
            Element.previous(ctrl, 'input').value = '';
            this.updateChoice(Element.previous(ctrl, 'input'));
        } else {
            var toggleNa = false;
            if (!$(ctrl).up().next()) toggleNa = true;
            this.FieldEngine.displayField(field);
            if (toggleNa && field.Validation == 'dc') {
                this.updateField(field.ColumnId, 'Validation');
            }
        }
    },
    deleteChoice: function (ctrl) {
        var extra = '';
        if(this.activeExtraField > 0) {
            extra = 'extra_';
            var field = this.Form.selectExtraFieldByColumnId(this.activeField, this.activeExtraField);
        } else {
            var field = this.Form.selectFieldByColumnId(this.activeField);
        }
        var position = $(ctrl).up().previousSiblings().length;
        if (position == 0 && ((field.Choices && field.Choices.length == 1) || (field.SubFields && field.SubFields.length == 1))) {
            Element.previous(ctrl, 'input').value = '';
            this.updateChoice(Element.previous(ctrl, 'input'));
        } else {
            var toggleOther = false;
            if (!$(ctrl).up().next()) toggleOther = true;
            this.FieldProperties.deleteChoice(ctrl.parentNode);
            this.FieldPreview.deleteChoice(field, position, extra);
            if (field.Typeof == 'checkbox') this.FormEngine.removeSubField(field, position);
            else this.FormEngine.removeChoice(field, position);
            if (field.Typeof == 'radio' && field.Settings.indexOf('other') != -1 && toggleOther) {
                this.Form.removeSetting('other', this.activeField);
                $('fieldOther').checked = false;
                this.updateFieldDisplay();
            }
        }
    },
    changeChoiceDefault: function (ctrl) {
        var extra = '';
        if(this.activeExtraField > 0) {
            extra = 'extra_';
            var field = this.Form.selectExtraFieldByColumnId(this.activeField, this.activeExtraField);
        } else {
            var field = this.Form.selectFieldByColumnId(this.activeField);
        }
        var position = $(ctrl).up().previousSiblings().length;
        if (field.Typeof == 'checkbox') this.FormEngine.setDefaultCheckboxChoice(field, position);
        else this.FormEngine.setDefaultChoice(field, position);
        this.FieldPreview.choiceDefault(field, position, ctrl, extra);
        this.FieldProperties.onFieldUpdate(field, 'IsDefault');
    },
    changeRightDefault: function (ctrl) {
        var extra = '';
        if(this.activeExtraField > 0) {
            extra = 'extra_';
            var field = this.Form.selectExtraFieldByColumnId(this.activeField, this.activeExtraField);
        } else {
            var field = this.Form.selectFieldByColumnId(this.activeField);
        }
        var position = $(ctrl).up().previousSiblings().length;
        if (field.Typeof == 'checkbox') this.FormEngine.setDefaultRightCheckboxChoice(field, position);
        else this.FormEngine.setDefaultRightChoice(field, position);
        //this.FieldPreview.choiceDefault(field, position, ctrl);
        this.FieldProperties.onFieldUpdate(field, 'IsRight');
    },
    toggleRadioOther: function (checked) {
        //var extra = '';
//        if(this.activeExtraField > 0) {
//            extra = 'extra_';
//            var field = this.Form.selectExtraFieldByColumnId(this.activeField, this.activeExtraField);
//        } else {
//            var field = this.Form.selectFieldByColumnId(this.activeField);
//        }
        var list = $('fieldChoices');
        var clickedEl = list.down('li', list.childElements().length - 1).down('img');
        if (checked) {
            this.Form.addSetting('other', this.activeField);
            this.addChoice(clickedEl, 'Other');
            this.FieldProperties.highlightRadioOther(list.down('li', list.childElements().length - 1));
            list.down('li', list.childElements().length - 1).down('img').remove();
            this.updateFieldDisplay();
        } else {
            this.Form.removeSetting('other', this.activeField);
            this.deleteChoice(clickedEl);
            this.updateFieldDisplay();
        }
    },
    processBulkAddition: function (choices) {
        var field = this.Form.selectFieldByColumnId(this.activeField);
        if (!this.currentlyAddingChoices) {
            this.currentlyAddingChoices = true;
            choices.each(function (choice) {
                ctrl = $('fieldChoices').down('li', $('fieldChoices').childElements().length - 1);
                choiceTextField = ctrl.down('img');
                if (choiceTextField.parentNode.hasClassName('dropReq') && field.Typeof != 'select') {
                    ctrl = $('fieldChoices').down('li', $('fieldChoices').childElements().length - 1);
                    choiceTextField = ctrl.previous().down('img');
                }
                this.addChoice(choiceTextField, choice, true);
            }.bind(this));
        }
        this.currentlyAddingChoices = false;
    },
    processBulkLikertAddition: function (choices) {
        var field = this.Form.selectFieldByColumnId(this.activeField);
        var showAlert = false;
        if (field.Choices.length + choices.length > 10 && field.Validation != 'na') showAlert = true;
        if (field.Choices.length + choices.length > 11 && field.Validation == 'na') showAlert = true;
        if (!this.currentlyAddingChoices) {
            this.currentlyAddingChoices = true;
            choices.each(function (choice) {
                if (field.Choices.length < 100 || (field.Choices.length < 101 && field.Validation == 'na')) {
                    var ctrl = $('likertColumns').lastChild.childNodes[3];
                    if (field.Validation == 'na') {
                        ctrl = $('likertColumns').down('li', $('likertColumns').childElements().length - 1);
                        ctrl = ctrl.previous().childNodes[3];
                    }
                    
                    var position = $(ctrl).up().previousSiblings().length;
                    this.FieldProperties.insertChoice(ctrl.up(), choice, 'column');
                    this.FormEngine.addChoice(field, {
                        "Choice": choice,
                        "IsDefault": "0",
                        "IsRight": "1"
                    }, position);
                }
            }.bind(this));
        }
        this.currentlyAddingChoices = false;
        this.FieldEngine.displayField(field);
        if (showAlert) alert(this.notAllChoicesWereAddedError);
    },
    deleteCheckbox: function (ctrl) {
        var extra = '';
        if(this.activeExtraField > 0) {
            extra = 'extra_';
            var field = this.Form.selectExtraFieldByColumnId(this.activeField, this.activeExtraField);
        } else {
            var field = this.Form.selectFieldByColumnId(this.activeField);
        }
        var position = $(ctrl).up().previousSiblings().length;
        subfield = field.SubFields[position];
        if (subfield.HasntBeenSaved || confirm(this.deletingWillDeleteDataWarning)) {
            if (subfield.HasntBeenSaved) this.deleteChoice(ctrl);
            else {
                subfield.Typeof = 'text';
                subfield.FormId = field.FormId;
                this.FieldEngine.deleteCheckboxChoice(subfield, ctrl);
            }
        }
    },
    deleteLikertStatement: function (ctrl) {
        var extra = '';
        if(this.activeExtraField > 0) {
            extra = 'extra_';
            var field = this.Form.selectExtraFieldByColumnId(this.activeField, this.activeExtraField);
        } else {
            var field = this.Form.selectFieldByColumnId(this.activeField);
        }
        var position = $(ctrl).up().previousSiblings().length;
        subfield = field.SubFields[position];
        if (subfield.HasntBeenSaved || confirm(this.deletingWillDeleteDataWarning)) {
            if (subfield.HasntBeenSaved) this.deleteSubField(ctrl);
            else {
                subfield.Typeof = 'text';
                subfield.FormId = field.FormId;
                this.FieldEngine.deleteLikertStatement(subfield, ctrl);
            }
        }
    },
    toggleLikertNumbers: function (checked) {
        if (checked) {
            this.Form.addSetting('hideNumbers', this.activeField);
            $('foli' + this.activeField).addClassName('hideNumbers');
        } else {
            this.Form.removeSetting('hideNumbers', this.activeField);
            $('foli' + this.activeField).removeClassName('hideNumbers');
        }
    },
    toggleLikertNonApplicable: function (checked) {
        var list = $('likertColumns');
        var clickedEl = list.down('li', list.childElements().length - 1).down('img');
        if (checked) {
            this.updateField('na', 'Validation');
            this.addLikertChoice(clickedEl, 'N/A');
            this.FieldProperties.highlightLikertColumn(list.down('li', list.childElements().length - 1));
            list.down('li', list.childElements().length - 1).down('img').remove();
        } else {
            this.updateField('', 'Validation');
            this.deleteLikertChoice(clickedEl);
        }
    },
    toggleLikertDatCheckbox: function (checked) {
        var list = $('likertColumns');
        var clickedEl = list.down('li', list.childElements().length - 1).down('img');
        if (checked) {
            this.updateField('dc', 'Validation');
            this.addLikertChoice2(clickedEl, 'N/A');
            //this.FieldProperties.highlightLikertColumn(list.down('li', list.childElements().length - 1));
            //list.down('li', list.childElements().length - 1).down('img').remove();
        } else {
            this.updateField('', 'Validation');
            this.deleteLikertChoice2(clickedEl);
        }
    },
    setCookie: function (id, minutes) {
        var today = new Date();
        today.setTime(today.getTime());
        var expires = minutes * 1000 * 60;
        var expires_date = new Date(today.getTime() + expires);
        var cookie = 'wuFormId' + "=" + escape(id) + ";expires=" + expires_date.toGMTString() + ";path=/;";
        document.cookie = cookie;
    },
    storeFormId: function (id) {
        this.WuCookie.init(this.WuCookie.getIFrameDefault());
        this.WuCookie.setData('formId', id);
    },
    /*
    * ExtraFields: Popup
    */
    addExtraDefaultFields: function(ctrl) {
        this.Fields.addExtraDefaultFields(ctrl);
    },
    renameGroup: function(field_name) {
        this.Fields.renameGroup(field_name);
    },
    defaultGroup: function(checkVal) {
        this.Fields.defaultGroup(checkVal);  
    },
    getSavedFields: function() {
        this.Fields.getSavedFields();  
    },
    extra_deleteField: function (columnId, e) {
        if (!e) var e = window.event;
        e.cancelBubble = true;
        if (e.stopPropagation) e.stopPropagation();
        var fieldToDelete = this.Fields.selectFieldByColumnId(columnId);
        if(!fieldToDelete) return;
        var confirm = window.confirm('ok?');
        if(confirm) this.extra_finishDeletingField({
            'ColumnId': columnId
        }, e);
    },
    extra_continueDeletingField: function () {
        var fieldToDelete = this.Form.selectFieldByColumnId($F('deleteFieldId'));
        Lightbox.close();
        this.FieldEngine.deleteField(fieldToDelete);
    },
    extra_finishDeletingField: function (ret, e) {
        var extra = '';
        this.Fields.removeField(ret.ColumnId);
        this.Fields.clearFieldAfterDelete(ret.ColumnId);
    },
    extra_processDeleteError: function (ret) {
        Lightbox.showContent(ret.response.lbmarkup);
    },
    extra_updateField: function (value, parameter, columnId) {
        if (!columnId) columnId = this.Fields.activeDetailField;
        var Field = this.Fields.setField(value, parameter, columnId);
        if(parameter == 'Title') {
            $('extra_fieldSingle' + this.Fields.activeDetailField).down('a').innerHTML = value;
        }
    },
    extra_removeEncryption: function () {
        var extra = 'extra_';
        this.extra_updateField('0', 'IsEncrypted');
        $(extra + 'fieldEncrypted').checked = false;
        Lightbox.close();
    },
    
    extra_changeFieldType: function (newType, sameDbColumn) {
        
    },
    extra_finishChangingField: function (ret, originalField, sameDbColumn) {
        var originalField = this.Form.selectFieldByColumnId(originalField);
        this.FormEngine.removeField(this.Form, originalField);
        this.processAddSuccess(ret, 'foli' + originalField.ColumnId);
        this.FieldPreview.removeFieldWithoutAnimation(originalField);
        this.updatePositionsByHtml();
        this.activeField = -1;
        if (sameDbColumn && !originalField.HasntBeenSaved) {
            var newField = this.Form.selectFieldByColumnId(ret.response.id);
            newField.HasntBeenSaved = false;
        }
        this.selectField($('foli' + ret.response.id));
        Event.observe($('foli' + ret.response.id), 'click', selectField.bind($('foli' + ret.response.id)), false);
    },
    extra_toggleChoicesLayout: function (setting) {
        var settings = new Array('notStacked', 'twoColumns', 'threeColumns');
        for (var i = 0; i < settings.length; i++) {
            this.Fields.removeSetting(settings[i], this.activeField);
        }
        this.Fields.addSetting(setting, this.activeField);
    },
    extra_updateChoice: function (ctrl) {
        var position = $(ctrl).up().previousSiblings().length;
        var value = $F(ctrl);
        var fieldBeingUpdated = this.Fields.setChoice(value, 'Choice', this.activeField, position, ctrl);
    },
    extra_addChoice: function (ctrl, value, noalert) {
        var extra = 'extra_';
        var field = this.Fields.getCurrentSelectField();
        var position = $(ctrl).up().previousSiblings().length;
        if (!value) value = '';
        if (field.Choices.length < 150) {
            this.Fields.insertChoice(ctrl.up(), value, field.Typeof);
            if (field.Typeof == 'checkbox') this.Fields.addSubField(field, {
                "Title": "",
                "ChoicesText": value,
                "DefaultVal": "0",
                "IsRight": "1"
            }, position, extra);
            else this.Fields.addChoice(field, {
                "Choice": value,
                "IsDefault": "0",
                "IsRight": "1"
            }, position, extra);
        } else if (!noalert) alert(this.choicesRestrictedWarning);
    },
    extra_addSubField: function (ctrl, value) {
        var extra = 'extra_';
        var field = this.Fields.getCurrentSelectField();
        var position = $(ctrl).up().previousSiblings().length;
        if (!value) value = '';
        var ftype = field.Typeof;
        if (field.Typeof == 'likert') ftype = 'statement';
        if (field.SubFields.length < 95) {
            this.Fields.insertChoice(ctrl.up(), value, ftype);
            this.Fields.addSubField(field, {
                "Title": "",
                "ChoicesText": value,
                "DefaultVal": "0",
                "IsRight": "1"
            }, position, extra);
        } else {
            alert(this.fieldMaxOptionsWarning);
        }
    },
    extra_deleteSubField: function (ctrl, value) {
        var extra = 'extra_';
        var field = this.Fields.getCurrentSelectField();
        var position = $(ctrl).up().previousSiblings().length;
        if (position == 0 && field.SubFields && field.SubFields.length == 1) {
            Element.previous(ctrl, 'input').value = '';
            this.extra_updateChoice(Element.previous(ctrl, 'input'));
        } else {
            this.Fields.deleteChoice(ctrl.parentNode);
            this.Fields.removeSubField(field, position);
        }
    },
    extra_addLikertChoice: function (ctrl, value) {
        var extra = 'extra_';
        var field = this.Fields.getCurrentSelectField();
        if($(extra + 'fieldDC').checked == true) {
            $(extra + 'fieldDC').checked = false;
        }
        var position = $(ctrl).up().previousSiblings().length;
        if (!value) value = '';
        if (field.Choices.length < 10 || (field.Choices.length < 11 && field.Validation == 'na')) {
            this.Fields.insertChoice(ctrl.up(), value, 'column');
            this.Fields.addChoice(field, {
                "Choice": value,
                "IsDefault": "0",
                "IsRight": "1"
            }, position, extra);
        } else {
            alert(this.fieldMaxColumnsWarning);
        }
    },
    extra_deleteLikertChoice: function (ctrl, value) {
        var extra = 'extra_';
        var field = this.Fields.getCurrentSelectField();
        if($(extra + 'fieldDC').checked == true) {
            $(extra + 'fieldDC').checked = false;
        }
        var position = $(ctrl).up().previousSiblings().length;
        if (position == 0 && field.Choices && field.Choices.length == 1) {
            Element.previous(ctrl, 'input').value = '';
            this.extra_updateChoice(Element.previous(ctrl, 'input'));
        } else {
            
            var toggleNa = false;
            if (!$(ctrl).up().next()) toggleNa = true;
            this.Fields.deleteChoice(ctrl.parentNode);
            this.Fields.removeChoice(field, position, extra);
            if (toggleNa && field.Validation == 'na') {
                $(extra + 'fieldNA').checked = false;
                this.extra_updateField(field.ColumnId, 'Validation');
            }
        }
    },
    extra_addLikertChoice2: function (ctrl, value) {
        var extra = 'extra_';
        var field = this.Fields.getCurrentSelectField();
        var position = $(ctrl).up().previousSiblings().length;
        if (!value) value = '';
        if (field.Choices.length < 10 || (field.Choices.length < 11 && field.Validation == 'dc')) {
            //this.FieldProperties.insertChoice(ctrl.up(), value, 'column');
            if($(extra + 'fieldNA').checked == true) {
                $(extra + 'fieldNA').checked = false;
                this.Fields.deleteChoice(ctrl.parentNode);
                this.Fields.removeChoice(field, position);  
            }
        } else {
            alert(this.fieldMaxColumnsWarning);
        }
    },
    extra_deleteLikertChoice2: function (ctrl, value) {
        var extra = 'extra_';
        var field = this.Fields.getCurrentSelectField();
        var position = $(ctrl).up().previousSiblings().length;
        if($(extra + 'fieldNA').checked == true) {
            $(extra + 'fieldNA').checked = false;
            this.Fields.deleteChoice(ctrl.parentNode);
            this.Fields.removeChoice(field, position);  
        }
        if (position == 0 && field.Choices && field.Choices.length == 1) {
            Element.previous(ctrl, 'input').value = '';
            this.extra_updateChoice(Element.previous(ctrl, 'input'));
        } else {
            var toggleNa = false;
            if (!$(ctrl).up().next()) toggleNa = true;
            if (toggleNa && field.Validation == 'dc') {
                this.extra_updateField(field.ColumnId, 'Validation');
            }
        }
    },
    extra_deleteChoice: function (ctrl) {
        var extra = 'extra_';
        var field = this.Fields.getCurrentSelectField();
        var position = $(ctrl).up().previousSiblings().length;
        if (position == 0 && ((field.Choices && field.Choices.length == 1) || (field.SubFields && field.SubFields.length == 1))) {
            Element.previous(ctrl, 'input').value = '';
            this.extra_updateChoice(Element.previous(ctrl, 'input'));
        } else {
            var toggleOther = false;
            if (!$(ctrl).up().next()) toggleOther = true;
            this.Fields.deleteChoice(ctrl.parentNode);
            if (field.Typeof == 'checkbox') this.Fields.removeSubField(field, position);
            else this.Fields.removeChoice(field, position);
            if (field.Typeof == 'radio' && field.Settings.indexOf('other') != -1 && toggleOther) {
                this.Fields.removeSetting('other', this.activeField);
                $(extra + 'fieldOther').checked = false;
            }
        }
    },
    extra_changeChoiceDefault: function (ctrl) {
        var extra = 'extra_';
        var field = this.Fields.getCurrentSelectField();
        var position = $(ctrl).up().previousSiblings().length;
        if (field.Typeof == 'checkbox') this.Fields.setDefaultCheckboxChoice(field, position);
        else this.Fields.setDefaultChoice(field, position);
        this.Fields.onFieldUpdate(field, 'IsDefault');
    },
    extra_changeRightDefault: function (ctrl) {
        var extra = 'extra_';
        var field = this.Fields.getCurrentSelectField();
        var position = $(ctrl).up().previousSiblings().length;
        if (field.Typeof == 'checkbox') this.Fields.setDefaultRightCheckboxChoice(field, position);
        else this.Fields.setDefaultRightChoice(field, position);
        this.Fields.onFieldUpdate(field, 'IsRight');
    },
    extra_toggleRadioOther: function (checked) {
        var extra = 'extra_';
        var field = this.Fields.getCurrentSelectField();
        var list = $(extra + 'fieldChoices');
        var clickedEl = list.down('li', list.childElements().length - 1).down('img');
        if (checked) {
            this.Fields.addSetting('other', this.activeField);
            this.extra_addChoice(clickedEl, 'Other');
            this.Fields.highlightRadioOther(list.down('li', list.childElements().length - 1));
            list.down('li', list.childElements().length - 1).down('img').remove();
        } else {
            this.Fields.removeSetting('other', this.activeField);
            this.extra_deleteChoice(clickedEl);
        }
    },
    
    extra_deleteCheckbox: function (ctrl) {
        var extra = 'extra_';
        var field = this.Fields.getCurrentSelectField();
        var position = $(ctrl).up().previousSiblings().length;
        subfield = field.SubFields[position];
        this.extra_deleteChoice(ctrl);
    },
    extra_deleteLikertStatement: function (ctrl) {
        var extra = 'extra_';
        var field = this.Fields.getCurrentSelectField();
        var position = $(ctrl).up().previousSiblings().length;
        subfield = field.SubFields[position];
        this.extra_deleteSubField(ctrl);
    },
    extra_toggleLikertNumbers: function (checked) {
        var extra = 'extra_';
        if (checked) {
            this.Fields.addSetting('hideNumbers', this.activeField);
        } else {
            this.Fields.removeSetting('hideNumbers', this.activeField);
        }
    },
    extra_toggleLikertNonApplicable: function (checked) {
        var extra = 'extra_';
        var list = $(extra + 'likertColumns');
        var clickedEl = list.down('li', list.childElements().length - 1).down('img');
        if (checked) {
            this.extra_updateField('na', 'Validation');
            this.extra_addLikertChoice(clickedEl, 'N/A');
            this.Fields.highlightLikertColumn(list.down('li', list.childElements().length - 1));
            list.down('li', list.childElements().length - 1).down('img').remove();
        } else {
            this.extra_updateField('', 'Validation');
            this.extra_deleteLikertChoice(clickedEl);
        }
    },
    extra_toggleLikertDatCheckbox: function (checked) {
        var extra = 'extra_';
        var list = $(extra + 'likertColumns');
        var clickedEl = list.down('li', list.childElements().length - 1).down('img');
        if (checked) {
            this.extra_updateField('dc', 'Validation');
            this.extra_addLikertChoice2(clickedEl, 'N/A');
        } else {
            this.extra_updateField('', 'Validation');
            this.extra_deleteLikertChoice2(clickedEl);
        }
    }
    /*
    * ExtraFields: End Popup
    */
});
var __BUILD;
Event.observe(window, 'load', init, false);

function init() {
    __BUILD = new FormBuilder();
    initBulkAdd();
}

function selectForm() {
    __BUILD.selectForm();
}

function selectPageSettings() {
    __BUILD.selectPageSettings();
}

function updateForm(value, parameter) {
    if (value.replace) value = value.replace(/\\\"/g, "\\ \"");
    __BUILD.updateForm(value, parameter);
}

function saveForm() {
    __BUILD.saveForm();
}

function saveField() {
    __BUILD.saveField();
}

function addFieldToForm(ctrl) {
    __BUILD.addFieldToForm(ctrl);
}
function addFieldToForm2(group_id) {
    __BUILD.addFieldToForm2(group_id);
}
function addGroupToForm(ctrl) {
    __BUILD.addGroupToForm(ctrl);
}

function finishSaving(ret) {
    if (ret.success == 'false') __BUILD.processSaveError(ret);
    else {
        if (ret.response.demo == 'true') __BUILD.processSaveDemoSuccess(ret);
        else __BUILD.processSaveSuccess(ret);
    }
}

function resetSaveFormAppearance() {
    __BUILD.resetSaveFormAppearance();
}

function addCaptchaToPreview(useCaptcha) {
    __BUILD.addCaptchaToPreview(useCaptcha);
}

function toggleFormConfirmationEmail(setting) {
    if (setting) __BUILD.showConfirmationEmailToUser();
    else __BUILD.hideConfirmationEmailToUser();
}

function displayReceiptLightbox() {
    __BUILD.displayReceiptLightbox();
}

function finishLoadingReceiptLightbox() {
    __BUILD.finishLoadingReceiptLightbox();
}

function toggleFormActivity(setting) {
    if (setting) __BUILD.showFormActivity();
    else __BUILD.hideFormActivity();
}

function selectFormActivity(cal) {
    var p = cal.params;
    var update = (cal.dateClicked || p.electric);
    year = p.inputField.id;
    day = year + '-2';
    month = year + '-1';
    document.getElementById(month).value = cal.date.print('%m');
    document.getElementById(day).value = cal.date.print('%e');
    document.getElementById(year).value = cal.date.print('%Y');
    if (year == 'startDate') updateForm('live', 'StartDate');
    if (year == 'endDate') updateForm('live', 'EndDate');
}

function toggleFormPassword(setting) {
    if (setting) __BUILD.showFormPassword();
    else __BUILD.hideFormPassword();
}

function toggleFormRedirect(setting) {
    __BUILD.toggleFormRedirect(setting);
}

function updatePageHeader() {
    __BUILD.updatePageHeader();
}

function finishPageHeaders(ret) {
    if (ret.success == 'false') __BUILD.processSaveError(ret);
    else __BUILD.finishPageHeaders(ret);
}

function showButtonText(category) {
    __BUILD.showButtonType(category, 'asTxt');
}

function showButtonImage(category) {
    __BUILD.showButtonType(category, 'asImg');
}

function jumpToPage(direction) {
    __BUILD.jumpToPage(direction);
}

function showFields(pulse) {
    __BUILD.showFields(pulse);
}

function selectField(ctrl) {
    __BUILD.selectField(this);
}

function showExtraFields(pulse) {
    __BUILD.showExtraFields(pulse);
}

function selectExtraField(ctrl) {
    __BUILD.selectExtraField(this);
}

function selectExtraFieldPreview(ctrl) {
    __BUILD.selectExtraFieldPreview(this);
}

function getFieldId(ctrl, removeText) {
    return $(ctrl).up('li').id.replace(removeText,'');
}
function selectFieldGroup(ctrl) {
    __BUILD.selectFieldGroup(getFieldId(ctrl, 'extra_fieldGroup'));
}
function selectFieldSingle(ctrl) {
    __BUILD.selectFieldSingle(getFieldId(ctrl, 'extra_fieldSingle'));
}
function deleteFieldGroup(ctrl) {
    var confirm = window.confirm('Ok?');
    if(!confirm) return;
    __BUILD.deleteFieldGroup(getFieldId(ctrl, 'extra_fieldGroup'));
}
function finishDeleteFieldGroup(field_id) {
     __BUILD.finishDeleteFieldGroup(field_id);
}
function deleteFieldSingle(ctrl) {
    __BUILD.deleteFieldSingle(getFieldId(ctrl, 'extra_fieldSingle'));
}

function addField(type) {
    __BUILD.addField(type);
}

function addFieldAfterDraggingComplete() {
    __BUILD.addFieldAfterDraggingComplete();
}

function finishAddingField(ret, insertAfterElement, dragDropBox) {
    if (ret.success == 'false') __BUILD.processAddError(ret);
    else __BUILD.processAddSuccess(ret, insertAfterElement, false, dragDropBox);
}

function extra_selectField(ctrl) {
    __BUILD.extra_selectField(ctrl);
}

function finishSaveField(ret) {
    if (ret.success == 'false') __BUILD.processSaveFieldError(ret);
    else __BUILD.processSaveFieldSuccess(ret);
}
/*
* ExtraField: Add
*/
function addExtraField(type) {
    __BUILD.addExtraField(type);
}

function addExtraFieldAfterDraggingComplete() {
    __BUILD.addExtraFieldAfterDraggingComplete();
}

function finishAddingExtraField(ret, insertAfterElement, dragDropBox) {
    if (ret.success == 'false') __BUILD.processAddExtraError(ret);
    else __BUILD.processAddExtraSuccess(ret, insertAfterElement, false, dragDropBox);
}

function finishAddedExtraField(ret, insertAfterElement, dragDropBox) {
    if (ret.success == 'false') __BUILD.processAddedExtraError(ret);
    else __BUILD.processAddedExtraSuccess(ret, insertAfterElement, false, dragDropBox);
}
function addFieldToFields(newField) {
    __BUILD.addFieldToFields(newField);
}
/*
* ExtraField: End Add
*/
function finishAddingPage(ret, insertAfterElement, dragDropBox) {
    if (ret.success == 'false') __BUILD.processAddError(ret);
    else __BUILD.finishAddingPage(ret, insertAfterElement, dragDropBox);
}

function duplicateField(columnId, e) {
    __BUILD.duplicateField(columnId, e);
}

function continueDeletingField() {
    __BUILD.continueDeletingField();
}

function changeFieldType(newType, sameDbColumn) {
    __BUILD.changeFieldType(newType, sameDbColumn);
}

function finishChangingField(ret, originalField, sameDbColumn) {
    if (ret.success == 'false') __BUILD.processAddError(ret);
    else __BUILD.finishChangingField(ret, originalField, sameDbColumn);
}

function removeField(columnId, e) {
    __BUILD.deleteField(columnId, e);
}

function finishDeletingField(ret) {
    if (ret.success == 'false') __BUILD.processDeleteError(ret);
    else __BUILD.finishDeletingField(ret.response);
}

function updateProperties(value, parameter, pos) {
    if (value.replace) value = value.replace(/\\\"/g, "\\ \"");
    __BUILD.updateField(value, parameter, pos);
}

function updateFieldDisplay() {
    __BUILD.updateFieldDisplay();
}

function finishDisplayingField(ret) {
    if (ret.success == 'false') __BUILD.processAddError(ret);
    else __BUILD.finishDisplayingField(ret);
}

function defaultDate(value) {
    __BUILD.defaultDate(value);
}

function defaultPrice(value) {
    __BUILD.defaultPrice(value);
}

function reorderField() {
    __BUILD.updatePositionsByHtml();
}

function removeEncryption() {
    __BUILD.removeEncryption();
}

function toggleLikertNumbers(checked) {
    __BUILD.toggleLikertNumbers(checked);
}

function toggleLikertNonApplicable(checked) {
    __BUILD.toggleLikertNonApplicable(checked);
}

function toggleLikertDatCheckbox(checked) {
    __BUILD.toggleLikertDatCheckbox(checked);
}

function toggleChoicesLayout(setting) {
    __BUILD.toggleChoicesLayout(setting);
}

function updateChoice(ctrl) {
    __BUILD.updateChoice(ctrl);
}

function addRadio(ctrl, value) {
    __BUILD.addChoice(ctrl, value);
}

function deleteRadio(ctrl) {
    __BUILD.deleteChoice(ctrl);
}

function changeRadioDefault(ctrl) {
    __BUILD.changeChoiceDefault(ctrl);
}

function changeRightDefault(ctrl) {
    __BUILD.changeRightDefault(ctrl);
}

function toggleRadioOther(checked) {
    __BUILD.toggleRadioOther(checked);
}

function addSelect(ctrl, value) {
    __BUILD.addChoice(ctrl, value);
}

function deleteSelect(ctrl) {
    __BUILD.deleteChoice(ctrl);
}

function changeSelectDefault(ctrl) {
    __BUILD.changeChoiceDefault(ctrl);
}

function processBulkAddition(choices) {
    __BUILD.processBulkAddition(choices);
}

function addCheckbox(ctrl, value) {
    __BUILD.addSubField(ctrl, value);
}

function deleteCheckbox(ctrl) {
    __BUILD.deleteCheckbox(ctrl);
}

function finishDeletingCheckbox(ret, ctrl) {
    if (ret.success == 'false') __BUILD.processDeleteError(ret);
    else __BUILD.deleteChoice(ctrl);
}

function changeCheckboxDefault(ctrl) {
    __BUILD.changeChoiceDefault(ctrl);
}

function addLikertStatement(ctrl, value) {
    __BUILD.addSubField(ctrl, value);
}

function deleteLikertStatement(ctrl, value) {
    __BUILD.deleteLikertStatement(ctrl, value);
}

function finishDeletingLikertStatement(ret, ctrl) {
    if (ret.success == 'false') __BUILD.processDeleteError(ret);
    else __BUILD.deleteSubField(ctrl);
}

function addLikertChoice(ctrl, value) {
    __BUILD.addLikertChoice(ctrl, value);
}

function deleteLikertChoice(ctrl, value) {
    __BUILD.deleteLikertChoice(ctrl, value);
}

function processBulkLikertAddition(choices) {
    __BUILD.processBulkLikertAddition(choices);
}

function changeLikertDefault(ctrl) {
    __BUILD.changeChoiceDefault(ctrl);
}

/*
* ExtraFields: Popup
*/
function autoCountField() {
    __BUILD.autoCountField();
}
function addExtraDefaultFields(ctrl) {
    __BUILD.addExtraDefaultFields(ctrl);
}
function defaultGroup(checkVal) {
    __BUILD.defaultGroup(checkVal);
}
function renameGroup(field_name) {
    if(field_name.length == 0) return;
    __BUILD.renameGroup(field_name);
}
function getSavedFields() {
    __BUILD.getSavedFields();
}
function extra_updateProperties(value, parameter, pos) {
    if (value.replace) value = value.replace(/\\\"/g, "\\ \"");
    __BUILD.extra_updateField(value, parameter, pos);
}


function extra_toggleLikertNumbers(checked) {
    __BUILD.extra_toggleLikertNumbers(checked);
}

function extra_toggleLikertNonApplicable(checked) {
    __BUILD.extra_toggleLikertNonApplicable(checked);
}

function extra_toggleLikertDatCheckbox(checked) {
    __BUILD.extra_toggleLikertDatCheckbox(checked);
}

function extra_toggleChoicesLayout(setting) {
    __BUILD.extra_toggleChoicesLayout(setting);
}

function extra_updateChoice(ctrl) {
    __BUILD.extra_updateChoice(ctrl);
}

function extra_addRadio(ctrl, value) {
    __BUILD.extra_addChoice(ctrl, value);
}

function extra_deleteRadio(ctrl) {
    __BUILD.extra_deleteChoice(ctrl);
}

function extra_changeRadioDefault(ctrl) {
    __BUILD.extra_changeChoiceDefault(ctrl);
}

function extra_changeRightDefault(ctrl) {
    __BUILD.extra_changeRightDefault(ctrl);
}

function extra_toggleRadioOther(checked) {
    __BUILD.extra_toggleRadioOther(checked);
}

function extra_addSelect(ctrl, value) {
    __BUILD.extra_addChoice(ctrl, value);
}

function extra_deleteSelect(ctrl) {
    __BUILD.extra_deleteChoice(ctrl);
}

function extra_changeSelectDefault(ctrl) {
    __BUILD.extra_changeChoiceDefault(ctrl);
}

function extra_processBulkAddition(choices) {
    __BUILD.extra_processBulkAddition(choices);
}

function extra_addCheckbox(ctrl, value) {
    __BUILD.extra_addSubField(ctrl, value);
}

function extra_deleteCheckbox(ctrl) {
    __BUILD.extra_deleteCheckbox(ctrl);
}

function extra_finishDeletingCheckbox(ret, ctrl) {
    if (ret.success == 'false') __BUILD.extra_processDeleteError(ret);
    else __BUILD.extra_deleteChoice(ctrl);
}

function extra_changeCheckboxDefault(ctrl) {
    __BUILD.extra_changeChoiceDefault(ctrl);
}

function extra_addLikertStatement(ctrl, value) {
    __BUILD.extra_addSubField(ctrl, value);
}

function extra_deleteLikertStatement(ctrl, value) {
    __BUILD.extra_deleteLikertStatement(ctrl, value);
}

function extra_finishDeletingLikertStatement(ret, ctrl) {
    if (ret.success == 'false') __BUILD.extra_processDeleteError(ret);
    else __BUILD.extra_deleteSubField(ctrl);
}

function extra_addLikertChoice(ctrl, value) {
    __BUILD.extra_addLikertChoice(ctrl, value);
}

function extra_deleteLikertChoice(ctrl, value) {
    __BUILD.extra_deleteLikertChoice(ctrl, value);
}

function extra_processBulkLikertAddition(choices) {
    __BUILD.extra_processBulkLikertAddition(choices);
}

function extra_changeLikertDefault(ctrl) {
    __BUILD.extra_changeChoiceDefault(ctrl);
}


/*
* ExtraFields: End Popup
*/
function setCookie(id) {
    __BUILD.setCookie(id);
}

function storeFormId(id) {
    __BUILD.storeFormId(id);
}
Calendar = function (firstDayOfWeek, dateStr, onSelected, onClose) {
    this.activeDiv = null;
    this.currentDateEl = null;
    this.getDateStatus = null;
    this.getDateToolTip = null;
    this.getDateText = null;
    this.timeout = null;
    this.onSelected = onSelected || null;
    this.onClose = onClose || null;
    this.dragging = false;
    this.hidden = false;
    this.minYear = 1970;
    this.maxYear = 2050;
    this.dateFormat = Calendar._TT["DEF_DATE_FORMAT"];
    this.ttDateFormat = Calendar._TT["TT_DATE_FORMAT"];
    this.isPopup = true;
    this.weekNumbers = true;
    this.firstDayOfWeek = typeof firstDayOfWeek == "number" ? firstDayOfWeek : Calendar._FD;
    this.showsOtherMonths = false;
    this.dateStr = dateStr;
    this.ar_days = null;
    this.showsTime = false;
    this.time24 = true;
    this.yearStep = 2;
    this.hiliteToday = true;
    this.multiple = null;
    this.table = null;
    this.element = null;
    this.tbody = null;
    this.firstdayname = null;
    this.monthsCombo = null;
    this.yearsCombo = null;
    this.hilitedMonth = null;
    this.activeMonth = null;
    this.hilitedYear = null;
    this.activeYear = null;
    this.dateClicked = false;
    if (typeof Calendar._SDN == "undefined") {
        if (typeof Calendar._SDN_len == "undefined") Calendar._SDN_len = 3;
        var ar = new Array();
        for (var i = 8; i > 0;) {
            ar[--i] = Calendar._DN[i].substr(0, Calendar._SDN_len);
        }
        Calendar._SDN = ar;
        if (typeof Calendar._SMN_len == "undefined") Calendar._SMN_len = 3;
        ar = new Array();
        for (var i = 12; i > 0;) {
            ar[--i] = Calendar._MN[i].substr(0, Calendar._SMN_len);
        }
        Calendar._SMN = ar;
    }
};
Calendar._C = null;
Calendar.is_ie = (/msie/i.test(navigator.userAgent) && !/opera/i.test(navigator.userAgent));
Calendar.is_ie5 = (Calendar.is_ie && /msie 5\.0/i.test(navigator.userAgent));
Calendar.is_opera = /opera/i.test(navigator.userAgent);
Calendar.is_khtml = /Konqueror|Safari|KHTML/i.test(navigator.userAgent);
Calendar.getAbsolutePos = function (el) {
    var SL = 0,
        ST = 0;
    var is_div = /^div$/i.test(el.tagName);
    if (is_div && el.scrollLeft) SL = el.scrollLeft;
    if (is_div && el.scrollTop) ST = el.scrollTop;
    var r = {
        x: el.offsetLeft - SL,
        y: el.offsetTop - ST
    };
    if (el.offsetParent) {
        var tmp = this.getAbsolutePos(el.offsetParent);
        r.x += tmp.x;
        r.y += tmp.y;
    }
    return r;
};
Calendar.isRelated = function (el, evt) {
    var related = evt.relatedTarget;
    if (!related) {
        var type = evt.type;
        if (type == "mouseover") {
            related = evt.fromElement;
        } else if (type == "mouseout") {
            related = evt.toElement;
        }
    }
    while (related) {
        if (related == el) {
            return true;
        }
        related = related.parentNode;
    }
    return false;
};
Calendar.removeClass = function (el, className) {
    if (!(el && el.className)) {
        return;
    }
    var cls = el.className.split(" ");
    var ar = new Array();
    for (var i = cls.length; i > 0;) {
        if (cls[--i] != className) {
            ar[ar.length] = cls[i];
        }
    }
    el.className = ar.join(" ");
};
Calendar.addClass = function (el, className) {
    Calendar.removeClass(el, className);
    el.className += " " + className;
};
Calendar.getElement = function (ev) {
    var f = Calendar.is_ie ? window.event.srcElement : ev.currentTarget;
    while (f.nodeType != 1 || /^div$/i.test(f.tagName))
    f = f.parentNode;
    return f;
};
Calendar.getTargetElement = function (ev) {
    var f = Calendar.is_ie ? window.event.srcElement : ev.target;
    while (f.nodeType != 1)
    f = f.parentNode;
    return f;
};
Calendar.stopEvent = function (ev) {
    ev || (ev = window.event);
    if (Calendar.is_ie) {
        ev.cancelBubble = true;
        ev.returnValue = false;
    } else {
        ev.preventDefault();
        ev.stopPropagation();
    }
    return false;
};
Calendar.addEvent = function (el, evname, func) {
    if (el.attachEvent) {
        el.attachEvent("on" + evname, func);
    } else if (el.addEventListener) {
        el.addEventListener(evname, func, true);
    } else {
        el["on" + evname] = func;
    }
};
Calendar.removeEvent = function (el, evname, func) {
    if (el.detachEvent) {
        el.detachEvent("on" + evname, func);
    } else if (el.removeEventListener) {
        el.removeEventListener(evname, func, true);
    } else {
        el["on" + evname] = null;
    }
};
Calendar.createElement = function (type, parent) {
    var el = null;
    if (document.createElementNS) {
        el = document.createElementNS("http://www.w3.org/1999/xhtml", type);
    } else {
        el = document.createElement(type);
    }
    if (typeof parent != "undefined") {
        parent.appendChild(el);
    }
    return el;
};
Calendar._add_evs = function (el) {
    with(Calendar) {
        addEvent(el, "mouseover", dayMouseOver);
        addEvent(el, "mousedown", dayMouseDown);
        addEvent(el, "mouseout", dayMouseOut);
        if (is_ie) {
            addEvent(el, "dblclick", dayMouseDblClick);
            el.setAttribute("unselectable", true);
        }
    }
};
Calendar.findMonth = function (el) {
    if (typeof el.month != "undefined") {
        return el;
    } else if (typeof el.parentNode.month != "undefined") {
        return el.parentNode;
    }
    return null;
};
Calendar.findYear = function (el) {
    if (typeof el.year != "undefined") {
        return el;
    } else if (typeof el.parentNode.year != "undefined") {
        return el.parentNode;
    }
    return null;
};
Calendar.showMonthsCombo = function () {
    var cal = Calendar._C;
    if (!cal) {
        return false;
    }
    var cal = cal;
    var cd = cal.activeDiv;
    var mc = cal.monthsCombo;
    if (cal.hilitedMonth) {
        Calendar.removeClass(cal.hilitedMonth, "hilite");
    }
    if (cal.activeMonth) {
        Calendar.removeClass(cal.activeMonth, "active");
    }
    var mon = cal.monthsCombo.getElementsByTagName("div")[cal.date.getMonth()];
    Calendar.addClass(mon, "active");
    cal.activeMonth = mon;
    var s = mc.style;
    s.display = "block";
    if (cd.navtype < 0) s.left = cd.offsetLeft + "px";
    else {
        var mcw = mc.offsetWidth;
        if (typeof mcw == "undefined") mcw = 50;
        s.left = (cd.offsetLeft + cd.offsetWidth - mcw) + "px";
    }
    s.top = (cd.offsetTop + cd.offsetHeight) + "px";
};
Calendar.showYearsCombo = function (fwd) {
    var cal = Calendar._C;
    if (!cal) {
        return false;
    }
    var cal = cal;
    var cd = cal.activeDiv;
    var yc = cal.yearsCombo;
    if (cal.hilitedYear) {
        Calendar.removeClass(cal.hilitedYear, "hilite");
    }
    if (cal.activeYear) {
        Calendar.removeClass(cal.activeYear, "active");
    }
    cal.activeYear = null;
    var Y = cal.date.getFullYear() + (fwd ? 1 : -1);
    var yr = yc.firstChild;
    var show = false;
    for (var i = 12; i > 0; --i) {
        if (Y >= cal.minYear && Y <= cal.maxYear) {
            yr.innerHTML = Y;
            yr.year = Y;
            yr.style.display = "block";
            show = true;
        } else {
            yr.style.display = "none";
        }
        yr = yr.nextSibling;
        Y += fwd ? cal.yearStep : -cal.yearStep;
    }
    if (show) {
        var s = yc.style;
        s.display = "block";
        if (cd.navtype < 0) s.left = cd.offsetLeft + "px";
        else {
            var ycw = yc.offsetWidth;
            if (typeof ycw == "undefined") ycw = 50;
            s.left = (cd.offsetLeft + cd.offsetWidth - ycw) + "px";
        }
        s.top = (cd.offsetTop + cd.offsetHeight) + "px";
    }
};
Calendar.tableMouseUp = function (ev) {
    var cal = Calendar._C;
    if (!cal) {
        return false;
    }
    if (cal.timeout) {
        clearTimeout(cal.timeout);
    }
    var el = cal.activeDiv;
    if (!el) {
        return false;
    }
    var target = Calendar.getTargetElement(ev);
    ev || (ev = window.event);
    Calendar.removeClass(el, "active");
    if (target == el || target.parentNode == el) {
        Calendar.cellClick(el, ev);
    }
    var mon = Calendar.findMonth(target);
    var date = null;
    if (mon) {
        date = new Date(cal.date);
        if (mon.month != date.getMonth()) {
            date.setMonth(mon.month);
            cal.setDate(date);
            cal.dateClicked = false;
            cal.callHandler();
        }
    } else {
        var year = Calendar.findYear(target);
        if (year) {
            date = new Date(cal.date);
            if (year.year != date.getFullYear()) {
                date.setFullYear(year.year);
                cal.setDate(date);
                cal.dateClicked = false;
                cal.callHandler();
            }
        }
    }
    with(Calendar) {
        removeEvent(document, "mouseup", tableMouseUp);
        removeEvent(document, "mouseover", tableMouseOver);
        removeEvent(document, "mousemove", tableMouseOver);
        cal._hideCombos();
        _C = null;
        return stopEvent(ev);
    }
};
Calendar.tableMouseOver = function (ev) {
    var cal = Calendar._C;
    if (!cal) {
        return;
    }
    var el = cal.activeDiv;
    var target = Calendar.getTargetElement(ev);
    if (target == el || target.parentNode == el) {
        Calendar.addClass(el, "hilite active");
        Calendar.addClass(el.parentNode, "rowhilite");
    } else {
        if (typeof el.navtype == "undefined" || (el.navtype != 50 && (el.navtype == 0 || Math.abs(el.navtype) > 2))) Calendar.removeClass(el, "active");
        Calendar.removeClass(el, "hilite");
        Calendar.removeClass(el.parentNode, "rowhilite");
    }
    ev || (ev = window.event);
    if (el.navtype == 50 && target != el) {
        var pos = Calendar.getAbsolutePos(el);
        var w = el.offsetWidth;
        var x = ev.clientX;
        var dx;
        var decrease = true;
        if (x > pos.x + w) {
            dx = x - pos.x - w;
            decrease = false;
        } else dx = pos.x - x;
        if (dx < 0) dx = 0;
        var range = el._range;
        var current = el._current;
        var count = Math.floor(dx / 10) % range.length;
        for (var i = range.length; --i >= 0;)
        if (range[i] == current) break;
        while (count-- > 0)
        if (decrease) {
            if (--i < 0) i = range.length - 1;
        } else if (++i >= range.length) i = 0;
        var newval = range[i];
        el.innerHTML = newval;
        cal.onUpdateTime();
    }
    var mon = Calendar.findMonth(target);
    if (mon) {
        if (mon.month != cal.date.getMonth()) {
            if (cal.hilitedMonth) {
                Calendar.removeClass(cal.hilitedMonth, "hilite");
            }
            Calendar.addClass(mon, "hilite");
            cal.hilitedMonth = mon;
        } else if (cal.hilitedMonth) {
            Calendar.removeClass(cal.hilitedMonth, "hilite");
        }
    } else {
        if (cal.hilitedMonth) {
            Calendar.removeClass(cal.hilitedMonth, "hilite");
        }
        var year = Calendar.findYear(target);
        if (year) {
            if (year.year != cal.date.getFullYear()) {
                if (cal.hilitedYear) {
                    Calendar.removeClass(cal.hilitedYear, "hilite");
                }
                Calendar.addClass(year, "hilite");
                cal.hilitedYear = year;
            } else if (cal.hilitedYear) {
                Calendar.removeClass(cal.hilitedYear, "hilite");
            }
        } else if (cal.hilitedYear) {
            Calendar.removeClass(cal.hilitedYear, "hilite");
        }
    }
    return Calendar.stopEvent(ev);
};
Calendar.tableMouseDown = function (ev) {
    if (Calendar.getTargetElement(ev) == Calendar.getElement(ev)) {
        return Calendar.stopEvent(ev);
    }
};
Calendar.calDragIt = function (ev) {
    var cal = Calendar._C;
    if (!(cal && cal.dragging)) {
        return false;
    }
    var posX;
    var posY;
    if (Calendar.is_ie) {
        posY = window.event.clientY + document.body.scrollTop;
        posX = window.event.clientX + document.body.scrollLeft;
    } else {
        posX = ev.pageX;
        posY = ev.pageY;
    }
    cal.hideShowCovered();
    var st = cal.element.style;
    st.left = (posX - cal.xOffs) + "px";
    st.top = (posY - cal.yOffs) + "px";
    return Calendar.stopEvent(ev);
};
Calendar.calDragEnd = function (ev) {
    var cal = Calendar._C;
    if (!cal) {
        return false;
    }
    cal.dragging = false;
    with(Calendar) {
        removeEvent(document, "mousemove", calDragIt);
        removeEvent(document, "mouseup", calDragEnd);
        tableMouseUp(ev);
    }
    cal.hideShowCovered();
};
Calendar.dayMouseDown = function (ev) {
    var el = Calendar.getElement(ev);
    if (el.disabled) {
        return false;
    }
    var cal = el.calendar;
    cal.activeDiv = el;
    Calendar._C = cal;
    if (el.navtype != 300) with(Calendar) {
        if (el.navtype == 50) {
            el._current = el.innerHTML;
            addEvent(document, "mousemove", tableMouseOver);
        } else addEvent(document, Calendar.is_ie5 ? "mousemove" : "mouseover", tableMouseOver);
        addClass(el, "hilite active");
        addEvent(document, "mouseup", tableMouseUp);
    } else if (cal.isPopup) {
        cal._dragStart(ev);
    }
    if (el.navtype == -1 || el.navtype == 1) {
        if (cal.timeout) clearTimeout(cal.timeout);
        cal.timeout = setTimeout("Calendar.showMonthsCombo()", 250);
    } else if (el.navtype == -2 || el.navtype == 2) {
        if (cal.timeout) clearTimeout(cal.timeout);
        cal.timeout = setTimeout((el.navtype > 0) ? "Calendar.showYearsCombo(true)" : "Calendar.showYearsCombo(false)", 250);
    } else {
        cal.timeout = null;
    }
    return Calendar.stopEvent(ev);
};
Calendar.dayMouseDblClick = function (ev) {
    Calendar.cellClick(Calendar.getElement(ev), ev || window.event);
    if (Calendar.is_ie) {
        document.selection.empty();
    }
};
Calendar.dayMouseOver = function (ev) {
    var el = Calendar.getElement(ev);
    if (Calendar.isRelated(el, ev) || Calendar._C || el.disabled) {
        return false;
    }
    if (el.ttip) {
        if (el.ttip.substr(0, 1) == "_") {
            el.ttip = el.caldate.print(el.calendar.ttDateFormat) + el.ttip.substr(1);
        }
        el.calendar.tooltips.innerHTML = el.ttip;
    }
    if (el.navtype != 300) {
        Calendar.addClass(el, "hilite");
        if (el.caldate) {
            Calendar.addClass(el.parentNode, "rowhilite");
        }
    }
    return Calendar.stopEvent(ev);
};
Calendar.dayMouseOut = function (ev) {
    with(Calendar) {
        var el = getElement(ev);
        if (isRelated(el, ev) || _C || el.disabled) return false;
        removeClass(el, "hilite");
        if (el.caldate) removeClass(el.parentNode, "rowhilite");
        if (el.calendar) el.calendar.tooltips.innerHTML = _TT["SEL_DATE"];
        return stopEvent(ev);
    }
};
Calendar.cellClick = function (el, ev) {
    var cal = el.calendar;
    var closing = false;
    var newdate = false;
    var date = null;
    if (typeof el.navtype == "undefined") {
        if (cal.currentDateEl) {
            Calendar.removeClass(cal.currentDateEl, "selected");
            Calendar.addClass(el, "selected");
            closing = (cal.currentDateEl == el);
            if (!closing) {
                cal.currentDateEl = el;
            }
        }
        cal.date.setDateOnly(el.caldate);
        date = cal.date;
        var other_month = !(cal.dateClicked = !el.otherMonth);
        if (!other_month && !cal.currentDateEl) cal._toggleMultipleDate(new Date(date));
        else newdate = !el.disabled;
        if (other_month) cal._init(cal.firstDayOfWeek, date);
    } else {
        if (el.navtype == 200) {
            Calendar.removeClass(el, "hilite");
            cal.callCloseHandler();
            return;
        }
        date = new Date(cal.date);
        if (el.navtype == 0) date.setDateOnly(new Date());
        cal.dateClicked = false;
        var year = date.getFullYear();
        var mon = date.getMonth();

        function setMonth(m) {
            var day = date.getDate();
            var max = date.getMonthDays(m);
            if (day > max) {
                date.setDate(max);
            }
            date.setMonth(m);
        };
        switch (el.navtype) {
            case 400:
                Calendar.removeClass(el, "hilite");
                var text = Calendar._TT["ABOUT"];
                if (typeof text != "undefined") {
                    text += cal.showsTime ? Calendar._TT["ABOUT_TIME"] : "";
                } else {
                    text = "Help and about box text is not translated into this language.\n" + "If you know this language and you feel generous please update\n" + "the corresponding file in \"lang\" subdir to match calendar-en.js\n" + "and send it back to <mihai_bazon@yahoo.com> to get it into the distribution  ;-)\n\n" + "Thank you!\n" + "http://dynarch.com/mishoo/calendar.epl\n";
                }
                alert(text);
                return;
            case -2:
                if (year > cal.minYear) {
                    date.setFullYear(year - 1);
                }
                break;
            case -1:
                if (mon > 0) {
                    setMonth(mon - 1);
                } else if (year-- > cal.minYear) {
                    date.setFullYear(year);
                    setMonth(11);
                }
                break;
            case 1:
                if (mon < 11) {
                    setMonth(mon + 1);
                } else if (year < cal.maxYear) {
                    date.setFullYear(year + 1);
                    setMonth(0);
                }
                break;
            case 2:
                if (year < cal.maxYear) {
                    date.setFullYear(year + 1);
                }
                break;
            case 100:
                cal.setFirstDayOfWeek(el.fdow);
                return;
            case 50:
                var range = el._range;
                var current = el.innerHTML;
                for (var i = range.length; --i >= 0;)
                if (range[i] == current) break;
                if (ev && ev.shiftKey) {
                    if (--i < 0) i = range.length - 1;
                } else if (++i >= range.length) i = 0;
                var newval = range[i];
                el.innerHTML = newval;
                cal.onUpdateTime();
                return;
            case 0:
                if ((typeof cal.getDateStatus == "function") && cal.getDateStatus(date, date.getFullYear(), date.getMonth(), date.getDate())) {
                    return false;
                }
                break;
        }
        if (!date.equalsTo(cal.date)) {
            cal.setDate(date);
            newdate = true;
        } else if (el.navtype == 0) newdate = closing = true;
    }
    if (newdate) {
        ev && cal.callHandler();
    }
    if (closing) {
        Calendar.removeClass(el, "hilite");
        ev && cal.callCloseHandler();
    }
};
Calendar.prototype.create = function (_par) {
    var parent = null;
    if (!_par) {
        parent = document.getElementsByTagName("body")[0];
        this.isPopup = true;
    } else {
        parent = _par;
        this.isPopup = false;
    }
    this.date = this.dateStr ? new Date(this.dateStr) : new Date();
    var table = Calendar.createElement("table");
    this.table = table;
    table.cellSpacing = 0;
    table.cellPadding = 0;
    table.calendar = this;
    Calendar.addEvent(table, "mousedown", Calendar.tableMouseDown);
    var div = Calendar.createElement("div");
    this.element = div;
    div.className = "calendar";
    if (this.isPopup) {
        div.style.position = "absolute";
        div.style.display = "none";
    }
    div.appendChild(table);
    var thead = Calendar.createElement("thead", table);
    var cell = null;
    var row = null;
    var cal = this;
    var hh = function (text, cs, navtype, clsnm) {
        cell = Calendar.createElement("td", row);
        cell.colSpan = cs;
        cell.className = "button";
        if (navtype != 0 && Math.abs(navtype) <= 2) cell.className += " nav";
        if (clsnm) cell.className += " " + clsnm;
        Calendar._add_evs(cell);
        cell.calendar = cal;
        cell.navtype = navtype;
        cell.innerHTML = "<div unselectable='on'>" + text + "</div>";
        return cell;
    };
    row = Calendar.createElement("tr", thead);
    var title_length = 6;
    (this.isPopup) && --title_length;
    (this.weekNumbers) && ++title_length;
    hh("?", 1, 400, 'question').ttip = Calendar._TT["INFO"];
    this.title = hh("", title_length, 300);
    this.title.className = "title";
    if (this.isPopup) {
        this.title.ttip = Calendar._TT["DRAG_TO_MOVE"];
        this.title.style.cursor = "move";
        hh("&#x00d7;", 1, 200, 'close').ttip = Calendar._TT["CLOSE"];
    }
    row = Calendar.createElement("tr", thead);
    row.className = "headrow";
    this._nav_py = hh("&#x00ab;", 1, -2);
    this._nav_py.ttip = Calendar._TT["PREV_YEAR"];
    this._nav_pm = hh("&#x2039;", 1, -1);
    this._nav_pm.ttip = Calendar._TT["PREV_MONTH"];
    this._nav_now = hh(Calendar._TT["TODAY"], this.weekNumbers ? 4 : 3, 0);
    this._nav_now.ttip = Calendar._TT["GO_TODAY"];
    this._nav_nm = hh("&#x203a;", 1, 1);
    this._nav_nm.ttip = Calendar._TT["NEXT_MONTH"];
    this._nav_ny = hh("&#x00bb;", 1, 2);
    this._nav_ny.ttip = Calendar._TT["NEXT_YEAR"];
    row = Calendar.createElement("tr", thead);
    row.className = "daynames";
    if (this.weekNumbers) {
        cell = Calendar.createElement("td", row);
        cell.className = "name wn";
        cell.innerHTML = Calendar._TT["WK"];
    }
    for (var i = 7; i > 0; --i) {
        cell = Calendar.createElement("td", row);
        if (!i) {
            cell.navtype = 100;
            cell.calendar = this;
            Calendar._add_evs(cell);
        }
    }
    this.firstdayname = (this.weekNumbers) ? row.firstChild.nextSibling : row.firstChild;
    this._displayWeekdays();
    var tbody = Calendar.createElement("tbody", table);
    this.tbody = tbody;
    for (i = 6; i > 0; --i) {
        row = Calendar.createElement("tr", tbody);
        if (this.weekNumbers) {
            cell = Calendar.createElement("td", row);
        }
        for (var j = 7; j > 0; --j) {
            cell = Calendar.createElement("td", row);
            cell.calendar = this;
            Calendar._add_evs(cell);
        }
    }
    if (this.showsTime) {
        row = Calendar.createElement("tr", tbody);
        row.className = "time";
        cell = Calendar.createElement("td", row);
        cell.className = "time";
        cell.colSpan = 2;
        cell.innerHTML = Calendar._TT["TIME"] || "&nbsp;";
        cell = Calendar.createElement("td", row);
        cell.className = "time";
        cell.colSpan = this.weekNumbers ? 4 : 3;
        (function () {
            function makeTimePart(className, init, range_start, range_end) {
                var part = Calendar.createElement("span", cell);
                part.className = className;
                part.innerHTML = init;
                part.calendar = cal;
                part.ttip = Calendar._TT["TIME_PART"];
                part.navtype = 50;
                part._range = [];
                if (typeof range_start != "number") part._range = range_start;
                else {
                    for (var i = range_start; i <= range_end; ++i) {
                        var txt;
                        if (i < 10 && range_end >= 10) txt = '0' + i;
                        else txt = '' + i;
                        part._range[part._range.length] = txt;
                    }
                }
                Calendar._add_evs(part);
                return part;
            };
            var hrs = cal.date.getHours();
            var mins = cal.date.getMinutes();
            var t12 = !cal.time24;
            var pm = (hrs > 12);
            if (t12 && pm) hrs -= 12;
            var H = makeTimePart("hour", hrs, t12 ? 1 : 0, t12 ? 12 : 23);
            var span = Calendar.createElement("span", cell);
            span.innerHTML = ":";
            span.className = "colon";
            var M = makeTimePart("minute", mins, 0, 59);
            var AP = null;
            cell = Calendar.createElement("td", row);
            cell.className = "time";
            cell.colSpan = 2;
            if (t12) AP = makeTimePart("ampm", pm ? "pm" : "am", ["am", "pm"]);
            else cell.innerHTML = "&nbsp;";
            cal.onSetTime = function () {
                var pm, hrs = this.date.getHours(),
                    mins = this.date.getMinutes();
                if (t12) {
                    pm = (hrs >= 12);
                    if (pm) hrs -= 12;
                    if (hrs == 0) hrs = 12;
                    AP.innerHTML = pm ? "pm" : "am";
                }
                H.innerHTML = (hrs < 10) ? ("0" + hrs) : hrs;
                M.innerHTML = (mins < 10) ? ("0" + mins) : mins;
            };
            cal.onUpdateTime = function () {
                var date = this.date;
                var h = parseInt(H.innerHTML, 10);
                if (t12) {
                    if (/pm/i.test(AP.innerHTML) && h < 12) h += 12;
                    else if (/am/i.test(AP.innerHTML) && h == 12) h = 0;
                }
                var d = date.getDate();
                var m = date.getMonth();
                var y = date.getFullYear();
                date.setHours(h);
                date.setMinutes(parseInt(M.innerHTML, 10));
                date.setFullYear(y);
                date.setMonth(m);
                date.setDate(d);
                this.dateClicked = false;
                this.callHandler();
            };
        })();
    } else {
        this.onSetTime = this.onUpdateTime = function () {};
    }
    var tfoot = Calendar.createElement("tfoot", table);
    row = Calendar.createElement("tr", tfoot);
    row.className = "footrow";
    cell = hh(Calendar._TT["SEL_DATE"], this.weekNumbers ? 8 : 7, 300);
    cell.className = "ttip";
    if (this.isPopup) {
        cell.ttip = Calendar._TT["DRAG_TO_MOVE"];
        cell.style.cursor = "move";
    }
    this.tooltips = cell;
    div = Calendar.createElement("div", this.element);
    this.monthsCombo = div;
    div.className = "combo";
    for (i = 0; i < Calendar._MN.length; ++i) {
        var mn = Calendar.createElement("div");
        mn.className = Calendar.is_ie ? "label-IEfix" : "label";
        mn.month = i;
        mn.innerHTML = Calendar._SMN[i];
        div.appendChild(mn);
    }
    div = Calendar.createElement("div", this.element);
    this.yearsCombo = div;
    div.className = "combo";
    for (i = 12; i > 0; --i) {
        var yr = Calendar.createElement("div");
        yr.className = Calendar.is_ie ? "label-IEfix" : "label";
        div.appendChild(yr);
    }
    this._init(this.firstDayOfWeek, this.date);
    parent.appendChild(this.element);
};
Calendar.prototype._init = function (firstDayOfWeek, date) {
    var today = new Date(),
        TY = today.getFullYear(),
        TM = today.getMonth(),
        TD = today.getDate();
    this.table.style.visibility = "hidden";
    var year = date.getFullYear();
    if (year < this.minYear) {
        year = this.minYear;
        date.setFullYear(year);
    } else if (year > this.maxYear) {
        year = this.maxYear;
        date.setFullYear(year);
    }
    this.firstDayOfWeek = firstDayOfWeek;
    this.date = new Date(date);
    var month = date.getMonth();
    var mday = date.getDate();
    var no_days = date.getMonthDays();
    date.setDate(1);
    var day1 = (date.getDay() - this.firstDayOfWeek) % 7;
    if (day1 < 0) day1 += 7;
    if (day1 < 0) {
        day1 = Math.abs(day1);
    } else {
        if (day1 > 0) day1 = 0 - day1;
    }
    date.setDate(day1);
    date.setDate(date.getDate() + 1);
    var row = this.tbody.firstChild;
    var MN = Calendar._SMN[month];
    var ar_days = this.ar_days = new Array();
    var weekend = Calendar._TT["WEEKEND"];
    var dates = this.multiple ? (this.datesCells = {}) : null;
    for (var i = 0; i < 6; ++i, row = row.nextSibling) {
        var cell = row.firstChild;
        if (this.weekNumbers) {
            cell.className = "day wn";
            cell.innerHTML = date.getWeekNumber();
            cell = cell.nextSibling;
        }
        row.className = "daysrow";
        var hasdays = false,
            iday, dpos = ar_days[i] = [];
        for (var j = 0; j < 7; ++j, cell = cell.nextSibling, date.setDate(iday + 1)) {
            iday = date.getDate();
            var wday = date.getDay();
            cell.className = "day";
            cell.pos = i << 4 | j;
            dpos[j] = cell;
            var current_month = (date.getMonth() == month);
            if (!current_month) {
                if (this.showsOtherMonths) {
                    cell.className += " othermonth";
                    cell.otherMonth = true;
                } else {
                    cell.className = "emptycell";
                    cell.innerHTML = "&nbsp;";
                    cell.disabled = true;
                    continue;
                }
            } else {
                cell.otherMonth = false;
                hasdays = true;
            }
            cell.disabled = false;
            cell.innerHTML = this.getDateText ? this.getDateText(date, iday) : iday;
            if (dates) dates[date.print("%Y%m%d")] = cell;
            if (this.getDateStatus) {
                var status = this.getDateStatus(date, year, month, iday);
                if (this.getDateToolTip) {
                    var toolTip = this.getDateToolTip(date, year, month, iday);
                    if (toolTip) cell.title = toolTip;
                }
                if (status === true) {
                    cell.className += " disabled";
                    cell.disabled = true;
                } else {
                    if (/disabled/i.test(status)) cell.disabled = true;
                    cell.className += " " + status;
                }
            }
            if (!cell.disabled) {
                cell.caldate = new Date(date);
                cell.ttip = "_";
                if (!this.multiple && current_month && iday == mday && this.hiliteToday) {
                    cell.className += " selected";
                    this.currentDateEl = cell;
                }
                if (date.getFullYear() == TY && date.getMonth() == TM && iday == TD) {
                    cell.className += " today";
                    cell.ttip += Calendar._TT["PART_TODAY"];
                }
                if (weekend.indexOf(wday.toString()) != -1) cell.className += cell.otherMonth ? " oweekend" : " weekend";
            }
        }
        if (!(hasdays || this.showsOtherMonths)) row.className = "emptyrow";
    }
    this.title.innerHTML = Calendar._MN[month] + " " + year;
    this.onSetTime();
    this.table.style.visibility = "visible";
    this._initMultipleDates();
};
Calendar.prototype._initMultipleDates = function () {
    if (this.multiple) {
        for (var i in this.multiple) {
            var cell = this.datesCells[i];
            var d = this.multiple[i];
            if (!d) continue;
            if (cell) cell.className += " selected";
        }
    }
};
Calendar.prototype._toggleMultipleDate = function (date) {
    if (this.multiple) {
        var ds = date.print("%Y%m%d");
        var cell = this.datesCells[ds];
        if (cell) {
            var d = this.multiple[ds];
            if (!d) {
                Calendar.addClass(cell, "selected");
                this.multiple[ds] = date;
            } else {
                Calendar.removeClass(cell, "selected");
                delete this.multiple[ds];
            }
        }
    }
};
Calendar.prototype.setDateToolTipHandler = function (unaryFunction) {
    this.getDateToolTip = unaryFunction;
};
Calendar.prototype.setDate = function (date) {
    if (!date.equalsTo(this.date)) {
        this._init(this.firstDayOfWeek, date);
    }
};
Calendar.prototype.refresh = function () {
    this._init(this.firstDayOfWeek, this.date);
};
Calendar.prototype.setFirstDayOfWeek = function (firstDayOfWeek) {
    this._init(firstDayOfWeek, this.date);
    this._displayWeekdays();
};
Calendar.prototype.setDateStatusHandler = Calendar.prototype.setDisabledHandler = function (unaryFunction) {
    this.getDateStatus = unaryFunction;
};
Calendar.prototype.setRange = function (a, z) {
    this.minYear = a;
    this.maxYear = z;
};
Calendar.prototype.callHandler = function () {
    if (this.onSelected) {
        this.onSelected(this, this.date.print(this.dateFormat));
    }
};
Calendar.prototype.callCloseHandler = function () {
    if (this.onClose) {
        this.onClose(this);
    }
    this.hideShowCovered();
};
Calendar.prototype.destroy = function () {
    var el = this.element.parentNode;
    el.removeChild(this.element);
    Calendar._C = null;
    window._dynarch_popupCalendar = null;
};
Calendar.prototype.reparent = function (new_parent) {
    var el = this.element;
    el.parentNode.removeChild(el);
    new_parent.appendChild(el);
};
Calendar._checkCalendar = function (ev) {
    var calendar = window._dynarch_popupCalendar;
    if (!calendar) {
        return false;
    }
    var el = Calendar.is_ie ? Calendar.getElement(ev) : Calendar.getTargetElement(ev);
    for (; el != null && el != calendar.element; el = el.parentNode);
    if (el == null) {
        window._dynarch_popupCalendar.callCloseHandler();
        return Calendar.stopEvent(ev);
    }
};
Calendar.prototype.show = function () {
    var rows = this.table.getElementsByTagName("tr");
    for (var i = rows.length; i > 0;) {
        var row = rows[--i];
        Calendar.removeClass(row, "rowhilite");
        var cells = row.getElementsByTagName("td");
        for (var j = cells.length; j > 0;) {
            var cell = cells[--j];
            Calendar.removeClass(cell, "hilite");
            Calendar.removeClass(cell, "active");
        }
    }
    this.element.style.display = "block";
    this.hidden = false;
    if (this.isPopup) {
        window._dynarch_popupCalendar = this;
        Calendar.addEvent(document, "mousedown", Calendar._checkCalendar);
    }
    this.hideShowCovered();
};
Calendar.prototype.hide = function () {
    if (this.isPopup) {
        Calendar.removeEvent(document, "mousedown", Calendar._checkCalendar);
    }
    this.element.style.display = "none";
    this.hidden = true;
    this.hideShowCovered();
};
Calendar.prototype.showAt = function (x, y) {
    var s = this.element.style;
    s.left = x + "px";
    s.top = y + "px";
    this.show();
};
Calendar.prototype.showAtElement = function (el, opts) {
    var self = this;
    var p = Calendar.getAbsolutePos(el);
    if (!opts || typeof opts != "string") {
        this.showAt(p.x, p.y + el.offsetHeight);
        return true;
    }

    function fixPosition(box) {
        if (box.x < 0) box.x = 0;
        if (box.y < 0) box.y = 0;
        var cp = document.createElement("div");
        var s = cp.style;
        s.position = "absolute";
        s.right = s.bottom = s.width = s.height = "0px";
        document.body.appendChild(cp);
        var br = Calendar.getAbsolutePos(cp);
        document.body.removeChild(cp);
        br.y += window.scrollY;
        br.x += window.scrollX;
        var tmp = box.x + box.width - br.x;
        if (tmp > 0) box.x -= tmp;
        tmp = box.y + box.height - br.y;
        if (tmp > 0) box.y -= tmp;
    };
    Calendar.continuation_for_khtml_browser = function () {
        var w = self.element.offsetWidth;
        var h = self.element.offsetHeight;
        self.element.style.display = "none";
        var valign = opts.substr(0, 1);
        var halign = "l";
        if (opts.length > 1) {
            halign = opts.substr(1, 1);
        }
        switch (valign) {
            case "T":
                p.y -= h;
                break;
            case "B":
                p.y += el.offsetHeight;
                break;
            case "C":
                p.y += (el.offsetHeight - h) / 2;
                break;
            case "t":
                p.y += el.offsetHeight - h;
                break;
            case "b":
                break;
        }
        switch (halign) {
            case "L":
                p.x -= w;
                break;
            case "R":
                p.x += el.offsetWidth;
                break;
            case "C":
                p.x += (el.offsetWidth - w) / 2;
                break;
            case "l":
                p.x += el.offsetWidth - w;
                break;
            case "r":
                break;
        }
        p.width = w;
        p.height = h + 40;
        self.monthsCombo.style.display = "none";
        fixPosition(p);
        self.showAt(p.x, p.y);
    };
    if (Calendar.is_khtml) setTimeout("Calendar.continuation_for_khtml_browser()", 10);
    else Calendar.continuation_for_khtml_browser();
};
Calendar.prototype.setDateFormat = function (str) {
    this.dateFormat = str;
};
Calendar.prototype.setTtDateFormat = function (str) {
    this.ttDateFormat = str;
};
Calendar.prototype.parseDate = function (str, fmt) {
    if (!fmt) fmt = this.dateFormat;
    this.setDate(Date.parseDate(str, fmt));
};
Calendar.prototype.hideShowCovered = function () {
    if (!Calendar.is_ie && !Calendar.is_opera) return;

    function getVisib(obj) {
        var value = obj.style.visibility;
        if (!value) {
            if (document.defaultView && typeof (document.defaultView.getComputedStyle) == "function") {
                if (!Calendar.is_khtml) value = document.defaultView.getComputedStyle(obj, "").getPropertyValue("visibility");
                else value = '';
            } else if (obj.currentStyle) {
                value = obj.currentStyle.visibility;
            } else value = '';
        }
        return value;
    };
    var tags = new Array("applet", "iframe", "select");
    var el = this.element;
    var p = Calendar.getAbsolutePos(el);
    var EX1 = p.x;
    var EX2 = el.offsetWidth + EX1;
    var EY1 = p.y;
    var EY2 = el.offsetHeight + EY1;
    for (var k = tags.length; k > 0;) {
        var ar = document.getElementsByTagName(tags[--k]);
        var cc = null;
        for (var i = ar.length; i > 0;) {
            cc = ar[--i];
            p = Calendar.getAbsolutePos(cc);
            var CX1 = p.x;
            var CX2 = cc.offsetWidth + CX1;
            var CY1 = p.y;
            var CY2 = cc.offsetHeight + CY1;
            if (this.hidden || (CX1 > EX2) || (CX2 < EX1) || (CY1 > EY2) || (CY2 < EY1)) {
                if (!cc.__msh_save_visibility) {
                    cc.__msh_save_visibility = getVisib(cc);
                }
                cc.style.visibility = cc.__msh_save_visibility;
            } else {
                if (!cc.__msh_save_visibility) {
                    cc.__msh_save_visibility = getVisib(cc);
                }
                cc.style.visibility = "hidden";
            }
        }
    }
};
Calendar.prototype._displayWeekdays = function () {
    var fdow = this.firstDayOfWeek;
    var cell = this.firstdayname;
    var weekend = Calendar._TT["WEEKEND"];
    for (var i = 0; i < 7; ++i) {
        cell.className = "day name";
        var realday = (i + fdow) % 7;
        if (i) {
            cell.ttip = Calendar._TT["DAY_FIRST"].replace("%s", Calendar._DN[realday]);
            cell.navtype = 100;
            cell.calendar = this;
            cell.fdow = realday;
            Calendar._add_evs(cell);
        }
        if (weekend.indexOf(realday.toString()) != -1) {
            Calendar.addClass(cell, "weekend");
        }
        cell.innerHTML = Calendar._SDN[(i + fdow) % 7];
        cell = cell.nextSibling;
    }
};
Calendar.prototype._hideCombos = function () {
    this.monthsCombo.style.display = "none";
    this.yearsCombo.style.display = "none";
};
Calendar.prototype._dragStart = function (ev) {
    if (this.dragging) {
        return;
    }
    this.dragging = true;
    var posX;
    var posY;
    if (Calendar.is_ie) {
        posY = window.event.clientY + document.body.scrollTop;
        posX = window.event.clientX + document.body.scrollLeft;
    } else {
        posY = ev.clientY + window.scrollY;
        posX = ev.clientX + window.scrollX;
    }
    var st = this.element.style;
    this.xOffs = posX - parseInt(st.left);
    this.yOffs = posY - parseInt(st.top);
    with(Calendar) {
        addEvent(document, "mousemove", calDragIt);
        addEvent(document, "mouseup", calDragEnd);
    }
};
Date._MD = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
Date.SECOND = 1000;
Date.MINUTE = 60 * Date.SECOND;
Date.HOUR = 60 * Date.MINUTE;
Date.DAY = 24 * Date.HOUR;
Date.WEEK = 7 * Date.DAY;
Date.parseDate = function (str, fmt) {
    var today = new Date();
    var y = 0;
    var m = -1;
    var d = 0;
    var a = str.split(/\W+/);
    var b = fmt.match(/%./g);
    var i = 0,
        j = 0;
    var hr = 0;
    var min = 0;
    for (i = 0; i < a.length; ++i) {
        if (!a[i]) continue;
        switch (b[i]) {
            case "%d":
            case "%e":
                d = parseInt(a[i], 10);
                break;
            case "%m":
                m = parseInt(a[i], 10) - 1;
                break;
            case "%Y":
            case "%y":
                y = parseInt(a[i], 10);
                (y < 100) && (y += (y > 29) ? 1900 : 2000);
                break;
            case "%b":
            case "%B":
                for (j = 0; j < 12; ++j) {
                    if (Calendar._MN[j].substr(0, a[i].length).toLowerCase() == a[i].toLowerCase()) {
                        m = j;
                        break;
                    }
                }
                break;
            case "%H":
            case "%I":
            case "%k":
            case "%l":
                hr = parseInt(a[i], 10);
                break;
            case "%P":
            case "%p":
                if (/pm/i.test(a[i]) && hr < 12) hr += 12;
                else if (/am/i.test(a[i]) && hr >= 12) hr -= 12;
                break;
            case "%M":
                min = parseInt(a[i], 10);
                break;
        }
    }
    if (isNaN(y)) y = today.getFullYear();
    if (isNaN(m)) m = today.getMonth();
    if (isNaN(d)) d = today.getDate();
    if (isNaN(hr)) hr = today.getHours();
    if (isNaN(min)) min = today.getMinutes();
    if (y != 0 && m != -1 && d != 0) return new Date(y, m, d, hr, min, 0);
    y = 0;
    m = -1;
    d = 0;
    for (i = 0; i < a.length; ++i) {
        if (a[i].search(/[a-zA-Z]+/) != -1) {
            var t = -1;
            for (j = 0; j < 12; ++j) {
                if (Calendar._MN[j].substr(0, a[i].length).toLowerCase() == a[i].toLowerCase()) {
                    t = j;
                    break;
                }
            }
            if (t != -1) {
                if (m != -1) {
                    d = m + 1;
                }
                m = t;
            }
        } else if (parseInt(a[i], 10) <= 12 && m == -1) {
            m = a[i] - 1;
        } else if (parseInt(a[i], 10) > 31 && y == 0) {
            y = parseInt(a[i], 10);
            (y < 100) && (y += (y > 29) ? 1900 : 2000);
        } else if (d == 0) {
            d = a[i];
        }
    }
    if (y == 0) y = today.getFullYear();
    if (m != -1 && d != 0) return new Date(y, m, d, hr, min, 0);
    return today;
};
Date.prototype.getMonthDays = function (month) {
    var year = this.getFullYear();
    if (typeof month == "undefined") {
        month = this.getMonth();
    }
    if (((0 == (year % 4)) && ((0 != (year % 100)) || (0 == (year % 400)))) && month == 1) {
        return 29;
    } else {
        return Date._MD[month];
    }
};
Date.prototype.getDayOfYear = function () {
    var now = new Date(this.getFullYear(), this.getMonth(), this.getDate(), 0, 0, 0);
    var then = new Date(this.getFullYear(), 0, 0, 0, 0, 0);
    var time = now - then;
    return Math.floor(time / Date.DAY);
};
Date.prototype.getWeekNumber = function () {
    var d = new Date(this.getFullYear(), this.getMonth(), this.getDate(), 0, 0, 0);
    var DoW = d.getDay();
    d.setDate(d.getDate() - (DoW + 6) % 7 + 3);
    var ms = d.valueOf();
    d.setMonth(0);
    d.setDate(4);
    return Math.round((ms - d.valueOf()) / (7 * 864e5)) + 1;
};
Date.prototype.equalsTo = function (date) {
    return ((this.getFullYear() == date.getFullYear()) && (this.getMonth() == date.getMonth()) && (this.getDate() == date.getDate()) && (this.getHours() == date.getHours()) && (this.getMinutes() == date.getMinutes()));
};
Date.prototype.setDateOnly = function (date) {
    var tmp = new Date(date);
    this.setDate(1);
    this.setFullYear(tmp.getFullYear());
    this.setMonth(tmp.getMonth());
    this.setDate(tmp.getDate());
};
Date.prototype.print = function (str) {
    var m = this.getMonth();
    var d = this.getDate();
    var y = this.getFullYear();
    var wn = this.getWeekNumber();
    var w = this.getDay();
    var s = {};
    var hr = this.getHours();
    var pm = (hr >= 12);
    var ir = (pm) ? (hr - 12) : hr;
    var dy = this.getDayOfYear();
    if (ir == 0) ir = 12;
    var min = this.getMinutes();
    var sec = this.getSeconds();
    s["%a"] = Calendar._SDN[w];
    s["%A"] = Calendar._DN[w];
    s["%b"] = Calendar._SMN[m];
    s["%B"] = Calendar._MN[m];
    s["%C"] = 1 + Math.floor(y / 100);
    s["%d"] = (d < 10) ? ("0" + d) : d;
    s["%e"] = d;
    s["%H"] = (hr < 10) ? ("0" + hr) : hr;
    s["%I"] = (ir < 10) ? ("0" + ir) : ir;
    s["%j"] = (dy < 100) ? ((dy < 10) ? ("00" + dy) : ("0" + dy)) : dy;
    s["%k"] = hr;
    s["%l"] = ir;
    s["%m"] = (m < 9) ? ("0" + (1 + m)) : (1 + m);
    s["%M"] = (min < 10) ? ("0" + min) : min;
    s["%n"] = "\n";
    s["%p"] = pm ? "PM" : "AM";
    s["%P"] = pm ? "pm" : "am";
    s["%s"] = Math.floor(this.getTime() / 1000);
    s["%S"] = (sec < 10) ? ("0" + sec) : sec;
    s["%t"] = "\t";
    s["%U"] = s["%W"] = s["%V"] = (wn < 10) ? ("0" + wn) : wn;
    s["%u"] = w + 1;
    s["%w"] = w;
    s["%y"] = ('' + y).substr(2, 2);
    s["%Y"] = y;
    s["%%"] = "%";
    var re = /%./g;
    if (!Calendar.is_ie5 && !Calendar.is_khtml) return str.replace(re, function (par) {
        return s[par] || par;
    });
    var a = str.match(re);
    for (var i = 0; i < a.length; i++) {
        var tmp = s[a[i]];
        if (tmp) {
            re = new RegExp(a[i], 'g');
            str = str.replace(re, tmp);
        }
    }
    return str;
};
Date.prototype.__msh_oldSetFullYear = Date.prototype.setFullYear;
Date.prototype.setFullYear = function (y) {
    var d = new Date(this);
    d.__msh_oldSetFullYear(y);
    if (d.getMonth() != this.getMonth()) this.setDate(28);
    this.__msh_oldSetFullYear(y);
};
window._dynarch_popupCalendar = null;

function selectDate(cal) {
    var p = cal.params;
    var update = (cal.dateClicked || p.electric);
    year = p.inputField.id;
    day = year + '-2';
    month = year + '-1';
    document.getElementById(month).value = cal.date.print('%m');
    document.getElementById(day).value = cal.date.print('%e');
    document.getElementById(year).value = cal.date.print('%Y');
}

function selectEuroDate(cal) {
    var p = cal.params;
    var update = (cal.dateClicked || p.electric);
    year = p.inputField.id;
    day = year + '-1';
    month = year + '-2';
    document.getElementById(month).value = cal.date.print('%m');
    document.getElementById(day).value = cal.date.print('%e');
    document.getElementById(year).value = cal.date.print('%Y');
}
Calendar._DN = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
Calendar._SDN = new Array("S", "M", "T", "W", "T", "F", "S", "S");
Calendar._FD = 0;
Calendar._MN = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
Calendar._SMN = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
Calendar._TT = {};
Calendar._TT["INFO"] = "About the Calendar";
Calendar._TT["ABOUT"] = "DHTML Date/Time Selector\n" + "(c) dynarch.com 2002-2005 / Author: Mihai Bazon\n" + "For latest version visit:\n http://www.dynarch.com/projects/calendar/\n" + "Distributed under GNU LGPL.\n See http://gnu.org/licenses/lgpl.html for details." + "\n\n" + "Date selection:\n" + "- Use the \xab, \xbb buttons to select year\n" + "- Use the " + String.fromCharCode(0x2039) + ", " + String.fromCharCode(0x203a) + " buttons to select month\n" + "- Hold mouse button for faster selection.";
Calendar._TT["ABOUT_TIME"] = "\n\n" + "Time selection:\n" + "- Click on any of the time parts to increase it\n" + "- or Shift-click to decrease it\n" + "- or click and drag for faster selection.";
Calendar._TT["PREV_YEAR"] = "Prev. Year (Hold for Menu)";
Calendar._TT["PREV_MONTH"] = "Prev. Month (Hold for Menu)";
Calendar._TT["GO_TODAY"] = "Go to Today";
Calendar._TT["NEXT_MONTH"] = "Next Month (Hold for Menu)";
Calendar._TT["NEXT_YEAR"] = "Next Year (Hold for Menu)";
Calendar._TT["SEL_DATE"] = "Select Date";
Calendar._TT["DRAG_TO_MOVE"] = "Drag to Move";
Calendar._TT["PART_TODAY"] = " (Today)";
Calendar._TT["DAY_FIRST"] = "Display %ss first";
Calendar._TT["WEEKEND"] = "0,6";
Calendar._TT["CLOSE"] = "Close Calendar";
Calendar._TT["TODAY"] = "Today";
Calendar._TT["TIME_PART"] = "(Shift-)Click or Drag to Change Value";
Calendar._TT["DEF_DATE_FORMAT"] = "%Y-%m-%d";
Calendar._TT["TT_DATE_FORMAT"] = "%b %e, %Y";
Calendar._TT["WK"] = "wk";
Calendar._TT["TIME"] = "Time:";
Calendar.setup = function (params) {
    function param_default(pname, def) {
        if (typeof params[pname] == "undefined") {
            params[pname] = def;
        }
    };
    param_default("inputField", null);
    param_default("displayArea", null);
    param_default("button", null);
    param_default("eventName", "click");
    param_default("ifFormat", "%Y/%m/%d");
    param_default("daFormat", "%Y/%m/%d");
    param_default("singleClick", true);
    param_default("disableFunc", null);
    param_default("dateStatusFunc", params["disableFunc"]);
    param_default("dateText", null);
    param_default("firstDay", null);
    param_default("align", "BR");
    param_default("range", [1900, 2999]);
    param_default("weekNumbers", false);
    param_default("flat", null);
    param_default("flatCallback", null);
    param_default("onSelect", null);
    param_default("onClose", null);
    param_default("onUpdate", null);
    param_default("date", null);
    param_default("showsTime", false);
    param_default("timeFormat", "24");
    param_default("electric", true);
    param_default("step", 2);
    param_default("position", null);
    param_default("cache", false);
    param_default("showOthers", false);
    param_default("multiple", null);
    var tmp = ["inputField", "displayArea", "button"];
    for (var i in tmp) {
        if (typeof params[tmp[i]] == "string") {
            params[tmp[i]] = document.getElementById(params[tmp[i]]);
        }
    }
    if (!(params.flat || params.multiple || params.inputField || params.displayArea || params.button)) {
        alert("Calendar.setup:\n  Nothing to setup (no fields found).  Please check your code");
        return false;
    }

    function onSelect(cal) {
        var p = cal.params;
        var update = (cal.dateClicked || p.electric);
        if (update && p.inputField) {
            p.inputField.value = cal.date.print(p.ifFormat);
            if (typeof p.inputField.onchange == "function") p.inputField.onchange();
        }
        if (update && p.displayArea) p.displayArea.innerHTML = cal.date.print(p.daFormat);
        if (update && typeof p.onUpdate == "function") p.onUpdate(cal);
        if (update && p.flat) {
            if (typeof p.flatCallback == "function") p.flatCallback(cal);
        }
        if (update && p.singleClick && cal.dateClicked) cal.callCloseHandler();
    };
    if (params.flat != null) {
        if (typeof params.flat == "string") params.flat = document.getElementById(params.flat);
        if (!params.flat) {
            alert("Calendar.setup:\n  Flat specified but can't find parent.");
            return false;
        }
        var cal = new Calendar(params.firstDay, params.date, params.onSelect || onSelect);
        cal.showsOtherMonths = params.showOthers;
        cal.showsTime = params.showsTime;
        cal.time24 = (params.timeFormat == "24");
        cal.params = params;
        cal.weekNumbers = params.weekNumbers;
        cal.setRange(params.range[0], params.range[1]);
        cal.setDateStatusHandler(params.dateStatusFunc);
        cal.getDateText = params.dateText;
        if (params.ifFormat) {
            cal.setDateFormat(params.ifFormat);
        }
        if (params.inputField && typeof params.inputField.value == "string") {
            cal.parseDate(params.inputField.value);
        }
        cal.create(params.flat);
        cal.show();
        return false;
    }
    var triggerEl = params.button || params.displayArea || params.inputField;
    triggerEl["on" + params.eventName] = function () {
        var dateEl = params.inputField || params.displayArea;
        var dateFmt = params.inputField ? params.ifFormat : params.daFormat;
        var mustCreate = false;
        var cal = window.calendar;
        if (dateEl) params.date = Date.parseDate(dateEl.value || dateEl.innerHTML, dateFmt);
        if (!(cal && params.cache)) {
            window.calendar = cal = new Calendar(params.firstDay, params.date, params.onSelect || onSelect, params.onClose || function (cal) {
                cal.hide();
            });
            cal.showsTime = params.showsTime;
            cal.time24 = (params.timeFormat == "24");
            cal.weekNumbers = params.weekNumbers;
            mustCreate = true;
        } else {
            if (params.date) cal.setDate(params.date);
            cal.hide();
        }
        if (params.multiple) {
            cal.multiple = {};
            for (var i = params.multiple.length; --i >= 0;) {
                var d = params.multiple[i];
                var ds = d.print("%Y%m%d");
                cal.multiple[ds] = d;
            }
        }
        cal.showsOtherMonths = params.showOthers;
        cal.yearStep = params.step;
        cal.setRange(params.range[0], params.range[1]);
        cal.params = params;
        cal.setDateStatusHandler(params.dateStatusFunc);
        cal.getDateText = params.dateText;
        cal.setDateFormat(dateFmt);
        if (mustCreate) cal.create();
        cal.refresh();
        if (!params.position) cal.showAtElement(params.button || params.displayArea || params.inputField, params.align);
        else cal.showAt(params.position[0], params.position[1]);
        return false;
    };
    return cal;
};