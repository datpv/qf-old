var LogsObject = Class.create({
    initialize: function (Logs) {
        this.data = Logs;
    },
    getLog: function (log_id) {
        if (this.data[log_id]) return this.data[log_id];
        else return false;
    },
    getLogComments: function(log_id) {
        if (this.data[log_id]) return this.data[log_id]['log_comments'];
        else return false;
    },
    getLogComment: function(log_id, comment_id) {
        var comment = false;
        this.data[log_id]['log_comments'].each(function(k,v) {
            if(k.comment_id == comment_id) {
                comment = k;
                return;
            }
        });
        return comment;
    },
    updateLogComments: function(log_id, comment_json) {
        var obj2 = [];
        var obj1 = this.data[log_id]['log_comments'];
        obj1.each(function(k, v) {
            obj2.push(k);
        });
        comment_json.each(function(k, v) {
            obj2.push(k);
        });
        this.data[log_id]['log_comments'] = obj2;
    },
    updateLogsJson: function (log) {
        var obj = [];
        for(var key in log) {
            this.data[key] = {
                'log_id' : log[key].log_id,
                'log_value' : log[key].log_value,
                'log_comment' : []
            };
        }
    }
});

var ReviewEngineObject = Class.create({
    commentForm: '',
    wrongAForm: '',
    Logs: '',
    initialize: function () {
        this.commentForm = $('comment_form_container').select('.comment_wrap')[0];
        this.wrongAForm = $('comment_form_container').select('.wronga_wrap')[0];
        this.commentForm.remove();
        this.wrongAForm.remove();
        this.Logs = new LogsObject(__LOGS);
    },
    addDelete: function() {
        var del_buttons = $$('.del_wa');
        del_buttons.each(function(del_button) {
            del_button.observe('click', function(event) {
                deleteLog(del_button);
                event.stop();
            });
        })
        
    },
    addComments: function() {
        var comment_buttons = $$('.add_comment');
        var comment_form = this.commentForm;
        comment_buttons.each(function(comment_button) {
            comment_button.observe('click', function(event) {
                $$('.comments .comment_wrap').invoke('remove');
                comment_button.up().next('.comments').insert(comment_form);
                event.stop();
            });
        })
        
    },
    addWrongA: function() {
        var wronga_buttons = $$('.add_wa');
        var wronga_form = this.wrongAForm;
        wronga_buttons.each(function(wronga_button) {
            wronga_button.observe('click', function(event) {
                $$('#comments li h4 .wronga .wronga_wrap').invoke('remove');
                this.up().down('.wronga').insert(wronga_form);
                event.stop();
            });
        })  
    },
    addComment: function(ctrl) {
        ctrl.addClassName('hide');
        var comment_box = ctrl.previous();
        var comment_text = comment_box.value;
        if(comment_text.length <= 0) {
            ctrl.removeClassName('hide');
            return false;
        }
        var log = ctrl.up('li');
        var log_id = this.getLogId(log);
        if(!log_id) {
            ctrl.removeClassName('hide');
            return false;
        }
        var pars='action=addComment&log_id='+encodeURIComponent(log_id)+'&commentText='+encodeURIComponent(comment_text);
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success) {
                    log.select('.comments ul')[0].insert(result.response.html);
                    comment_box.value = '';
                    updateCommentsJson(log_id, result.response.json);
                    ctrl.removeClassName('hide');
                }
                
            }
        });
    },
    
    loadComments: function(ctrl) {
        ctrl.addClassName('hide');
        var log = ctrl.up('li');
        var log_id = this.getLogId(log);
        if(!log_id) {
            ctrl.removeClassName('hide');
            return;
        }
        var comment_ids = this.getLogCommentsIds(log);
        var pars='action=loadComments&log_id='+encodeURIComponent(log_id);
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: {
                'action' : 'loadComments',
                'log_id' : encodeURIComponent(log_id),
                'comment_ids[]' : comment_ids
            },
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success) {
                    log.select('.comments ul')[0].insert(result.response.html);
                    if(!result.response.over) log.select('.view_comments')[0].remove();
                    else {
                        log.select('.view_comments a b')[0].innerHTML = '(' + result.response.over + ')';
                    }
                    updateCommentsJson(log_id, result.response.json);
                    ctrl.removeClassName('hide');
                }
            }
        });
    },
    getCommentId: function(log_id, comment) {
        if(!log_id) return false;
        var comment_id = comment.id.replace('comment_','');
        var comment = this.Logs.getLogComment(log_id, comment_id);
        if(comment) return comment.comment_id;
        return false;
    },
    getLogId: function(log) {
        if(!log) return false;
        var log_id = log.id.replace('log_','');
        if(!this.Logs.getLog(log_id)) return false;
        return log_id;
    },
    getLogCommentsIds: function(log) {
        if(!log) return false;
        var log_id = log.id.replace('log_','');
        var comments = this.Logs.getLogComments(log_id);
        var comment_ids = [];
        
        comments.each(function(k, v) {
            comment_ids.push(k.comment_id);
        });
        return comment_ids;
    },
    deleteComment: function(ctrl) {
        var comfirm = window.confirm('Ok?');
        if(!comfirm) return;
        var comment = ctrl.up('li');
        var log = ctrl.up('li').up('li');
        ctrl.addClassName('hide');
        var log_id = this.getLogId(log);
        var comment_id = this.getCommentId(log_id, comment);
        if(!comment_id || !log_id) {
            ctrl.removeClassName('hide');
            return;
        }
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: {
                'action' : 'deleteComment',
                'log_id' : encodeURIComponent(log_id),
                'comment_id' : encodeURIComponent(comment_id)
            },
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success) {
                    comment.remove();
                } else {
                    ctrl.removeClassName('hide');
                }
            }
        });
    },
    addWa: function(ctrl) {
        ctrl.addClassName('hide');
        var field = ctrl.up('li');
        var field_id = field.id.replace('field_','');
        var field_box = ctrl.previous();
        var fieldValue = field_box.value;
        var fieldTitle = field.down('.fieldTitle').innerHTML;
        if(fieldValue.length == 0) {
            ctrl.removeClassName('hide');
            return;
        }
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: {
                'action' : 'addWa',
                'fieldId' : encodeURIComponent(field_id),
                'fieldValue' : (fieldValue),
                'fieldTitle' : (fieldTitle)
            },
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success) {
                    field_box.value = '';
                    if(!result.update) {
                        field.select('.child')[0].insert(result.response.html);
                        addActions();
                        updateLogsJson(result.response.json);
                    } else {
                        field.down('#log_'+result.update).down('.hits').innerHTML = ' ( ' + result.update_hits + ' Hits )';
                    }
                    ctrl.removeClassName('hide');
                }
            }
        });
    },
    deleteLog: function(ctrl) {
        var comfirm = window.confirm('Ok?');
        if(!comfirm) return;
        var log = ctrl.up('li');
        ctrl.addClassName('hide');
        var log_id = this.getLogId(log);
        if(!log_id) {
            ctrl.removeClassName('hide');
            return;
        }
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: {
                'action' : 'deleteLog',
                'log_id' : encodeURIComponent(log_id)
            },
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success) {
                    log.remove();
                } else {
                    ctrl.removeClassName('hide');
                }
            }
        });
    }
});
var __REVIEWER;
Event.observe(window, 'load', init, false);
function init() {
    __REVIEWER = new ReviewEngineObject();
    addActions();
}
function addActions() {
    addComments();
    addWrongA();
    addDelete();
}
function addDelete() {
    __REVIEWER.addDelete();
}
function addComments() {
    __REVIEWER.addComments();
}
function addWrongA() {
    __REVIEWER.addWrongA();
}
function deleteLog(ctrl) {
    __REVIEWER.deleteLog(ctrl);
}
function addComment(ctrl) {
    __REVIEWER.addComment(ctrl);
}
function loadComments(ctrl) {
    __REVIEWER.loadComments(ctrl);
}
function updateCommentsJson(log_id,comment_json) {
    __REVIEWER.Logs.updateLogComments(log_id, comment_json);
}
function updateLogsJson(log) {
    __REVIEWER.Logs.updateLogsJson(log);
}
function addWa(ctrl) {
    __REVIEWER.addWa(ctrl);
}
function deleteComment(ctrl) {
    __REVIEWER.deleteComment(ctrl);
}