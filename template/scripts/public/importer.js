var DragDropObject = Class.create({
    dragging: false,
    dragpos: -1,
    columns: new Array('col1'),
    initialize: function () {
        Sortable.SERIALIZE_RULE = /^[^_\-][foli](.*)$/;
        this.grantFieldDragAbility();
        for (var i = 0; i < this.columns.length; i++) {
            this.grantMenuDragAbility(this.columns[i]);
        }
    },
    grantFieldDragAbility: function () {
        Sortable.create("formFields", {
            dropOnEmpty: false,
            containment: ["formFields", "col1"],
            constraint: "vertical",
            scroll: window,
            only: 'dragable',
            onUpdate: reorderField
        });
    },
    grantMenuDragAbility: function (col) {
        Sortable.create(col, {
            dropOnEmpty: false,
            containment: [],
            constraint: false,
            scroll: window,
            onUpdate: addFieldAfterDraggingComplete
        });
    },
    returnButtonToAddFieldMenu: function (dragDropBox) {
        var menu = this.findMenuButtonBelongsTo(dragDropBox);
        dragDropBox.removeClassName('hide');
        var elPos = dragDropBox.className.replace("dragfld ", "").replace(" bigger", "").replace(" biggest", "").replace(" wide", "");
        if (menu.down('li', elPos)) {
            menu.down('li', elPos).insert({
                before: dragDropBox
            });
        } else menu.appendChild(dragDropBox);
    },
    findMenuButtonBelongsTo: function (button) {
        if (button.id && button.id.startsWith('drag1')) return $('col1');
        else if (button.id && button.id.startsWith('drag2')) return $('col2');
        else if (button.id && button.id.startsWith('drag3')) return $('col3');
        else return $('col4');
    }
});
var ReorderObject = Class.create({
    Form: '',
    FieldProperties: '',
    FieldPreview: '',
    FormEngine: '',
    position: 0,
    displayPosition: 0,
    currentPage: 1,
    needsRefresh: false,
    initialize: function (Form, FieldPreview, FieldProperties, FormEngine) {
        this.Form = Form;
        this.FieldProperties = FieldProperties;
        this.FieldPreview = FieldPreview;
        this.FormEngine = FormEngine;
    },
    updatePositionsByHtml: function (activeField) {
        var fieldList = this.prepFieldList();
        fieldList.each(function (item, index) {
            if (this.isField(item)) {
                var Field = this.Form.selectFieldByColumnId(item.id.replace('foli', ''));
                if (Field) {
                    this.handlePageReorder(Field);
                    this.updatePosition(Field, activeField);
                    this.updateDisplayPosition(Field);
                    this.updateCurrentPage(Field);
                    this.FieldPreview.updateAfterReorder(Field, this.Form);
                }
            }
        }.bind(this));
        var ret = this.needsRefresh;
        this.resetCounters();
        return ret;
    },
    findInsertPositionByHtml: function (insertAfterElement) {
        var ret = 0;
        if (insertAfterElement == 'top') ret = 0;
        else if ($(insertAfterElement)) {
            ret = this.loopThroughFieldsToFindPos(insertAfterElement);
        } else {
            ret = this.Form.FormJson.Fields.length;
            if (this.Form.FormJson.PageCount > 1) ret = ret - 1;
        }
        return ret;
    },
    loopThroughFieldsToFindPos: function (insertAfterElement) {
        var ret = 0;
        var fieldList = this.prepFieldList();
        fieldList.each(function (item, index) {
            if (this.isField(item)) {
                ret++;
                if (item == $(insertAfterElement)) throw $break;
            }
        }.bind(this));
        return ret;
    },
    prepFieldList: function () {
        var allFields = $('formFields');
        allFields.cleanWhitespace();
        return $A(allFields.childNodes);
    },
    isField: function (item) {
        var ret = false;
        if (item.id.startsWith('foli')) ret = true;
        return ret;
    },
    updatePosition: function (Field, activeField) {
        Field.Pos = this.position;
        if (activeField == Field.ColumnId) this.FieldProperties.position(Field.Pos);
        this.position++;
    },
    updateDisplayPosition: function (Field) {
        if (Field.Typeof != 'section' && Field.Typeof != 'page') {
            Field.DisplayPos = this.displayPosition;
            this.displayPosition++;
        }
    },
    updateCurrentPage: function (Field) {
        Field.Page = this.currentPage;
        if (Field.Typeof == 'page') this.currentPage += 1;
    },
    handlePageReorder: function (Field) {
        if (Field.Typeof == 'page' && Field.Page != this.currentPage) {
            this.FormEngine.removeField(this.Form, Field);
            this.FormEngine.addField(this.Form, Field, this.position);
            this.needsRefresh = true;
        }
    },
    resetCounters: function () {
        this.position = 0;
        this.displayPosition = 0;
        this.currentPage = 1;
        this.needsRefresh = false;
    }
});
var BulkAddObject = Class.create({ //<sl:translate>
    pre_gender: new Array('Male', 'Female', 'Prefer Not to Answer'),
    pre_age: new Array('Under 18', '18-24', '25-34', '35-44', '45-54', '55-64', '65 or Above', 'Prefer Not to Answer'),
    pre_employment: new Array('Employed Full-Time', 'Employed Part-Time', 'Self-employed', 'Not employed, but looking for work', 'Not employed and not looking for work', 'Homemaker', 'Retired', 'Student', 'Prefer Not to Answer'),
    pre_income: new Array('Under $20,000', '$20,000 - $30,000', '$30,000 - $40,000', '$40,000 - $50,000', '$50,000 - $75,000', '$75,000 - $100,000', '$100,000 - $150,000', '$150,000 or more', 'Prefer Not to Answer'),
    pre_education: new Array('Some High School', 'High School Graduate or Equivalent', 'Trade or Vocational Degree', 'Some College', 'Associate Degree', 'Bachelor\'s Degree', 'Graduate of Professional Degree', 'Prefer Not to Answer'),
    pre_marital: new Array('Single, Never Married', 'Married', 'Living with Partner', 'Separated', 'Divorced', 'Widowed', 'Prefer Not to Answer'),
    pre_race: new Array('White / Caucasian', 'Spanish / Hispanic / Latino', 'Black / African American', 'Asian', 'Pacific Islander', 'Native American', 'Other', 'Prefer Not to Answer'),
    pre_timezones: new Array('(-12:00) International Date Line', '(-11:00) Midway Island, Samoa', '(-10:00) Hawaii', '(-9:00) Alaska', '(-8:00) Pacific', '(-7:00) Mountain', '(-6:00) Central', '(-5:00) Eastern', '(-4:30) Venezuela', '(-4:00) Atlantic', '(-3:30) Newfoundland', '(-3:00) Brasilia, Buenos Aires, Georgetown', '(-2:00) Mid-Atlantic', '(-1:00) Azores, Cape Verde Isles', '(0:00) Dublin, London, Monrovia, Casablanca', '(+1:00) Paris, Madrid, Athens, Berlin, Rome', '(+2:00) Eastern Europe, Harare, Pretoria, Israel', '(+3:00) Moscow, Baghdad, Kuwait, Nairobi', '(+3:30) Tehran', '(+4:00) Abu Dhabi, Muscat, Tbilisi, Kazan', '(+4:30) Kabul', '(+5:00) Islamabad, Karachi, Ekaterinburg', '(+5:30) Bombay, Calcutta, Madras, New Delhi', '(+6:00) Almaty, Dhaka', '(+7:00) Bangkok, Jakarta, Hanoi', '(+8:00) Beijing, Hong Kong, Perth, Singapore', '(+9:00) Tokyo, Osaka, Sapporo, Seoul, Yakutsk', '(+9:30) Adelaide, Darwin', '(+10:00) Sydney, Guam, Port Moresby, Vladivostok', '(+11:00) Magadan, Solomon Isles, New Caledonia', '(+12:00) Fiji, Marshall Isles, Wellington, Auckland'),
    pre_months: new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'),
    pre_days: new Array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'),
    pre_states: new Array('Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'),
    pre_continents: new Array('Africa', 'Antarctica', 'Asia', 'Australia', 'Europe', 'North America', 'South America'),
    pre_howoften: new Array('Everyday', 'Once a week', '2 to 3 times a month', 'Once a month', 'Less than once a month'),
    pre_howlong: new Array('Less than a month', '1-6 months', '1-3 years', 'Over 3 Years', 'Never used'),
    pre_satisfied: new Array('Very Satisfied', 'Satisfied', 'Neutral', 'Unsatisfied', 'Very Unsatisfied', 'N/A'),
    pre_important: new Array('Very Important', 'Important', 'Neutral', 'Somewhat Important', 'Not at all Important', 'N/A'),
    pre_agree: new Array('Strongly Agree', 'Agree', 'Neutral', 'Disagree', 'Strongly Disagree', 'N/A'),
    pre_compare: new Array('Much Better', 'Somewhat Better', 'About the Same', 'Somewhat Worse', 'Much Worse', 'Don\'t Know'),
    pre_wouldyou: new Array('Definitely', 'Probably', 'Not Sure', 'Probably Not', 'Definitely Not'),
    pre_agree2: new Array('Strongly Disagree', 'Disagree', 'Neutral', 'Agree', 'Strongly Agree'),
    pre_compare2: new Array('Much Worse', 'Somewhat Worse', 'About the Same', 'Somewhat Better', 'Much Better'),
    pre_important2: new Array('Not at all Important', 'Somewhat Important', 'Neutral', 'Important', 'Very Important'),
    pre_satisfied2: new Array('Very Unsatisfied', 'Unsatisfied', 'Neutral', 'Satisfied', 'Very Satisfied'),
    pre_wouldyou2: new Array('Definitely Not', 'Probably Not', 'Not Sure', 'Probably', 'Definitely'),
    pre_ten: new Array('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'),
    pre_football: new Array('Arizona Cardinals', 'Atlanta Falcons', 'Baltimore Ravens', 'Buffalo Bills', 'Carolina Panthers', 'Chicago Bears', 'Cincinnati Bengals', 'Cleveland Browns', 'Dallas Cowboys', 'Denver Broncos', 'Detroit Lions', 'Green Bay Packers', 'Houston Texans', 'Indianapolis Colts', 'Jacksonville Jaguars', 'Kansas City Chiefs', 'Miami Dolphins', 'Minnesota Vikings', 'New England Patriots', 'New Orleans Saints', 'New York Giants', 'New York Jets', 'Oakland Raiders', 'Philadelphia Eagles', 'Pittsburgh Steelers', 'San Diego Chargers', 'San Francisco 49ers', 'Seattle Seahawks', 'St. Louis Rams', 'Tampa Bay Buccaneers', 'Tennessee Titans', 'Washington Redskins'), //</sl:translate>
    initialize: function () {},
    getSet: function (category) {
        return this[category].join('\n');
    }
});
var __BULKADD;

function initBulkAdd() {
    __BULKADD = new BulkAddObject();
}

function getBulkSet(category) {
    return __BULKADD.getSet(category);
}
var TabInterface = Class.create({
    activeAnimation: false,
    initialize: function () {},
    firstTab: function () {
        $('stage').className = "afi";
    },
    secondTab: function () {
        $('stage').className = "cfi";
    },
    thirdTab: function () {
        $('stage').className = "cfo";
    },
    showSidebar: function (name) {
        this.hideSidebars();
        this.toggleDisplay(name, 'block');
    },
    hideSidebars: function () {
        this.toggleDisplay('addFields', 'none');
        this.toggleDisplay('fieldProperties', 'none');
        this.toggleDisplay('formProperties', 'none');
    },
    toggleDisplay: function (element, visible) {
        $(element).style.display = visible;
    },
    showFields: function (pulse) {
        if (pulse == true && $('addFields').style.display == 'block' && !this.activeAnimation) {
            this.activeAnimation = true;
            if (Prototype.Browser.IE) {
                new Effect.Highlight('addFields', {
                    startcolor: '#FFF7C0',
                    endcolor: '#FDFCE9',
                    afterFinish: function () {
                        this.activeAnimation = false;
                    }.bind(this)
                });
            } else {
                new Effect.Shake('shake', 3, {
                    afterFinish: function () {
                        this.activeAnimation = false;
                    }.bind(this)
                });
                setTimeout(function () {
                    this.activeAnimation = false
                }.bind(this), 500);
            }
        } else {
            this.showSidebar('addFields');
            this.firstTab();
        }
    },
    showFieldProperties: function (columnId) {
        this.showSidebar('fieldProperties');
        this.secondTab();
        if (!columnId || columnId < 0) {
            $('noFieldSelected').style.display = 'block';
            $('fieldPos').style.display = 'none';
            $('allProps').style.display = 'none';
        }
    }
});
var FormPropertiesObject = Class.create({ //<sl:translate>
    noEmailsOptionText: 'No Email Fields Found', //</sl:translate>
    initialize: function () {
    },
    populate: function (Form, Profile) {
        $('formProperties').removeClassName('paging');
        this.Profile = Profile;
        this.Form = Form;
        this.name();
        this.description();
    },
    populateSingle: function (Form, Profile, attribute) {
        this.Profile = Profile;
        this.Form = Form;
        this[attribute]();
    },
    name: function () {
        $('formName').value = this.Form.FormJson.Name;
    },
    description: function () {
        var desc = this.Form.FormJson.Description;
        if (desc != '' && desc != null) desc = desc.replace(/\<br \/\>/g, "\n");
        $('formDescription').value = desc;
    },
    captcha: function () {
        cap = $('formCaptcha');
        cap.selectedIndex = 1;
        for (var i = 0; i < cap.options.length; i++) {
            if (this.Form.FormJson.UseCaptcha == cap.options[i].value) {
                cap.selectedIndex = i;
            }
        }
    },
    dropdownSelection: function (dropdown, value) {
        var drop = $(dropdown);
        for (var i = 0; i < drop.length; i++) {
            if (drop.options[i].value == value || '0' + drop.options[i].value == value) {
                drop.selectedIndex = i;
            }
        }
    }
});
var FieldPropertiesObject = Class.create({
    show: '',
    Form: '', //<sl:translate>
    fieldTypeCanNoLongerBeSavedWarning: 'Since this field has already been saved to your account, the field type can no longer be changed.',
    fieldOnceEncryptedWarning: 'Once a field has been encrypted it cannot be changed.',
    radioMarkupText: '<li><input name="rightAnswered" class="" type="checkbox" onclick="changeRightDefault(this)" #{selectedRightAnswered} title="Make this choice right answered." /><input name="allChoices" class="" type="radio" onclick="changeRadioDefault(this)" #{selectedAttribute} title="Make this choice pre-selected." /> <input class="text" type="text" maxlength="150" autocomplete="off" value="#{choice}" onkeyup="updateChoice(this);" /> <img src="template/images/icons/add.png" onclick="addRadio(this)" class="notranslate" alt="Add" title="Add another choice."/> <img src="template/images/icons/delete.png" class="notranslate" onclick="deleteRadio(this)" alt="Delete" title="Delete this choice."/></li>',
    charactersOptionText: 'Characters',
    wordsOptionText: 'Words',
    valueOptionText: 'Value',
    digitsOptionText: 'Digits', //</sl:translate>
    initialize: function (Form) {
        this.Form = Form;
        this.show = new Array();
        this.show['radio'] = new Array('choices', 'displayAnswer');
    },
    display: function (Field) {
        this.Field = Field;
        this.hideAll();
        this.title();
        for (var x = 0; x < this.show[this.Field.Typeof].length; x++) {
            this[this.show[this.Field.Typeof][x]]();
        }
        this.position(Field.DisplayPos);
        $('fieldPos').style.display = 'block';
        $('allProps').style.display = 'block';
    },
    hideAll: function () {
        $('listOptions').style.display = 'none';
        $('listChoices').style.display = 'none';
        $('listDisplayAnswer').style.display = 'none';
        $('noFieldSelected').style.display = 'none';
        $('listButtons').style.display = 'none';
        $('fieldPos').style.display = 'none';
        $('allProps').style.display = 'none';
    },
    onFieldUpdate: function (Field, parameter) {
    },
    listButtons: function () {
    },
    position: function (val) {
        $('fieldPos').removeClassName('hide');
        $('fieldPos').innerHTML = (parseInt(val) + 1) + '.';
    },
    title: function () {
        if (this.Field.Typeof == 'page') $('listTitle').style.display = 'none';
        else $('listTitle').style.display = 'block';
        $('fieldTitle').value = this.Field.Title;
    },
    size: function () {
        $('listSize').style.display = 'block';
        for (var i = 0; i < $('fieldSize').options.length; i++) {
            if ($('fieldSize').options[i].value == this.Field.Size) {
                $('fieldSize').options[i].selected = true;
                break;
            }
        }
    },
    displayAnswer: function () {
        $('listDisplayAnswer').style.display = 'block';
        $('fieldDisplayRightAnswer').value = this.Field.RightAnswerDescription;
        $('fieldDisplayWrongAnswer').value = this.Field.WrongAnswerDescription;
    },
    
    populate: function () {
        $('listPopulate').style.display = 'block';
    },
    textDefault: function () {
        $('listTextDefault').style.display = 'block';
        $('fieldDefault').value = this.Field.DefaultVal;
    },
    
    choices: function () {
        $('listChoices').style.display = 'block';
        $('fieldChoices').innerHTML = '';
        var choicesMarkup = '';
        if (this.Field.Typeof == 'checkbox') {
            this.Field.SubFields.each(function (num, index) {
                choicesMarkup += this[this.Field.Typeof + 'Markup'](num, index);
            }.bind(this));
        } else {
            this.Field.Choices.each(function (num, index) {
                choicesMarkup += this[this.Field.Typeof + 'Markup'](num, index);
            }.bind(this));
        }
        $('fieldChoices').innerHTML = choicesMarkup;
        if (this.Field.Typeof == 'radio' && this.Field.Settings.indexOf('other') != -1) {
            var list = $('fieldChoices');
            var listEl = $('fieldChoices').down('li', $('fieldChoices').childElements().length - 1);
            this.highlightRadioOther(listEl);
            list.down('li', list.childElements().length - 1).down('img').remove();
        }
    },
    radioMarkup: function (choice, index) {
        var selected, selectedR = '';
        if (choice.IsDefault == '1') selected = 'checked="checked"';
        if (choice.IsRight == '1') selectedR = 'checked="checked"';
        var choiceValue = choice.Choice.gsub("\"", "&quot;");
        var tpl = new Template(this.radioMarkupText);
        return tpl.evaluate({
            selectedAttribute: selected,
            choice: choiceValue,
            selectedRightAnswered: selectedR
        });
    },
    highlightRadioOther: function (choiceToHighlight) {
        if (!choiceToHighlight.hasClassName('dropReq')) {
            choiceToHighlight.addClassName('dropReq');
            choiceToHighlight.insert({
                top: this.highlightRadioMarkupText
            });
        }
    },
    insertChoice: function (ctrl, value, type) {
        var choice = {
            "Choice": value,
            "IsDefault": "0",
            "IsRight": "1"
        }
        if (type == 'checkbox' || type == 'statement') {
            choice = {
                "ChoicesText": value,
                "DefaultVal": "0",
                "IsRight": "1"
            }
        }
        var markup = this[type + 'Markup'](choice, 1);
        ctrl.insert({
            after: markup
        });
    },
    deleteChoice: function (ctrl) {
        Element.remove(ctrl);
    },
    selectMarkup: function (choice, index) {
        var selected,selectedR = '';
        var defaultClass = '';
        var linkTip = '';
        var choiceValue = choice.Choice.gsub("\"", "&quot;");
        if (choice.IsRight == '1') selectedR = 'checked="checked"';
        if (choice.IsDefault == '1') {
            selected = 'checked="checked"';
            if (this.Field.IsRequired == '1') {
                defaultClass = 'class="dropReq"';
                linkTip = this.linkTipMarkupText;
            }
        }
        var tpl = new Template(this.selectMarkupText);
        return tpl.evaluate({
            defaultClassKey: defaultClass,
            linkTipKey: linkTip,
            selectedKey: selected,
            choiceKey: choiceValue,
            selectedRightAnswered: selectedR
        });
    },
    highlightDefaultIfRequired: function (Field) {
        this.unhighlightOldDefaultChoice();
        if (Field.Typeof == 'select' && Field.IsRequired == '1') {
            var choiceToHighlight = '';
            Field.Choices.each(function (num, index) {
                if (num.IsDefault == '1') {
                    choiceToHighlight = $('fieldChoices').down('li', index);
                }
            });
            if (choiceToHighlight != '') {
                if (!choiceToHighlight.hasClassName('dropReq')) {
                    choiceToHighlight.addClassName('dropReq');
                    choiceToHighlight.insert({
                        top: this.choiceToHighlightMarkupText
                    });
                }
            }
        }
    },
    unhighlightOldDefaultChoice: function () {
        var oldReq = $$('.dropReq')[0];
        if (oldReq) {
            var link = oldReq.down('a');
            if (link) link.remove();
            oldReq.removeClassName('dropReq');
        }
    },
    checkboxMarkup: function (choice, index) {
        var selected = selectedR = '';
        if (choice.DefaultVal == '1') selected = 'checked="checked"';
        if (choice.IsRight == '1') selectedR = 'checked="checked"';
        var choiceValue = choice.ChoicesText.gsub("\"", "&quot;");
        var deleteValue = (index != 0) ? this.checkboxMarkupDeleteText : '';
        var tpl = new Template(this.checkboxMarkupText);
        return tpl.evaluate({
            selectedKey: selected,
            choiceKey: choiceValue,
            deleteKey: deleteValue,
            selectedRightAnswered: selectedR
        });
    }
});
var PagePropertiesObject = Class.create({
    titleMarkup: '<li><b>#{sequence}.</b> <input class="text" type="text" value="#{title}" ' + 'onkeyup="updateProperties(this.value, \'Title\', #{columnId})" ' + 'onmouseup="updateProperties(this.value, \'Title\', #{columnId})"/></li>',
    initialize: function () {},
    populate: function (Form, Profile) {
        $('formProperties').addClassName('paging');
        this.Profile = Profile;
        this.Form = Form;
        this.selectBreadCrumb();
        this.selectShowPageTitle();
        this.selectShowPageFooter();
        this.displayPageTitles();
    },
    selectBreadCrumb: function () {
        $('breadcrumbs' + this.Form.FormJson.BreadCrumbType).checked = true;
    },
    selectShowPageTitle: function () {
        if (this.Form.FormJson.ShowPageTitle == 1) $('showPageTitles').checked = true;
        else $('showPageTitles').checked = false;
    },
    selectShowPageFooter: function () {
        if (this.Form.FormJson.ShowPageFooter == 1) $('pageFooter').checked = false;
        else $('pageFooter').checked = true;
    },
    displayPageTitles: function () {
        var markup = '';
        $('pageTitles').childElements().each(function (num) {
            if (num.className != 'paymentPageTitle') num.remove();
        });
        if (this.Form.FormJson.MerchantEnabled == '1') {
            $('paymentTitle').value = this.Form.FormJson.PaymentPageTitle;
            $('pageTitles').addClassName('paymentEnabled');
        }
        this.Form.FormJson.Fields.each(function (field, index) {
            var pageTemplate = new Template(this.titleMarkup);
            if (field.Typeof == 'page') {
                var tempPage = {
                    title: field.Title.gsub("\"", "&quot;"),
                    sequence: field.Page,
                    columnId: field.ColumnId
                };
                markup += pageTemplate.evaluate(tempPage);
            }
        }.bind(this));
        $('pageTitles').insert({
            top: markup
        });
    }
});
var FormPreviewObject = Class.create({
    initialize: function () {},
    modify: function (value, parameter, Form, Field) {
        if (this[parameter]) {
            this[parameter](value, Form, Field);
        }
    },
    Name: function (value, Form, Field) {
        if ($('fname')) $('fname').innerHTML = Form.FormJson.Name;
    },
    Description: function (value, Form, Field) {
        if ($('fdescription')) $('fdescription').innerHTML = Form.FormJson.Description.replace(/\n/g, "<br />");
    },
    Language: function (value, Form, Field) {
        if (value == 'preview') {
            $('formLanguage').selectedIndex = 10;
            window.open('http://hoctudau.com', '', 'width=700,height=500,toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes');
        }
        if (value == 'arabic' || value == 'persian' || value == 'hebrew') {
            $('main').addClassName('rtl');
            $('fieldProperties').addClassName('rtl-lang');
            $('formProperties').addClassName('rtl-lang');
        } else {
            $('main').removeClassName('rtl');
            $('fieldProperties').removeClassName('rtl-lang')
            $('formProperties').removeClassName('rtl-lang');
        }
    },
    LabelAlign: function (value, Form, Field) {
        $('formPreview').className = 'hoctudau page1 ' + Form.FormJson.LabelAlign;
    },
    EntryLimit: function (value, Form, Field) {
        if (Form.FormJson.EntryLimit == 0) $('formEntryLimit').value = '';
    },
    StartDate: function (value, Form, Field) {},
    EndDate: function (value, Form, Field) {},
    PageHeader: function (value, Form, Field) {
        var circle = $('pagecircle' + Field.Page);
        if (circle) {
            circle.innerHTML = value;
        }
    },
    replaceHeader: function (markup) {
        $('pageHeader').innerHTML = markup;
    },
    PaymentPageTitle: function (value, Form, Field) {
        var pc = Form.instancesOfField('page') + 1;
        $('pagecircle' + pc).innerHTML = value;
    },
    toggleShowPageHeaderText: function (param) {}
});
var AccountProfile = Class.create({
    data: '', //<sl:translate>
    upgradeRequiredTitle: 'Upgrade Required',
    upgradeRequiredFileMsg: 'In order to add a file upload field, you must upgrade yourHoctudau  subscription. To do so, click on the "Account" link at the top of this page.',
    upgradeRequiredFieldsMsg: 'In order to add more fields, you must upgrade yourHoctudau subscription. To do so, click on the "Account" link at the top of this page.',
    fieldLimitTitle: 'Field Limit',
    fieldLimitMsg: 'You have reached the maximum amount of fields (50) available for a form.', //</sl:translate>
    initialize: function () {
        this.data = __USER.Profile;
        this.initUnlock();
    },
    initUnlock: function () {
        Event.observe(window, 'unload', this.unlock.bind(this), false);
    },
    unlock: function () {},
    permissionToAddField: function (type, fieldCount, changing) {
        if (type == 'file' && this.data.ProfileId == '0') {
            Lightbox.rawUrl("lightboxes/Demo.FileUpload.php");
            return false;
        } else if (type == 'file' && this.data.Plan.Upload == '0') {
            Lightbox.rawUrl("lightboxes/FormBuilder.FileUpload.php");
            return false;
        }
        if (fieldCount >= this.data.Plan.FieldCount && !changing) {
            lightbox(this.fieldLimitTitle, this.fieldLimitMsg);
            return false;
        }
        return true;
    },
    isDemoAccount: function () {
        if (this.data.ProfileId == 'demo' || this.data.ProfileId == '0') return true;
        else return false;
    },
    permissionToRedirect: function () {
        if (this.data.ProfileId == '0') {
            Lightbox.rawUrl("lightboxes/Demo.RedirectURL.php");
            return false;
        } else if (this.data.Plan.PlanId <= 1) {
            Lightbox.rawUrl("lightboxes/FormBuilder.RedirectURL.php");
            return false;
        }
        return true;
    },
    getTargetURL: function () {
        return new String(document.location);
    }
});
var FormObject = Class.create({
    data: '',
    firstInstruction: true, //<sl:translate>
    encryptedFieldsWarning: 'You may only have up to 5 encrypted fields.',
    paragraphFieldsWarningTitle: 'Size Restriction',
    paragraphFieldsWarning: 'Due to storage space restrictions, a form may have no more than 10 Paragraph fields. The best option is to try and structure certain fields as Single Line text fields.', //</sl:translate>
    initialize: function () {
        this.FormJson = __FORM;
    },
    set: function (value, parameter) {
        switch (parameter) {
            case 'EntryLimit':
                if (value > 0 && value < 99999999999) value = value;
                else value = 0;
                break;
            case 'Language':
                if (value == 'preview') value = 'english';
            case 'StartDate':
                if (value == 'live') {
                    var d = $F('startDate') + '-' + $F('startDate-1') + '-' + $F('startDate-2');
                    if (d == '--') d = '2000-01-01';
                    var hour = $F('startTime-0');
                    if ($F('startTime-2') == 'PM') hour = parseInt(hour) + 12;
                    var t = hour + ':' + $F('startTime-1') + ':00';
                    value = d + ' ' + t;
                }
                break;
            case 'EndDate':
                if (value == 'live') {
                    var d = $F('endDate') + '-' + $F('endDate-1') + '-' + $F('endDate-2');
                    if (d == '--') d = '2030-01-01';
                    var hour = $F('endTime-0');
                    if ($F('endTime-2') == 'PM') hour = parseInt(hour) + 12;
                    var t = hour + ':' + $F('endTime-1') + ':00';
                    value = d + ' ' + t;
                }
                break;
            case 'UseCaptcha':
                addCaptchaToPreview(value);
                break;
            case 'ShowPageTitle':
                if (value == 1) {
                    $('pageHeader').removeClassName('nopagelabel');
                    $('pageTitles').removeClassName('hide');
                } else {
                    $('pageHeader').addClassName('nopagelabel');
                    $('pageTitles').addClassName('hide');
                }
                break;
            case 'ShowPageFooter':
                if (value == 1) $('formFields').removeClassName('hideMarkers');
                else $('formFields').addClassName('hideMarkers');
                break;
            default:
                break;
        }
        this.FormJson[parameter] = value;
    },
    setField: function (value, parameter, columnId) {
        var ret = false;
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.ColumnId == columnId) {
                Field[parameter] = value;
                switch (parameter) {
                    
                }
                ret = Field;
            }
        }.bind(this));
        return ret;
    },
    setChoice: function (value, parameter, columnId, position, ctrl) {
        var ret = false;
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.ColumnId == columnId) {
                if (Field.Choices[position]) {
                    Field.Choices[position][parameter] = value;
                }
            ret = Field;
            }
        }.bind(this));
        return ret;
    },
    appendToClassNames: function (setting, columnId) {
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.ColumnId == columnId) {
                Field.ClassNames = Field.ClassNames + ' ' + setting;
            }
        }.bind(this));
    },
    removeFromClassNames: function (setting, columnId) {
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.ColumnId == columnId) {
                Field.ClassNames = Field.ClassNames.gsub(setting, '');
            }
        }.bind(this));
    },
    addSetting: function (setting, columnId) {
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.ColumnId == columnId) {
                Field.Settings.push(setting);
            }
        }.bind(this));
    },
    removeSetting: function (setting, columnId) {
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.ColumnId == columnId) {
                Field.Settings = Field.Settings.without(setting);
            }
        }.bind(this));
    },
    eraseValidationIfEmail: function () {
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.Typeof == 'email' && Field.Validation == 'email') {
                Field.Validation = '';
            }
        });
    },
    permissionToAddField: function (type) {
        if (type == 'textarea') {
            if (this.instancesOfField(type) >= 10) {
                lightbox(this.paragraphFieldsWarningTitle, this.paragraphFieldsWarning);
                return false;
            }
        }
        return true;
    },
    instancesOfField: function (type) {
        var tc = 0;
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.Typeof == type) tc++;
        });
        return tc;
    },
    instancesOfProperty: function (property, value) {
        var tc = 0;
        this.FormJson.Fields.each(function (Field, index) {
            if (Field[property] == value) tc++;
        });
        return tc;
    },
    selectFieldByColumnId: function (columnId) {
        var ret = this.FormJson.Fields.find(function (Field) {
            return Field.ColumnId == columnId;
        });
        return ret;
    },
    pageHasNoFields: function (columnId) {
        var nofieldsonpage = true;
        var prev = $('foli' + columnId).previous();
        if (prev && prev.title == 'This page has no fields!') prev = prev.previous();
        if (prev && !prev.hasClassName('buttons') && prev.id != 'pageHeader') nofieldsonpage = false;
        return nofieldsonpage;
    },
    calculateFormHeight: function () {
        var heights = [];
        var maxHeight = 1;
        this.FormJson.Fields.each(function (Field, index) {
            if (Field.Typeof != 'page') {
                var offset = $('foli' + Field.ColumnId).offsetHeight;
                if (!heights[Field.Page]) {
                    heights[Field.Page] = offset;
                } else heights[Field.Page] += offset;
            }
        });
        heights.each(function (num, index) {
            if (num > maxHeight) maxHeight = num;
        });
        return maxHeight + 200;
    }
});
var FormEngineObject = Class.create({
    target: new String(document.location),
    initialize: function (Profile) {
        this.Profile = Profile;
        this.target = this.Profile.getTargetURL();
        Event.observe('wuform', 'mousedown', selectForm, false);
        Event.observe($('saveForm'), 'click', saveForm, false);
        $('saveForm').onclick = function () {
            return false;
        }
    },
    save: function (Form) {
        var myAjax = new Ajax.Request(this.getSaveURL(), {
            method: 'post',
            parameters: 'action=save&form=' + encodeURIComponent(Object.toJSON(Form.FormJson)),
            onComplete: function (r) {
                var ret = r.responseText.evalJSON();
                Event.observe($('saveForm'), 'click', saveForm, false);
                finishSaving(ret);
            }.bind(this)
        });
    },
    saveDemo: function (Form) {
        $('formData').value = Object.toJSON(Form.FormJson);
        $('demoSave').submit();
    },
    getSaveURL: function () {
        var url = document.URL.split('/');
        var urlToUse = this.target;
        if (url[url.length - 2] != 'build' && url[url.length - 2] != 'form-builder') urlToUse += url[url.length - 2] + '/';
        return urlToUse;
    },
    fetchPageHeader: function (Form) {
        var myAjax = new Ajax.Request(this.target, {
            method: 'post',
            parameters: 'action=pageheader&form=' + encodeURIComponent(Object.toJSON(Form.FormJson)),
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishPageHeaders(ret);
            }.bind(this)
        });
    },
    addField: function (Form, json, pos) {
        json.HasntBeenSaved = true;
        if (!pos || pos < 0) pos = 0
        if (Form.FormJson.PageCount > 1) {
            Form.FormJson.Fields.splice(pos, 0, json);
        } else {
            Form.FormJson.Fields.push(json);
        }
    },
    removeField: function (Form, Field) {
        var i = -1;
        Form.FormJson.Fields.each(function (F, index) {
            if (F.ColumnId == Field.ColumnId) {
                i = index;
            }
        });
        if (i >= 0) Form.FormJson.Fields.splice(i, 1);
    },
    addChoice: function (Field, Choice, pos) {
        Field.Choices.splice((pos + 1), 0, Choice);
    },
    removeChoice: function (Field, pos) {
        var choice = Field.Choices[pos];
        var defChoice = false;
        if (choice.IsDefault == 1) {
            defChoice = true;
        }
        Field.Choices.splice(pos, 1);
        if (defChoice && Field.Typeof == 'select') this.setDefaultChoice(Field, 0);
    },
    addSubField: function (Field, SubField, pos) {
        SubField.HasntBeenSaved = true;
        Field.SubFields.splice((pos + 1), 0, SubField);
    },
    removeSubField: function (Field, pos) {
        Field.SubFields.splice(pos, 1);
    },
    setDefaultChoice: function (Field, pos, forceDefault) {
        Field.Choices.each(function (choice, index) {
            if (index == pos && (choice.IsDefault == 0 || forceDefault || Field.Typeof == 'select')) choice.IsDefault = 1;
            else if (index == pos && choice.IsDefault == 1) choice.IsDefault = 0;
            else if (Field.Typeof != 'checkbox') choice.IsDefault = 0;
        });
    },
    setDefaultRightChoice: function (Field, pos, forceDefault) {
        Field.Choices.each(function (choice, index) {
            if (index == pos && choice.IsRight == 0) choice.IsRight = 1;
            else if (index == pos && choice.IsRight == 1) choice.IsRight = 0;
        });
    },
    setDefaultCheckboxChoice: function (Field, pos) {
        Field.SubFields.each(function (subfield, index) {
            if (index == pos && subfield.DefaultVal == 0) subfield.DefaultVal = 1;
            else if (index == pos && subfield.DefaultVal == 1) subfield.DefaultVal = 0;
        });
    },
    setDefaultRightCheckboxChoice: function (Field, pos, forceDefault) {
        Field.SubFields.each(function (subfield, index) {
            if (index == pos && subfield.IsRight == 0) subfield.IsRight = 1;
            else if (index == pos && subfield.IsRight == 1) subfield.IsRight = 0;
        });
    },
    incrementPageCount: function (Form) {
        Form.FormJson.PageCount += 1;
    },
    decrementPageCount: function (Form) {
        if (Form.FormJson.PageCount > 1) Form.FormJson.PageCount -= 1;
    }
});
var FieldObject = Class.create({
    initialize: function () {}
});
var FieldEngineObject = Class.create({
    lastColumnId: 1,
    attributes: {
        "radio": {
            "FieldCount": 1
        }
    },
    target: new String(document.location),
    initialize: function (Form, Profile) {
        this.lastColumnId = 1;
        this.formUrl = Form.FormJson.Url;
        this.Form = Form;
        this.Profile = Profile;
        this.target = this.Profile.getTargetURL();
        Form.FormJson.Fields.each(function (Field, index) {
            this.updateLastColumnId(Field);
            this.attachLoadEvents(Field.ColumnId);
        }.bind(this));
    },
    attachLoadEvents: function (id) {
        obj = $('foli' + id);
        if (obj) {
            Event.observe(obj, 'click', selectField.bind(obj), false);
            Event.observe(obj, 'mousedown', function () {
                this.style.cursor = 'move';
            }.bind(obj), false);
            Event.observe(obj, 'mouseup', function () {
                this.style.cursor = 'pointer';
            }.bind(obj), false);
        }
    },
    updateLastColumnId: function (field) {
        var newMax = parseInt(field.ColumnId) + this.attributes[field.Typeof].FieldCount;
        if (newMax >= this.lastColumnId) this.lastColumnId = newMax + 1;
    },
    generateNewColumnId: function (type) {
        ret = this.lastColumnId;
        this.lastColumnId += this.attributes[type].FieldCount;
        return ret;
    },
    addField: function (type, insertAfterElement, dragDropBox, language) {
        var columnId = this.generateNewColumnId(type);
        var myAjax = new Ajax.Request(this.target, {
            method: 'post',
            parameters: 'action=addfield&type=' + type + '&fieldId=' + columnId + '&lang=' + language,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                if (type != 'page') finishAddingField(ret, insertAfterElement, dragDropBox);
                else finishAddingPage(ret, insertAfterElement, dragDropBox);
            }.bind(this)
        });
    },
    duplicateField: function (field, language) {
        var columnId = this.generateNewColumnId(field.Typeof);
        var myAjax = new Ajax.Request(this.target, {
            method: 'post',
            parameters: 'action=addfield&type=' + field.Typeof + '&fieldId=' + columnId + '&field=' + encodeURIComponent(Object.toJSON(field)) + '&lang=' + language,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                if (field.Typeof != 'page') finishAddingField(ret, 'foli' + field.ColumnId);
                else finishAddingPage(ret, 'foli' + field.ColumnId);
            }
        });
    },
    changeField: function (field, newType, sameDbColumn, language) {
        var columnId = field.ColumnId;
        if (!sameDbColumn) columnId = this.generateNewColumnId(newType);
        var myAjax = new Ajax.Request(this.target, {
            method: 'post',
            parameters: 'action=changefield&type=' + newType + '&fieldId=' + columnId + '&field=' + encodeURIComponent(Object.toJSON(field)) + '&lang=' + language,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishChangingField(ret, field.ColumnId, sameDbColumn);
            }
        });
    },
    deleteField: function (field) {
        var myAjax = new Ajax.Request(this.target, {
            method: 'post',
            parameters: 'action=delete&field=' + encodeURIComponent(Object.toJSON(field)) + '&url=' + this.formUrl,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishDeletingField(ret);
            }
        });
    },
    deleteCheckboxChoice: function (field, ctrl) {
        var myAjax = new Ajax.Request(this.target, {
            method: 'post',
            parameters: 'action=delete&field=' + encodeURIComponent(Object.toJSON(field)) + '&url=' + this.formUrl,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishDeletingCheckbox(ret, ctrl);
            }
        });
    },
    deleteLikertStatement: function (field, ctrl) {
        var myAjax = new Ajax.Request(this.target, {
            method: 'post',
            parameters: 'action=delete&field=' + encodeURIComponent(Object.toJSON(field)) + '&url=' + this.formUrl,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishDeletingLikertStatement(ret, ctrl);
            }
        });
    },
    displayField: function (field) {
        var myAjax = new Ajax.Request(this.target, {
            method: 'post',
            parameters: 'action=displayfield&field=' + encodeURIComponent(Object.toJSON(field)),
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishDisplayingField(ret);
            }
        });
    }
});
var FieldPreviewObject = Class.create({
    target: new String(document.location), //<sl:translate>
    noFieldsInPageMarkupText: '<div id="nfip#{columnIdKey}" class="nofieldsonpage notice" title="This page has no fields!">#{html}</div>', //</sl:translate>
    initialize: function (Profile) {
        this.Profile = Profile;
        this.target = this.Profile.getTargetURL();
    },
    addField: function (markup, insertAfterElement) {
        if (insertAfterElement == 'top') $('formFields').insert({
            top: markup
        });
        else if (insertAfterElement == 'dragdropbox') {
            var switchMe = $('formFields').select('.dragfld')[0];
            switchMe.addClassName('hide');
            switchMe.insert({
                before: markup
            });
        } else if ($(insertAfterElement)) $(insertAfterElement).insert({
            after: markup
        });
        else {
            var lastChild = $('formFields').lastChild;
            if (!lastChild || lastChild.hasClassName('dragable') || lastChild.hasClassName('paging-context')) {
                $('formFields').insert({
                    bottom: markup
                });
            } else lastChild.previous('li').insert({
                after: markup
            });
        }
    },
    modify: function (value, parameter, Field) {
        if (this[parameter]) {
            this[parameter](value, Field);
        }
    },
    Title: function (value, Field) {
        if (Field.Title.length > 100) {
            if (!$('fieldTitle').hasClassName('expand')) $('fieldTitle').addClassName('expand');
        } else $('fieldTitle').removeClassName('expand');
        if ($('title' + Field.ColumnId)) {
            var el = $('title' + Field.ColumnId);
            value = Field.Title + '<span class="req" id="req_' + Field.ColumnId + '"></span>';
            if (el) el.innerHTML = value.replace(/\n/g, "<br />");
            if (Field.IsRequired == 1) this.IsRequired(1, Field);
        }
    },
    Instructions: function (value, Field) {
        el = $('instruct' + Field.ColumnId);
        if (el) {
            if (Field.Instructions != '') el.removeClassName('hide');
            else el.addClassName('hide');
            if (Field.Typeof != 'section') el.down().innerHTML = Field.Instructions.replace(/\n/g, "<br />");
            else el.innerHTML = Field.Instructions.replace(/\n/g, "<br />");
        }
    },
    Size: function (value, Field) {
        $('Field' + Field.ColumnId).removeClassName('small').removeClassName('medium').removeClassName('large');
        $('Field' + Field.ColumnId).addClassName(Field.Size);
    },
    IsRequired: function (value, Field) {
        var el = $('req_' + Field.ColumnId);
        (el && value == '1') ? el.innerHTML = ' *' : el.innerHTML = '';
    },
    IsPrivate: function (value, Field) {
        (Field.IsPrivate == 1) ? Element.addClassName('foli' + Field.ColumnId, 'private') : Element.removeClassName('foli' + Field.ColumnId, 'private');
    },
    Validation: function (value, Field) {
        if (Field.Typeof == 'money') {
            updateFieldDisplay();
        }
    },
    DefaultVal: function (value, Field) {
        switch (Field.Typeof) {
            case 'date':
                break;
            case 'eurodate':
                break;
            case 'phone':
                this.defaultPhone(value, Field);
                break;
            case 'address':
                this.defaultAddress(value, Field);
                break;
            case 'page':
                this.defaultPage(value, Field);
                break;
            default:
                this.defaultText(value, Field);
                break;
        }
    },
    defaultText: function (value, Field) {
        $('Field' + Field.ColumnId).value = value;
    },
    defaultPhone: function (value, Field) {
        $('Field' + Field.ColumnId).value = value.substring(0, 3);;
        $('Field' + Field.ColumnId + '-1').value = value.substring(3, 6);;
        $('Field' + Field.ColumnId + '-2').value = value.substring(6, 10);;
    },
    defaultAddress: function (value, Field) {
        var countryDropDown = $('foli' + Field.ColumnId).down('select');
        for (var i = 0; i < countryDropDown.options.length; i++) {
            if (countryDropDown.options[i].value == value) countryDropDown.selectedIndex = i;
        }
    },
    defaultPage: function (value, Field) {
        $('previousPageButton' + Field.ColumnId).innerHTML = Field.DefaultVal;
    },
    defaultDate: function (value, Field) {
        if (value != '') {
            var myAjax = new Ajax.Request(this.target, {
                method: 'post',
                parameters: 'action=strtotime&time=' + value,
                onComplete: function (r) {
                    ret = r.responseText.evalJSON();
                    defaultVal = ret.response.date.split('/');
                    $('Field' + Field.ColumnId + '-2').value = defaultVal[1];
                    $('Field' + Field.ColumnId + '-1').value = defaultVal[0];
                    $('Field' + Field.ColumnId).value = defaultVal[2];
                    if (Field.Typeof == 'eurodate') {
                        $('Field' + Field.ColumnId + '-2').value = defaultVal[0];
                        $('Field' + Field.ColumnId + '-1').value = defaultVal[1];
                    }
                }
            });
        } else {
            $('Field' + Field.ColumnId + '-2').value = '';
            $('Field' + Field.ColumnId + '-1').value = '';
            $('Field' + Field.ColumnId).value = '';
        }
    },
    defaultPrice: function (value, Field) {
        var values = value.split('.');
        var dollars = values[0];
        var cents = values[1];
        $('Field' + Field.ColumnId).value = dollars;
        if (cents) $('Field' + Field.ColumnId + '-1').value = cents;
        else $('Field' + Field.ColumnId + '-1').value = '';
    },
    ChoicesText: function (value, Field) {
        if (Field.Typeof == 'page') {
            if ($('listNextButtons').hasClassName('asImg')) {
                $('nextPageButtonImg' + Field.ColumnId).src = value;
            } else $('nextPageButton' + Field.ColumnId).value = value;
        }
    },
    choiceDefault: function (field, position, ctrl) {
        switch (field.Typeof) {
            case 'radio':
                this.defaultRadio(field, position, ctrl);
                break;
            case 'select':
                this.defaultSelect(field, position, ctrl);
                break;
            case 'checkbox':
                this.defaultCheckbox(field, position, ctrl);
                break;
            case 'likert':
                this.defaultLikert(field, position, ctrl);
                break;
        }
    },
    defaultRadio: function (field, position, ctrl) {
        var i = $('foli' + field.ColumnId).down('input', (position + 1));
        if (i.checked) {
            i.checked = false;
            ctrl.checked = false;
        } else i.checked = true;
    },
    defaultLikert: function (field, position, ctrl) {
        var el = $('foli' + field.ColumnId);
        var body = el.down('tbody');
        body.childElements().each(function (num, index) {
            var i = num.down('td', position).down('input');
            if (i.checked) {
                i.checked = false;
                ctrl.checked = false;
            } else i.checked = true;
        });
    },
    defaultSelect: function (field, position, ctrl) {
        $('Field' + field.ColumnId).selectedIndex = position;
    },
    defaultCheckbox: function (field, position, ctrl) {
        var i = $('foli' + field.ColumnId).down('input', position);
        if (i.checked) i.checked = false;
        else i.checked = true;
    },
    removeField: function (Field) {
        var ctrl = $('foli' + Field.ColumnId);
        if ($('nfip' + Field.ColumnId)) {
            $('nfip' + Field.ColumnId).remove();
        }
        Effect.Fade(ctrl, {
            duration: .5,
            afterFinish: function () {
                ctrl.remove();
                __BUILD.updatePositionsByHtml();
            }.bind(this)
        });
    },
    removeFieldWithoutAnimation: function (Field) {
        $('foli' + Field.ColumnId).remove();
    },
    updateAfterReorder: function (Field, Form) {
        switch (Field.Typeof) {
            case 'section':
                this.SectionLine(Field);
                break;
            case 'page':
                this.updatePageCounter(Field, Form);
                this.removeNoFieldsInPage(Form, Field);
                break;
            default:
                break;
        }
    },
    SectionLine: function (Field) {
        if (Field.Typeof == 'section') {
            var el = $('foli' + Field.ColumnId);
            var previousEl = el.previous('li');
            if (previousEl) previousEl = previousEl.down('div');
            if (Field.Pos == 0) el.addClassName('first');
            else if (previousEl && previousEl.hasClassName('marker')) el.addClassName('first');
            else el.removeClassName('first');
        }
    },
    modifyChoice: function (Field, value, pos, ctrl) {
        switch (Field.Typeof) {
            case 'radio':
                if (value == '') value = '&nbsp;'
                var el = $('foli' + Field.ColumnId).down('div');
                el.down('label', pos).innerHTML = value;
                break;
            case 'select':
                var el = $('Field' + Field.ColumnId);
                el.down(pos).innerHTML = value;
                break;
            case 'checkbox':
                if (value == '') value = '&nbsp;';
                var el = $('foli' + Field.ColumnId).down('div');
                el.down('label', pos).innerHTML = value;
                break;
            case 'likert':
                if (ctrl.up('ul').id == 'likertColumns') {
                    var el = $('foli' + Field.ColumnId).down('thead');
                    el.down('td', pos).innerHTML = value;
                } else {
                    var el = $('foli' + Field.ColumnId).down('tbody');
                    el.down('tr', pos).down('th').down('label').innerHTML = value;
                }
                break;
        }
    },
    addChoice: function (Field, pos, value) {
        switch (Field.Typeof) {
            case 'radio':
                this.addRadioChoice(Field, pos, value);
                break;
            case 'select':
                this.addSelectChoice(Field, pos, value);
                break;
            case 'checkbox':
                this.addCheckboxChoice(Field, pos);
                break;
            case 'likert':
                this.addLikertChoice(Field, pos);
                break;
        }
    },
    deleteChoice: function (Field, pos) {
        switch (Field.Typeof) {
            case 'radio':
                this.deleteRadioChoice(Field, pos);
                break;
            case 'select':
                this.deleteSelectChoice(Field, pos);
                break;
            case 'checkbox':
                this.deleteCheckboxChoice(Field, pos);
                break;
            case 'likert':
                this.deleteLikertChoice(Field, pos);
                break;
        }
    },
    addRadioChoice: function (Field, pos, value) {
        if (value == '') value = '&nbsp;';
        var el = $('foli' + Field.ColumnId).down('div');
        var markup = '<span><input disabled="disabled" id="Field' + Field.ColumnId + '_' + (pos + 1) + '"' + ' name="Field' + Field.ColumnId + '"' + ' class="field radio" type="radio" />' + '<label class="choice" for="Field' + Field.ColumnId + '_' + (pos + 1) + '">' + value + '</label></span>';
        el.down().next('span', pos).insert({
            after: markup
        });
    },
    deleteRadioChoice: function (Field, pos) {
        var el = $('foli' + Field.ColumnId).down('div');
        var i = el.down('span', pos);
        Element.remove(i);
    },
    addLikertChoice: function (Field, pos) {
        var el = $('foli' + Field.ColumnId);
        var tr = el.down('tr', pos + 1);
        var newEl = tr.cloneNode(true);
        var uniquename = pos + (Math.floor(Math.random() * 1000000 + 1));
        newEl.childElements().each(function (child, index) {
            if (child.down() && child.down('input')) {
                var radio = child.down('input');
                radio.id = radio.id + '_' + index;
                radio.name = radio.name + '_' + uniquename;
            }
        });
        tr.insert({
            after: newEl
        });
        el.down('tr', pos + 2).down('th').down('label').innerHTML = '&nbsp;';
        this.highlightLikertStatements(Field);
    },
    highlightLikertStatements: function (Field) {
        var el = $('foli' + Field.ColumnId);
        var body = el.down('tbody');
        var highlight = '';
        body.childElements().each(function (tr, index) {
            tr.className = highlight;
            if (highlight == '') highlight = 'alt';
            else highlight = '';
        });
    },
    deleteLikertChoice: function (Field, pos) {
        var el = $('foli' + Field.ColumnId);
        var tr = el.down('tr', pos + 1);
        tr.remove();
    },
    addSelectChoice: function (Field, pos, value) {
        if (Prototype.Browser.IE) {
            var optn = document.createElement("OPTION");
            optn.text = value;
            optn.value = value;
            $('Field' + Field.ColumnId).options.add(optn, pos + 1);
        } else $('Field' + Field.ColumnId).down(pos).insert({
            after: '<option value="' + value + '">' + value + '</option>'
        });
    },
    deleteSelectChoice: function (Field, pos) {
        $('Field' + Field.ColumnId).down(pos).remove();
    },
    addCheckboxChoice: function (Field, pos) {
        var lastCheckbox = $('foli' + Field.ColumnId).down('span', (pos + 1));
        var markup = '<span><input disabled="disabled" id="Field' + Field.ColumnId + '_' + (pos + 1) + '"' + ' name="Field' + Field.ColumnId + '"' + ' class="field checkbox" type="checkbox" />' + '<label class="choice" for="Field' + Field.ColumnId + '_' + (pos + 1) + '">&nbsp;</label></span>';
        lastCheckbox.insert({
            after: markup
        });
    },
    deleteCheckboxChoice: function (Field, pos) {
        var el = $('foli' + Field.ColumnId).down('div');
        var i = el.down('span', pos);
        Element.remove(i);
    },
    addNoFieldsInPage: function (columnId, Form) {
        if ($('nfip' + columnId)) $('nfip' + columnId).remove();
        var tpl = new Template(this.noFieldsInPageMarkupText);
        var markup = tpl.evaluate({
            columnIdKey: columnId,
            html: $('nofieldsonpage').innerHTML
        });
        var location = 'top';
        if ($('foli' + columnId).previous()) location = $('foli' + columnId).previous();
        this.addField(markup, location);
    },
    removeNoFieldsInPage: function (Form, Field) {
        if (!Form.pageHasNoFields(Field.ColumnId)) {
            if ($('nfip' + Field.ColumnId)) {
                $('nfip' + Field.ColumnId).remove();
            }
        } else {
            this.addNoFieldsInPage(Field.ColumnId, Form);
        }
    },
    updatePageCounter: function (Field, Form) {
        var pages = Form.instancesOfField('page');
        var prevbutton = $$('#foli' + Field.ColumnId + ' .hasprev')[0];
        var nextbutton = $$('#foli' + Field.ColumnId + ' .hasnext')[0];
        if (Field.ChoicesText.include('http')) nextbutton = $$('#foli' + Field.ColumnId + ' .hasnext')[1];
        $('pagecurrent' + Field.ColumnId).innerHTML = Field.Page;
        $('pagetotal' + Field.ColumnId).innerHTML = pages;
        if (Field.Page == 1) prevbutton.addClassName('hide');
        else prevbutton.removeClassName('hide');
        if (Field.Page == pages) nextbutton.addClassName('hide');
        else nextbutton.removeClassName('hide');
    },
    removeDragable: function (ColumnId) {
        $('foli' + ColumnId).removeClassName('dragable');
        $('indicator' + ColumnId).remove();
        $('fa' + ColumnId).remove();
    },
    showPageHeader: function () {
        $('pageHeader').removeClassName('hide');
    },
    hidePageHeader: function () {
        $('pageHeader').addClassName('hide');
    },
    togglePagingButton: function (ColumnId, category, newtype) {
        category = category.toLowerCase();
        if (newtype == 'asImg') {
            $(category + 'PageButton' + ColumnId).addClassName('hide');
            $(category + 'PageButtonImg' + ColumnId).removeClassName('hide');
        } else {
            $(category + 'PageButtonImg' + ColumnId).addClassName('hide');
            $(category + 'PageButton' + ColumnId).removeClassName('hide');
        }
    },
    showPageJump: function (Form) {
        this.adjustPageJump(1, Form)
        $('pagejump').removeClassName('hide');
    },
    hidePageJump: function () {
        $('pagejump').addClassName('hide');
    },
    adjustPageJump: function (pageId, Form) {
        var pages = Form.instancesOfField('page');
        if (pages == pageId) $('jumpnext').style.visibility = 'hidden';
        else $('jumpnext').style.visibility = 'visible';
        if (pageId == 1) $('jumpprev').style.visibility = 'hidden';
        else $('jumpprev').style.visibility = 'visible';
        $('jumptarget').value = pageId;
    }
});
var Interface = Class.create({
    scrolling: false,
    afterScrollTime: 0,
    scrollTime: -1,
    initialize: function () {
        this.initButtons();
    },
    hideStatus: function () {
        hideStatus();
    },
    initButtons: function () {
        this.adjustButtons();
        Event.observe(window, 'scroll', this.adjustButtons.bind(this), false);
        Event.observe(window, 'resize', this.adjustButtons.bind(this), false);
        Event.observe(window, 'scroll', this.afterScroll.bind(this), false);
    },
    showSaveButton: function () {
        Element.removeClassName('formButtons', 'hide');
    },
    hideSaveButton: function () {
        Element.addClassName('formButtons', 'hide');
    },
    showNoFields: function () {
        Element.removeClassName('container', 'hasFields');
    },
    hideNoFields: function () {
        Element.addClassName('container', 'hasFields');
    },
    highlightField: function (id) {
        if (id >= 0) {
            Element.addClassName('foli' + id, 'editing focused');
            $('foli' + id).style.zIndex = "500";
        }
        this.unhighlightForm();
        this.unhighlightPageSettings();
    },
    unhighlightField: function (id) {
        if (id >= 0) {
            Element.removeClassName('foli' + id, 'editing');
            Element.removeClassName('foli' + id, 'focused');
            if ($('foli' + id)) {
                $('foli' + id).style.zIndex = "100";
            }
        }
    },
    highlightForm: function () {
        Element.addClassName('wuform', 'editing');
        new Effect.ScrollTo('container', {
            duration: .2
        });
    },
    unhighlightForm: function () {
        Element.removeClassName('wuform', 'editing');
    },
    highlightPageSettings: function () {
        $('pageHeader').addClassName('editing focused');
        new Effect.ScrollTo('container', {
            duration: .2
        });
    },
    unhighlightPageSettings: function () {
        $('pageHeader').removeClassName('editing');
        $('pageHeader').removeClassName('focused');
    },
    showFormActivity: function () {
        $('listStartDate').removeClassName('hide');
        $('listEndDate').removeClassName('hide');
    },
    hideFormActivity: function () {
        $('listStartDate').addClassName('hide');
        $('listEndDate').addClassName('hide');
    },
    showFormPassword: function () {
        Element.removeClassName('formPasswordDiv', 'hide');
    },
    hideFormPassword: function () {
        Element.addClassName('formPasswordDiv', 'hide');
    },
    showFormConfirmationMessage: function () {
        Element.removeClassName('formMessageVal', 'hide');
        Element.addClassName('formRedirectVal', 'hide');
    },
    showFormRedirectUrl: function () {
        Element.addClassName('formMessageVal', 'hide');
        Element.removeClassName('formRedirectVal', 'hide');
    },
    showConfirmationEmailToUser: function () {
        $('formReceiptDiv').removeClassName('hide');
    },
    hideConfirmationEmailToUser: function () {
        $('formReceiptDiv').addClassName('hide');
    },
    snapFieldProperties: function (list) {
        if (list.innerHTML) {
            propsPos = $(list).cumulativeOffset()[1] - 150;
            if (propsPos < 0) propsPos = 0;
            $('fieldProperties').style.marginTop = propsPos + 'px';
        }
    },
    adjustButtons: function () {
        var scrollCovered = document.viewport.getScrollOffsets()['top'] + document.viewport.getHeight();
        if (document.viewport.getHeight() <= $('importer').offsetHeight && scrollCovered <= $('container').offsetHeight * .96) {
            $('container').addClassName('tooLong')
        } else {
            $('container').removeClassName('tooLong');
        }
    },
    adjustAdd: function () {
        if (document.viewport.getHeight() >= 550 && document.viewport.getScrollOffsets()['top'] >= document.viewport.getHeight() * .20) {
            offset = (document.viewport.getHeight() / 2) - ($('addFields').offsetHeight / 2) - 75;
        } else {
            offset = 0;
        }
        var value = document.viewport.getScrollOffsets()['top'] + offset + 'px';
        $('addFields').morph('margin-top:' + value, {
            duration: .5
        });
    },
    afterScroll: function () {
        if (!this.scrolling) {
            this.scrolling = true;
            this.scrollTime = new Date().getTime();
            setTimeout(this.scrollCheck.bind(this), 100);
        }
    },
    scrollCheck: function () {
        if (this.scrollTime == this.afterScrollTime) {
            this.scrolling = false;
            this.afterScrollTime = 0;
            this.scrollTime = -1;
            this.adjustAdd();
        } else {
            this.afterScrollTime = this.scrollTime;
            setTimeout(arguments.callee.bind(this), 100);
        }
    },
    showDemoMessage: function () {}
});
var FormBuilder = Class.create({
    UI: new Interface(),
    Tabs: new TabInterface(),
    Profile: new AccountProfile(),
    Form: new FormObject(),
    FormEngine: '',
    FormPreview: new FormPreviewObject(),
    Field: new FieldObject(),
    Reorder: '',
    FieldPreview: '',
    FieldProperties: '',
    FieldEngine: '',
    FormProperties: '',
    PageProperties: '',
    DragDrop: '',
    hasntSaved: false,
    activeField: '', //<sl:translate>
    choicesRestrictedWarning: 'Choices are restricted to 300 per field.',
    youHaveNotSavedWarning: "YOU HAVEN'T SAVED YET AND\nYOU'LL BE ABANDONING YOUR CHANGES.",
    fieldMaxOptionsWarning: 'This field is allowed a maximum of 95 options.',
    fieldMaxColumnsWarning: 'This field is allowed a maximum of 10 columns.',
    notAllChoicesWereAddedError: 'Not all choices were added since the maximum of 10 choices was exceeded.',
    deletingWillDeleteDataWarning: 'Deleting this choice will also delete all data associated with it. This cannot be undone. Are you sure you want to delete this choice?',
    savingFormDataStatus: 'Saving form data ...',
    unableToDeleteFieldTitle: 'Unable to delete this field!',
    unableToDeleteField: 'You must have at least 1 field in your form.',
    saveFormTxt: 'Save Form',
    savingTxt: 'Saving',
    addingNewFieldTxt: 'Adding a new field ...', //</sl:translate>
    initialize: function () {
        this.FormEngine = new FormEngineObject(this.Profile);
        this.FieldEngine = new FieldEngineObject(this.Form, this.Profile);
        this.FieldProperties = new FieldPropertiesObject(this.Form), this.FormProperties = new FormPropertiesObject();
        this.PageProperties = new PagePropertiesObject();
        this.FieldPreview = new FieldPreviewObject(this.Profile);
        this.DragDrop = new DragDropObject();
        this.Reorder = new ReorderObject(this.Form, this.FieldPreview, this.FieldProperties, this.FormEngine)
        window.onbeforeunload = this.wannaLeave.bind(this);
        this.UI.hideStatus();
        this.updatePositionsByHtml();
    },
    wannaLeave: function () {
        unlockSession()
        if (this.hasntSaved) {
            return this.youHaveNotSavedWarning;
        }
    },
    selectForm: function () {
        this.UI.unhighlightField(this.activeField);
        this.UI.unhighlightPageSettings();
        this.UI.highlightForm();
        this.FormProperties.populate(this.Form, this.Profile);
        this.Tabs.showSidebar('formProperties');
        this.Tabs.thirdTab();
        this.activeField = -1;
        this.UI.adjustButtons();
    },
    selectPageSettings: function () {
        this.UI.unhighlightField(this.activeField);
        this.UI.unhighlightForm();
        this.UI.highlightPageSettings();
        this.PageProperties.populate(this.Form, this.Profile);
        this.Tabs.showSidebar('formProperties');
        this.Tabs.thirdTab();
        this.activeField = -1;
        this.UI.adjustButtons();
    },
    updateForm: function (value, parameter) {
        this.Form.set(value, parameter);
        this.FormPreview.modify(value, parameter, this.Form);
        if (parameter == 'BreadCrumbType') this.updatePageHeader();
        this.hasntSaved = true;
    },
    saveForm: function () {
        $('statusText').innerHTML = this.savingFormDataStatus;
        $('saveForm').innerHTML = '<img src="template/images/icons/arrow_refresh.png" /> ' + this.savingTxt;
        this.Form.set(this.Form.calculateFormHeight(), 'OffsetHeight');
        this.FormEngine.save(this.Form);
    },
    resetSaveFormAppearance: function () {
        $('saveForm').innerHTML = '<img src="template/images/icons/tick.png" /> ' + this.saveFormTxt;
    },
    processSaveSuccess: function (ret) {
        this.hasntSaved = false;
        if (ret.success == true) document.location = 'index.php?req=index/';
        else if (ret.success == false) {
            if(ret.message) lightbox('Some Error Occured', ret.message);
            else lightbox('Some Error Occured', 'Unknown Error');
        }
    },
    processSaveDemoSuccess: function (ret) {
        this.hasntSaved = false;
        $('formData').value = Object.toJSON(this.Form.FormJson);
        $('demoSave').submit();
    },
    processSaveError: function (ret) {
        this.resetSaveFormAppearance();
        Lightbox.showContent(ret.response.lbmarkup);
    },
    showFormActivity: function () {
        this.UI.showFormActivity();
    },
    hideFormActivity: function () {
        this.UI.hideFormActivity();
        this.Form.set('2000-01-01 12:00:00', 'StartDate');
        this.Form.set('2030-01-01 12:00:00', 'EndDate');
    },
    showFormPassword: function () {
        this.UI.showFormPassword();
    },
    hideFormPassword: function () {
        this.UI.hideFormPassword();
        this.Form.set('', 'Password');
        $('formPasswordVal').value = '';
    },
    toggleFormRedirect: function (setting) {
        if (setting == 'url') this.showFormRedirectUrl();
        else this.showFormConfirmationMessage();
    },
    showFormRedirectUrl: function () {
        if (this.Profile.permissionToRedirect()) {
            this.Form.set($F('formRedirectVal'), 'Redirect');
            this.UI.showFormRedirectUrl();
        } else {
            $('formMessage').checked = true;
        }
    },
    showFormConfirmationMessage: function () {
        this.Form.set('', 'Redirect');
        this.UI.showFormConfirmationMessage();
    },
    addCaptchaToPreview: function (useCaptcha) {
        if (useCaptcha == 2) $('forceCaptcha').removeClassName('hide');
        else $('forceCaptcha').addClassName('hide');
    },
    showConfirmationEmailToUser: function () {
        this.UI.showConfirmationEmailToUser();
    },
    hideConfirmationEmailToUser: function () {
        this.Form.eraseValidationIfEmail();
        $('formSendTo').selectedIndex = 0;
        this.UI.hideConfirmationEmailToUser();
    },
    displayReceiptLightbox: function () {
        this.FormProperties.displayReceiptLightbox();
    },
    finishLoadingReceiptLightbox: function () {
        if (this.Form.FormJson.FormId > 0) {
            $('receiptApi').href = 'https://' + document.domain + '/api/code/' + this.Form.FormJson.FormId + '/';
        }
        this.FormProperties.populateReceiptLightbox(this.Form);
        this.UI.hideStatus();
    },
    updatePageHeader: function () {
        this.FormEngine.fetchPageHeader(this.Form);
    },
    finishPageHeaders: function (ret) {
        this.FormPreview.replaceHeader(ret.response.markup);
    },
    jumpToPage: function (direction) {
        if (direction == 'next') this.jumpToNextPage();
        else this.jumpToPreviousPage();
    },
    jumpToNextPage: function () {
        var pageId = $('jumptarget').value;
        if (pageId >= 1) {
            pageId = parseInt(pageId) + 1;
            var pageObj = $('formFields').down('li.page', (pageId - 2));
            if (pageObj) {
                var target = pageObj.next();
                Effect.ScrollTo(target, {
                    'offset': -35,
                    'duration': 0.3
                });
            }
        }
        this.FieldPreview.adjustPageJump(pageId, this.Form);
    },
    jumpToPreviousPage: function () {
        var pageId = $('jumptarget').value;
        if (pageId >= 2) {
            pageId = parseInt(pageId) - 1;
            var pageObj = $('formFields').down('li.page', (pageId - 2));
            if (pageObj) {
                var target = pageObj.next();
                Effect.ScrollTo(target, {
                    'offset': -35,
                    'duration': 0.3
                });
            }
        }
        if (pageId == 1) {
            target = $('formFields').next();
            Effect.ScrollTo(target, {
                'duration': 0.3
            });
        }
        this.FieldPreview.adjustPageJump(pageId, this.Form);
    },
    showFields: function (pulse) {
        this.UI.unhighlightField(this.activeField);
        this.UI.unhighlightForm();
        this.UI.unhighlightPageSettings();
        this.Tabs.showFields(pulse);
        this.UI.adjustButtons();
        this.activeField = -1;
    },
    selectField: function (ctrl) {
        var columnId = -1;
        if (ctrl.id) columnId = ctrl.id.replace('foli', '');
        else {
            if (this.Form.FormJson.Fields[0]) columnId = this.Form.FormJson.Fields[0].ColumnId;
        }
        if (columnId != this.activeField || columnId == -1) {
            this.UI.unhighlightField(this.activeField);
            if (columnId >= 0) this.activeField = columnId;
            this.Tabs.showFieldProperties(columnId);
            this.UI.highlightField(columnId);
            if (columnId >= 0) this.FieldProperties.display(this.Form.selectFieldByColumnId(columnId));
            this.UI.snapFieldProperties(ctrl);
            this.UI.adjustButtons();
        }
    },
    addField: function (type, insertAfterElement, dragDropBox) {
        if (this.Profile.permissionToAddField(type, this.Form.FormJson.Fields.length, false) && this.Form.permissionToAddField(type)) {
            $('statusText').innerHTML = this.addingNewFieldTxt;
            this.FieldEngine.addField(type, insertAfterElement, dragDropBox, this.Form.FormJson.Language);
            this.UI.hideNoFields();
            this.UI.showSaveButton();
            this.UI.adjustButtons();
            this.hasntSaved = true;
        }
    },
    processAddSuccess: function (ret, insertAfterElement, nodrag, dragDropBox) {
        var insertType = insertAfterElement;
        if (dragDropBox) insertType = 'dragdropbox';
        this.FieldPreview.addField(ret.response.html, insertType);
        if (dragDropBox) this.DragDrop.returnButtonToAddFieldMenu(dragDropBox);
        this.FormEngine.addField(this.Form, ret.response.json.evalJSON(), this.findInsertPosition(insertAfterElement));
        this.updatePositionsByHtml();
        if (!nodrag) this.DragDrop.grantFieldDragAbility();
        Event.observe($('foli' + ret.response.id), 'click', selectField.bind($('foli' + ret.response.id)), false);
    },
    findInsertPosition: function (insertAfterElement) {
        return this.Reorder.findInsertPositionByHtml(insertAfterElement);
    },
    finishAddingPage: function (ret, insertAfterElement, dragDropBox) {
        var nodrag = false;
        var pages = this.Form.instancesOfField('page');
        if (pages == 1) nodrag = true;
        if (pages >= 1) {
            this.FormEngine.incrementPageCount(this.Form);
        }
        this.processAddSuccess(ret, insertAfterElement, nodrag, dragDropBox);
        if (pages == 0) {
            this.addField('page');
            this.FieldPreview.showPageHeader();
        }
        if (pages == 1) {
            this.FieldPreview.showPageJump(this.Form);
            this.FieldPreview.removeDragable(ret.response.id);
        }
    },
    processAddError: function (ret) {
        Lightbox.showContent(ret.response.lbmarkup);
    },
    addFieldAfterDraggingComplete: function () {
        var insertAfter = 'top';
        var dragDropBox = $('formFields').select('.dragfld')[0];
        var fieldType = dragDropBox.id.replace('drag1_', '').replace('drag2_', '').replace('drag3_', '').replace('drag4_', '');
        if (dragDropBox.previous()) insertAfter = dragDropBox.previous().id;
        this.addField(fieldType, insertAfter, dragDropBox);
    },
    duplicateField: function (columnId, e) {
        var fieldToDuplicate = this.Form.selectFieldByColumnId(columnId);
        if (this.Profile.permissionToAddField(fieldToDuplicate.Typeof, this.Form.FormJson.Fields.length) && this.Form.permissionToAddField(fieldToDuplicate.Typeof)) {
            $('statusText').innerHTML = this.addingNewFieldTxt;
            this.FieldEngine.duplicateField(fieldToDuplicate, this.Form.FormJson.Language);
        }
    },
    deleteField: function (columnId, e) {
        if (!e) var e = window.event;
        e.cancelBubble = true;
        if (e.stopPropagation) e.stopPropagation();
        if (this.Form.FormJson.Fields.length > 1 || this.Form.FormJson.FormId < 1) {
            var fieldToDelete = this.Form.selectFieldByColumnId(columnId);
            if (fieldToDelete.HasntBeenSaved || fieldToDelete.Typeof == 'page') {
                this.finishDeletingField({
                    'ColumnId': columnId
                }, e);
            } else {
                Lightbox.showUrl('lightboxes/Field.Delete.php', function () {
                    $('deleteFieldId').value = fieldToDelete.ColumnId;
                }, 'error');
            }
        } else {
            lightbox(this.unableToDeleteFieldTitle, this.unableToDeleteField);
        }
    },
    continueDeletingField: function () {
        var fieldToDelete = this.Form.selectFieldByColumnId($F('deleteFieldId'));
        Lightbox.close();
        this.FieldEngine.deleteField(fieldToDelete);
    },
    finishDeletingField: function (ret, e) {
        var field = this.Form.selectFieldByColumnId(ret.ColumnId);
        this.FieldPreview.removeField(field);
        this.FormEngine.removeField(this.Form, field);
        this.handlePageDeletion(field, e);
        this.Tabs.showFieldProperties(-1);
        this.activeField = -1;
        if (this.Form.FormJson.Fields.length <= 0) {
            this.UI.showNoFields();
            this.UI.hideSaveButton();
        }
        this.UI.adjustButtons();
    },
    handlePageDeletion: function (field, e) {
        var instances = this.Form.instancesOfField('page');
        if (field.Typeof == 'page' && instances >= 1) {
            this.FormEngine.decrementPageCount(this.Form);
            this.checkAndDeleteLastPage(instances, e);
        }
    },
    checkAndDeleteLastPage: function (instances, e) {
        if (instances == 1) {
            var id = $('formFields').lastChild.id;
            id = id.replace('foli', '');
            this.deleteField(id, e);
            this.FieldPreview.hidePageHeader();
            this.FieldPreview.hidePageJump();
        }
    },
    processDeleteError: function (ret) {
        Lightbox.showContent(ret.response.lbmarkup);
    },
    updatePositionsByHtml: function () {
        var refresh = this.Reorder.updatePositionsByHtml(this.activeField);
        if (refresh) this.updatePageHeader();
    },
    updateField: function (value, parameter, columnId) {
        if (!columnId) columnId = this.activeField;
        var Field = this.Form.setField(value, parameter, columnId);
        if (!this.updatePageBreak(value, parameter, Field)) {
            this.FieldPreview.modify(value, parameter, Field);
            this.FieldProperties.onFieldUpdate(Field, parameter);
        }
        this.hasntSaved = true;
    },
    updatePageBreak: function (value, parameter, Field) {
        var ret = false;
        if (Field.Typeof == 'page') {
            if (parameter == 'Title') {
                this.FormPreview.modify(value, 'PageHeader', this.Form, Field);
                ret = true;
            }
        }
        return ret;
    },
    removeEncryption: function () {
        this.updateField('0', 'IsEncrypted');
        $('fieldEncrypted').checked = false;
        Lightbox.close();
    },
    defaultDate: function (value) {
        var field = this.Form.selectFieldByColumnId(this.activeField);
        this.FieldPreview.defaultDate(value, field);
    },
    defaultPrice: function (value) {
        var field = this.Form.selectFieldByColumnId(this.activeField);
        this.FieldPreview.defaultPrice(value, field);
    },
    updateFieldDisplay: function () {
        var fieldToChange = this.Form.selectFieldByColumnId(this.activeField);
        this.FieldEngine.displayField(fieldToChange);
    },
    finishDisplayingField: function (ret) {
        var insertAfter = $('foli' + ret.response.id).previous();
        if (!insertAfter) insertAfter = 'top';
        else insertAfter = insertAfter.id;
        $('foli' + ret.response.id).remove();
        this.FieldPreview.addField(ret.response.html, insertAfter);
        this.DragDrop.grantFieldDragAbility();
        Event.observe($('foli' + ret.response.id), 'click', selectField.bind($('foli' + ret.response.id)), false);
        this.UI.highlightField(ret.response.id);
    },
    changeFieldType: function (newType, sameDbColumn) {
        if (this.Profile.permissionToAddField(newType, this.Form.FormJson.Fields.length, true) && this.Form.permissionToAddField(newType)) {
            var fieldToChange = this.Form.selectFieldByColumnId(this.activeField);
            this.FieldEngine.changeField(fieldToChange, newType, sameDbColumn, this.Form.FormJson.Language);
            this.hasntSaved = true;
        }
    },
    finishChangingField: function (ret, originalField, sameDbColumn) {
        var originalField = this.Form.selectFieldByColumnId(originalField);
        this.FormEngine.removeField(this.Form, originalField);
        this.processAddSuccess(ret, 'foli' + originalField.ColumnId);
        this.FieldPreview.removeFieldWithoutAnimation(originalField);
        this.updatePositionsByHtml();
        this.activeField = -1;
        if (sameDbColumn && !originalField.HasntBeenSaved) {
            var newField = this.Form.selectFieldByColumnId(ret.response.id);
            newField.HasntBeenSaved = false;
        }
        this.selectField($('foli' + ret.response.id));
        Event.observe($('foli' + ret.response.id), 'click', selectField.bind($('foli' + ret.response.id)), false);
    },
    toggleChoicesLayout: function (setting) {
        var settings = new Array('notStacked', 'twoColumns', 'threeColumns');
        for (var i = 0; i < settings.length; i++) {
            this.Form.removeSetting(settings[i], this.activeField);
            $('foli' + this.activeField).removeClassName(settings[i]);
        }
        this.Form.addSetting(setting, this.activeField);
        $('foli' + this.activeField).addClassName(setting);
    },
    showButtonType: function (category, newtype) {
        var oldtype = 'asTxt';
        if (newtype == 'asTxt') oldtype = 'asImg';
        $('list' + category + 'Buttons').removeClassName(oldtype);
        $('list' + category + 'Buttons').addClassName(newtype);
        if (newtype == 'asTxt' && category == 'Next') updateProperties($F('nextButtonsText'), 'ChoicesText');
        if (newtype == 'asImg' && category == 'Next') updateProperties($F('nextButtonImg'), 'ChoicesText');
        this.FieldPreview.togglePagingButton(this.activeField, category, newtype);
    },
    updateChoice: function (ctrl) {
        var position = $(ctrl).up().previousSiblings().length;
        var value = $F(ctrl);
        var fieldBeingUpdated = this.Form.setChoice(value, 'Choice', this.activeField, position, ctrl);
        this.FieldPreview.modifyChoice(fieldBeingUpdated, value, position, ctrl);
        this.hasntSaved = true;
    },
    addChoice: function (ctrl, value, noalert) {
        var position = $(ctrl).up().previousSiblings().length;
        if (!value) value = '';
        var field = this.Form.selectFieldByColumnId(this.activeField);
        if (field.Choices.length < 300) {
            this.FieldProperties.insertChoice(ctrl.up(), value, field.Typeof);
            if (field.Typeof == 'checkbox') this.FormEngine.addSubField(field, {
                "Title": "",
                "ChoicesText": value,
                "DefaultVal": "0",
                "IsRight": "1"
            }, position);
            else this.FormEngine.addChoice(field, {
                "Choice": value,
                "IsDefault": "0",
                "IsRight": "1"
            }, position);
            this.FieldPreview.addChoice(field, position, value);
        } else if (!noalert) alert(this.choicesRestrictedWarning);
    },
    addSubField: function (ctrl, value) {
        var position = $(ctrl).up().previousSiblings().length;
        if (!value) value = '';
        var field = this.Form.selectFieldByColumnId(this.activeField);
        var ftype = field.Typeof;
        if (field.Typeof == 'likert') ftype = 'statement';
        if (field.SubFields.length < 95) {
            this.FieldProperties.insertChoice(ctrl.up(), value, ftype);
            this.FormEngine.addSubField(field, {
                "Title": "",
                "ChoicesText": value,
                "DefaultVal": "0",
                "IsRight": "1"
            }, position);
            this.FieldPreview.addChoice(field, position, value);
        } else {
            alert(this.fieldMaxOptionsWarning);
        }
    },
    deleteSubField: function (ctrl, value) {
        var field = this.Form.selectFieldByColumnId(this.activeField);
        var position = $(ctrl).up().previousSiblings().length;
        if (position == 0 && field.SubFields && field.SubFields.length == 1) {
            Element.previous(ctrl, 'input').value = '';
            this.updateChoice(Element.previous(ctrl, 'input'));
        } else {
            this.FieldProperties.deleteChoice(ctrl.parentNode);
            this.FieldPreview.deleteChoice(field, position);
            this.FormEngine.removeSubField(field, position);
            if (field.Typeof == 'likert') this.FieldPreview.highlightLikertStatements(field);
        }
    },
    deleteChoice: function (ctrl) {
        var field = this.Form.selectFieldByColumnId(this.activeField);
        var position = $(ctrl).up().previousSiblings().length;
        if (position == 0 && ((field.Choices && field.Choices.length == 1) || (field.SubFields && field.SubFields.length == 1))) {
            Element.previous(ctrl, 'input').value = '';
            this.updateChoice(Element.previous(ctrl, 'input'));
        } else {
            var toggleOther = false;
            if (!$(ctrl).up().next()) toggleOther = true;
            this.FieldProperties.deleteChoice(ctrl.parentNode);
            this.FieldPreview.deleteChoice(field, position);
            if (field.Typeof == 'checkbox') this.FormEngine.removeSubField(field, position);
            else this.FormEngine.removeChoice(field, position);
            if (field.Typeof == 'radio' && field.Settings.indexOf('other') != -1 && toggleOther) {
                this.Form.removeSetting('other', this.activeField);
                $('fieldOther').checked = false;
                this.updateFieldDisplay();
            }
        }
    },
    changeChoiceDefault: function (ctrl) {
        var field = this.Form.selectFieldByColumnId(this.activeField);
        var position = $(ctrl).up().previousSiblings().length;
        if (field.Typeof == 'checkbox') this.FormEngine.setDefaultCheckboxChoice(field, position);
        else this.FormEngine.setDefaultChoice(field, position);
        this.FieldPreview.choiceDefault(field, position, ctrl);
        this.FieldProperties.onFieldUpdate(field, 'IsDefault');
    },
    changeRightDefault: function (ctrl) {
        var field = this.Form.selectFieldByColumnId(this.activeField);
        var position = $(ctrl).up().previousSiblings().length;
        if (field.Typeof == 'checkbox') this.FormEngine.setDefaultRightCheckboxChoice(field, position);
        else this.FormEngine.setDefaultRightChoice(field, position);
        //this.FieldPreview.choiceDefault(field, position, ctrl);
        this.FieldProperties.onFieldUpdate(field, 'IsRight');
    },
    toggleRadioOther: function (checked) {
        var list = $('fieldChoices');
        var clickedEl = list.down('li', list.childElements().length - 1).down('img');
        if (checked) {
            this.Form.addSetting('other', this.activeField);
            this.addChoice(clickedEl, 'Other');
            this.FieldProperties.highlightRadioOther(list.down('li', list.childElements().length - 1));
            list.down('li', list.childElements().length - 1).down('img').remove();
            this.updateFieldDisplay();
        } else {
            this.Form.removeSetting('other', this.activeField);
            this.deleteChoice(clickedEl);
            this.updateFieldDisplay();
        }
    },
    processBulkAddition: function (choices) {
        var field = this.Form.selectFieldByColumnId(this.activeField);
        if (!this.currentlyAddingChoices) {
            this.currentlyAddingChoices = true;
            choices.each(function (choice) {
                ctrl = $('fieldChoices').down('li', $('fieldChoices').childElements().length - 1);
                choiceTextField = ctrl.down('img');
                if (choiceTextField.parentNode.hasClassName('dropReq') && field.Typeof != 'select') {
                    ctrl = $('fieldChoices').down('li', $('fieldChoices').childElements().length - 1);
                    choiceTextField = ctrl.previous().down('img');
                }
                this.addChoice(choiceTextField, choice, true);
            }.bind(this));
        }
        this.currentlyAddingChoices = false;
    },
    processBulkLikertAddition: function (choices) {
        var field = this.Form.selectFieldByColumnId(this.activeField);
        var showAlert = false;
        if (field.Choices.length + choices.length > 10 && field.Validation != 'na') showAlert = true;
        if (field.Choices.length + choices.length > 11 && field.Validation == 'na') showAlert = true;
        if (!this.currentlyAddingChoices) {
            this.currentlyAddingChoices = true;
            choices.each(function (choice) {
                if (field.Choices.length < 100 || (field.Choices.length < 101 && field.Validation == 'na')) {
                    var ctrl = $('likertColumns').lastChild.childNodes[3];
                    if (field.Validation == 'na') {
                        ctrl = $('likertColumns').down('li', $('likertColumns').childElements().length - 1);
                        ctrl = ctrl.previous().childNodes[3];
                    }
                    
                    var position = $(ctrl).up().previousSiblings().length;
                    this.FieldProperties.insertChoice(ctrl.up(), choice, 'column');
                    this.FormEngine.addChoice(field, {
                        "Choice": choice,
                        "IsDefault": "0",
                        "IsRight": "1"
                    }, position);
                }
            }.bind(this));
        }
        this.currentlyAddingChoices = false;
        this.FieldEngine.displayField(field);
        if (showAlert) alert(this.notAllChoicesWereAddedError);
    },
    deleteCheckbox: function (ctrl) {
        var field = this.Form.selectFieldByColumnId(this.activeField);
        var position = $(ctrl).up().previousSiblings().length;
        subfield = field.SubFields[position];
        if (subfield.HasntBeenSaved || confirm(this.deletingWillDeleteDataWarning)) {
            if (subfield.HasntBeenSaved) this.deleteChoice(ctrl);
            else {
                subfield.Typeof = 'text';
                subfield.FormId = field.FormId;
                this.FieldEngine.deleteCheckboxChoice(subfield, ctrl);
            }
        }
    },
    setCookie: function (id, minutes) {
        var today = new Date();
        today.setTime(today.getTime());
        var expires = minutes * 1000 * 60;
        var expires_date = new Date(today.getTime() + expires);
        var cookie = 'wuFormId' + "=" + escape(id) + ";expires=" + expires_date.toGMTString() + ";path=/;";
        document.cookie = cookie;
    },
    storeFormId: function (id) {
    }
});
var __BUILD;
Event.observe(window, 'load', init, false);

function init() {
    __BUILD = new FormBuilder();
    initBulkAdd();
}

function selectForm() {
    __BUILD.selectForm();
}

function selectPageSettings() {
    __BUILD.selectPageSettings();
}

function updateForm(value, parameter) {
    if (value.replace) value = value.replace(/\\\"/g, "\\ \"");
    __BUILD.updateForm(value, parameter);
}

function saveForm() {
    __BUILD.saveForm();
}

function finishSaving(ret) {
    if (ret.success == 'false') __BUILD.processSaveError(ret);
    else {
        if (ret.response.demo == 'true') __BUILD.processSaveDemoSuccess(ret);
        else __BUILD.processSaveSuccess(ret);
    }
}

function resetSaveFormAppearance() {
    __BUILD.resetSaveFormAppearance();
}

function addCaptchaToPreview(useCaptcha) {
    __BUILD.addCaptchaToPreview(useCaptcha);
}

function toggleFormConfirmationEmail(setting) {
    if (setting) __BUILD.showConfirmationEmailToUser();
    else __BUILD.hideConfirmationEmailToUser();
}

function displayReceiptLightbox() {
    __BUILD.displayReceiptLightbox();
}

function finishLoadingReceiptLightbox() {
    __BUILD.finishLoadingReceiptLightbox();
}

function toggleFormActivity(setting) {
    if (setting) __BUILD.showFormActivity();
    else __BUILD.hideFormActivity();
}

function selectFormActivity(cal) {
    var p = cal.params;
    var update = (cal.dateClicked || p.electric);
    year = p.inputField.id;
    day = year + '-2';
    month = year + '-1';
    document.getElementById(month).value = cal.date.print('%m');
    document.getElementById(day).value = cal.date.print('%e');
    document.getElementById(year).value = cal.date.print('%Y');
    if (year == 'startDate') updateForm('live', 'StartDate');
    if (year == 'endDate') updateForm('live', 'EndDate');
}

function toggleFormPassword(setting) {
    if (setting) __BUILD.showFormPassword();
    else __BUILD.hideFormPassword();
}

function toggleFormRedirect(setting) {
    __BUILD.toggleFormRedirect(setting);
}

function updatePageHeader() {
    __BUILD.updatePageHeader();
}

function finishPageHeaders(ret) {
    if (ret.success == 'false') __BUILD.processSaveError(ret);
    else __BUILD.finishPageHeaders(ret);
}

function showButtonText(category) {
    __BUILD.showButtonType(category, 'asTxt');
}

function showButtonImage(category) {
    __BUILD.showButtonType(category, 'asImg');
}

function jumpToPage(direction) {
    __BUILD.jumpToPage(direction);
}

function showFields(pulse) {
    __BUILD.showFields(pulse);
}

function selectField(ctrl) {
    __BUILD.selectField(this);
}

function addField(type) {
    __BUILD.addField(type);
}

function addFieldAfterDraggingComplete() {
    __BUILD.addFieldAfterDraggingComplete();
}

function finishAddingField(ret, insertAfterElement, dragDropBox) {
    if (ret.success == 'false') __BUILD.processAddError(ret);
    else __BUILD.processAddSuccess(ret, insertAfterElement, false, dragDropBox);
}

function finishAddingPage(ret, insertAfterElement, dragDropBox) {
    if (ret.success == 'false') __BUILD.processAddError(ret);
    else __BUILD.finishAddingPage(ret, insertAfterElement, dragDropBox);
}

function duplicateField(columnId, e) {
    __BUILD.duplicateField(columnId, e);
}

function continueDeletingField() {
    __BUILD.continueDeletingField();
}

function changeFieldType(newType, sameDbColumn) {
    __BUILD.changeFieldType(newType, sameDbColumn);
}

function finishChangingField(ret, originalField, sameDbColumn) {
    if (ret.success == 'false') __BUILD.processAddError(ret);
    else __BUILD.finishChangingField(ret, originalField, sameDbColumn);
}

function removeField(columnId, e) {
    __BUILD.deleteField(columnId, e);
}

function finishDeletingField(ret) {
    if (ret.success == 'false') __BUILD.processDeleteError(ret);
    else __BUILD.finishDeletingField(ret.response);
}

function updateProperties(value, parameter, pos) {
    if (value.replace) value = value.replace(/\\\"/g, "\\ \"");
    __BUILD.updateField(value, parameter, pos);
}

function updateFieldDisplay() {
    __BUILD.updateFieldDisplay();
}

function finishDisplayingField(ret) {
    if (ret.success == 'false') __BUILD.processAddError(ret);
    else __BUILD.finishDisplayingField(ret);
}

function defaultDate(value) {
    __BUILD.defaultDate(value);
}

function defaultPrice(value) {
    __BUILD.defaultPrice(value);
}

function reorderField() {
    __BUILD.updatePositionsByHtml();
}

function removeEncryption() {
    __BUILD.removeEncryption();
}

function toggleLikertNumbers(checked) {
    __BUILD.toggleLikertNumbers(checked);
}

function toggleLikertNonApplicable(checked) {
    __BUILD.toggleLikertNonApplicable(checked);
}

function toggleLikertDatCheckbox(checked) {
    __BUILD.toggleLikertDatCheckbox(checked);
}

function toggleChoicesLayout(setting) {
    __BUILD.toggleChoicesLayout(setting);
}

function updateChoice(ctrl) {
    __BUILD.updateChoice(ctrl);
}

function addRadio(ctrl, value) {
    __BUILD.addChoice(ctrl, value);
}

function deleteRadio(ctrl) {
    __BUILD.deleteChoice(ctrl);
}

function changeRadioDefault(ctrl) {
    __BUILD.changeChoiceDefault(ctrl);
}

function changeRightDefault(ctrl) {
    __BUILD.changeRightDefault(ctrl);
}

function toggleRadioOther(checked) {
    __BUILD.toggleRadioOther(checked);
}

function addSelect(ctrl, value) {
    __BUILD.addChoice(ctrl, value);
}

function deleteSelect(ctrl) {
    __BUILD.deleteChoice(ctrl);
}

function changeSelectDefault(ctrl) {
    __BUILD.changeChoiceDefault(ctrl);
}

function processBulkAddition(choices) {
    __BUILD.processBulkAddition(choices);
}

function addCheckbox(ctrl, value) {
    __BUILD.addSubField(ctrl, value);
}

function deleteCheckbox(ctrl) {
    __BUILD.deleteCheckbox(ctrl);
}

function finishDeletingCheckbox(ret, ctrl) {
    if (ret.success == 'false') __BUILD.processDeleteError(ret);
    else __BUILD.deleteChoice(ctrl);
}

function changeCheckboxDefault(ctrl) {
    __BUILD.changeChoiceDefault(ctrl);
}

function addLikertStatement(ctrl, value) {
    __BUILD.addSubField(ctrl, value);
}

function deleteLikertStatement(ctrl, value) {
    __BUILD.deleteLikertStatement(ctrl, value);
}

function finishDeletingLikertStatement(ret, ctrl) {
    if (ret.success == 'false') __BUILD.processDeleteError(ret);
    else __BUILD.deleteSubField(ctrl);
}

function addLikertChoice(ctrl, value) {
    __BUILD.addLikertChoice(ctrl, value);
}

function deleteLikertChoice(ctrl, value) {
    __BUILD.deleteLikertChoice(ctrl, value);
}

function processBulkLikertAddition(choices) {
    __BUILD.processBulkLikertAddition(choices);
}

function changeLikertDefault(ctrl) {
    __BUILD.changeChoiceDefault(ctrl);
}

function setCookie(id) {
    __BUILD.setCookie(id);
}

function storeFormId(id) {
    __BUILD.storeFormId(id);
}