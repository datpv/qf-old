
var Form2EngineObject = Class.create({
    field: {},
    initialize: function () {
    },
    getSize: function(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    },
    loadFields: function() {
        var fields = $$('.field');
        var size = this.getSize(__ENTRY);
        if(size > 0) {
            for(i=0;i<fields.length;i++) {
                if(fields[i].type=='radio' || fields[i].type=='checkbox') {
                    fields[i].checked = false;
                    if(__ENTRY[fields[i].name] == fields[i].value) {
                        fields[i].checked = true;
                    }
                } else {
                    fields[i].value = __ENTRY[fields[i].name];
                }
            }
        }
    },
    qsend: function(data) {
        data.action = 'save';
        var f = this.field.fieldObj.id.replace('foli','');
        var url = new String(document.location);
        var myAjax = new Ajax.Request(url.replace('#public','') + '&f=' + f, {
            parameters: data,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                finishSend(result);
            }
        });
    },
    qSendSuccess: function(ret) {
        var field = $(this.field.fieldObj);
        var fieldPosts = $(this.field.fieldPosts);
        field.removeClassName('error');
        field.addClassName('saved');
        field.select('div, p').each(function(a,b) {
            a.style.backgroundColor = '#FFFB8E';
            new Effect.Fade(a,{ duration:1});
        });
        if(ret.response.review) {
            $('saveForm').up('li').innerHTML = 'Gửi Link Này Cho Thầy Giáo: <br><input class="full text" onclick="this.select()" value="' + ret.response.review + '" />';
        }
        setTimeout(function() {
            field.select('div').invoke('remove');
            field.select('p').invoke('remove');
            var h = 'Saved';
            if(ret.swjt) {
                h = [];
                for(var i in fieldPosts) {
                    if(i.startsWith('Field') && fieldPosts[i].length > 0) {
                        h.push(fieldPosts[i]);
                    }
                }
                h = h.join(' | ');
                if(ret.response.lr) {
                    if(ret.response.lr.point == '1') {
                        h += '<br><p style="color: #379507;">Right</p>'
                    } else if(ret.response.lr.point == '0') {
                        h += '<br><p style="color: #FF1E1E;">Wrong</p>'
                    }
                    
                }
            }
            
            var div = new Element('div', {'class': 'message success'}).update(h);
            field.insert(div);
            
        },1000);
    },
    qSendError: function(ret) {
        $(this.field.fieldObj).addClassName('error');
        $(this.field.fieldObj).select('.send')[0].removeClassName('hide');
        if($(this.field.fieldObj).down('.error')) $(this.field.fieldObj).down('.error').remove();
        var p = new Element('p', {'class': 'error'}).update(ret.response.message);
            $(this.field.fieldObj).insert(p);
    },
    addSendActions: function() {
        var ajaxButtons = $$('.send');
        var field = this.field;
        ajaxButtons.each(function(ajaxButton, index) {
            ajaxButton.observe('click', function(event) {event.stop();
                ajaxButton.addClassName('hide');
                var inputs = ajaxButton.up('li').select('input, select, textarea');
                var posts = {};
                inputs.each(function(a,b) {
                    if($F(a)) {
                        posts[a.name] = $F(a);
                    } else if(!posts[a.name]) {
                        posts[a.name] = '';
                    }
                });
                field.fieldObj = ajaxButton.up('li');
                field.fieldPosts = posts;
                qsend(posts);
                event.stop();
            });
        });
    }
});
var __FORM;
Event.observe(window, 'load', init, false);
function init() {
    __FORM = new Form2EngineObject();
    loadFields();
    addSendActions();
}
function addSendActions() {
    __FORM.addSendActions();
}
function loadFields() {
    __FORM.loadFields();
}
function handleInput(ctrl) {
    
}
function qsend(data) {
    __FORM.qsend(data);
}
function finishSend(ret) {
    if(ret.success == false) __FORM.qSendError(ret);
    else __FORM.qSendSuccess(ret);
}