var StatsEngineObject = Class.create({
    loadAll: function() {
        $$('.choices').invoke('removeClassName','hide');
    },
    loadNB: function() {
        $$('.choices.as').invoke('addClassName','hide');
        $$('.choices.nb').invoke('removeClassName','hide');
    },
    loadAS: function() {
        $$('.choices.nb').invoke('addClassName','hide');
        $$('.choices.as').invoke('removeClassName','hide');
    }
});

var __STATS;
Event.observe(window, 'load', init, false);
function init() {
    __STATS = new StatsEngineObject();
}
function loadAll() {
    __STATS.loadAll();
}
function loadNB() {
    __STATS.loadNB();
}
function loadAS() {
    __STATS.loadAS();
}