var Interface = Class.create({
    initialize: function () {
        getPlatform();
    },
    hideStatus: function () {
        hideStatus();
    },
    toggleFormPublicStatus: function (formId, publicStatus) {
        if (publicStatus == 1) {
            this.showButtons(formId);
        } else {
            this.hideButtons(formId);
        }
    },
    showButtons: function (formId) {
        $('li' + formId).removeClassName('notActive');
    },
    hideButtons: function (formId) {
        $('li' + formId).addClassName('notActive');
    },
    duplicateAnimation: function () {
        var formList = Array();
        $$('.group li').each(function (num, index) {
            formList[index] = num.id.replace('li', '') * 1;
        });
        maxFormID = formList.max();
        el = $('li' + maxFormID).down();
        var initialColor = el.getStyle('backgroundColor');
        var hexInitialColor = new RGBColor(initialColor);
        el.style.backgroundColor = '#99D247';
        new Effect.Highlight(el, {
            startcolor: '#99D247',
            endcolor: hexInitialColor.toHex(),
            restorecolor: initialColor,
            duration: 2,
            queue: 'end'
        });
    },
    showSearching: function (searchTerm) {
        $('searchTerm').innerHTML = searchTerm;
        $('searching').removeClassName('hide');
    },
    hideSearching: function () {
        if (!$('searching').hasClassName('hide')) $('searching').addClassName('hide');
    },
    showNoResults: function () {
        this.hideSearching();
        $('noResults').removeClassName('hide');
    },
    hideNoResults: function () {
        if (!$('noResults').hasClassName('hide')) $('noResults').addClassName('hide');
    },
    toggleSelectedSort: function (sort) {
        $('sort').childElements().invoke('removeClassName', 'selected');
        $(sort).addClassName('selected');
    }
});
var FormEngineObject = Class.create({
    initialize: function () {}, //<sl:translate>
    deleteFormStatus: 'Deleting form ...',
    duplicatingFormStatus: 'Duplicating form ...',
    savingPublicStatusStatus: 'Toggling the public setting of your form ...',
    saveThemeStatus: 'Changing the theme for this form ...',
    sortFormsStatus: 'Resorting your forms ...',
    displayGraphStatus: 'Processing data and drawing graph ...', //</sl:translate>
    deleteForm: function (formId, formName) {
        $('statusText').innerHTML = this.deleteFormStatus;
        var myAjax = new Ajax.Request('index.php?req=admin/', {
            method: 'post',
            parameters: 'action=delete&formId=' + formId + '&formName=' + encodeURIComponent(formName),
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishDeletingForm(ret);
            }.bind(this)
        });
    },
    duplicateForm: function (formId, formName) {
        $('statusText').innerHTML = this.duplicatingFormStatus;
        var myAjax = new Ajax.Request('index.php?req=admin/', {
            method: 'post',
            parameters: 'action=duplicate&formId=' + formId + '&formName=' + encodeURIComponent(formName),
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishDuplicatingForm(ret);
            }.bind(this)
        });
    },
    savePublicStatus: function (formId, formName, publicStatus) {
        $('statusText').innerHTML = this.savingPublicStatusStatus;
        var myAjax = new Ajax.Request('index.php?req=admin/', {
            method: 'post',
            parameters: 'action=savePublicStatus&formId=' + formId + '&formName=' + encodeURIComponent(formName) + '&publicStatus=' + publicStatus,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishSavingPublicStatus(ret);
            }.bind(this)
        });
    },
    savePassword: function (formId, formName, password) {
        var myAjax = new Ajax.Request('index.php?req=admin/', {
            method: 'post',
            parameters: 'action=protect&formId=' + formId + '&formName=' + encodeURIComponent(formName) + '&password=' + encodeURIComponent(password),
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                finishProtectForm(ret);
            }.bind(this)
        });
    },
    saveTheme: function (formId, formName, styleId, formUrl) {
        $('statusText').innerHTML = this.saveThemeStatus;
        var myAjax = new Ajax.Request('index.php?req=themes/attach/', {
            method: 'post',
            parameters: 'formId=' + formId + '&formName=' + formName + '&styleId=' + styleId + '&formUrl=' + formUrl,
            onComplete: function (response) {}
        });
    },
    sortForms: function (sortBy) {
        $('statusText').innerHTML = this.sortFormsStatus;
        var params = [];
        var $htmlContainer = $('groupList-1');
        var $pagedContainer = $$('.navHolder')[0];
        params['action'] = 'getForms',
        params['sortby'] = $('sort').select('.selected')[0].id,
        params['direction'] = 'desc',
        params['approveType'] = $$('.approveButton.selected')[0].rel,
        params['category_id'] = $('category_list').select('.active')[0].id.replace('cat_', '');
        params['paged'] = 1;
        __MANAGER.getForms(params, $htmlContainer, $pagedContainer);
    },
    formMatchesFilter: function (form, filter) {
        var name = form.Name || '';
        if (name.toLowerCase().include(filter.toLowerCase())) return true;
        else return false;
    },
    displayGraph: function (formId, type) {
        $('statusText').innerHTML = this.displayGraphStatus;
        var now = new Date();
        var myAjax = new Ajax.Request('/graphs/' + type + '/' + formId + '/' + now.getFullYear() + '/', {
            method: 'post',
            parameters: '',
            onComplete: function (response) {
                $('formGraph').innerHTML = response.responseText;
            }
        });
    }
});
var FormsObject = Class.create({
    initialize: function (Forms) {
        this.data = Forms;
    },
    getForm: function (formId) {
        if (this.data[formId]) return this.data[formId];
        else return false;
    }
});
var FormManager = Class.create({
    FormEngine: new FormEngineObject(),
    UI: '',
    Forms: '',
    sortSetting: 'DateCreated',
    filterSetting: '',
    g_type: 'week',
    initialize: function () {
        if ($('expandThis')) {
            this.actionMarkup = $('expandThis').innerHTML;
        }
        this.UI = new Interface();
        this.Forms = new FormsObject(__FORMS);
        this.UI.hideStatus();
        this.setSortDefaults();
        if (document.URL.indexOf('highlight') != -1) {
            this.UI.duplicateAnimation();
        }
        if ($('searchBox')) $('searchBox').focus();
        if (this.readCookie('stats') && this.readCookie('stats') != 'Off') this.loadDefaultStats();
    },
    attachEvents: function () {
        $$('#groupList-1 li').each(function (num, index) {
            num.observe('mouseover', showActions.bind(num));
            num.observe('mouseout', hideActions.bind(num));
        });
    },
    setSortDefaults: function () {
        var filter = this.readCookie('formManagerFilter');
        if (filter && filter != '') {
            this.filterSetting = filter
            this.runFilter();
        }
    },
    showActions: function (ctrl) {
        var ctrl = $(ctrl);
        if (ctrl.down) {
            this.activeForm = ctrl.id;
            if (!ctrl.down('.actions')) {
                ctrl.insert({
                    bottom: this.actionMarkup
                });
                var linkToEntries = $(ctrl.id.replace('li', 'link')).href;
                ctrl.down('.view').href = linkToEntries.replace('entries', 'forms');
            }
            ctrl.addClassName('hover');
        }
    },
    hideActions: function (ctrl) {
        var ctrl = $(ctrl);
        if (ctrl.down) {
            ctrl.removeClassName('hover');
        }
    },
    editForm: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=build/' + form.Url + '';
    },
    viewEntries: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=entries/' + form.Url + '';
    },
    viewCode: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=code/' + form.Url + '';
    },
    viewPayment: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=merchant/' + form.Url + '';
    },
    viewRules: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=rules/' + form.Url + '';
    },
    viewAnalytics: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=analytics/' + form.Url + '';
    },
    viewNotifications: function (formId, ctrl) {
        var form = this.Forms.getForm(formId);
        document.location = 'index.php?req=notifications/' + form.Url + '';
    },
    deleteForm: function (formId) {
        var form = this.Forms.getForm(formId);
        Lightbox.showUrl('lightboxes/Form.Delete.php', function () {
            $('deleteFormId').value = form.FormId;
            $('deleteFormName').innerHTML = form.Name;
        }, 'error');
        return false;
    },
    continueDeletingForm: function () {
        var form = this.Forms.getForm($F('deleteFormId'));
        this.FormEngine.deleteForm(form.FormId, form.Name);
    },
    successfulFormDeletion: function () {
        window.location.reload(true);
        return;
    },
    failedFormDeletion: function (ret) {
        Lightbox.showContent(ret.response.lbmarkup);
    },
    duplicateForm: function (formId) {
        var form = this.Forms.getForm(formId);
        this.FormEngine.duplicateForm(form.FormId, form.Name);
    },
    successfulFormDuplication: function (formId) {
        window.location.reload(true);
        return;
    },
    failedFormDuplication: function (ret) {
        Lightbox.showContent(ret.response.lbmarkup);
    },
    savePublicStatus: function (formId, formName, ctrl) {
        publicStatus = (ctrl.checked) ? 1 : 0;
        this.UI.toggleFormPublicStatus(formId, publicStatus);
        this.FormEngine.savePublicStatus(formId, formName, publicStatus);
    },
    failedSavePublicStatus: function (ret) {
        var ctrl = $('publicStatus_' + ret.response.formId);
        ctrl.checked = !ctrl.checked;
        var publicStatus = (ctrl.checked) ? 1 : 0;
        this.UI.toggleFormPublicStatus(ret.response.formId, publicStatus);
        Lightbox.showContent(ret.response.lbmarkup);
    },
    showProtectForm: function (formId) {
        var form = this.Forms.getForm(formId);
        Lightbox.showUrl('lightboxes/Form.Protect.php?formId=' + form.FormId, function () {
            $('protect_formId').value = form.FormId;
        });
        return false;
    },
    protectForm: function () {
        var form = this.Forms.getForm($('protect_formId').value);
        this.FormEngine.savePassword(form.FormId, form.Name, $('protect_password').value);
    },
    successfulProtectForm: function (ret) {
        Lightbox.close();
    },
    failedProtectForm: function (ret) {
        $('protect_error').innerHTML = ret.response.errorMessage;
        Element.addClassName('protect_error', 'error');
        Element.addClassName('protect_parent', 'error');
    },
    changeTheme: function (formId, formName, ctrl) {
        var form = this.Forms.getForm(formId);
        if (ctrl.options[ctrl.selectedIndex].value == 'create') {
            document.location = 'index.php?req=themes/';
            return false;
        }
        this.FormEngine.saveTheme(formId, formName, ctrl.options[ctrl.selectedIndex].value, form.Url);
    },
    sortBy: function (sort) {
        if (sort != this.sortSetting) {
            this.sortSetting = sort;
            this.UI.toggleSelectedSort(this.sortSetting);
            this.FormEngine.sortForms(this.sortSetting);
            this.createCookie('formManagerOrder', sort);
        }
    },
    filterBy: function (filter) {
        if (filter != this.filterSetting) {
            this.filterSetting = filter;
            this.runFilter();
            this.createCookie('formManagerFilter', filter);
        }
    },
    runFilter: function () {
        this.UI.showSearching(this.filterSetting);
        var zebra = 'alt';
        var matches = false;
        var numberOfMatches = 0;
        $('groupList-1').childElements().each(function (num, index) {
            var form = this.Forms.data[num.id.replace('li', '')];
            if (this.FormEngine.formMatchesFilter(form, this.filterSetting)) {
                matches = true;
                numberOfMatches++;
                $('li' + form.FormId).removeClassName('hide');
                if (num != 0) {
                    if (numberOfMatches == 1) {
                        this.showActions($('li' + form.FormId));
                        $('li' + form.FormId).removeClassName('hover');
                        if (!$('li' + form.FormId).hasClassName('first')) $('li' + form.FormId).addClassName('first');
                    } else {
                        this.hideActions($('li' + form.FormId));
                        $('li' + form.FormId).removeClassName('first');
                    }
                }
                if (zebra == 'alt') {
                    $('li' + form.FormId).addClassName('alt');
                    zebra = '';
                } else {
                    $('li' + form.FormId).removeClassName('alt');
                    zebra = 'alt';
                }
            } else $('li' + form.FormId).addClassName('hide');
        }.bind(this));
        if (!this.filterSetting) {
            this.UI.hideSearching();
        }
        if (!matches) this.UI.showNoResults();
        else this.UI.hideNoResults();
    },
    failedFilterForm: function (ret) {
        Lightbox.showContent(ret.response.lbmarkup);
    },
    successfulFilterForm: function (ret) {
        $('groupList-1').innerHTML = ret.response.formList;
        this.runFilter();
        this.attachEvents();
    },
    loadDefaultStats: function () {
        vals = this.readCookie('stats').split('|');
        found = false;
        for (var i = 0; i < $('allForms').options.length; i++) {
            if ($('allForms').options[i].value == vals[1]) {
                $('allForms').selectedIndex = i;
                found = true;
                break;
            }
        }
        if (!found) $('allForms').selectedIndex = 0;
        Effect.BlindDown('stats', {
            duration: .4,
            afterFinish: function () {
                this.changeType(vals[2], true);
            }.bind(this)
        });
    },
    changeType: function (newType, first) {
        if (newType && newType != this.g_type || first == true) {
            this.g_type = newType;
            Element.removeClassName('todayGraph', 'selected');
            Element.removeClassName('weekGraph', 'selected');
            Element.removeClassName('monthGraph', 'selected');
            Element.removeClassName('yearGraph', 'selected');
            Element.addClassName(newType + 'Graph', 'selected');
            this.refreshGraph();
        }
    },
    refreshGraph: function () {
        cookieVal = 'on|' + $('allForms').options[$('allForms').selectedIndex].value + '|' + this.g_type;
        this.createCookie('stats', cookieVal);
        this.FormEngine.displayGraph($('allForms').options[$('allForms').selectedIndex].value, this.g_type);
    },
    toggleGraph: function () {
        if (!$('stats').offsetHeight || $('stats').offsetHeight == 0) this.showGraph();
        else this.hideGraph();
    },
    showGraph: function () {
        Effect.BlindDown('stats', {
            duration: .4,
            afterFinish: function () {
                this.refreshGraph();
            }.bind(this)
        });
    },
    hideGraph: function () {
        this.createCookie('stats', 'Off');
        $('formGraph').innerHTML = '';
        Effect.BlindUp('stats', {
            duration: .4
        });
    },
    createCookie: function (name, value) {
        createCookie(name, value);
    },
    readCookie: function (name) {
        return readCookie(name);
    },
    approveForm: function(form_id) {
        $('statusText').innerHTML = 'Approving Form';
        var myAjax = new Ajax.Request(new String(document.location), {
            method: 'post',
            parameters: 'action=approveform&formId=' + form_id,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                if(ret.success == true) {
                    $('li'+form_id).remove();
                } else {
                    alert('error occured');
                }
            }.bind(this)
        });
    },
    unApproveForm: function(form_id) {
        $('statusText').innerHTML = 'Approving Form';
        var myAjax = new Ajax.Request(new String(document.location), {
            method: 'post',
            parameters: 'action=unapproveform&formId=' + form_id,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                if(ret.success == true) {
                    $('li'+form_id).remove();
                } else {
                    alert('error occured');
                }
            }.bind(this)
        });
    },
    getApprovedForm: function() {
        $('statusText').innerHTML = 'Getting Approved Forms';
        var params = [];
        var $htmlContainer = $('groupList-1');
        var $pagedContainer = $$('.navHolder')[0];
        params['action'] = 'getForms',
        params['sortby'] = $('sort').select('.selected')[0].id,
        params['direction'] = 'desc',
        params['approveType'] = 'y',
        params['category_id'] = $('category_list').select('.active')[0].id.replace('cat_', '');
        params['paged'] = 1;
        this.getForms(params, $htmlContainer, $pagedContainer);
        return;
    },
    getUnApprovedForm: function() {
        $('statusText').innerHTML = 'Getting UnApproved Forms';
        var params = [];
        var $htmlContainer = $('groupList-1');
        var $pagedContainer = $$('.navHolder')[0];
        params['action'] = 'getForms',
        params['sortby'] = $('sort').select('.selected')[0].id,
        params['direction'] = 'desc',
        params['approveType'] = 'n',
        params['category_id'] = $('category_list').select('.active')[0].id.replace('cat_', '');
        params['paged'] = 1;
        this.getForms(params, $htmlContainer, $pagedContainer);
        return;
    },
    goToPage: function($paged,$page) {
        $('statusText').innerHTML = 'Getting Forms';
        var params = [];
        var $upPage = $page.up().up();
        var $htmlContainer = $upPage.up().previous();
        var $pagedContainer = $upPage;
        params['action'] = 'getForms',
        params['sortby'] = $('sort').select('.selected')[0].id,
        params['direction'] = 'desc',
        params['approveType'] = $$('.approveButton.selected')[0].rel,
        params['category_id'] = $('category_list').select('.active')[0].id.replace('cat_', '');
        params['paged'] = $paged;
        this.getForms(params, $htmlContainer, $pagedContainer);
        return;
    },
    getFormsByCategory: function(category_id) {
        $('statusText').innerHTML = 'Getting Forms';
        var aclick = false;
        var params = [];
        var $htmlContainer = $('groupList-1');
        var $pagedContainer = $$('.navHolder')[0];
        params['action'] = 'getForms',
        params['sortby'] = $('sort').select('.selected')[0].id,
        params['direction'] = 'desc',
        params['approveType'] = $$('.approveButton.selected')[0].rel,
        params['category_id'] = category_id;
        params['paged'] = 1;
        this.getForms(params, $htmlContainer, $pagedContainer);
        return;
    },
    getForms: function(params, $htmlContaiter, $pagedContainer) {
        var action = params['action'];
        var sortby = params['sortby'];
        var direction = params['direction'];
        var approveType = params['approveType'];
        var category_id = params['category_id'];
        var $paged = params['paged'];
        var pars='action='+action+'&paged='+encodeURIComponent($paged)+'&sortby='+sortby+'&direction='+direction+'&approveType='+approveType+'&category_id='+category_id;
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == true) {
                    $htmlContaiter.innerHTML = result.response.html;
                    $pagedContainer.innerHTML = result.response.paged;
                    
                    updateFormsJson(result.response.json);
                    
                } else {
                    if(result.message) {
                        lightbox('Some Error Occured', result.message);
                    } else {
                        lightbox('Some Error Occured', 'Unknownerror');
                    }
                    
                }
            }
        });
        return;
    },
    addCategory: function(categories,form_id) {
        $('statusText').innerHTML = 'Adding Categories';
        var pars='action=addCategory&s_categories='+encodeURIComponent(categories)+'&formId='+form_id;
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == true) {
                    Lightbox.close();
                } else {
                    
                }
            }
        });
        return;
    },
    categoryRename: function(category_name, category_id) {
        $('statusText').innerHTML = 'Renaming Category';
        var pars='action=categoryRename&category_id='+encodeURIComponent(category_id)+'&category_name='+category_name;
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == true) {
                    location.reload(true);
                } else {
                    
                }
            }
        });
        return;
    },
    categoryDelete: function(category_id) {
        $('statusText').innerHTML = 'Deleting Category';
        var pars='action=categoryDelete&category_id='+encodeURIComponent(category_id);
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == true) {
                    location.reload(true);
                } else {
                    
                }
            }
        });
        return;
    },
    categoryAdd: function(category_name) {
        $('statusText').innerHTML = 'Adding Category';
        var pars='action=categoryAdd&category_name='+category_name;
        var myAjax = new Ajax.Request(new String(document.location), {
            parameters: pars,
            onComplete: function (response) {
                var result = response.responseText.evalJSON();
                if(result.success == true) {
                    location.reload(true);
                } else {
                    
                }
            }
        });
        return;
    }
});
var __MANAGER, maxScroll;
Event.observe(window, 'load', init, false);

function init() {
    __MANAGER = new FormManager();
    addCategoriesChooice();
    //maxScroll = window.scrollMaxY;
    //addResizeEvent();
    //addScroller();
}

function showActions(el) {
    if (__MANAGER) __MANAGER.showActions(el);
}

function hideActions(el) {
    if (__MANAGER) __MANAGER.hideActions(el);
}

function getFormId(ctrl) {
    return $(ctrl).up('li').id.replace('li', '');
}

function deleteForm(ctrl) {
    __MANAGER.deleteForm(getFormId(ctrl));
}

function continueDeletingForm() {
    __MANAGER.continueDeletingForm();
}

function finishDeletingForm(ret) {
    if (ret.success == 'false') __MANAGER.failedFormDeletion(ret);
    else __MANAGER.successfulFormDeletion();
}

function duplicateForm(ctrl) {
    __MANAGER.duplicateForm(getFormId(ctrl));
}

function finishDuplicatingForm(ret) {
    if (ret.success == 'false') __MANAGER.failedFormDuplication(ret);
    else __MANAGER.successfulFormDuplication();
}

function togglePublicForm(formId, formName, ctrl) {
    __MANAGER.savePublicStatus(formId, formName, ctrl);
}

function finishSavingPublicStatus(ret) {
    if (ret.success == 'false') __MANAGER.failedSavePublicStatus(ret);
}

function showProtectForm(ctrl) {
    __MANAGER.showProtectForm(getFormId(ctrl));
}

function protectForm() {
    __MANAGER.protectForm();
}

function finishProtectForm(ret) {
    if (ret.success == 'false') __MANAGER.failedProtectForm(ret);
    else __MANAGER.successfulProtectForm(ret);
}

function exportMe(ctrl) {
    return __MANAGER.exportForm(getFormId(ctrl));
}

function changeTheme(formId, formName, ctrl) {
    return __MANAGER.changeTheme(formId, formName, ctrl);
}

function editForm(ctrl) {
    __MANAGER.editForm(getFormId(ctrl), ctrl);
}

function viewEntries(ctrl) {
    __MANAGER.viewEntries(getFormId(ctrl), ctrl);
}

function viewCode(ctrl) {
    __MANAGER.viewCode(getFormId(ctrl), ctrl);
}

function viewRules(ctrl) {
    __MANAGER.viewRules(getFormId(ctrl), ctrl);
}

function viewPayment(ctrl) {
    __MANAGER.viewPayment(getFormId(ctrl), ctrl);
}

function viewAnalytics(ctrl) {
    __MANAGER.viewAnalytics(getFormId(ctrl), ctrl);
}

function viewNotifications(ctrl) {
    __MANAGER.viewNotifications(getFormId(ctrl), ctrl);
}

function sortBy(sort) {
    __MANAGER.sortBy(sort);
}

function filterBy(filter) {
    __MANAGER.filterBy(filter);
}

function finishFilteringForm(ret) {
    if (ret.success == 'false') __MANAGER.failedFilterForm(ret);
    else __MANAGER.successfulFilterForm(ret);
}

function toggleGraph() {
    __MANAGER.toggleGraph();
}

function refreshGraph() {
    __MANAGER.refreshGraph();
}

function changeType(type) {
    __MANAGER.changeType(type);
}

function RGBColor(color_string) {
    this.ok = false;
    if (color_string.charAt(0) == '#') {
        color_string = color_string.substr(1, 6);
    }
    color_string = color_string.replace(/ /g, '');
    color_string = color_string.toLowerCase();
    var simple_colors = {
        antiquewhite: 'faebd7',
        aqua: '00ffff',
        aquamarine: '7fffd4',
        azure: 'f0ffff',
        beige: 'f5f5dc',
        black: '000000',
        blue: '0000ff',
        blueviolet: '8a2be2',
        brown: 'a52a2a',
        cadetblue: '5f9ea0',
        chartreuse: '7fff00',
        chocolate: 'd2691e',
        coral: 'ff7f50',
        cornflowerblue: '6495ed',
        crimson: 'dc143c',
        cyan: '00ffff',
        darkblue: '00008b',
        darkgray: 'a9a9a9',
        darkgreen: '006400',
        darkorange: 'ff8c00',
        darkred: '8b0000',
        darkviolet: '9400d3',
        floralwhite: 'fffaf0',
        forestgreen: '228b22',
        fuchsia: 'ff00ff',
        gold: 'ffd700',
        goldenrod: 'daa520',
        gray: '808080',
        green: '008000',
        greenyellow: 'adff2f',
        hotpink: 'ff69b4',
        indianred: 'cd5c5c',
        indigo: '4b0082',
        ivory: 'fffff0',
        khaki: 'f0e68c',
        lavender: 'e6e6fa',
        lawngreen: '7cfc00',
        lightblue: 'add8e6',
        lightgrey: 'd3d3d3',
        lightgreen: '90ee90',
        lightpink: 'ffb6c1',
        lightyellow: 'ffffe0',
        lime: '00ff00',
        limegreen: '32cd32',
        linen: 'faf0e6',
        magenta: 'ff00ff',
        maroon: '800000',
        midnightblue: '191970',
        moccasin: 'ffe4b5',
        navy: '000080',
        olive: '808000',
        orange: 'ffa500',
        orangered: 'ff4500',
        palegreen: '98fb98',
        pink: 'ffc0cb',
        plum: 'dda0dd',
        powderblue: 'b0e0e6',
        purple: '800080',
        red: 'ff0000',
        royalblue: '4169e1',
        salmon: 'fa8072',
        sandybrown: 'f4a460',
        seagreen: '2e8b57',
        sienna: 'a0522d',
        silver: 'c0c0c0',
        skyblue: '87ceeb',
        slateblue: '6a5acd',
        slategray: '708090',
        snow: 'fffafa',
        steelblue: '4682b4',
        tan: 'd2b48c',
        teal: '008080',
        tomato: 'ff6347',
        turquoise: '40e0d0',
        violet: 'ee82ee',
        violetred: 'd02090',
        white: 'ffffff',
        yellow: 'ffff00',
        yellowgreen: '9acd32'
    };
    for (var key in simple_colors) {
        if (color_string == key) {
            color_string = simple_colors[key];
        }
    }
    var color_defs = [{
            re: /^rgb\((\d{1,3}),\s*(\d{1,3}),\s*(\d{1,3})\)$/,
            example: ['rgb(123, 234, 45)', 'rgb(255,234,245)'],
            process: function (bits) {
                return [parseInt(bits[1]), parseInt(bits[2]), parseInt(bits[3])];
            }
        }, {
            re: /^(\w{2})(\w{2})(\w{2})$/,
            example: ['#00ff00', '336699'],
            process: function (bits) {
                return [parseInt(bits[1], 16), parseInt(bits[2], 16), parseInt(bits[3], 16)];
            }
        }, {
            re: /^(\w{1})(\w{1})(\w{1})$/,
            example: ['#fb0', 'f0f'],
            process: function (bits) {
                return [parseInt(bits[1] + bits[1], 16), parseInt(bits[2] + bits[2], 16), parseInt(bits[3] + bits[3], 16)];
            }
        }
    ];
    for (var i = 0; i < color_defs.length; i++) {
        var re = color_defs[i].re;
        var processor = color_defs[i].process;
        var bits = re.exec(color_string);
        if (bits) {
            channels = processor(bits);
            this.r = channels[0];
            this.g = channels[1];
            this.b = channels[2];
            this.ok = true;
        }
    }
    this.r = (this.r < 0 || isNaN(this.r)) ? 0 : ((this.r > 255) ? 255 : this.r);
    this.g = (this.g < 0 || isNaN(this.g)) ? 0 : ((this.g > 255) ? 255 : this.g);
    this.b = (this.b < 0 || isNaN(this.b)) ? 0 : ((this.b > 255) ? 255 : this.b);
    this.toRGB = function () {
        return 'rgb(' + this.r + ', ' + this.g + ', ' + this.b + ')';
    }
    this.toHex = function () {
        var r = this.r.toString(16);
        var g = this.g.toString(16);
        var b = this.b.toString(16);
        if (r.length == 1) r = '0' + r;
        if (g.length == 1) g = '0' + g;
        if (b.length == 1) b = '0' + b;
        return '#' + r + g + b;
    }
}

function approveForm(ctrl) {
    __MANAGER.approveForm(getFormId(ctrl));
}
function unApproveForm(ctrl) {
    __MANAGER.unApproveForm(getFormId(ctrl));
}
function getApprovedForm(ctrl) {
    ctrl.next().removeClassName('selected');
    ctrl.addClassName('selected');
    __MANAGER.getApprovedForm();
}

function getUnApprovedForm(ctrl) {
    ctrl.previous().removeClassName('selected');
    ctrl.addClassName('selected');
    __MANAGER.getUnApprovedForm();
}
function updateFormsJson(json) {
    __MANAGER.Forms = new FormsObject(json);
    console.log(__MANAGER.Forms);
}
function goToPage($paged,$page) {
    __MANAGER.goToPage($paged,$page);
}
function addFormCategories(form_id) {
    Lightbox.showUrl('lightboxes/Category.Add.php?form_id='+form_id, function () {
        
    }, '');
}
function addCategory(form_id) {
    var categories = [];
    $('addCategoryForm').select('#s_categories option').each(function(i) {
        if (i.selected == true) {
            categories.push(i.value);
        }
    });
    __MANAGER.addCategory(categories, form_id);
}
function addCategoriesChooice() {
    categories = $$('#category_list li');
    $('stage').observe('click', function(event) {
        $$('#category_list li .setting_content').invoke('addClassName','hide');
    });
    $('cat_name').observe('keypress', function(event) {
        var key = event.which || event.keyCode;
        switch (key) {
            default:
            break;
            case Event.KEY_RETURN:
                category_name = $('cat_name').value;
                __MANAGER.categoryAdd(category_name);
                event.stop();
            break;   
        }
    });
    categories.each(function(category) {
        var setting = category.select('.setting')[0];
        if(setting) {
            setting.observe('click', function(event) {
                $$('#category_list li .setting_content').invoke('addClassName','hide');
                setting.next().removeClassName('hide');
                event.stop();
            }, false);
        }
        category.select('a')[0].observe('click', function(event) {
            var category_id = category.id.replace('cat_','');
            Cookie.set('cate_id',category_id);
            categories.invoke('removeClassName','active');
            category.addClassName('active');
            __MANAGER.getFormsByCategory(category_id);
            aclick = false;
            event.stop();
        }, false);
    });
}
function category_rename(cate_id) {
    Lightbox.showUrl('lightboxes/Category.Rename.php?category_id='+cate_id, function () {
        
    }, '');
}
function categoryRename(cate_name, cate_id) {
    __MANAGER.categoryRename(cate_name, cate_id);
}
function category_delete(cate_id) {
    Lightbox.showUrl('lightboxes/Category.Delete.php?category_id='+cate_id, function () {
        
    }, '');
}
function categoryDelete(cate_id) {
    __MANAGER.categoryDelete(cate_id);
}
function addResizeEvent() {
    Event.observe(window, 'resize', function(event) {
        maxScroll = window.scrollMaxY;
    });
}
function addScroller() {
    var scroler;
    var nomore = false;
    var footerHeight = maxScroll*25/100;
    var maxValue = maxScroll - footerHeight;
    var curentValue = 0;
    var pars = '', url = new String(document.location);
    Event.observe(window, 'scroll', function(event) {
        if(!nomore) {
            clearTimeout(scroler);
            var value = event.target.viewport.getScrollOffsets();
            if(value.top > curentValue) {
                scroler = setTimeout(function() {
                    if(value.top >= maxValue) {
                        curentValue = value.top;
                        console.log(value.top);
                        var myAjax = new Ajax.Request(url, {
                            parameters: pars,
                            onComplete: function (response) {
                                var result = response.responseText.evalJSON();
                                if(result.success == true) {
                                    nomore = false;
                                } else {
                                    nomore = true;
                                }
                            }
                        });
                    }
                },400);
            }
        }
    });
}