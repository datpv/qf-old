var SubjectsEngineObject = Class.create({
    initialize: function () {
        this.addTabEvent();
    },
    registerSubject: function(ctrl) {
        this.ajax_post('action=registerSubject','finishRegisterSubject');
    },
    finishRegisterSubject: function(ret) {
        if(ret.success) {
            $('registerSubject').remove();
            if(ret.response.status == 'N') {
                $('subject_actions').insert({
                    bottom: '<span class="button pending">PENDING</span>'
                })
            } else if(ret.response.status == 'Y') {
                $('subject_actions').insert({
                    bottom: '<span class="button unlocked">UNLOCKED</span>'
                })
            }
        } else {
            if(ret.response.message) {
                alert(ret.response.message);
            } else {
                alert('Error Occured!');
            }
        }
    },
    addTabEvent: function() {
        this.addTabClick();
    },
    addTabClick: function() {
        var tabs = $('tabs').select('li');
        tabs.each(function(a,b) {
            if(a.down('a')) {
                var rel = a.down('a').rel;
                if(rel) {
                    a.observe('click', function(event) {
                        $$('.subject_tab').invoke('addClassName','hide');
                        tabs.invoke('removeClassName','selected');
                        a.addClassName('selected');
                        $(rel).removeClassName('hide');
                        event.stop();
                    });
                }
            }
        });
    },
    ajax_post: function(data,callback) {
        var myAjax = new Ajax.Request(new String(document.location), {
            method: 'post',
            parameters: data,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                if(callback) {
                    var fn = window[callback];
                    if(typeof fn === 'function') {
                        fn(ret);
                    }
                }
            }.bind(this)
        });
    }
});
var SUBJ_OBJ;
Event.observe(window, 'load', init, false);
function init() {
    SUBJ_OBJ = new SubjectsEngineObject();
};
function registerSubject(ctrl) {
    SUBJ_OBJ.registerSubject(ctrl);
}
function finishRegisterSubject(ret) {
    SUBJ_OBJ.finishRegisterSubject(ret);
}