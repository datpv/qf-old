var SubjectsEngineObject = Class.create({
    isNew: 0,
    forms: '',
    sforms: '',
    subject: '',
    outline: {},
    renameTabHtml: '',
    currentForm: '',
    initialize: function() {
        this.forms = __FORMS;
        this.sforms = __SFORMS;
        this.isNew = SUBJ_NEW;
        this.subject = __SUBJECT;
        this.addRenameTabEvent();
        this.setFormOrder();
        this.addSortable();
        this.ckeInit();
        this.saveInit();
        this.outlineInit();
        this.addTabEvent();
        this.initCalendars();
        this.populate();
    },
    populate: function (Form, Profile) {
        this.setDates();
        this.setLimitUser();
        this.setPrice();
    },
    setFormOrder: function() {
        var form_holder = $('form_holder_col');
        this.subject['FormOrder'].each(function(a,b) {
            form_holder.insert({
                bottom: $('sform_' + a)
            });
        });
    },
    setPrice: function() {
        $('formPrice').value = this.subject.Price;
    },
    setLimitUser: function() {
      $('formLimitUser').value =  this.subject.LimitUser; 
    },
    setDates: function() {
        var starting = this.subject.ExpireDate;
        if (starting != '0000-00-00') {
            temp = starting.split(' ');
            sDate = temp[0];
            sTime = temp[1];
            this.setExpireDate(sDate);
        }
    },
    setExpireDate: function (sDate) {
        sDateArray = sDate.split('-');
        $('startDate-1').value = sDateArray[1];
        $('startDate-2').value = sDateArray[2];
        $('startDate').value = sDateArray[0];
        if (sDate == '2000-01-01') {
            $('startDate-1').value = '';
            $('startDate-2').value = '';
            $('startDate').value = '';
        }
    },
    updateSubject: function(key, value) {
        switch (key) {
            case 'ExpireDate':
                if (value == 'live') {
                    var d = $F('startDate') + '-' + $F('startDate-1') + '-' + $F('startDate-2');
                    if (d == '--') d = '2000-01-01';
                    //var hour = $F('startTime-0');
                    //if ($F('startTime-2') == 'PM') hour = parseInt(hour) + 12;
                    //var t = hour + ':' + $F('startTime-1') + ':00';
                    value = d;// + ' ' + t;
                }
                break;
            case 'ThumbnailUrl':
                $('thumbnail_image').src = value;
                break;
            default:
                break;
        }
        this.subject[key] = value;
        console.log(this.subject);
    },
    initCalendars: function() {
        Calendar.setup({
            inputField: "startDate",
            displayArea: "calStart",
            button: "pickStart",
            ifFormat: "%B %e, %Y",
            onSelect: selectFormActivity
        });
    },
    addForms: function() {
        var outline = this.outline;
        var data = [];
        for(var i in outline) {
            data.push(outline[i].FormId);
        }
        var callback = 'finishSaveOutline';
        this.saveOutline(data,callback);
    },
    saveOutline: function(data,callback) {
        save('action=saveOutline&data='+encodeURIComponent(Object.toJSON(data)),callback);
    },
    finishSaveOutline: function(ret) {
        if(ret.success) {
            var outline = this.outline;
            for(var i in outline) {
                var formID = 'form_' + outline[i].FormId;
                var form = $(formID);
                form.observe('click', function(event) {
                    if (this.readAttribute("data-disabled")) {
                        event.stop();
                    }
                });
                form.setAttribute('data-disabled', 1);
                form.addClassName('data-disabled');
                var newLi = new Element('li',{
                   'id':  'sform_' + outline[i].FormId,
                   'class': 'dragable'
                }).update('<a class="form_title" href="#">'+outline[i].Name+'</a><span class="hold"><a class="button" onclick="deleteForm(this); return false;" href="#"><img title="Delete." alt="Delete." src="template/images/icons/delete.png" class="faDel" /></a></span>');
                $('form_holder_col').insert({
                    bottom: newLi
                });
                this.sforms[outline[i].FormId] = outline[i];
                this.reloadFormOrder();
                this.addFormHolderSortable();
            }
            this.outline = {};
        }
    },
    deleteForm: function(form_id) {
        var callback = 'finishDeleteOutline';
        this.currentForm = form_id;
        this.deleteOutline(form_id,callback);
    },
    deleteOutline: function(form_id,callback) {
        save('action=deleteOutline&form_id='+encodeURIComponent(form_id),callback);
    },
    finishDeleteOutline: function(ret) {
        if(ret.success) {
            var form = this.getSForm(this.currentForm);
            if($('form_' + this.currentForm)) {
                $('form_' + this.currentForm).removeAttribute('data-disabled');
                $('form_' + this.currentForm).removeClassName('data-disabled');
                this.outlineInit();
            } else {
                var newLi = new Element('li',{
                   'id':  'form_' + form.FormId
                }).update('<a href="index.php?req=forms/'+form.Url+'" class="form_title" target="_blank">'+form.Name+'</a><span class="hold"><input type="checkbox" class="checkbox" onclick="(this.checked) ? addForm(this) : removeForm(this);"></span>');
                $('form_list_col').insert({
                    top: newLi
                });
                
            }
            delete this.sforms[this.currentForm];
            this.forms[this.currentForm] = form;
            $('sform_' + this.currentForm).remove();
            this.reloadFormOrder();
        }
    },
    addForm: function(form) {
        this.outline[form.FormId] = form;
    },
    removeForm: function(form_id) {
        delete this.outline[form_id];
    },
    getForm: function(formId) {
        if(this.forms[formId]) {
            return this.forms[formId];
        }
        return false;
    },
    getSForm: function(formId) {
        if(this.sforms[formId]) {
            return this.sforms[formId];
        }
        return false;
    },
    resetChecked: function(list) {
        list.each(function(a) {
            a.checked = 0;
        });
    },
    outlineInit: function() {
        var list = $$('#form_list li:not(.data-disabled) input[type=checkbox]');
        this.resetChecked(list);
    },
    addSortable: function() {
        this.addFormHolderSortable();
    },
    addFormHolderSortable: function() {
        if($("form_holder_col")) {
            Sortable.create("form_holder_col", {
                dropOnEmpty: false,
                containment: ["form_holder_col","form_list_col"],
                constraint: "vertical",
                scroll: window,
                only: 'dragable',
                onUpdate: reloadFormOrder
            });
        }
        if($("form_holder_col")) $("form_holder_col").select('li a').each(function(a,b) {
            a.observe('click',function(event) {
                event.stop();
            });
        })
    },
    reloadFormOrder: function() {
        var form_list = $('form_holder_col').select('li.dragable');
        var FormOrder = [];
        form_list.each(function(a,b) {
            var form_id = a.id.replace('sform_','');
            FormOrder.push(form_id);
        });
        this.subject['FormOrder'] = FormOrder;
    },
    saveInit: function() {
        var subject = this.subject;
        $('saveSubject').observe('click', function(event) {
            var data = {};
            for(var i in CKEDITOR.instances) {
                var editor = CKEDITOR.instances[i],
                    element = editor.element;
                var parentNode = $(editor.element.$).up('form');
                var parentNodePrettyName = parentNode.id.replace('subject_tab_','');
                var prefix = 'subject_' + parentNodePrettyName;
                if(editor.checkDirty()) {
                    if(!data[prefix]) {
                        data[prefix] = {};
                    }
                    if(!data[prefix]['tab_name']) {
                       data[prefix]['tab_name'] = String.trim( $('tabs').select('a[rel="'+parentNode.id+'"]')[0].innerHTML ); 
                    }
                    
                    if(element.getAttribute('class').indexOf('title') > -1) {
                        data[prefix]['title'] = editor.getData();
                    } else if(element.getAttribute('class').indexOf('content') > -1) {
                        data[prefix]['content'] = editor.getData();
                    }
                }
            }
            if(!data['subject_outline']) {
                data['subject_outline'] = {};
            }
            save('action=save&data='+encodeURIComponent(Object.toJSON(data))+'&settings='+encodeURIComponent(Object.toJSON(subject)),'finishSaveSubject');
            event.stop();
        });
    },
    finishSaveSubject: function(ret) {
        if (ret.success == true) Lightbox.showUrl('lightboxes/SubjectBuilder.Wdywtdn.php?url=' + ret.response.url + '&id=' + ret.response.id, false, 'modal');
        else if (ret.success == false) {
            if(ret.message) alert(ret.message);
            else alert('some error occured!');
        }
    },
    ckeInit: function() {
        CKEDITOR.disableAutoInline = true;
    },
    addTabEvent: function() {
        this.addTabAdd();
        this.addTab();
        this.addTabClick();
    },
    deleteTab: function(ctrl) {
        var tab_key = $(ctrl).up('li').down('a.tab_name').rel.replace('subject_tab_','');
        this.ajax_post('action=deleteTab&tab_key='+encodeURIComponent(tab_key),'finshDeleteTab');
    },
    addRenameTabEvent: function() {
        this.renameTabHtml = $('tab_rename');
        
        var $this = this;
        $('tab_rename_btn').observe('click', function(event) {
            $('statusText').innerHTML = "Renaming";
            $this.renameTabSend(this);
            event.stop();
        });
        $('tab_rename').remove();
    },
    renameTab: function(ctrl) {
        var tagRenameContainer = this.renameTabHtml;
        $(ctrl).up('li').down('.tab_settings').insert(tagRenameContainer);
        if(tagRenameContainer.hasClassName('hide')) {
            tagRenameContainer.removeClassName('hide');
        } else {
            tagRenameContainer.addClassName('hide');
        }
        
        $(document).observe('click', function(event) {
            if (!event.target.descendantOf('tab_rename')) {
                tagRenameContainer.addClassName('hide');
            }
        });
    },
    renameTabSend: function(ctrl) {
        var tab_key = ctrl.up('li').down('a.tab_name').rel.replace('subject_tab_','');
        var tab_name = $('tab_rename_input').value;
        if(tab_name != '') {
            this.ajax_post('action=renameTab&tab_key='+encodeURIComponent(tab_key)+'&tab_name='+encodeURIComponent(tab_name),'finshRenameTab');
        }
        
    },
    finshRenameTab: function(ret) {
        if(ret.success) {
            var tab_id = 'subject_tab_'+ret.response.tab_key;
            $('tabs').select('li a[rel='+tab_id+']')[0].innerHTML = ret.response.tab_name;
            $('tab_rename_input').value = '';
        }
    },
    finshDeleteTab: function(ret) {
        if(ret.success) {
            var tab_id = 'subject_tab_'+ret.response.tab_key;
            $(tab_id).remove();
            $('tabs').select('li a[rel='+tab_id+']')[0].up('li').remove();
            $('tabs').select('li a[rel=subject_tab_content]')[0].click();
        }
    },
    addTabAdd: function() {
        var tabAddBtn = $('add_tab');
        var toggle = 0;
        var tabNameId = 'add_tab_name';
        var tagNameContainer = $(tabNameId);
        tabAddBtn.observe('click', function(event) {
            if(toggle == 0) {
                tagNameContainer.removeClassName('hide');
                toggle = 1;
            } else {
                tagNameContainer.addClassName('hide');
                toggle = 0;
            }
            event.stop();
        });
        $(document).observe('click', function(event) {
            if (!event.target.descendantOf(tabNameId)) {
                tagNameContainer.addClassName('hide');
                toggle = 0;
            }
        });
    },
    addTab: function() {
        var $this = this;
        $('tab_name_btn').observe('click', function(event) {
            $('statusText').innerHTML = "Getting Data";
            var key_name = $('tab_name_input').value;
            if(key_name != '') {
                var data = 'action=checkKey&key_name='+encodeURIComponent(key_name);
                var callback = 'finishCheckTab';
                checkKey(data,callback);
            } else {
                $this.checkTabError();
            }
            event.stop();
        });
    },
    checkKey: function(data,callback) {
        $('statusText').innerHTML = "Checking Data";
        this.ajax_post(data,callback);
    },
    finishCheckTab: function(ret) {
        if(ret.success) {
            
            var tabId = 'subject_tab_' + ret.response.key_id;
            var tabName = $('tab_name_input').value;
            if($(tabId)) {
                this.checkTabError();
                return;
            };
            var tabBtn = new Element('li').update('<a class="tab_name" href="#'+tabId+'" rel="'+tabId+'">'+tabName+'</a>');
            $('add_tab').up('li').insert({
                before: tabBtn
            });
            
            var tabForm = new Element('form',{
                'class': 'subject_tab hide',
                'id': tabId,
                'action': '#'
            }).update('<h3 class="stand title" contenteditable="true">Empty Title</h3><div class="content" contenteditable="true">Empty Content</div>');
            $('tabs_content').insert(tabForm);
            this.addTabClick();
            $('add_tab').click();
            tabBtn.down('a').click();
            $('tab_name_input').value = '';
            var contenteditables = $(tabId).select('*[contenteditable]');
            contenteditables.each(function(a,b) {
                CKEDITOR.inline( a );
            });
        } else {
            this.checkTabError();
        }
    },
    checkTabError: function() {
        $('tab_name_input').style.backgroundColor = '#F40000';
        setTimeout(function() {
            $('tab_name_input').style.backgroundColor = '#ffffff';
        },2500);
    },
    addTabClick: function() {
        var tabs = $('tabs').select('li');
        tabs.each(function(a,b) {
            if(a.down('a.tab_name')) {
                var rel = a.down('a.tab_name').rel;
                if(rel) {
                    a.observe('click', function(event) {
                        $$('.subject_tab').invoke('addClassName','hide');
                        $$('.tab_settings').invoke('addClassName','hide');
                        $('outline_builder').addClassName('hide');
                        if(rel == 'subject_tab_outline') {
                            $('outline_builder').removeClassName('hide');
                        }
                        tabs.invoke('removeClassName','selected');
                        a.addClassName('selected');
                        if(a.down('.tab_settings')) a.down('.tab_settings').removeClassName('hide');
                        $(rel).removeClassName('hide');
                        event.stop();
                    });
                }
            }
        });
    },
    showMoreForms: function(ctrl) {
        if($(ctrl).hasClassName('loadding')) {
            return;
        }
        $(ctrl).addClassName('loadding');
        var form_ids = [];
        for(var i in this.forms) {
            form_ids.push(i);
        }
        for(var i in this.sforms) {
            if(!in_array(form_ids,i)) {
                form_ids.push(i);
            }
        }
        this.ajax_post('action=getForms&forms='+encodeURIComponent(Object.toJSON(form_ids)),'finishShowMoreForms');
    },
    finishShowMoreForms: function(ret) {
        if(ret.success) {
            if(ret.response.total > 0) {
                $$('.load_forms')[0].removeClassName('loadding');
                $$('.load_forms i')[0].innerHTML = ret.response.total;
            } else {
                $$('.load_forms')[0].addClassName('hide');
            }
            
            $('form_list_col').insert(ret.response.html);
            this.forms = merge_options(this.forms,ret.response.json);
        }
    },
    ajax_save: function(data,callback) {
        $('statusText').innerHTML = "Saving Data";
        this.ajax_post(data,callback);
    },
    ajax_post: function(data,callback) {
        var myAjax = new Ajax.Request(new String(document.location), {
            method: 'post',
            parameters: data,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                if(callback) {
                    var fn = window[callback];
                    if(typeof fn === 'function') {
                        fn(ret);
                    }
                }
            }.bind(this)
        });
    }
});
var SUBJ_OBJ;
Event.observe(window, 'load', init, false);
function init() {
    SUBJ_OBJ = new SubjectsEngineObject();
};
function merge_options(obj1,obj2){
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
}
function in_array(array, value) {
    for(var i=0;i<array.length;i++) {
        if(array[i] === value) {
            return true;
        }
    }
    return false;
}
function save(data,callback) {
    SUBJ_OBJ.ajax_save(data,callback);
}
function finishCheckTab(ret) {
    SUBJ_OBJ.finishCheckTab(ret);
}
function checkKey(data,callback) {
    SUBJ_OBJ.checkKey(data,callback);
}
function getForm(ctrl) {
    var formId = $(ctrl).up('li').id.replace('form_','');
    return SUBJ_OBJ.getForm(formId);
}
function getSForm(ctrl) {
    var formId = $(ctrl).up('li').id.replace('sform_','');
    return SUBJ_OBJ.getSForm(formId);
}
function addForm(ctrl) {
    var form = getForm(ctrl);
    if(form) SUBJ_OBJ.addForm(form);
}
function removeForm(ctrl) {
    var form = getForm(ctrl);
    if(form) SUBJ_OBJ.removeForm(form.FormId);
}
function deleteForm(ctrl) {
    var form = getSForm(ctrl);
    if(form) SUBJ_OBJ.deleteForm(form.FormId);
}
function addForms() {
    SUBJ_OBJ.addForms();
}
function finishSaveOutline(ret) {
    SUBJ_OBJ.finishSaveOutline(ret);
}
function finishDeleteOutline(ret) {
    SUBJ_OBJ.finishDeleteOutline(ret);
}
function updateSubject(key, value) {
    SUBJ_OBJ.updateSubject(key, value);
}
function selectFormActivity(cal) {
    var p = cal.params;
    var update = (cal.dateClicked || p.electric);
    year = p.inputField.id;
    day = year + '-2';
    month = year + '-1';
    document.getElementById(month).value = cal.date.print('%m');
    document.getElementById(day).value = cal.date.print('%e');
    document.getElementById(year).value = cal.date.print('%Y');
    if (year == 'startDate') updateSubject('ExpireDate', 'live');
}
function updateSubjectTitle(ctrl) {
    $('subject_title').innerHTML = $(ctrl).innerHTML;
}
function renameTab(ctrl) {
    SUBJ_OBJ.renameTab(ctrl);
}
function deleteTab(ctrl) {
    var confirm = window.confirm('You Sure Want To Delete?');
    if(!confirm) return;
    SUBJ_OBJ.deleteTab(ctrl);
}
function finshDeleteTab(ret) {
    SUBJ_OBJ.finshDeleteTab(ret);
}
function finshRenameTab(ret) {
    SUBJ_OBJ.finshRenameTab(ret);
}
function reloadFormOrder() {
    SUBJ_OBJ.reloadFormOrder();
}
function showMoreForms(ctrl) {
    SUBJ_OBJ.showMoreForms(ctrl);
}
function finishShowMoreForms(ret) {
    SUBJ_OBJ.finishShowMoreForms(ret);
}
function finishSaveSubject(ret) {
    SUBJ_OBJ.finishSaveSubject(ret);
}