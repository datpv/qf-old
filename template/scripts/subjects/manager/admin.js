var SubjectsEngineObject = Class.create({
    initialize: function() {
    },
    deleteSubject: function(subject_id) {
        this.ajax_post('action=deleteSubject&subject_id='+subject_id,'finishDeleteSubject');
    },
    finishDeleteSubject: function(ret) {
        if(ret.success) {
            location.reload(true);
        }
    },
    ajax_post: function(data,callback) {
        var myAjax = new Ajax.Request(new String(document.location), {
            method: 'post',
            parameters: data,
            onComplete: function (r) {
                ret = r.responseText.evalJSON();
                if(callback) {
                    var fn = window[callback];
                    if(typeof fn === 'function') {
                        fn(ret);
                    }
                }
            }.bind(this)
        });
    }
});
var SUBJ_OBJ;
Event.observe(window, 'load', init, false);
function init() {
    SUBJ_OBJ = new SubjectsEngineObject();
};
function getSubjectId(ctrl) {
    return $(ctrl).up('li').id.replace('li_','');
}
function deletesubject(ctrl){
    SUBJ_OBJ.deleteSubject(getSubjectId(ctrl));
}
function finishDeleteSubject(ret) {
    SUBJ_OBJ.finishDeleteSubject(ret);
}