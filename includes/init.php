<?php
    function autoLoadClasses($class_name) {
        $classFile = str_replace( '_', DIRECTORY_SEPARATOR, $class_name );
        $classPI = pathinfo( $classFile );
        $classPath = strtolower( $classPI[ 'dirname' ] );
        include_once($classPath . DIRECTORY_SEPARATOR . $classPI[ 'filename' ] . '.class.php');
    }
    spl_autoload_register('autoLoadClasses');
    
    $registry = new Applications_Registry;
    
    $registry->options = $options;
    
    $registry->db = new Models_DatabaseModel($registry);
    
    $registry->mysqli = $registry->db->connect($db['db_host'], $db['db_user'], $db['db_pass'], $db['db_name']);
    
    $registry->helpers = new Applications_Helpers($registry);
    
    $registry->router = new Applications_Router($registry);
    
    $registry->template = new Applications_Template($registry);
    
    