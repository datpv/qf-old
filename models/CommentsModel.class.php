<?php
if ( ! defined( 'GETOVER' ) ) exit;
    class Models_CommentsModel extends Models_DatabaseModel {
        private $table = 'comments', $table_log_meta = 'forms_log_meta';
        function getComments($select = '*', $where = '', $order = '', $dir = '', $limit = '') {
            return $this->getEntities($select,$this->table,$where,$order,$dir,$limit);
        }
        function getComment($where = '') {
            return $this->select('*', $this->table, $where);
        }
        function getCommentUser($where = '') {
            //return $this->select('*', $this->table_log_user, $where);
        }
        function getCommentUsers($select = '*', $where = '', $order = '', $dir = '', $limit = '') {
            //return $this->getEntities($select,$this->table_log_user,$where,$order,$dir,$limit);
        }
        function getCommentsRows($where = '') {
            return $this->getRows($this->table,$where);
        }
        function updateComment($updates, $where) {
            return $this->update($this->table,$updates,$where);
        }
        function deleteComment($where) {
            return $this->delete($this->table,$where);
        }
        function deleteCommentMeta($where) {
            return $this->delete($this->table_log_meta,$where);
        }
        function insertComment($fields,$values,$where = '') {
            return $this->insert($this->table, $fields, $values, $where);
        }
        function insertCommentMeta($fields,$values,$where = '') {
            return $this->insert($this->table_log_meta, $fields, $values, $where);
        }
        function getCommentsOfLog($log_id, $c_ids, $order = '', $dir = '', $limit = '') {
            $table = $this->db_prefix . $this->table;
            $table_log_meta = $this->db_prefix . $this->table_log_meta;
            if(!empty($order)) {
                $order = 'ORDER BY `'.$order.'`';
            }
            if(!empty($limit)) {
                $limit = 'LIMIT '.$limit;
            }
            return $this->fetchAll("SELECT SQL_CALC_FOUND_ROWS * FROM $table a JOIN $table_log_meta b ON a.`comment_id` = b.`comment_id` WHERE b.`log_id` = '$log_id' AND b.`comment_id` NOT IN ($c_ids) $order $dir $limit");
        }
    }