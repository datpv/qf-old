<?php
if ( ! defined( 'GETOVER' ) ) exit;
    class Models_ReportsModel extends Models_DatabaseModel {
        private $table = 'reports';
        function getReport($uuid, $id, $field_uuid = 'report_uuid', $field_id = 'report_id', $extraWhere = '') {
            return $this->getEntityById('*', $this->table, $uuid, $id, $field_uuid, $field_id, $extraWhere);
        }
        function getReports($where = '', $order = '', $dir = '', $limit = '') {
            return $this->getEntities('*',$this->table,$where,$order,$dir,$limit);
        }
        function insertReport($fields, $values, $where = '') {
            return $this->insertEntity($this->table,$fields,$values,$where);
        }
        function updateReport($updates, $where = '', $limit = '') {
            return $this->updateEntity($this->table, $updates, $where, $limit);
        }
        function deleteReport($uuid, $id, $field_uuid = 'report_uuid', $field_id = 'report_id') {
            return $this->deleteEntityById($this->table, $uuid, $id, $field_uuid, $field_id,'');
        }
        function updateProtectPassword($password, $id) {
            return $this->update($this->table,"`report_password` = '$password'","`report_id` = '$id'");
        }
        function updatePublicStatus($status, $id) {
            return $this->update($this->table,"`report_status` = '$status'","`report_id` = '$id'");
        }
    }