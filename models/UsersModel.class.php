<?php
if ( ! defined( 'GETOVER' ) ) exit;
    class Models_UsersModel extends Models_DatabaseModel {
        private $table = 'users';
        function getUsers($select = '*', $where = '', $order = '', $dir = '', $limit = '') {
            return $this->getEntities($select,$this->table,$where,$order,$dir,$limit);
        }
        function getUser($where = '') {
            return $this->select('*', $this->table, $where);
        }
        function getUsersRows($where = '') {
            return $this->getRows($this->table,$where);
        }
        function updateUser($updates, $where) {
            return $this->update($this->table,$updates,$where);
        }
        function deleteUser($where) {
            return $this->delete($this->table,$where);
        }
        function isDuplicate($contents) {
            $result = $this->fetch("SELECT * FROM {$this->db_prefix}users WHERE `user_name` = '$contents[LoginName]' OR `user_email` = '$contents[Email]'");
            return !empty($result)?true:false;
        }
        function insertUser($fields,$values,$where = '') {
            $where = '';
            return $this->insert($this->table, $fields, $values, $where);
        }
        function userLogin($user, $pass) {
            return $this->fetch("SELECT * FROM {$this->db_prefix}users WHERE `username` = '$user' AND `password` = '$pass' AND `user_status` = 'A' LIMIT 1");
        }
    }