<?php
if ( ! defined( 'GETOVER' ) ) exit;
    class Models_EntriesModel extends Models_DatabaseModel {
        private $table = 'entries', $table_meta = 'entries_meta';
        function insertEntry($fields, $values, $where = '') {
            return $this->insertEntity($this->table,$fields,$values,$where);
        }
        function insertEntryMeta($fields, $values, $where = '') {
            return $this->insertEntity($this->table_meta,$fields,$values,$where);
        }
        function updateEntry($updates, $where, $limit = '') {
            return $this->update($this->table,$updates,$where,$limit);
        }
        function getEntry($uuid = '', $id = '', $field_uuid = 'entry_uuid', $field_id = 'entry_id', $extraWhere = '') {
            return $this->getEntityById('*', $this->table, $uuid, $id, $field_uuid, $field_id, $extraWhere);
        }
        function getEntryMeta($where = '') {
            return $this->select('*',$this->table_meta,$where);
        }
        function getEntries($where = '', $order = '', $dir = '', $limit = '') {
            return $this->getEntities('*',$this->table,$where,$order,$dir,$limit);
        }
        function getEntriesMeta($where = '', $order = '', $dir = '', $limit = '') {
            return $this->getEntities('*',$this->table_meta,$where,$order,$dir,$limit);
        }
        function getEntriesNumber($form_id) {
            return $this->getRows('entries_meta',"`form_id` = '$form_id'");
        }
        function deleteEntry($where) {
            return $this->delete($this->table,$where);
        }
        function deleteEntries($where) {
            return $this->deleteEntitiesByIds($this->table,$where);
        }
        function deleteEntriesMeta($where) {
            return $this->deleteEntitiesByIds($this->table_meta,$where);
        }
        function deleteEntryMeta($uuid, $id, $field_uuid = '', $field_id = '') {
            return $this->deleteEntityById($this->table_meta,$uuid,$id,$field_uuid,$field_id,'');
        }
        function scoringOnce($user_scoring_id, $form_id) {
            return $this->fetch("SELECT * FROM {$this->db_prefix}entries_meta a JOIN {$this->db_prefix}entries b ON a.`entry_id` = b.`entry_id` WHERE b.`entry_user` = '$user_scoring_id' AND b.`entry_type` = 'AS' AND a.`form_id` = '$form_id' LIMIT 1");
        }
        function submitingOnce($user_scoring_id, $form_id) {
            return $this->fetch("SELECT * FROM {$this->db_prefix}entries_meta a JOIN {$this->db_prefix}entries b ON a.`entry_id` = b.`entry_id` WHERE b.`entry_user` = '$user_scoring_id' AND b.`entry_type` = 'BT' AND a.`form_id` = '$form_id' LIMIT 1");
        }
        function entryOnce($form_id) {
            return $this->fetchAll("SELECT * FROM `{$this->db_prefix}entries` a, `{$this->db_prefix}entries_meta` b WHERE a.`entry_id` = b.`entry_id` AND b.`form_id` = '$form_id' LIMIT 1");
        }
        function entriesInForm($form_id) {
            return $this->fetchAll("SELECT * FROM {$this->db_prefix}entries_meta a, {$this->db_prefix}entries b WHERE a.`form_id` = '$form_id' AND a.`entry_id` = b.`entry_id` ORDER BY b.`entry_id` DESC");
        }
    }