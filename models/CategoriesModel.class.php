<?php
if ( ! defined( 'GETOVER' ) ) exit;
    class Models_CategoriesModel extends Models_DatabaseModel {
        private $table = 'categories', $table_meta = 'categories_meta';
        function getCategory($uuid, $id, $field_uuid = '', $field_id = 'category_id', $extraWhere = '') {
            return $this->getEntityById('*', $this->table, $uuid, $id, $field_uuid, $field_id, $extraWhere);
        }
        function getCategories($select = '*',$where = '', $order = '', $dir = '', $limit = '') {
            return $this->getEntities($select,$this->table,$where,$order,$dir,$limit);
        }
        function getCategoriesMeta($select = '*',$where = '', $order = '', $dir = '', $limit = '') {
            return $this->getEntities($select,$this->table_meta,$where,$order,$dir,$limit);
        }
        function insertCategory($fields, $values, $where = '') {
            return $this->insertEntity($this->table,$fields,$values,$where);
        }
        function insertCategoryMeta($fields, $values, $where = '') {
            return $this->insertEntity($this->table_meta,$fields,$values,$where);
        }
        function deleteCategory($where) {
            return $this->delete($this->table,$where);
        }
        function deleteCategories($where) {
            return $this->deleteEntitiesByIds($this->table,$where);
        }
        function deleteCategoriesMeta($where) {
            return $this->deleteEntitiesByIds($this->table_meta,$where);
        }
        function deleteCategoryMeta($uuid, $id, $field_uuid = '', $field_id = '') {
            return $this->deleteEntityById($this->table_meta,$uuid,$id,$field_uuid,$field_id,'');
        }
        function updateCategory($updates, $where) {
            return $this->update($this->table,$updates,$where);
        }
    }