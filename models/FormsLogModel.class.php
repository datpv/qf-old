<?php
if ( ! defined( 'GETOVER' ) ) exit;
    class Models_FormsLogModel extends Models_DatabaseModel {
        private $table = 'forms_log', $table_meta = 'forms_log_meta', $table_log_user = 'forms_log_user';
        function getLogs($select = '*', $where = '', $order = '', $dir = '', $limit = '') {
            return $this->getEntities($select,$this->table,$where,$order,$dir,$limit);
        }
        function getLog($where = '') {
            return $this->select('*', $this->table, $where);
        }
        function getLogUser($where = '') {
            return $this->select('*', $this->table_log_user, $where);
        }
        function getLogUsers($select = '*', $where = '', $order = '', $dir = '', $limit = '') {
            return $this->getEntities($select,$this->table_log_user,$where,$order,$dir,$limit);
        }
        function getLogsRows($where = '') {
            return $this->getRows($this->table,$where);
        }
        function updateLog($updates, $where) {
            return $this->update($this->table,$updates,$where);
        }
        function deleteLog($where,$limit = '1') {
            return $this->delete($this->table,$where,$limit);
        }
        function deleteLogUser($where,$limit = '1') {
            return $this->delete($this->table_log_user,$where,$limit);
        }
        function deleteLogMeta($where,$limit = '1') {
            return $this->delete($this->table_meta,$where,$limit);
        }
        function insertLog($fields,$values,$where = '') {
            return $this->insert($this->table, $fields, $values, $where);
        }
        function insertLogUser($fields,$values,$where = '') {
            return $this->insert($this->table_log_user, $fields, $values, $where);
        }
    }