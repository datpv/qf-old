<?php
if ( ! defined( 'GETOVER' ) ) exit;
    class Models_GroupsModel extends Models_DatabaseModel {
        private $table = 'groups', $table_meta = 'groups_meta';
        function getGroup($uuid = '', $id, $field_uuid = '', $field_id = 'group_id', $extraWhere = '') {
            return $this->getEntityById('*', $this->table, $uuid, $id, $field_uuid, $field_id, $extraWhere);
        }
        function getGroups($where = '', $order = '', $dir = '', $limit = '') {
            return $this->getEntities('*',$this->table,$where,$order,$dir,$limit);
        }
        function getGroupsMeta($where = '', $order = '', $dir = '', $limit = '') {
            return $this->getEntities('*',$this->table_meta,$where,$order,$dir,$limit);
        }
        function insertGroup($fields, $values, $where = '') {
            return $this->insertEntity($this->table,$fields,$values,$where);
        }
        function insertGroupMeta($fields, $values, $where = '') {
            return $this->insertEntity($this->table_meta,$fields,$values,$where);
        }
        function deleteGroup($uuid, $id, $field_uuid = '', $field_id = 'group_id') {
            return $this->deleteEntityById($this->table, $uuid, $id, $field_uuid, $field_id);
        }
        function deleteGroupMeta($where) {
            return $this->delete($this->table_meta,$where);
        }
        function updateGroup($updates, $where) {
            return $this->update($this->table,$updates,$where);
        }
        function updateGroupMeta($updates, $where, $limit = '') {
            return $this->update($this->table_meta,$updates,$where, $limit);
        }
        function getGroupedUsers($user_id) {
            return $this->fetchAll("SELECT * FROM {$this->db_prefix}groups_meta a, {$this->db_prefix}groups b WHERE a.`user_id` = '$user_id' AND a.`group_id` = b.`group_id`");
        }
    }