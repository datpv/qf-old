<?php
if ( ! defined( 'GETOVER' ) ) exit;
    class Models_FormsModel extends Models_DatabaseModel {
        private $table = 'forms';
        function getForm($uuid, $id, $field_uuid = 'form_uuid', $field_id = 'form_id', $extraWhere = '') {
            return $this->getEntityById('*', $this->table, $uuid, $id, $field_uuid, $field_id, $extraWhere);
        }
        function getForms($select = '*',$where = '', $order = '', $dir = '', $limit = '') {
            return $this->getEntities($select,$this->table,$where,$order,$dir,$limit);
        }
        function getFormsInCategory($cate_id, $approveType = 'Y', $order = '', $dir = '', $limit = '') {
            if(!empty($order)) {
                $order = 'ORDER BY `'.$order.'`';
            }
            if(!empty($limit)) {
                $limit = 'LIMIT '.$limit;
            }
            return $this->fetchAll("SELECT SQL_CALC_FOUND_ROWS * FROM {$this->db_prefix}forms a JOIN {$this->db_prefix}categories_meta b ON a.`form_id` = b.`form_id` WHERE b.`category_id` = '$cate_id' AND a.`form_approved` = '$approveType' $order $dir $limit");
        }
        function insertForm($fields, $values, $where = '') {
            return $this->insertEntity($this->table,$fields,$values,$where);
        }
        function deleteForm($uuid, $id, $field_uuid = 'form_uuid', $field_id = 'form_id') {
            return $this->deleteEntityById($this->table, $uuid, $id, $field_uuid, $field_id);
        }
        function updateForm($updates, $where) {
            return $this->update($this->table,$updates,$where);
        }
        function updateProtectPassword($form_pass, $form_id) {
            return $this->update($this->table,"`form_password` = '$form_pass'","`form_id` = '$form_id'");
        }
        function updatePublicStatus($status, $form_id) {
            return $this->update($this->table,"`form_status` = '$status'","`form_id` = '$form_id'");
        }
    }