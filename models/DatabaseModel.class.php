<?php
if ( ! defined( 'GETOVER' ) ) exit;
    class Models_DatabaseModel {
        public static $connect = null, $instance = null;
        public $registry, $db_prefix = DB_PREFIX;
        public $mysqli;
        public $query_count = 0;
        function __construct($registry) {
            $this->registry = $registry;
        }
        function connect($host, $user, $pass, $name) {
            $this->mysqli = new mysqli($host, $user, $pass, $name);
            if($this->mysqli->connect_error) die('Database Connection Error!');
            $this->mysqli->query("SET NAMES 'utf8'");
            $this->mysqli->query("SET time_zone = 'Etc/GMT-7'");
            date_default_timezone_set('Etc/GMT-7');
            return $this->mysqli;
        }
        public function setMysqli($mysqli) {
            $this->mysqli = $mysqli;
        }
        public function getMysqli() {
            return $this->mysqli;
        }
        private function __clone() {
            
        }
        function getErrors() {
            return $this->registry->mysqli->error;
        }
        public function check($qs) {
            $q = $this->registry->mysqli->query($qs);
            if(!$q) return false;
            if($q->num_rows > 0) return true;
            return false;
        }
        public function getRows($table, $where) {
            $table = $this->db_prefix.$table;
            if(!empty($where)) {
                $where = 'WHERE '.$where;
            }
            return $this->numRows("SELECT * FROM $table $where");
        }
        public function numRows($qs) {
            $q = $this->registry->mysqli->query($qs);
            if(!$q) return false;
            return $q->num_rows;
        }
        public function select($select = '*', $table, $where = '') {
            $table = $this->db_prefix.$table;
            if(!empty($where)) {
                $where = 'WHERE '.$where;
            }
            return $this->fetch("SELECT $select FROM $table $where LIMIT 1");
        }
        public function selectJoin($select = '*', $table, $join = '', $where = '', $limit = '1') {
            $table = $this->db_prefix.$table;
            if(!empty($where)) {
                $where = 'WHERE '.$where;
            }
            if(!empty($limit)) {
                $limit = 'LIMIT '.$limit;
            }
            return $this->fetch("SELECT $select FROM $table a $join $where $limit");
        }
        public function selectAll($select = '*', $table, $where = '', $order = '', $dir = '', $limit = '') {
            $table = $this->db_prefix.$table;
            if(!empty($where)) {
                $where = 'WHERE '.$where;
            }
            if(!empty($order)) {
                $order = 'ORDER BY `'.$order.'`';
            }
            if(!empty($limit)) {
                $limit = 'LIMIT '.$limit;
            }
            return $this->fetchAll("SELECT $select FROM $table $where $order $dir $limit");
        }
        public function insert($table, $fields, $values, $where = '') {
            $table = $this->db_prefix.$table;
            if(!empty($where)) {
                $where = 'WHERE '.$where;
            }
            return $this->registry->mysqli->query("INSERT INTO $table
                ($fields) VALUES ($values) $where
            ");
        }
        public function update($table, $updates, $where, $limit = '') {
            $table = $this->db_prefix.$table;
            if(!empty($where)) {
                $where = 'WHERE '.$where;
            }
            if(!empty($limit)) {
                $limit = 'LIMIT '.$limit;
            }
            return $this->registry->mysqli->query("UPDATE $table
                SET $updates $where $limit
            ");
        }
        public function delete($table, $where, $limit = '1') {
            $table = $this->db_prefix.$table;
            if(!empty($where)) {
                $where = 'WHERE '.$where;
            }
            if(!empty($limit)) {
                $limit = 'LIMIT '.$limit;
            }
            return $this->registry->mysqli->query("DELETE FROM $table
                $where $limit
            ");
        }
        public function fetch($qs) {
            $q = $this->registry->mysqli->query($qs);
            if(!$q || $q->num_rows == 0) return false;
            return $q->fetch_assoc();
        }
        public function fetchAll($qs) {
            $q = $this->registry->mysqli->query($qs);
            if(!$q || $q->num_rows == 0) return false;
            $res = array();
            while($r = $q->fetch_assoc()) {
                $res[] = $r;
            }
            return $res;
        }
        /*
        * GROUP OF COMMONS FUNCTIONS
        */
        function getEntityById($select = '*', $table, $entity_uuid, $entity_id, $field_uuid, $field_id, $extraWhere = '') {
            $where = '';
            if(!empty($entity_uuid) && !empty($field_uuid)) {
                $where = "`$field_uuid` = '$entity_uuid'";
            }
            if(!empty($entity_id) && !empty($field_id)) {
                $where = "`$field_id` = '$entity_id'";
            }
            if(!empty($extraWhere)) {
                $where .= " $extraWhere";
            }
            
            if(empty($entity_uuid) && empty($entity_id)) {
                return false;
            }
            return $this->select($select, $table, $where);
            
        }
        function getEntities($select = '*', $table, $where = '', $order = '', $dir = '', $limit = '') {
            return $this->selectAll($select, $table, $where, $order, $dir, $limit);
        }
        function insertEntity($table, $fields, $values, $where = '') {
            return $this->insert($table, $fields, $values, $where);
        }
        function updateEntity($table, $updates, $where, $limit = '') {
            if(empty($where)) return false;
            return $this->update($table, $updates, $where, $limit = '');
        }
        function deleteEntityById($table, $entity_uuid, $entity_id, $field_uuid, $field_id, $limit = '1') {
            if(!empty($entity_uuid) && !empty($field_uuid)) {
                return $this->delete($table, "`$field_uuid` = '$entity_uuid'", $limit);
            }
            if(!empty($entity_id) && !empty($field_id)) {
                return $this->delete($table, "`$field_id` = '$entity_id'", $limit);
            }
            return false;
        }
        function deleteEntitiesByIds($table, $where, $limit = '') {
            if(empty($where)) return false;
            return $this->delete($table,$where,$limit);
        }
        function getCalcNumRows() {
            $result = $this->fetch("SELECT FOUND_ROWS() as trows");
            return $result['trows'];
        }
        function doQuery($qs) {
            return $this->registry->mysqli->query($qs);
        }
    }