<?php
if ( ! defined( 'GETOVER' ) ) exit;
    class Models_SubjectsModel extends Models_DatabaseModel {
        private $table = 'subjects', $table_meta = 'subjects_meta', $table_sf = 'subject_form', $table_su = 'subject_user';
        function getSubject($where = '') {
            return $this->select('*', $this->table, $where);
        }
        function getSubjectMeta($where = '') {
            return $this->select('*', $this->table_meta, $where);
        }
        function getSF($where = '') {
            return $this->select('*', $this->table_sf, $where);
        }
        function getSU($where = '') {
            return $this->select('*', $this->table_su, $where);
        }
        function getSubjectByJoin($where = '') {
            $table = $this->db_prefix . $this->table;
            $table_meta = $this->db_prefix . $this->table_meta;
            $join = "JOIN {$table_meta} ON {$table}.`subject_id` = {$table_meta}.`subject_id`";
            return $this->selectJoin('*', $this->table, $join, $where);
        }
        function getSubjects($select = '*', $where = '', $order = '', $dir = '', $limit = '') {
            return $this->getEntities($select,$this->table,$where,$order,$dir,$limit);
        }
        function getSubjectsMeta($select = '*', $where = '', $order = '', $dir = '', $limit = '') {
            return $this->getEntities($select,$this->table_meta,$where,$order,$dir,$limit);
        }
        function getSFs($select = '*', $where = '', $order = '', $dir = '', $limit = '') {
            return $this->getEntities($select,$this->table_sf,$where,$order,$dir,$limit);
        }
        function getSUs($select = '*', $where = '', $order = '', $dir = '', $limit = '') {
            return $this->getEntities($select,$this->table_su,$where,$order,$dir,$limit);
        }
        function getSFsByJoin($where = '', $limit = '') {
            $table = $this->db_prefix . $this->table;
            $table_sf = $this->db_prefix . $this->table_sf;
            $join = "JOIN {$table} b ON a.`subject_id` = b.`subject_id`";
            return $this->selectJoin('*', $this->table_sf, $join, $where, $limit);
        }
        function insertSubject($fields, $values, $where = '') {
            return $this->insertEntity($this->table,$fields,$values,$where);
        }
        function insertSF($fields, $values, $where = '') {
            return $this->insertEntity($this->table_sf,$fields,$values,$where);
        }
        function insertSU($fields, $values, $where = '') {
            return $this->insertEntity($this->table_su,$fields,$values,$where);
        }
        function insertSubjectMeta($fields, $values, $where = '') {
            return $this->insertEntity($this->table_meta,$fields,$values,$where);
        }
        function updateSubject($updates, $where = '', $limit = '') {
            return $this->updateEntity($this->table, $updates, $where, $limit);
        }
        function updateSU($updates, $where = '', $limit = '') {
            return $this->updateEntity($this->table_su, $updates, $where, $limit);
        }
        function updateSubjectMeta($updates, $where = '', $limit = '') {
            return $this->updateEntity($this->table_meta, $updates, $where, $limit);
        }
        function deleteSubject($where,$limit = '1') {
            return $this->delete($this->table,$where,$limit);
        }
        function deleteSubjectMeta($where,$limit = '1') {
            return $this->delete($this->table_meta,$where,$limit);
        }
        function deleteSF($where,$limit = '1') {
            return $this->delete($this->table_sf,$where,$limit);
        }
        function deleteSU($where,$limit = '1') {
            return $this->delete($this->table_su,$where,$limit);
        }
    }