<?php
if ( ! defined( 'GETOVER' ) ) exit;
    class Models_ThemesModel extends Models_DatabaseModel {
        private $table = 'themes';
        function getTheme($uuid, $id, $field_uuid = '', $field_id = 'theme_id') {
            return $this->getEntityById('*', $this->table, $uuid, $id, $field_uuid, $field_id);
        }
        function getThemes($where = '', $order = '', $dir = '', $limit = '') {
            return $this->getEntities('*',$this->table,$where,$order,$dir,$limit);
        }
        function updateTheme($updates, $where) {
            return $this->update($this->table,$updates,$where);
        }
        function insertTheme($fields, $values, $where = '') {
            return $this->insertEntity($this->table,$fields,$values,$where);
        }
        function deleteTheme($uuid, $id, $field_uuid = '', $field_id = 'theme_id') {
            return $this->deleteEntityById($this->table, $uuid, $id, $field_uuid, $field_id);
        }
    }