<?php
if ( ! defined( 'GETOVER' ) ) exit;
    class Models_CachedModel extends Models_DatabaseModel {
        private $table_as = 'cached_entries_as', $table_nb = 'cached_entries_nb', $table_sced = 'cached_entries_sced', $table_nbed = 'cached_entries_nbed', $table_users = 'cached_users_stats';
        function getCachedUser($where = '') {
            return $this->select('*', $this->table_users, $where);
        }
        function getCachedEntry($select = '*', $table = '',$where = '') {
            $table = 'table_'.$table;
            $$table = $table;
            if(!isset($this->$$table)) return false;
            $_table = $this->$$table;
            return $this->select('*', $_table, $where);
        }
        function getCachedEntries($select = '*', $table = '',$where = '', $order = '', $dir = '', $limit = '') {
            $table = 'table_'.$table;
            $$table = $table;
            if(!isset($this->$$table)) return false;
            $_table = $this->$$table;
            return $this->getEntities($select,$_table,$where,$order,$dir,$limit);
        }
        function getASRows($where) {
            return $this->getRows($this->table_as,$where);
        }
        function getNBRows($where) {
            return $this->getRows($this->table_nb,$where);
        }
        function getSCEDRows($where) {
            return $this->getRows($this->table_sced,$where);
        }
        function getNBEDRows($where) {
            return $this->getRows($this->table_nbed,$where);
        }
        function getCachedEntriesNumber($form_id) {
            return $this->getRows('entries_meta',"`form_id` = '$form_id'");
        }
        function getCachedUserGrouped($group_id) {
            return $this->fetchAll("SELECT * FROM {$this->db_prefix}groups_meta a, {$this->db_prefix}cached_users_stats b WHERE a.`group_id` = '$group_id' AND a.`user_id` = b.`cache_user_id`");
        }
        function deleteCachedEntries($where) {
            $this->deleteEntitiesByIds($this->table_as,$where);
            $this->deleteEntitiesByIds($this->table_nb,$where);
            $this->deleteEntitiesByIds($this->table_sced,$where);
            $this->deleteEntitiesByIds($this->table_nbed,$where);
        }
        /*
        *
        */
        function cache_entries_nb($contents = array()) {
            $mysqli = $this->registry->mysqli;
            $up_i = $in_i = 0;
            $cache_user_id = $contents['cache_user_id'];
            $cache_entry_id = $contents['cache_entry_id'];
            $cache_result  = $mysqli->query("SELECT * FROM {$this->db_prefix}cached_entries_nb WHERE `cache_user_id` = '$cache_user_id' AND `cache_entry_id` = '$cache_entry_id'");
            if($cache_result->num_rows > 0) {
                /*
                * UPDATE
                */
                $mysqli->query("UPDATE 
                    {$this->db_prefix}cached_entries_nb 
                    SET
                        `cache_user_username` = '$contents[cache_user_username]',
                        `cache_user_email` = '$contents[cache_user_email]',
                        `cache_user_nickh2d` = '$contents[cache_user_nickh2d]',
                        `cache_user_nickskype` = '$contents[cache_user_nickskype]',
                        `cache_user_fullname` = '$contents[cache_user_fullname]',
                        `cache_user_class` = '$contents[cache_user_class]',
                        `cache_user_content` = '$contents[cache_user_content]',
                        `cache_entry_uuid` = '$contents[cache_entry_uuid]',
                        `cache_entry_public` = '$contents[cache_entry_public]',
                        `cache_entry_ip` = '$contents[cache_entry_ip]',
                        `cache_entry_content` = '$contents[cache_entry_content]',
                        `cache_entry_complete` = '$contents[cache_entry_complete]',
                        `cache_entry_t` = '$contents[cache_entry_t]',
                        `cache_entry_hs` = '$contents[cache_entry_hs]',
                        `cache_entry_tg` = '$contents[cache_entry_tg]',
                        `cache_entry_sys` = '$contents[cache_entry_sys]',
                        `cache_entry_sys_t` = '$contents[cache_entry_sys_t]',
                        `cache_form_nb_id` = '$contents[cache_form_nb_id]',
                        `cache_form_nb_uuid` = '$contents[cache_form_nb_uuid]',
                        `cache_form_sc_uuid` = '$contents[cache_form_sc_uuid]',
                        `cache_form_nb_type` = '$contents[cache_form_nb_type]',
                        `cache_form_nb_name` = '$contents[cache_form_nb_name]',
                        `cache_form_nb_status` = '$contents[cache_form_nb_status]',
                        `cache_form_nb_theme_id` = '$contents[cache_form_nb_theme_id]',
                        `cache_form_nb_content` = '$contents[cache_form_nb_content]',
                        `cache_form_nb_display_result` = '$contents[cache_form_nb_display_result]',
                        `cache_form_nb_password` = '$contents[cache_form_nb_password]',
                        `cache_content` = '$contents[cache_content]'
                    WHERE `cache_user_id` = '$cache_user_id' AND `cache_entry_id` = '$cache_entry_id'
                ");
                $up_i = $mysqli->affected_rows;
                if($mysqli->error) echo 'UPDATE NB: '.$mysqli->error;
            } else {
                /*
                * INSERT
                */
                $mysqli->query("INSERT INTO 
                    {$this->db_prefix}cached_entries_nb (
                        `cache_user_id`,
                        `cache_user_username`,
                        `cache_user_email`,
                        `cache_user_nickh2d`,
                        `cache_user_nickskype`,
                        `cache_user_fullname`,
                        `cache_user_class`,
                        `cache_user_content`,
                        `cache_entry_id`,
                        `cache_entry_uuid`,
                        `cache_entry_public`,
                        `cache_entry_ip`,
                        `cache_entry_content`,
                        `cache_entry_complete`,
                        `cache_entry_t`,
                        `cache_entry_hs`,
                        `cache_entry_tg`,
                        `cache_entry_sys`,
                        `cache_entry_sys_t`,
                        `cache_form_nb_id`,
                        `cache_form_nb_uuid`,
                        `cache_form_sc_uuid`,
                        `cache_form_nb_type`,
                        `cache_form_nb_name`,
                        `cache_form_nb_status`,
                        `cache_form_nb_theme_id`,
                        `cache_form_nb_content`,
                        `cache_form_nb_display_result`,
                        `cache_form_nb_password`,
                        `cache_content`
                    )
                    VALUES (
                        '$contents[cache_user_id]',
                        '$contents[cache_user_username]',
                        '$contents[cache_user_email]',
                        '$contents[cache_user_nickh2d]',
                        '$contents[cache_user_nickskype]',
                        '$contents[cache_user_fullname]',
                        '$contents[cache_user_class]',
                        '$contents[cache_user_content]',
                        '$contents[cache_entry_id]',
                        '$contents[cache_entry_uuid]',
                        '$contents[cache_entry_public]',
                        '$contents[cache_entry_ip]',
                        '$contents[cache_entry_content]',
                        '$contents[cache_entry_complete]',
                        '$contents[cache_entry_t]',
                        '$contents[cache_entry_hs]',
                        '$contents[cache_entry_tg]',
                        '$contents[cache_entry_sys]',
                        '$contents[cache_entry_sys_t]',
                        '$contents[cache_form_nb_id]',
                        '$contents[cache_form_nb_uuid]',
                        '$contents[cache_form_sc_uuid]',
                        '$contents[cache_form_nb_type]',
                        '$contents[cache_form_nb_name]',
                        '$contents[cache_form_nb_status]',
                        '$contents[cache_form_nb_theme_id]',
                        '$contents[cache_form_nb_content]',
                        '$contents[cache_form_nb_display_result]',
                        '$contents[cache_form_nb_password]',
                        '$contents[cache_content]'
                    )
                ");
                $in_i = $mysqli->affected_rows;
                if($mysqli->error) echo 'INSERT NB: '.$mysqli->error;
            }
            return array(
                'done' => (!$mysqli->error)?true:false,
                'up' => $up_i,
                'ins' => $in_i
            );
        }
        function cache_entries_as($contents = array()) {
            $mysqli = $this->registry->mysqli;
            $up_i = $in_i = 0;
            $cache_user_id = $contents['cache_user_id'];
            $cache_entry_id = $contents['cache_entry_id'];
            $cache_result  = $mysqli->query("SELECT * FROM {$this->db_prefix}cached_entries_as WHERE `cache_user_id` = '$cache_user_id' AND `cache_entry_id` = '$cache_entry_id'");
            if($cache_result->num_rows > 0) {
                /*
                * UPDATE
                */
                $mysqli->query("UPDATE 
                    {$this->db_prefix}cached_entries_as 
                    SET
                        `cache_user_username` = '$contents[cache_user_username]',
                        `cache_user_email` = '$contents[cache_user_email]',
                        `cache_user_nickh2d` = '$contents[cache_user_nickh2d]',
                        `cache_user_nickskype` = '$contents[cache_user_nickskype]',
                        `cache_user_fullname` = '$contents[cache_user_fullname]',
                        `cache_user_class` = '$contents[cache_user_class]',
                        `cache_user_content` = '$contents[cache_user_content]',
                        `cache_entry_uuid` = '$contents[cache_entry_uuid]',
                        `cache_entry_public` = '$contents[cache_entry_public]',
                        `cache_entry_ip` = '$contents[cache_entry_ip]',
                        `cache_entry_content` = '$contents[cache_entry_content]',
                        `cache_entry_complete` = '$contents[cache_entry_complete]',
                        `cache_entry_t` = '$contents[cache_entry_t]',
                        `cache_entry_hs` = '$contents[cache_entry_hs]',
                        `cache_entry_tg` = '$contents[cache_entry_tg]',
                        `cache_entry_sys` = '$contents[cache_entry_sys]',
                        `cache_entry_sys_t` = '$contents[cache_entry_sys_t]',
                        `cache_form_nb_id` = '$contents[cache_form_nb_id]',
                        `cache_form_nb_uuid` = '$contents[cache_form_nb_uuid]',
                        `cache_form_sc_uuid` = '$contents[cache_form_sc_uuid]',
                        `cache_form_nb_type` = '$contents[cache_form_nb_type]',
                        `cache_form_nb_name` = '$contents[cache_form_nb_name]',
                        `cache_form_nb_status` = '$contents[cache_form_nb_status]',
                        `cache_form_nb_theme_id` = '$contents[cache_form_nb_theme_id]',
                        `cache_form_nb_content` = '$contents[cache_form_nb_content]',
                        `cache_form_nb_password` = '$contents[cache_form_nb_password]',
                        `cache_form_nb_display_result` = '$contents[cache_form_nb_display_result]',
                        `cache_content` = '$contents[cache_content]'
                    WHERE `cache_user_id` = '$cache_user_id' AND `cache_entry_id` = '$cache_entry_id'
                ");
                $up_i = $mysqli->affected_rows;
                if($mysqli->error) echo 'UPDATE AS: '.$mysqli->error;
            } else {
                /*
                * INSERT
                */
                $mysqli->query("INSERT INTO 
                    {$this->db_prefix}cached_entries_as (
                        `cache_user_id`,
                        `cache_user_username`,
                        `cache_user_email`,
                        `cache_user_nickh2d`,
                        `cache_user_nickskype`,
                        `cache_user_fullname`,
                        `cache_user_class`,
                        `cache_user_content`,
                        `cache_entry_id`,
                        `cache_entry_uuid`,
                        `cache_entry_public`,
                        `cache_entry_ip`,
                        `cache_entry_content`,
                        `cache_entry_complete`,
                        `cache_entry_t`,
                        `cache_entry_hs`,
                        `cache_entry_tg`,
                        `cache_entry_sys`,
                        `cache_entry_sys_t`,
                        `cache_form_nb_id`,
                        `cache_form_nb_uuid`,
                        `cache_form_sc_uuid`,
                        `cache_form_nb_type`,
                        `cache_form_nb_name`,
                        `cache_form_nb_status`,
                        `cache_form_nb_theme_id`,
                        `cache_form_nb_content`,
                        `cache_form_nb_password`,
                        `cache_form_nb_display_result`,
                        `cache_content`
                    )
                    VALUES (
                        '$contents[cache_user_id]',
                        '$contents[cache_user_username]',
                        '$contents[cache_user_email]',
                        '$contents[cache_user_nickh2d]',
                        '$contents[cache_user_nickskype]',
                        '$contents[cache_user_fullname]',
                        '$contents[cache_user_class]',
                        '$contents[cache_user_content]',
                        '$contents[cache_entry_id]',
                        '$contents[cache_entry_uuid]',
                        '$contents[cache_entry_public]',
                        '$contents[cache_entry_ip]',
                        '$contents[cache_entry_content]',
                        '$contents[cache_entry_complete]',
                        '$contents[cache_entry_t]',
                        '$contents[cache_entry_hs]',
                        '$contents[cache_entry_tg]',
                        '$contents[cache_entry_sys]',
                        '$contents[cache_entry_sys_t]',
                        '$contents[cache_form_nb_id]',
                        '$contents[cache_form_nb_uuid]',
                        '$contents[cache_form_sc_uuid]',
                        '$contents[cache_form_nb_type]',
                        '$contents[cache_form_nb_name]',
                        '$contents[cache_form_nb_status]',
                        '$contents[cache_form_nb_theme_id]',
                        '$contents[cache_form_nb_content]',
                        '$contents[cache_form_nb_password]',
                        '$contents[cache_form_nb_display_result]',
                        '$contents[cache_content]'
                    )
                ");
                $in_i = $mysqli->affected_rows;
                if($mysqli->error) echo 'INSERT AS: '.$mysqli->error;
            }
            return array(
                'done' => (!$mysqli->error)?true:false,
                'up' => $up_i,
                'ins' => $in_i
            );
        }
        function cache_entries_sced($contents = array()) {
            $mysqli = $this->registry->mysqli;
            $up_i = $in_i = 0;
            $cache_user_id = $contents['cache_user_id'];
            $cache_entry_id = $contents['cache_entry_id'];
            $cache_result  = $mysqli->query("SELECT * FROM {$this->db_prefix}cached_entries_sced WHERE `cache_user_id` = '$cache_user_id' AND `cache_entry_id` = '$cache_entry_id'");
            if($cache_result->num_rows > 0) {
                /*
                * UPDATE
                */
                $mysqli->query("UPDATE 
                    {$this->db_prefix}cached_entries_sced 
                    SET
                        `cache_user_username` = '$contents[cache_user_username]',
                        `cache_user_email` = '$contents[cache_user_email]',
                        `cache_user_nickh2d` = '$contents[cache_user_nickh2d]',
                        `cache_user_nickskype` = '$contents[cache_user_nickskype]',
                        `cache_user_fullname` = '$contents[cache_user_fullname]',
                        `cache_user_class` = '$contents[cache_user_class]',
                        `cache_user_content` = '$contents[cache_user_content]',
                        
                        `cache_user_of_id` = '$contents[cache_user_of_id]',
                        `cache_user_of_username` = '$contents[cache_user_of_username]',
                        `cache_user_of_email` = '$contents[cache_user_of_email]',
                        `cache_user_of_nickh2d` = '$contents[cache_user_of_nickh2d]',
                        `cache_user_of_nickskype` = '$contents[cache_user_of_nickskype]',
                        `cache_user_of_fullname` = '$contents[cache_user_of_fullname]',
                        `cache_user_of_class` = '$contents[cache_user_of_class]',
                        `cache_user_of_content` = '$contents[cache_user_of_content]',
                        
                        `cache_entry_uuid` = '$contents[cache_entry_uuid]',
                        `cache_entry_public` = '$contents[cache_entry_public]',
                        `cache_entry_ip` = '$contents[cache_entry_ip]',
                        `cache_entry_content` = '$contents[cache_entry_content]',
                        `cache_entry_complete` = '$contents[cache_entry_complete]',
                        `cache_entry_t` = '$contents[cache_entry_t]',
                        `cache_entry_hs` = '$contents[cache_entry_hs]',
                        `cache_entry_tg` = '$contents[cache_entry_tg]',
                        `cache_entry_sys` = '$contents[cache_entry_sys]',
                        `cache_entry_sys_t` = '$contents[cache_entry_sys_t]',
                        `cache_form_nb_id` = '$contents[cache_form_nb_id]',
                        `cache_form_nb_uuid` = '$contents[cache_form_nb_uuid]',
                        `cache_form_sc_uuid` = '$contents[cache_form_sc_uuid]',
                        `cache_form_nb_type` = '$contents[cache_form_nb_type]',
                        `cache_form_nb_name` = '$contents[cache_form_nb_name]',
                        `cache_form_nb_status` = '$contents[cache_form_nb_status]',
                        `cache_form_nb_theme_id` = '$contents[cache_form_nb_theme_id]',
                        `cache_form_nb_content` = '$contents[cache_form_nb_content]',
                        `cache_form_nb_password` = '$contents[cache_form_nb_password]',
                        `cache_content` = '$contents[cache_content]'
                    WHERE `cache_user_id` = '$cache_user_id' AND `cache_entry_id` = '$cache_entry_id'
                ");
                $up_i = $mysqli->affected_rows;
                if($mysqli->error) echo 'UPDATE SCED: '.$mysqli->error;
            } else {
                /*
                * INSERT
                */
                $mysqli->query("INSERT INTO 
                    {$this->db_prefix}cached_entries_sced (
                        `cache_user_id`,
                        `cache_user_username`,
                        `cache_user_email`,
                        `cache_user_nickh2d`,
                        `cache_user_nickskype`,
                        `cache_user_fullname`,
                        `cache_user_class`,
                        `cache_user_content`,
                        
                        `cache_user_of_id`,
                        `cache_user_of_username`,
                        `cache_user_of_email`,
                        `cache_user_of_nickh2d`,
                        `cache_user_of_nickskype`,
                        `cache_user_of_fullname`,
                        `cache_user_of_class`,
                        `cache_user_of_content`,
                        
                        `cache_entry_id`,
                        `cache_entry_uuid`,
                        `cache_entry_public`,
                        `cache_entry_ip`,
                        `cache_entry_content`,
                        `cache_entry_complete`,
                        `cache_entry_t`,
                        `cache_entry_hs`,
                        `cache_entry_tg`,
                        `cache_entry_sys`,
                        `cache_entry_sys_t`,
                        `cache_form_nb_id`,
                        `cache_form_nb_uuid`,
                        `cache_form_sc_uuid`,
                        `cache_form_nb_type`,
                        `cache_form_nb_name`,
                        `cache_form_nb_status`,
                        `cache_form_nb_theme_id`,
                        `cache_form_nb_content`,
                        `cache_form_nb_password`,
                        `cache_content`
                    )
                    VALUES (
                        '$contents[cache_user_id]',
                        '$contents[cache_user_username]',
                        '$contents[cache_user_email]',
                        '$contents[cache_user_nickh2d]',
                        '$contents[cache_user_nickskype]',
                        '$contents[cache_user_fullname]',
                        '$contents[cache_user_class]',
                        '$contents[cache_user_content]',
                        
                        '$contents[cache_user_of_id]',
                        '$contents[cache_user_of_username]',
                        '$contents[cache_user_of_email]',
                        '$contents[cache_user_of_nickh2d]',
                        '$contents[cache_user_of_nickskype]',
                        '$contents[cache_user_of_fullname]',
                        '$contents[cache_user_of_class]',
                        '$contents[cache_user_of_content]',
                        
                        '$contents[cache_entry_id]',
                        '$contents[cache_entry_uuid]',
                        '$contents[cache_entry_public]',
                        '$contents[cache_entry_ip]',
                        '$contents[cache_entry_content]',
                        '$contents[cache_entry_complete]',
                        '$contents[cache_entry_t]',
                        '$contents[cache_entry_hs]',
                        '$contents[cache_entry_tg]',
                        '$contents[cache_entry_sys]',
                        '$contents[cache_entry_sys_t]',
                        '$contents[cache_form_nb_id]',
                        '$contents[cache_form_nb_uuid]',
                        '$contents[cache_form_sc_uuid]',
                        '$contents[cache_form_nb_type]',
                        '$contents[cache_form_nb_name]',
                        '$contents[cache_form_nb_status]',
                        '$contents[cache_form_nb_theme_id]',
                        '$contents[cache_form_nb_content]',
                        '$contents[cache_form_nb_password]',
                        '$contents[cache_content]'
                    )
                ");
                $in_i = $mysqli->affected_rows;
                if($mysqli->error) echo 'INSERT SCED: '.$mysqli->error;
            }
            return array(
                'done' => (!$mysqli->error)?true:false,
                'up' => $up_i,
                'ins' => $in_i
            );
        }
        function cache_entries_nbed($contents = array()) {
            $mysqli = $this->registry->mysqli;
            $up_i = $in_i = 0;
            $cache_user_id = $contents['cache_user_id'];
            $cache_entry_id = $contents['cache_entry_id'];
            $cache_result  = $mysqli->query("SELECT * FROM {$this->db_prefix}cached_entries_nbed WHERE `cache_user_id` = '$cache_user_id' AND `cache_entry_id` = '$cache_entry_id'");
            if($cache_result->num_rows > 0) {
                /*
                * UPDATE
                */
                $mysqli->query("UPDATE 
                    {$this->db_prefix}cached_entries_nbed 
                    SET
                        `cache_user_username` = '$contents[cache_user_username]',
                        `cache_user_email` = '$contents[cache_user_email]',
                        `cache_user_nickh2d` = '$contents[cache_user_nickh2d]',
                        `cache_user_nickskype` = '$contents[cache_user_nickskype]',
                        `cache_user_fullname` = '$contents[cache_user_fullname]',
                        `cache_user_class` = '$contents[cache_user_class]',
                        `cache_user_content` = '$contents[cache_user_content]',
                        
                        `cache_user_by_id` = '$contents[cache_user_by_id]',
                        `cache_user_by_username` = '$contents[cache_user_by_username]',
                        `cache_user_by_email` = '$contents[cache_user_by_email]',
                        `cache_user_by_nickh2d` = '$contents[cache_user_by_nickh2d]',
                        `cache_user_by_nickskype` = '$contents[cache_user_by_nickskype]',
                        `cache_user_by_fullname` = '$contents[cache_user_by_fullname]',
                        `cache_user_by_class` = '$contents[cache_user_by_class]',
                        `cache_user_by_content` = '$contents[cache_user_by_content]',
                        
                        `cache_entry_uuid` = '$contents[cache_entry_uuid]',
                        `cache_entry_public` = '$contents[cache_entry_public]',
                        `cache_entry_ip` = '$contents[cache_entry_ip]',
                        `cache_entry_content` = '$contents[cache_entry_content]',
                        `cache_entry_complete` = '$contents[cache_entry_complete]',
                        `cache_entry_t` = '$contents[cache_entry_t]',
                        `cache_entry_hs` = '$contents[cache_entry_hs]',
                        `cache_entry_tg` = '$contents[cache_entry_tg]',
                        `cache_entry_sys` = '$contents[cache_entry_sys]',
                        `cache_entry_sys_t` = '$contents[cache_entry_sys_t]',
                        `cache_form_nb_id` = '$contents[cache_form_nb_id]',
                        `cache_form_nb_uuid` = '$contents[cache_form_nb_uuid]',
                        `cache_form_sc_uuid` = '$contents[cache_form_sc_uuid]',
                        `cache_form_nb_type` = '$contents[cache_form_nb_type]',
                        `cache_form_nb_name` = '$contents[cache_form_nb_name]',
                        `cache_form_nb_status` = '$contents[cache_form_nb_status]',
                        `cache_form_nb_theme_id` = '$contents[cache_form_nb_theme_id]',
                        `cache_form_nb_content` = '$contents[cache_form_nb_content]',
                        `cache_form_nb_password` = '$contents[cache_form_nb_password]',
                        `cache_content` = '$contents[cache_content]'
                    WHERE `cache_user_id` = '$cache_user_id' AND `cache_entry_id` = '$cache_entry_id'
                ");
                $up_i = $mysqli->affected_rows;
                if($mysqli->error) echo 'UPDATE NBED: '.$mysqli->error;
            } else {
                /*
                * INSERT
                */
                $mysqli->query("INSERT INTO 
                    {$this->db_prefix}cached_entries_nbed (
                        `cache_user_id`,
                        `cache_user_username`,
                        `cache_user_email`,
                        `cache_user_nickh2d`,
                        `cache_user_nickskype`,
                        `cache_user_fullname`,
                        `cache_user_class`,
                        `cache_user_content`,
                        
                        `cache_user_by_id`,
                        `cache_user_by_username`,
                        `cache_user_by_email`,
                        `cache_user_by_nickh2d`,
                        `cache_user_by_nickskype`,
                        `cache_user_by_fullname`,
                        `cache_user_by_class`,
                        `cache_user_by_content`,
                        
                        `cache_entry_id`,
                        `cache_entry_uuid`,
                        `cache_entry_public`,
                        `cache_entry_ip`,
                        `cache_entry_content`,
                        `cache_entry_complete`,
                        `cache_entry_t`,
                        `cache_entry_hs`,
                        `cache_entry_tg`,
                        `cache_entry_sys`,
                        `cache_entry_sys_t`,
                        `cache_form_nb_id`,
                        `cache_form_nb_uuid`,
                        `cache_form_sc_uuid`,
                        `cache_form_nb_type`,
                        `cache_form_nb_name`,
                        `cache_form_nb_status`,
                        `cache_form_nb_theme_id`,
                        `cache_form_nb_content`,
                        `cache_form_nb_password`,
                        `cache_content`
                    )
                    VALUES (
                        '$contents[cache_user_id]',
                        '$contents[cache_user_username]',
                        '$contents[cache_user_email]',
                        '$contents[cache_user_nickh2d]',
                        '$contents[cache_user_nickskype]',
                        '$contents[cache_user_fullname]',
                        '$contents[cache_user_class]',
                        '$contents[cache_user_content]',
                        
                        '$contents[cache_user_by_id]',
                        '$contents[cache_user_by_username]',
                        '$contents[cache_user_by_email]',
                        '$contents[cache_user_by_nickh2d]',
                        '$contents[cache_user_by_nickskype]',
                        '$contents[cache_user_by_fullname]',
                        '$contents[cache_user_by_class]',
                        '$contents[cache_user_by_content]',
                        
                        '$contents[cache_entry_id]',
                        '$contents[cache_entry_uuid]',
                        '$contents[cache_entry_public]',
                        '$contents[cache_entry_ip]',
                        '$contents[cache_entry_content]',
                        '$contents[cache_entry_complete]',
                        '$contents[cache_entry_t]',
                        '$contents[cache_entry_hs]',
                        '$contents[cache_entry_tg]',
                        '$contents[cache_entry_sys]',
                        '$contents[cache_entry_sys_t]',
                        '$contents[cache_form_nb_id]',
                        '$contents[cache_form_nb_uuid]',
                        '$contents[cache_form_sc_uuid]',
                        '$contents[cache_form_nb_type]',
                        '$contents[cache_form_nb_name]',
                        '$contents[cache_form_nb_status]',
                        '$contents[cache_form_nb_theme_id]',
                        '$contents[cache_form_nb_content]',
                        '$contents[cache_form_nb_password]',
                        '$contents[cache_content]'
                    )
                ");
                $in_i = $mysqli->affected_rows;
                if($mysqli->error) echo 'INSERT NBED: '.$mysqli->error;
            }
            return array(
                'done' => (!$mysqli->error)?true:false,
                'up' => $up_i,
                'ins' => $in_i
            );
        }
        function cache_users_stats($contents = array()) {
            $mysqli = $this->registry->mysqli;
            $up_i = $in_i = 0;
            $cache_user_id = $contents['cache_user_id'];
            $cache_result  = $mysqli->query("SELECT * FROM {$this->db_prefix}cached_users_stats WHERE `cache_user_id` = '$cache_user_id'");
            if($cache_result->num_rows > 0) {
                /*
                * UPDATE
                */
                $mysqli->query("UPDATE 
                    {$this->db_prefix}cached_users_stats 
                    SET
                        `cache_user_status` = '$contents[cache_user_status]',
                        `cache_user_username` = '$contents[cache_user_username]',
                        `cache_user_email` = '$contents[cache_user_email]',
                        `cache_user_nickh2d` = '$contents[cache_user_nickh2d]',
                        `cache_user_nickskype` = '$contents[cache_user_nickskype]',
                        `cache_user_fullname` = '$contents[cache_user_fullname]',
                        `cache_user_class` = '$contents[cache_user_class]',
                        `cache_user_content` = '$contents[cache_user_content]',
                        `cache_stats_t` = '$contents[cache_stats_t]',
                        `cache_stats_hs` = '$contents[cache_stats_hs]',
                        `cache_stats_tg` = '$contents[cache_stats_tg]',
                        `cache_stats_sys` = '$contents[cache_stats_sys]',
                        `cache_stats_sys_t` = '$contents[cache_stats_sys_t]',
                        `cache_time` = '$contents[cache_time]'
                    WHERE `cache_user_id` = '$cache_user_id'
                ");
                $up_i = $mysqli->affected_rows;
                if($mysqli->error) echo 'UPDATE NBED: '.$mysqli->error;
            } else {
                /*
                * INSERT
                */
                $mysqli->query("INSERT INTO 
                    {$this->db_prefix}cached_users_stats (
                        `cache_user_id`,
                        `cache_user_status`,
                        `cache_user_username`,
                        `cache_user_email`,
                        `cache_user_nickh2d`,
                        `cache_user_nickskype`,
                        `cache_user_fullname`,
                        `cache_user_class`,
                        `cache_user_content`,
                        `cache_stats_t`,
                        `cache_stats_hs`,
                        `cache_stats_tg`,
                        `cache_stats_sys`,
                        `cache_stats_sys_t`,
                        `cache_time`
                    )
                    VALUES (
                        '$contents[cache_user_id]',
                        '$contents[cache_user_status]',
                        '$contents[cache_user_username]',
                        '$contents[cache_user_email]',
                        '$contents[cache_user_nickh2d]',
                        '$contents[cache_user_nickskype]',
                        '$contents[cache_user_fullname]',
                        '$contents[cache_user_class]',
                        '$contents[cache_user_content]',
                        '$contents[cache_stats_t]',
                        '$contents[cache_stats_hs]',
                        '$contents[cache_stats_tg]',
                        '$contents[cache_stats_sys]',
                        '$contents[cache_stats_sys_t]',
                        '$contents[cache_time]'
                    )
                ");
                $in_i = $mysqli->affected_rows;
                if($mysqli->error) echo 'INSERT STATS: '.$mysqli->error;
            }
            return array(
                'done' => (!$mysqli->error)?true:false,
                'up' => $up_i,
                'ins' => $in_i
            );
        }
        function deleteCachedNBED($cache_user_id, $entri_sc_ids) {
            $mysqli = $this->registry->mysqli;
            $cache_result  = $mysqli->query("DELETE FROM {$this->db_prefix}cached_entries_nbed WHERE `cache_user_id` = '$cache_user_id' AND `cache_entry_uuid` IN ($entri_sc_ids)");
            return (!$mysqli->error)?true:false;
        }
        function deleteEntriesCachedNotInGroup() {
            $mysqli = $this->registry->mysqli;
            $mysqli->query("DELETE FROM {$this->db_prefix}cached_entries_nbed WHERE `cache_user_group_id` = '0' OR  `cache_user_by_group_id` = '0'");
            $mysqli->query("DELETE FROM {$this->db_prefix}cached_entries_sced WHERE `cache_user_group_id` = '0' OR  `cache_user_of_group_id` = '0'");
            return (!$mysqli->error)?true:false;
        }
        
        function deleteCachedNBEDNotInGroup($user_id,$group_id) {
            $mysqli = $this->registry->mysqli;
            $mysqli->query("DELETE FROM {$this->db_prefix}cached_entries_nbed WHERE `cache_user_id` = '$user_id' AND  `cache_user_by_group_id` != '$group_id'");
            return (!$mysqli->error)?true:false;
        }
        function deleteCachedSCEDNotInGroup($user_id,$group_id) {
            $mysqli = $this->registry->mysqli;
            $mysqli->query("DELETE FROM {$this->db_prefix}cached_entries_sced WHERE `cache_user_id` = '$user_id' AND  `cache_user_of_group_id` != '$group_id'");
            return (!$mysqli->error)?true:false;
        }
        function emptyCached() {
            $mysqli = $this->registry->mysqli;
            
            $mysqli->query("DELETE FROM {$this->db_prefix}cached_entries_as");
            $mysqli->query("ALTER TABLE {$this->db_prefix}cached_entries_as AUTO_INCREMENT = '1'");
            
            $mysqli->query("DELETE FROM {$this->db_prefix}cached_entries_nb");
            $mysqli->query("ALTER TABLE {$this->db_prefix}cached_entries_nb AUTO_INCREMENT = '1'");
            
            $mysqli->query("DELETE FROM {$this->db_prefix}cached_entries_sced");
            $mysqli->query("ALTER TABLE {$this->db_prefix}cached_entries_sced AUTO_INCREMENT = '1'");
            
            $mysqli->query("DELETE FROM {$this->db_prefix}cached_entries_nbed");
            $mysqli->query("ALTER TABLE {$this->db_prefix}cached_entries_nbed AUTO_INCREMENT = '1'");
            
            $mysqli->query("DELETE FROM {$this->db_prefix}cached_users_stats");
            $mysqli->query("ALTER TABLE {$this->db_prefix}cached_users_stats AUTO_INCREMENT = '1'");
            
            return (!$mysqli->error)?true:false;
        }
    }