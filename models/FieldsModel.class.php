<?php
if ( ! defined( 'GETOVER' ) ) exit;
    class Models_FieldsModel extends Models_DatabaseModel {
        private $table = 'fields';
        function getFields($select = '*', $where = '', $order = '', $dir = '', $limit = '') {
            return $this->getEntities($select,$this->table,$where,$order,$dir,$limit);
        }
        function getField($where = '') {
            return $this->select('*', $this->table, $where);
        }
        function updateField($updates, $where) {
            return $this->update($this->table,$updates,$where);
        }
        function deleteField($where) {
            return $this->delete($this->table,$where);
        }
        function insertField($fields,$values,$where = '') {
            return $this->insert($this->table, $fields, $values, $where);
        }
    }