<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<div id="container">
	<div id="stage">
		<div id="main">
			<h1>
				Page not found.
			</h1>
			<img src="template/images/404.gif" alt="Hoctudau couldn't find this file. It may have been moved or deleted. Be sure to check your spelling." />
		</div>
		<!--main-->
	</div>
	<!--stage-->
</div>