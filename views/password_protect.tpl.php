<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Password protected</title>
        <style>
            body, html{
                height:100%;
                width:100%;
                padding:0px;
                margin:0px;
            }
            
            body{
                font-family:Verdana, Geneva, Arial, Helvetica, sans-serif;
                font-size:14px;
                color:#333;
                background:#545454;
            }
            .notification{
                box-shadow:0 0 5px rgba(0, 0, 0, 0.5);
                border-radius:5px;
                min-width:500px;
            }
        </style>
    </head>
    <body>
        <table cellpadding="0" cellspacing="0" width="100%" height="100%" bgcolor="#545454">
            <tr>
                <td height="100">&nbsp;</td>
            </tr>
            <tr>
                <td align="center" style="font-family:Verdana, Geneva, Arial, Helvetica, sans-serif;font-size:14px;color:#333;">
                    
                        <table cellpadding="0" cellspacing="0" class="notification" >
                            <tr>
                                                                <td height="10">
                                </td>
                                <td width="20" style="background-position:right top;">
                                    &nbsp;
                                </td>
                                                                                                    
                            </tr>
                            <tr>
                                <td align="left" valign="top" bgcolor="#ffffff" style="padding:30px !important; max-width: 638px;" colspan="2">
                                   <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<style type="text/css">
    .details {
        background: none repeat scroll 0 0 #F5F5F5;
        border: 1px solid #AAAAAA;

        -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.5) inset;
        -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.5) inset;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.5) inset;
        font-size: 12px;
        margin-bottom: -24px;
        margin-top: 16px;
        padding: 10px;

        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
    }

    #show {
        border: 1px solid #CCCCCC;
        background: #f5f5f5;
        margin-bottom: 10px;
        margin-left: 70px;
        margin-top: 10px;
        padding: 3px 10px;

        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        text-shadow: 0 1px 0 #fff;
    }

    #show:hover {
        border: 1px solid #aaa;
    }

    #show:active {
        border: 1px solid #888;
        background: #eee;
    }
</style>
<script>
    $(function(){
        var hidden = true;
        var width = parseInt($('.notification').css('width'), 10);
        $('.details').css('width', width-82);
        $('.details').slideDown();
        $('#show').click(function(){
            
            if(hidden){
                $('.details').slideDown();
                $('#show').val('Less Details');
            }else{
                $('.details').slideUp();
                $('#show').val('More Details');
            }
            hidden = !hidden;
        });
    });
</script>
<div style="text-align:left">
    <div style="">
        <h1 style=" margin-left:70px; margin-bottom:5px; margin-top:20px;text-align:left">Restricted Access</h1>
    </div>
    <div style="margin-left:71px;">
        <h4>Enter Password to Access</h4><form method="post" action="">
        <label>Password: 
        <input type="password" name="passKey" style="width:200px; font-size:16px; padding:5px;">
        </label><br><br>
        <button style="padding:5px; font-size:14px;" type="submit">Access</button>
        </form>    </div>
        <input type="button" id="show" value="More Details" />
    </div>
<div style="display:none;" class="details"> <?php if(isset($pass_err)) echo $pass_err; else echo isset($pass_require)?$pass_require:'Details: Unauthorized access'; ?> </div>
                                </td>
                            </tr>
                            <tr>
                                <td height="22">&nbsp;</td>
                                <td height="22" width="20" style="background-position:right">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td height="100">&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
