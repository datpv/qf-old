<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<!DOCTYPE html>
<html class="firefox en">
	<head>
		<title>
			Report Auto Builder
		</title>
	</head>
	<body id="report" class="step0 dragtrick zoom " style="height: 10px; overflow: hidden;">
        Creating Report...
		<noscript>
			<style type="text/css">
				
body, html{height:100%; overflow:hidden}
.graph, .map{height:0 !important}
select{visibility:hidden}

			</style>
			<div id="overlay">
			</div>
			<div id="lightbox" class="done error">
				<div id="lbContent">
					<div class="prompt">
						<h2>
							Sorry, The admin interface can
							<br/>
							only work on a JavaScript enabled browser.
						</h2>
						<p>
							You'll either need to
							<a target="_blank" href="http://www.google.com/search?hl=en&q=how+to+enable+javascript">enable JavaScript</a>
							on your browser or
							<br />
							<a href="http://www.google.com/search?hl=en&safe=off&q=download+browser" target="_blank">download</a>
							a browser that allows you to run JavaScript.
						</p>
					</div>
				</div>
			</div>
		</noscript>
		<!-- JavaScript -->
		<script type="text/javascript">
__REPORT = <?php
    
?>{"ReportId":"","Name":<?php echo json_encode('Report: '.$form_orgn['Name']); ?>,"IsPublic":"0","Url":"","Description":"This is my report. View it in all its glory!","FormId":<?php echo $form_id; ?>,"MatchType":"all","HasGrid":0,"IsEditable":0,"PageSize":10,"ShowColumns":"","StyleId":"","DateCreated":"0","DateUpdated":"0","AllowExport":"0","Layout":"layout1","Conditions":[],"Graphs":<?php $grp = array();
if(!empty($form_orgn['Fields'])) {
    $ki = 0;
    foreach($form_orgn['Fields'] as $kfo => $fo) {
        
        if($fo['Typeof'] == 'likert') {
            foreach($fo['SubFields'] as $skfo => $sfo) {
                $grp[] = array(
                    'GraphId' => $ki,
                    'Name' => $sfo['Title'],
                    'Size' => '',
                    'Typeof' => 'fieldChart',
                    'FieldName' => 'Field'.$sfo['ColumnId'],
                    'ReportId' => '',
                    'Description' => '',
                    'Format' => '',
                    'Detail' => 'both',
                    'Color' => '',
                    'Url' => null,
                    'Zone' => '1',
                    'Position' => $ki,
                    'ChoiceId' => null,
                    'Width' => null,
                    'Height' => null
                );
                $ki++;
            }
        } else {
            $grp[] = array(
                'GraphId' => $kfo,
                'Name' => $fo['Title'],
                'Size' => '',
                'Typeof' => 'fieldChart',
                'FieldName' => 'Field'.$fo['ColumnId'],
                'ReportId' => '',
                'Description' => '',
                'Format' => '',
                'Detail' => 'both',
                'Color' => '',
                'Url' => null,
                'Zone' => '1',
                'Position' => $ki,
                'ChoiceId' => null,
                'Width' => null,
                'Height' => null
            );
        }
        
        $ki++;
    }   
}
echo json_encode($grp);
?>,"Widgets":[]}
__FORM = <?php echo json_encode($_form); ?>;
</script>
<script src="template/scripts/global/dynamic-1-7.740.js">
</script>
<script>
var ReportEngine=Class.create({
    save:function(report){
        var myAjax=new Ajax.Request(
            new String(document.location),
            {
                method:'post',
                parameters:'action=save&report='+encodeURIComponent(Object.toJSON(report)),
                onComplete:function(r){
                    ret=r.responseText.evalJSON()
                    if(ret.success == true) {
                        window.location = ('index.php?req=report/builder/'+ret.response.reportId);
                    } else {
                        alert('Error When Autoing Report');
                    }
                    
                }.bind(this)
            }
        );
        }
});
var ReportBuilder=Class.create({
    ReportEngine:new ReportEngine(),
    save:function(){this.ReportEngine.save(__REPORT);}
});
var __BUILD;
Event.observe(window,'load',init,false);function init(){__BUILD=new ReportBuilder();save();}
function save(){__BUILD.save();}
</script>
	</body>
</html>