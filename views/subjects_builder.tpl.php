<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<div id="stage" class="afi">
    <?php
        $content_title = 'Empty Title';
        $content_content = 'Empty Content';
        if(isset($subject_result)) {
            $subject_content = $this->helpers->json_decode_to_array($subject_result['subject_content']);
            $content_title = $subject_result['subject_name'];
            $content_content = $subject_content['content'];
        }
        if(empty($content_title)) $content_title = 'Empty Title';
        if(empty($content_content)) $content_content = 'Empty Content';
    ?>
    <div class="info">
        <h2><span id="subject_title"><?php echo $content_title; ?></span> (<?php echo $user_count; ?> Users)</h2>
    </div>
	<div id="full">
        <form id="subject_settings" class="clearfix">
            <div class="subject_info left">
                <div class="col1 col">
                    <ul>
                        <li id="listStartDate">
        					<label class="desc">
        						Expire Date
        					</label>
        					<span>
        						<input id="startDate-1" name="startDate-1" class="field text month" size="2" type="text" maxlength="2" value="" onblur="updateSubject('ExpireDate', 'live');" />
        						<label for="startDate-1">
        							MM
        						</label>
        					</span>
        					<span>
        						<input id="startDate-2" name="startDate-2" class="field text days" size="2" type="text" maxlength="2" value="" onblur="updateSubject('ExpireDate', 'live');" />
        						<label for="startDate-2">
        							DD
        						</label>
        					</span>
        					<span>
        						<input id="startDate" name="startDate" class="field text year" size="4" type="text" maxlength="4" onblur="updateSubject('ExpireDate', 'live');" />
        						<label for="startDate">
        							YYYY
        						</label>
        					</span>
        					<span id="calStart">
        						<img id="pickStart" class="icon" src="template/images/icons/calendar.png" alt="Pick date." />
        					</span>
        				</li>
                    </ul>
                </div>
                <div class="col2 col">
                    <ul>
                        <li>
							<label class="desc">
								Limit User
								<a class="tooltip" title="About Limit User" rel="0 For Unlimit">(?)</a>
							</label>
							<div>
								<span>
									<input id="formLimitUser" class="text" type="text" value="" maxlength="10" onkeyup="updateSubject('LimitUser', this.value)" onblur="updateSubject('LimitUser', this.value)" />
								</span>
							</div>
						</li>
                    </ul>
                </div>
                <div class="col3 col">
                    <ul>
                        <li id="listPrice">
							<label class="desc">
								Price ("Free" for Free Subject)
                                <a rel="Enter Any String That Specify The Price" title="Subject Price, Ex: 500k VND" class="tooltip">(?)</a>
							</label>
							<div>
								<span>
									<input id="formPrice" class="text" type="text" value="" maxlength="10" onkeyup="updateSubject('Price', this.value)" onblur="updateSubject('Price', this.value)" />
								</span>
							</div>
						</li>
                    </ul>
                </div>
            </div>
        </form>
		<ul id="tabs" class="clearfix">
			<li class="selected">
				<a class="tab_name" rel="subject_tab_content" href="#subject_tab_content">Content</a>
			</li>
            <?php
            if(isset($subject_meta_results) && !empty($subject_meta_results)) foreach($subject_meta_results as $subject_meta_result) {
                $meta_key = substr($subject_meta_result['meta_key'],strlen('subject_'));
                $key_name = $subject_meta_result['key_name'];
            ?>
            <li>
				<a class="tab_name" rel="subject_tab_<?php echo $meta_key; ?>" href="#subject_tab_<?php echo $meta_key; ?>">
                <?php echo $key_name; ?>
                </a>
                <?php
                if($meta_key != 'outline') {
                ?>
                <div class="tab_settings hide">
                    <img class="faDel" onclick="deleteTab(this); return false;" src="template/images/icons/delete.png" alt="Delete." title="Delete." />
                    <img class="faRename" onclick="renameTab(this); return false;" src="template/images/icons/textfield_rename.png" alt="Rename." title="Rename." />
                </div>
                <?php
                }
                ?>
			</li>
            <?php
            } else {
            ?>
            <li>
				<a class="tab_name" rel="subject_tab_outline" href="#subject_tab_outline">Outline</a>
			</li>
            <?php
            }
            ?>
            
            <li>
                <span title="Add New Tab" id="add_tab">+</span>
                <div class="hide" id="add_tab_name">
                    <form><input id="tab_name_input" name="tab_name_input" /><button id="tab_name_btn">Add Tab</button></form>
                </div>
                <div class="hide" id="tab_rename">
                    <form><input id="tab_rename_input" name="tab_rename_input" /><button id="tab_rename_btn">Rename</button></form>
                </div>
            </li>
		</ul>
        <div id="tabs_content" class="main clearfix">
            <form class="subject_tab" id="subject_tab_content" action="#">
                <?php
                $thumb_url = 'http://hoctudau.com/wp-content/themes/bueno/images/logo.png';
                if(isset($subject_settings)) {
                    $thumb_url = isset($subject_settings['ThumbnailUrl'])?$subject_settings['ThumbnailUrl']:'http://hoctudau.com/wp-content/themes/bueno/images/logo.png';
                }
                ?>
                <div class="subject_thumbnail">
                    <img id="thumbnail_image" src="<?php echo $thumb_url; ?>" />
                    <label for="thumnail_url">Image Url: </label><input onkeyup="updateSubject('ThumbnailUrl', this.value)" onblur="updateSubject('ThumbnailUrl', this.value)" value="<?php echo $thumb_url; ?>" id="thumbnail_url" name="thumbnail_url" />
                </div>
                <div class="subject_content">
                    <h3 onkeyup="updateSubjectTitle(this)" class="stand title" contenteditable="true"><?php echo $content_title; ?></h3>
                    <div class="content" contenteditable="true"><?php echo $content_content; ?></div>
                </div>
            </form>
            <?php
            if(isset($subject_meta_results) && !empty($subject_meta_results)) foreach($subject_meta_results as $subject_meta_result) {
                $meta_key = substr($subject_meta_result['meta_key'],strlen('subject_'));
                $meta_value = $this->helpers->json_decode_to_array_not_in_mysql($subject_meta_result['meta_value']);
            ?>
            <form class="subject_tab hide" id="subject_tab_<?php echo $meta_key; ?>" action="#">
                <h3 class="stand title" contenteditable="true"><?php echo $meta_value['title']; ?></h3>
                <div class="content" contenteditable="true"><?php echo !empty($meta_value['content'])?$meta_value['content']:'Empty Content'; ?></div>
            </form>
            <?php
            } else {
            ?>
            <form class="subject_tab hide" id="subject_tab_outline" action="#">
                <h3 class="stand title" contenteditable="true"><?php echo 'Empty Title'; ?></h3>
                <div class="content" contenteditable="true"><?php echo 'Empty Content'; ?></div>
            </form>
            <?php
            }
            ?>
        </div>
        
        <div class="clearfix hide" id="outline_builder">
            <?php
            if(!empty($subject_uuid)) {
            ?>
            <div id="form_list">
                <h2>All Forms</h2>
                <ul id="form_list_col">
                    <?php
                    foreach($forms_result as $form) {
                        $form_content = $this->helpers->json_decode_to_array($form['form_content']);
                    ?>
                    <li id="form_<?php echo $form['form_id']; ?>">
                        <a target="_blank" class="form_title" href="<?php echo $siteUrl . 'forms/' .  $form['form_uuid']; ?>"><?php echo $form_content['Name']; ?></a>
                        <span class="hold"><input onclick="(this.checked) ? addForm(this) : removeForm(this);" type="checkbox" class="checkbox" /></span>
                    </li>
                    <?php
                    }
                    ?>
                </ul>
                <?php
                if($total_rows > $rows_limit) {
                ?>
                <a class="load_forms" onclick="showMoreForms(this); return false;" href="#">Show More ( <i><?php echo $total_rows - $rows_limit; ?></i> )</a>
                <?php
                }
                ?>
            </div><!-- #Form List -->
            <div id="form_actions">
                <h2>Actions</h2>
                <a onclick="addForms(); return false;" href="#" class="button"><img title="Add." alt="Add." src="template/images/icons/add.png" class="faDel" />Add</a>
            </div>
            <div id="form_holder">
                <h2 class="fname">Forms Of This Subject</h2>
                <ul id="form_holder_col">
                    <?php 
                    if(!empty($sf_results))
                    foreach($sf_results as $sf_result) {
                        $form_content = $this->helpers->json_decode_to_array($sf_result['form_content']);
                    ?>
                    <li class="dragable" id="sform_<?php echo $sf_result['form_id']; ?>">
                        <a class="form_title" href="#"><?php echo $form_content['Name']; ?></a>
                        <span class="hold">
        		          <a class="button" onclick="deleteForm(this); return false;" href="#"><img title="Delete." alt="Delete." src="template/images/icons/delete.png" class="faDel" /></a>
                        </span>
                    </li>
                    <?php
                    }
                    ?>
                </ul>
            </div><!-- #Form Holder -->
            <?php
            } else {
            ?>
            <div id="nopermission" class="notice">
        		<h2>
        			You need to save this subject first.
        		</h2>
        	</div>
            <?php
            }
            ?>
        </div><!-- #Outline Builder -->
        <div class="saveSubject clearfix">
			<a class="button positive" id="saveSubject" href="#">
				<img alt="" src="template/images/icons/tick.png" /> Save Subject
            </a>
		</div>
	</div>
</div>

