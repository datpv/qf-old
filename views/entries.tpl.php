<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<div id="stage">
<form id="first">
	<div class="info">
		<h2>
			Entry Manager
		</h2>
		<div class="notranslate">
			This was the noblest Roman of them all.
		</div>
	</div>
	<div class="choose">
		Select an Entry from Below
		<strong>
			or
		</strong>
		<a class="button positive" href="#" onclick="unselectEntry(); return false">Create a New Entry</a>
	</div>
</form>
<!--first-->
<div id="main" class="hide notranslate ">
	<div class="ltr">
		<form id="entry_form" class="hoctudau topLabel " onsubmit="submitForm(this);" autocomplete="off" enctype="multipart/form-data" method="post" action="<?php echo $siteUrl; ?>entries/submit/<?php echo $formId; ?>" target="submit_form_here"
		novalidate >
			<div class="info">
				<h2 class="notranslate">
					Untitled Form
				</h2>
				<div class="notranslate">
					This is my form. Please fill it out. It's awesome!
				</div>
			</div>
			<ul>
				<?php echo $html; ?>
                <li class="buttons">
					<input type="hidden" name="currentPage" id="currentPage" value="fap0wuBer2xciTNLwuslashvMyWl9Vs33vnYnCLT6W7SIyej0fTk=" />
					<input id="saveForm" name="saveForm" class="btTxt submit" type="submit" tabindex="4" value="Submit" />
				</li>
				<li style="display:none">
				</li>
			</ul>
		</form>
	</div>
</div>
<!--main-->
<div id="newEntry" class="hide notranslate">
	<div class="ltr">
		<form id="entry_form" class="hoctudau topLabel " onsubmit="submitForm(this);" autocomplete="off" enctype="multipart/form-data" method="post" action="<?php echo $siteUrl; ?>entries/submit/<?php echo $formId; ?>" target="submit_form_here"
		novalidate>
			<div class="info">
				<h2 class="notranslate">
					Untitled Form
				</h2>
				<div class="notranslate">
					This is my form. Please fill it out. It's awesome!
				</div>
			</div>
			<ul>
				<?php echo $html; ?>
				<li class="buttons">
					<input type="hidden" name="currentPage" id="currentPage" value="fap0wuBer2xciTNLwuslashvMyWl9Vs33vnYnCLT6W7SIyej0fTk=" />
					<input id="saveForm" name="saveForm" class="btTxt submit" type="submit" tabindex="4" value="Submit" />
				</li>
				<li style="display:none">
				</li>
			</ul>
		</form>
	</div>
</div>
<div id="side" class="">
	<ul id="actions" class="clearfix hide">
		<li class="new">
			<a href="#" title="Create a New Entry" onclick="unselectEntry(); return false">
			<em></em>
			<b>New</b></a>
		</li>
		<li class="edi">
			<a href="#" title="Edit Current Entry" onclick="changeEditMode(1); return false">
			<em></em>
			<b>Edit</b></a>
		</li>
		<li class="vie">
			<a href="#" title="View a Read Only Version of the Current Entry" onclick="changeEditMode(0); return false">
			<em></em>
			<b>View</b></a>
		</li>
		<li class="ema undone">
			<a href="#" title="Email this Entry" onclick="emailEntry(); return false">
			<em></em>
			<b>Email</b></a>
		</li>
		<li class="pri undone">
			<a href="#" title="Print this Entry" onclick="printEntry(); return false">
			<em></em>
			<b>Print</b></a>
		</li>
		<li class="del">
			<a href="#" title="Delete this Entry" onclick="deleteEntry(); return false">
			<em></em>
			<b>Delete</b></a>
		</li>
	</ul>
	<div id="completeInfo" class="complete" title="Form Completion Status">
		<table class="pgStyle2" cellspacing="0">
			<tr>
				<td>
					<var id="pc" style="width:100%">
						<b id="dc">
							Complete
						</b>
					</var>
				</td>
			</tr>
		</table>
	</div>
	<h2 id="notify" class="hide">
		<a id="prevEntry" href="#" title="Previous Entry" onclick="nextEntry(); return false">&lsaquo;</a>
		<a id="nextEntry" href="#" title="Next Entry" onclick="previousEntry(); return false">&rsaquo;</a>
		<a id="permalink" href="#" title="Permanent Link to this Entry">
		<span id="notification">Viewing</span> <b id="notification_id"></b>
		</a>
	</h2>
	<div id="paymentInfo" class="hide">
		<h3>
			Payment Info
			<b id="pay_merchant" class="notranslate">
			</b>
			<a class="tooltip" title="About Payment Information" rel="Payment Information shows when a payment was made along with the form submission and the status of that payment from the merchant. Please note that Authorize.net integration DOES NOT reflect payments that have been declined after the submission.">(?)</a>
		</h3>
		<table cellspacing="0">
			<tbody>
				<tr>
					<td id="pay_status_class">
						<a href="#" onclick="togglePaidStatus(); return false">
						<var id="pay_amount" class="notranslate"></var>
						<b id="pay_status" class="notranslate">PAID</b>
						<span><em>&#x21E7;</em> Click to change the payment status.</span>
						</a>
					</td>
					<td>
						<b id="pay_date" class="notranslate">
						</b>
						<var id="pay_transaction" title="Confirmation Number" class="notranslate">
						</var>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div id="mturkInfo" class="hide">
		<h3>
			Amazon Mechanical Turk
			<b id="mturk_environment">
				Sandbox
			</b>
		</h3>
		<table cellspacing="0">
			<tbody>
				<tr>
					<td id="mturk_environment_class" class="neutral">
					</td>
					<td id="mturkIDs">
						<span class="hit">
							HIT :
							<var id="mturk_hit" class="notranslate">
							</var>
						</span>
						<span>
							Assignment :
							<var id="mturk_assignment" title="Assignment Id" class="notranslate">
							</var>
						</span>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div id="entryComments" class="hide">
		<h3 id="commentCount" class="hide">
			<a href="#" title="Subscribe to Comments for Untitled Form" class="notranslate">
			<img src="template/images/icons/rss.png" alt="RSS"></a>
			<span id="commentHead">
			</span>
		</h3>
		<div id="entryCommentsInner">
		</div>
		<form id="saveComment" action="#" onsubmit="return false;">
			<fieldset>
				<div>
					<label class="desc" for="comment_body">
						&middot; Add a Comment &middot;
					</label>
					<textarea class="textarea full" name="comment_body" id="comment_body" title="comment" rows="10" cols="50">
					</textarea>
				</div>
				<div>
					<label class="desc" for="comment_name">
						&middot; Your Name &middot;
					</label>
					<input class="text" type="text" name="comment_name" id="comment_name" value="datphan" />
				</div>
			</fieldset>
			<input class="submit translate" type="submit" onclick="saveComment();" value="Save Comment" />
		</form>
		<!--saveComment-->
	</div>
	<!--entryComments-->
</div>
<!--side-->
</div>
<!--stage-->
</div>
		<!--container-->
		<form id="entries" class="" method="post" action="#" onsubmit="return false;">
			<div id="entryFilter" style="display:none">
				<ul>
					<li>
						<span>
							Display entries that meet
							<select class="select" id="anyall">
								<option value="all">
									all
								</option>
								<option value="any">
									any
								</option>
							</select>
							of the following conditions :
						</span>
					</li>
					<li id="allConditions" class="hide">
						<span class="fval">
							<select id="standardConditionFieldList" class="select fixed conditionSelect" onblur="filterChange(this);" onchange="filterChange(this);">
							</select>
						</span>
						<span class="fval">
							<select class="select">
								<option value="Contains">
									contains
								</option>
								<option value="Does not contain">
									does not contain
								</option>
								<option value="Begins with">
									begins with
								</option>
								<option value="Ends with">
									ends with
								</option>
								<option value="Is equal to">
									is equal to
								</option>
								<option value="Is not equal to">
									is not equal to
								</option>
							</select>
						</span>
						<span class="fval">
							<input class="text" type="text" />
						</span>
						<span class="hide">
							<img class="datepicker" src="template/images/icons/calendar.png" alt="Pick a date." />
						</span>
						<span>
							<img class="button" src="template/images/icons/add.png" alt="Duplicate" onclick="window.addFilter(this.parentNode.parentNode);" />
							<img class="button deleteButton" src="template/images/icons/delete.png" alt="Delete" onclick="window.deleteFilter(this.parentNode);" />
						</span>
					</li>
					<li class="buttons" id="submitFilter">
						<a href="#" class="button positive" onclick="applyFilters();return false">
						<img src="template/images/icons/magnifier.png" alt=""/> Filter Entries</a>
						<a href="#" class="button negative" onclick="removeFilters();return false">
						<img src="template/images/icons/cross.png" alt=""/> Cancel</a>
					</li>
					<li>
						<div class="hide">
							<select id="basicFilters">
								<option value="Contains">
									contains
								</option>
								<option value="Does not contain">
									does not contain
								</option>
								<option value="Begins with">
									begins with
								</option>
								<option value="Ends with">
									ends with
								</option>
								<option value="Is equal to">
									is equal to
								</option>
								<option value="Is not equal to">
									is not equal to
								</option>
							</select>
							<select id="numberFilters">
								<option value="Is equal to">
									is equal to
								</option>
								<option value="Is greater than">
									is greater than
								</option>
								<option value="Is less than">
									is less than
								</option>
							</select>
							<select id="dateFilters">
								<option value="Is on">
									is on
								</option>
								<option value="Is before">
									is before
								</option>
								<option value="Is after">
									is after
								</option>
							</select>
							<select id="standardOptionsFormData">
								<optgroup label="Entry Info">
									<option value="EntryId">
										Entry ID
									</option>
									<option value="DateCreated">
										Date Created
									</option>
									<option value="CreatedBy">
										Created By
									</option>
									<option value="DateUpdated">
										Last Updated
									</option>
									<option value="UpdatedBy">
										Updated By
									</option>
									<option value="IP">
										IP Address
									</option>
								</optgroup>
							</select>
							<select id="standardOptionsMultiplePagesFormData">
								<optgroup label="Entry Info">
									<option value="EntryId">
										Entry ID
									</option>
									<option value="DateCreated">
										Date Created
									</option>
									<option value="CreatedBy">
										Created By
									</option>
									<option value="DateUpdated">
										Last Updated
									</option>
									<option value="UpdatedBy">
										Updated By
									</option>
									<option value="IP">
										IP Address
									</option>
									<option value="LastPage">
										Last Page Accessed
									</option>
									<option value="CompleteSubmission">
										Completion Status
									</option>
								</optgroup>
							</select>
							<select id="standardOptionsPaymentData">
								<optgroup label="Payment Info">
									<option value="Status">
										Payment Status
									</option>
									<option value="PurchaseTotal">
										Payment Total
									</option>
									<option value="Currency">
										Payment Currency
									</option>
									<option value="TransactionId">
										Payment Confirmation
									</option>
									<option value="MerchantType">
										Payment Merchant
									</option>
								</optgroup>
							</select>
						</div>
					</li>
				</ul>
			</div>
			<!--entryFilter-->
			<table id="gridBar" cellspacing="0">
				<tr>
					<td class="col1">
						<span id="grid_label" class="">
							Recent Entries
						</span>
						<a href="#" onclick="toggleBulkActions(); return false">Bulk Actions</a>
					</td>
					<td class="col2">
						<span class="quickSearch">
							<span class="ls" title="Advanced Search" onclick="toggleFilter();">
							</span>
							<input type="text" id="search" name="search" value="Search" onkeypress="runSearch(event);" onfocus="startSearch(this);" onblur="resetSearch(this)" class="translate"/>
							<span class="rs">
								<a id="searchHelper" href="#" onclick="clearSearch(); return false">Clear</a>
							</span>
						</span>
					</td>
					<td class="col3">
						<span id="navHolder">
						</span>
					</td>
					<tr>
			</table>
			<div id="gridHolder" class="dg">
			</div>
			<div id="bulk">
				<h3>
					<b id="bulk_entries">
					</b>
					<span id="bulk_message">
						Entries Selected
					</span>
				</h3>
				<div>
					<a href="#" onclick="showExportLightbox(); return false" class="button export">Export</a>
					<a href="#" onclick="deleteGridEntries(); return false" class="button delete">Delete</a>
					<a href="#" onclick="toggleBulkActions(); return false" class="button cancel">Cancel</a>
				</div>
			</div>
		</form>
        <iframe id="submit_form_here" name="submit_form_here" src="views/entries/blank.php" allowTransparency="true" height="1" width="1">
		</iframe>
        <div>