<div id="stage" class="afi">
	<div id="side">
		<ul id="tabs">
			<li id="afi">
				<a onmousedown="showFields(false);" title="Add fields to your form.">Add a Field</a>
			</li>
			<li id="cfi">
				<a onmousedown="selectField();" title="Change the properties of a field here.">Field Settings</a>
			</li>
			<li id="cfo">
				<a onmousedown="selectForm();" title="Change the properties for this form here.">Form Settings</a>
			</li>
		</ul>
		<div id="addFields" class="clearfix">
			<div id="shake">
				<h3 class="stand">
					Standard
				</h3>
				<ul id="col1">
					<li id="drag1_radio" class="dragfld 2 bigger">
						<a id="mc" class="button" href="#" onclick="addField('radio');return false"><b></b>
						Multiple Choice</a>
					</li>
				</ul>
                <ul id="col2">
					
				</ul>
                <ul id="col3">
					
				</ul>
                <ul id="col4">
					
				</ul>
			</div>
			<!-- shake -->
		</div>
		<!-- addFields -->
		<form id="fieldProperties" action="#" onsubmit="return false;" class="noI ">
			<div class="notice" id="noFieldSelected">
				<h3>
					<b>
						No Field Selected
					</b>
				</h3>
				<p>
					Please click on a field in the form preview on the right to change its properties.
				</p>
			</div>
			<ul id="allProps">
				<li class="num" id="fieldPos">
					1.
				</li>
				<li id="listTitle">
					<label class="desc">
						Field Label
						<a href="/docs/" class="tooltip" title="About Field Label" rel="A field's label is the most direct way of telling your user what kind of data 
						should be entered into a particular field. Field labels are usually just one or two words, 
						but can also be a question.">(?)</a>
					</label>
					<div>
						<textarea id="fieldTitle" class="textarea" rows="10" cols="35" onkeyup="updateProperties(this.value, 'Title')" onmouseup="updateProperties(this.value, 'Title')">
						</textarea>
					</div>
				</li>
				<li class="clear noheight">
				</li>
				
				<li id="listChoices" class="clear">
					<fieldset class="choices">
						<legend>
							Choices
							<a href="#" class="tooltip" title="About Choices" rel="This property shows what predefined options the user can select for that particular field. Use the plus and minus buttons to add and delete choices. Click on the star to make a choice the default selection. Multiple Choice fields will automatically select the first choice as default if none is specified.">(?)</a>
						</legend>
						<ul id="fieldChoices">
							<li class="dropReq">
								<a href="#" class="tooltip" title="About Required Dropdowns" rel="This is the default down choice. When you make a drop down field required, 
								you are essentially telling the user they have to make a choice. If they leave 
								the drop down on the default choice (the one that is selected when the page loads), 
								Hoctudauwill consider that as not making a choice and will throw an error if not changed.">(?)</a>
								<input type="radio" title="Make this choice pre-selected." />
								<input class="text" type="text" value="If Dropdown Required"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this choice."/>
							</li>
							<li>
								<input type="radio" title="Make this choice pre-selected." />
								<input class="text" type="text" value="For Radio / Dropdown"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this choice."/>
							</li>
							<li>
								<input type="checkbox" title="Make this choice pre-selected." />
								<input class="text" type="text" value="For Checkbox"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this choice."/>
							</li>
						</ul>
					</fieldset>
				</li>
                <!----------------------------------
                * Display Answers
                ------------------------------------>
                <li id="listDisplayAnswer" class="clear">
					<label class="desc" for="fieldDisplayRightAnswer">
						Description For Right Answer
						<a href="#" class="tooltip" title="Field Display Answer" rel="This is an optional property that displays the text specified to your users while they're picking right answer">(?)</a>
					</label>
					<div>
						<textarea class="textarea" rows="10" cols="50" id="fieldDisplayRightAnswer" onkeyup="updateProperties(this.value, 'RightAnswerDescription')" onmouseup="updateProperties(this.value, 'RightAnswerDescription')"></textarea>
					</div>
                    <label class="desc" for="fieldDisplayWrongAnswer">
						Description For Wrong Answer
						<a href="#" class="tooltip" title="Field Display Answer" rel="This is an optional property that displays the text specified to your users while they're picking wrong answer">(?)</a>
					</label>
					<div>
						<textarea class="textarea" rows="10" cols="50" id="fieldDisplayWrongAnswer" onkeyup="updateProperties(this.value, 'WrongAnswerDescription')" onmouseup="updateProperties(this.value, 'WrongAnswerDescription')"></textarea>
					</div>
				</li>
                <!----------------------------------
                ------------------------------------>
				<li class="left half clear" id="listOptions">
					<fieldset>
						<legend>
							Options
						</legend>
					</fieldset>
				</li>
                <li class="clear noheight"></li>
				<li id="listButtons" class="center">
					<a href="#" class="positive button" title="Duplicate this field." id="listButtonsDuplicate">
					<img src="template/images/icons/add.png" alt=""/> Duplicate</a>
					<a href="#" class="negative button" title="Delete this field." id="listButtonsDelete">
					<img src="template/images/icons/delete.png" alt=""/> Delete</a>
					<a href="#" class="button" title="Add another field." onclick="showFields(true);return false">
					<img src="template/images/icons/textfield_add.png" alt=""/> Add Field</a>
				</li>
			</ul>
		</form>
		<!-- fieldProperties -->
		<form id="formProperties" action="#" onsubmit="return false;" class="noI ">
			<ul id="listPagination">
				<li>
					<fieldset class="choices">
						<legend>
							Pagination Options
							<a href="#" class="tooltip" title="About Pagination Options" rel="By default on forms with more than one page,Hoctudau tries to present to the user some context at the top and bottom of every page
							to help the user understand how much of the form has been completed and what is left to be filled out. You 
							can choose to show this progress as a series of steps, a percentage bar or not have it show at all.">(?)</a>
						</legend>
						<ul class="pagingStyles">
							<li>
								<input id="breadcrumbs1" name="breadcrumbs" type="radio" value="1" onclick="updateForm($F(this), 'BreadCrumbType');" checked="checked" />
								<label for="breadcrumbs1">
									Steps
								</label>
								<input id="breadcrumbs2" name="breadcrumbs" type="radio" value="2" onclick="updateForm($F(this), 'BreadCrumbType');" />
								<label for="breadcrumbs2">
									Percentage
								</label>
								<input id="breadcrumbs0" name="breadcrumbs" type="radio" value="0" onclick="updateForm($F(this), 'BreadCrumbType');" />
								<label for="breadcrumbs0">
									No Context
								</label>
							</li>
						</ul>
						<div class="highlight">
							<input id="showPageTitles" class="checkbox" type="checkbox" value="1" checked="checked" onclick="(this.checked)?updateForm('1', 'ShowPageTitle'):updateForm('0','ShowPageTitle');" />
							<label class="choice" for="showPageTitles">
								Show Page Titles in Progress Bar
							</label>
							<a class="tooltip" title="About Page Titles in Progress Bar" rel="In addition to showing the page number or percentage of the form that's been completed, you
							can specify individual names for each page on your form. This is great for forms that want to create clear
							sign posts for each step in the form completion process.">(?)</a>
							<br />
							<ul id="pageTitles" class="">
								<li class="paymentPageTitle">
									<h4>
										Payment Page
									</h4>
									<input id="paymentTitle" name="" class="text" type="text" value="" onkeyup="updateForm(this.value, 'PaymentPageTitle')" onblur="updateForm(this.value, 'PaymentPageTitle')" />
									<a class="tooltip" title="About Payment Page Title" rel="This is the title for the payment summary pageHoctudau will redirect to after the form 
									is submitted if the page is payment enabled.">(?)</a>
								</li>
							</ul>
						</div>
						<ul class="pagelabelul">
							<li>
								<input id="pageFooter" name="" class="checkbox" type="checkbox" onclick="(this.checked)?updateForm('0', 'ShowPageFooter'):updateForm('1','ShowPageFooter');" />
								<label for="pageFooter" class="choice">
									Hide Page Numbers in Footer
								</label>
							</li>
						</ul>
					</fieldset>
				</li>
			</ul>
			<ul id="formSettings">
				<li>
					<label class="desc">
						Form Name
						<a class="tooltip" title="About Form Name" rel="The first piece of text displayed to the user when they see your form. The form name is also used to 
						create the URL that indicates where the form is located on the web.">(?)</a>
					</label>
					<div>
						<input id="formName" class="text large" type="text" value="" maxlength="50" onkeyup="updateForm(this.value, 'Name')" onblur="updateForm(this.value, 'Name')" />
					</div>
				</li>
				<li class="clear">
					<label class="desc">
						Description
						<a class="tooltip" title="About Form Description" rel="Use this property to display a short description or any instructions, notes, or guidelines that your user should read when filling out the form. This will appear directly below the form name.">(?)</a>
					</label>
					<div>
						<textarea class="textarea" rows="10" cols="50" id="formDescription" onkeyup="updateForm(this.value, 'Description')" onblur="updateForm(this.value, 'Description')">
						</textarea>
					</div>
				</li>
			</ul>
		</form>
		<!--formProperties-->
	</div>
	<!--side-->
	<div id="main" class="noI ">
		<form id="formPreview" class="hoctudau page1 topLabel" action="#">
			<div id="wuform" class="info" title="Click to edit.">
				<img src="template/images/arrow2.png" alt="" class="arrow" />
				<h2 id="fname" class="notranslate">
					<?php echo isset($_form)?$_form['Name']:'Untitled Form'; ?>
				</h2>
				<div id="fdescription" class="notranslate">
					<?php echo isset($_form)?$_form['Description']:'This is my form. Please fill it out. It\'s awesome!'; ?>
				</div>
			</div>
			<div class="notice" id="nofields" onclick="showFields(true);">
				<h2>
					<b>
						&larr; No Fields!
					</b>
					You should add a field.
				</h2>
			</div>
			<div id="nofieldsonpage" class="notice hide">
				<h2 onclick="showFields(true);">
					<b>
						&larr; This page is without fields!
					</b>
					Drag some here.
				</h2>
			</div>
            <ul class="" id="formFields">
                <li class="paging-context hide " onclick="selectPageSettings();" id="pageHeader">
				</li>
            </ul>
		</form>
		<div id="formButtons" class="<?php echo $buttonClass; ?>">
			<table cellspacing="0">
				<tr>
					<td>
						<a href="#" onclick="showFields(true);return false" class="button">
						<img src="template/images/icons/textfield_add.png" alt=""/> Add Field</a>
					</td>
					<td id="pagejump" class="pagejump hide">
						<img id="jumpprev" style="visibility:hidden" src="template/images/icons/smallprev.png" onclick="jumpToPage('prev'); return false;" />
						<input id="jumptarget" name="" class="text" type="text" value="1" />
						<img id="jumpnext" src="template/images/icons/smallnext.png" onclick="jumpToPage('next'); return false;" />
					</td>
					<td>
						<a href="#" id="saveForm" class="button positive">
						<img src="template/images/icons/tick.png" alt=""/> Save Form</a>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!--main-->
</div>
<!--stage-->