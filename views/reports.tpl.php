<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<div id="stage">
	<div class="info">
		<div class="buttons">
			<a class="button positive" href="<?php echo $siteUrl; ?>report/builder/" title="Create a New Report">
			<img src="template/images/icons/report_add.png" alt=""/> New Report!</a>
		</div>
		<!--buttons-->
		<h2>
			Report Manager
		</h2>
		<div class="notranslate">
			Move still, still so, and own no other function.
		</div>
	</div>
	<!--info-->
    <?php
    $i=0;
    if($count > 0 && $form_count > 0) {
        
    ?>
    <div id="main" class="reports">
    	<form id="" name="" class="search" onsubmit="return false;">
    		<label>
    			Filter
    		</label>
    		<input onkeyup="filterBy($F(this));" onsearch="filterBy($F(this));" id="searchBox" class="text" type="search" value="" />
    	</form>
    	<div id="sort">
    		<label>
    			Sort By :
    		</label>
    		<a id="ReportId" class="selected" href="#" onclick="sortBy('ReportId'); return false;">Date Created</a>
    		<a id="DateUpdated" class="" href="#" onclick="sortBy('DateUpdated'); return false;">Date Edited</a>
    	</div>
    	<div class="group">
    		<h3 id="searching" class="hide">
    			Filtered Results for :
    			<span id="searchTerm">
    			</span>
    		</h3>
    		<h3 id="noResults" class="hide">
    			None of your reports match your filter.
    		</h3>
    		<ul id="groupList-1">
                <?php
                $i = 0;
                foreach($reports_result as $report) {
                    $report_id = $report['report_id'];
                    $reportId = $report['report_uuid'];
                    if(empty($report['report_content'])) continue;
                    $_report = $this->helpers->json_decode_to_array($report['report_content']);
                ?>
    			<li value="d_<?php echo $reportId; ?>" id="li<?php echo $report_id; ?>" class="<?php echo ($i==0)?'first':'';$i++; echo ($report['report_status'] == 'D')?' notActive':''; ?>" onmouseover="showActions(this)" onmouseout="hideActions(this)">
    				<h4>
    					<a id="link<?php echo $report_id; ?>" href="<?php echo $siteUrl; ?>reports/<?php echo $reportId; ?>" target="_blank" class="notranslate">
    					<?php echo $_report['Name']; ?>
    					</a>
    				</h4>
    				<!--<span class="themeSelect">
    					<select onchange="changeTheme(<?php echo $report_id; ?>, 'Untitled%20Report', this)">
    						<option value="1">
    							Default Theme
    						</option>
    						<option value="create">
    							-----------
    						</option><!--
    						<option value="create">
    							New Theme!
    						</option>
    					</select>
    				</span>-->
    				<span class="activeCheck">
    					<label for="publicStatus_<?php echo $report_id; ?>">
    						Public
    					</label>
    					<input <?php echo ($report['report_status'] == 'A')?'checked="checked"':''; ?> type="checkbox" id="publicStatus_<?php echo $report_id; ?>" onclick="togglePublicReport(<?php echo $report_id; ?>, 'Untitled%20Report', this)" />
    				</span>
    				<div id="expandThis">
    					<div class="actions">
    						<a class="del" onclick="deleteReport(this); return false;" href="#">Delete</a>
    						<a class="dup" onclick="duplicateReport(this); return false;" href="#">Duplicate</a>
    						<a class="view" href="<?php echo $siteUrl; ?>reports/<?php echo $reportId; ?>" target="_blank">View</a>
    						<a class="edit" href="#" onclick="editReport(this); return false;">Edit</a>
    						<!--<a class="export undone" href="#" onclick="exportMe(this);return false;">Export</a>
    						<a class="code undone" href="../widget/manager/index.php?reportId=<?php echo $reportId; ?>" target="_self">Widgets</a>
    						--><span>
    							<a class="protect" href="#" onclick="showProtectReport(this);return false;">Protect</a>
    						</span>
    					</div>
    				</div>
    			</li>
                <?php
                }
                ?>
    		</ul>
    	</div>
    	<!--group-->
    	<div id="nopermission" class="notice hide">
    		<h2>
    			You do not have any reports.
    		</h2>
    	</div>
    </div>
    <!--main-->
    <?php
    } else {
    ?>
	<div class="notice bigMessage">
		<h2>
        <?php if($form_count > 0): ?>
			<a href="<?php echo $siteUrl; ?>report/builder/">
            Oh no. Buddy! <span class="bigMessageRed">You don't have any reports.</span> <span class="bigMessageGreen">Let's go make one!</span>
            </a>
        <?php else: ?>
            <span class="bigMessageRed">Holy carts before horses!</span> You need a <span class="bigMessageBlue">form</span> before you can make a <span class="bigMessagePurple">report</span>
        <?php endif; ?>
		</h2>
	</div>
    <?php
    }
    ?>
</div>
<!--stage-->