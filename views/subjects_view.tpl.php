<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<div id="stage" class="afi">
    <?php
    $settings = $this->helpers->json_decode_to_array($subject_result['subject_settings']);
    ?>
	<div id="full">
        <div id="subject_settings" class="clearfix">
            <?php
            if($user_id) {
            ?>
            <div id="subject_actions" class="subject_actions right">
                <?php
                if(!$su_result) {
                ?>
                <a id="registerSubject" onclick="registerSubject(this); return false;" href="#" class="button">UNLOCK</a>
                <?php
                } else {
                    if($su_result['subject_unlocked'] == 'N') {
                ?>
                <span class="button pending">PENDING</span>
                <?php
                    } else {
                ?>
                <span class="button unlocked">UNLOCKED</span>
                <?php
                    }
                }
                ?>
            </div>
            <?php
            }
            ?>
            <div class="subject_info left">
                <div class="col1 col">
                    <p>
                        <strong>Expired On:</strong> <span><?php echo isset($settings['ExpireDate'])?$settings['ExpireDate']:''; ?></span><br />
                    </p>
                </div>
                <div class="col2 col">
                    <p>
                        <strong>Limit User:</strong> <span><?php echo isset($settings['LimitUser'])?$settings['LimitUser']:''; ?></span><br />
                    </p>
                </div>
                <div class="col3 col">
                    <p>
                        <strong>Price:</strong> <span><?php echo isset($settings['Price'])?$settings['Price']:''; ?></span><br />
                    </p>
                </div>
            </div>
        </div>
		<ul id="tabs" class="clearfix">
			<li class="selected">
				<a rel="subject_tab_content" href="#subject_tab_content">Content</a>
			</li>
            <?php
            if(isset($subject_meta_results) && !empty($subject_meta_results)) foreach($subject_meta_results as $subject_meta_result) {
                $meta_key = substr($subject_meta_result['meta_key'],strlen('subject_'));
                $key_name = $subject_meta_result['key_name'];
            ?>
            <li>
				<a rel="subject_tab_<?php echo $meta_key; ?>" href="#subject_tab_<?php echo $meta_key; ?>"><?php echo $key_name; ?></a>
			</li>
            <?php
            }
            ?>
		</ul>
        <div id="tabs_content" class="main clearfix">
            <?php
            $content_title = 'Empty Title';
            $content_content = 'Empty Content';
            if(isset($subject_result)) {
                $subject_content = $this->helpers->json_decode_to_array($subject_result['subject_content']);
                $content_title = $subject_result['subject_name'];
                $content_content = $subject_content['content'];
            }
            ?>
            <div class="subject_tab" id="subject_tab_content" action="#">
                <?php
                $thumb_url = 'http://hoctudau.com/wp-content/themes/bueno/images/logo.png';
                if(isset($subject_settings)) {
                    $thumb_url = isset($subject_settings['ThumbnailUrl'])?$subject_settings['ThumbnailUrl']:'http://hoctudau.com/wp-content/themes/bueno/images/logo.png';
                }
                ?>
                <div class="subject_thumbnail">
                    <img alt="<?php echo $content_title; ?>" src="<?php echo $thumb_url; ?>" />
                </div>
                <div class="subject_content">
                    <h3 class="stand title"><?php echo $content_title; ?></h3>
                    <div class="content"><?php echo $content_content; ?></div>
                </div>
            </div>
            <?php
            if(isset($subject_meta_results) && !empty($subject_meta_results)) foreach($subject_meta_results as $subject_meta_result) {
                $meta_key = substr($subject_meta_result['meta_key'],strlen('subject_'));
                $meta_value = $this->helpers->json_decode_to_array_not_in_mysql($subject_meta_result['meta_value']);
            ?>
            <div class="subject_tab hide" id="subject_tab_<?php echo $meta_key; ?>" action="#">
                <h3 class="stand title"><?php echo $meta_value['title']; ?></h3>
                <div class="content"><?php echo !empty($meta_value['content'])?$meta_value['content']:'Empty Content'; ?></div>
                <?php
                if($meta_key == 'outline') {
                ?>
                <div id="form_holder">
                    <h2 class="fname">Forms For This Subject</h2>
                    <ul id="form_holder_col">
                        <?php 
                        if(isset($sf_results))
                        foreach($sf_results as $sf_result) {
                            $form_content = $this->helpers->json_decode_to_array($sf_result['form_content']);
                        ?>
                        <li class="dragable">
                            <a target="_blank" class="form_title" href="<?php echo $siteUrl . 'forms/'.$sf_result['form_uuid']; ?>"><?php echo $form_content['Name']; ?></a>
                        </li>
                        <?php
                        }
                        ?>
                    </ul>
                </div><!-- #Form Holder -->
                <?php
                }
                ?>
            </div>
            <?php
            }
            ?>
        </div>
        
	</div>
</div>

