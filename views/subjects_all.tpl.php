<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<div id="stage" class="afi">
	<div id="full">
        <div class="subject_list">
    		<ul>
                <?php 
                foreach($subjects_result as $subject_result) {
                    $subject_settings = $this->helpers->json_decode_to_array($subject_result['subject_settings']);
                    $subject_content = $this->helpers->json_decode_to_array($subject_result['subject_content']);
                    $thumb_url = 'http://hoctudau.com/wp-content/themes/bueno/images/logo.png';
                    if(!empty($subject_settings)) {
                        $thumb_url = isset($subject_settings['ThumbnailUrl'])?$subject_settings['ThumbnailUrl']:'http://hoctudau.com/wp-content/themes/bueno/images/logo.png';
                    }
                ?>
                <li>
                	<div class="thumbnail_image">
                		<a href="<?php echo $siteUrl . 'subjects/view/' . $subject_result['subject_uuid']; ?>">
                            <img alt="<?php echo $subject_result['subject_name']; ?>" src="<?php echo $thumb_url; ?>" />
                        </a>
                	</div>
                	<div class="subject_content">
                        <h2><a href="<?php echo $siteUrl . 'subjects/view/' . $subject_result['subject_uuid']; ?>"><?php echo $subject_result['subject_name']; ?></a></h2>
                        <div class="subject_excerpt">
                            <?php echo $this->helpers->truncate(strip_tags($subject_content['content']),1000); ?>
                        </div>
                    </div>
                </li>
                <?php
                }
                ?>
            </ul>
        </div>
	</div>
</div>

