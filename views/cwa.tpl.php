<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<div id="container" class="ltr">
    <h1 id="logo">
    	<a href="#">Hoctudau</a>
    </h1>
	<header id="header" class="info">
		<div class="buttons">
		</div>
        <h2 class="notranslate">
            Common Wrong Answers Of <strong>Form:</strong> <a href="<?php echo $siteUrl; ?>forms/<?php echo $form['form_uuid']; ?>"><?php echo $_form['Name']; ?></a>
		</h2>
		<div class="notranslate">
            
		</div>
	</header>
	<div id="stage" class="clearfix">
        <div id="comments" class="clearfix">
            <h3 class="subject">Bình Luận Cho Những Câu Sai</h3>
            <div>&nbsp;</div>
            <div class="suggestions">
                <ul class="parent">
                    <?php
                    $first_log = -1;
                    $displayedFieldId = array();
                        foreach($logs3 as $kl => $logs) {
                            $displayedFieldId[$kl] = $kl;
                        ?>
                        <li id="field_<?php echo $kl; ?>">
                            <h4 class="title"><span class="fieldTitle"><?php echo $logs4[$kl]; ?></span><a class="add_wa" href="#">Add New Wrong Answer</a><div class="wronga"></div></h4>
                            <ul class="child">
                            <?php
                            foreach($logs as $key => $log) {
                                $type = $log['type'];
                               
                                $first_log++;
                                $log_content = array();
                                $log_content = $this->helpers->json_decode_to_array($log['log_content']);
                                $log_value = $log['log_subject'];
                                if($type == 'checkbox') {
                                    $field = $log['field'];
                                    $field_ids = explode('-',$log_value);
                                    $log_values = array();
                                    foreach($field['SubFields'] as $subF) {
                                        if(in_array($subF['ColumnId'],$field_ids)) {
                                            $log_values[] = $subF['ChoicesText'];
                                        }
                                    }
                                    $log_value = implode(' | ', $log_values);
                                } elseif($log_content['type'] == 'radio' || $log_content['type'] == 'select') {
                                    $field = $log_content['field'];
                                    $log_value_id = explode('_',$log_value);
                                    foreach($field['Choices'] as $ck => $choiC) {
                                        if(isset($log_value_id[1])) if($log_value_id[1] == $ck) {
                                            $log_value = $choiC['Choice'];
                                            break;
                                        }
                                    }
                                }
                            ?>
                            <li id="log_<?php echo $log['log_id']; ?>">
                                <header class="heading"><span class="value"><?php echo $log_value; ?></span> <span class="hits">( <?php echo $log['log_hit']; ?> Hits ) </span><a href="#" class="add_comment">Add comments</a> <a href="#" class="del_wa delete">Delete Answer</a></header>
                                <div class="comments">
                                    <ul>
                                        <?php
                                        if($log['comments']) foreach($log['comments'] as $comment) {
                                            $user = $this->helpers->json_decode_to_array_not_in_mysql($comment['user']['user_content']);
                                        ?>
                                        <li id="comment_<?php echo $comment['comment_id']; ?>">
                                            <div class="avatar" href="#">
                                                <a class="image" target="_blank" href="https://vi.gravatar.com/site/signup/" title="Change Avatar">
                                                    <img width="50" src="<?php echo $this->helpers->get_gravatar($comment['user']['user_email']); ?>" />
                                                </a>
                                                <div class="info">
                                                    <?php
                                                    if($this->options['show_nickh2d']) {
                                                    ?>
                                                    <span class="user_h2d"><?php echo $user['NickH2d']; ?></span><br />
                                                    <?php
                                                    }
                                                    ?>
                                                    <span class="user_fullname"><?php echo $user['Name']; ?></span>
                                                </div>
                                                
                                            </div>
                                            <div class="comment_content">
                                                <?php
                                                if($this->helpers->isAdmin()) {
                                                ?>
                                                <div class="actions"><a class="delete" onclick="deleteComment(this); return false;" href="#">Delete</a></div>
                                                <?php
                                                }
                                                ?>
                                                <p><?php echo nl2br($comment['comment_content']); ?></p>
                                            </div>
                                        </li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                    <?php
                                    if($log['total_comments'] > 3) {
                                    ?>
                                    <div class="view_comments"><a onclick="loadComments(this); return false;" href="#">View More <b>(<?php echo $log['total_comments'] - 3; ?>)</b></a></div>
                                    <?php
                                    }
                                    if($first_log == 0) {
                                    ?>
                                    <div class="comment_wrap">
                                        <form id="comment_form">
                                            <textarea id="comment_text" name="comment_text"></textarea>
                                            <button onclick="addComment(this); return false;" id="submitFormButton" name="submitFormButton">Comment</button>
                                        </form>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </li>
                            <?php
                            }
                            ?>
                            </ul>
                        </li>
                        <?php
                        }
                        foreach($fields as $field) {
                            if($field['Typeof'] == 'text' || $field['Typeof'] == 'select' || $field['Typeof'] == 'radio' || $field['Typeof'] == 'checkbox') {
                                if(!in_array($field['ColumnId'], $displayedFieldId)) {
                        ?>
                        <li id="field_<?php echo $field['ColumnId']; ?>">
                            <h4 class="title"><span class="fieldTitle"><?php echo $field['Title']; ?></span><a class="add_wa" href="#">Add New Wrong Answer</a>
                            <div class="wronga"></div></h4>
                            <ul class="child">
                                
                            </ul>
                        </li>
                        <?php
                                }
                            }
                        }
                    ?>
                </ul>
            </div>
        </div>
	</div>
    <div class="hide" id="comment_form_container">
        <div class="comment_wrap">
            <form id="comment_form">
                <textarea id="comment_text" name="comment_text"></textarea>
                <button onclick="addComment(this); return false;" id="submitFormButton" name="submitFormButton">Comment</button>
            </form>
        </div>
        <div class="wronga_wrap">
            <form id="wronga_form">
                <textarea id="wrongat_text" name="wronga_text"></textarea>
                <button onclick="addWa(this); return false;" id="submitFormButton2" name="submitFormButton2">Add</button>
            </form>
        </div>
    </div>