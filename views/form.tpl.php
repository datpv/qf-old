<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<div id="container" class="ltr">
    <h1 id="logo">
    	<a href="#">Hoctudau</a>
    </h1>
    <form id="form8" name="form8" class="hoctudau <?php echo $_form['LabelAlign']; ?> page1" autocomplete="off" enctype="multipart/form-data" method="post" novalidate action="<?php echo $siteUrl; ?>forms/<?php echo $formId; if($formType == 'score') echo '/'.$entryId; if($f) echo '&f=' . $f; ?>#public">
    	<header id="header" class="info">
    		<h2>
    			<?php //echo $this->helpers->detectLink($_form['Name']);
                echo $title;
                ?>
    		</h2>
    		<div>
    			<?php echo $this->helpers->detectLink($_form['Description']); ?>
    		</div>
    	</header>
        <?php
        if($formType == 'score') {
        ?>
        <div id="entry_content" style="border: 1px solid #aaa;overflow: hidden; padding: 5px;">
        <?php
            echo $formNBDisplayResult;
        ?>
        </div>
        <?php
        }
        ?>
    	<ul>
            <?php if(isset($errors)): ?>
            <li id="errorLi">
            <h3 id="errorMsgLbl">There was a problem with your submission.</h3>
            <p id="errorMsg">
            <?php
                foreach($errors as $error) {
                    echo $error . '<br>';
                }
            ?>
            </p>
            </li>
    		<?php
            endif;
                echo $html;
            if($_form['UseCaptcha']) {
            ?>
            <li id="fo14licaptcha" class="captcha notranslate <?php if(isset($errorFields["recaptcha_response_field"])): echo ' error'; endif; ?>">
                <label class="desc" for="recaptcha_response_field">
                Type the two words from the image below.
                <span class="req">*</span>
                </label>
                <script type="text/javascript">
                var RecaptchaOptions = {theme : 'clean'};
                var host = (("https:" == document.location.protocol) ? "https://www." : "http://www.");
                document.write(unescape("%3Cscript src='" + host + "google.com/recaptcha/api/challenge?k=6LfNO9wSAAAAADPYua7W_7hRj7pdyhw5VVrDR4cQ' type='text/javascript'%3E%3C/script%3E"));
                </script>
                <noscript>
                    <style type="text/css">
                    form li.captcha label.desc{
                        display:none;
                    }
                    form li.captcha{
                        border-bottom:1px dotted #ccc;
                        padding-bottom:1.2em;
                    }
                    </style>
                    <div class="noscript">
                    <label class="desc">Step 1</label>
                    <iframe src="http://www.google.com/recaptcha/api/noscript?k=6LfNO9wSAAAAADPYua7W_7hRj7pdyhw5VVrDR4cQ" height="300" width="500" frameborder="0"></iframe><br>
                    <textarea name="recaptcha_challenge_field" rows="3" cols="40">
                    </textarea>
                    <input type="hidden" name="recaptcha_response_field" value="manual_challenge" />
                    </div>
                </noscript>
            </li>
            <?php
            }
            ?>
    		<li class="buttons ">
    			<div>
    				<input id="saveForm" name="saveForm" class="btTxt submit" type="submit" value="<?php echo isset($_theme['buttonValue'])?$_theme['buttonValue']:'Submit'; ?>" onmousedown="doSubmitEvents();" />
    			</div>
    		</li>
            <?php
            ?>
    		<li class="hide">
    			<label for="comment">
    				Do Not Fill This Out
    			</label>
    			<textarea name="comment" id="comment" rows="1" cols="1">
    			</textarea>
    			<input type="hidden" id="clickOrEnter" name="clickOrEnter" value="" />
    		</li>
    	</ul>
    </form>
<!--container-->