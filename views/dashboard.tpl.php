<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<?php
$cookies = $this->helpers->getCookies();
extract($cookies);
?>
<div id="stage">
<div id="main" class="dg">
	<div class="info">
        <?php
        if($user_id == 1) :
        ?>
        <div class="buttons">
            <a href="<?php echo $siteUrl; ?>statistic/" class="button">View Statistic</a>
        </div>
        <?php
        endif;
        ?>
		<h2>
			Dashboard
		</h2>
		<div class="notranslate">
        T: Tổng, TG: Thầy Giáo, HS: Học Sinh, DC: Date Created, System: Điểm Tự Động
		</div>
	</div>
    <div class="sep-block">
        <div class="subdomain clearfix">
			<p id="statistic" class="plan1 notranslate">
            <?php
            $pc = $apc = $spc = 'unmark';
            if($user['cache_stats_hs'] > 80) {
                $pc = 'markred';
            }
            if($user['cache_stats_tg'] > 80) {
                $pc = 'markred';
            }
            if($user['cache_stats_sys'] > 80) {
                $pc = 'markred';
            }
            ?>
            Public: <?php echo $user['cache_stats_t'] ; ?> - <strong>HS: </strong><span class="<?php echo $pc; ?>"><?php echo $user['cache_stats_hs'] ; ?>%</span> | <strong>TG: </strong><span class="<?php echo $apc; ?>"><?php echo $user['cache_stats_tg'] ; ?>%</span> _ Private: <?php echo $user['cache_stats_sys_t'] ; ?> - <strong> System: </strong><span class="<?php echo $spc; ?>"><?php echo $user['cache_stats_sys'] ; ?>%</span>"
			</p>
		</div>
        <div class="tab">
            <ul>
                <li><a rel="ui_container" href="#">User Info</a></li>
                <li><a rel="gm_container" href="#">Group Members</a></li>
                <li><a rel="as_container" href="#">Form Nộp Private</a></li>
                <li><a rel="nb_container" href="#">Form Nộp Public</a></li>
                <li><a rel="nbed_container" href="#">Form Cần Chấm</a></li>
                <li><a rel="sced_container" href="#">Form Đã Chấm</a></li>
            </ul>
        </div>
    </div>
	<div class="block">
		<div id="ui_container" class="fullTable hide">
        <form id="user_info" action="#">
			<table class="table" cellspacing="0">
				<caption>
					<h3>
						User Information
					</h3>
				</caption>
				<tfoot>
					<tr>
						<td colspan="2">
                            <?php
                            if(isset($_SESSION['admin_user'])) {
                            ?>
							<a onclick="updateAccount(this); return false;" href="#" class="button">Update User Infomation</a>
                            <?php
                            }
                            ?>
                            <a onclick="showChangePasswordLightbox(this); return false;" href="#" class="button">Change User Password</a>
						</td>
					</tr>
				</tfoot>
				<tbody>
					<tr>
						<th>
							Username:
						</th>
						<td class="notranslate">
							<?php echo $user['cache_user_username']; ?>
						</td>
					</tr>
					<tr>
						<th>
							Email:
						</th>
						<td class="notranslate">
							<?php echo $user['cache_user_email']; ?>
						</td>
					</tr>
					<tr>
                        <?php
                        if($this->options['show_nickh2d']) {
                        ?>
						<th>
							Nick H2d:
						</th>
						<td class="notranslate">
                            <?php
                            if(!isset($_SESSION['admin_user'])) {
                            echo $user['cache_user_nickh2d'];
                            } else {
                            ?>
							<input type="text" name="nickh2d" id="nickh2d" value="<?php echo $user['cache_user_nickh2d']; ?>" />
                            <?php
                            }
                            ?>
						</td>
                        <?php
                        }
                        ?>
					</tr>
                    <tr>
                        <?php
                        if($this->options['show_nickskype']) {
                        ?>
						<th>
							Nick Skype:
						</th>
						<td class="notranslate">
                            <?php
                            if(!isset($_SESSION['admin_user'])) {
                            echo $user['cache_user_nickskype'];
                            } else {
                            ?>
							<input type="text" name="nickskype" id="nickskype" value="<?php echo $user['cache_user_nickskype']; ?>" />
                            <?php
                            }
                            ?>
						</td>
                        <?php
                        }
                        ?>
					</tr>
                    <tr>
						<th>
							Tên Đầy Đủ:
						</th>
						<td class="notranslate">
                            <?php
                            if(!isset($_SESSION['admin_user'])) {
                            echo $user['cache_user_fullname'];
                            } else {
                            ?>
							<input type="text" name="fullname" id="fullname" value="<?php echo $user['cache_user_fullname']; ?>" />
                            <?php
                            }
                            ?>
						</td>
					</tr>
                    <tr>
						<th>
							Courses/Groups:
						</th>
						<td class="notranslate">
                            <ul>
                            <?php                  
                            foreach($groups_meta_result as $group_meta) {
                                $group_id = $group_meta['group_id'];
                            ?>
                                <li>GROUP: <strong><?php echo $group_meta['group_name'] ?></strong> <a class="button" href="#" onclick="showChangeGroupLightbox(this); return false;">Change Group</a></li>
                            <?php
                            }
                            ?>
                            </ul>
                            <a class="button" href="#" onclick="showJoinGroupLightbox(this); return false;">Join Group</a>
						</td>
					</tr>
				</tbody>
			</table>
        </form>
		</div>
        <div id="gm_container" class="fullTable hide">
            <?php
            foreach($groups_meta_result as $group_meta) {
                $group_id = $group_meta['group_id'];
                $group_name = $group_meta['group_name'];
                $users_grouped = $group_meta['users_grouped'];
            ?>
                <table class="table" cellspacing="0">
                <thead style="padding-bottom: 7px;">
					<caption><h3>
						<?php echo $group_name;?>
					</h3></caption>
				</thead>
                <tbody>
                    <?php
                    if($users_grouped) foreach($users_grouped as $ug) {
                    $ug_user_id = $ug['user_id'];
                    ?>
                    <tr <?php echo ($user_id == $ug['cache_user_id'])?'class="hightlight"':''; ?>>
                        <td><?php echo $ug['cache_user_fullname'] ; ?></td>
                        <td>
                            <strong>T:</strong> <?php echo $ug['cache_stats_t'] ; ?>
                            <strong>HS:</strong> <?php echo $ug['cache_stats_hs'] ; ?>
                            <strong>TG:</strong> <?php echo $ug['cache_stats_tg'] ; ?>
                            <strong>Sys:</strong> <?php echo $ug['cache_stats_sys'] ; ?>
                        </td>
                    </tr>
                    <?php
                    }
                    ?>
                </tbody>
                </table>
            <?php
            }
            ?>
        </div>
	</div>
	<!--col2-->
    <div class="block">
        <div id="as_container" class="fullTable hide">
            <div>
            <div class="sorts">
                <form id="as" class="form-actions" action="#">
                    <strong>Sort By: </strong>
                    <a rel="dc" class="<?php if($active_sort_as == 'dc') echo 'active'; ?> button" href="#">DC</a>
                    <a rel="sys" class="<?php if($active_sort_as == 'sys') echo 'active'; ?> button" href="#">System</a>
                    <strong>Direction: </strong>
                    <select class="direction" name="direction">
                        <option <?php if($active_dir_as == 'asc') echo 'selected="selected"'; ?> value="asc">ASC</option>
                        <option <?php if($active_dir_as == 'desc') echo 'selected="selected"'; ?> value="desc">DESC</option>
                    </select>
                </form>
            </div>
            <table class="table list_nb_sc" cellspacing="0">
				<caption style="padding-bottom: 7px;">
					<h3>
						Các bài làm đã nộp (Private) <?php $as_numrows = count($entries_result_as); echo '('.$as_numrows.')'; ?>
					</h3>
				</caption>
				<tfoot>
					<tr>
						<td colspan="2">
							
						</td>
					</tr>
				</tfoot>
				<tbody>
                    <?php
                    if($as_numrows == 0) {
                    ?>
                    <tr>
						<th>
							&nbsp;
						</th>
						<td class="notranslate">
							Bạn chưa nộp bài nào
						</td>
					</tr>
                    <?php
                    }
                    foreach($entries_result_as as $entry) {
                    ?>
					<tr>
						<td>
							<?php echo $entry['cache_form_nb_name']; ?>
						</td>
                        <td class="notranslate">
							<a title="xem tổng điểm" class="summary" rel="<?php echo $entry['cache_entry_uuid']; ?>" target="_blank" href="<?php echo $siteUrl; ?>summary/<?php echo $entry['cache_entry_uuid']; ?>"><strong>System: </strong><span class="<?php echo ($entry['cache_entry_sys']>80)?'markred':'unmark'; ?>"><?php echo $entry['cache_entry_sys']; ?>%</span></a>
                            
                        <?php
                        $cache_content = $this->helpers->json_decode_to_array_not_in_mysql($entry['cache_content']);
                        foreach($cache_content['excerpts'] as $exerpt) {
                            echo '<div class="excerpts">'.$exerpt['value'] . '<div class="preview">&nbsp;</div></div>';
                        }
                        ?>
						</td>
					</tr>
                    <?php
                    }
                    ?>
				</tbody>
			</table>
            </div>
        </div>
        <div id="nb_container" class="fullTable hide">
            <div>
            <div class="sorts">
                <form id="nb" class="form-actions" action="#">
                    <strong>Sort By: </strong>
                    <a rel="dc" class="<?php if($active_sort_nb == 'dc') echo 'active'; ?> button" href="#">DC</a>
                    <a rel="t" class="<?php if($active_sort_nb == 't') echo 'active'; ?> button" href="#">T</a>
                    <a rel="hs" class="<?php if($active_sort_nb == 'hs') echo 'active'; ?> button" href="#">HS</a>
                    <a rel="tg" class="<?php if($active_sort_nb == 'tg') echo 'active'; ?> button" href="#">TG</a>
                    <strong>Direction: </strong>
                    <select class="direction" name="direction">
                        <option <?php if($active_dir_nb == 'asc') echo 'selected="selected"'; ?> value="asc">ASC</option>
                        <option <?php if($active_dir_nb == 'desc') echo 'selected="selected"'; ?> value="desc">DESC</option>
                    </select>
                </form>
            </div>
            <table class="table list_nb_sc" cellspacing="0">
				<caption style="padding-bottom: 7px;">
					<h3>
						Các bài làm đã nộp (Public) <?php $nb_numrows = count($entries_result_nb);echo '('.$nb_numrows.')'; ?>
					</h3>
				</caption>
				<tfoot>
					<tr>
						<td colspan="3">
							
						</td>
					</tr>
				</tfoot>
				<tbody>
                    <?php
                    if($nb_numrows == 0) {
                    ?>
                    <tr>
						<th>
							&nbsp;
						</th>
						<td class="notranslate">
							Bạn chưa nộp bài nào
						</td>
					</tr>
                    <?php
                    }
                    foreach($entries_result_nb as $entry) {
                    ?>
					<tr>
						<td>
							<?php echo $entry['cache_form_nb_name']; ?>
						</td>
						<td class="notranslate">
							<a target="_blank" href="<?php echo $siteUrl; ?>forms/<?php echo $entry['cache_form_sc_uuid']; ?>/<?php echo $entry['cache_entry_uuid']; ?>">Link Chấm Điểm</a>
						</td>
                        <td class="notranslate">
							<a title="xem tổng điểm" class="summary" rel="<?php echo $entry['cache_entry_uuid']; ?>" target="_blank" href="<?php echo $siteUrl; ?>summary/<?php echo $entry['cache_entry_uuid']; ?>"><strong>T: </strong><?php echo $entry['cache_entry_t']; ?> <strong>HS: </strong><span class="<?php echo ($entry['cache_entry_hs']>80)?'markred':'unmark'; ?>"><?php echo $entry['cache_entry_hs']; ?>%</span> | <strong>TG: </strong><span class="<?php echo ($entry['cache_entry_tg']>80)?'markred':'unmark'; ?>"><?php echo $entry['cache_entry_tg']; ?>%</span></a>
                            
                        <?php 
                        $cache_content = $this->helpers->json_decode_to_array_not_in_mysql($entry['cache_content']);
                        foreach($cache_content['excerpts'] as $exerpt) {
                            echo '<div class="excerpts">'.$exerpt['value'] . '<div class="preview">&nbsp;</div></div>';
                        }
                        ?>
						</td>
					</tr>
                    <?php
                    }
                    ?>
				</tbody>
			</table>
            </div>
        </div>
    </div>
    <div class="block">
    <div id="sced_container" class="fullTable hide">
        <div class="contain_nb_sc">
        <div class="sorts">
            <form id="sced" class="form-actions" action="#">
                <strong>Sort By: </strong>
                <a rel="dc" class="<?php if($active_sort_sced == 'dc') echo 'active'; ?> button" href="#">DC</a>
                <a rel="t" class="<?php if($active_sort_sced == 't') echo 'active'; ?> button" href="#">T</a>
                <a rel="hs" class="<?php if($active_sort_sced == 'hs') echo 'active'; ?> button" href="#">HS</a>
                <a rel="tg" class="<?php if($active_sort_sced == 'tg') echo 'active'; ?> button" href="#">TG</a>
                <strong>Direction: </strong>
                <select class="direction" name="direction">
                    <option <?php if($active_dir_sced == 'asc') echo 'selected="selected"'; ?> value="asc">ASC</option>
                    <option <?php if($active_dir_sced == 'desc') echo 'selected="selected"'; ?> value="desc">DESC</option>
                </select>
                <br /><br />
                <strong>Class/Group:  </strong>
                <select class="classmate" name="classmate">
                    <?php
                    foreach($groups_meta_result as $group_meta) {
                    $group_id = $group_meta['group_id'];
                    $group_name = $group_meta['group_name'];
                    ?>
                    <option value="<?php echo $group_id; ?>"><?php echo $group_name; ?></option>
                    <?php
                    }
                    ?>
                </select>
            </form>
        </div>
        <table class="table list_nb_sc" cellspacing="0">
			<caption style="padding-bottom: 7px;">
				<h3>
					Các bài đã chấm <?php $sced_numrows = count($entries_result_sced);echo '('.$entries_result_sced_rows.')'; ?>
				</h3>
			</caption>
			<tfoot>
				<tr>
					<td colspan="2">
						
					</td>
				</tr>
			</tfoot>
			<tbody>
                <?php
                if($sced_numrows == 0) {
                ?>
                <tr>
					<th>
						&nbsp;
					</th>
					<td class="notranslate">
						Bạn chưa chấm bài nào hoặc chưa được vào nhóm
					</td>
				</tr>
                <?php
                }
                foreach($entries_result_sced as $entry) {
                ?>
				<tr>
					<td>
						<?php
                        echo '<strong>Bài Làm Của:</strong><br /> '; echo isset($entry['cache_user_of_fullname'])?$entry['cache_user_of_fullname']:'';
                        if($this->options['show_nickh2d']) {
                        echo '<br /><strong>Nick H2d:</strong><br /> '; 
                        echo '<a target="_blank" href="http://hoctudau.com/h2d/index.php?qa=user&qa_1=';echo isset($entry['cache_user_of_nickh2d'])?$entry['cache_user_of_nickh2d']:''; echo '">';echo isset($entry['cache_user_of_nickh2d'])?$entry['cache_user_of_nickh2d']:'';echo '</a>';
                        }
                        ?>
					</td>
					<td class="notranslate">
						<a title="xem tổng điểm" class="summary" rel="<?php echo $entry['cache_entry_uuid']; ?>" target="_blank" href="<?php echo $siteUrl; ?>summary/<?php echo $entry['cache_entry_uuid'];; ?>"><strong>T: </strong><?php echo $entry['cache_entry_t']; ?> <strong>HS: </strong><span class="<?php echo ($entry['cache_entry_hs']>80)?'markred':'unmark'; ?>"><?php echo $entry['cache_entry_hs']; ?>%</span> | <strong>TG: </strong><span class="<?php echo ($entry['cache_entry_tg']>80)?'markred':'unmark'; ?>"><?php echo $entry['cache_entry_tg']; ?>%</span></a>
					</td>
				</tr>
                <?php
                }
                ?>
			</tbody>
		</table>
        <?php
        if($entries_result_sced_rows==0) $entries_result_sced_rows = 1;
        $total_paged_sced = ceil($entries_result_sced_rows/$form_limited);
        $paged_sced = $pages_sced = $entry_count_sced = 1;
        ?>
        </div>
        <div id="get_sced" class="paged">
            <span class="navHolder">
                <span class="dgNav">
                <a class="firstPage disable" onclick="goToPage(1,this);return false;" href="#" title="First Page">«</a>
                <a class="prevPage disable" onclick="goToPage(1,this);return false;" href="#" title="Previous Page">‹</a>
                <span class="dgInfo">
                    <span class="startEntry var"><?php echo $paged_sced; ?></span> of <span class="totalEntries var"><?php echo $total_paged_sced; ?></span></span>
                <a class="nextPage <?php
                    if(($paged_sced) >= $total_paged_sced) {
                        echo 'disable';
                    } else {
                        echo 'show';
                    }
                ?>" onclick="goToPage(<?php echo (($paged_sced+1) <= $total_paged_sced)?$paged_sced+1:$total_paged_sced; ?>,this);return false;" href="#" rel="<?php echo ($paged_sced+1); ?>" title="Next Page">›</a>
                <a class="lastPage <?php
                    if(($paged_sced) >= $total_paged_sced) {
                        echo 'disable';
                    } else {
                        echo 'show';
                    }
                ?>" onclick="goToPage(<?php echo $total_paged_sced; ?>,this);return false;" href="#" rel="<?php echo ($total_paged_sced); ?>" title="Last Page">»</a>
                </span>
            </span>
        </div>
    </div>
    <div id="nbed_container" class="fullTable hide">
        <div class="contain_nb_sc">
            <div class="sorts">
                <form id="nbed" class="form-actions" action="#">
                    <strong>Sort By: </strong>
                    <a rel="dc" class="<?php if($active_sort_nbed == 'dc') echo 'active'; ?> button" href="#">DC</a>
                    <a rel="t" class="<?php if($active_sort_nbed == 't') echo 'active'; ?> button" href="#">T</a>
                    <a rel="hs" class="<?php if($active_sort_nbed == 'hs') echo 'active'; ?> button" href="#">HS</a>
                    <a rel="tg" class="<?php if($active_sort_nbed == 'tg') echo 'active'; ?> button" href="#">TG</a>
                    <strong>Direction: </strong>
                    <select class="direction" name="direction">
                        <option <?php if($active_dir_nbed == 'asc') echo 'selected="selected"'; ?> value="asc">ASC</option>
                        <option <?php if($active_dir_nbed == 'desc') echo 'selected="selected"'; ?> value="desc">DESC</option>
                    </select>
                    <br /><br />
                    <strong>Class/Group:  </strong>
                    <select class="classmate" name="classmate">
                        <?php
                        foreach($groups_meta_result as $group_meta) {
                        $group_id = $group_meta['group_id'];
                        $group_name = $group_meta['group_name'];
                        ?>
                        <option value="<?php echo $group_id; ?>"><?php echo $group_name; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </form>
            </div>
            <table class="table list_nb_sc" cellspacing="0">
				<caption style="padding-bottom: 7px;">
					<h3>
						Các bài đang cần chấm <?php $nbed_numrows = count($entries_result_nbed);echo '('.$entries_result_nbed_rows.')'; ?>
					</h3>
				</caption>
				<tfoot>
					<tr>
						<td colspan="3">
							
						</td>
					</tr>
				</tfoot>
				<tbody>
                    <?php
                    if($nbed_numrows == 0) {
                    ?>
                    <tr>
						<th>
							&nbsp;
						</th>
						<td class="notranslate">
							Chưa có bài nào cần chấm hoặc chưa được vào nhóm
						</td>
					</tr>
                    <?php
                    }
                    foreach($entries_result_nbed as $entry) {
                    ?>
					<tr>
						<td>
							<?php
                            echo '<strong>Bài Làm Của:</strong><br /> '; echo isset($entry['cache_user_by_fullname'])?$entry['cache_user_by_fullname']:'';
                            if($this->options['show_nickh2d']) {
                            echo '<br /><strong>Nick H2d:</strong><br /> ';
                            echo '<a target="_blank" href="http://hoctudau.com/h2d/index.php?qa=user&qa_1=';echo isset($entry['cache_user_by_nickh2d'])?$entry['cache_user_by_nickh2d']:''; echo '">';echo isset($entry['cache_user_by_nickh2d'])?$entry['cache_user_by_nickh2d']:'';echo '</a>';
                            }
                            ?>
						</td>
						<td class="notranslate">
                            <?php
                            echo '<strong>Form: </strong>'; echo isset($entry['cache_form_nb_name'])?$entry['cache_form_nb_name']:'';
                            ?><br />
							<a target="_blank" href="<?php echo $siteUrl; ?>forms/<?php echo $entry['cache_form_sc_uuid']; ?>/<?php echo $entry['cache_entry_uuid']; ?>">Link Chấm Điểm</a>
						</td>
                        <td class="notranslate">
							<a title="xem tổng điểm" class="summary" rel="<?php echo $entry['cache_entry_uuid']; ?>" target="_blank" href="<?php echo $siteUrl; ?>summary/<?php echo $entry['cache_entry_uuid']; ?>"><strong>T: </strong><?php echo $entry['cache_entry_t']; ?> <strong>HS: </strong><span class="<?php echo ($entry['cache_entry_hs']>80)?'markred':'unmark'; ?>"><?php echo $entry['cache_entry_hs']; ?>%</span> | <strong>TG: </strong><span class="<?php echo ($entry['cache_entry_tg']>80)?'markred':'unmark'; ?>"><?php echo $entry['cache_entry_tg']; ?>%</span></a>
                            
                            <?php 
                            $cache_content = $this->helpers->json_decode_to_array_not_in_mysql($entry['cache_content']);
                            foreach($cache_content['excerpts'] as $exerpt) {
                                echo '<div class="excerpts">'. $exerpt['value'] . '<div class="preview">&nbsp;</div></div>';
                            }
                            ?>
						</td>
					</tr>
                    <?php
                    }
                    ?>
				</tbody>
			</table>
        </div>
        <?php
        if($entries_result_nbed_rows==0) $entries_result_nbed_rows = 1;
        $total_paged_nbed = ceil($entries_result_nbed_rows/$form_limited);
        $paged_nbed = $pages_nbed = $entry_count_nbed = 1;
        ?>
        <div id="get_nbed" class="paged">
            <span class="navHolder">
                <span class="dgNav">
                <a class="firstPage disable" onclick="goToPage(1,this);return false;" href="#" title="First Page">«</a>
                <a class="prevPage disable" onclick="goToPage(1,this);return false;" href="#" title="Previous Page">‹</a>
                <span class="dgInfo">
                    <span class="startEntry var"><?php echo $paged_nbed; ?></span> of <span class="totalEntries var"><?php echo $total_paged_nbed; ?></span></span>
                <a class="nextPage <?php
                    if(($paged_nbed) >= $total_paged_nbed) {
                        echo 'disable';
                    } else {
                        echo 'show';
                    }
                ?>" onclick="goToPage(<?php echo (($paged_nbed+1) <= $total_paged_nbed)?$paged_nbed+1:$total_paged_nbed; ?>,this);return false;" href="#" rel="<?php echo ($paged_nbed+1); ?>" title="Next Page">›</a>
                <a class="lastPage <?php
                    if(($paged_nbed) >= $total_paged_nbed) {
                        echo 'disable';
                    } else {
                        echo 'show';
                    }
                ?>" onclick="goToPage(<?php echo $total_paged_nbed; ?>,this);return false;" href="#" rel="<?php echo ($total_paged_nbed); ?>" title="Last Page">»</a>
                </span>
            </span>
        </div>
    </div>
    </div><!--block-->
</div>
</div>
<!--stage-->