<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<h1 id="logo">
	<a class="notranslate">Hoctudau</a>
</h1>
<header id="header" class="info">
	<div class="buttons" style="width: 250px;">
        <a href="#" onclick="loadAll(); return false;" class="button">All</a>
        <a href="#" onclick="loadNB(); return false;" class="button">Public Form</a>
        <a href="#" onclick="loadAS(); return false;" class="button">Private Form</a>
	</div>
	<h2 class="notranslate">
		Thống Kê Học Sinh Có Điểm Cao Theo Từng Form
	</h2>
	<div class="notranslate">
	</div>
</header>
<div id="stage" class="clearfix">
    <div id="z1" class="zone clearfix" style="padding-bottom: 10px;">
    <div class="wfo_widget fc">
        <table cellspacing="0" class="choices">
			<caption>
				<h4>
				<strong><?php echo $limit; ?> Học Sinh</strong> Có Thành Tích Cao Nhất Public Form
				</h4>
			</caption>
            <thead>
				<tr>
					<th>
						Thông Tin
					</th>
					<th class="percent">
						Tổng Điểm Học Sinh / Thầy Giáo
					</th>
					<th class="count">
						Tổng Bài Nộp
					</th>
				</tr>
			</thead>
            <tbody>
                <?php
                $i=0;
                foreach($best_users_result as $best_user) {
                    if($i>=8) $i=0;
                    $i++;
                    ?>
                    <tr class="color<?php echo $i; ?>">
                        <th>
                            <div>
                            Tên: <a target="_blank" href="../dashboard/index.php?user_id=<?php echo $best_user['cache_user_id']; ?>"><?php echo $best_user['cache_user_fullname']; ?></a>
                            </div>
                            <?php
                            if($this->options['show_nickh2d']) {
                            ?>
                            <div>
                            H2d: <a target="_blank" href="http://hoctudau.com/h2d/index.php?qa=user&qa_1=<?php echo $best_user['cache_user_nickh2d']; ?>"><?php echo $best_user['cache_user_nickh2d']; ?></a>
                            </div>
                            <?php
                            }
                            if($this->options['show_nickskype']) {
                            ?>
                            <div>
                            Skype: <a href="skype:<?php echo $best_user['cache_user_nickskype']; ?>?chat"><?php echo $best_user['cache_user_nickskype']; ?></a>
                            </div>
                            <?php
                            }
                            ?>
                        </th>
                        <td class="percent">
                            <div class="bar_sep">
        						<div class="bar" style="width:<?php echo $best_user['cache_stats_hs']; ?>%">
        						</div>
        						<var>
        							<?php echo $best_user['cache_stats_hs']; ?>%
        						</var>
                            </div>
                            <div class="bar_sep">
                                <div class="admin_bar" style="width:<?php echo $best_user['cache_stats_tg']; ?>%">
        						</div>
        						<var>
        							<?php echo $best_user['cache_stats_tg']; ?>%
        						</var>
                            </div>
                        </td>
                        <td class="count">
                        <?php echo $best_user['cache_stats_t']; ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="wfo_widget fc">
        <table cellspacing="0" class="choices">
			<caption>
				<h4>
				<strong><?php echo $limit; ?> Học Sinh</strong> Có Thành Tích Cao Nhất Private Form
				</h4>
			</caption>
            <thead>
				<tr>
					<th>
						Thông Tin
					</th>
					<th class="percent">
						Tổng Điểm System
					</th>
					<th class="count">
						Tổng Bài Nộp
					</th>
				</tr>
			</thead>
            <tbody>
                <?php
                $i=0;
                foreach($best_users_result_private as $best_user) {
                    if($i>=8) $i=0;
                    $i++;
                    ?>
                    <tr class="color<?php echo $i; ?>">
                        <th>
                            <div>
                            Tên: <a target="_blank" href="../dashboard/index.php?user_id=<?php echo $best_user['cache_user_id']; ?>"><?php echo $best_user['cache_user_fullname']; ?></a>
                            </div>
                            <?php
                            if($this->options['show_nickh2d']) {
                            ?>
                            <div>
                            H2d: <a target="_blank" href="http://hoctudau.com/h2d/index.php?qa=user&qa_1=<?php echo $best_user['cache_user_nickh2d']; ?>"><?php echo $best_user['cache_user_nickh2d']; ?></a>
                            </div>
                            <?php
                            }
                            if($this->options['show_nickskype']) {
                            ?>
                            <div>
                            Skype: <a href="skype:<?php echo $best_user['cache_user_nickskype']; ?>?chat"><?php echo $best_user['cache_user_nickskype']; ?></a>
                            </div>
                            <?php
                            }
                            ?>
                        </th>
                        <td class="percent">
                            <div class="bar_sep">
        						<div class="bar" style="width:<?php echo $best_user['cache_stats_sys']; ?>%">
        						</div>
        						<var>
        							<?php echo $best_user['cache_stats_sys']; ?>%
        						</var>
                            </div>
                        </td>
                        <td class="count">
                        <?php echo $best_user['cache_stats_sys_t']; ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    </div>
	<div id="z2" class="zone clearfix" style="padding-bottom: 10px;">
        <div class="wfo_widget fc">
        <?php
        foreach($form_results as $form) {
        $form_id = $form['form_id'];
        $form_content = $this->helpers->json_decode_to_array_not_in_mysql($form['form_content']);
        if($form_content['Formtype'] == 'nopbai') {
            $form_nb_result = $form['form_nb_result'];
        } elseif($form_content['Formtype'] == 'autoscore') {
            $form_as_result = $form['form_as_result'];
        }
        
        $form_count = $form['form_count'];
        ?>
            <table id="form_<?php echo $form_id; ?>" cellspacing="0" class="choices <?php if($form_content['Formtype'] == 'nopbai') echo 'nb'; if($form_content['Formtype'] == 'autoscore') echo 'as'; ?>">
            <caption>
				<h4>
					<strong>Form: </strong> <?php echo $form_content['Name']; ?>
				</h4>
				<span class="fcNav">
					<a>&nbsp;</a>
				</span>
			</caption>
			<thead>
				<tr>
					<th>
						Thông Tin
					</th>
					<th class="percent">
						Điểm Học Sinh / Thầy Giáo
					</th>
					<th class="count">
						Số Người Chấm
					</th>
				</tr>
			</thead>
            <tfoot>
                <tr>
                    <th colspan="2" class="count">Tổng Kết Form Này Có</th>
                    <th class="count">Số Lượng</th>
                </tr>
                <?php
                    $p80 = $p50_80 = $p_50 = 0;
                    if($form_content['Formtype'] == 'nopbai') {
                        foreach($form_nb_result as $form_nb) {
                            if($form_nb['cache_entry_hs'] >=80) {
                                $p80++;
                            } elseif($form_nb['cache_entry_hs'] < 80 && $form_nb['cache_entry_hs'] >= 50) {
                                $p50_80++;
                            } elseif($form_nb['cache_entry_hs'] < 50) {
                                $p_50++;
                            }
                        }
                    } elseif($form_content['Formtype'] == 'autoscore') {
                        foreach($form_as_result as $form_as) {
                            if($form_as['cache_entry_sys'] >=80) {
                                $p80++;
                            } elseif($form_as['cache_entry_sys'] < 80 && $form_as['cache_entry_sys'] >= 50) {
                                $p50_80++;
                            } elseif($form_as['cache_entry_sys'] < 50) {
                                $p_50++;
                            }
                        };
                    }
                    ?>
                <tr class="color1">
                    <th>
                        Số Điểm &gt;= 80%
                        
                    </th>
                    <td class="percent">
                        <div class="bar_sep">
    						<div class="bar" style="width:<?php echo number_format($p80/$form_count*100,1); ?>%">
    						</div>
    						<var>
    							<?php echo number_format($p80/$form_count*100,1); ?>%
    						</var>
                        </div>
                    </td>
                    <td class="count"><?php echo $p80; ?></td>
                </tr>
                <tr class="color3">
                    <th>
                        Số Điểm &gt;= 50% &amp; &lt; 80%
                        
                    </th>
                    <td class="percent">
                        <div class="bar_sep">
    						<div class="bar" style="width:<?php echo number_format($p50_80/$form_count*100,1); ?>%">
    						</div>
    						<var>
    							<?php echo number_format($p50_80/$form_count*100,1); ?>%
    						</var>
                        </div>
                    </td>
                    <td class="count"><?php echo $p50_80; ?></td>
                </tr>
                <tr class="color2">
                    <th>
                        Số Điểm &lt; 50%
                        
                    </th>
                    <td class="percent">
                        <div class="bar_sep">
    						<div class="bar" style="width:<?php echo number_format($p_50/$form_count*100,1); ?>%">
    						</div>
    						<var>
    							<?php echo number_format($p_50/$form_count*100,1); ?>%
    						</var>
                        </div>
                    </td>
                    <td class="count"><?php echo $p_50; ?></td>
                </tr>
            </tfoot>
            <tbody>
                <?php
                if($form_content['Formtype'] == 'nopbai') {
                    $i=0;
                    foreach($form_nb_result as $form_nb) {
                        if($i>=$limit) break;
                        if($i>=8) $i=0;
                        $i++;
                        ?>
                        <tr class="color<?php echo $i; ?>">
                            <th>
        						<div>
        							Tên: <a href="<?php echo $siteUrl; ?>dashboard/&user_id=<?php echo $form_nb['cache_user_id']; ?>"><?php echo $form_nb['cache_user_fullname']; ?></a>
        						</div>
                                <?php
                                if($this->options['show_nickh2d']) {
                                ?>
                                <div>
        							H2D: <a href="http://hoctudau.com/h2d/index.php?qa=user&qa_1=<?php echo $form_nb['cache_user_nickh2d']; ?>"><?php echo $form_nb['cache_user_nickh2d']; ?></a>
        						</div>
                                <?php
                                }
                                if($this->options['show_nickskype']) {
                                ?>
                                <div>
        							Skype: <a href="skype:<?php echo $form_nb['cache_user_nickskype']; ?>?chat"><?php echo $form_nb['cache_user_nickskype']; ?></a>
        						</div>
                                <?php
                                }
                                ?>
        					</th>
        					<td class="percent">
                                <div class="bar_sep">
            						<div class="bar" style="width:<?php echo $form_nb['cache_entry_hs']; ?>%">
            						</div>
            						<var>
            							<?php echo $form_nb['cache_entry_hs']; ?>%
            						</var>
                                </div>
                                <div class="bar_sep">
                                    <div class="admin_bar" style="width:<?php echo $form_nb['cache_entry_tg']; ?>%">
            						</div>
            						<var>
            							<?php echo $form_nb['cache_entry_tg']; ?>%
            						</var>
                                </div>
        					</td>
                            <td class="count">
                            <?php echo $form_nb['cache_entry_t']; ?>
        					</td>
                        </tr>
                        <?php
                    }
                } elseif($form_content['Formtype'] == 'autoscore') {
                    $i=0;
                    foreach($form_as_result as $form_as) {
                        if($i>=$limit) break;
                        if($i>=8) $i=0;
    
                        $i++;
                        ?>
                        <tr class="color<?php echo $i; ?>">
                            <th>
        						<div>
        							Tên: <a href="<?php echo $siteUrl; ?>dashboard/&user_id=<?php echo $form_as['cache_user_id']; ?>"><?php echo $form_as['cache_user_fullname']; ?></a>
        						</div>
                                <?php
                                if($this->options['show_nickh2d']) {
                                ?>
                                <div>
        							H2D: <a href="http://hoctudau.com/h2d/index.php?qa=user&qa_1=<?php echo $form_as['cache_user_nickh2d']; ?>"><?php echo $form_as['cache_user_nickh2d']; ?></a>
        						</div>
                                <?php
                                }
                                if($this->options['show_nickskype']) {
                                ?>
                                <div>
        							Skype: <a href="skype:<?php echo $form_as['cache_user_nickskype']; ?>?chat"><?php echo $form_as['cache_user_nickskype']; ?></a>
        						</div>
                                <?php
                                }
                                ?>
        					</th>
        					<td class="percent">
                                <div class="bar_sep">
            						<div class="bar" style="width:<?php echo $form_as['cache_entry_sys']; ?>%">
            						</div>
            						<var>
            							<?php echo $form_as['cache_entry_sys']; ?>%
            						</var>
                                </div>
        					</td>
                            <td class="count">
                            <?php echo $form_as['cache_entry_sys_t']; ?>
        					</td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
            </table>
        <?php
        }
        ?>
        </div>
	</div>
    <div id="z3" class="zone clearfix">
	</div>
	<footer class="footer">
		<h4 class="notranslate">
			Hoctudau.com
		</h4>
		<span>
			<?php echo date('Y-m-d'); ?>
		</span>
	</footer>
	<!--footer-->
</div>
<!--stage-->