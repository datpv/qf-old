<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<div id="stage">
<div id="main" class="dg">
	<div class="info">
		<h2>
			Accounts Mananger (Total <strong><?php echo $user_count_total; ?></strong> Accounts.)
		</h2>
	</div>
    <form id="" name="" class="search" onsubmit="return false;">
		<label>
			Filter (Activated (A:) || Disabled (D:)): 
		</label>
		<input id="searchBox" class="text" type="search" value="" />
	</form>
    <div class="col2" style="float: left; width: 100%;">
        <h3 class="hide" id="searching">
			Filtered Results for :
			<span id="searchTerm">
			</span>
		</h3>
        <h3 class="hide" id="noResults">
			None of your users match your filter.
		</h3>
        <ul class="account_header clearfix">
            <li>
                <div class="col col-1"><strong>Account Infomations</strong></div>
                <div class="col col-2"><strong>Subject Unlock Requests</strong></div>
                <div class="col col-3"><strong>Thông tin khác</strong></div>
                <div class="col col-4"><strong>Groups</strong></div>
                <div class="col col-5"><strong>&nbsp;</strong></div>
            </li>
        </ul>
        <ul id="groupList-1" class="account_list">
	<?php
    $i=0;
    foreach($users as $user) {
        $i++;
        $user_id = $user['user_id'];
        $username = $user['username'];
        $email = $user['user_email'];
        $user_content = $this->helpers->json_decode_to_array($user['user_content']);
        $user_status = $user['user_status'];
        $su_results = isset($user['su_results'])?$user['su_results']:array();
    ?>
        <li class="<?php echo ($i%2==0)?'even':''; ?> user_info" id="user_<?php echo $user_id; ?>">
            <div class="col col-1">
                <div>
                    <strong>Account:</strong> <a id="link<?php echo $user_id; ?>" class="view account_name" target="_blank" href="<?php echo $siteUrl ?>dashboard/&user_id=<?php echo $user_id; ?>"><?php echo $username; ?></a>
                </div>
                <div>
                    <strong>Email:</strong> <?php echo $email; ?>
                </div>
                <div>
                    <strong>Tên:</strong> <?php echo $user_content['Name']; ?>
                </div>
                <?php
                if($this->options['show_nickh2d']) {
                ?>
                <div>
                    <strong>Nick H2d:</strong> <a target="_blank" href="http://hoctudau.com/h2d/index.php?qa=user&qa_1=<?php echo $user_content['NickH2d']; ?>"><?php echo $user_content['NickH2d']; ?></a>
                </div>
                <?php
                }
                if($this->options['show_nickskype']) {
                ?>
                <div>
                    <strong>Nick Skype:</strong> <a href="skype:<?php echo $user_content['NickSkype']; ?>?chat"><?php echo $user_content['NickSkype']; ?></a>
                </div>
                <?php
                }
                ?>
            </div>
            <div class="col col-2">
                <?php
                foreach($su_results as $su_result) {
                ?>
                <p>
                    <a id="s_<?php echo $su_result['subject_id']; ?>" title="<?php echo $su_result['subject_name']; ?>" class="subject_title" href="#"><?php echo $su_result['subject_name']; ?></a>
                    <?php
                    if($su_result['subject_unlocked'] == 'N') {
                    ?>
                    <a href="#" title="Click to unlock." class="button unlock" onclick="unlockSubject(this); return false;">UNLOCK</a>
                    <?php
                    } else {
                    ?>
                    <a href="#" title="Click to lock." class="button lock" onclick="lockSubject(this); return false;">LOCKED</a>
                    <?php
                    }
                    ?>
                </p>
                <?php
                }
                ?>
                &nbsp;
            </div>
            <div class="col col-3">
                &nbsp;
            </div>
            <div class="col col-4">
                <div class="group">
                    <form action="#">
                    <ul>
                        <?php
                        $dupl_group_id = array();
                        if($user_grouped) foreach($user_grouped as $user_group) {
                            if($user_group['user_id'] == $user_id) {
                                $dupl_group_id[] = $user_group['group_id'];
                                ?>
                                <li id="group_<?php echo $user_group['group_id']; ?>">
                                <?php
                                    echo '<strong>'.$user_group['group_name'].'</strong>';
                                ?>
                                
                                <select class="groupname" name="groupname">
                                    <?php
                                    foreach($groups_result as $group) {
                                    if(in_array($group['group_id'],$dupl_group_id)) continue;
                                    
                                    ?>
                                    <option value="<?php echo $group['group_id']; ?>"><?php echo $group['group_name']; ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                    <br /><br />
                                    <?php
                                    
                                    ?>
                                <a style="color: #221989;" onclick="changeGroup(this); return false;" class="addgroup button" href="#">Change Group</a>&nbsp;&nbsp;
                                <a style="color: #FF2A2A;" onclick="deleteGroup(this); return false;" class="button" href="#">Delete Group</a>
                                <?php
                                ?>
                                
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                    <div>
                    <select>
                        <?php
                        foreach($groups_result as $group) {
                            if(in_array($group['group_id'],$dupl_group_id)) continue;
                            ?>
                            <option value="<?php echo $group['group_id']; ?>"><?php echo $group['group_name']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <a onclick="addGroup(this); return false;" class="addgroup button" href="#">Add Group</a>
                    </div>
                    </form>
                </div>
            </div>
            <div class="col col-5">
                <strong><a class="button" rel="<?php echo $user_id; ?>" onclick="deleteAccount(this); return false;" href="#" style="color: red;">Delete</a></strong><br /><br />
                <?php if($user_status == 'A'): ?><a class="button" rel="<?php echo $user_id; ?>" onclick="disableAccount(this); return false;" style="color: green;" href="#">Activated</a><?php else: ?><a class="button" href="#" rel="<?php echo $user_id; ?>" onclick="activeAccount(this); return false;" style="color: red;">Disabled</a><?php endif; ?>
            </div>
        </li>
    <?php
    }
    ?>
        </tbody>
        </table>
    </div>
    <?php
    ?>
    <div class="paged">
        <span id="navHolder">
                <span class="dgNav">
                <a class="firstPage <?php
                    if($paged <= 1) {
                        echo 'disable';
                    } else {
                        echo 'show';
                    }
                ?>" href="<?php echo $siteUrl . 'accounts/&paged=1'; ?>" title="First Page">«</a>
                <a class="prevPage <?php
                    if($paged <= 1) {
                        echo 'disable';
                    } else {
                        echo 'show';
                    }
                ?>" href="<?php echo $siteUrl . 'accounts/&paged='.($paged-1); ?>" title="Previous Page">‹</a>
                <span class="dgInfo">
                    <var class="startEntry"><?php echo $paged; ?></var>-<var class="endEntry"><?php echo $user_count; ?></var> of <var class="totalEntries"><?php echo $total_paged; ?></var></span>
                <a class="nextPage <?php
                    if(($paged) >= $total_paged) {
                        echo 'disable';
                    } else {
                        echo 'show';
                    }
                ?>" href="<?php echo $siteUrl . 'accounts/&paged='.($paged+1); ?>" title="Next Page">›</a>
                <a class="lastPage <?php
                    if(($paged) >= $total_paged) {
                        echo 'disable';
                    } else {
                        echo 'show';
                    }
                ?>" href="<?php echo $siteUrl . 'accounts/&paged='.($total_paged); ?>" title="Last Page">»</a>
                </span>
            </span>
    </div>
</div>

</div>
<!--stage-->