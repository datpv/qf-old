<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<div id="stage">
<div id="main" class="dg">

	<div class="info">
		<div class="buttons">
		</div>
		<h2>
			Groups Manangers
		</h2>
	</div>
    <div class="subdomain clearfix">
			<p class="plan1 notranslate">
            Total: <strong><?php echo count($groups_result); ?></strong> Groups
			</p>
	</div>
  
  <div class="block">
  <h4>Create New Group</h4>
  <form id="addgroup" action="#">
    <input type="text" name="group_name" id="group_name" value="" />
    <a id="create_group" onclick="addGroup(); return false;" class="button">Create</a>
  </form>
  </div>
    <div class="block">
        <div class="col3">
            <table class="table">
                <thead>
                    <th>
                        Group ID
                    </th>
                    <th>
                        Group Name
                    </th>
                    <th>
                        User count
                    </th>
                    <th>&nbsp;</th>
                </thead>
                <tbody>
                <?php 
                if($groups_result) foreach($groups_result as $group) {
                    $group_id = $group['group_id'];
                    $user_group_meta = $group['user_group_meta'];
                ?>
                    <tr id="group_<?php echo $group_id; ?>">
                        <td><?php echo $group_id; ?></td>
                        <td><input class="group_name" value="<?php echo $group['group_name']; ?>" /></td>
                        <td class="<?php echo (count($user_group_meta)>=5)?'full_user':'unfull_user'; ?>"><?php 
                        
                        echo '<strong>'.count($user_group_meta).'</strong> Users'; 
                        
                        ?></td>
                        <td><a href="#" class="button" onclick="updateGroup(this); return false;">Update</a></td>
                        
                    </tr>
                <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
<!--stage-->