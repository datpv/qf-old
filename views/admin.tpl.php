<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<div id="stage">
	<div class="info">
		<div class="buttons">
			<a class="button positive" href="<?php echo $siteUrl; ?>build/" title="Create a New Form">
			<img src="template/images/icons/application_form_add.png" alt=""/> New Form Normal!</a>
            <a class="button positive" href="<?php echo $siteUrl; ?>build&formType=1" title="Create a New Form">
			<img src="template/images/icons/application_form_add.png" alt=""/> New Form Score!</a>
            <a class="button positive" href="<?php echo $siteUrl; ?>build&formType=3" title="Create a New Form">
			<img src="template/images/icons/application_form_add.png" alt=""/> New Form Nopbai!</a>
            <a class="button positive" href="<?php echo $siteUrl; ?>build&formType=4" title="Create a New Form">
			<img src="template/images/icons/application_form_add.png" alt=""/> New Form AutoScore!</a>
		</div>
		<!--buttons-->
		<h2>
			Form Manager
		</h2>
		<div class="notranslate">
			O, what men dare do!
		</div>
	</div>
	<!--info-->
    <div id="main" class="forms">
    	<form id="" name="" class="search" onsubmit="return false;">
    		<label>
    			Filter
    		</label>
    		<input onkeyup="filterBy($F(this));" onsearch="filterBy($F(this));" id="searchBox" class="text" type="search" value="" />
    	</form>
        <div class="abuttons">
            <label>
    			Form Classify :
    		</label>
            <a rel="y" onclick="getApprovedForm(this); return false;" class="approveButton selected">Approved Forms</a> 
            <a rel="n" onclick="getUnApprovedForm(this); return false;" class="approveButton">UnApproved Forms</a>
            </div>
    	<div id="sort">
    		<label>
    			Sort By :
    		</label>
    		<a id="FormId" class="selected" href="#" onclick="sortBy('FormId'); return false;">Date Created</a>
    		<a id="DateUpdated" class="" href="#" onclick="sortBy('DateUpdated'); return false;">Date Edited</a>
    		<!--<a class="undone" id="EntryCount" href="#" onclick="sortBy('EntryCount'); return false;">Entries Today</a>
    	--></div>
    	<div id="stats" style="display:none">
    		<table id="graphs" cellspacing="0">
    			<tr>
    				<td>
    					<select id="allForms" name="allForms" class="select" onchange="refreshGraph();">
    						<option value="4" class="notranslate">
    							Untitled Form
    						</option>
    					</select>
    				</td>
    				<td>
    					<img src="template/images/paren.gif" alt=""/>
    				</td>
    				<td>
    					<div id="formGraph" class="graph">
    						Loading...
    					</div>
    				</td>
    				<td>
    					<ul id="graphMenu">
    						<li id="todayGraph">
    							<a href="#" onclick="changeType('today'); return false;">Today</a>
    						</li>
    						<li class="selected" id="weekGraph">
    							<a href="#" onclick="changeType('week'); return false;">Week</a>
    						</li>
    						<li id="monthGraph">
    							<a href="#" onclick="changeType('month'); return false;">Month</a>
    						</li>
    						<li id="yearGraph">
    							<a href="#" onclick="changeType('year'); return false;">Year</a>
    						</li>
    					</ul>
    				</td>
    			</tr>
    		</table>
    	</div>
    	<div class="group">
    		<h3 id="searching" class="hide">
    			Filtered Results for :
    			<span id="searchTerm">
    			</span>
    		</h3>
    		<h3 id="noResults" class="hide">
    			None of your forms match your filter.
    		</h3>
            
    		<ul id="groupList-1">
            <?php
            if($count != 0) {
                $i=0;
                if($count > 0) {
                foreach($forms_result as $form) {
                    $form_id = $form['form_id'];
                    $entty_count = $form['entry_count'];
                    $theme_id = $form['theme_id'];
                    $entty_count = $entty_count - $form['form_flag'];
                    if(empty($form['form_content'])) continue;
                    $_form = $this->helpers->json_decode_to_array($form['form_content']);
                    $user_c = $this->helpers->json_decode_to_array_not_in_mysql($form['user_by']['user_content']);
                ?>
    			<li id="li<?php echo $form['form_id']; ?>" class="<?php echo $form['form_type'].' '; echo ($i==0)?'first':'';$i++; echo ($form['form_status'] == 'D')?' notActive':''; ?> approvedForm" onmouseover="showActions(this)" onmouseout="hideActions(this)">
    				<h4>
                        <span class="by_user">
                        <a title="<?php echo !empty($user_c['NickH2d'])?$user_c['NickH2d']:$user_c['Name']; ?>" href="<?php echo $siteUrl; ?>dashboard/&user_id=<?php echo $form['user_by']['user_id']; ?>" target="_blank"><?php echo $this->helpers->truncate(!empty($user_c['NickH2d'])?$user_c['NickH2d']:$user_c['Name'],6,' ',3); ?></a>
                        </span>
    					<a title="<?php echo $_form['Name']; ?>" id="link<?php echo $form['form_id']; ?>" href="<?php echo $siteUrl; ?>entries/<?php echo $form['form_uuid']; ?>">
    					<span class="notranslate"> <?php echo $this->helpers->truncate($_form['Name'],46); ?> </span>
                        <?php if($entty_count>0): ?>
                        <b title="Hooray!"><span class="notranslate"><?php echo $entty_count; ?></span> New Entries.</b>
                        <?php endif; ?>
                        
    					</a>
    				</h4>
                    <span title="Add Categories" onclick="addFormCategories(<?php echo $form['form_id']; ?>); return false;" class="category_select">
                    &nbsp;
                    </span>
    				<span class="themeSelect">
    					<select onchange="changeTheme(<?php echo $form['form_id']; ?>, 'Untitled%20Form', this)">
                            <?php echo $this->getOptions($theme_result, $theme_id, 'theme_id'); ?>
    						<option value="create">
    							-----------
    						</option>
    						<option value="create">
    							New Theme!
    						</option>
    					</select>
    				</span>
    				<span class="activeCheck">
    					<label for="publicStatus_<?php echo $form['form_id']; ?>">
    						Public
    					</label>
    					<input type="checkbox" id="publicStatus_<?php echo $form['form_id']; ?>" onclick="togglePublicForm(<?php echo $form['form_id']; ?>, 'Untitled%20Form', this)" <?php echo ($form['form_status'] == 'A')?'checked="checked"':''; ?> />
    				</span>
    				<div id="expandThis">
    					<div class="actions">
                            <a class="approve" href="#" onclick="approveForm(this); return false;">Approve</a>
                            <a class="unapprove" href="#" onclick="unApproveForm(this); return false;">UnApprove</a>
    						<a class="del" href="#" onclick="deleteForm(this); return false;">Delete</a>
    						<a class="dup" href="#" onclick="duplicateForm(this); return false;">Duplicate</a>
    						<a class="entries" href="#" onclick="viewEntries(this); return false;">Entries</a>
    						<a class="edit" href="#" onclick="editForm(this); return false;">Edit</a>
    						<a class="view" href="<?php echo $siteUrl; ?>forms/<?php echo $form['form_uuid'] ?>" target="_blank">View</a>
                            <a class="view" href="<?php echo $siteUrl; ?>cwa/<?php echo $form['form_uuid'] ?>" target="_blank">CWA</a>
    						<!--<a class="undone stats" href="#" onclick="viewAnalytics(this); return false;">Analytics</a>
    						--><a class="subscribe" href="#" onclick="viewNotifications(this); return false;">Notifications</a>
    						<a class="rules" href="#" onclick="viewRules(this); return false;">Rules</a>
    						<!--<a class="undone payment" href="#" onclick="viewPayment(this); return false;">Payment</a>
    						--><span>
    							<!--<a class="undone code" href="#" onclick="viewCode(this); return false;">Code</a>
    							--><a class="protect" href="#" onclick="showProtectForm(this);return false;">Protect</a>
    						</span>
                            <span>
    							<!--<a class="undone code" href="#" onclick="viewCode(this); return false;">Code</a>
    							--><a target="_blank" class="protect" href="<?php echo $siteUrl; ?>report/auto_builder/<?php echo $form['form_uuid'] ?>">Create Report</a>
    						</span>
    						<span id="paidPlanWarningDiv" class="hide">
    							This feature is only available to our paid plans.
    						</span>
    					</div>
    				</div>
    			</li>
                <?php
                }
                }
            } else {
            ?>
                <li class="notice bigMessage">
                	<h2>
                		<a href="<?php echo $siteUrl; ?>build/"><span class="bigMessageRed">You don't have any forms!</span> <span class="bigMessageGreen">On This Category!</span></a>
                	</h2>
                </li>
            <?php
            }
            ?>
    		</ul>
            <div class="pageinate">
                <span class="navHolder">
                    <span class="dgNav">
                    <a class="firstPage disable" onclick="goToPage(1,this);return false;" href="#" title="First Page">«</a>
                    <a class="prevPage disable" onclick="goToPage(1,this);return false;" href="#" title="Previous Page">‹</a>
                    <span class="dgInfo">
                        <span class="startEntry var"><?php echo $paged; ?></span> of <span class="totalEntries var"><?php echo $total_paged; ?></span></span>
                    <a class="nextPage <?php
                        if(($paged) >= $total_paged) {
                            echo 'disable';
                        } else {
                            echo 'show';
                        }
                    ?>" onclick="goToPage(<?php echo (($paged+1) <= $total_paged)?$paged+1:$total_paged; ?>,this);return false;" href="#" rel="<?php echo ($paged+1); ?>" title="Next Page">›</a>
                    <a class="lastPage <?php
                        if(($paged) >= $total_paged) {
                            echo 'disable';
                        } else {
                            echo 'show';
                        }
                    ?>" onclick="goToPage(<?php echo $total_paged; ?>,this);return false;" href="#" rel="<?php echo ($total_paged); ?>" title="Last Page">»</a>
                    </span>
                </span>
            </div>
    	</div>
        <div class="categories classify">
            <h3>Categories</h3>
            <ul id="category_list">
                <li id="cat_0" <?php if($cate_id == '0') echo 'class="active"'; ?>><a href="#">All</a></li>
                <?php
                if($categories) foreach($categories as $category) {
                ?>
                <li id="cat_<?php echo $category['category_id']; ?>" <?php if($cate_id == $category['category_id']) echo 'class="active"'; ?>>
                <a href="#"><?php echo $category['category_name']; ?></a>
                <span class="setting">&nbsp;</span>
                <div class="hide setting_content">
                    <span onclick="category_rename(<?php echo $category['category_id']; ?>); return false;">Rename</span>
                    <span onclick="category_delete(<?php echo $category['category_id']; ?>); return false;">Delete</span>
                </div>
                </li>
                <?php
                }
                ?>
            </ul>
            <div class="forms">
                <form id="category_create">
                    <div>
                        <label for="cat_name">Name:</label> <input type="text" class="text" id="cat_name" />
                    </div>
                </form>
            </div>
        </div>
        <div class="clearfix"></div>
    	<!--group-->
    </div>
    <!--main-->
    <div class="forms">
        <div class="subjects classify">
            <h3>Subjects</h3>
            <ul>
                <li><a>á</a></li>
            </ul>
            <div class="forms">
                <form id="subject_create">
                    <div>
                        <label for="subject_name">Name:</label> <input type="text" class="text" id="subject_name" />
                    </div>
                </form>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="footer clear">
    	You have
    	<strong class="notranslate">
    		1000000000000
    	</strong>
    	forms left.
    	<a class="button" href="<?php echo $siteUrl; ?>account/"><img src="template/images/icons/tinyupgrade.png" /> Degrade for Less</a>
    </div>
    <?php 
    
    ?>
</div>
<!--stage-->