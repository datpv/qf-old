<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<!DOCTYPE html>
<html class=" notranslate">
	<head>
		<title>
			Your Result
		</title>
		<!-- Meta Tags -->
		<meta charset="utf-8" />
		<meta name="robots" content="index, nofollow, noarchive" />
		<!-- CSS -->
        <link href="template/stylesheets/base.css" rel="stylesheet" />
		<link href="template/stylesheets/public/reports/css/index.740.css" rel="stylesheet" />
        <link href="template/stylesheets/global/css/index.991.css" rel="stylesheet" />
		<!-- JavaScript -->
		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js">
			</script>
		<![endif]-->
	</head>
	<body id="report" class="hoctudau layout1">
		<div id="container">
			<h1 id="logo">
				<a class="notranslate">Hoctudau</a>
			</h1>
			<header id="header" class="info">
				<div class="buttons">
				</div>
                <h2 class="notranslate">
                    <?php
                    if(isset($entry_uuid)) {
                    ?>
					Copy Link Ở Dưới Để Đưa Cho Thầy Giáo
                    <?php
                    } else {
                    ?>
                    Kết Quả Được Chia Sẻ
                    <?php
                    }
                    ?>
				</h2>
				<div class="notranslate">
                    <?php
                    $siteUrl = $this->helpers->getUrl();
                    if(isset($entry_uuid)) {
                    ?>
                    <input onclick="this.select()" style="height: 25px; font-size: 20px; line-height: 25px; width: 100%;" type="text" value="<?php echo $siteUrl; ?>review/<?php echo $entry_uuid; ?>" />
                    <?php
                    }
                    ?>
					
				</div>
			</header>
			<div id="stage" class="clearfix">
				<div id="scores" class="clearfix">
                    <?php echo isset($display_)?$display_:''; ?>
                </div>
                <?php
                $routers = $this->registry->router->getRouters();
                if($routers['controller'] == 'review') {
                ?>
                
                <div id="comments" class="clearfix">
                    <h3 class="subject">Sai giống bạn và các gợi ý 
                    <?php
                    if($this->helpers->isAdmin()) {
                    ?>
                    <a target="_blank" href="<?php echo $siteUrl; ?>cwa/<?php echo $form_uuid; ?>">Xem tổng thể</a>
                    <?php
                    }
                    ?>
                    </h3>
                    <div>&nbsp;</div>
                    <div class="suggestions">
                        <ul class="parent">
                            <?php
                            
                            foreach($logs as $key => $log) {
                                $log_content = array();
                                if(isset($log['log_content2'])) {
                                    $log_content = $log['log_content2'];
                                } else {
                                    $log_content = $this->helpers->json_decode_to_array($log['log_content']);
                                }
                            ?>
                            <li>
                                <h4><?php echo $log_content['Title']; ?></h4>
                                <ul class="child">
                                <?php
                                if(is_string($log_content['Value'])) {
                                    $log_value = $log_content['Value'];
                                    if($log_content['type'] == 'checkbox') {
                                        $field = $log_content['field'];
                                        $field_ids = explode('-',$log_value);
                                        $log_values = array();
                                        foreach($field['SubFields'] as $subF) {
                                            if(in_array($subF['ColumnId'],$field_ids)) {
                                                $log_values[] = $subF['ChoicesText'];
                                            }
                                        }
                                        $log_value = implode(' | ', $log_values);
                                    } elseif($log_content['type'] == 'radio' || $log_content['type'] == 'select') {
                                        $field = $log_content['field'];
                                        $log_value_id = explode('_',$log_value);
                                        foreach($field['Choices'] as $ck => $choiC) {
                                            if($log_value_id[1] == $ck) {
                                                $log_value = $choiC['Choice'];
                                                break;
                                            }
                                        }
                                    }
                                ?>
                                <li id="log_<?php echo $log['log_id']; ?>">
                                    <header class="heading"><span class="value"><?php echo $log_value; ?></span> ( <?php echo $log['log_hit']; ?> Hits ) <a href="#" class="add_comment">Add comments</a></header>
                                    <div class="comments">
                                        <ul>
                                            <?php
                                            if($log['comments']) foreach($log['comments'] as $comment) {
                                                $user = $this->helpers->json_decode_to_array_not_in_mysql($comment['user']['user_content']);
                                            ?>
                                            <li id="comment_<?php echo $comment['comment_id']; ?>">
                                                <div class="avatar" href="#">
                                                    <a class="image" target="_blank" href="https://vi.gravatar.com/site/signup/" title="Change Avatar">
                                                        <img width="50" src="<?php echo $this->helpers->get_gravatar($comment['user']['user_email']); ?>" />
                                                    </a>
                                                    <div class="info">
                                                        <?php
                                                        if($this->options['show_nickh2d']) {
                                                        ?>
                                                        <span class="user_h2d"><?php echo $user['NickH2d']; ?></span><br />
                                                        <?php
                                                        }
                                                        ?>
                                                        <span class="user_fullname"><?php echo $user['Name']; ?></span>
                                                    </div>
                                                    
                                                </div>
                                                <div class="comment_content">
                                                    <?php
                                                    if($this->helpers->isAdmin()) {
                                                    ?>
                                                    <div class="actions"><a class="delete" onclick="deleteComment(this); return false;" href="#">Delete</a></div>
                                                    <?php
                                                    }
                                                    ?>
                                                    <p><?php echo nl2br($comment['comment_content']); ?></p>
                                                </div>
                                            </li>
                                            <?php
                                            }
                                            ?>
                                        </ul>
                                        <?php
                                        if($log['total_comments'] > 3) {
                                        ?>
                                        <div class="view_comments"><a onclick="loadComments(this); return false;" href="#">View More <b>(<?php echo $log['total_comments'] - 3; ?>)</b></a></div>
                                        <?php
                                        }
                                        if($key == 0) {
                                        ?>
                                        <div class="comment_wrap">
                                            <form id="comment_form">
                                                <textarea id="comment_text" name="comment_text"></textarea>
                                                <button onclick="addComment(this); return false;" id="submitFormButton" name="submitFormButton">Comment</button>
                                            </form>
                                        </div>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </li>
                                <?php
                                }
                                ?>
                                </ul>
                            </li>
                            <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <?php   
                } else {
                ?>
                <h2>Qua <a target="_blank" href="<?php echo $siteUrl; ?>review/<?php echo $entry_uuid; ?>">đây</a> để bình luận</h2>
                <?php
                }
                ?>
			</div>
            <div class="hide" id="comment_form_container">
                <div class="comment_wrap">
                    <form id="comment_form">
                        <textarea id="comment_text" name="comment_text"></textarea>
                        <button onclick="addComment(this); return false;" id="submitFormButton" name="submitFormButton">Comment</button>
                    </form>
                </div>
                <div class="wronga_wrap"></div>
            </div>
			<!--stage-->
		</div>
        <div id="status">
            <div id="y" style="left: 0px; top: 0px; display: none;">
                <div id="statusText">Loading Data ...</div>
            </div>
        </div>
        <script>
        __LOGS = {<?php 
            $i = 0;
            $count = count($logs);
            foreach($logs as $log) {
                $log_comments = array();
                if(isset($log['comments']) && !empty($log['comments'])) foreach($log['comments'] as $lcs) {
                    $user = $this->helpers->json_decode_to_array_not_in_mysql($lcs['user']['user_content']);
                    $log_comments[] = array(
                        'comment_id' => $lcs['comment_id'],
                        'comment_text' => $lcs['comment_content'],
                        'comment_user' => $user['NickH2d']
                    );
                }
                ?>"<?php echo $log['log_id']; ?>":{"log_id":"<?php echo $log['log_id']; ?>","log_value":<?php echo json_encode($log['log_subject']); ?>,"log_comments":<?php echo json_encode($log_comments); ?>}<?php if($i<$count-1) echo ','; $i++;
            }
        ?>};
        </script>
        <script src="template/scripts/global/dynamic-1-7.724.js"></script>
        <script src="template/scripts/public/reviewer.js"></script>
	</body>

</html>