<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<h1 id="logo">
	<a class="notranslate">Hoctudau</a>
</h1>
<header id="header" class="info">
	<div class="buttons">
	</div>
	<h2 class="notranslate">
		Điểm của: <?php echo $entry['cache_user_fullname']; ?><br />
        Nick H2d: <?php echo $entry['cache_user_nickh2d']; ?>
	</h2>
	<div class="notranslate">
        <?php
        if($entry_type == 'AS') {
            echo 'Form chấm tự động, nếu có thắc mắc gì xin liên hệ Thầy Giáo.';
        } else {
        ?>
        Có tổng cộng <strong><?php echo $entry['cache_entry_t']; ?> </strong> người đã chấm.
        <?php
        }
        ?>
	</div>
</header>
<div id="stage" class="clearfix">
    <div class="clearfix" style="padding-bottom: 10px;">
        <?php echo $this->helpers->json_decode_to_array_not_in_mysql($entry['cache_form_nb_display_result']); ?>
    </div>
    <?php
    if($entry_type == 'AS') {
    ?>
    <div id="z1" class="zone clearfix" style="padding-bottom: 10px;">
        <h3><strong>Hệ Thống</strong> chấm bài này</h3>
		<?php echo 'Total Point: ' . $scoring['scoring']['sys_total']; ?><br />
        <?php echo 'Right Point: ' . $scoring['scoring']['sys_point']; ?><br />
        <div style="margin-top: 10px;">
        <table cellspacing="0" class="choices" style="width: 100%;">
        <tbody>
            <tr class="fc">
                <td class="percent">
					<div style="height: 25px; width:100.00%;  background: #CF3B19;" class="bar">
                        <div style="height: 25px; line-height: 25px;width:<?php echo ($entry['cache_entry_sys']); ?>%; text-align: center;  background: #499CC9;color: #ffffff;" class="bar"><strong><?php echo number_format($entry['cache_entry_sys'],1) . '%' ?></strong></div>
					</div>
				</td>
        	</tr>
            
        </tbody>
        </table>
        </div>
	</div>
    <?php
    } else {
    ?>
	<div id="z1" class="zone clearfix" style="padding-bottom: 10px;">
        <h3><strong>Học Sinh</strong> chấm bài này (theo trọng số)</h3>
		<?php echo 'Total Point: ' . $scoring['scoring']['total']; ?><br />
        <?php echo 'Right Point: ' . $scoring['scoring']['point']; ?><br />
        <div style="margin-top: 10px;">
        <table cellspacing="0" class="choices" style="width: 100%;">
        <tbody>
            <tr class="fc">
                <td class="percent">
					<div style="height: 25px; width:100.00%;  background: #CF3B19;" class="bar">
                        <div style="height: 25px; line-height: 25px;width:<?php echo ($entry['cache_entry_hs']); ?>%; text-align: center;  background: #499CC9;color: #ffffff;" class="bar"><strong><?php echo $entry['cache_entry_hs'] . '%' ?></strong></div>
					</div>
				</td>
        	</tr>
            
        </tbody>
        </table>
        </div>
	</div>
    <div id="z2" class="zone clearfix">
        <h3><strong>Thầy giáo</strong> chấm bài này</h3>
        <?php echo 'Total Point: ' . $scoring['scoring']['admin_total']; ?><br />
        <?php echo 'Right Point: ' . $scoring['scoring']['admin_point']; ?><br />
        <div style="margin-top: 10px;">
        <table cellspacing="0" class="choices" style="width: 100%;">
        <tbody>
            <tr class="fc">
                <td class="percent">
					<div style="height: 25px; width:100.00%;  background: #CF3B19;" class="bar">
                        <div style="height: 25px; line-height: 25px;width:<?php echo ($entry['cache_entry_tg']); ?>%; text-align: center;  background: #499CC9;color: #ffffff;" class="bar"><strong><?php echo $entry['cache_entry_tg'] . '%' ?></strong></div>
					</div>
				</td>
        	</tr>
            
        </tbody>
        </table>
        </div>
	</div>
    <?php
    }
    ?>
</div>
<!--stage-->