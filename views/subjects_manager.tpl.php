<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<div id="stage">
	<div class="info">
		<div class="buttons">
			<a class="button positive" href="<?php echo $siteUrl; ?>subjects/builder/" title="Create a New Subject">
			<img src="template/images/icons/report_add.png" alt=""/> New Subject!</a>
		</div>
		<!--buttons-->
		<h2>
			Subject Manager
		</h2>
		<div class="notranslate">
			Move still, still so, and own no other function.
		</div>
	</div>
	<!--info-->
    <?php
    $i=0;
    if($count > 0 && $form_count > 0) {
        
    ?>
    <div id="main" class="reports">
    <!--
    	<form id="" name="" class="search" onsubmit="return false;">
    		<label>
    			Filter
    		</label>
    		<input onkeyup="filterBy($F(this));" onsearch="filterBy($F(this));" id="searchBox" class="text" type="search" value="" />
    	</form>
    	<div id="sort">
    		<label>
    			Sort By :
    		</label>
    		<a id="subjectId" class="selected" href="#" onclick="sortBy('subjectId'); return false;">Date Created</a>
    		<a id="DateUpdated" class="" href="#" onclick="sortBy('DateUpdated'); return false;">Date Edited</a>
    	</div>-->
    	<div class="group">
    		<h3 id="searching" class="hide">
    			Filtered Results for :
    			<span id="searchTerm">
    			</span>
    		</h3>
    		<h3 id="noResults" class="hide">
    			None of your subjects match your filter.
    		</h3>
    		<ul id="groupList-1">
                <?php
                $i = 0;
                foreach($subjects_result as $subject) {
                    $subject_id = $subject['subject_id'];
                    $subjectId = $subject['subject_uuid'];
                    $subject_name = $subject['subject_name'];
                    if(empty($subject['subject_content'])) continue;
                    $_subject = $this->helpers->json_decode_to_array($subject['subject_content']);
                ?>
    			<li value="d_<?php echo $subjectId; ?>" id="li_<?php echo $subject_id; ?>" class="<?php echo ($i==0)?'first':'';$i++; echo ($subject['subject_status'] == 'D')?' notActive':''; ?>">
    				<h4>
    					<a id="link<?php echo $subject_id; ?>" href="<?php echo $siteUrl; ?>subjects/view/<?php echo $subjectId; ?>" target="_blank" class="notranslate">
    					<?php echo $subject_name; ?>
    					</a>
                        <span class="user_count"><?php echo $subject['user_count']; ?> Users</span>
    				</h4>
    				<span class="activeCheck">
    					<label for="publicStatus_<?php echo $subject_id; ?>">
    						Public
    					</label>
    					<input <?php echo ($subject['subject_status'] == 'A')?'checked="checked"':''; ?> type="checkbox" id="publicStatus_<?php echo $subject_id; ?>" onclick="togglePublicsubject(<?php echo $subject_id; ?>, 'Untitled%20subject', this)" />
    				</span>
    				<div id="expandThis">
    					<div class="actions">
    						<a class="del" onclick="deletesubject(this); return false;" href="#">Delete</a>
    						<!--<a class="dup" onclick="duplicatesubject(this); return false;" href="#">Duplicate</a>-->
    						<a class="view" href="<?php echo $siteUrl; ?>subjects/view/<?php echo $subjectId; ?>" target="_blank">View</a>
    						<a class="edit" href="<?php echo $siteUrl; ?>subjects/builder/<?php echo $subjectId; ?>">Edit</a>
    					</div>
    				</div>
    			</li>
                <?php
                }
                ?>
    		</ul>
    	</div>
    	<!--group-->
    	<div id="nopermission" class="notice hide">
    		<h2>
    			You do not have any subjects.
    		</h2>
    	</div>
    </div>
    <!--main-->
    <?php
    } else {
    ?>
	<div class="notice bigMessage">
		<h2>
        <?php if($form_count > 0): ?>
			<a href="<?php echo $siteUrl; ?>subjects/builder/">
            Oh no. Buddy! <span class="bigMessageRed">You don't have any subjects.</span> <span class="bigMessageGreen">Let's go make one!</span>
            </a>
        <?php else: ?>
            <span class="bigMessageRed">Holy carts before horses!</span> You need a <span class="bigMessageBlue">form</span> before you can make a <span class="bigMessagePurple">subject</span>
        <?php endif; ?>
		</h2>
	</div>
    <?php
    }
    ?>
</div>
<!--stage-->