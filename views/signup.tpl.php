<?php if ( ! defined( 'GETOVER' ) ) exit; ?>

<div id="nav">
<h1 id="logo" class="logo color1"><a title="Hoctudau">Hoctudau</a></h1>
</div>

<form id="payment" name="payment" class="hoctudau leftLabel plan1" action="" method="post">

<ul class="fundamentals">
<li class="first section">
<h3>Account Creation!</h3>
<div>
Every journey starts somewhere. This journey starts here.
</div>
</li>
<?php if(!empty($errors)) : ?>
<li id="errorLi">
<h3 id="errorMsgLbl">There was a problem creating your account.</h3>
<?php if(isset($errors['message'])) : ?>
<p id="errorMsg">
<?php echo $errors['message']; ?>
</p>
<?php endif; ?>
<p id="errorMsg">
Errors have been <strong>highlighted</strong> below.
</p>
</li>
<?php endif; ?>
<li class="<?php if(isset($errors['username'])) echo ' error'; ?>">
<label class="desc" for="username">
Username <span class="req">*</span>
<strong><?php if(isset($errors['username'])) echo $errors['username']['message']; ?></strong>
</label>
<div>
<input id="username" name="username" class="field text full" type="text" value="<?php if(isset($posts['username'])) echo $posts['username']; ?>" />
<label>...</label>
</div>
</li>
<li class="<?php if(isset($errors['email'])) echo ' error'; ?>">
<label class="desc" for="email">
Email Address <span class="req">*</span>
<strong><?php if(isset($errors['email'])) echo $errors['email']['message']; ?></strong>
</label>
<div>
<input id="email" name="email" class="field text full" type="text" value="<?php if(isset($posts['email'])) echo $posts['email']; ?>" />
<label>...</label>
</div>
</li>

<li class="complex<?php if(isset($errors['password'])) echo ' error'; ?>">
<label class="desc" for="password">
Password <span class="req">*</span>
<strong><?php if(isset($errors['password'])) echo $errors['password']['message']; ?></strong>
</label>
<div>
<span class="left">
<input id="password" name="password" class="field text addr" type="password" value="<?php if(isset($posts['password'])) echo $posts['password']; ?>" />
<label>At least <b>6</b> characters long.</label>
</span>
<span class="right">
<input id="password1" name="password1" class="field text addr" type="password" value="<?php if(isset($posts['password1'])) echo $posts['password1']; ?>" />
<label>Once more to verify, please.</label>
</span>
</div>
</li>
<?php
if($this->options['show_nickh2d']) {
?>
<li class="<?php if(isset($errors['nickh2d'])) echo ' error'; ?>">
<label class="desc" for="nickh2d">
Nick H2d <span class="req">*</span>
<strong><?php if(isset($errors['nickh2d'])) echo $errors['nickh2d']['message']; ?></strong>
</label>
<div>
<input id="nickh2d" name="nickh2d" class="field text full" type="text" value="<?php if(isset($posts['nickh2d'])) echo $posts['nickh2d']; ?>" />
<label>Nick H2D</label>
</div>
</li>
<?php
}
if($this->options['show_nickskype']) {
?>
<li class="<?php if(isset($errors['nickskype'])) echo ' error'; ?>">
<label class="desc" for="nickskype">
Nick Skype <span class="req">*</span>
<strong><?php if(isset($errors['nickskype'])) echo $errors['nickskype']['message']; ?></strong>
</label>
<div>
<input id="nickskype" name="nickskype" class="field text full" type="text" value="<?php if(isset($posts['nickskype'])) echo $posts['nickskype']; ?>" />
<label>Nick Skype</label>
</div>
</li>
<?php
}
?>
<li class="<?php if(isset($errors['fullname'])) echo ' error'; ?>">
<label class="desc" for="fullname">
Fullname <span class="req">*</span>
<strong><?php if(isset($errors['fullname'])) echo $errors['fullname']['message']; ?></strong>
</label>
<div>
<input id="fullname" name="fullname" class="field text full" type="text" value="<?php if(isset($posts['fullname'])) echo $posts['fullname']; ?>" />
<label>Tên đầy đủ</label>
</div>
</li>

<li id="licaptcha" class="captcha <?php if(isset($errors['captcha'])) echo ' error'; ?>">
<label class="desc" for="recaptcha_response_field">
                        Type the two words from the image below.
                        <span class="req">*</span>
                        </label>
                        <script type="text/javascript">
                        var RecaptchaOptions = {theme : 'clean'};
                        var host = (("https:" == document.location.protocol) ? "https://www." : "http://www.");
                        document.write(unescape("%3Cscript src='" + host + "google.com/recaptcha/api/challenge?k=6LfNO9wSAAAAADPYua7W_7hRj7pdyhw5VVrDR4cQ' type='text/javascript'%3E%3C/script%3E"));
                        </script>
                        <noscript>
                            <style type="text/css">
                            form li.captcha label.desc{
                                display:none;
                            }
                            form li.captcha{
                                border-bottom:1px dotted #ccc;
                                padding-bottom:1.2em;
                            }
                            </style>
                            <div class="noscript">
                            <label class="desc">Step 1</label>
                            <iframe src="http://www.google.com/recaptcha/api/noscript?k=6LfNO9wSAAAAADPYua7W_7hRj7pdyhw5VVrDR4cQ" height="300" width="500" frameborder="0"></iframe><br>
                            <textarea name="recaptcha_challenge_field" rows="3" cols="40">
                            </textarea>
                            <input type="hidden" name="recaptcha_response_field" value="manual_challenge" />
                            </div>
                        </noscript>

</li>

<li class="buttons">
<div>
<input id="signupButton" class="btTxt submit translate" type="submit" value="Create Account" onclick="submitSignup(); return false;" />
<a href="#">Cancel</a>
</div>

<input type="hidden" id="timezone" name="timezone" value="" />
<input type="hidden" id="idstamp" name="idstamp" value="kWZbp4UN0ivaLPxcKL5jMcCiW5T5MNZrSWWzi8/j0/E=" />
</li>

</ul><!--paymentSummary-->
</form>
