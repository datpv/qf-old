<?php if ( ! defined( 'GETOVER' ) ) exit; ?>

<div id="lightbox" class="done" style="visibility:hidden">
	<div id="lbContent" class="clearfix">
		<form id="loginForm" name="f" action="" method="post">
			<div class="info">
				<h2>
					Welcome!
				</h2>
				<div>
					...
				</div>
			</div>
			<ul>
                <?php if(isset($errors['fail'])): ?>
                <li class="error">
                     <?php echo $errors['fail']; ?>
                </li>
                <?php endif; ?>
				<li>
					<div>
						<label class="desc" for="username">
							Username
						</label>
						<input id="username" name="username" class="field text large" type="text" value=""/>
					</div>
				</li>
				<li>
					<div>
						<label class="desc" for="password">
							Password
						</label>
						<input id="password" name="password" class="field text large" type="password" value="" />
					</div>
				</li>
				<!--<li>
					<span>
						<input id="remember" name="remember" class="checkbox field" type="checkbox" value="1" />
						<label class="choice" for="remember">
							Remember me.
						</label>
					</span>
				</li>-->
				<li class="">
					<button type="submit" id="saveForm" class="button">
						<img src="template/images/icons/door_in.png" alt=""/>
						Login
					</button>
					<a href="#" class="negative button">
					<img src="template/images/icons/cross.png" alt=""/> Cancel</a>
				</li>
			</ul>
			<br />
		</form>
        <div id="noaccount">
            <a href="<?php echo $siteUrl; ?>auth/signup/">
            <h2>Don't have an account? <b> ..</b> 
            <span>Create one now.</span> <b>..</b></h2>
            </a>
        </div>
    </div>
</div>