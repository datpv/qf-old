<?php if ( ! defined( 'GETOVER' ) ) exit; ?>

<div id="stage" class="afi">
	<div id="side">
		<ul id="tabs">
			<li id="afi">
				<a onmousedown="showFields(false);" title="Add fields to your form.">Add a Field</a>
			</li>
			<li id="cfi">
				<a onmousedown="selectField();" title="Change the properties of a field here.">Field Settings</a>
			</li>
			<li id="cfo">
				<a onmousedown="selectForm();" title="Change the properties for this form here.">Form Settings</a>
			</li>
		</ul>
		<div id="addFields" class="clearfix">
			<div id="shake">
				<h3 class="stand">
					Standard
				</h3>
				<ul id="col1">
					<li id="drag1_text" class="dragfld 0">
						<a id="sl" class="button" href="#" onclick="addField('text');return false"><b></b>
						Single Line Text</a>
					</li>
					<li id="drag1_textarea" class="dragfld 1 bigger">
						<a id="ml" class="button" href="#" onclick="addField('textarea');return false"><b></b>
						Paragraph Text</a>
					</li>
					<li id="drag1_radio" class="dragfld 2 bigger">
						<a id="mc" class="button" href="#" onclick="addField('radio');return false"><b></b>
						Multiple Choice</a>
					</li>
					<li id="drag1_section" class="dragfld 3 wide">
						<a id="sb" class="button" href="#" onclick="addField('section');return false">
						Section Break</a>
					</li>
				</ul>
				<ul id="col2">
					<li id="drag2_number" class="dragfld 0">
						<a id="nu" class="button" href="#" onclick="addField('number');return false"><b></b>
						Number</a>
					</li>
					<li id="drag2_checkbox" class="dragfld 1 bigger">
						<a id="cb" class="button" href="#" onclick="addField('checkbox');return false"><b></b>
						Checkboxes</a>
					</li>
					<li id="drag2_select" class="dragfld 2">
						<a id="dd" class="button" href="#" onclick="addField('select');return false"><b></b>
						Dropdown</a>
					</li>
					<li id="drag2_page" class="dragfld 3 wide undone">
						<a id="pb" class="button" href="#" onclick="return false;addField('page');return false">
						Page Break</a>
					</li>
				</ul>
				<h3 class="fancy">
					Fancy Pants
				</h3>
				<ul id="col3">
					<li id="drag3_shortname" class="dragfld 0">
						<a id="na" class="button" href="#" onclick="addField('shortname');return false"><b></b>
						Name</a>
					</li>
					<li id="drag3_address" class="dragfld 1 biggest">
						<a id="ad" class="button" href="#" onclick="addField('address');return false"><b></b>
						Address</a>
					</li>
					<li id="drag3_email" class="dragfld 2">
						<a id="em" class="button" href="#" onclick="addField('email');return false"><b></b>
						Email</a>
					</li>
					<li id="drag3_phone" class="dragfld 3">
						<a id="ph" class="button" href="#" onclick="addField('phone');return false"><b></b>
						Phone</a>
					</li>
					<li id="drag3_money" class="dragfld 4">
						<a id="mo" class="button" href="#" onclick="addField('money');return false"><b></b>
						Price</a>
					</li>
				</ul>
				<ul id="col4">
					<li id="drag4_file" class="dragfld 0 undone">
						<a id="fu" class="button" href="#" onclick="return false;addField('file');return false;"><b></b>
						File Upload</a>
					</li>
					<li id="drag4_date" class="dragfld 1">
						<a id="da" class="button" href="#" onclick="addField('date');return false"><b></b>
						Date</a>
					</li>
					<li id="drag4_time" class="dragfld 2">
						<a id="ti" class="button" href="#" onclick="addField('time');return false"><b></b>
						Time</a>
					</li>
					<li id="drag4_url" class="dragfld 3">
						<a id="ws" class="button" href="#" onclick="addField('url');return false"><b></b>
						Website</a>
					</li>
					<li id="drag4_likert" class="dragfld 4 biggest wide">
						<a id="lk" class="button" href="#" onclick="addField('likert');return false"><b></b>
						Likert</a>
					</li>
				</ul>
			</div>
			<!-- shake -->
		</div>
		<!-- addFields -->
		<form id="fieldProperties" action="#" onsubmit="return false;" class="noI ">
			<div class="notice" id="noFieldSelected">
				<h3>
					<b>
						No Field Selected
					</b>
				</h3>
				<p>
					Please click on a field in the form preview on the right to change its properties.
				</p>
			</div>
			<ul id="allProps">
				<li class="num" id="fieldPos">
					1.
				</li>
                <li class="clear" id="listFieldLink">
					<label class="desc">
						Field Link
					</label>
					<div>
                        <input onclick="this.select()" class="text large" id="fieldLink" name="fieldLink" />
					</div>
				</li>
                <li class="clear" id="listExtraFields">
					<label class="desc">
						Extra Fields
						<a href="/docs/" class="tooltip" title="About Extra Fields" rel="Add more field at the end.">(?)</a>
					</label>
					<div>
						<a id="fieldAddExtra" type="submit" class="button" onclick="Lightbox.showUrl('lightboxes/FormBuilder.ImportExtraFields.php');return false;">Import Extra Fields</a>
                        <a id="fieldAddExtraDefault" type="submit" class="button" onclick="addExtraDefaultFields(this);return false;">Import Default Extra Fields</a>
					</div>
				</li>
				<li id="listTitle">
					<label class="desc">
						Field Label
						<a href="/docs/" class="tooltip" title="About Field Label" rel="A field's label is the most direct way of telling your user what kind of data 
						should be entered into a particular field. Field labels are usually just one or two words, 
						but can also be a question.">(?)</a>
					</label>
					<div>
						<textarea id="fieldTitle" class="textarea" rows="10" cols="35" onkeyup="updateProperties(this.value, 'Title')" onmouseup="updateProperties(this.value, 'Title')">
						</textarea>
					</div>
				</li>
				<li class="left half" id="listType">
					<label class="desc">
						Field Type
						<a href="#" class="tooltip" title="About Field Type" rel="This property detemines what kind of data can be collected by your field. In addition to your standard fields,Hoctudau provides premade structures of the most common data types like addresses and dates. This property can only be changed on newly added fields. After you save an added field to a form, the field type cannot be changed.">(?)</a>
					</label>
					<select class="select full  undone" id="fieldType" autocomplete="off" onchange="changeFieldType($F(this))">
						<optgroup label="Standard">
							<option value="text">
								Single Line Text
							</option>
							<option value="textarea">
								Paragraph Text
							</option>
							<option value="radio">
								Multiple Choice
							</option>
							<option value="checkbox">
								Checkboxes
							</option>
							<option value="select">
								Drop Down
							</option>
							<option value="number">
								Number
							</option>
							<option value="file">
								File Upload
							</option>
							<option value="section">
								Section Break
							</option>
							<!--<option value="page">Page Break</option>-->
						</optgroup>
						<optgroup label="Fancy Pants">
							<option value="shortname">
								Name
							</option>
							<option value="date">
								Date
							</option>
							<option value="time">
								Time
							</option>
							<option value="phone">
								Phone
							</option>
							<option value="address">
								Address
							</option>
							<option value="url">
								Website
							</option>
							<option value="money">
								Price
							</option>
							<option value="email">
								Email
							</option>
							<option value="likert">
								Likert
							</option>
						</optgroup>
					</select>
				</li>
				<li class="right half" id="listSize">
					<label class="desc">
						Field Size
						<a href="#" class="tooltip" title="About Field Size" rel="This property only affects the visual appearance of the field in your form. On most fields, this property affects the field's width. On Paragraph Text fields, the Field Size determines the height of the field. It does not limit nor increase the amount of data that can be collected by the field.">(?)</a>
					</label>
					<select class="select full" id="fieldSize" autocomplete="off" onchange="updateProperties($F(this), 'Size')">
						<option value="small">
							Small
						</option>
						<option value="medium">
							Medium
						</option>
						<option value="large">
							Large
						</option>
					</select>
				</li>
				<li class="right half" id="listLayout">
					<label class="desc">
						Field Layout
						<a href="#" class="tooltip" title="About Field Layout" rel="For Checkbox and Multiple Choice fields, this property determines whether you would like your choices arranged in multiple columns or sitting next to one another.">(?)</a>
					</label>
					<select class="select full" id="fieldLayout" autocomplete="off" onchange="toggleChoicesLayout($F(this));">
						<option value="">
							One Column
						</option>
						<option value="twoColumns">
							Two Columns
						</option>
						<option value="threeColumns">
							Three Columns
						</option>
						<option value="notStacked">
							Side by Side
						</option>
					</select>
				</li>
				<li class="right half" id="listDateFormat">
					<label class="desc">
						Date Format
						<a href="#" class="tooltip" title="About Date Format" rel="Choose between American and European Date Formats">(?)</a>
					</label>
					<select class="select full" id="fieldSize" autocomplete="off" onchange="changeFieldType($F(this), true);">
						<option id="fieldDateAmerican" value="date" selected="selected">
							MM / DD / YYYY
						</option>
						<option id="fieldDateEuro" value="eurodate">
							DD / MM / YYYY
						</option>
					</select>
				</li>
				<li class="right half" id="listNameFormat">
					<label class="desc">
						Name Format
						<a href="#" class="tooltip" title="About Name Format" rel="Choose between a normal name field, or an extended name field with title and suffix.">(?)</a>
					</label>
					<select class="select full" id="nameFormat" autocomplete="off" onchange="changeFieldType($F(this));">
						<option id="fieldNameNormal" value="shortname" selected="selected">
							Normal
						</option>
						<option id="fieldNameExtended" value="name">
							Extended
						</option>
					</select>
				</li>
				<li class="right half" id="listPhoneFormat">
					<label class="desc">
						Phone Format
						<a href="#" class="tooltip" title="About Phone Format" rel="Choose between American and International Phone Formats">(?)</a>
					</label>
					<select class="select full" id="fieldSize" autocomplete="off" onchange="changeFieldType($F(this), true);">
						<option id="fieldPhoneAmerican" value="phone" selected="selected">
							### - ### - ####
						</option>
						<option id="fieldPhoneEuro" value="europhone">
							International
						</option>
					</select>
				</li>
				<li class="right half" id="listMoneyFormat">
					<label class="desc">
						Currency Format
					</label>
					<select class="select full" id="fieldSize" autocomplete="off" onchange="updateProperties($F(this), 'Validation');">
						<option id="fieldMoneyBaht" value="baht">
							&#3647; Baht
						</option>
						<option id="fieldMoneyAmerican" value="" selected="selected">
							&#36; Dollars
						</option>
						<option id="fieldMoneyEuro" value="euro">
							&#8364; Euros
						</option>
						<option id="fieldMoneyForint" value="forint">
							&#70;&#116; Forint
						</option>
						<option id="fieldMoneyFranc" value="franc">
							CHF Francs
						</option>
						<option id="fieldMoneyKoruna" value="koruna">
							&#75;&#269; Koruna
						</option>
						<option id="fieldMoneyKrona" value="krona">
							kr Krona
						</option>
						<option id="fieldMoneyPesos" value="pesos">
							&#36; Pesos
						</option>
						<option id="fieldMoneyPound" value="pound">
							&#163; Pounds Sterling
						</option>
						<option id="fieldMoneyRinggit" value="ringgit">
							RM Ringgit
						</option>
						<option id="fieldMoneyShekel" value="shekel">
							&#8362; Shekel
						</option>
						<option id="fieldMoneyYen" value="yen">
							&#165; Yen
						</option>
						<option id="fieldMoneyZloty" value="zloty">
							&#122;&#322; Zloty
						</option>
					</select>
				</li>
				<li class="clear noheight">
				</li>
				<li id="listLikert" class="clear">
					<fieldset class="choices">
						<legend>
							Statements
							<a href="#" class="tooltip" title="About Statements" rel="">(?)</a>
						</legend>
						<ul id="likertStatements">
							<li>
								<input class="text statement" type="text" value="Statement 1"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
							</li>
							<li>
								<input class="text statement" type="text" value="Statement 2"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this choice."/>
							</li>
							<li>
								<input class="text statement" type="text" value="Statement 3"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this choice."/>
							</li>
						</ul>
					</fieldset>
					<br />
					<fieldset class="choices">
						<legend>
							Columns
							<a href="#" class="tooltip" title="About Statements" rel="">(?)</a>
						</legend>
						<ul id="likertColumns">
							<li>
								<input type="radio" title="Make this column pre-selected." />
								<input class="text col" type="text" value="Strongly Disagree"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another column."/>
							</li>
							<li>
								<input type="radio" title="Make this column pre-selected." />
								<input class="text col" type="text" value="Disagree"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this column."/>
							</li>
							<li>
								<input type="radio" title="Make this column pre-selected." />
								<input class="text col" type="text" value="Neither Agree nor Disagree"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this column."/>
							</li>
							<li>
								<input type="radio" title="Make this column pre-selected." />
								<input class="text" type="text" value="Agree"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this column."/>
							</li>
							<li>
								<input type="radio" title="Make this column pre-selected." />
								<input class="text" type="text" value="Strongly Agree"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this column."/>
							</li>
						</ul>
						<div id="listBulkLikert">
							<input id="bulkButton2" type="submit" class="translate" onclick="Lightbox.showUrl('lightboxes/FormBuilder.BulkLikert.php');return false;" value="Use Predefined Columns" />
						</div>
					</fieldset>
				</li>
				<li id="listChoices" class="clear">
					<fieldset class="choices">
						<legend>
							Choices
							<a href="#" class="tooltip" title="About Choices" rel="This property shows what predefined options the user can select for that particular field. Use the plus and minus buttons to add and delete choices. Click on the star to make a choice the default selection. Multiple Choice fields will automatically select the first choice as default if none is specified.">(?)</a>
						</legend>
						<ul id="fieldChoices">
							<li class="dropReq">
								<a href="#" class="tooltip" title="About Required Dropdowns" rel="This is the default down choice. When you make a drop down field required, 
								you are essentially telling the user they have to make a choice. If they leave 
								the drop down on the default choice (the one that is selected when the page loads), 
								Hoctudauwill consider that as not making a choice and will throw an error if not changed.">(?)</a>
								<input type="radio" title="Make this choice pre-selected." />
								<input class="text" type="text" value="If Dropdown Required"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this choice."/>
							</li>
							<li>
								<input type="radio" title="Make this choice pre-selected." />
								<input class="text" type="text" value="For Radio / Dropdown"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this choice."/>
							</li>
							<li>
								<input type="checkbox" title="Make this choice pre-selected." />
								<input class="text" type="text" value="For Checkbox"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this choice."/>
							</li>
						</ul>
						<div id="listPopulate">
							<input id="bulkButton" type="submit" class="translate" onclick="Lightbox.showUrl('lightboxes/FormBuilder.BulkAdd.php');return false;" value="Import Predefined Choices" />
						</div>
					</fieldset>
				</li>
                <!----------------------------------
                * Display Answers
                ------------------------------------>
                <li id="listInputAnswer" class="clear">
                    <label class="desc" for="fieldInputRightAnswer">
						List Of Right Answer
						<a href="#" class="tooltip" title="Field Input Answer" rel="This is an optional property that the right answer will compare with, separate by enter">(?)</a>
					</label>
					<div>
                        <input id="fieldInputRightAnswerRegex" type="checkbox" onkeyup="updateProperties(this.value, 'InputRightAnswerRegex')" onmouseup="updateProperties(this.value, 'InputRightAnswerRegex')" /> Use Regular Expression?
						<textarea class="textarea" rows="10" cols="50" id="fieldInputRightAnswer" onkeyup="updateProperties(this.value, 'InputRightAnswer')" onmouseup="updateProperties(this.value, 'InputRightAnswer')"></textarea>
					</div>
                </li>
                <li id="listDisplayAnswer" class="clear">
					<label class="desc" for="fieldDisplayRightAnswer">
						Description For Right Answer
						<a href="#" class="tooltip" title="Field Display Answer" rel="This is an optional property that displays the text specified to your users while they're picking right answer">(?)</a>
					</label>
					<div>
						<textarea class="textarea" rows="10" cols="50" id="fieldDisplayRightAnswer" onkeyup="updateProperties(this.value, 'RightAnswerDescription')" onmouseup="updateProperties(this.value, 'RightAnswerDescription')"></textarea>
					</div>
                    <label class="desc" for="fieldDisplayWrongAnswer">
						Description For Wrong Answer
						<a href="#" class="tooltip" title="Field Display Answer" rel="This is an optional property that displays the text specified to your users while they're picking wrong answer">(?)</a>
					</label>
					<div>
						<textarea class="textarea" rows="10" cols="50" id="fieldDisplayWrongAnswer" onkeyup="updateProperties(this.value, 'WrongAnswerDescription')" onmouseup="updateProperties(this.value, 'WrongAnswerDescription')"></textarea>
					</div>
				</li>
                <!----------------------------------
                ------------------------------------>
				<!--Button Settings-->
				<li id="listNextButtons" class="asTxt">
					<fieldset class="choices">
						<legend>
							Primary Action
							<a href="#" class="tooltip" title="About Primary Action" rel="The primary action on a form is traditionally a submit button. ForHoctudau forms with multiple pages, the primary action
							takes the user to the next page and is always shown before the secondary action, which is used to take the user to the previous page.
							On the last page of the form, the style and look of the primary action to submit the entry is handled in the Theme Designer.">(?)</a>
						</legend>
						<ul>
							<li class="left half">
								<input id="nextButtonString" name="nextButtonType" type="radio" onclick="showButtonText('Next');" />
								<label for="nextButtonString">
									Use Text Button
								</label>
							</li>
							<li class="right half">
								<input id="nextButtonLink" name="nextButtonType" type="radio" onclick="showButtonImage('Next');" />
								<label for="nextButtonLink">
									Use Image for Button
								</label>
							</li>
							<li class="btxt clear">
								<label class="desc">
									What do you want your button to say?
								</label>
								<input id="nextButtonsText" name="nextButtonsText" class="text large" type="text" value="Next Page" onkeyup="updateProperties(this.value, 'ChoicesText')" onmouseup="updateProperties(this.value, 'ChoicesText')"
								/>
							</li>
							<li class="bimg clear">
								<label class="desc">
									Where's the image of your button on the web?
								</label>
								<input id="nextButtonImg" name="nextButtonImg" class="text large" type="text" value="http://" onkeyup="updateProperties(this.value, 'ChoicesText')" onmouseup="updateProperties(this.value, 'ChoicesText')"
								/>
							</li>
						</ul>
					</fieldset>
				</li>
				<li id="listPreviousButtons" class="asTxt">
					<fieldset class="choices">
						<legend>
							Secondary Action
							<a href="#" class="tooltip" title="About Secondary Action" rel="ForHoctudau forms with multiple pages, the secondary action
							takes the user to the previous page and is always shown after the primary action, which is used to take the user to the next page.
							On the first page of a multi-page form, there is no secondary action shown because there are no previous pages before the first page.
							On the last page of the form, the style and look of the primary action to submit the entry is handled in the Theme Designer.">(?)</a>
						</legend>
						<ul>
							<li class="left half hide">
								<input id="" name="" type="radio" checked="checked" />
								<label>
									Show as Text
								</label>
							</li>
							<li class="btxt clear">
								<label class="desc">
									What do you want your link to say?
								</label>
								<input id="previousButtonsText" name="previousButtonsText" class="text large" type="text large" value="" onkeyup="updateProperties(this.value, 'DefaultVal')" onmouseup="updateProperties(this.value, 'DefaultVal')"
								/>
							</li>
						</ul>
					</fieldset>
				</li>
				<li class="left half clear" id="listOptions">
					<fieldset>
						<legend>
							Options
						</legend>
						<input id="fieldRequired" class="checkbox" type="checkbox" value="" onclick="(this.checked) ? checkVal = '1' : checkVal = '0';updateProperties(checkVal, 'IsRequired')" />
						<label class="choice" for="fieldRequired">
							Required
						</label>
						<a href="#" class="tooltip" title="About Required Validation" rel="If you want to make sure that a user fills out a particular field, use the Required validation to haveHoctudau check if the field is empty before submitting the entry. A message will be displayed to the user if they have not filled out the field. A red asterisk will appear to the right of the Field Title if the field is required.">(?)</a>
                        <br class="fieldExcerptToggle" />
						<input id="fieldExcerpt" class="checkbox fieldExcerptToggle" type="checkbox" value="" onchange="(this.checked) ? checkVal = '1' : checkVal = '0';updateProperties(checkVal, 'Excerpt')" />
						<label class="choice fieldExcerptToggle" for="fieldExcerpt">
							Excerpt
						</label>
						<a href="#" class="tooltip fieldExcerptToggle" title="About No Duplicates" rel="Field Excerpt Workon Nopbai Form">(?)</a>
						<br class="fieldUniqueToggle" />
						<input id="fieldUnique" class="checkbox fieldUniqueToggle" type="checkbox" value="" onchange="(this.checked) ? checkVal = '1' : checkVal = '0';updateProperties(checkVal, 'IsUnique')" />
						<label class="undone choice fieldUniqueToggle" for="fieldUnique">
							No Duplicates
						</label>
						<a href="#" class="tooltip fieldUniqueToggle" title="About No Duplicates" rel="Checking this validation will haveHoctudau verify that the information entered into this field is unique and has not been submitted previously. Useful for mailing lists and registration forms, where preventing the users from entering the same information more than once is often needed.">(?)</a>
						<br class="fieldEncryptedToggle" />
						<input id="fieldEncrypted" class="checkbox fieldEncryptedToggle" type="checkbox" value="" onchange="(this.checked) ? checkVal = '1' : checkVal = '0';updateProperties(checkVal, 'IsEncrypted')" />
						<label class="choice fieldEncryptedToggle" for="fieldEncrypted">
							Encrypted
						</label>
						<a href="#" class="tooltip fieldEncryptedToggle" title="About Encryption" rel="An encrypted field is stored with additional security on our servers. You may have up to 5 encrypted fields. These fields are also NOT sent in email notifications. Ideal for collecting sensitive data.">(?)</a>
						<br class="fieldRandomToggle" />
						<input id="fieldRandom" name="randomize" class="checkbox fieldRandomToggle" type="checkbox" value="rand" onchange="(this.checked) ? checkVal = 'rand' : checkVal = '';updateProperties(checkVal, 'Validation')"/>
						<label class="choice fieldRandomToggle" for="fieldRandom">
							Randomize
						</label>
						<a href="#" class="tooltip fieldRandomToggle" title="About Random Order" rel="Check this option if you would like the choices to be shuffled around each time someone views your form.">(?)</a>
						<br class="fieldOtherToggle" />
						<input id="fieldOther" name="other" class="checkbox fieldOtherToggle translate" type="checkbox" value="other" onclick="(this.checked) ? checkVal = true : checkVal = false;toggleRadioOther(checkVal)"/>
						<label class="choice fieldOtherToggle" for="fieldOther">
							Allow Other
						</label>
						<a href="#" class="tooltip fieldOtherToggle" title="About Allow Other" rel="Check this option if you would like the last choice in your multiple choice field to have a text field for additional input.">(?)</a>
						<br class="fieldNumbersToggle" />
						<input id="fieldNumbers" name="numbers" class="checkbox fieldNumbersToggle" type="checkbox" value="hideNumbers" onclick="(this.checked) ? checkVal = true : checkVal = false;toggleLikertNumbers(checkVal)"/>
						<label class="choice fieldNumbersToggle" for="fieldNumbers">
							Hide Numbers
						</label>
						<a href="#" class="tooltip fieldNumbersToggle" title="About Showing Numbers" rel="Each option may have a numerical label underneath it. These labels represent the score of the option. Checking this box will toggle the labels on and off.">(?)</a>
						<br class="fieldNAToggle" />
						<input id="fieldNA" name="nonapplicable" class="checkbox fieldNAToggle" type="checkbox" value="na" onclick="(this.checked) ? checkVal = true : checkVal = false;toggleLikertNonApplicable(checkVal)"/>
						<label class="choice fieldNAToggle" for="fieldNA">
							Not Applicable
						</label>
						<a href="#" class="tooltip fieldNAToggle" title="About Non Applicable" rel="Check this option if you would like to add an additional column which allows the users to flag a choice as non applicable.">(?)</a>
                        <br class="fieldDCToggle" />
						<input id="fieldDC" name="datcheckbox" class="checkbox fieldDCToggle" type="checkbox" value="dc" onclick="(this.checked) ? checkVal = true : checkVal = false;toggleLikertDatCheckbox(checkVal)"/>
						<label class="choice fieldDCToggle" for="fieldDC">
							Switch to checkbox
						</label>
						<a href="#" class="tooltip fieldDCToggle" title="About Switch checkbox" rel="Check this option if you would like to switch to checkbox style.">(?)</a>
					</fieldset>
				</li>
				<li class="right half" id="listSecurity">
					<fieldset>
						<legend>
							Show Field to
						</legend>
						<input id="fieldPublic" name="security" class="radio" type="radio" value="" checked="checked" onclick="updateProperties('0', 'IsPrivate')" />
						<label class="choice" for="fieldPublic">
							Everyone
						</label>
						<a href="#" class="tooltip" title="About Public Access" rel="Set the field to 'Public Access' if you would like the field to be accessible by anyone when the form is made public.">(?)</a>
						<br />
						<input id="fieldPrivate" name="security" class="radio" type="radio" value="" onclick="updateProperties('1', 'IsPrivate')" />
						<label class="choice" for="fieldPrivate">
							Admin Only
						</label>
						<a href="#" class="tooltip" title="About Admin Only" rel="Set the field to 'Admin Only' if you would like the field to ONLY be accessible via theHoctudau Admin interface. Fields that are set to 'Admin Only' will not be shown to users when the form is made public. Useful for forms that need to collect public submissions but then need to be ranked or added to privately in the Admin interface.">(?)</a>
					</fieldset>
				</li>
				<li class="clear noheight">
				</li>
				<li id="listRange" class="complex clear">
					<fieldset class="minmax">
						<legend>
							Range
							<a href="#" class="tooltip" title="About Range" rel="Range allows you to specify limits on the data entered. For example, you can limit the amount of characters typed to be between 5 and 10. You can also substitute the character limit with that of words, or in the case of numbers, their values (x > y). If you do not wish to have a range limitation set, just leave the values blank or set them to 0.">(?)</a>
						</legend>
						<div>
							<span>
								<label class="desc" for="fieldRangeMin">
									Min
								</label>
								<input id="fieldRangeMin" name="fieldRangeMin" class="text" type="text" value="" onkeyup="updateProperties(this.value, 'RangeMin')" onmouseup="updateProperties(this.value, 'RangeMin')" />
							</span>
							<span>
								<label class="desc" for="fieldRangeMax">
									Max
								</label>
								<input id="fieldRangeMax" name="fieldRangeMax" class="text" type="text" value="" onkeyup="updateProperties(this.value, 'RangeMax')" onmouseup="updateProperties(this.value, 'RangeMax')" />
							</span>
							<span>
								<label class="desc" for="fieldRangeType">
									Format
								</label>
								<select id="fieldRangeType" name="fieldRangeType" class="select" onchange="updateProperties(this.value, 'RangeType')">
									<option value="character">
										Characters
									</option>
									<option value="word">
										Words
									</option>
								</select>
							</span>
						</div>
					</fieldset>
				</li>
				<li class="clear" id="listTextDefault">
					<label class="desc" for="fieldDefault">
						Predefined Value
						<a href="#" class="tooltip" title="About Predefined Value" rel="By setting this value, the field will be prepopulated with the text you enter when a user comes to visit your form.">(?)</a>
					</label>
					<div>
						<input id="fieldDefault" class="text large" type="text" name="text" value="" maxlength="255" onkeyup="updateProperties($F(this), 'DefaultVal')" onmouseup="updateProperties($F(this), 'DefaultVal')" />
					</div>
				</li>
				<li class="clear" id="listDateDefault">
					<label class="desc" for="fieldDateDefault">
						Predefined Date
						<a href="#" class="tooltip" title="About Predefined Value" rel="By setting this value, the field will be prepopulated with the text you enter when a user comes to visit your form. For Date fields, acceptable formats are ##/##/#### or time based words such as 'today', 'tomorrow', '+1 week', 'last thursday'">(?)</a>
					</label>
					<div>
						<input id="fieldDateDefault" class="text large" type="text" name="text" value="" maxlength="255" onkeyup="updateProperties($F(this), 'DefaultVal'); defaultDate($F(this));" onmouseup="updateProperties($F(this), 'DefaultVal'); defaultDate($F(this));"
						/>
					</div>
				</li>
				<li class="clear" id="listEuroDateDefault">
					<label class="desc" for="fieldEuroDateDefault">
						Predefined Date
						<a href="#" class="tooltip" title="About Predefined Value" rel="By setting this value, the field will be prepopulated with the text you enter when a user comes to visit your form. For Date fields, acceptable formats are ##/##/#### or time based words such as 'today', 'tomorrow', '+1 week', 'last thursday'">(?)</a>
					</label>
					<div>
						<input id="fieldEuroDateDefault" class="text large" type="text" name="text" value="" maxlength="255" onkeyup="updateProperties($F(this), 'DefaultVal'); defaultDate($F(this));" onmouseup="updateProperties($F(this), 'DefaultVal'); defaultDate($F(this));"
						/>
					</div>
				</li>
				<li class="clear" id="listPriceDefault">
					<label class="desc" for="fieldPriceDefault">
						Predefined Price
						<a href="#" class="tooltip" title="About Predefined Value" rel="By setting this value, the field will be prepopulated with the price amount you enter.">(?)</a>
					</label>
					<div>
						<input id="fieldPriceDefault" class="text large" type="text" name="text" value="" maxlength="255" onkeyup="updateProperties($F(this), 'DefaultVal'); defaultPrice($F(this));" onmouseup="updateProperties($F(this), 'DefaultVal'); defaultPrice($F(this));"
						/>
					</div>
				</li>
				<li class="clear" id="listAddressDefault">
					<label class="desc" for="fieldAddressDefault">
						Predefined Country
						<a href="#" class="tooltip" title="About Predefined Country" rel="By setting this value, the country field will be prepopulated with the selection you make when a user visits your form.">(?)</a>
					</label>
					<div>
						<select class="select full" id="fieldCountries" onchange="updateProperties($F(this), 'DefaultVal')">
							<option value="">
							</option>
							<optgroup label="North America">
								<option value="Antigua and Barbuda">
									Antigua and Barbuda
								</option>
								<option value="Aruba">
									Aruba
								</option>
								<option value="Bahamas">
									Bahamas
								</option>
								<option value="Barbados">
									Barbados
								</option>
								<option value="Belize">
									Belize
								</option>
								<option value="Bermuda">
									Bermuda
								</option>
								<option value="Canada">
									Canada
								</option>
								<option value="Cayman Islands">
									Cayman Islands
								</option>
								<option value="Cook Islands">
									Cook Islands
								</option>
								<option value="Costa Rica">
									Costa Rica
								</option>
								<option value="Cuba">
									Cuba
								</option>
								<option value="Dominica">
									Dominica
								</option>
								<option value="Dominican Republic">
									Dominican Republic
								</option>
								<option value="El Salvador">
									El Salvador
								</option>
								<option value="Grenada">
									Grenada
								</option>
								<option value="Guatemala">
									Guatemala
								</option>
								<option value="Haiti">
									Haiti
								</option>
								<option value="Honduras">
									Honduras
								</option>
								<option value="Jamaica">
									Jamaica
								</option>
								<option value="Mexico">
									Mexico
								</option>
								<option value="Netherlands Antilles">
									Netherlands Antilles
								</option>
								<option value="Nicaragua">
									Nicaragua
								</option>
								<option value="Panama">
									Panama
								</option>
								<option value="Puerto Rico">
									Puerto Rico
								</option>
								<option value="Saint Kitts and Nevis">
									Saint Kitts and Nevis
								</option>
								<option value="Saint Lucia">
									Saint Lucia
								</option>
								<option value="Saint Vincent and the Grenadines">
									Saint Vincent and the Grenadines
								</option>
								<option value="Trinidad and Tobago">
									Trinidad and Tobago
								</option>
								<option value="United States">
									United States
								</option>
								<option value="Virgin Islands, British">
									Virgin Islands, British
								</option>
								<option value="Virgin Islands, U.S.">
									Virgin Islands, U.S.
								</option>
							</optgroup>
							<optgroup label="South America">
								<option value="Argentina">
									Argentina
								</option>
								<option value="Bolivia">
									Bolivia
								</option>
								<option value="Brazil">
									Brazil
								</option>
								<option value="Chile">
									Chile
								</option>
								<option value="Colombia">
									Colombia
								</option>
								<option value="Ecuador">
									Ecuador
								</option>
								<option value="Guyana">
									Guyana
								</option>
								<option value="Paraguay">
									Paraguay
								</option>
								<option value="Peru">
									Peru
								</option>
								<option value="Suriname">
									Suriname
								</option>
								<option value="Uruguay">
									Uruguay
								</option>
								<option value="Venezuela">
									Venezuela
								</option>
							</optgroup>
							<optgroup label="Europe">
								<option value="Albania">
									Albania
								</option>
								<option value="Andorra">
									Andorra
								</option>
								<option value="Armenia">
									Armenia
								</option>
								<option value="Austria">
									Austria
								</option>
								<option value="Azerbaijan">
									Azerbaijan
								</option>
								<option value="Belarus">
									Belarus
								</option>
								<option value="Belgium">
									Belgium
								</option>
								<option value="Bosnia and Herzegovina">
									Bosnia and Herzegovina
								</option>
								<option value="Bulgaria">
									Bulgaria
								</option>
								<option value="Croatia">
									Croatia
								</option>
								<option value="Cyprus">
									Cyprus
								</option>
								<option value="Czech Republic">
									Czech Republic
								</option>
								<option value="Denmark">
									Denmark
								</option>
								<option value="Estonia">
									Estonia
								</option>
								<option value="Faroe Islands">
									Faroe Islands
								</option>
								<option value="Finland">
									Finland
								</option>
								<option value="France">
									France
								</option>
								<option value="Georgia">
									Georgia
								</option>
								<option value="Germany">
									Germany
								</option>
								<option value="Greece">
									Greece
								</option>
								<option value="Hungary">
									Hungary
								</option>
								<option value="Iceland">
									Iceland
								</option>
								<option value="Ireland">
									Ireland
								</option>
								<option value="Italy">
									Italy
								</option>
								<option value="Latvia">
									Latvia
								</option>
								<option value="Liechtenstein">
									Liechtenstein
								</option>
								<option value="Lithuania">
									Lithuania
								</option>
								<option value="Luxembourg">
									Luxembourg
								</option>
								<option value="Macedonia">
									Macedonia
								</option>
								<option value="Malta">
									Malta
								</option>
								<option value="Moldova">
									Moldova
								</option>
								<option value="Monaco">
									Monaco
								</option>
								<option value="Montenegro">
									Montenegro
								</option>
								<option value="Netherlands">
									Netherlands
								</option>
								<option value="Norway">
									Norway
								</option>
								<option value="Poland">
									Poland
								</option>
								<option value="Portugal">
									Portugal
								</option>
								<option value="Romania">
									Romania
								</option>
								<option value="San Marino">
									San Marino
								</option>
								<option value="Serbia">
									Serbia
								</option>
								<option value="Slovakia">
									Slovakia
								</option>
								<option value="Slovenia">
									Slovenia
								</option>
								<option value="Spain">
									Spain
								</option>
								<option value="Sweden">
									Sweden
								</option>
								<option value="Switzerland">
									Switzerland
								</option>
								<option value="Ukraine">
									Ukraine
								</option>
								<option value="United Kingdom">
									United Kingdom
								</option>
								<option value="Vatican City">
									Vatican City
								</option>
							</optgroup>
							<optgroup label="Asia">
								<option value="Afghanistan">
									Afghanistan
								</option>
								<option value="Bahrain">
									Bahrain
								</option>
								<option value="Bangladesh">
									Bangladesh
								</option>
								<option value="Bhutan">
									Bhutan
								</option>
								<option value="Brunei Darussalam">
									Brunei Darussalam
								</option>
								<option value="Myanmar">
									Myanmar
								</option>
								<option value="Cambodia">
									Cambodia
								</option>
								<option value="China">
									China
								</option>
								<option value="East Timor">
									East Timor
								</option>
								<option value="Hong Kong">
									Hong Kong
								</option>
								<option value="India">
									India
								</option>
								<option value="Indonesia">
									Indonesia
								</option>
								<option value="Iran">
									Iran
								</option>
								<option value="Iraq">
									Iraq
								</option>
								<option value="Israel">
									Israel
								</option>
								<option value="Japan">
									Japan
								</option>
								<option value="Jordan">
									Jordan
								</option>
								<option value="Kazakhstan">
									Kazakhstan
								</option>
								<option value="North Korea">
									North Korea
								</option>
								<option value="South Korea">
									South Korea
								</option>
								<option value="Kuwait">
									Kuwait
								</option>
								<option value="Kyrgyzstan">
									Kyrgyzstan
								</option>
								<option value="Laos">
									Laos
								</option>
								<option value="Lebanon">
									Lebanon
								</option>
								<option value="Malaysia">
									Malaysia
								</option>
								<option value="Maldives">
									Maldives
								</option>
								<option value="Mongolia">
									Mongolia
								</option>
								<option value="Nepal">
									Nepal
								</option>
								<option value="Oman">
									Oman
								</option>
								<option value="Pakistan">
									Pakistan
								</option>
								<option value="Palestine">
									Palestine
								</option>
								<option value="Philippines">
									Philippines
								</option>
								<option value="Qatar">
									Qatar
								</option>
								<option value="Russia">
									Russia
								</option>
								<option value="Saudi Arabia">
									Saudi Arabia
								</option>
								<option value="Singapore">
									Singapore
								</option>
								<option value="Sri Lanka">
									Sri Lanka
								</option>
								<option value="Syria">
									Syria
								</option>
								<option value="Taiwan">
									Taiwan
								</option>
								<option value="Tajikistan">
									Tajikistan
								</option>
								<option value="Thailand">
									Thailand
								</option>
								<option value="Turkey">
									Turkey
								</option>
								<option value="Turkmenistan">
									Turkmenistan
								</option>
								<option value="United Arab Emirates">
									United Arab Emirates
								</option>
								<option value="Uzbekistan">
									Uzbekistan
								</option>
								<option value="Vietnam">
									Vietnam
								</option>
								<option value="Yemen">
									Yemen
								</option>
							</optgroup>
							<optgroup label="Oceania">
								<option value="Australia">
									Australia
								</option>
								<option value="Fiji">
									Fiji
								</option>
								<option value="Kiribati">
									Kiribati
								</option>
								<option value="Marshall Islands">
									Marshall Islands
								</option>
								<option value="Micronesia">
									Micronesia
								</option>
								<option value="Nauru">
									Nauru
								</option>
								<option value="New Zealand">
									New Zealand
								</option>
								<option value="Palau">
									Palau
								</option>
								<option value="Papua New Guinea">
									Papua New Guinea
								</option>
								<option value="Samoa">
									Samoa
								</option>
								<option value="Solomon Islands">
									Solomon Islands
								</option>
								<option value="Tonga">
									Tonga
								</option>
								<option value="Tuvalu">
									Tuvalu
								</option>
								<option value="Vanuatu">
									Vanuatu
								</option>
							</optgroup>
							<optgroup label="Africa">
								<option value="Algeria">
									Algeria
								</option>
								<option value="Angola">
									Angola
								</option>
								<option value="Benin">
									Benin
								</option>
								<option value="Botswana">
									Botswana
								</option>
								<option value="Burkina Faso">
									Burkina Faso
								</option>
								<option value="Burundi">
									Burundi
								</option>
								<option value="Cameroon">
									Cameroon
								</option>
								<option value="Cape Verde">
									Cape Verde
								</option>
								<option value="Central African Republic">
									Central African Republic
								</option>
								<option value="Chad">
									Chad
								</option>
								<option value="Comoros">
									Comoros
								</option>
								<option value="Democratic Republic of the Congo">
									Democratic Republic of the Congo
								</option>
								<option value="Republic of the Congo">
									Republic of the Congo
								</option>
								<option value="Djibouti">
									Djibouti
								</option>
								<option value="Egypt">
									Egypt
								</option>
								<option value="Equatorial Guinea">
									Equatorial Guinea
								</option>
								<option value="Eritrea">
									Eritrea
								</option>
								<option value="Ethiopia">
									Ethiopia
								</option>
								<option value="Gabon">
									Gabon
								</option>
								<option value="Gambia">
									Gambia
								</option>
								<option value="Ghana">
									Ghana
								</option>
								<option value="Gibraltar">
									Gibraltar
								</option>
								<option value="Guinea">
									Guinea
								</option>
								<option value="Guinea-Bissau">
									Guinea-Bissau
								</option>
								<option value="C&ocirc;te d'Ivoire">
									C&ocirc;te d'Ivoire
								</option>
								<option value="Kenya">
									Kenya
								</option>
								<option value="Lesotho">
									Lesotho
								</option>
								<option value="Liberia">
									Liberia
								</option>
								<option value="Libya">
									Libya
								</option>
								<option value="Madagascar">
									Madagascar
								</option>
								<option value="Malawi">
									Malawi
								</option>
								<option value="Mali">
									Mali
								</option>
								<option value="Mauritania">
									Mauritania
								</option>
								<option value="Mauritius">
									Mauritius
								</option>
								<option value="Morocco">
									Morocco
								</option>
								<option value="Mozambique">
									Mozambique
								</option>
								<option value="Namibia">
									Namibia
								</option>
								<option value="Niger">
									Niger
								</option>
								<option value="Nigeria">
									Nigeria
								</option>
								<option value="Rwanda">
									Rwanda
								</option>
								<option value="Sao Tome and Principe">
									Sao Tome and Principe
								</option>
								<option value="Senegal">
									Senegal
								</option>
								<option value="Seychelles">
									Seychelles
								</option>
								<option value="Sierra Leone">
									Sierra Leone
								</option>
								<option value="Somalia">
									Somalia
								</option>
								<option value="South Africa">
									South Africa
								</option>
								<option value="Sudan">
									Sudan
								</option>
								<option value="Swaziland">
									Swaziland
								</option>
								<option value="United Republic of Tanzania">
									Tanzania
								</option>
								<option value="Togo">
									Togo
								</option>
								<option value="Tunisia">
									Tunisia
								</option>
								<option value="Uganda">
									Uganda
								</option>
								<option value="Zambia">
									Zambia
								</option>
								<option value="Zimbabwe">
									Zimbabwe
								</option>
							</optgroup>
						</select>
					</div>
				</li>
				<li class="clear" id="listPhoneDefault">
					<label class="desc" for="fieldPhoneDefault1">
						Predefined Value
						<a href="#" class="tooltip" title="About Predefined Value" rel="By setting this value, the field will be prepopulated with the text you enter when a user comes to visit your form.">(?)</a>
					</label>
					<div>
						<input id="fieldPhoneDefault1" class="text" size="3" type="text" name="text" value="" maxlength="3" onkeyup="updateProperties($F('fieldPhoneDefault1').toString()+$F('fieldPhoneDefault2').toString()+$F('fieldPhoneDefault3').toString(), 'DefaultVal')"
						onmouseup="updateProperties($F('fieldPhoneDefault1').toString()+$F('fieldPhoneDefault2').toString()+$F('fieldPhoneDefault3').toString(), 'DefaultVal')" />
						-
						<input id="fieldPhoneDefault2" class="text" size="3" type="text" name="text" value="" maxlength="3" onkeyup="updateProperties($F('fieldPhoneDefault1').toString()+$F('fieldPhoneDefault2').toString()+$F('fieldPhoneDefault3').toString(), 'DefaultVal')"
						onmouseup="updateProperties($F('fieldPhoneDefault1').toString()+$F('fieldPhoneDefault2').toString()+$F('fieldPhoneDefault3').toString(), 'DefaultVal')" />
						-
						<input id="fieldPhoneDefault3" class="text" size="4" type="text" name="text" value="" maxlength="4" onkeyup="updateProperties($F('fieldPhoneDefault1').toString()+$F('fieldPhoneDefault2').toString()+$F('fieldPhoneDefault3').toString(), 'DefaultVal')"
						onmouseup="updateProperties($F('fieldPhoneDefault1').toString()+$F('fieldPhoneDefault2').toString()+$F('fieldPhoneDefault3').toString(), 'DefaultVal')" />
					</div>
				</li>
				<li id="listInstructions" class="clear">
					<label class="desc" for="fieldInstructions">
						Instructions for User
						<a href="#" class="tooltip" title="Field Instructions" rel="This is an optional property that displays the text specified to your users while they're filling out that particular field. They appear off to the right side of the field.">(?)</a>
					</label>
					<div>
						<textarea class="textarea" rows="10" cols="50" id="fieldInstructions" onkeyup="updateProperties(this.value, 'Instructions')" onmouseup="updateProperties(this.value, 'Instructions')">
						</textarea>
					</div>
				</li>
				<li id="listClassNames" class="clear">
					<label class="desc" for="fieldClassNames">
						Add CSS Layout Keywords
						<a href="/docs/" class="tooltip" title="About CSS Keywords" rel="For each field, you can specify CSS classnames to be added to the parent element surrounding the field to use as handles for your custom stylesheet using the advanced properties in the Theme Designer. You can also use built in keywords that activate styles built into theHoctudau Form Stylesheet like 'hide', 'leftHalf, 'rightHalf' and 'altInstruct'. These changes will NOT appear live in Form Builder--only on the live form and Entry Manager.">(?)</a>
					</label>
					<div>
						<input id="fieldClassNames" class="text large" type="text" name="text" value="" maxlength="255" onkeyup="updateProperties($F(this), 'ClassNames');" onmouseup="updateProperties($F(this), 'ClassNames');"
						/>
					</div>
					<span class="hide">
						<a href="#" onclick="updateClassNames('hide'); return false;">hide</a>
						<a href="#" onclick="updateClassNames('rightLabel'); return false;">rightLabel</a>
						<a href="#" onclick="updateClassNames('leftLabel'); return false;">leftLabel</a>
					</span>
				</li>
				<li id="listButtons" class="center">
					<a href="#" class="positive button" title="Duplicate this field." id="listButtonsDuplicate">
					<img src="template/images/icons/add.png" alt=""/> Duplicate</a>
					<a href="#" class="negative button" title="Delete this field." id="listButtonsDelete">
					<img src="template/images/icons/delete.png" alt=""/> Delete</a>
					<a href="#" class="button" title="Add another field." onclick="showFields(true);return false">
					<img src="template/images/icons/textfield_add.png" alt=""/> Add Field</a>
				</li>
			</ul>
		</form>
		<!-- fieldProperties -->
		<form id="formProperties" action="#" onsubmit="return false;" class="noI ">
			<ul id="listPagination">
				<li>
					<fieldset class="choices">
						<legend>
							Pagination Options
							<a href="#" class="tooltip" title="About Pagination Options" rel="By default on forms with more than one page,Hoctudau tries to present to the user some context at the top and bottom of every page
							to help the user understand how much of the form has been completed and what is left to be filled out. You 
							can choose to show this progress as a series of steps, a percentage bar or not have it show at all.">(?)</a>
						</legend>
						<ul class="pagingStyles">
							<li>
								<input id="breadcrumbs1" name="breadcrumbs" type="radio" value="1" onclick="updateForm($F(this), 'BreadCrumbType');" checked="checked" />
								<label for="breadcrumbs1">
									Steps
								</label>
								<input id="breadcrumbs2" name="breadcrumbs" type="radio" value="2" onclick="updateForm($F(this), 'BreadCrumbType');" />
								<label for="breadcrumbs2">
									Percentage
								</label>
								<input id="breadcrumbs0" name="breadcrumbs" type="radio" value="0" onclick="updateForm($F(this), 'BreadCrumbType');" />
								<label for="breadcrumbs0">
									No Context
								</label>
							</li>
						</ul>
						<div class="highlight">
							<input id="showPageTitles" class="checkbox" type="checkbox" value="1" checked="checked" onclick="(this.checked)?updateForm('1', 'ShowPageTitle'):updateForm('0','ShowPageTitle');" />
							<label class="choice" for="showPageTitles">
								Show Page Titles in Progress Bar
							</label>
							<a class="tooltip" title="About Page Titles in Progress Bar" rel="In addition to showing the page number or percentage of the form that's been completed, you
							can specify individual names for each page on your form. This is great for forms that want to create clear
							sign posts for each step in the form completion process.">(?)</a>
							<br />
							<ul id="pageTitles" class="">
								<li class="paymentPageTitle">
									<h4>
										Payment Page
									</h4>
									<input id="paymentTitle" name="" class="text" type="text" value="" onkeyup="updateForm(this.value, 'PaymentPageTitle')" onblur="updateForm(this.value, 'PaymentPageTitle')" />
									<a class="tooltip" title="About Payment Page Title" rel="This is the title for the payment summary pageHoctudau will redirect to after the form 
									is submitted if the page is payment enabled.">(?)</a>
								</li>
							</ul>
						</div>
						<ul class="pagelabelul">
							<li>
								<input id="pageFooter" name="" class="checkbox" type="checkbox" onclick="(this.checked)?updateForm('0', 'ShowPageFooter'):updateForm('1','ShowPageFooter');" />
								<label for="pageFooter" class="choice">
									Hide Page Numbers in Footer
								</label>
							</li>
						</ul>
					</fieldset>
				</li>
			</ul>
			<ul id="formSettings">
                <li>
					<label class="desc">
						Form Type
						<a class="tooltip" title="About Form Type" rel="Choice Your Form Type">(?)</a>
					</label>
					<div>
                        <input id="formType3" class="formtype radio" name="Formtype" type="radio" value="nopbai" onkeyup="updateForm(this.value, 'Formtype')" onblur="updateForm(this.value, 'Formtype')" /><label class="choice" for="formType3">Nopbai Form</label>
						<input id="formType1" class="formtype radio" name="Formtype" type="radio" value="score" onkeyup="updateForm(this.value, 'Formtype')" onblur="updateForm(this.value, 'Formtype')" /><label class="choice" for="formType1">Score Form</label>
                        <input id="formType2" class="formtype radio" name="Formtype" type="radio" value="normal" onkeyup="updateForm(this.value, 'Formtype')" onblur="updateForm(this.value, 'Formtype')" /><label class="choice" for="formType2">Normal Form</label>
                        <input id="formType4" class="formtype radio" name="Formtype" type="radio" value="autoscore" onkeyup="updateForm(this.value, 'Formtype')" onblur="updateForm(this.value, 'Formtype')" /><label class="choice" for="formType4">AutoScore Form</label>
					</div>
				</li>
                <li>
                    <div>
                        <label class="desc">
							Form Chấm Điểm Cho Form Nộp Bài
							<a class="tooltip" title="About Form Name" rel="Chọn Form Dùng Để Chấm Điểm Cho Form Nộp Bài">(?)</a>
						</label>
                        <select onchange="updateForm($F(this), 'FormScoreUrl')" autocomplete="off" id="FormScoreUrl" class="select large">
                            <option value=""></option>
                        <?php
                        foreach($forms_result as $form_result) {
                            $form_sc_id = $form_result['form_uuid'];
                            $form_sc_content = $this->helpers->json_decode_to_array($form_result['form_content']);
                        ?>
							<option <?php if($formScoreUrl == $form_sc_id) echo 'selected="selected"'; ?> value="<?php echo $form_sc_id; ?>">
								<?php echo $form_sc_content['Name']; ?>
							</option>
                        <?php
                        }
                        ?>
                        </select>
					</div>
                </li>
				<li>
					<label class="desc">
						Form Name
						<a class="tooltip" title="About Form Name" rel="The first piece of text displayed to the user when they see your form. The form name is also used to 
						create the URL that indicates where the form is located on the web.">(?)</a>
					</label>
					<div>
						<input id="formName" class="text large" type="text" value="" maxlength="250" onkeyup="updateForm(this.value, 'Name')" onblur="updateForm(this.value, 'Name')" />
					</div>
				</li>
				<li class="clear">
					<label class="desc">
						Description
						<a class="tooltip" title="About Form Description" rel="Use this property to display a short description or any instructions, notes, or guidelines that your user should read when filling out the form. This will appear directly below the form name.">(?)</a>
					</label>
					<div>
						<textarea class="textarea" rows="10" cols="50" id="formDescription" onkeyup="updateForm(this.value, 'Description')" onblur="updateForm(this.value, 'Description')">
						</textarea>
					</div>
				</li>
				<li class="left half">
					<label class="desc">
						Language
						<a class="tooltip" title="About Languages" rel="When there are system messages (such as submission errors)  you can choose what language they appear in. If your language is not on the list, consider contributing to our translation project at hoctudau.com/translate/public/">(?)</a>
					</label>
					<div>
						<select class="select large" id="formLanguage" autocomplete="off" onchange="updateForm($F(this), 'Language')">
							<option value="preview">
								Preview Translations
							</option>
							<option class="notranslate" value="preview">
								- - - - - - - - -
							</option>
							<option class="notranslate" value="afrikaans">
								Afrikaans
							</option>
							<option class="notranslate" value="arabic" dir="rtl">
								????????
							</option>
							<option class="notranslate" value="armenian">
								???????
							</option>
							<option class="notranslate" value="bulgarian">
								?????????
							</option>
							<option class="notranslate" value="catalan">
								Català
							</option>
							<option class="notranslate" value="chinese-simplified">
								??(??)
							</option>
							<option class="notranslate" value="chinese-traditional">
								??(??)
							</option>
							<option class="notranslate" value="croatian">
								Hrvatski
							</option>
							<option class="notranslate" value="czech">
								Ceština
							</option>
							<option class="notranslate" value="danish">
								Dansk
							</option>
							<option class="notranslate" value="dutch">
								Nederlands
							</option>
							<option class="notranslate" value="english">
								English
							</option>
							<option class="notranslate" value="estonian">
								Eesti
							</option>
							<option class="notranslate" value="faroese">
								Føroyskt
							</option>
							<option class="notranslate" value="finnish">
								Suomi
							</option>
							<option class="notranslate" value="french">
								Français
							</option>
							<option class="notranslate" value="galician">
								Galego
							</option>
							<option class="notranslate" value="german">
								Deutsch
							</option>
							<option class="notranslate" value="greek">
								????????
							</option>
							<option class="notranslate" value="hebrew" dir="rtl">
								???????
							</option>
							<option class="notranslate" value="hungarian">
								Magyar
							</option>
							<option class="notranslate" value="icelandic">
								Íslenska
							</option>
							<option class="notranslate" value="indonesian">
								Bahasa Indonesia
							</option>
							<option class="notranslate" value="italian">
								Italiano
							</option>
							<option class="notranslate" value="japanese">
								???
							</option>
							<option class="notranslate" value="korean">
								???
							</option>
							<option class="notranslate" value="latvian">
								Latviešu
							</option>
							<option class="notranslate" value="lithuanian">
								Lietuviu
							</option>
							<option class="notranslate" value="macedonian">
								??????????
							</option>
							<option class="notranslate" value="malay">
								Bahasa Melayu
							</option>
							<option class="notranslate" value="norwegian">
								Norsk
							</option>
							<option class="notranslate" value="persian" dir="rtl">
								??????
							</option>
							<option class="notranslate" value="polish">
								Polski
							</option>
							<option class="notranslate" value="portuguese">
								Português
							</option>
							<option class="notranslate" value="brazilian-portuguese">
								Português (Brasil)
							</option>
							<option class="notranslate" value="romanian">
								Româna
							</option>
							<option class="notranslate" value="russian">
								???????
							</option>
							<option class="notranslate" value="serbian">
								??????
							</option>
							<option class="notranslate" value="slovak">
								Slovencina
							</option>
							<option class="notranslate" value="slovenian">
								Slovenšcina
							</option>
							<option class="notranslate" value="spanish">
								Español
							</option>
							<option class="notranslate" value="swedish">
								Svenska
							</option>
							<option class="notranslate" value="swiss-german">
								Schwyzerdütsch
							</option>
							<option class="notranslate" value="turkish">
								Türkçe
							</option>
							<option class="notranslate" value="ukranian">
								??????????
							</option>
							<option class="notranslate" value="vietnamese">
								Ti?ng Vi?t
							</option>
							<option class="notranslate" value="welsh">
								Cymraeg
							</option>
						</select>
					</div>
				</li>
				<li class="right half">
					<label class="desc" for="formLabelAlign">
						Label Placement
						<a class="tooltip" title="About Label Placement" rel="This property sets the field labels to be placed either above the fields or right/left aligned on the left side of the fields.">(?)</a>
					</label>
					<div>
						<select class="select large" id="formLabelAlign" autocomplete="off" onchange="updateForm($F(this), 'LabelAlign')">
							<option value="topLabel">
								Top Aligned
							</option>
							<option value="leftLabel">
								Left Aligned
							</option>
							<option value="rightLabel">
								Right Aligned
							</option>
						</select>
					</div>
				</li>
				<li class="clear noheight">
				</li>
				<li id="listConfirm" class="clear">
					<fieldset class="confirm">
						<legend>
							Confirmation Options
						</legend>
						<ul>
							<li class="left" style="width:40%">
								<input id="formMessage" name="confirmation" class="radio" type="radio" value="" checked="checked" onclick="toggleFormRedirect();" />
								<label class="choice" for="formMessage">
									Show Text
								</label>
								<a class="tooltip" title="About Confirmation Message" rel="After your users have successfully submitted an entry, they will be sent to The Confirmation Page, which will display the text you have specified here.">(?)</a>
							</li>
							<li class="right" style="width:60%;text-align:right">
								<input id="formRedirect" name="confirmation" class="radio" type="radio" value="" onclick="toggleFormRedirect('url');">
								<label class="choice" for="formRedirect">
									Redirect to Website
								</label>
								<a class="tooltip" title="About Redirect URL" rel="After your users have successfully submitted an entry, you can redirect them to another 
								website. This will bypass the defaultHoctudau Confirmation Page, and send them directly to a URL 
								of your choice. A paidHoctudau subscription is required to use this feature.">(?)</a>
							</li>
							<li class="clear">
								<textarea class="textarea" rows="10" cols="50" id="formMessageVal" onkeyup="updateForm($F(this), 'RedirectMessage')" onblur="updateForm($F(this), 'RedirectMessage')">
									Thank you. Your entry has been successfully submitted.
								</textarea>
								<input id="formRedirectVal" class="text large hide" type="text" name="text" value="http://" tabindex="10" onkeyup="updateForm($F(this), 'Redirect')" onblur="updateForm($F(this), 'Redirect')" />
								<div class="templating" style="padding:7px 0 2px 0;text-align:center">
									<a href="/docs/templating/" target="_blank">Templating Options</a>
									|
									<a href="/api/code/0/" target="_blank">Template Tags </a>
								</div>
							</li>
						</ul>
						<div class="highlight">
							<input id="formReceipt" class="checkbox" type="checkbox" value="1" onclick="toggleFormConfirmationEmail(this.checked);" />
							<label class="choice" for="formReceipt">
								Send Confirmation Email to User
							</label>
							<a class="tooltip" title="About Email Receipts" rel="Check this off to send an email to the user when their entry is successfully submitted. Requires your form to have an email field.">(?)</a>
							<br />
							<div id="formReceiptDiv" class="hide">
								<div>
									<label class="desc" for="formSendTo">
										Send To
										<a class="tooltip" title="About Send To:" rel="Select the email field you want us to send the receipt to.">(?)</a>
									</label>
									<select id="formSendTo" class="select" onchange="updateProperties('email', 'Validation', this.value);">
									</select>
								</div>
								<div>
									<label class="desc" for="receiptReplyTo">
										Reply To
										<a class="tooltip" title="About Reply To:" rel="Email address you want the users to send messages when they reply to the receipt. If you don't want them to email you, just leave it blank.">(?)</a>
									</label>
									<input id="receiptReplyTo" class="text" type="text" value="" maxlength="255" onkeyup="updateForm(this.value, 'ReceiptReplyTo')" onblur="updateForm(this.value, 'ReceiptReplyTo')" />
								</div>
								<input id="customButton" class="button translate" type="submit" onclick="displayReceiptLightbox();return false;" value="Customize Confirmation Email" />
							</div>
							<!--formReceiptDiv-->
						</div>
						<!--receipt-->
					</fieldset>
				</li>
				<li class="clear noheight">
				</li>
				<li>
					<fieldset class="limitform">
						<legend>
							Limit Form Activity
						</legend>
						<ul>
							<li>
								<label class="desc" for="formCaptcha">
									Captcha
									<a class="tooltip" title="About Captcha" rel="A captcha is a way to make sure your form is being filled out by a human. It works by asking the user to type in two words shown in an image. By default,Hoctudau will automatically show a captcha on your form when it detects abusive behavior. This allows your form to be protected without severely affecting the conversion rate on a form. You can also force a captcha to always show on your form or tellHoctudau to never show the captcha, which is useful if many submissions are coming from the same network.">(?)</a>
								</label>
								<div>
									<select class="select" id="formCaptcha" autocomplete="off" onchange="updateForm($F(this), 'UseCaptcha')">
										<option value="1">
											Auto (Recommended)
										</option>
										<option value="2">
											Always Show
										</option>
										<option value="0">
											Never Show
										</option>
									</select>
								</div>
							</li>
							<li>
								<label class="desc">
									Turn Form Off After
									<a class="tooltip" title="About Total Entries Allowed" rel="This is the total number of entries you want your form to accept. Once your form has collected that many entries, the form will stop accepting submissions. 
									Leave this blank if you want the form to collected unlimited entries.">(?)</a>
								</label>
								<div>
									<span>
										<input id="formEntryLimit" class="text" type="text" value="" maxlength="10" onkeyup="updateForm(this.value, 'EntryLimit')" onblur="updateForm(this.value, 'EntryLimit')" />
									</span>
									<span id="entrySpan">
										Entries
									</span>
								</div>
							</li>
                            <li class="clear noheight">
							</li>
                            <li>
								<label class="desc">
									Limit Submissions/Form Each User
									<a class="tooltip" title="About Total Limit Submission" rel="0 For Unlimit">(?)</a>
								</label>
								<div>
									<span>
										<input id="formUserSubmitLimit" class="text" type="text" value="" maxlength="10" onkeyup="updateForm(this.value, 'UserSubmitLimit')" onblur="updateForm(this.value, 'UserSubmitLimit')" />
									</span>
									<span id="entrySpan">
										Times
									</span>
								</div>
							</li>
							<li class="clear noheight">
							</li>
                            <li class="listAjaxEnable">
								<input id="formAjaxEnable" class="checkbox" type="checkbox" value="" onchange="(this.checked)?updateForm('1', 'AjaxEnable'):updateForm('0','AjaxEnable');autoCountField();" />
								<label class="choice" for="formAjaxEnable">
									Enable Ajax Submit
								</label>
								<a class="tooltip" title="About Ajax Enable" rel="Enable Ajax Submit">(?)</a>
								<br />
							</li>
                            <li class="listAjaxShowWhatJustType">
								<input id="formAjaxShowWhatJustType" class="checkbox" type="checkbox" value="" onchange="(this.checked)?updateForm('1', 'AjaxShowWhatJustType'):updateForm('0','AjaxShowWhatJustType');" />
								<label class="choice" for="formAjaxShowWhatJustType">
									Show What Just Typed
								</label>
								<a class="tooltip" title="About Show What Just Typed" rel="After Submit By Ajax, User Will Saw  What They Just Typed.">(?)</a>
								<br />
							</li>
                            <li class="listAjaxLiveResponse">
								<input id="formAjaxLiveResponse" class="checkbox" type="checkbox" value="" onchange="(this.checked)?updateForm('1', 'AjaxLiveResponse'):updateForm('0','AjaxLiveResponse');" />
								<label class="choice" for="formAjaxLiveResponse">
									Live Response
								</label>
								<a class="tooltip" title="About Ajax Enable" rel="After Submit By Ajax, User Will Saw The Message From Server">(?)</a>
								<br />
							</li>
                            <li class="listUserLogin">
								<input id="formUserLogin" class="checkbox" type="checkbox" value="" onchange="(this.checked)?updateForm('1', 'UserLogin'):updateForm('0','UserLogin');" />
								<label class="choice" for="formUserLogin">
									User Must Loged In To Submit This Form
								</label>
								<a class="tooltip" title="About UserLogin" rel="User Must Login To Submit This Form">(?)</a>
								<br />
							</li>
                            
							<li class="listIP">
								<input id="formIp" class="checkbox" type="checkbox" value="" onchange="(this.checked)?updateForm('1', 'UniqueIP'):updateForm('0','UniqueIP');" />
								<label class="choice" for="formIp">
									Allow Only One Entry Per IP
								</label>
								<a class="tooltip" title="About One Entry per IP" rel="This is used to prevent users from filling out your form more than once.Hoctudau uses the computer's IP address to detect multiple submissions. Note : If the same user were to access your form on another computer with a different internet connection, they could fill out the form again.">(?)</a>
								<br />
							</li>
							<li class="listSchedule">
								<input id="formSchedule" class="checkbox" type="checkbox" value="" onclick="toggleFormActivity(this.checked);" />
								<label class="choice" for="formSchedule">
									Schedule Form Activity
								</label>
								<a class="tooltip" title="About Schedule" rel="If you would like your form to automatically become active or inactive at a certain date, check this box to get setup.">(?)</a>
							</li>
						</ul>
					</fieldset>
				</li>
				<li class="hide" id="listStartDate">
					<label class="desc">
						Start Date
					</label>
					<span>
						<input id="startDate-1" name="startDate-1" class="field text month" size="2" type="text" maxlength="2" value="" onblur="updateForm('live', 'StartDate');" />
						<label for="startDate-1">
							MM
						</label>
					</span>
					<span>
						<input id="startDate-2" name="startDate-2" class="field text days" size="2" type="text" maxlength="2" value="" onblur="updateForm('live', 'StartDate');" />
						<label for="startDate-2">
							DD
						</label>
					</span>
					<span>
						<input id="startDate" name="startDate" class="field text year" size="4" type="text" maxlength="4" onblur="updateForm('live', 'StartDate');" />
						<label for="startDate">
							YYYY
						</label>
					</span>
					<span id="calStart">
						<img id="pickStart" class="icon" src="template/images/icons/calendar.png" alt="Pick date." />
					</span>
					<span>
						<select class="field select" id="startTime-0" name="startTime-0" onchange="updateForm('live', 'StartDate');">
							<option value="1">
								1
							</option>
							<option value="2">
								2
							</option>
							<option value="3">
								3
							</option>
							<option value="4">
								4
							</option>
							<option value="5">
								5
							</option>
							<option value="6">
								6
							</option>
							<option value="7">
								7
							</option>
							<option value="8">
								8
							</option>
							<option value="9">
								9
							</option>
							<option value="10">
								10
							</option>
							<option value="11">
								11
							</option>
							<option value="00" selected="selected">
								12
							</option>
						</select>
						<label for="startTime-0">
							HH
						</label>
					</span>
					<span>
						<select class="field select" id="startTime-1" name="startTime-1" onchange="updateForm('live', 'StartDate');">
							<option value="00">
								00
							</option>
							<option value="15">
								15
							</option>
							<option value="30">
								30
							</option>
							<option value="45">
								45
							</option>
						</select>
						<label for="startTime-1">
							MM
						</label>
					</span>
					<span>
						<select class="field select" id="startTime-2" name="startTime-2" onchange="updateForm('live', 'StartDate');">
							<option value="AM">
								AM
							</option>
							<option value="PM">
								PM
							</option>
						</select>
						<label for="startTime-2">
							AM/PM
						</label>
					</span>
				</li>
				<li class="hide" id="listEndDate">
					<label class="desc">
						End Date
					</label>
					<span>
						<input id="endDate-1" name="endDate-1" class="field text month" size="2" type="text" maxlength="2" value="" onblur="updateForm('live', 'EndDate');" />
						<label for="endDate-1">
							MM
						</label>
					</span>
					<span>
						<input id="endDate-2" name="endDate-2" class="field text days" size="2" type="text" maxlength="2" value="" onblur="updateForm('live', 'EndDate');" />
						<label for="endDate-2">
							DD
						</label>
					</span>
					<span>
						<input id="endDate" name="endDate" class="field text year" size="4" type="text" maxlength="4" value="" onblur="updateForm('live', 'EndDate');" />
						<label for="endDate">
							YYYY
						</label>
					</span>
					<span id="calEnd">
						<img id="pickEnd" class="icon" src="template/images/icons/calendar.png" alt="Pick date." />
					</span>
					<span>
						<select class="field select" id="endTime-0" name="endTime-0" onchange="updateForm('live', 'EndDate');">
							<option value="1">
								1
							</option>
							<option value="2">
								2
							</option>
							<option value="3">
								3
							</option>
							<option value="4">
								4
							</option>
							<option value="5">
								5
							</option>
							<option value="6">
								6
							</option>
							<option value="7">
								7
							</option>
							<option value="8">
								8
							</option>
							<option value="9">
								9
							</option>
							<option value="10">
								10
							</option>
							<option value="11">
								11
							</option>
							<option value="00" selected="selected">
								12
							</option>
						</select>
						<label for="endTime-0">
							HH
						</label>
					</span>
					<span>
						<select class="field select" id="endTime-1" name="endTime-1" onchange="updateForm('live', 'EndDate');">
							<option value="00">
								00
							</option>
							<option value="15">
								15
							</option>
							<option value="30">
								30
							</option>
							<option value="45">
								45
							</option>
						</select>
						<label for="endTime-1">
							MM
						</label>
					</span>
					<span>
						<select class="field select" id="endTime-2" name="endTime-2" onchange="updateForm('live', 'EndDate');">
							<option value="AM">
								AM
							</option>
							<option value="PM">
								PM
							</option>
						</select>
						<label for="endTime-2">
							AM/PM
						</label>
					</span>
				</li>
			</ul>
		</form>
		<!--formProperties-->
	</div>
	<!--side-->
	<div id="main" class="noI ">
		<form id="formPreview" class="hoctudau page1 topLabel" action="#">
			<div id="wuform" class="info" title="Click to edit.">
				<img src="template/images/arrow2.png" alt="" class="arrow" />
				<h2 id="fname" class="notranslate">
					<?php echo isset($_form)?$_form['Name']:'Untitled Form'; ?>
				</h2>
				<div id="fdescription" class="notranslate">
					<?php echo isset($_form)?$_form['Description']:'This is my form. Please fill it out. It\'s awesome!'; ?>
				</div>
			</div>
			<div class="notice" id="nofields" onclick="showFields(true);">
				<h2>
					<b>
						&larr; No Fields!
					</b>
					You should add a field.
				</h2>
				<p>
					This is a
					<strong>
						live preview
					</strong>
					of your form. Currently,
					<strong>
						you don't have any fields
					</strong>
					. Use the buttons under the
					<strong>
						Add a Field
					</strong>
					tab on the left to create inputs for your form. Click on the fields to change their properties.
				</p>
				<div class="promo">
					<img src="template/images/icons/application_form.png" alt="" />
					Try one of our HTML Templates from the
					<a href="/gallery/templates/">HoctudauForm Gallery</a>
					!
				</div>
			</div>
			<div id="nofieldsonpage" class="notice hide">
				<h2 onclick="showFields(true);">
					<b>
						&larr; This page is without fields!
					</b>
					Drag some here.
				</h2>
			</div>
			<ul id="formFields" class="">
				<li id="pageHeader" onclick="selectPageSettings();" class="paging-context hide ">
				</li>
                <?php
                echo $html;
                ?>
			</ul>
			<ul id="forceCaptcha" class="hide">
				<li class="captcha" onclick="selectForm();">
					<label class="desc">
						Type the two words in the image below.
						<span class="req">
							*
						</span>
					</label>
					<img src="template/images/preview/captcha.png" alt="Captcha Field" />
				</li>
			</ul>
		</form>
		<div id="formButtons" class="<?php echo $buttonClass; ?>">
			<table cellspacing="0">
				<tr>
					<td>
						<a href="#" onclick="showFields(true);return false" class="button">
						<img src="template/images/icons/textfield_add.png" alt=""/> Add Field</a>
					</td>
					<td id="pagejump" class="pagejump hide">
						<img id="jumpprev" style="visibility:hidden" src="template/images/icons/smallprev.png" onclick="jumpToPage('prev'); return false;" />
						<input id="jumptarget" name="" class="text" type="text" value="1" />
						<img id="jumpnext" src="template/images/icons/smallnext.png" onclick="jumpToPage('next'); return false;" />
					</td>
					<td>
						<a href="#" id="saveForm" class="button positive">
						<img src="template/images/icons/tick.png" alt=""/> Save Form</a>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!--main-->
</div>
<!--stage-->
</div>