<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<div id="secret">
			</div>
			<form id="controls" action="#">
				<div id="step0">
					<div class="info">
						<div class="buttons">
							<a class="button getStarted dark" href="#" onclick="selectReportSettings();return false"><b>&#x2192;</b> Get Started!</a>
							<a id="hdControls2" class="button dark" href="#" onclick="toggleControls();return false">
							Show Settings</a>
						</div>
						<h2>
							Report Builder
						</h2>
						<div class="notranslate">
							And thus I clothe my naked villany
						</div>
					</div>
				</div>
				<div id="step1" class="step">
					<h2>
						<a title="Report Settings" href="#" onclick="selectReportSettings();return false">
						<strong>1.</strong> <span><b>Report</b> Settings</span> <img src="template/images/rpArrow.png" alt="" /></a>
					</h2>
					<div class="panel">
						<ul class="col1">
							<li>
								<label class="desc" for="reportName">
									Title
									<span class="req">
										*
									</span>
								</label>
								<div>
									<input id="reportName" class="text large" type="text" value="<?php echo isset($_report['Name'])?$_report['Name']:'Untitled Report'; ?>" maxlength="50" onkeyup="updateReport(this, 'Name');" onmouseup="updateReport(this, 'Name');" class="notranslate"/>
								</div>
							</li>
							<li>
								<label class="desc" for="reportDescription">
									Description
								</label>
								<div>
									<textarea id="reportDescription" class="textarea small" rows="10" cols="50" onkeyup="updateReport(this, 'Description');" onmouseup="updateReport(this, 'Description');" class="notranslate">
                                        <?php echo isset($_report['Description'])?$_report['Description']:'This is my report. View it in all its glory!'; ?>
									</textarea>
								</div>
								<div>
									<span>
										<input id="showExport" name="showExport" class="checkbox" type="checkbox" onclick="updateReportAllowExport(this);" />
										<label for="showExport" class="choice">
											Allow User to Export Data
										</label>
									</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<!-- Step 1 -->
				<div id="step2" class="step">
					<h2>
						<a title="Data Settings" href="#" onclick="selectDataSettings();return false">
						<strong>2.</strong> <span><b>Select</b> Data</span> 
						<img src="template/images/rpArrow.png" alt="" /></a>
					</h2>
					<div id="conditions" class="panel ">
						<h3>
							What data would you like to use?
						</h3>
						<ul id="dataSettingsUL">
							<li class="which">
								<span>
									Use
									<select class="select" id="filtered" onblur="toggleConditions(this);" onchange="toggleConditions(this);">
										<option value="hide" selected="selected">
											all entries
										</option>
										<option value="show">
											selected entries
										</option>
									</select>
									from
								</span>
								<span>
									<select class="select notranslate" id="allforms" onblur="changeForm(this);" onchange="changeForm(this);">
                                    <?php
                                    $i=0;
                                    foreach($results as $f) {
                                        //$fc = stripslashes($f['form_content']);
                                        $fc = ($f['form_content']);
                                        $_fc = $this->helpers->json_decode_to_array($fc);
                                    ?>
										<option value="<?php echo $f['form_id']; ?>" <?php if($i==0) echo 'selected="selected"'; ?>>
											<?php echo $_fc['Name']; ?>
										</option>
                                    <?php
                                    $i++;
                                    }
                                    ?>
									</select>
								</span>
								<span id="anyAll" class="anyall">
									that match
									<select class="select" id="anyallSelect" onblur="updateReport(this, 'MatchType');" onchange="updateReport(this, 'MatchType');">
										<option value="all">
											all
										</option>
										<option value="any">
											any
										</option>
									</select>
									of the following conditions :
								</span>
							</li>
							<li id="allConditions" class="hide">
								<span class="fval">
									<select id="standardConditionFieldList" class="select fixed conditionSelect" onblur="conditionChange(this);" onchange="conditionChange(this);">
										<optgroup label="Fields">
                                            <?php
                                            if(!empty($_form['Fields']))
                                            foreach($_form['Fields'] as $ff) :
                                            if(isset($ff['FieldId'])) :
                                            ?>
											<option class="notranslate" value="<?php echo 'Field'.$ff['ColumnId']; ?>">
												<?php echo $ff['Title']; ?>
											</option>
											<?php
                                            endif;
                                            endforeach;
                                            ?>
										</optgroup>
										<optgroup label="Entry Info">
											<option value="EntryId">
												Entry Id
											</option>
											<option value="DateCreated">
												Date Created
											</option>
											<option value="CreatedBy">
												Created By
											</option>
											<option value="DateUpdated">
												Last Updated
											</option>
											<option value="UpdatedBy">
												Updated By
											</option>
											<option value="IP">
												IP Address
											</option>
										</optgroup>
									</select>
								</span>
								<span class="fval">
									<select class="select" onblur="conditionChange(this);" onchange="conditionChange(this);">
										<option value="Contains">
											contains
										</option>
										<option value="Does not contain">
											does not contain
										</option>
										<option value="Begins with">
											begins with
										</option>
										<option value="Ends with">
											ends with
										</option>
										<option value="Is equal to">
											is equal to
										</option>
										<option value="Is not equal to">
											is not equal to
										</option>
									</select>
								</span>
								<span class="fval">
									<input class="text" type="text" onblur="conditionChange(this);" onchange="conditionChange(this);" />
								</span>
								<span class="hide">
									<img class="datepicker" src="template/images/icons/calendar.png" alt="Pick a date." />
								</span>
								<span>
									<img class="button" src="template/images/icons/add.png" alt="Duplicate" onclick="addCondition(this.parentNode.parentNode);" />
									<img class="button deleteButton" src="template/images/icons/delete.png" alt="Delete" onclick="removeCondition(this.parentNode);" />
								</span>
								<div class="hide">
									<select id="basicFilters">
										<option value="Contains">
											contains
										</option>
										<option value="Does not contain">
											does not contain
										</option>
										<option value="Begins with">
											begins with
										</option>
										<option value="Ends with">
											ends with
										</option>
										<option value="Is equal to">
											is equal to
										</option>
										<option value="Is not equal to">
											is not equal to
										</option>
									</select>
									<select id="numberFilters">
										<option value="Is equal to">
											is equal to
										</option>
										<option value="Is greater than">
											is greater than
										</option>
										<option value="Is less than">
											is less than
										</option>
									</select>
									<select id="dateFilters">
										<option value="Is on">
											is on
										</option>
										<option value="Is before">
											is before
										</option>
										<option value="Is after">
											is after
										</option>
									</select>
									<select id="standardOptionsFormData">
										<optgroup label="Entry Info">
											<option value="EntryId">
												Entry ID
											</option>
											<option value="DateCreated">
												Date Created
											</option>
											<option value="CreatedBy">
												Created By
											</option>
											<option value="DateUpdated">
												Last Updated
											</option>
											<option value="UpdatedBy">
												Updated By
											</option>
											<option value="IP">
												IP Address
											</option>
										</optgroup>
									</select>
									<select id="standardOptionsMultiplePagesFormData">
										<optgroup label="Entry Info">
											<option value="EntryId">
												Entry ID
											</option>
											<option value="DateCreated">
												Date Created
											</option>
											<option value="CreatedBy">
												Created By
											</option>
											<option value="DateUpdated">
												Last Updated
											</option>
											<option value="UpdatedBy">
												Updated By
											</option>
											<option value="IP">
												IP Address
											</option>
											<option value="LastPage">
												Last Page Accessed
											</option>
											<option value="CompleteSubmission">
												Completion Status
											</option>
										</optgroup>
									</select>
									<select id="standardOptionsPaymentDataLimited">
										<optgroup label="Payment Info">
											<option value="Status">
												Payment Status
											</option>
											<option value="PurchaseTotal">
												Payment Total
											</option>
										</optgroup>
									</select>
									<select id="standardOptionsPaymentData">
										<optgroup label="Payment Info">
											<option value="Status">
												Payment Status
											</option>
											<option value="PurchaseTotal">
												Payment Total
											</option>
											<option value="Currency">
												Payment Currency
											</option>
											<option value="TransactionId">
												Payment Confirmation
											</option>
											<option value="MerchantType">
												Payment Merchant
											</option>
										</optgroup>
									</select>
								</div>
							</li>
						</ul>
					</div>
					<!--panel-->
				</div>
				<!-- Step 2 -->
				<div id="step3" class="step">
					<h2>
						<a title="Layout Settings" href="#" onclick="selectLayoutSettings();return false">
						<strong>3.</strong> <span><b>Add</b> Widgets</span> <img src="template/images/rpArrow.png" alt="" /></a>
					</h2>
					<div id="pulse" class="panel">
						<h3 class="add">
							<strong>
								Click
							</strong>
							<br />
							or
							<br />
							<strong>
								Drag
							</strong>
							<br />
							<br />
							to add a widget to a zone.
						</h3>
						<div class="col1" id="widgetButtons">
							<div id="layoutGraphWidget" class="dtaGraph dragfld undone" onclick="return false;addWidget('pie')">
								<a href="#" class="button positive" onclick="return false">
								<img class="" src="template/images/icons/chart_pie.png" alt="" /> Graph</a>
							</div>
							<div id="layoutFieldChartWidget" class="dtaChart dragfld" onclick="addWidget('fieldChart')">
								<a href="#" class="button positive" onclick="return false">
								<img src="template/images/icons/chart_bar2.png" alt="" /> Chart</a>
							</div>
							<div id="layoutBigNumberWidget" class="dtaNumber dragfld undone" onclick="return false;addWidget('bigNumber')">
								<a href="#" class="button positive" onclick="return false">
								<img src="template/images/icons/number.gif" alt="" /> Number</a>
							</div>
							<div id="layoutTextWidget" class="dtaText dragfld undone" onclick="return false;addWidget('text')">
								<a href="#" class="button positive" onclick="return false">
								<img src="template/images/icons/text.gif" alt="" /> Text</a>
							</div>
							<div id="layoutGridWidget" class="dtaGrid dragfld undone" onclick="return false;addWidget('grid')">
								<a href="#" class="button positive" onclick="return false">
								<img src="template/images/icons/table.png" alt="" /> Datagrid</a>
							</div>
						</div>
						<h3 class="lay">
							Select Your Layout
						</h3>
						<div class="col2">
							<input id="layout1" name="layout" class="radio" type="radio" value="" onclick="changeLayout('layout1');" <?php echo (isset($_report['Layout']) && $_report['Layout'] == 'layout1')?'checked="checked"':''; ?> />
							<label for="layout1" class="choice l1">
								Layout 1
							</label>
							<input id="layout2" name="layout" class="radio" type="radio" value="" onclick="changeLayout('layout2');" <?php echo (isset($_report['Layout']) && $_report['Layout'] == 'layout2')?'checked="checked"':''; ?> />
							<label for="layout2" class="choice l2">
								Layout 2
							</label>
						</div>
						<!--col1-->
						<div class="col3">
							<input id="layout3" name="layout" class="radio" type="radio" value="" onclick="changeLayout('layout3');" <?php echo (isset($_report['Layout']) && $_report['Layout'] == 'layout3')?'checked="checked"':''; ?> />
							<label for="layout3" class="choice l3">
								Layout 3
							</label>
							<input id="layout4" name="layout" class="radio" type="radio" value="" onclick="changeLayout('layout4');" <?php echo (isset($_report['Layout']) && $_report['Layout'] == 'layout4')?'checked="checked"':''; ?> />
							<label for="layout4" class="choice l4">
								Layout 4
							</label>
						</div>
						<!--col2-->
					</div>
					<!--panel-->
				</div>
				<!--Step 3 -->
				<div id="step4" class="step">
					<h2>
						<a title="Widget Settings" href="#" onclick="selectWidgetSettings();return false">
						<strong>4.</strong> <span><b>Widget</b> Settings</span> <img src="template/images/rpArrow.png" alt="" /></a>
					</h2>
					<div class="panel">
						<div id="nW">
							<h3>
								You haven't added any widgets, partner!
							</h3>
							<p>
								You can add a widget to your report from the
								<a href="#" onclick="selectLayoutSettings();return false">Layout Settings.</a>
							</p>
						</div>
						<div id="sW">
							<h3>
								No Widget Selected
							</h3>
							<p>
								Click on a widget in the report preview
								<br />
								below to change its properties.
							</p>
						</div>
						<div id="gS">
							<h3>
								Graph Settings
							</h3>
							<ul>
								<li id="graphSettingsLi" class="initial">
									<span>
										<input id="graphName" class="text" type="text" value="" maxlength="100" style="width:180px" onkeyup="wsChange(this, 'Name');" onmouseup="wsChange(this, 'Name');" />
										<label>
											Graph Title
										</label>
									</span>
									<span>
										<select id="graphType" class="select translate" style="width:80px" onchange="wsChange(this, 'Typeof', true);" onclick="wsChange(this, 'Typeof', true);">
											<option value="pie">
												Pie
											</option>
											<option value="bar">
												Bar
											</option>
											<option value="line">
												Line
											</option>
										</select>
										<label>
											Type
										</label>
									</span>
									<span class="fval">
										<select id="graphFieldList" class="select" style="width:200px" onchange="wsChange(this, 'FieldName', true);" onclick="wsChange(this, 'FieldName');">
										</select>
										<label>
											Data
										</label>
									</span>
									<span>
										<select id="graphSize" class="select" onchange="wsChange(this, 'Size');" onclick="wsChange(this, 'Size', true);">
											<option value="small">
												Small
											</option>
											<option value="medium">
												Medium
											</option>
											<option value="large">
												Large
											</option>
										</select>
										<label>
											Size
										</label>
									</span>
									<div id="graphDateRangeSection">
										<span>
											<select id="graphDateRange" class="select" style="width:140px" onchange="wsChange(this, 'Detail', true);" onclick="wsChange(this, 'Detail', true);">
												<option value="all">
													All
												</option>
												<option value="">
													&middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot;
												</option>
												<option value="today">
													Today
												</option>
												<option value="week">
													This Week
												</option>
												<option value="month">
													This Month
												</option>
												<option value="year">
													This Year
												</option>
												<option value="">
													&middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot;
												</option>
												<option value="last24HR">
													Last 24 hrs
												</option>
												<option value="last7D">
													Last 7 Days
												</option>
												<option value="last30D">
													Last 30 Days
												</option>
												<option value="last12M">
													Last 12 Months
												</option>
												<option value="">
													&middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot;
												</option>
												<option value="custom">
													Custom
												</option>
											</select>
											<label>
												Date Range
											</label>
										</span>
										<span class="start">
											<b>
												:
											</b>
										</span>
										<span class="start">
											<input id="graphDateRangeStartDate" name="graphDateRangeStartDate" class="text" type="text" value="" onchange="wsChange(this, 'Detail');" onclick="wsChange(this, 'Detail');"/>
											<label>
												Start Date
											</label>
										</span>
										<span class="start">
											<img id="graphStartDateButton" class="datepicker" src="template/images/icons/calendar.png" alt="Pick a date." />
										</span>
										<span class="end">
											&mdash; &nbsp;
										</span>
										<span class="end">
											<input id="graphDateRangeEndDate" name="graphDateRangeEndDate" class="text" type="text" value="" onchange="wsChange(this, 'Detail');" onclick="wsChange(this, 'Detail');"/>
											<label>
												End Date
											</label>
										</span>
										<span class="end">
											<img id="graphEndDateButton" class="datepicker" src="template/images/icons/calendar.png" alt="Pick a date." />
										</span>
									</div>
								</li>
							</ul>
							<div class="hide">
								<span id="graphTypeAllTypesSpan">
									<select id="graphTypeAllTypes" class="select" style="width:80px" onchange="wsChange(this, 'Typeof', true);" onclick="wsChange(this, 'Typeof');">
										<option value="bar">
											Bar
										</option>
										<option value="pie">
											Pie
										</option>
										<option value="line">
											Line
										</option>
									</select>
									<label>
										Type
									</label>
								</span>
								<span id="graphTypeBarAndPieSpan">
									<select id="graphTypeBarAndPie" class="select" style="width:80px" onchange="wsChange(this, 'Typeof', true);" onclick="wsChange(this, 'Typeof');">
										<option value="bar">
											Bar
										</option>
										<option value="pie">
											Pie
										</option>
									</select>
									<label>
										Type
									</label>
								</span>
							</div>
						</div>
						<div id="cS">
							<h3>
								Chart Settings
							</h3>
							<ul>
								<li>
									<span>
										<input id="fieldChartName" name="fieldChartName" class="text" type="text" value="" maxlength="100" style="width:240px" onkeyup="wsChange(this, 'Name');" onmouseup="wsChange(this, 'Name');" />
										<label for="fieldChartName">
											Chart Title
										</label>
									</span>
									<span class="fval">
										<select id="fieldChartFieldList" name="fieldChartFieldList" class="select" style="width:200px" onchange="wsChange(this, 'FieldName', true);" onclick="wsChange(this, 'FieldName', true);">
										</select>
										<label for="fieldChartFieldList">
											Based on this Field
										</label>
									</span>
									<span id="fieldChartDetailDiv">
										<select id="fieldChartDetail" name="fieldChartDetail" class="select" onchange="wsChange(this, 'Detail', true);">
											<option value="both">
												Show All
											</option>
											<option value="percent">
												Show Percentage Only
											</option>
											<option value="count">
												Show Count Only
											</option>
										</select>
										<label for="fieldChartDetail">
											Column Options
										</label>
									</span>
								</li>
						</div>
						<div class="hide">
							<select id="chartOptionsMultiplePages">
								<optgroup label="Entry Info">
									<option value="LastPage">
										Last Page Accessed
									</option>
									<option value="CompleteSubmission">
										Completion Status
									</option>
								</optgroup>
							</select>
						</div>
						<div id="dS">
							<h3>
								Datagrid Settings
							</h3>
							<div class="custom">
								<div class="clearfix">
									<span class="left">
										<select id="gridPageSize" name="gridPageSize" class="select" onchange="wsGridChange(this, 'PageSize');" onclick="wsGridChange(this, 'PageSize');">
											<option value="1">
												Show 1 Entry per Page
											</option>
											<option value="10" selected="selected">
												Show 10 Entries per Page
											</option>
											<option value="25">
												Show 25 Entries per Page
											</option>
											<option value="50">
												Show 50 Entries per Page
											</option>
											<option value="100">
												Show 100 Entries per Page
											</option>
										</select>
									</span>
									<span id="customizeGrid" class="right">
										<input id="gridIsEditable" name="gridIsEditable" class="" type="checkbox" onclick="wsGridChange(this, 'IsEditable');" />
										<label for="gridIsEditable">
											<b>
												Allow User to View Columns Not Checked
											</b>
										</label>
										<a class="tooltip" title="About this Setting" rel="This will allow your users to view other fields in the datagrid 
										that you haven't checked to be initially shown above. This will provide additional flexibility to your 
										users viewing the reports without having to clutter the datagrid with every column on load. However, if there's 
										certain data from your form you don't want to share, leave this checked off to keep only the columns you've specified
										above to show in your report.">(?)</a>
									</span>
								</div>
							</div>
							<ul id="allGridCheckBoxes">
								<li id="gridSortBy" class="sortby">
									<span>
										<b>
											Sort By :
										</b>
									</span>
									<span>
										<select id="gridFieldList" name="gridFieldList" class="select" style="width:300px" onchange="wsGridChange(this, 'FieldName');" onclick="wsGridChange(this, 'FieldName');">
										</select>
									</span>
									<span>
										<select id="gridAscDesc" name="gridAscDesc" class="select" onchange="wsGridChange(this, 'Detail');" onclick="wsGridChange(this, 'Detail');">
											<option value="ASC">
												Ascending
											</option>
											<option value="DESC">
												Descending
											</option>
										</select>
									</span>
								</li>
								<li class="threeColumns">
									<div id="gridCheckboxes">
									</div>
								</li>
								<li id="gridCheckboxesEntryInfo" class="threeColumns">
									<label class="desc">
										Entry Info
									</label>
									<div>
										<span>
											<input onclick="wsGridChange(this, 'ShowColumns', true)" id="EntryId" name="EntryId" class="checkbox showCol" type="checkbox" value="EntryId" />
											<label class="choice" for="EntryId">
												Entry ID
											</label>
										</span>
										<span>
											<input onclick="wsGridChange(this, 'ShowColumns', true)" id="DateCreated" name="DateCreated" class="checkbox showCol" type="checkbox" value="DateCreated" />
											<label class="choice" for="DateCreated">
												Date Created
											</label>
										</span>
										<span>
											<input onclick="wsGridChange(this, 'ShowColumns', true)" id="CreatedBy" name="CreatedBy" class="checkbox showCol" type="checkbox" value="CreatedBy" />
											<label class="choice" for="CreatedBy">
												Created By
											</label>
										</span>
										<span>
											<input onclick="wsGridChange(this, 'ShowColumns', true)" id="DateUpdated" name="DateUpdated" class="checkbox showCol" type="checkbox" value="DateUpdated" />
											<label class="choice" for="DateUpdated">
												Last Updated
											</label>
										</span>
										<span>
											<input onclick="wsGridChange(this, 'ShowColumns', true)" id="UpdatedBy" name="UpdatedBy" class="checkbox showCol" type="checkbox" value="UpdatedBy" />
											<label class="choice" for="UpdatedBy">
												Updated By
											</label>
										</span>
										<span>
											<input onclick="wsGridChange(this, 'ShowColumns', true)" id="IP" name="IP" class="checkbox showCol" type="checkbox" value="IP" />
											<label class="choice" for="IP">
												IP Address
											</label>
										</span>
										<span id="lastPageGridCheckbox" class="hide">
											<input onclick="wsGridChange(this, 'ShowColumns', true)" id="LastPage" name="LastPage" class="checkbox showCol" type="checkbox" value="LastPage" />
											<label class="choice" for="LastPage">
												Last Page Accessed
											</label>
										</span>
										<span id="completeSubmissionGridCheckbox" class="hide">
											<input onclick="wsGridChange(this, 'ShowColumns', true)" id="CompleteSubmission" name="CompleteSubmission" class="checkbox showCol" type="checkbox" value="CompleteSubmission" />
											<label class="choice" for="CompleteSubmission">
												Completion Status
											</label>
										</span>
									</div>
								</li>
								<li id="gridCheckboxesPaymentInfo" class="twoColumns hide">
									<label class="desc">
										Payment Info
									</label>
									<div>
										<span>
											<input onclick="wsGridChange(this, 'ShowColumns', true)" id="Status" name="Status" class="checkbox showCol" type="checkbox" value="Status" />
											<label class="choice" for="Status">
												Payment Status
											</label>
										</span>
										<span>
											<input onclick="wsGridChange(this, 'ShowColumns', true)" id="PurchaseTotal" name="PurchaseTotal" class="checkbox showCol" type="checkbox" value="PurchaseTotal" />
											<label class="choice" for="PurchaseTotal">
												Payment Total
											</label>
										</span>
										<span>
											<input onclick="wsGridChange(this, 'ShowColumns', true)" id="Currency" name="Currency" class="checkbox showCol" type="checkbox" value="Currency" />
											<label class="choice" for="Currency">
												Payment Currency
											</label>
										</span>
										<span>
											<input onclick="wsGridChange(this, 'ShowColumns', true)" id="TransactionId" name="TransactionId" class="checkbox showCol" type="checkbox" value="TransactionId" />
											<label class="choice" for="TransactionId" title="TransactionId">
												Payment Confirmation
											</label>
										</span>
										<span>
											<input onclick="wsGridChange(this, 'ShowColumns', true)" id="MerchantType" name="MerchantType" class="checkbox showCol" type="checkbox" value="MerchantType" />
											<label class="choice" for="MerchantType">
												Payment Merchant
											</label>
										</span>
									</div>
								</li>
							</ul>
						</div>
						<div id="nS" class="clearfix translate">
							<h3>
								Number Settings
							</h3>
							<ul class="col1">
								<li>
									<div>
										<input id="bigNumberName" class="text" type="text" maxlength="50" onkeyup="wsChange(this, 'Name');" onmouseup="wsChange(this, 'Name');"/>
										<label>
											Title
										</label>
									</div>
									<div>
										<input id="bigNumberDescription" class="text" type="text" maxlength="50" onkeyup="wsChange(this, 'Description');" onmouseup="wsChange(this, 'Description');"/>
										<label>
											Description
										</label>
									</div>
									<div class="fval gOnly last">
										<select id="bigNumberFieldList">
										</select>
										<label>
											Data
										</label>
									</div>
								</li>
							</ul>
							<ul class="col2">
								<li>
									<div>
										<select id="bigNumberCalculation" class="select">
										</select>
										<label>
											Calculation
										</label>
									</div>
									<div>
										<select id="bigNumberFormat" class="select">
										</select>
										<label>
											Number Format
										</label>
									</div>
									<div class="last">
										<select id="bigNumberCurrency" class="select" onclick="wsChange(this, 'Format');" onchange="wsChange(this, 'Format');">
											<option value="dollars">
												$ Dollars
											</option>
											<option value="pounds">
												&pound; Pounds
											</option>
											<option value="euros">
												&euro; Euros
											</option>
											<option value="yen">
												&yen; Yen / Yuan
											</option>
										</select>
										<label>
											Currency Format
										</label>
									</div>
								</li>
							</ul>
							<div class="col3">
								<div id="bigNumberColorDiv">
									<a id="bigNumberColor1" href="#" value="color1" onclick="wsChange(this, 'Color', true);return false" class="swatch color1"><b>x</b></a>
									<a id="bigNumberColor2" href="#" value="color2" onclick="wsChange(this, 'Color', true);return false" class="swatch color2"><b>x</b></a>
									<a id="bigNumberColor3" href="#" value="color3" onclick="wsChange(this, 'Color', true);return false" class="swatch color3"><b>x</b></a>
									<a id="bigNumberColor4" href="#" value="color4" onclick="wsChange(this, 'Color', true);return false" class="swatch color4"><b>x</b></a>
									<a id="bigNumberColor5" href="#" value="color5" onclick="wsChange(this, 'Color', true);return false" class="swatch color5"><b>x</b></a>
									<a id="bigNumberColor6" href="#" value="color6" onclick="wsChange(this, 'Color', true);return false" class="swatch color6"><b>x</b></a>
									<a id="bigNumberColor7" href="#" value="color7" onclick="wsChange(this, 'Color', true);return false" class="swatch color7"><b>x</b></a>
									<a id="bigNumberColor8" href="#" value="color0" onclick="wsChange(this, 'Color', true);return false" class="swatch color0"><b>x</b></a>
								</div>
								<label>
									Color
								</label>
							</div>
						</div>
						<!-- Template divs -->
						<div class="hide translate">
							<select id="bigNumberAdditionalOptions">
								<optgroup label="Form Stats">
									<option value="entries">
										Entries
									</option>
									<option value="views">
										Views
									</option>
									<option value="bounceRate">
										Bounce Rate
									</option>
									<option value="conversionRate">
										Conversion Rate
									</option>
								</optgroup>
							</select>
							<select id="bigNumberPaymentOptions">
								<optgroup label="Payment Info">
									<option value="Paid">
										Paid
									</option>
									<option value="Unpaid">
										Unpaid
									</option>
									<option value="Pending">
										Pending
									</option>
									<option value="Cancelled">
										Cancelled
									</option>
									<option value="Refunded">
										Refunded
									</option>
								</optgroup>
							</select>
							<select id="bigNumberFormats">
								<option value="normal">
									Normal &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; &#160;&#160;&#160;&#160;&#160;&#160;&#160; 1,234
								</option>
								<option value="noformat">
									No Format &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; &#160;&#160;&#160; 1234
								</option>
								<option disabled="disabled">
									&middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot;
								</option>
								<option value="rounded">
									Rounded &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 1,235
								</option>
								<option value="decimal">
									2 Decimals &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; &#160;&#160; 1,234.56
								</option>
								<option value="scientific">
									Scientific &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; &#160;&#160;&#160;&#160; 1.23E+03
								</option>
								<option disabled="disabled">
									&middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot;
								</option>
								<option value="currency">
									Currency &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; &#160;&#160; $1,234.56
								</option>
								<option value="currencyround">
									Currency Rounded &#160;&#160;&#160;&#160; $1,235
								</option>
								<option disabled="disabled">
									&middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot;
								</option>
								<option value="percent">
									Percent &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 12.34%
								</option>
								<option value="percentround">
									Percent Rounded &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 12%
								</option>
							</select>
							<select id="bigNumberFormatsNoPercent">
								<option value="normal">
									Normal &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; &#160;&#160;&#160;&#160;&#160;&#160;&#160; 1,234
								</option>
								<option value="noformat">
									No Format &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; &#160;&#160;&#160; 1234
								</option>
								<option disabled="disabled">
									&middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot;
								</option>
								<option value="rounded">
									Rounded &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 1,235
								</option>
								<option value="decimal">
									2 Decimals &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; &#160;&#160; 1,234.56
								</option>
								<option value="scientific">
									Scientific &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; &#160;&#160;&#160;&#160; 1.23E+03
								</option>
								<option disabled="disabled">
									&middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot; &middot;
								</option>
								<option value="currency">
									Currency &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; &#160;&#160; $1,234.56
								</option>
								<option value="currencyround">
									Currency Rounded &#160;&#160;&#160;&#160; $1,235
								</option>
							</select>
							<select id="bigNumberPercentOnlyFormats">
								<option value="percent">
									Percent &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 12.34%
								</option>
								<option value="percentround">
									Percent Rounded &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; 12%
								</option>
							</select>
							<select id="bigNumberCalculations">
								<option value="cnt">
									Count
								</option>
								<option value="sum">
									Sum
								</option>
								<option value="avg">
									Average
								</option>
								<option value="min">
									Minimum
								</option>
								<option value="max">
									Maximum
								</option>
								<option value="mod">
									Most Popular
								</option>
							</select>
							<select id="bigNumberCountOnlyCalculations">
								<option value="cnt">
									Count
								</option>
							</select>
						</div>
						<div id="tS">
							<h3>
								Text!
							</h3>
							<textarea id="textDescription" class="textarea small" rows="10" cols="50" onkeyup="wsChange(this, 'Description');" onmouseup="wsChange(this, 'Description');">
							</textarea>
						</div>
					</div>
					<!--panel-->
				</div>
				<!-- Step 4 -->
			</form>
			<!--controls-->
		</div>
		<!--container-->
		<div id="preview" class="hoctudau <?php  if(isset($_report['Layout'])) { echo $_report['Layout']; } ?>">
			<div id="container2" class="<?php empty($_report['Graphs'])?'noWidgets':''; ?>">
				<h1 class="logo">
					<a><img src="template/images/htdlogo.png" /></a>
				</h1>
				<div id="info2" onmouseover="try{toggleZones();}catch(e){}" onmouseout="try{toggleZones();}catch(e){}">
					<div class="info2" onclick="selectReportSettings()">
						<div id="previewButtons" class="buttons">
							<a id="exportButton" class="button export" title="Export Button" href="#" onclick="return false" style="display:none">
							<img src="template/images/icons/table_save.png" alt=""/> Export Data</a>
						</div>
						<!--buttons-->
						<h2 id="reportPreviewName" class="notranslate">
							<?php echo isset($_report['Name'])?$_report['Name']:'Untitled Report'; ?>
						</h2>
						<div id="reportPreviewDescription" class="notranslate">
							<?php echo isset($_report['Description'])?$_report['Description']:'This is my report. View it in all its glory!'; ?>
						</div>
					</div>
				</div>
				<!--info2-->
				<div id="stage2" class="clearfix">
					<div id="noWidgets" class="notice" onclick="selectLayoutSettings();">
						<h3>
							<b>
								No Widgets!
							</b>
							You should add some.
						</h3>
						<p>
							This is a
							<strong>
								simulated preview
							</strong>
							of your report. Currently,
							<strong>
								you don't have any widgets
							</strong>
							. Use the buttons in the
							<strong>
								Layout Settings
							</strong>
							above to add visual elements for your report. Click on the widgets to change their properties.
						</p>
					</div>
                    <?php
                    $z1 = $z2 = $z3 = $html = '';
                    
                    if(!empty($_report['Graphs'])) {
                    //aasort($_report['Graphs'],'Position');
                    foreach($_report['Graphs'] as $graph) {
                        $html = '';
                        ob_start();
                        ?>
                        <div id="widget_preview_<?php echo $graph['GraphId']; ?>" class="wfo_widget fc" onmousedown="pickWidget(this)" title="Click to edit. Drag to reorder."> <table cellspacing="0" class="choices "> <caption> <h4 id="widget_preview_<?php echo $graph['GraphId']; ?>_fieldChart_title" class="notranslate"> <?php echo $graph['Name']; ?> </h4> <span class="fcNav"> <a>&nbsp;</a> </span> </caption> <thead> <tr> <th> Choices </th> <th class="percent"> Percentage </th> <th class="count"> Count </th> </tr> </thead> <tfoot> <tr> <td class="empty"> &nbsp; </td> <th> Total </th> <td class="count"> 100 </td> </tr> <tr> <td class="empty"> &nbsp; </td> <th> <em> Unanswered </em> </th> <td class="count"> <em> 10 </em> </td> </tr> </tfoot> <tbody> <tr class="color1"> <th> <div> &nbsp; </div> </th> <td class="percent"> <div class="bar" style="width:50%"> </div> <var> 50% </var> </td> <td class="count"> 50 </td> </tr> <tr class="color2 alt"> <th> <div> &nbsp; </div> </th> <td class="percent"> <div class="bar" style="width:25%"> </div> <var> 25% </var> </td> <td class="count"> 25 </td> </tr> <tr class="color3"> <th> <div> &nbsp; </div> </th> <td class="percent"> <div class="bar" style="width:10%"> </div> <var> 10% </var> </td> <td class="count"> 10 </td> </tr> <tr class="color4 alt"> <th> <div> &nbsp; </div> </th> <td class="percent"> <div class="bar" style="width:5%"> </div> <var> 5% </var> </td> <td class="count"> 5 </td> </tr> <tr class="color5"> <th> <div> &nbsp; </div> </th> <td class="percent"> <div class="bar" style="width:2%"> </div> <var> 2% </var> </td> <td class="count"> 2 </td> </tr> <tr class="color0 alt"> <th> <div> &nbsp; </div> </th> <td class="percent"> <div class="bar" style="width:8%"> </div> <var> 8% </var> </td> <td class="count"> 8 </td> </tr> </tbody> </table> <div id="wa<?php echo $graph['GraphId']; ?>" class="widgetActions"> <img class="waDup" src="template/images/icons/add.png" alt="Duplicate." title="Duplicate" onclick="duplicateWidget(this); return false" /> <img class="waDel" src="template/images/icons/delete.png" alt="Delete." title="Delete" onclick="deleteWidget(this); return false" /> </div> </div>
                        <?php
                        $html = ob_get_contents();
                        ob_clean();
                        if($graph['Zone'] == 1) {
                            $z1 .= $html;
                        } elseif($graph['Zone'] == 2) {
                            $z2 .= $html;
                        } elseif($graph['Zone'] == 3) {
                            $z3 .= $html;
                        }
                    }
                    }
                    ?>
					<div id="z1" class="zone clearfix target" onclick="targetZone(this);" title="Drag a widget into this zone to add it to the report.">
                    <?php echo $z1; ?></div>
					<div id="z2" class="zone clearfix " onclick="targetZone(this);" title="Drag a widget into this zone to add it to the report.">
					<?php echo $z2; ?></div>
					<div id="z3" class="zone clearfix " onclick="targetZone(this);" title="Drag a widget into this zone to add it to the report.">
					<?php echo $z3; ?></div>
					<div class="footer2">
						<h4 id="reportPreviewTimestamp" class="notranslate">
							<?php echo isset($_report['Name'])?$_report['Name']:'Untitled Report'; ?>
						</h4>
						<span class="notranslate">
							<?php ;echo isset($report_result['report_create'])?$report_result['report_create']:$this->helpers->get_datetime(); ?>
						</span>
					</div>
					<!--footer2-->
				</div>
				<!--stage2-->
			</div>
			<!--container2-->
		</div>
		<!--preview-->
		<div id="dash" class="clearfix">
			<table cellspacing="0">
				<tr>
					<td class="l">
						<a id="addButton" class="dark" href="#" onclick="selectLayoutSettings();return false">
						<img src="template/images/icons/add.png" alt=""/> Add Another Widget</a>
					</td>
					<td>
						<a id="saveReport" class="darkPos" href="#" onclick="save(); return false">Save Report</a>
					</td>
					<td class="r">
						<a id="hdControls" class="dark" href="#" onclick="toggleControls();return false">
						<span class="hi">Expand Preview</span><span class="sh">Show Settings</span> </a>
					</td>
				</tr>
			</table>