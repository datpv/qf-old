<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<div id="styleChanger" class="clearfix">
<div class="info">
	<div class="buttons">
		<a id="saveBtn" href="#" class="button positive " onclick="saveTheme();return false">Save Theme</a>
		<span id="otherButtons">
			<a href="#" class="button" onclick="renameTheme();return false">Rename</a>
			<a href="#" class="button" onclick="duplicateTheme();return false">Duplicate</a>
			<a href="#" class="button negative" onclick="deleteTheme();return false">Delete</a>
		</span>
	</div>
	<h2>
		Theme Designer
	</h2>
	<div class="notranslate">
		Such seems your beauty still.
	</div>
</div>
<form id="themeManager" action="../themes/submit.php" target="submit_form_here" method="post">
	<div id="themes" class="left">
		<label for="themeMenu" class="desc">
			Theme
		</label>
		<select id="themeMenu" name="themeMenu" class="select" onchange="selectTheme($F(this))">
			<option>
				Loading Themes...
			</option>
		</select>
	</div>
	<div id="first" class="bracket left">
	</div>
	<div id="properties" class="left">
		<label for="propertiesMenu" class="desc">
			Properties
		</label>
		<select id="propertiesMenu" name="propertiesMenu" size="7" class="select" onchange="selectProperties($F(this))">
			<option value="1">
				Logo
			</option>
			<option value="3">
				Backgrounds
			</option>
			<option value="2">
				Typography
			</option>
			<option value="4">
				Borders
			</option>
			<option value="5">
				Shadows
			</option>
			<option value="6">
				Buttons
			</option>
			<option value="7">
				Advanced
			</option>
		</select>
	</div>
	<div id="typography" class="left">
		<label for="typographyMenu" class="desc">
			Typography
		</label>
		<select id="typographyMenu" name="typographyMenu" size="7" class="select" onchange="selectTypography($F(this))">
			<option value="ftFormTitle">
				Title
			</option>
			<option value="ftFormDescription">
				Description
			</option>
			<option value="ftSectionTitle">
				Section Title
			</option>
			<option value="ftSectionText">
				Section Text
			</option>
			<option value="ftFieldTitle">
				Field Title
			</option>
			<option value="ftFieldText">
				Field Text
			</option>
			<option value="ftInstruct">
				Instructions
			</option>
		</select>
	</div>
	<div id="backgrounds" class="left">
		<label for="backgroundsMenu" class="desc">
			Backgrounds
		</label>
		<select id="backgroundsMenu" size="7" class="select" onchange="selectBackgrounds($F(this))">
			<option value="bgHtml">
				Wallpaper
			</option>
			<option value="bgLogo">
				Header
			</option>
			<option value="bgForm">
				Form
			</option>
			<option value="bgField">
				Field
			</option>
			<option value="bgHighlight">
				Highlight
			</option>
			<option value="bgInstruct">
				Instructions
			</option>
		</select>
	</div>
	<div id="borders" class="left">
		<label for="bordersMenu" class="desc">
			Borders
		</label>
		<select id="bordersMenu" size="7" class="select" onchange="selectBorders($F(this))">
			<option value="brForm">
				Form
			</option>
			<option value="brDivider">
				Sections
			</option>
			<option value="brInstruct">
				Instructions
			</option>
		</select>
	</div>
	<div id="second" class="bracket left">
	</div>
	<div id="shadowMenu" class="left">
		<label for="shadowMenu" class="desc">
			Shadows
		</label>
		<select name="shadowMenu" size="7" class="select" onchange="selectShadow($F(this))">
			<option value="visible">
				On
			</option>
			<option value="hidden">
				Off
			</option>
		</select>
	</div>
	<div id="logoMenu" class="left">
		<label for="logoMenu" class="desc">
			Logo
		</label>
		<select name="logoMenu" size="7" class="select" onchange="selectLogo($F(this))">
			<option value="template/images/htdlogo.png">
				Hoctudau!
			</option>
			<option value="custom">
				Your Logo &rarr;
			</option>
			<option value="template/images/themes/logos/none.png">
				None
			</option>
			<option value="template/images/themes/logos/none.png">
				-------------
			</option>
			<option value="template/images/themes/logos/robot.png">
				Robotronicon
			</option>
			<option value="template/images/themes/logos/ninja.png">
				Poised Ninja
			</option>
			<option value="template/images/themes/logos/shark.png">
				Shark Attack
			</option>
			<option value="template/images/themes/logos/dinosaur.png">
				Dinosaur Buddy
			</option>
			<option value="template/images/themes/logos/monkey.png">
				Friendly Monkey
			</option>
			<option value="template/images/themes/logos/pirate.png">
				Pirate Face
			</option>
			<option value="template/images/themes/logos/star.png">
				Star Power
			</option>
			<option value="template/images/themes/logos/heart.png">
				Heart!
			</option>
			<option value="template/images/themes/logos/butterfly.png">
				Butterfly Master
			</option>
			<option value="template/images/themes/logos/unicorn.png">
				Unicorn Strike
			</option>
			<option value="template/images/themes/logos/flower.png">
				Pretty Flower
			</option>
			<option value="template/images/themes/logos/envelope.png" title="By Bulius.com">
				Envelope
			</option>
			<option value="template/images/themes/logos/cake.png" title="By Bulius.com">
				Cake
			</option>
			<option value="template/images/themes/logos/martini.png" title="By Bulius.com">
				Martini
			</option>
			<option value="template/images/themes/logos/guitar.png" title="By Bulius.com">
				Guitar
			</option>
			<option value="template/images/themes/logos/starstripes.png" title="By Bulius.com">
				Stars Stripes
			</option>
			<option value="template/images/themes/logos/cat.png" title="By Bulius.com">
				Kitty!
			</option>
			<option value="template/images/themes/logos/killerwhale.png" title="By Bulius.com">
				Killer Whale
			</option>
			<option value="template/images/themes/logos/panda.png" title="By Bulius.com">
				Panda
			</option>
			<option value="template/images/themes/logos/whale.png" title="By Bulius.com">
				Whale
			</option>
			<option value="template/images/themes/logos/butterfly2.png" title="By Bulius.com">
				Butterfly Sequel
			</option>
			<option value="template/images/themes/logos/ladybug.png" title="By Bulius.com">
				Ladybug
			</option>
			<option value="template/images/themes/logos/risingsun.png" title="By Bulius.com">
				Rising Sun
			</option>
			<option value="template/images/themes/logos/shuriken.png" title="By Bulius.com">
				Shuriken!
			</option>
			<option value="template/images/themes/logos/shurikenpattern.png" title="By Bulius.com">
				Many Shurikens!
			</option>
			<option value="template/images/themes/logos/katana.png" title="By Bulius.com">
				Katana Blade
			</option>
		</select>
	</div>
	<div id="fontMenu" class="left">
		<label class="desc">
			Font
		</label>
		<div>
			<a id="selectFont" onclick="showFontPickerLB(); return false;" href="#">
			<b id="selectFontExample">AaZz</b> <span id="selectFontType">Default</span>
			</a>
			<select name="fontMenu" size="7" class="select hide notranslate" onchange="selectFont($F(this))">
				<option value="inherit">
					Default
				</option>
				<option value="Arial">
					Arial
				</option>
				<option value="Courier New">
					Courier
				</option>
				<option value="Georgia">
					Georgia
				</option>
				<option value="Trebuchet MS">
					Trebuchet
				</option>
				<option value="Times New Roman">
					Times
				</option>
				<option value="Tahoma">
					Tahoma
				</option>
				<option value="Verdana">
					Verdana
				</option>
			</select>
		</div>
	</div>
	<div id="formatMenu" class="left">
		<label for="formatMenuDD" class="desc">
			Style
		</label>
		<select id="formatMenuDD" name="formatMenuDD" size="7" class="select" onchange="selectFormat($F(this))">
			<option value="normal" class="">
				Normal
			</option>
			<option value="bold" class="">
				Bold
			</option>
			<option value="italic" class="">
				Italic
			</option>
		</select>
	</div>
	<div id="sizeMenu" class="left">
		<label for="sizeMenu" class="desc">
			Size
		</label>
		<select name="sizeMenu" size="7" class="select" onchange="selectSize($F(this))">
			<option value="80%">
				8
			</option>
			<option value="85%">
				9
			</option>
			<option value="95%">
				10
			</option>
			<option value="100%">
				11
			</option>
			<option value="110%">
				12
			</option>
			<option value="140%">
				14
			</option>
			<option value="160%">
				16
			</option>
			<option value="180%">
				18
			</option>
			<option value="240%">
				24
			</option>
			<option value="280%">
				28
			</option>
			<option value="320%">
				32
			</option>
			<option value="360%">
				36
			</option>
			<option value="400%">
				40
			</option>
			<option value="450%">
				45
			</option>
			<option value="640%">
				64
			</option>
		</select>
	</div>
	<div id="thickMenu" class="left">
		<label for="thickMenu" class="desc">
			Thickness
		</label>
		<select name="thickMenu" size="7" class="select" onchange="selectThickness($F(this))">
			<option value="0">
				None
			</option>
			<option value="1px">
				Hairline
			</option>
			<option value="3px">
				Thin
			</option>
			<option value="5px">
				Medium
			</option>
			<option value="10px">
				Thick
			</option>
		</select>
	</div>
	<div id="styleMenu" class="left">
		<label for="styleMenu" class="desc">
			Style
		</label>
		<select name="styleMenu" size="7" class="select" onchange="selectStyle($F(this))">
			<option value="solid">
				Solid
			</option>
			<option value="dotted">
				Dotted
			</option>
			<option value="dashed">
				Dashed
			</option>
			<option value="double">
				Double
			</option>
		</select>
	</div>
	<div id="patternMenu" class="left">
		<label class="desc">
			Pattern
		</label>
		<ul id="patternList">
			<li id="patternColor" style="background-image:none">
				Solid Color &rarr;
			</li>
			<li id="customPattern">
				Custom Pattern &rarr;
			</li>
			<li class="p" style="background:url(../images/background.gif)" title="url(../images/background.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/oldhtd.png)" title="url(../images/themes/patterns/oldhtd.png)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/fernWH.png)" title="url(../images/themes/patterns/fernWH.png)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/fernBL.png)" title="url(../images/themes/patterns/fernBL.png)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid1.gif);" title="url(../images/themes/patterns/squid1.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid2.gif);" title="url(../images/themes/patterns/squid2.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid3.gif);" title="url(../images/themes/patterns/squid3.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid4.gif);" title="url(../images/themes/patterns/squid4.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid5.gif);" title="url(../images/themes/patterns/squid5.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid6.gif);" title="url(../images/themes/patterns/squid6.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid7.gif);" title="url(../images/themes/patterns/squid7.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid8.gif);" title="url(../images/themes/patterns/squid8.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid9.gif);" title="url(../images/themes/patterns/squid9.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid10.gif);" title="url(../images/themes/patterns/squid10.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid11.gif);" title="url(../images/themes/patterns/squid11.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid12.gif);" title="url(../images/themes/patterns/squid12.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid13.gif);" title="url(../images/themes/patterns/squid13.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid14.gif);" title="url(../images/themes/patterns/squid14.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid15.gif);" title="url(../images/themes/patterns/squid15.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid16.gif);" title="url(../images/themes/patterns/squid16.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid17.gif);" title="url(../images/themes/patterns/squid17.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid18.gif);" title="url(../images/themes/patterns/squid18.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid19.gif);" title="url(../images/themes/patterns/squid19.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid20.gif);" title="url(../images/themes/patterns/squid20.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid21.gif);" title="url(../images/themes/patterns/squid21.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid22.gif);" title="url(../images/themes/patterns/squid22.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid23.gif);" title="url(../images/themes/patterns/squid23.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid24.gif);" title="url(../images/themes/patterns/squid24.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid25.gif);" title="url(../images/themes/patterns/squid25.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid26.gif);" title="url(../images/themes/patterns/squid26.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid27.gif);" title="url(../images/themes/patterns/squid27.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid28.gif);" title="url(../images/themes/patterns/squid28.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid29.gif);" title="url(../images/themes/patterns/squid29.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid30.gif);" title="url(../images/themes/patterns/squid30.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid31.gif);" title="url(../images/themes/patterns/squid31.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid32.gif);" title="url(../images/themes/patterns/squid32.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid33.gif);" title="url(../images/themes/patterns/squid33.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid34.gif);" title="url(../images/themes/patterns/squid34.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid35.gif);" title="url(../images/themes/patterns/squid35.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid36.gif);" title="url(../images/themes/patterns/squid36.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid37.gif);" title="url(../images/themes/patterns/squid37.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid38.gif);" title="url(../images/themes/patterns/squid38.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid39.gif);" title="url(../images/themes/patterns/squid39.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid40.gif);" title="url(../images/themes/patterns/squid40.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid41.gif);" title="url(../images/themes/patterns/squid41.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid42.gif);" title="url(../images/themes/patterns/squid42.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid43.gif);" title="url(../images/themes/patterns/squid43.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid44.gif);" title="url(../images/themes/patterns/squid44.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid45.gif);" title="url(../images/themes/patterns/squid45.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid46.gif);" title="url(../images/themes/patterns/squid46.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid47.gif);" title="url(../images/themes/patterns/squid47.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid48.gif);" title="url(../images/themes/patterns/squid48.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid49.gif);" title="url(../images/themes/patterns/squid49.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid50.gif);" title="url(../images/themes/patterns/squid50.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid51.gif);" title="url(../images/themes/patterns/squid51.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid52.gif);" title="url(../images/themes/patterns/squid52.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid53.gif);" title="url(../images/themes/patterns/squid53.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid54.gif);" title="url(../images/themes/patterns/squid54.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid55.gif);" title="url(../images/themes/patterns/squid55.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid56.gif);" title="url(../images/themes/patterns/squid56.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid57.gif);" title="url(../images/themes/patterns/squid57.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid58.gif);" title="url(../images/themes/patterns/squid58.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid59.gif);" title="url(../images/themes/patterns/squid59.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid60.gif);" title="url(../images/themes/patterns/squid60.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid61.gif);" title="url(../images/themes/patterns/squid61.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid62.gif);" title="url(../images/themes/patterns/squid62.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid63.gif);" title="url(../images/themes/patterns/squid63.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid64.gif);" title="url(../images/themes/patterns/squid64.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid65.gif);" title="url(../images/themes/patterns/squid65.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid66.gif);" title="url(../images/themes/patterns/squid66.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid67.gif);" title="url(../images/themes/patterns/squid67.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid68.gif);" title="url(../images/themes/patterns/squid68.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid69.gif);" title="url(../images/themes/patterns/squid69.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid70.gif);" title="url(../images/themes/patterns/squid70.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid71.gif);" title="url(../images/themes/patterns/squid71.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid72.gif);" title="url(../images/themes/patterns/squid72.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid73.gif);" title="url(../images/themes/patterns/squid73.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid74.gif);" title="url(../images/themes/patterns/squid74.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid75.gif);" title="url(../images/themes/patterns/squid75.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid76.gif);" title="url(../images/themes/patterns/squid76.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid77.gif);" title="url(../images/themes/patterns/squid77.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid78.gif);" title="url(../images/themes/patterns/squid78.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid79.gif);" title="url(../images/themes/patterns/squid79.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid80.gif);" title="url(../images/themes/patterns/squid80.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid81.gif);" title="url(../images/themes/patterns/squid81.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid82.gif);" title="url(../images/themes/patterns/squid82.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid83.gif);" title="url(../images/themes/patterns/squid83.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid84.gif);" title="url(../images/themes/patterns/squid84.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid85.gif);" title="url(../images/themes/patterns/squid85.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid86.gif);" title="url(../images/themes/patterns/squid86.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid87.gif);" title="url(../images/themes/patterns/squid87.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid88.gif);" title="url(../images/themes/patterns/squid88.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid89.gif);" title="url(../images/themes/patterns/squid89.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid90.gif);" title="url(../images/themes/patterns/squid90.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid91.gif);" title="url(../images/themes/patterns/squid91.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid92.gif);" title="url(../images/themes/patterns/squid92.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid93.gif);" title="url(../images/themes/patterns/squid93.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid94.gif);" title="url(../images/themes/patterns/squid94.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid95.gif);" title="url(../images/themes/patterns/squid95.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid96.gif);" title="url(../images/themes/patterns/squid96.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid97.gif);" title="url(../images/themes/patterns/squid97.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid98.gif);" title="url(../images/themes/patterns/squid98.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid99.gif);" title="url(../images/themes/patterns/squid99.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid100.gif);" title="url(../images/themes/patterns/squid100.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid101.gif);" title="url(../images/themes/patterns/squid101.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid102.gif);" title="url(../images/themes/patterns/squid102.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid103.gif);" title="url(../images/themes/patterns/squid103.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid104.gif);" title="url(../images/themes/patterns/squid104.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid105.gif);" title="url(../images/themes/patterns/squid105.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid106.gif);" title="url(../images/themes/patterns/squid106.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid107.gif);" title="url(../images/themes/patterns/squid107.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid108.gif);" title="url(../images/themes/patterns/squid108.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid109.gif);" title="url(../images/themes/patterns/squid109.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid110.gif);" title="url(../images/themes/patterns/squid110.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid111.gif);" title="url(../images/themes/patterns/squid111.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid112.gif);" title="url(../images/themes/patterns/squid112.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid113.gif);" title="url(../images/themes/patterns/squid113.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid114.gif);" title="url(../images/themes/patterns/squid114.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid115.gif);" title="url(../images/themes/patterns/squid115.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid116.gif);" title="url(../images/themes/patterns/squid116.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid117.gif);" title="url(../images/themes/patterns/squid117.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid118.gif);" title="url(../images/themes/patterns/squid118.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid119.gif);" title="url(../images/themes/patterns/squid119.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid120.gif);" title="url(../images/themes/patterns/squid120.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid121.gif);" title="url(../images/themes/patterns/squid121.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid122.gif);" title="url(../images/themes/patterns/squid122.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid123.gif);" title="url(../images/themes/patterns/squid123.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid124.gif);" title="url(../images/themes/patterns/squid124.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid125.gif);" title="url(../images/themes/patterns/squid125.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid126.gif);" title="url(../images/themes/patterns/squid126.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid127.gif);" title="url(../images/themes/patterns/squid127.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid128.gif);" title="url(../images/themes/patterns/squid128.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid129.gif);" title="url(../images/themes/patterns/squid129.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid130.gif);" title="url(../images/themes/patterns/squid130.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid131.gif);" title="url(../images/themes/patterns/squid131.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid132.gif);" title="url(../images/themes/patterns/squid132.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid133.gif);" title="url(../images/themes/patterns/squid133.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid134.gif);" title="url(../images/themes/patterns/squid134.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid135.gif);" title="url(../images/themes/patterns/squid135.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid136.gif);" title="url(../images/themes/patterns/squid136.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid137.gif);" title="url(../images/themes/patterns/squid137.gif)">
				<br />
			</li>
			<li class="p" style="background:url(../images/themes/patterns/squid138.gif);" title="url(../images/themes/patterns/squid138.gif)">
				<br />
			</li>
		</ul>
	</div>
	<div id="colorMenu" class="left">
		<label class="desc">
			Color
		</label>
		<div id="colorPicker">
			<ul id="colorList">
				<!-- Initial Gray -->
				<li style="background:transparent" title="transparent">
					<span>
					</span>
				</li>
				<li style="background:#000000">
				</li>
				<li style="background:#333333">
				</li>
				<li style="background:#444444">
				</li>
				<li style="background:#666666">
				</li>
				<li style="background:#999999">
				</li>
				<li style="background:#cccccc">
				</li>
				<li style="background:#dedede">
				</li>
				<li style="background:#eeeeee">
				</li>
				<li style="background:#ffffff">
				</li>
				<!-- Quick Spectrum -->
				<li style="background:#cc0000">
				</li>
				<li style="background:#ff0000">
				</li>
				<li style="background:#ff6600">
				</li>
				<li style="background:#ff9900">
				</li>
				<li style="background:#ffcc00">
				</li>
				<li style="background:#ffff00">
				</li>
				<li style="background:#99cc00">
				</li>
				<li style="background:#669900">
				</li>
				<li style="background:#339900">
				</li>
				<li style="background:#006600">
				</li>
				<li style="background:#006666">
				</li>
				<li style="background:#0033ff">
				</li>
				<li style="background:#003399">
				</li>
				<li style="background:#3333cc">
				</li>
				<li style="background:#6600cc">
				</li>
				<li style="background:#990099">
				</li>
				<li style="background:#ff00ff">
				</li>
				<li style="background:#ff66ff">
				</li>
				<!-- More Colors -->
				<li style="background:#ff6666">
				</li>
				<li style="background:#ff3333">
				</li>
				<li style="background:#ff0000">
				</li>
				<li style="background:#cc0000">
				</li>
				<li style="background:#990000">
				</li>
				<li style="background:#ff9966">
				</li>
				<li style="background:#ff6633">
				</li>
				<li style="background:#ff3300">
				</li>
				<li style="background:#cc3300">
				</li>
				<li style="background:#993300">
				</li>
				<li style="background:#ffcccc">
				</li>
				<li style="background:#cc9999">
				</li>
				<li style="background:#996666">
				</li>
				<li style="background:#663333">
				</li>
				<li style="background:#330000">
				</li>
				<li style="background:#ff9999">
				</li>
				<li style="background:#cc6666">
				</li>
				<li style="background:#cc3333">
				</li>
				<li style="background:#993333">
				</li>
				<li style="background:#660000">
				</li>
				<li style="background:#ffcc99">
				</li>
				<li style="background:#cc9966">
				</li>
				<li style="background:#cc6633">
				</li>
				<li style="background:#996633">
				</li>
				<li style="background:#663300">
				</li>
				<li style="background:#ff9933">
				</li>
				<li style="background:#ff6600">
				</li>
				<li style="background:#ff9900">
				</li>
				<li style="background:#cc6600">
				</li>
				<li style="background:#cc9933">
				</li>
				<li style="background:#ffcc66">
				</li>
				<li style="background:#ffcc33">
				</li>
				<li style="background:#ffcc00">
				</li>
				<li style="background:#cc9900">
				</li>
				<li style="background:#996600">
				</li>
				<li style="background:#ffffcc">
				</li>
				<li style="background:#cccc99">
				</li>
				<li style="background:#999966">
				</li>
				<li style="background:#666633">
				</li>
				<li style="background:#333300">
				</li>
				<li style="background:#ffff99">
				</li>
				<li style="background:#cccc66">
				</li>
				<li style="background:#cccc33">
				</li>
				<li style="background:#999933">
				</li>
				<li style="background:#666600">
				</li>
				<li style="background:#ffff66">
				</li>
				<li style="background:#ffff33">
				</li>
				<li style="background:#ffff00">
				</li>
				<li style="background:#cccc00">
				</li>
				<li style="background:#999900">
				</li>
				<li style="background:#ccff66">
				</li>
				<li style="background:#ccff33">
				</li>
				<li style="background:#ccff00">
				</li>
				<li style="background:#99cc00">
				</li>
				<li style="background:#669900">
				</li>
				<li style="background:#ccff99">
				</li>
				<li style="background:#99cc66">
				</li>
				<li style="background:#99cc33">
				</li>
				<li style="background:#669933">
				</li>
				<li style="background:#336600">
				</li>
				<li style="background:#99ff33">
				</li>
				<li style="background:#99ff00">
				</li>
				<li style="background:#66ff00">
				</li>
				<li style="background:#66cc00">
				</li>
				<li style="background:#66cc33">
				</li>
				<li style="background:#99ff66">
				</li>
				<li style="background:#66ff33">
				</li>
				<li style="background:#33ff00">
				</li>
				<li style="background:#33cc00">
				</li>
				<li style="background:#339900">
				</li>
				<li style="background:#ccffcc">
				</li>
				<li style="background:#99cc99">
				</li>
				<li style="background:#669966">
				</li>
				<li style="background:#336633">
				</li>
				<li style="background:#003300">
				</li>
				<li style="background:#99ff99">
				</li>
				<li style="background:#66cc66">
				</li>
				<li style="background:#33cc33">
				</li>
				<li style="background:#339933">
				</li>
				<li style="background:#006600">
				</li>
				<li style="background:#66ff66">
				</li>
				<li style="background:#33ff33">
				</li>
				<li style="background:#00ff00">
				</li>
				<li style="background:#00cc00">
				</li>
				<li style="background:#009900">
				</li>
				<li style="background:#66ff99">
				</li>
				<li style="background:#33ff66">
				</li>
				<li style="background:#00ff33">
				</li>
				<li style="background:#00cc33">
				</li>
				<li style="background:#009933">
				</li>
				<li style="background:#99ffcc">
				</li>
				<li style="background:#66cc99">
				</li>
				<li style="background:#33cc66">
				</li>
				<li style="background:#339966">
				</li>
				<li style="background:#006633">
				</li>
				<li style="background:#33ff99">
				</li>
				<li style="background:#00ff66">
				</li>
				<li style="background:#00ff99">
				</li>
				<li style="background:#00cc66">
				</li>
				<li style="background:#33cc99">
				</li>
				<li style="background:#66ffcc">
				</li>
				<li style="background:#33ffcc">
				</li>
				<li style="background:#00ffcc">
				</li>
				<li style="background:#00cc99">
				</li>
				<li style="background:#009966">
				</li>
				<li style="background:#ccffff">
				</li>
				<li style="background:#99cccc">
				</li>
				<li style="background:#669999">
				</li>
				<li style="background:#336666">
				</li>
				<li style="background:#003333">
				</li>
				<li style="background:#99ffff">
				</li>
				<li style="background:#66cccc">
				</li>
				<li style="background:#33cccc">
				</li>
				<li style="background:#339999">
				</li>
				<li style="background:#006666">
				</li>
				<li style="background:#66ffff">
				</li>
				<li style="background:#33ffff">
				</li>
				<li style="background:#00ffff">
				</li>
				<li style="background:#00cccc">
				</li>
				<li style="background:#009999">
				</li>
				<li style="background:#66ccff">
				</li>
				<li style="background:#33ccff">
				</li>
				<li style="background:#00ccff">
				</li>
				<li style="background:#0099cc">
				</li>
				<li style="background:#006699">
				</li>
				<li style="background:#99ccff">
				</li>
				<li style="background:#6699cc">
				</li>
				<li style="background:#3399cc">
				</li>
				<li style="background:#336699">
				</li>
				<li style="background:#003366">
				</li>
				<li style="background:#3399ff">
				</li>
				<li style="background:#0099ff">
				</li>
				<li style="background:#0066ff">
				</li>
				<li style="background:#0066cc">
				</li>
				<li style="background:#3366cc">
				</li>
				<li style="background:#6699ff">
				</li>
				<li style="background:#3366ff">
				</li>
				<li style="background:#0033ff">
				</li>
				<li style="background:#0033cc">
				</li>
				<li style="background:#003399">
				</li>
				<li style="background:#ccccff">
				</li>
				<li style="background:#9999cc">
				</li>
				<li style="background:#666699">
				</li>
				<li style="background:#333366">
				</li>
				<li style="background:#000033">
				</li>
				<li style="background:#9999ff">
				</li>
				<li style="background:#6666cc">
				</li>
				<li style="background:#3333cc">
				</li>
				<li style="background:#333399">
				</li>
				<li style="background:#000066">
				</li>
				<li style="background:#6666ff">
				</li>
				<li style="background:#3333ff">
				</li>
				<li style="background:#0000ff">
				</li>
				<li style="background:#0000cc">
				</li>
				<li style="background:#000099">
				</li>
				<li style="background:#9966ff">
				</li>
				<li style="background:#6633ff">
				</li>
				<li style="background:#3300ff">
				</li>
				<li style="background:#3300cc">
				</li>
				<li style="background:#330099">
				</li>
				<li style="background:#cc99ff">
				</li>
				<li style="background:#9966cc">
				</li>
				<li style="background:#6633cc">
				</li>
				<li style="background:#663399">
				</li>
				<li style="background:#330066">
				</li>
				<li style="background:#9933ff">
				</li>
				<li style="background:#6600ff">
				</li>
				<li style="background:#9900ff">
				</li>
				<li style="background:#6600cc">
				</li>
				<li style="background:#9933cc">
				</li>
				<li style="background:#cc66ff">
				</li>
				<li style="background:#cc33ff">
				</li>
				<li style="background:#cc00ff">
				</li>
				<li style="background:#9900cc">
				</li>
				<li style="background:#660099">
				</li>
				<li style="background:#ffccff">
				</li>
				<li style="background:#cc99cc">
				</li>
				<li style="background:#996699">
				</li>
				<li style="background:#663366">
				</li>
				<li style="background:#330033">
				</li>
				<li style="background:#ff99ff">
				</li>
				<li style="background:#cc66cc">
				</li>
				<li style="background:#cc33cc">
				</li>
				<li style="background:#993399">
				</li>
				<li style="background:#660066">
				</li>
				<li style="background:#ff66ff">
				</li>
				<li style="background:#ff33ff">
				</li>
				<li style="background:#ff00ff">
				</li>
				<li style="background:#cc00cc">
				</li>
				<li style="background:#990099">
				</li>
				<li style="background:#ff66cc">
				</li>
				<li style="background:#ff33cc">
				</li>
				<li style="background:#ff00cc">
				</li>
				<li style="background:#cc0099">
				</li>
				<li style="background:#990066">
				</li>
				<li style="background:#ff99cc">
				</li>
				<li style="background:#cc6699">
				</li>
				<li style="background:#cc3399">
				</li>
				<li style="background:#993366">
				</li>
				<li style="background:#660033">
				</li>
				<li style="background:#ff3399">
				</li>
				<li style="background:#ff0099">
				</li>
				<li style="background:#ff0066">
				</li>
				<li style="background:#cc0066">
				</li>
				<li style="background:#cc3366">
				</li>
				<li style="background:#ff6699">
				</li>
				<li style="background:#ff3366">
				</li>
				<li style="background:#ff0033">
				</li>
				<li style="background:#cc0033">
				</li>
				<li style="background:#990033">
				</li>
				<li style="background:#FFE4E1">
				</li>
				<li style="background:#E6E6FA">
				</li>
				<li style="background:#F0F8FF">
				</li>
				<li style="background:#F0FFFF">
				</li>
				<li style="background:#F0FFF0">
				</li>
				<li style="background:#FFF5EE">
				</li>
				<li style="background:#FFFACD">
				</li>
				<li style="background:#FFF8DC">
				</li>
				<li style="background:#FFFAF0">
				</li>
				<li style="background:#FDF5E6">
				</li>
				<li style="background:#FAF0E6">
				</li>
				<li style="background:#FFEFD5">
				</li>
				<li style="background:#FFE4C4">
				</li>
				<li style="background:#FFDAB9">
				</li>
				<li style="background:#FFE4B5">
				</li>
			</ul>
		</div>
		<var id="colorPreview">
		</var>
		<input id="color" name="colorMenu" onkeyup="typeColor($F(this));" onblur="typeColor($F(this));" class="text" type="text" value="#" size="7" maxlength="7"/>
	</div>
	<div id="customMenu" class="left">
		<span id="imageLink">
			<label for="custom" class="desc">
				Where's your image on the web?
			</label>
			<input id="custom" name="custom" class="text" type="text" value="http://" />
		</span>
		<span id="heightMenu">
			<label for="logoHeight" class="desc inline">
				Height :
			</label>
			<input id="logoHeight" name="logoHeight" class="text" type="text" value="" size="3" />
			px
		</span>
		<input class="button translate" type="submit" onclick="selectCustom();return false" value="Apply"/>
	</div>
	<div id="buttonMenu" class="left">
		<label class="desc">
			Button Type
		</label>
		<input id="butText" name="butType" type="radio" checked="checked" onclick="changeButtonType();"/>
		<label for="butText">
			Text
		</label>
		<input id="butLink" name="butType" type="radio" onclick="changeButtonType();"/>
		<label for="butLink">
			Link
		</label>
		<span id="textInput">
			<label for="buttonText" class="desc">
				What's your button going to say?
			</label>
			<input id="buttonText" name="buttonText" class="text" type="text" value="Submit" />
		</span>
		<span id="linkInput" class="hide">
			<label for="buttonImage" class="desc">
				Where's your image on the web?
			</label>
			<input id="buttonImage" name="buttonImage" class="text" type="text" value="http://" />
		</span>
	</div>
	<div id="advancedMenu" class="left">
		<label for="cssURL" class="desc">
			Where's your CSS file on the web?
		</label>
		<input id="cssURL" name="cssURL" class="text" type="text" value="http://" />
		<input class="button translate" type="submit" onclick="Lightbox.showUrl('/lightboxes/style-preview.php');return false;" value="Preview" />
		<p>
			Download the
			<a href="#">Hoctudau Theme Kit!</a>
			<a href="#" class="tooltip" title="HoctudauTheme Kit" rel="Use this to help you design a CSS file to further customize yourHoctudau  form.">(<b>?</b>)</a>
		</p>
	</div>
	<!--Fields used to save the form-->
	<input type="hidden" id="action" name="action" value="">
	<input type="hidden" id="name" name="name" value="">
	<input type="hidden" id="oldName" name="oldName" value="">
	<input type="hidden" id="styleId" name="styleId" value="">
	<input type="hidden" id="dbCss" name="dbCss" value="">
	<input type="hidden" id="submitType" name="submitType" value="">
	<input type="hidden" id="submitVal" name="submitVal" value="">
	<input type="hidden" id="customCss" name="customCss" value="">
</form>
</div>
<!--styleChanger-->
<div id="preview">
<div id="container2">
	<h1 id="nav2" class="logo">
		<a>Hoctudau</a>
	</h1>
	<form class="hoctudau" action="#">
		<header class="info">
			<h2>
				The Form Title
			</h2>
			<div>
				And the description of the form.
			</div>
		</header>
		<ul id="formFields">
			<li class="section first">
				<section>
					<h3>
						A Section Title
					</h3>
					<div>
						With the description of the section.
					</div>
				</section>
			</li>
			<li class="focused">
				<label class="desc">
					Field Title
					<span class="req">
						*
					</span>
				</label>
				<div>
					<textarea class="textarea medium translate" rows="10" cols="50">
						From days of long ago, from uncharted regions of the universe, comes a legend of a mighty robot, loved by good, feared by evil. As the legend grew, peace settled across the galaxy. On planet Earth, a
						galaxy alliance was formed and they maintained peace throughout the universe, until a new horrible menace threatened the galaxy. The robot was needed once more.
					</textarea>
				</div>
				<p class="instruct translate">
					<small>
						Field Instructions: Activate interlocks! Dyna-therms connected. Infra-cells up. Mega-thrusters are go!
					</small>
				</p>
			</li>
			<li>
				<label class="desc">
					Another Field Title
				</label>
				<div>
					<input class="text medium translate" type="text" value="Form Blazing Sword!"/>
				</div>
			</li>
			<li class="buttons">
				<input id="saveForm" class="btTxt submit translate" type="submit" value="Submit" onclick="return false" />
				<input id="saveFormImage" class="btImg submit hide" type="image" onclick="return false" />
			</li>
		</ul>
		<!--formFields-->
	</form>
</div>
<!--container2-->
<div class="promo">
	<b>
		NEW!
	</b>
	<img src="template/images/icons/palette.png" alt="" />
	Try one of our CSS Themes from the
	<a href="/gallery/designs/">HoctudauForm Gallery</a>
	!
</div>
</div>
<!--preview-->
</div>