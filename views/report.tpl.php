<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<h1 id="logo">
	<a class="notranslate">Hoctudau</a>
</h1>
<header id="header" class="info">
	<div class="buttons">
	</div>
	<h2 class="notranslate">
		<?php echo $_report['Name']; ?>
	</h2>
	<div class="notranslate">
		<?php echo $_report['Description']; ?>
	</div>
</header>
<div id="stage" class="clearfix">
	<div id="z1" class="zone clearfix">
		<?php echo $z1; ?>
	</div>
	<div id="z2" class="zone clearfix">
        <?php echo $z2; ?>
	</div>
	<div id="z3" class="zone clearfix">
        <?php echo $z3; ?>
	</div>
	<footer class="footer">
		<h4 class="notranslate">
			<?php echo $_report['Name']; ?>
		</h4>
		<span>
			<?php echo $report_result['report_create']; ?>
		</span>
	</footer>
	<!--footer-->
</div>
<!--stage-->