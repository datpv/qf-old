<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<div id="stage">
	<div class="info">
		<div class="buttons">
			<a class="button" href="#" onclick="lightboxElement('feedreader');return false;">
			<img class="icon" src="template/images/icons/rss.png" alt=""/> Subscribe to RSS Feeds</a>
		</div>
		<h2>
			Notification Settings
		</h2>
		<div class="notranslate">
			My words fly up, my thoughts remain below.
		</div>
	</div>
	<!--info-->
	<h2 class="f">
		Send Notifications from
		<b class="notranslate">
			<?php echo $_form['Name']; ?>
		</b>
		...
	</h2>
	<form id="inbox" class="blue" action="#" onsubmit="return false;">
		<h3>
			<img class="logo" src="template/images/integration/InboxSmall.png" alt=""/>
			to My Inbox
		</h3>
		<ul>
			<li>
				<label class="desc" for="Email">
					Your Email Address
					<a class="tooltip" title="About Emailing New Entries" rel="If you would like every successfully submitted public entry from this form to be emailed to you, 
					type in the email address where you'd like a summary of the entry to be sent. All submissions will 
					still be accessible through yourHoctudau account. Files submitted via the File Upload field will not be 
					sent and entries created in theHoctudau Admin interface will not be emailed. Multiple email addresses 
					may be entered here by separating them with a comma. For example: 'one@gmail.com, two@gmail.com'">(?)</a>
				</label>
				<input id="Email" name="Email" class="text" type="text" value="<?php echo isset($_notify['Email'])?$_notify['Email']:''; ?>" maxlength="255" />
			</li>
			<li>
				<label class="desc" for="ReplyTo">
					Set Reply To:
					<a class="tooltip" title="About Set Reply To:" rel="Sets the Reply To: address of your emailed entries to the email field specified. Useful in workflows that require contacting the users that filled out yourHoctudau form via email. Works great with contact forms, trouble tickets and bug trackers. Requires your form to have an email field.">(?)</a>
				</label>
				<select id="ReplyTo" name="ReplyTo" class="select large">
                    <?php if(empty($emails)): ?>
					<option value="">
						No email fields found on this form.
					</option>
                    <?php else:
                    $reply_id = isset($_notify['ReplyTo'])?$_notify['ReplyTo']:'';
                    ?>
                    <option value="">
						No Reply To
					</option>
                    <?php foreach($emails as $email) :
                    ?>
                    <option <?php if($email['ColumnId'] == $reply_id) echo 'selected="selected"'; ?> value="<?php echo $email['ColumnId']; ?>">
						<?php echo $email['Title']; ?>
					</option>
                    <?php endforeach; endif; ?>
				</select>
			</li>
			<li>
				<label class="desc">
					Send Me Emails About :
				</label>
				<input id="EmailNewEntries" name="EmailNewEntries" class="checkbox" type="checkbox" value="1" checked="checked" />
				<label class="choice" for="EmailNewEntries">
					New Entries
				</label>
				<input id="EmailNewComments" name="EmailNewComments" class="checkbox" type="checkbox" value="1" />
				<label class="choice" for="EmailNewComments">
					New Comments
				</label>
			</li>
			<li class="hide">
				<input type="hidden" id="From" name="ConfirmationFromAddress" value="Hoctudau" />
				<input type="hidden" id="Subject" name="ConfirmationSubject" value="{form:Name} [#{entry:EntryId}]" />
				<input type="hidden" id="PlainText" name="PlainText" value="0" />
				<span id="defaultSubject">
					{form:Name} [#{entry:EntryId}]
				</span>
			</li>
		</ul>
		<div class="actions clearfix">
			<span class="left">
				<a href="#" onclick="showEmailOptionsLightbox(); return false;" class="button">Customize Notification Email</a>
			</span>
			<span class="right">
				<a id="saveButtonInbox" href="#" onclick="inbox(); return false;" class="button positive">Save</a>
			</span>
		</div>
	</form>
	<!--inbox-->
	<form id="mobile" class="purple" action="#" method="post" onsubmit="return false;">
		<h3>
			<img class="logo" src="template/images/integration/MobileSmall.png" alt=""/>
			to My Mobile Device
		</h3>
		<ul>
			<li>
				<label class="desc" for="mobileNumber">
					Your Cell Phone Number
				</label>
				<input name="Mobile" id="mobileNumber" class="text" type="text" value="" />
				<a id="otherLink" class="hide" target="_blank" href="/docs/notifications/#carrier">About Using Your Own Carrier</a>
			</li>
			<li>
				<label class="desc" for="Carrier">
					Your Carrier
					<a class="tooltip" title="About Carrier" rel="This is where you specify the carrier you use for your mobile phone service. If your carrier is listed twice, the first option 
					is the most up-to-date Email to SMS pattern we have on file. If your carrier is not on the list, you can select Other and provide your carrier's
					Email to SMS pattern (i.e. 1234567@yourcarrier.com)">(?)</a>
				</label>
				<select id="carriers" name="Carrier" onclick="toggleMobileOther(); return null;" onchange="toggleMobileOther(); return null;" class="select large">
					<option value="">
					</option>
					<option value="Alltel">
						Alltel
					</option>
					<option value="AT&amp;TText">
						AT&amp;T (iPhone)
					</option>
					<option value="AT&amp;T">
						AT&amp;T
					</option>
					<option value="BellAtlantic">
						Bell Atlantic (message.bam.com)
					</option>
					<option value="BellCanada">
						Bell Canada (bellmobility.ca)
					</option>
					<option value="BellMobilityCanada">
						Bell Mobility (Canada - txt.bell.ca)
					</option>
					<option value="BellMobility">
						Bell Mobility (txt.bellmobility.ca)
					</option>
					<option value="Boost">
						Boost
					</option>
					<option value="CingularMe">
						Cingular (cingularme.com)
					</option>
					<option value="Cingular">
						Cingular (mobile.mycingular.com)
					</option>
					<option value="Nextel">
						Nextel
					</option>
					<option value="Rogers">
						Rogers
					</option>
					<option value="Sprint">
						Sprint
					</option>
					<option value="T-Mobile">
						T-Mobile
					</option>
					<option value="Telus">
						Telus
					</option>
					<option value="Verizon">
						Verizon
					</option>
					<option value="VirginMoblie">
						Virgin Moblie (vmobl.com)
					</option>
					<option value="VirginMoblieCA">
						Virgin Moblie CA (vmobile.ca)
					</option>
					<option value="Vodafone UK">
						Vodafone UK
					</option>
					<option value="Other">
						Other
					</option>
				</select>
			</li>
			<li>
				<label class="desc">
					Send Me Text Messages About :
				</label>
				<input id="PhoneNewEntries" name="PhoneNewEntries" class="checkbox" type="checkbox" value="1" checked="checked" />
				<label class="choice" for="PhoneNewEntries">
					New Entries
				</label>
				<input id="PhoneNewComments" name="PhoneNewComments" class="checkbox" type="checkbox" value="1" />
				<label class="choice" for="PhoneNewComments">
					New Comments
				</label>
			</li>
		</ul>
		<div class="actions clearfix">
			<span class="left">
			</span>
			<span class="right">
				<a id="saveButtonMobile" href="#" onClick="mobile(); return false;" class="button positive">Save</a>
			</span>
		</div>
	</form>
	<!--mobile-->
	<form id="add" class="outline" onsubmit="return false;">
		<div>
			<table cellspacing="0">
				<tr>
					<td>
						<span id="logoHolder">
							<img id="integrationLogo" src="template/images/integration/Add.png" alt="" />
						</span>
					</td>
				</tr>
			</table>
			<h3>
				to Another Application
				<a id="addIntegrationHelpLink" href="/docs/integrations/" target="_blank">(?)</a>
			</h3>
			<select id="addNewNotification" class="select">
				<option value="">
				</option>
				<option value="Twitter">
					Twitter
				</option>
				<option value="CampaignMonitor">
					Campaign Monitor
				</option>
				<option value="MailChimp">
					MailChimp
				</option>
				<option value="Highrise">
					Highrise
				</option>
				<option value="Campfire">
					Campfire
				</option>
				<option value="FreshBooks">
					FreshBooks
				</option>
				<option value="Salesforce">
					Salesforce
				</option>
				<option value="WebHook">
					WebHook
				</option>
				<optgroup label="HoctudauWingman">
					<option value="Sendloop">
						Sendloop
					</option>
					<option value="Heap">
						Heap
					</option>
					<option value="Torch">
						Torch
					</option>
					<option value="Zferral">
						Zferral
					</option>
				</optgroup>
			</select>
			<p>
				<button onclick="addNewThirdPartyIntegration(); return false;">
					Add Integration
				</button>
			</p>
		</div>
	</form>
</div>
<!--stage-->
</div>
<form id="feedreader" class="yellow boxed" style="display:none">
	<h3>
		<img class="logo" src="template/images/integration/FeedSmall.png" alt=""/>
		<img class="remove" src="template/images/icons/cross.png" onclick="Lightbox.close();" alt="Remove"/>
		to My Feedreader
	</h3>
	<ul>
		<li>
			<label class="desc center">
				Subscribe to the Following Feeds
			</label>
			<div class="center">
				<a href="#" class="button">
				<img class="icon" src="template/images/icons/rss.png" alt=""/>
				New Entries</a>
				<a href="#" class="button">
				<img class="icon" src="template/images/icons/rss.png" alt=""/>
				New Comments</a>
			</div>
			<div class="notice">
				<b>
					Security Notice :
				</b>
				AllHoctudau RSS feeds are encrypted and password protected. You must use yourHoctudau account
				<strong>
					email address
				</strong>
				and
				<strong>
					password
				</strong>
				to access these feeds.
				<br />
				<br />
				Secure RSS feeds are
				<strong>
					not
				</strong>
				supported by all feed readers. If you are unsure how to use credentials in your feed reader, please contact
				<strong>
					their
				</strong>
				support department.
			</div>
		</li>
	</ul>
</form>
<!--feedreader-->
<div id="preload">
	<img src="template/images/integration/CampaignMonitor.png" alt=""/>
	<img src="template/images/integration/MailChimp.png" alt=""/>
	<img src="template/images/integration/Highrise.png" alt=""/>
	<img src="template/images/integration/Campfire.png" alt=""/>
	<img src="template/images/integration/FreshBooks.png" alt=""/>
	<img src="template/images/integration/WebHook.png" alt=""/>
	<img src="template/images/integration/Twitter.png" alt=""/>
	<img src="template/images/integration/Salesforce.png" alt=""/>
</div>

<div>
        