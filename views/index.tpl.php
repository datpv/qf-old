<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<div id="stage">
    <div id="main">
        <h1>Bạn có thể đóng góp <?php echo $x; ?> bài, mỗi bài tối đa <?php echo $y; ?> câu hỏi</h1>
        <div class="group">
            <?php
            if($x != 'X') {
            ?>
            <p class="notice">Bạn có câu hỏi (Quiz) muốn đóng góp thì ấn vào <a href="<?php echo $siteUrl ?>importer/">đây</a>. Lưu ý: phải tuân theo QUY ƯỚC ĐẶT TÊN của Thầy Giáo.</p>
            <?php
            }
            ?>
            <h3>Các bài đã đóng góp (<?php echo count($forms); ?>)</h3>
            <ol>
                <?php
                foreach($forms as $form) {
                    $form_content = $this->helpers->json_decode_to_array($form['form_content']);
                ?>
                    <li>
                    <span class="notranslate"><?php echo $form_content['Name'] ?> </span>
                    <?php 
                    if($form['form_approved'] == 'Y') {
                    ?>
                    <span class="approved">Đã Được Duyệt</span>
                    <?php
                    } else {
                    ?>    
                    <span class="unapprove">Chưa Được Duyệt</span>
                    <?php
                    }
                    ?>
                    </li>
                <?php
                }
                ?>
            </ol>
        </div>
    </div>
</div>