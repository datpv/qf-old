<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<div id="stage">
	<div id="main" class="dg">

		<div class="info">
			<div class="buttons">
				
			</div>
			<h2>
				Caches Mananger
			</h2>
		</div>
        <div class="subdomain clearfix">
				<p class="plan1 notranslate">
                Total: <strong><?php echo $all_user_count; ?></strong> Users | 
                Cached <strong id="cached_user"><?php echo count($cached_users_result); ?></strong> Users | 
                <strong id="left_cached"><?php echo ($all_user_count-count($cached_users_result));?></strong> Cached User Left
				</p>
		</div>
        <div class="block">
        <div class="leftTable">
            <a onclick="rebuildAllCaches(); return false;" id="rebuildAll" class="button" href="#">Rebuild All User Caches Of This Page</a>
            
        </div>
        <div class="rightTable">
            <a onclick="deleteNullCached(); return false;" id="deleteNullCached" class="button" href="#">Delete ALL Cached Not In Group</a>
            <a onclick="emptyCached(); return false;" id="emptyCached" class="button" href="#">Empty All Cache</a>
        </div>
	  </div>
        <div class="block">
            <div class="col3">
                <table class="table">
                    <thead>
                        <th>
                            Username
                        </th>
                        <th>
                            Cached Form AS
                        </th>
                        <th>
                            Cached Form NB
                        </th>
                        <th>
                            Cached Form SCED
                        </th>
                        <th>
                            Cached Form NBED
                        </th>
                        <th>&nbsp;</th>
                    </thead>
                    <tbody>
                    <?php 
                    foreach($users_result as $user) {
                    ?>
                        <tr id="user_<?php echo $user['user_id']; ?>">
                            <td><?php echo $user['username']; ?></td>
                            <td class="as"><?php
                            echo 'Cached <strong>'.$user['as_count'].'</strong> Rows'; 
                            
                            ?><p></p></td>
                            <td class="nb"><?php 
                            echo 'Cached <strong>'.$user['nb_count'].'</strong> Rows'; 
                            
                            ?><p></p></td>
                            <td class="sced"><?php 
                            echo 'Cached <strong>'.$user['sced_count'].'</strong> Rows'; 
                            
                            ?><p></p></td>
                            <td class="nbed"><?php 
                            echo 'Cached <strong>'.$user['nbed_count'].'</strong> Rows'; 
                            
                            ?><p></p></td>
                            <td><a rel="<?php echo $user['user_id']; ?>" class="rebuild button" href="#">Rebuild Cache</a></td>
                            
                        </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                </table>
                
                <div class="paged">
            <span id="navHolder">
                <span class="dgNav">
                <a class="firstPage <?php
                    if($paged <= 1) {
                        echo 'disable';
                    } else {
                        echo 'show';
                    }
                ?>" href="<?php echo $siteUrl . 'caches/&paged=1'; ?>" title="First Page">«</a>
                <a class="prevPage <?php
                    if($paged <= 1) {
                        echo 'disable';
                    } else {
                        echo 'show';
                    }
                ?>" href="<?php echo $siteUrl . 'caches/&paged='.($paged-1); ?>" title="Previous Page">‹</a>
                <span class="dgInfo">
                    <var class="startEntry"><?php echo $paged; ?></var>-<var class="endEntry"><?php echo $user_count; ?></var> of <var class="totalEntries"><?php echo $total_paged; ?></var></span>
                <a class="nextPage <?php
                    if(($paged) >= $total_paged) {
                        echo 'disable';
                    } else {
                        echo 'show';
                    }
                ?>" href="<?php echo $siteUrl . 'caches/&paged='.($paged+1); ?>" title="Next Page">›</a>
                <a class="lastPage <?php
                    if(($paged) >= $total_paged) {
                        echo 'disable';
                    } else {
                        echo 'show';
                    }
                ?>" href="<?php echo $siteUrl . 'caches/&paged='.($total_paged); ?>" title="Last Page">»</a>
                </span>
            </span>
        </div>
            </div>
        </div>
    </div>
</div>
<!--stage-->