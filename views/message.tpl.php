<?php if ( ! defined( 'GETOVER' ) ) exit; ?>
<?php
if(empty($message)) $message = '';
?><!DOCTYPE html>
<html class="firefox">
	<head>
		<title>
			Tin Nhắn
		</title>
		<!-- Meta Tags -->
		<meta charset="utf-8" />
		<?php
        if(isset($redirect)) {
        ?>
        <meta HTTP-EQUIV="Refresh" CONTENT="5; URL=index.php" />
        <?php
        }
        ?>
		<meta name="robots" content="index, nofollow, noarchive">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<!-- CSS -->
		<link href="template/stylesheets/public/forms/css/index.740.css" rel="stylesheet" />
		<!--[if lt IE 10]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js">
			</script>
		<![endif]-->
	</head>
	<body id="public" onorientationchange="window.scrollTo(0, 1)">
		<div id="container" class="confirm ltr">
			<h1 id="logo">
				<a href="#">Hoctudau</a>
			</h1>
			<form class="hoctudau" novalidate>
				<div>
                <h2><?php echo $message; ?>  
                </h2>
				</div>
			</form>
		</div>
	</body>

</html>