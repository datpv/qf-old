<?php
$cate_id = isset($_GET['category_id'])?$_GET['category_id']:'';
?>
<form id="confirmDeleteReport" class="confirm" action="#">
	<h2>Are you sure you want to <b>delete</b></h2>
	<a href="#" id="categoryDelete" class="button" onclick="categoryDelete(<?php echo $cate_id; ?>); return false">
		<img src="template/images/icons/tick.png" alt=""/> Yes, I am sure. Please delete this category.</a>
		
	<a href="#" class="lbOff negative button">
		<img src="template/images/icons/cross.png" alt=""/> Cancel</a>
</form>