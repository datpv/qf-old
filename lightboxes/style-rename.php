<form id="form1" name="f" class="prompt" action="#" onsubmit="saveRename($F('themeName'), $F('themeId')); return false;">
	<h2>Choose a New Name for Your Theme:</h2>
	<ul>
		<li>
			<div>
			<input id="themeName" name="themeName" class="text medium" type="text" value=""/> 
			</div>
			<p id="themeError"></p>
		</li>
		<li>
			<a href="#" class="button positive" onclick="saveRename($F('themeName'), $F('themeId')); return false;">
				<img src="template/images/icons/tick.png" alt=""/> Submit</a>
			<a href="#" class="lbOff button negative">
				<img src="template/images/icons/cross.png" alt=""/> Cancel</a>
		</li>
	</ul>
	<div>
		<input id="themeId" name="themeId" type="hidden" value=""/>
	</div>
</form>

<!-- JavaScript -->
<script type="text/javascript">
document.f.themeName.focus();
</script>