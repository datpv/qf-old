
<div class="info">
<h2>Confirmation Message</h2>
</div>

<form id="" action="#">
	<ul>
		<li>
		<label class="desc" for="receiptMessage">Message</label>
		<div>
		<textarea id="receiptMessage" name="" rows="10" cols="50" class="field textarea medium"
			onblur="updateForm(this.value, 'ReceiptMessage');" onkeyup="updateForm(this.value, 'ReceiptMessage');"></textarea>
		
		<label class="center">
			<a href="/docs/templating/" target="_blank">Templating Options</a> | 
			<a id="receiptApi" href="/api/code/" target="_blank">Template Tags </a>
		</label>
		</div>
		</li>
		
		<li class="complex">
		<label class="desc" for="fromAddress">Your Name or Company</label>
		<div>
		<span class="left">
		<input id="fromAddress" name="" class="field text" type="text" value=""
			onblur="updateForm(this.value, 'FromAddress');" onkeyup="updateForm(this.value, 'FromAddress');"/> 
		<label>e.g. <b>Wile E. Coyote</b> or <b>ACME Corporation</b></label>
		</span>
		<span class="half">
			<input id="receiptCopy" name="" class="checkbox field" type="checkbox" value="1" onclick="updateForm(this.checked, 'ReceiptCopy');" />
			<label class="choice" for="receiptCopy">Include a Copy of the User's Entry</label>
		</span>
		</div>
		</li>
		
		<li class="buttons">
			<a href="#" class="lbOff button positive">
				<img src="template/images/icons/tick.png" alt=""/> Done</a>
		</li>
	</ul>
</form>