<table id="fontPicker" class="deFonts" cellspacing="0">
	<thead>
	<tr>
		<td colspan="2">
			<div id="bigPreview" onclick="rotateText();" title="Click to view different text samples.">
																	<h2 id="bigSample" style="font-family: inherit;">
											Pick a typographic weapon from below.
									</h2>
				<span>Font Preview</span>
			</div>
		</td>
	</tr>
	</thead>
	
	<tbody>
	<tr>
		<td class="side">
			<ul>
				<li class="side1"><a onclick="selectFontGroup('Standard', 'deFonts'); return false" href="#">
					System Fonts</a></li>
				<li class="side2"><a onclick="selectFontGroup('LocalI', 'lclFonts1'); return false" href="#">
					Bell Fonts <b>(A-L)</b></a></li>
				<li class="side3"><a onclick="selectFontGroup('LocalII', 'lclFonts2'); return false" href="#">
					Whistle Fonts <b>(M-Z)</b></a></li>
				<li class="side4"><a onclick="selectFontGroup('Typekit', 'typFonts showKitForm'); document.TypekitForm.kitid.focus();return false" href="#">
					Typekit Fonts</a></li>
			</ul>
		</td>
		
		<td class="main">
			<ul id="StandardFonts" class="df previews clearfix">
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class="selected">
			<h3 style="font-family: inherit;">Default</h3>
			<span>Default <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: Arial, sans-serif;">Arial</h3>
			<span>Arial <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: 'Courier New', Courier, monospace;">Courier New</h3>
			<span>Courier New <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: Georgia, serif;">Georgia</h3>
			<span>Georgia <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: 'Palatino Linotype', 'Book Antiqua', Palatino, serif;">Palatino Linotype</h3>
			<span>Palatino Linotype <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: Tahoma, Geneva, sans-serif;">Tahoma</h3>
			<span>Tahoma <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: 'Trebuchet MS', Helvetica, sans-serif;">Trebuchet MS</h3>
			<span>Trebuchet MS <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: 'Times New Roman', serif;">Times New Roman</h3>
			<span>Times New Roman <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: Verdana, sans-serif;">Verdana</h3>
			<span>Verdana <a target="_blank" href="#" />(?)</a></span>
		</li>
			</ul>

<div id="StandardFontPaging" class="df left nav">
<a class="selected" href="#" onclick="fetchPage('Standard', 1); return false;" >1</a>
<a class="" href="#" onclick="fetchPage('Standard', 2); return false;" >2</a>
</div>			<ul id="LocalIFonts" class="lclI previews clearfix">
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class="selected">
			<h3 style="font-family: inherit;">Default</h3>
			<span>Default <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: Arial, sans-serif;">Arial</h3>
			<span>Arial <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: 'Courier New', Courier, monospace;">Courier New</h3>
			<span>Courier New <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: Georgia, serif;">Georgia</h3>
			<span>Georgia <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: 'Palatino Linotype', 'Book Antiqua', Palatino, serif;">Palatino Linotype</h3>
			<span>Palatino Linotype <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: Tahoma, Geneva, sans-serif;">Tahoma</h3>
			<span>Tahoma <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: 'Trebuchet MS', Helvetica, sans-serif;">Trebuchet MS</h3>
			<span>Trebuchet MS <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: 'Times New Roman', serif;">Times New Roman</h3>
			<span>Times New Roman <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: Verdana, sans-serif;">Verdana</h3>
			<span>Verdana <a target="_blank" href="#" />(?)</a></span>
		</li>
			</ul>

<div id="LocalIFontPaging" class="lclI left nav">
	<a class="selected" href="#" onclick="fetchPage('LocalI', 1); return false;" >1</a>
	<a class="" href="#" onclick="fetchPage('LocalI', 2); return false;" >2</a>
	<a class="" href="#" onclick="fetchPage('LocalI', 3); return false;" >3</a>
	<a class="" href="#" onclick="fetchPage('LocalI', 4); return false;" >4</a>
	<a class="" href="#" onclick="fetchPage('LocalI', 5); return false;" >5</a>
	<a class="" href="#" onclick="fetchPage('LocalI', 6); return false;" >6</a>
	<a class="" href="#" onclick="fetchPage('LocalI', 7); return false;" >7</a>
</div>			<ul id="LocalIIFonts" class="lclII previews clearfix">
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class="selected">
			<h3 style="font-family: inherit;">Default</h3>
			<span>Default <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: Arial, sans-serif;">Arial</h3>
			<span>Arial <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: 'Courier New', Courier, monospace;">Courier New</h3>
			<span>Courier New <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: Georgia, serif;">Georgia</h3>
			<span>Georgia <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: 'Palatino Linotype', 'Book Antiqua', Palatino, serif;">Palatino Linotype</h3>
			<span>Palatino Linotype <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: Tahoma, Geneva, sans-serif;">Tahoma</h3>
			<span>Tahoma <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: 'Trebuchet MS', Helvetica, sans-serif;">Trebuchet MS</h3>
			<span>Trebuchet MS <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: 'Times New Roman', serif;">Times New Roman</h3>
			<span>Times New Roman <a target="_blank" href="#" />(?)</a></span>
		</li>
											
		<li onclick="pickFont(this);" ondblclick="saveFont(); Lightbox.close(); return false;" class=" notranslate">
			<h3 style="font-family: Verdana, sans-serif;">Verdana</h3>
			<span>Verdana <a target="_blank" href="#" />(?)</a></span>
		</li>
			</ul>

<div id="LocalIIFontPaging" class="lclII left nav">
	<a class="selected" href="#" onclick="fetchPage('LocalII', 1); return false;" >1</a>
	<a class="" href="#" onclick="fetchPage('LocalII', 2); return false;" >2</a>
	<a class="" href="#" onclick="fetchPage('LocalII', 3); return false;" >3</a>
	<a class="" href="#" onclick="fetchPage('LocalII', 4); return false;" >4</a>
	<a class="" href="#" onclick="fetchPage('LocalII', 5); return false;" >5</a>
	<a class="" href="#" onclick="fetchPage('LocalII', 6); return false;" >6</a>
</div>			<div id="TypekitUI" class="tk">
	<form id="TypekitForm" name="TypekitForm" action="#" method="post">
		<section class="clearfix">
			<div class="fyi">
				<h3>
					<a href="http://typekit.com" target="_blank">
					<img src="/images/themes/typekit.png" alt="Typekit Integration" />
					</a>
				</h3>
		
				<p>If you have a <a href="http://typekit.com" target="_blank" class="notranslate">Typekit</a> account, you can use the fonts from one of your pre-made kits on yourHoctudau  forms.</p>
				
				<p>Be sure to <a href="/docs/integrations/typekit-integration/#kitSetup" target="_blank">make the kit available</a> for use on <b>*.hoctuda.com</b> and then tell us that kit's ID.</p>
		
			</div>
		
			<div class="kitinfo">
				<label for="kitid" class="desc notranslate">
				Typekit Kit ID
				</label>
				<input id="kitid" name="kitid" class="text" value="" /><br />
				<label class="kittip">e.g. <b>cds5kjk</b></label>
				<br /><br />
				<button id="getKitDetailsBtn" class="button">
					<img src="/images/icons/font_add.png" alt="" /> Get those fonts!
				</button>
			</div>
		</section>

		<div class="tkCancel">
			<a href="#" class="button negative" onclick="cancelSaveFont(); Lightbox.close(); return false;">
			<img src="/images/icons/cross.png" alt=""/> Cancel</a>
		</div>
	</form>

	<ul id="TypekitFonts" class="previews clearfix">
		<!--Typekit Fonts-->
		<li class="loader"><img src="/images/loader2.gif" alt="Loading..." /></li>
	</ul>
</div>			
			<div id="TypekitDifferent" class="left">
				<a class="button" href="#" onclick="changeKit(); return false;">
					<img src="/images/icons/text_replace.png" alt=""> Use a different kit.</a>
			</div>
			
			<div id="pickThisFont" class="right buttons">
				<a href="#" class="positive button" onclick="saveFont(); Lightbox.close(); return false;">
					<img src="/images/icons/style.png" alt="">
					Pick this Font
				</a>
				<a href="#" class="button negative" onclick="cancelSaveFont(); Lightbox.close(); return false;">
					<img src="/images/icons/cross.png" alt=""/> Cancel</a>
			</div>
		</td>
	</tr>
	</tbody>
</table>