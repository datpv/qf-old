<?php
$formId = isset($_GET['id'])?$_GET['id']:'';
$siteUrl = 'index.php?req=';
?>
<div class="info">
<h2>Awesome. Your subject is saved.</h2>
<div>What do you want to do now?</div>
</div>

<ol class="decide">
	<li class="blue"><a href="<?php echo $siteUrl; ?>subjects/builder/<?php echo $formId; ?>"><span>1.</span> <b>Continue editing</b> this subject.</a></li>
    <li class="blue"><a target="_blank" href="<?php echo $siteUrl; ?>subjects/view/<?php echo $formId; ?>"><span>2.</span> <b>Live View</b> this subject.</a></li>
	<li class="red"><a href="<?php echo $siteUrl; ?>subjects/all/"><span>4.</span> Take me to view <b>All Subjects</b>.</a></li>
    <li class="red"><a href="<?php echo $siteUrl; ?>subjects/manager/"><span>4.</span> Take me back to view <b>Subjects Manager</b>.</a></li>
</ol>