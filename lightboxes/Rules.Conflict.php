<div class="confirm">
<h2><b>Hey.</b> <strong>Just a heads up.</strong></h2>

<p>We noticed that you have two or more rules that perform an action on the same field. We just wanted to let you
	know that these rules <b>MAY</b> conflict with one another and cause unexpected behavior. You might want to consider reworking your
	rules to avoid this.</p>

<a href="#" class="lbOff negative button"><img src="template/images/icons/cross.png" alt=""/> Return to Rule Builder</a>
</div>