<?php
include('../config.php');
$formId = isset($_GET['id'])?$_GET['id']:'';
$siteUrl = 'index.php?req=';
?>
<div class="info">
<h2>Awesome. Your form is saved.</h2>
<div>What do you want to do now?</div>
</div>

<ol class="decide">
	<li class="blue"><a href="<?php echo $siteUrl; ?>build/<?php echo $formId; ?>"><span>1.</span> <b>Continue editing</b> this form.</a></li>
    <li class="blue"><a target="_blank" href="<?php echo $siteUrl; ?>forms/<?php echo $formId; ?>"><span>2.</span> <b>Live View</b> this form.</a></li>
	<li class="green"><a href="<?php echo $siteUrl; ?>notifications/<?php echo $formId; ?>"><span>3.</span> Setup <b>email notifications</b> for this form.</a></li>
	<li class="red"><a href="<?php echo $siteUrl; ?>admin/"><span>4.</span> I'm finished! Take me back to the <b>Form Manager</b>.</a></li>
</ol>