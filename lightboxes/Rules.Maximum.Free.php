
<div class="confirm">
<h2>You can only create <b>3</b> rules on <strong>Free</strong> accounts.</h2>

<p>The Rule Builder only allows you to create a total of 3 Field and Page rules for each form if you're on a Free plan. If you want to 
	create more rules, you'll need to upgrade to a paying account. Paid plans allow you to create up to <b>50</b> rules for each rule type.</p>

<a href="/account/" class="positive button" target="_blank">
	<img src="template/images/icons/tick.png" alt=""/> Upgrade Now</a>
<a href="/docs/rule-builder/" class="button" target="_blank">
	<img src="template/images/icons/information.png" alt=""/> Learn More About Rules</a>
<a href="#" class="lbOff negative button">
	<img src="template/images/icons/cross.png" alt=""/> Cancel</a>
</div>
