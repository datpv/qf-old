<form id="protectedForm" name="f" class="prompt" onsubmit="protectForm();return false;" >
	<h2>Create a Password for this Form</h2>
	<ul>
		<li id="protect_parent">
			<div>
			<input id="protect_password" name="password" class="text medium" type="password" value="" />
			</div>
			<p id="protect_error" class="error"></p>
		</li>
		<li class="buttons">
			<button type="submit" id="saveForm" class="positive button">
				<img src="template/images/icons/tick.png" alt=""/> Save Password</button>

			<a href="#" class="lbOff button negative">
				<img src="template/images/icons/cross.png" alt=""/> Cancel</a>
		</li>
	</ul>
	<input type="hidden" id="protect_formId" name="formId" />
</form>

<!-- JavaScript -->
<script type="text/javascript">
document.f.protect_password.focus();
</script>