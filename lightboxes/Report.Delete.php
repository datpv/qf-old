<form id="confirmDeleteReport" class="confirm" action="#">
	<h2>Are you sure you want to <b>delete</b> <strong id="deleteReportName" class="itemName"></strong>?</h2>
	
	<input id="deleteReportId" name="deleteReportId" type="hidden" value="" >
	
	<p>Deleting a report means that <b>it may no longer be viewed, exported or edited</b>. 
	All references to the report will be removed. Also note that deleting a report will <b>not</b> delete
	the data, fields, files or any other information associated with the form that the report referenced. 
	This action <b>cannot</b> be undone. If that all sounds good, let us know.</p>
	
	<a href="#" id="confirmReportDeletion" class="button" onclick="continueDeletingReport(); return false">
		<img src="template/images/icons/tick.png" alt=""/> Yes, I am sure. Please delete this report.</a>
		
	<a href="#" class="lbOff negative button">
		<img src="template/images/icons/cross.png" alt=""/> Cancel</a>
</form>