<?php
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?><style>
#lightbox {
    width: 95%;
    height: 90%;
    min-width: 1100px;
}
#lightbox.done, #lbContent {
    border-radius: 0 !important;
    border: 2px solid #d2d2d2;
}
#lbContent {
    margin: 0;
}
</style>
<div id="extra_side">
<div class="info">
<h2>Import Predefined Fields</h2>
<div><b>1)</b> Select fields. <b>2)</b> Customize as you see fit. <b>3)</b> Save/Add fields.</div>
</div>
<div id="extraFieldsManager" class="clearfix">
    <div id="addExtraFields">
    	<div id="extra_shake">
    		<h3 class="stand">
    			Add New Field
    		</h3>
    		<ul id="extra_col1">
    			<li id="extra_drag1_text" class="dragfld 0">
    				<a id="extra_sl" class="button" href="#" onclick="addExtraField('text');return false"><b></b>
    				Single Line Text</a>
    			</li>
    			<li id="extra_drag1_radio" class="dragfld 2 bigger">
    				<a id="extra_mc" class="button" href="#" onclick="addExtraField('radio');return false"><b></b>
    				Multiple Choice</a>
    			</li>
    		</ul>
    		<ul id="extra_col2">
    			<li id="extra_drag2_number" class="dragfld 0">
    				<a id="extra_nu" class="button" href="#" onclick="addExtraField('number');return false"><b></b>
    				Number</a>
    			</li>
    			<li id="extra_drag2_checkbox" class="dragfld 1 bigger">
    				<a id="extra_cb" class="button" href="#" onclick="addExtraField('checkbox');return false"><b></b>
    				Checkboxes</a>
    			</li>
    			<li id="extra_drag2_select" class="dragfld 2">
    				<a id="extra_dd" class="button" href="#" onclick="addExtraField('select');return false"><b></b>
    				Dropdown</a>
    			</li>
    		</ul>
    		<ul id="extra_col3">
    		</ul>
    		<ul id="extra_col4">
    			<li id="extra_drag4_likert" class="dragfld 4 biggest wide">
    				<a id="extra_lk" class="button" href="#" onclick="addExtraField('likert');return false"><b></b>
    				Likert</a>
    			</li>
    		</ul>
    	</div>
    	<!-- shake -->
    </div>
    <div id="extraFields">
        <div id="shake">
    		<h3 class="stand">
    			Saved Group Fields
    		</h3>
            <div class="choiceMenu">
                <ul>
                    <li id="extra_fieldGroup0"><a href="#" onclick="selectFieldGroup(this); return false; ">New Field Group</a></li>
                </ul>
            </div>
        </div>
        <div id="groupRename">
            <form action="#">
                <label for="group_rename_input">Group Name: </label><input onmouseup="renameGroup(this.value)" onkeyup="renameGroup(this.value)" class="text half" id="group_rename_input" name="group_rename_input" />
                <input class="checkbox" onclick="(this.checked) ? checkVal = '1' : checkVal = '0';defaultGroup(checkVal);" type="checkbox" id="group_default_input" name="group_default_input" /><label class="choice" for="group_default_input">Is Default? </label>
            </form>
        </div>
    </div>
    
    <div id="listOfFields">
        <h3 class="stand">
			List Fields
		</h3>
        <div class="choiceMenu">
            <ul>
            </ul>
        </div>
    </div>
    <div id="fieldPropertiesContainer">
        <form id="extra_fieldProperties" action="#" onsubmit="return false;" class="noI ">
			<ul id="extra_allProps">
				<li class="num" id="extra_fieldPos">
					1.
				</li>
				<li id="extra_listTitle">
					<label class="desc">
						Field Label
						<a href="/docs/" class="tooltip" title="About Field Label" rel="A field's label is the most direct way of telling your user what kind of data 
						should be entered into a particular field. Field labels are usually just one or two words, 
						but can also be a question.">(?)</a>
					</label>
					<div>
						<textarea id="extra_fieldTitle" class="textarea" rows="10" cols="35" onkeyup="extra_updateProperties(this.value, 'Title')" onmouseup="extra_updateProperties(this.value, 'Title')"></textarea>
					</div>
				</li>
				<li class="left half" id="extra_listType">
					<label class="desc">
						Field Type
						<a href="#" class="tooltip" title="About Field Type" rel="This property detemines what kind of data can be collected by your field. In addition to your standard fields,Hoctudau provides premade structures of the most common data types like addresses and dates. This property can only be changed on newly added fields. After you save an added field to a form, the field type cannot be changed.">(?)</a>
					</label>
					<select class="select full  undone" id="extra_fieldType" autocomplete="off" onchange="changeFieldType($F(this))">
						<optgroup label="Standard">
							<option value="text">
								Single Line Text
							</option>
							<option value="textarea">
								Paragraph Text
							</option>
							<option value="radio">
								Multiple Choice
							</option>
							<option value="checkbox">
								Checkboxes
							</option>
							<option value="select">
								Drop Down
							</option>
							<option value="number">
								Number
							</option>
							<option value="file">
								File Upload
							</option>
							<option value="section">
								Section Break
							</option>
							<!--<option value="page">Page Break</option>-->
						</optgroup>
						<optgroup label="Fancy Pants">
							<option value="shortname">
								Name
							</option>
							<option value="date">
								Date
							</option>
							<option value="time">
								Time
							</option>
							<option value="phone">
								Phone
							</option>
							<option value="address">
								Address
							</option>
							<option value="url">
								Website
							</option>
							<option value="money">
								Price
							</option>
							<option value="email">
								Email
							</option>
							<option value="likert">
								Likert
							</option>
						</optgroup>
					</select>
				</li>
				<li class="right half" id="extra_listSize">
					<label class="desc">
						Field Size
						<a href="#" class="tooltip" title="About Field Size" rel="This property only affects the visual appearance of the field in your form. On most fields, this property affects the field's width. On Paragraph Text fields, the Field Size determines the height of the field. It does not limit nor increase the amount of data that can be collected by the field.">(?)</a>
					</label>
					<select class="select full" id="extra_fieldSize" autocomplete="off" onchange="updateProperties($F(this), 'Size')">
						<option value="small">
							Small
						</option>
						<option value="medium">
							Medium
						</option>
						<option value="large">
							Large
						</option>
					</select>
				</li>
				<li class="right half" id="extra_listLayout">
					<label class="desc">
						Field Layout
						<a href="#" class="tooltip" title="About Field Layout" rel="For Checkbox and Multiple Choice fields, this property determines whether you would like your choices arranged in multiple columns or sitting next to one another.">(?)</a>
					</label>
					<select class="select full" id="extra_fieldLayout" autocomplete="off" onchange="toggleChoicesLayout($F(this));">
						<option value="">
							One Column
						</option>
						<option value="twoColumns">
							Two Columns
						</option>
						<option value="threeColumns">
							Three Columns
						</option>
						<option value="notStacked">
							Side by Side
						</option>
					</select>
				</li>
				<li class="right half" id="extra_listDateFormat">
					<label class="desc">
						Date Format
						<a href="#" class="tooltip" title="About Date Format" rel="Choose between American and European Date Formats">(?)</a>
					</label>
					<select class="select full" id="extra_fieldSize" autocomplete="off" onchange="changeFieldType($F(this), true);">
						<option id="extra_fieldDateAmerican" value="date" selected="selected">
							MM / DD / YYYY
						</option>
						<option id="extra_fieldDateEuro" value="eurodate">
							DD / MM / YYYY
						</option>
					</select>
				</li>
				<li class="right half" id="extra_listNameFormat">
					<label class="desc">
						Name Format
						<a href="#" class="tooltip" title="About Name Format" rel="Choose between a normal name field, or an extended name field with title and suffix.">(?)</a>
					</label>
					<select class="select full" id="extra_nameFormat" autocomplete="off" onchange="changeFieldType($F(this));">
						<option id="extra_fieldNameNormal" value="shortname" selected="selected">
							Normal
						</option>
						<option id="extra_fieldNameExtended" value="name">
							Extended
						</option>
					</select>
				</li>
				<li class="right half" id="extra_listPhoneFormat">
					<label class="desc">
						Phone Format
						<a href="#" class="tooltip" title="About Phone Format" rel="Choose between American and International Phone Formats">(?)</a>
					</label>
					<select class="select full" id="extra_fieldSize" autocomplete="off" onchange="changeFieldType($F(this), true);">
						<option id="extra_fieldPhoneAmerican" value="phone" selected="selected">
							### - ### - ####
						</option>
						<option id="extra_fieldPhoneEuro" value="europhone">
							International
						</option>
					</select>
				</li>
				<li class="right half" id="extra_listMoneyFormat">
					<label class="desc">
						Currency Format
					</label>
					<select class="select full" id="extra_fieldSize" autocomplete="off" onchange="updateProperties($F(this), 'Validation');">
						<option id="extra_fieldMoneyBaht" value="baht">
							&#3647; Baht
						</option>
						<option id="extra_fieldMoneyAmerican" value="" selected="selected">
							&#36; Dollars
						</option>
						<option id="extra_fieldMoneyEuro" value="euro">
							&#8364; Euros
						</option>
						<option id="extra_fieldMoneyForint" value="forint">
							&#70;&#116; Forint
						</option>
						<option id="extra_fieldMoneyFranc" value="franc">
							CHF Francs
						</option>
						<option id="extra_fieldMoneyKoruna" value="koruna">
							&#75;&#269; Koruna
						</option>
						<option id="extra_fieldMoneyKrona" value="krona">
							kr Krona
						</option>
						<option id="extra_fieldMoneyPesos" value="pesos">
							&#36; Pesos
						</option>
						<option id="extra_fieldMoneyPound" value="pound">
							&#163; Pounds Sterling
						</option>
						<option id="extra_fieldMoneyRinggit" value="ringgit">
							RM Ringgit
						</option>
						<option id="extra_fieldMoneyShekel" value="shekel">
							&#8362; Shekel
						</option>
						<option id="extra_fieldMoneyYen" value="yen">
							&#165; Yen
						</option>
						<option id="extra_fieldMoneyZloty" value="zloty">
							&#122;&#322; Zloty
						</option>
					</select>
				</li>
				<li class="clear noheight">
				</li>
				<li id="extra_listLikert" class="clear">
					<fieldset class="choices">
						<legend>
							Statements
							<a href="#" class="tooltip" title="About Statements" rel="">(?)</a>
						</legend>
						<ul id="extra_likertStatements">
							<li>
								<input class="text statement" type="text" value="Statement 1"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
							</li>
							<li>
								<input class="text statement" type="text" value="Statement 2"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this choice."/>
							</li>
							<li>
								<input class="text statement" type="text" value="Statement 3"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this choice."/>
							</li>
						</ul>
					</fieldset>
					<br />
					<fieldset class="choices">
						<legend>
							Columns
							<a href="#" class="tooltip" title="About Statements" rel="">(?)</a>
						</legend>
						<ul id="extra_likertColumns">
							<li>
								<input type="radio" title="Make this column pre-selected." />
								<input class="text col" type="text" value="Strongly Disagree"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another column."/>
							</li>
							<li>
								<input type="radio" title="Make this column pre-selected." />
								<input class="text col" type="text" value="Disagree"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this column."/>
							</li>
							<li>
								<input type="radio" title="Make this column pre-selected." />
								<input class="text col" type="text" value="Neither Agree nor Disagree"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this column."/>
							</li>
							<li>
								<input type="radio" title="Make this column pre-selected." />
								<input class="text" type="text" value="Agree"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this column."/>
							</li>
							<li>
								<input type="radio" title="Make this column pre-selected." />
								<input class="text" type="text" value="Strongly Agree"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this column."/>
							</li>
						</ul>
						<div id="extra_listBulkLikert">
							<input id="extra_bulkButton2" type="submit" class="translate" onclick="Lightbox.showUrl('lightboxes/FormBuilder.BulkLikert.php');return false;" value="Use Predefined Columns" />
						</div>
					</fieldset>
				</li>
				<li id="extra_listChoices" class="clear">
					<fieldset class="choices">
						<legend>
							Choices
							<a href="#" class="tooltip" title="About Choices" rel="This property shows what predefined options the user can select for that particular field. Use the plus and minus buttons to add and delete choices. Click on the star to make a choice the default selection. Multiple Choice fields will automatically select the first choice as default if none is specified.">(?)</a>
						</legend>
						<ul id="extra_fieldChoices">
							<li class="dropReq">
								<a href="#" class="tooltip" title="About Required Dropdowns" rel="This is the default down choice. When you make a drop down field required, 
								you are essentially telling the user they have to make a choice. If they leave 
								the drop down on the default choice (the one that is selected when the page loads), 
								Hoctudauwill consider that as not making a choice and will throw an error if not changed.">(?)</a>
								<input type="radio" title="Make this choice pre-selected." />
								<input class="text" type="text" value="If Dropdown Required"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this choice."/>
							</li>
							<li>
								<input type="radio" title="Make this choice pre-selected." />
								<input class="text" type="text" value="For Radio / Dropdown"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this choice."/>
							</li>
							<li>
								<input type="checkbox" title="Make this choice pre-selected." />
								<input class="text" type="text" value="For Checkbox"/>
								<img src="template/images/icons/add.png" alt="Add" title="Add another choice."/>
								<img src="template/images/icons/delete.png" alt="Delete" title="Delete this choice."/>
							</li>
						</ul>
						<div id="extra_listPopulate">
						</div>
					</fieldset>
				</li>
                <!----------------------------------
                * Display Answers
                ------------------------------------>
                <li id="extra_listInputAnswer" class="clear">
                    <label class="desc" for="extra_fieldInputRightAnswer">
						List Of Right Answer
						<a href="#" class="tooltip" title="Field Input Answer" rel="This is an optional property that the right answer will compare with, separate by enter">(?)</a>
					</label>
					<div>
                        <input id="extra_fieldInputRightAnswerRegex" type="checkbox" onkeyup="extra_updateProperties(this.value, 'InputRightAnswerRegex')" onmouseup="extra_updateProperties(this.value, 'InputRightAnswerRegex')" /> Use Regular Expression?
						<textarea class="textarea" rows="10" cols="50" id="extra_fieldInputRightAnswer" onkeyup="extra_updateProperties(this.value, 'InputRightAnswer')" onmouseup="extra_updateProperties(this.value, 'InputRightAnswer')"></textarea>
					</div>
                </li>
                <li id="extra_listDisplayAnswer" class="clear">
					<label class="desc" for="extra_fieldDisplayRightAnswer">
						Description For Right Answer
						<a href="#" class="tooltip" title="Field Display Answer" rel="This is an optional property that displays the text specified to your users while they're picking right answer">(?)</a>
					</label>
					<div>
						<textarea class="textarea" rows="10" cols="50" id="extra_fieldDisplayRightAnswer" onkeyup="extra_updateProperties(this.value, 'RightAnswerDescription')" onmouseup="extra_updateProperties(this.value, 'RightAnswerDescription')"></textarea>
					</div>
                    <label class="desc" for="extra_fieldDisplayWrongAnswer">
						Description For Wrong Answer
						<a href="#" class="tooltip" title="Field Display Answer" rel="This is an optional property that displays the text specified to your users while they're picking wrong answer">(?)</a>
					</label>
					<div>
						<textarea class="textarea" rows="10" cols="50" id="extra_fieldDisplayWrongAnswer" onkeyup="extra_updateProperties(this.value, 'WrongAnswerDescription')" onmouseup="extra_updateProperties(this.value, 'WrongAnswerDescription')"></textarea>
					</div>
				</li>
                <!----------------------------------
                ------------------------------------>
				<!--Button Settings-->
				<li id="extra_listNextButtons" class="asTxt">
					<fieldset class="choices">
						<legend>
							Primary Action
							<a href="#" class="tooltip" title="About Primary Action" rel="The primary action on a form is traditionally a submit button. ForHoctudau forms with multiple pages, the primary action
							takes the user to the next page and is always shown before the secondary action, which is used to take the user to the previous page.
							On the last page of the form, the style and look of the primary action to submit the entry is handled in the Theme Designer.">(?)</a>
						</legend>
						<ul>
							<li class="left half">
								<input id="extra_nextButtonString" name="extra_nextButtonType" type="radio" onclick="extra_showButtonText('Next');" />
								<label for="extra_nextButtonString">
									Use Text Button
								</label>
							</li>
							<li class="right half">
								<input id="extra_nextButtonLink" name="extra_nextButtonType" type="radio" onclick="extra_showButtonImage('Next');" />
								<label for="extra_nextButtonLink">
									Use Image for Button
								</label>
							</li>
							<li class="btxt clear">
								<label class="desc">
									What do you want your button to say?
								</label>
								<input id="extra_nextButtonsText" name="extra_nextButtonsText" class="text large" type="text" value="Next Page" onkeyup="extra_updateProperties(this.value, 'ChoicesText')" onmouseup="extra_updateProperties(this.value, 'ChoicesText')"
								/>
							</li>
							<li class="bimg clear">
								<label class="desc">
									Where's the image of your button on the web?
								</label>
								<input id="extra_nextButtonImg" name="extra_nextButtonImg" class="text large" type="text" value="http://" onkeyup="extra_updateProperties(this.value, 'ChoicesText')" onmouseup="extra_updateProperties(this.value, 'ChoicesText')"
								/>
							</li>
						</ul>
					</fieldset>
				</li>
				<li id="extra_listPreviousButtons" class="asTxt">
					<fieldset class="choices">
						<legend>
							Secondary Action
							<a href="#" class="tooltip" title="About Secondary Action" rel="ForHoctudau forms with multiple pages, the secondary action
							takes the user to the previous page and is always shown after the primary action, which is used to take the user to the next page.
							On the first page of a multi-page form, there is no secondary action shown because there are no previous pages before the first page.
							On the last page of the form, the style and look of the primary action to submit the entry is handled in the Theme Designer.">(?)</a>
						</legend>
						<ul>
							<li class="left half hide">
								<input id="extra_" name="extra_" type="radio" checked="checked" />
								<label>
									Show as Text
								</label>
							</li>
							<li class="btxt clear">
								<label class="desc">
									What do you want your link to say?
								</label>
								<input id="extra_previousButtonsText" name="extra_previousButtonsText" class="text large" type="text large" value="" onkeyup="extra_updateProperties(this.value, 'DefaultVal')" onmouseup="extra_updateProperties(this.value, 'DefaultVal')"
								/>
							</li>
						</ul>
					</fieldset>
				</li>
				<li class="left half clear" id="extra_listOptions">
					<fieldset>
						<legend>
							Options
						</legend>
						<input id="extra_fieldRequired" class="checkbox" type="checkbox" value="" onclick="(this.checked) ? checkVal = '1' : checkVal = '0';extra_updateProperties(checkVal, 'IsRequired')" />
						<label class="choice" for="extra_fieldRequired">
							Required
						</label>
						<a href="#" class="tooltip" title="About Required Validation" rel="If you want to make sure that a user fills out a particular field, use the Required validation to haveHoctudau check if the field is empty before submitting the entry. A message will be displayed to the user if they have not filled out the field. A red asterisk will appear to the right of the Field Title if the field is required.">(?)</a>
                        <br class="fieldExcerptToggle" />
						<input id="extra_fieldExcerpt" class="checkbox fieldExcerptToggle" type="checkbox" value="" onchange="(this.checked) ? checkVal = '1' : checkVal = '0';extra_updateProperties(checkVal, 'Excerpt')" />
						<label class="choice fieldExcerptToggle" for="extra_fieldExcerpt">
							Excerpt
						</label>
						<a href="#" class="tooltip fieldExcerptToggle" title="About No Duplicates" rel="Field Excerpt Workon Nopbai Form">(?)</a>
						<br class="fieldUniqueToggle" />
						<input id="extra_fieldUnique" class="checkbox fieldUniqueToggle" type="checkbox" value="" onchange="(this.checked) ? checkVal = '1' : checkVal = '0';extra_updateProperties(checkVal, 'IsUnique')" />
						<label class="undone choice fieldUniqueToggle" for="extra_fieldUnique">
							No Duplicates
						</label>
						<a href="#" class="tooltip fieldUniqueToggle" title="About No Duplicates" rel="Checking this validation will haveHoctudau verify that the information entered into this field is unique and has not been submitted previously. Useful for mailing lists and registration forms, where preventing the users from entering the same information more than once is often needed.">(?)</a>
						<br class="fieldEncryptedToggle" />
						<input id="extra_fieldEncrypted" class="checkbox fieldEncryptedToggle" type="checkbox" value="" onchange="(this.checked) ? checkVal = '1' : checkVal = '0';extra_updateProperties(checkVal, 'IsEncrypted')" />
						<label class="choice fieldEncryptedToggle" for="extra_fieldEncrypted">
							Encrypted
						</label>
						<a href="#" class="tooltip fieldEncryptedToggle" title="About Encryption" rel="An encrypted field is stored with additional security on our servers. You may have up to 5 encrypted fields. These fields are also NOT sent in email notifications. Ideal for collecting sensitive data.">(?)</a>
						<br class="fieldRandomToggle" />
						<input id="extra_fieldRandom" name="extra_randomize" class="checkbox fieldRandomToggle" type="checkbox" value="rand" onchange="(this.checked) ? checkVal = 'rand' : checkVal = '';extra_updateProperties(checkVal, 'Validation')"/>
						<label class="choice fieldRandomToggle" for="extra_fieldRandom">
							Randomize
						</label>
						<a href="#" class="tooltip fieldRandomToggle" title="About Random Order" rel="Check this option if you would like the choices to be shuffled around each time someone views your form.">(?)</a>
						<br class="fieldOtherToggle" />
						<input id="extra_fieldOther" name="extra_other" class="checkbox fieldOtherToggle translate" type="checkbox" value="other" onclick="(this.checked) ? checkVal = true : checkVal = false;extra_toggleRadioOther(checkVal)"/>
						<label class="choice fieldOtherToggle" for="extra_fieldOther">
							Allow Other
						</label>
						<a href="#" class="tooltip fieldOtherToggle" title="About Allow Other" rel="Check this option if you would like the last choice in your multiple choice field to have a text field for additional input.">(?)</a>
						<br class="fieldNumbersToggle" />
						<input id="extra_fieldNumbers" name="extra_numbers" class="checkbox fieldNumbersToggle" type="checkbox" value="hideNumbers" onclick="(this.checked) ? checkVal = true : checkVal = false;extra_toggleLikertNumbers(checkVal)"/>
						<label class="choice fieldNumbersToggle" for="extra_fieldNumbers">
							Hide Numbers
						</label>
						<a href="#" class="tooltip fieldNumbersToggle" title="About Showing Numbers" rel="Each option may have a numerical label underneath it. These labels represent the score of the option. Checking this box will toggle the labels on and off.">(?)</a>
						<br class="fieldNAToggle" />
						<input id="extra_fieldNA" name="extra_nonapplicable" class="checkbox fieldNAToggle" type="checkbox" value="na" onclick="(this.checked) ? checkVal = true : checkVal = false;extra_toggleLikertNonApplicable(checkVal)"/>
						<label class="choice fieldNAToggle" for="extra_fieldNA">
							Not Applicable
						</label>
						<a href="#" class="tooltip fieldNAToggle" title="About Non Applicable" rel="Check this option if you would like to add an additional column which allows the users to flag a choice as non applicable.">(?)</a>
                        <br class="fieldDCToggle" />
						<input id="extra_fieldDC" name="extra_datcheckbox" class="checkbox fieldDCToggle" type="checkbox" value="dc" onclick="(this.checked) ? checkVal = true : checkVal = false;extra_toggleLikertDatCheckbox(checkVal)"/>
						<label class="choice fieldDCToggle" for="extra_fieldDC">
							Switch to checkbox
						</label>
						<a href="#" class="tooltip fieldDCToggle" title="About Switch checkbox" rel="Check this option if you would like to switch to checkbox style.">(?)</a>
					</fieldset>
				</li>
				<li class="right half" id="extra_listSecurity">
					<fieldset>
						<legend>
							Show Field to
						</legend>
						<input id="extra_fieldPublic" name="extra_security" class="radio" type="radio" value="" checked="checked" onclick="extra_updateProperties('0', 'IsPrivate')" />
						<label class="choice" for="extra_fieldPublic">
							Everyone
						</label>
						<a href="#" class="tooltip" title="About Public Access" rel="Set the field to 'Public Access' if you would like the field to be accessible by anyone when the form is made public.">(?)</a>
						<br />
						<input id="extra_fieldPrivate" name="extra_security" class="radio" type="radio" value="" onclick="extra_updateProperties('1', 'IsPrivate')" />
						<label class="choice" for="extra_fieldPrivate">
							Admin Only
						</label>
						<a href="#" class="tooltip" title="About Admin Only" rel="Set the field to 'Admin Only' if you would like the field to ONLY be accessible via theHoctudau Admin interface. Fields that are set to 'Admin Only' will not be shown to users when the form is made public. Useful for forms that need to collect public submissions but then need to be ranked or added to privately in the Admin interface.">(?)</a>
					</fieldset>
				</li>
				<li class="clear noheight">
				</li>
				<li id="extra_listRange" class="complex clear">
					<fieldset class="minmax">
						<legend>
							Range
							<a href="#" class="tooltip" title="About Range" rel="Range allows you to specify limits on the data entered. For example, you can limit the amount of characters typed to be between 5 and 10. You can also substitute the character limit with that of words, or in the case of numbers, their values (x > y). If you do not wish to have a range limitation set, just leave the values blank or set them to 0.">(?)</a>
						</legend>
						<div>
							<span>
								<label class="desc" for="extra_fieldRangeMin">
									Min
								</label>
								<input id="extra_fieldRangeMin" name="extra_fieldRangeMin" class="text" type="text" value="" onkeyup="extra_updateProperties(this.value, 'RangeMin')" onmouseup="extra_updateProperties(this.value, 'RangeMin')" />
							</span>
							<span>
								<label class="desc" for="extra_fieldRangeMax">
									Max
								</label>
								<input id="extra_fieldRangeMax" name="extra_fieldRangeMax" class="text" type="text" value="" onkeyup="extra_updateProperties(this.value, 'RangeMax')" onmouseup="extra_updateProperties(this.value, 'RangeMax')" />
							</span>
							<span>
								<label class="desc" for="extra_fieldRangeType">
									Format
								</label>
								<select id="extra_fieldRangeType" name="extra_fieldRangeType" class="select" onchange="updateProperties(this.value, 'RangeType')">
									<option value="character">
										Characters
									</option>
									<option value="word">
										Words
									</option>
								</select>
							</span>
						</div>
					</fieldset>
				</li>
				<li class="clear" id="extra_listTextDefault">
					<label class="desc" for="extra_fieldDefault">
						Predefined Value
						<a href="#" class="tooltip" title="About Predefined Value" rel="By setting this value, the field will be prepopulated with the text you enter when a user comes to visit your form.">(?)</a>
					</label>
					<div>
						<input id="extra_fieldDefault" class="text large" type="text" name="extra_text" value="" maxlength="255" onkeyup="extra_updateProperties($F(this), 'DefaultVal')" onmouseup="extra_updateProperties($F(this), 'DefaultVal')" />
					</div>
				</li>
				<li class="clear" id="extra_listDateDefault">
					<label class="desc" for="extra_fieldDateDefault">
						Predefined Date
						<a href="#" class="tooltip" title="About Predefined Value" rel="By setting this value, the field will be prepopulated with the text you enter when a user comes to visit your form. For Date fields, acceptable formats are ##/##/#### or time based words such as 'today', 'tomorrow', '+1 week', 'last thursday'">(?)</a>
					</label>
					<div>
						<input id="extra_fieldDateDefault" class="text large" type="text" name="extra_text" value="" maxlength="255" onkeyup="extra_updateProperties($F(this), 'DefaultVal'); defaultDate($F(this));" onmouseup="extra_updateProperties($F(this), 'DefaultVal'); defaultDate($F(this));"
						/>
					</div>
				</li>
				<li class="clear" id="extra_listEuroDateDefault">
					<label class="desc" for="extra_fieldEuroDateDefault">
						Predefined Date
						<a href="#" class="tooltip" title="About Predefined Value" rel="By setting this value, the field will be prepopulated with the text you enter when a user comes to visit your form. For Date fields, acceptable formats are ##/##/#### or time based words such as 'today', 'tomorrow', '+1 week', 'last thursday'">(?)</a>
					</label>
					<div>
						<input id="extra_fieldEuroDateDefault" class="text large" type="text" name="extra_text" value="" maxlength="255" onkeyup="extra_updateProperties($F(this), 'DefaultVal'); defaultDate($F(this));" onmouseup="extra_updateProperties($F(this), 'DefaultVal'); defaultDate($F(this));"
						/>
					</div>
				</li>
				<li class="clear" id="extra_listPriceDefault">
					<label class="desc" for="extra_fieldPriceDefault">
						Predefined Price
						<a href="#" class="tooltip" title="About Predefined Value" rel="By setting this value, the field will be prepopulated with the price amount you enter.">(?)</a>
					</label>
					<div>
						<input id="extra_fieldPriceDefault" class="text large" type="text" name="extra_text" value="" maxlength="255" onkeyup="extra_updateProperties($F(this), 'DefaultVal'); defaultPrice($F(this));" onmouseup="extra_updateProperties($F(this), 'DefaultVal'); defaultPrice($F(this));"
						/>
					</div>
				</li>
				<li class="clear" id="extra_listAddressDefault">
					<label class="desc" for="extra_fieldAddressDefault">
						Predefined Country
						<a href="#" class="tooltip" title="About Predefined Country" rel="By setting this value, the country field will be prepopulated with the selection you make when a user visits your form.">(?)</a>
					</label>
					<div>
						<select class="select full" id="extra_fieldCountries" onchange="updateProperties($F(this), 'DefaultVal')">
							<option value="">
							</option>
							<optgroup label="North America">
								<option value="Antigua and Barbuda">
									Antigua and Barbuda
								</option>
								<option value="Aruba">
									Aruba
								</option>
								<option value="Bahamas">
									Bahamas
								</option>
								<option value="Barbados">
									Barbados
								</option>
								<option value="Belize">
									Belize
								</option>
								<option value="Bermuda">
									Bermuda
								</option>
								<option value="Canada">
									Canada
								</option>
								<option value="Cayman Islands">
									Cayman Islands
								</option>
								<option value="Cook Islands">
									Cook Islands
								</option>
								<option value="Costa Rica">
									Costa Rica
								</option>
								<option value="Cuba">
									Cuba
								</option>
								<option value="Dominica">
									Dominica
								</option>
								<option value="Dominican Republic">
									Dominican Republic
								</option>
								<option value="El Salvador">
									El Salvador
								</option>
								<option value="Grenada">
									Grenada
								</option>
								<option value="Guatemala">
									Guatemala
								</option>
								<option value="Haiti">
									Haiti
								</option>
								<option value="Honduras">
									Honduras
								</option>
								<option value="Jamaica">
									Jamaica
								</option>
								<option value="Mexico">
									Mexico
								</option>
								<option value="Netherlands Antilles">
									Netherlands Antilles
								</option>
								<option value="Nicaragua">
									Nicaragua
								</option>
								<option value="Panama">
									Panama
								</option>
								<option value="Puerto Rico">
									Puerto Rico
								</option>
								<option value="Saint Kitts and Nevis">
									Saint Kitts and Nevis
								</option>
								<option value="Saint Lucia">
									Saint Lucia
								</option>
								<option value="Saint Vincent and the Grenadines">
									Saint Vincent and the Grenadines
								</option>
								<option value="Trinidad and Tobago">
									Trinidad and Tobago
								</option>
								<option value="United States">
									United States
								</option>
								<option value="Virgin Islands, British">
									Virgin Islands, British
								</option>
								<option value="Virgin Islands, U.S.">
									Virgin Islands, U.S.
								</option>
							</optgroup>
							<optgroup label="South America">
								<option value="Argentina">
									Argentina
								</option>
								<option value="Bolivia">
									Bolivia
								</option>
								<option value="Brazil">
									Brazil
								</option>
								<option value="Chile">
									Chile
								</option>
								<option value="Colombia">
									Colombia
								</option>
								<option value="Ecuador">
									Ecuador
								</option>
								<option value="Guyana">
									Guyana
								</option>
								<option value="Paraguay">
									Paraguay
								</option>
								<option value="Peru">
									Peru
								</option>
								<option value="Suriname">
									Suriname
								</option>
								<option value="Uruguay">
									Uruguay
								</option>
								<option value="Venezuela">
									Venezuela
								</option>
							</optgroup>
							<optgroup label="Europe">
								<option value="Albania">
									Albania
								</option>
								<option value="Andorra">
									Andorra
								</option>
								<option value="Armenia">
									Armenia
								</option>
								<option value="Austria">
									Austria
								</option>
								<option value="Azerbaijan">
									Azerbaijan
								</option>
								<option value="Belarus">
									Belarus
								</option>
								<option value="Belgium">
									Belgium
								</option>
								<option value="Bosnia and Herzegovina">
									Bosnia and Herzegovina
								</option>
								<option value="Bulgaria">
									Bulgaria
								</option>
								<option value="Croatia">
									Croatia
								</option>
								<option value="Cyprus">
									Cyprus
								</option>
								<option value="Czech Republic">
									Czech Republic
								</option>
								<option value="Denmark">
									Denmark
								</option>
								<option value="Estonia">
									Estonia
								</option>
								<option value="Faroe Islands">
									Faroe Islands
								</option>
								<option value="Finland">
									Finland
								</option>
								<option value="France">
									France
								</option>
								<option value="Georgia">
									Georgia
								</option>
								<option value="Germany">
									Germany
								</option>
								<option value="Greece">
									Greece
								</option>
								<option value="Hungary">
									Hungary
								</option>
								<option value="Iceland">
									Iceland
								</option>
								<option value="Ireland">
									Ireland
								</option>
								<option value="Italy">
									Italy
								</option>
								<option value="Latvia">
									Latvia
								</option>
								<option value="Liechtenstein">
									Liechtenstein
								</option>
								<option value="Lithuania">
									Lithuania
								</option>
								<option value="Luxembourg">
									Luxembourg
								</option>
								<option value="Macedonia">
									Macedonia
								</option>
								<option value="Malta">
									Malta
								</option>
								<option value="Moldova">
									Moldova
								</option>
								<option value="Monaco">
									Monaco
								</option>
								<option value="Montenegro">
									Montenegro
								</option>
								<option value="Netherlands">
									Netherlands
								</option>
								<option value="Norway">
									Norway
								</option>
								<option value="Poland">
									Poland
								</option>
								<option value="Portugal">
									Portugal
								</option>
								<option value="Romania">
									Romania
								</option>
								<option value="San Marino">
									San Marino
								</option>
								<option value="Serbia">
									Serbia
								</option>
								<option value="Slovakia">
									Slovakia
								</option>
								<option value="Slovenia">
									Slovenia
								</option>
								<option value="Spain">
									Spain
								</option>
								<option value="Sweden">
									Sweden
								</option>
								<option value="Switzerland">
									Switzerland
								</option>
								<option value="Ukraine">
									Ukraine
								</option>
								<option value="United Kingdom">
									United Kingdom
								</option>
								<option value="Vatican City">
									Vatican City
								</option>
							</optgroup>
							<optgroup label="Asia">
								<option value="Afghanistan">
									Afghanistan
								</option>
								<option value="Bahrain">
									Bahrain
								</option>
								<option value="Bangladesh">
									Bangladesh
								</option>
								<option value="Bhutan">
									Bhutan
								</option>
								<option value="Brunei Darussalam">
									Brunei Darussalam
								</option>
								<option value="Myanmar">
									Myanmar
								</option>
								<option value="Cambodia">
									Cambodia
								</option>
								<option value="China">
									China
								</option>
								<option value="East Timor">
									East Timor
								</option>
								<option value="Hong Kong">
									Hong Kong
								</option>
								<option value="India">
									India
								</option>
								<option value="Indonesia">
									Indonesia
								</option>
								<option value="Iran">
									Iran
								</option>
								<option value="Iraq">
									Iraq
								</option>
								<option value="Israel">
									Israel
								</option>
								<option value="Japan">
									Japan
								</option>
								<option value="Jordan">
									Jordan
								</option>
								<option value="Kazakhstan">
									Kazakhstan
								</option>
								<option value="North Korea">
									North Korea
								</option>
								<option value="South Korea">
									South Korea
								</option>
								<option value="Kuwait">
									Kuwait
								</option>
								<option value="Kyrgyzstan">
									Kyrgyzstan
								</option>
								<option value="Laos">
									Laos
								</option>
								<option value="Lebanon">
									Lebanon
								</option>
								<option value="Malaysia">
									Malaysia
								</option>
								<option value="Maldives">
									Maldives
								</option>
								<option value="Mongolia">
									Mongolia
								</option>
								<option value="Nepal">
									Nepal
								</option>
								<option value="Oman">
									Oman
								</option>
								<option value="Pakistan">
									Pakistan
								</option>
								<option value="Palestine">
									Palestine
								</option>
								<option value="Philippines">
									Philippines
								</option>
								<option value="Qatar">
									Qatar
								</option>
								<option value="Russia">
									Russia
								</option>
								<option value="Saudi Arabia">
									Saudi Arabia
								</option>
								<option value="Singapore">
									Singapore
								</option>
								<option value="Sri Lanka">
									Sri Lanka
								</option>
								<option value="Syria">
									Syria
								</option>
								<option value="Taiwan">
									Taiwan
								</option>
								<option value="Tajikistan">
									Tajikistan
								</option>
								<option value="Thailand">
									Thailand
								</option>
								<option value="Turkey">
									Turkey
								</option>
								<option value="Turkmenistan">
									Turkmenistan
								</option>
								<option value="United Arab Emirates">
									United Arab Emirates
								</option>
								<option value="Uzbekistan">
									Uzbekistan
								</option>
								<option value="Vietnam">
									Vietnam
								</option>
								<option value="Yemen">
									Yemen
								</option>
							</optgroup>
							<optgroup label="Oceania">
								<option value="Australia">
									Australia
								</option>
								<option value="Fiji">
									Fiji
								</option>
								<option value="Kiribati">
									Kiribati
								</option>
								<option value="Marshall Islands">
									Marshall Islands
								</option>
								<option value="Micronesia">
									Micronesia
								</option>
								<option value="Nauru">
									Nauru
								</option>
								<option value="New Zealand">
									New Zealand
								</option>
								<option value="Palau">
									Palau
								</option>
								<option value="Papua New Guinea">
									Papua New Guinea
								</option>
								<option value="Samoa">
									Samoa
								</option>
								<option value="Solomon Islands">
									Solomon Islands
								</option>
								<option value="Tonga">
									Tonga
								</option>
								<option value="Tuvalu">
									Tuvalu
								</option>
								<option value="Vanuatu">
									Vanuatu
								</option>
							</optgroup>
							<optgroup label="Africa">
								<option value="Algeria">
									Algeria
								</option>
								<option value="Angola">
									Angola
								</option>
								<option value="Benin">
									Benin
								</option>
								<option value="Botswana">
									Botswana
								</option>
								<option value="Burkina Faso">
									Burkina Faso
								</option>
								<option value="Burundi">
									Burundi
								</option>
								<option value="Cameroon">
									Cameroon
								</option>
								<option value="Cape Verde">
									Cape Verde
								</option>
								<option value="Central African Republic">
									Central African Republic
								</option>
								<option value="Chad">
									Chad
								</option>
								<option value="Comoros">
									Comoros
								</option>
								<option value="Democratic Republic of the Congo">
									Democratic Republic of the Congo
								</option>
								<option value="Republic of the Congo">
									Republic of the Congo
								</option>
								<option value="Djibouti">
									Djibouti
								</option>
								<option value="Egypt">
									Egypt
								</option>
								<option value="Equatorial Guinea">
									Equatorial Guinea
								</option>
								<option value="Eritrea">
									Eritrea
								</option>
								<option value="Ethiopia">
									Ethiopia
								</option>
								<option value="Gabon">
									Gabon
								</option>
								<option value="Gambia">
									Gambia
								</option>
								<option value="Ghana">
									Ghana
								</option>
								<option value="Gibraltar">
									Gibraltar
								</option>
								<option value="Guinea">
									Guinea
								</option>
								<option value="Guinea-Bissau">
									Guinea-Bissau
								</option>
								<option value="C&ocirc;te d'Ivoire">
									C&ocirc;te d'Ivoire
								</option>
								<option value="Kenya">
									Kenya
								</option>
								<option value="Lesotho">
									Lesotho
								</option>
								<option value="Liberia">
									Liberia
								</option>
								<option value="Libya">
									Libya
								</option>
								<option value="Madagascar">
									Madagascar
								</option>
								<option value="Malawi">
									Malawi
								</option>
								<option value="Mali">
									Mali
								</option>
								<option value="Mauritania">
									Mauritania
								</option>
								<option value="Mauritius">
									Mauritius
								</option>
								<option value="Morocco">
									Morocco
								</option>
								<option value="Mozambique">
									Mozambique
								</option>
								<option value="Namibia">
									Namibia
								</option>
								<option value="Niger">
									Niger
								</option>
								<option value="Nigeria">
									Nigeria
								</option>
								<option value="Rwanda">
									Rwanda
								</option>
								<option value="Sao Tome and Principe">
									Sao Tome and Principe
								</option>
								<option value="Senegal">
									Senegal
								</option>
								<option value="Seychelles">
									Seychelles
								</option>
								<option value="Sierra Leone">
									Sierra Leone
								</option>
								<option value="Somalia">
									Somalia
								</option>
								<option value="South Africa">
									South Africa
								</option>
								<option value="Sudan">
									Sudan
								</option>
								<option value="Swaziland">
									Swaziland
								</option>
								<option value="United Republic of Tanzania">
									Tanzania
								</option>
								<option value="Togo">
									Togo
								</option>
								<option value="Tunisia">
									Tunisia
								</option>
								<option value="Uganda">
									Uganda
								</option>
								<option value="Zambia">
									Zambia
								</option>
								<option value="Zimbabwe">
									Zimbabwe
								</option>
							</optgroup>
						</select>
					</div>
				</li>
				<li class="clear" id="extra_listPhoneDefault">
					<label class="desc" for="extra_fieldPhoneDefault1">
						Predefined Value
						<a href="#" class="tooltip" title="About Predefined Value" rel="By setting this value, the field will be prepopulated with the text you enter when a user comes to visit your form.">(?)</a>
					</label>
					<div>
						<input id="extra_fieldPhoneDefault1" class="text" size="3" type="text" name="extra_text" value="" maxlength="3" onkeyup="extra_updateProperties($F('fieldPhoneDefault1').toString()+$F('fieldPhoneDefault2').toString()+$F('fieldPhoneDefault3').toString(), 'DefaultVal')"
						onmouseup="extra_updateProperties($F('fieldPhoneDefault1').toString()+$F('fieldPhoneDefault2').toString()+$F('fieldPhoneDefault3').toString(), 'DefaultVal')" />
						-
						<input id="extra_fieldPhoneDefault2" class="text" size="3" type="text" name="extra_text" value="" maxlength="3" onkeyup="extra_updateProperties($F('fieldPhoneDefault1').toString()+$F('fieldPhoneDefault2').toString()+$F('fieldPhoneDefault3').toString(), 'DefaultVal')"
						onmouseup="extra_updateProperties($F('fieldPhoneDefault1').toString()+$F('fieldPhoneDefault2').toString()+$F('fieldPhoneDefault3').toString(), 'DefaultVal')" />
						-
						<input id="extra_fieldPhoneDefault3" class="text" size="4" type="text" name="extra_text" value="" maxlength="4" onkeyup="extra_updateProperties($F('fieldPhoneDefault1').toString()+$F('fieldPhoneDefault2').toString()+$F('fieldPhoneDefault3').toString(), 'DefaultVal')"
						onmouseup="extra_updateProperties($F('fieldPhoneDefault1').toString()+$F('fieldPhoneDefault2').toString()+$F('fieldPhoneDefault3').toString(), 'DefaultVal')" />
					</div>
				</li>
				<li id="extra_listInstructions" class="clear">
					<label class="desc" for="extra_fieldInstructions">
						Instructions for User
						<a href="#" class="tooltip" title="Field Instructions" rel="This is an optional property that displays the text specified to your users while they're filling out that particular field. They appear off to the right side of the field.">(?)</a>
					</label>
					<div>
						<textarea class="textarea" rows="10" cols="50" id="extra_fieldInstructions" onkeyup="extra_updateProperties(this.value, 'Instructions')" onmouseup="extra_updateProperties(this.value, 'Instructions')">
						</textarea>
					</div>
				</li>
				<li id="extra_listClassNames" class="clear">
					<label class="desc" for="extra_fieldClassNames">
						Add CSS Layout Keywords
						<a href="/docs/" class="tooltip" title="About CSS Keywords" rel="For each field, you can specify CSS classnames to be added to the parent element surrounding the field to use as handles for your custom stylesheet using the advanced properties in the Theme Designer. You can also use built in keywords that activate styles built into theHoctudau Form Stylesheet like 'hide', 'leftHalf, 'rightHalf' and 'altInstruct'. These changes will NOT appear live in Form Builder--only on the live form and Entry Manager.">(?)</a>
					</label>
					<div>
						<input id="extra_fieldClassNames" class="text large" type="text" name="extra_text" value="" maxlength="255" onkeyup="extra_updateProperties($F(this), 'ClassNames');" onmouseup="extra_updateProperties($F(this), 'ClassNames');"
						/>
					</div>
					<span class="hide">
						<a href="#" onclick="extra_updateClassNames('hide'); return false;">hide</a>
						<a href="#" onclick="extra_updateClassNames('rightLabel'); return false;">rightLabel</a>
						<a href="#" onclick="extra_updateClassNames('leftLabel'); return false;">leftLabel</a>
					</span>
				</li>
				<li id="extra_listButtons" class="center">
					<a href="#" class="positive button" title="Duplicate this field." id="extra_listButtonsDuplicate">
					<img src="template/images/icons/add.png" alt=""/> Duplicate</a>
					<a href="#" class="negative button" title="Delete this field." id="extra_listButtonsDelete">
					<img src="template/images/icons/delete.png" alt=""/> Delete</a>
					<a href="#" class="button" title="Add another field." onclick="extra_showFields(true);return false">
					<img src="template/images/icons/textfield_add.png" alt=""/> Add Field</a>
				</li>
			</ul>
		</form>
    </div>
</div>
<div class="bulkButton" style="float: left;">
<a href="#" class="button" onclick="addFieldToForm(this); Lightbox.close(); return false;">
	<img src="template/images/icons/arrow_switch.png" alt="" />
	Add Group To Field
</a>
<a href="#" class="button" onclick="addGroupToForm(this); Lightbox.close(); return false;">
	<img src="template/images/icons/text_list_bullets.png" alt="" />
	Add Group To Form
</a>
</div>
<div class="bulkButton">
<a href="#" class="button" onclick="saveField(this); return false;">
	<img src="template/images/icons/table_save.png" alt="" />
	Save This Group
</a>
<a href="#" class="lbOff button negative">
	<img src="template/images/icons/cross.png" alt=""/> Cancel</a>
</div>
</div>
<script>
    getSavedFields();
    selectFieldGroup($('extra_fieldGroup0').down('a'));
</script>