
<div id="file-upload-popup" class="popup">
  <img src="template/images/demo/dialog-close-button.png" alt="close" class="close lbOff">

  <h2>Redirecting to an external web page is not available to FREE accounts.</h2>
  <p>This feature is only availble on our paid plans. You can either continue designing your form without it
    or if you want to use this feature now, save what you have so far and then go to the Account tab to upgrade.</p>

  <span>You can <b>upgrade</b> from here to use this feature.</span>
  <img src="template/images/demo/clickonaccount.png" alt="close" class="dialog-info-graphic">
</div>