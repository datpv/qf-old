<?php
$cate_id = isset($_GET['category_id'])?$_GET['category_id']:'';
?>
<form id="form1" name="f" class="prompt" action="#">
	<h2>Choose a New Name:</h2>
	<ul>
		<li>
			<div>
			<input id="cateName" name="cateName" class="text medium" type="text" value=""/> 
			</div>
			<p id="themeError"></p>
		</li>
		<li>
			<a href="#" class="button positive" onclick="categoryRename($F('cateName'), <?php echo $cate_id; ?>); return false;">
				<img src="template/images/icons/tick.png" alt=""/> Submit</a>
			<a href="#" class="lbOff button negative">
				<img src="template/images/icons/cross.png" alt=""/> Cancel</a>
		</li>
	</ul>
	<div>
		<input id="themeId" name="themeId" type="hidden" value=""/>
	</div>
</form>

<!-- JavaScript -->
<script type="text/javascript">
document.f.cateName.focus();
</script>