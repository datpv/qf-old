<form id="confirmDeleteBulk" class="confirm" action="#">
	<h2>Are you sure you want to <b>bulk delete</b> <strong id="deleteBulkCount" class="itemName"></strong>?</h2>
		
	<p>Bulk delete will <b>remove all data, files and comments</b> associated with your selected entries. 
	<b>All</b> your entries will be removed at this time. This action <b>cannot</b> be undone. If you're ready to delete in bulk, let us know.</p>
	
	<a href="#" id="confirmBulkDeletion" class="button" onclick="continueBulkDelete(); return false">
		<img src="template/images/icons/tick.png" alt=""/> Yes, I am ready. Please delete these entries.</a>
		
	<a href="#" class="lbOff negative button">
		<img src="template/images/icons/cross.png" alt=""/> Cancel</a>
</form>