<?php 
include('../config.php');
$siteUrl = __ABS_PATH.'/../index.php?req='; ?>
<div class="info">
<h2>Parting is such sweet sorrow</h2>
<div>Are you sure you want to logout?</div>
</div>
<ol class="decide">
<li class="green"><a href="<?php echo $siteUrl; ?>auth/logout"><span>1.</span> Yes, I need to <b>leave</b>.</a></li>
<li class="red"><a href="#" class="lbOff"><span>2.</span> No, I need to <b>go back</b>.</a></li>
</ol>