<form id="confirmDeleteForm" class="confirm" action="#">
	<h2>Are you sure you want to <b>delete</b> <strong id="deleteFormName" class="itemName"></strong>?</h2>
	
	<input id="deleteFormId" name="deleteFormId" type="hidden" value="" >
	
	<p>Deleting a form means that <b>all data collected by the form will be deleted immediately.</b> 
	Additionally, all files uploaded to the form and all reports referencing the form will no longer 
	be available. Because this action <b>cannot</b> be undone, you might want to consider 
	<a href="/docs/report-manager/#export" target="_blank">exporting
	your data</a> first. If that all sounds kosher, let us know.</p>
	
	<a href="#" id="confirmFormDeletion" class="button" onclick="continueDeletingForm(); return false">
		<img src="template/images/icons/tick.png" alt=""/> Yes, I am sure. Please delete this form.</a>
		
	<a href="#" class="lbOff negative button">
		<img src="template/images/icons/cross.png" alt=""/> Cancel</a>
</form>