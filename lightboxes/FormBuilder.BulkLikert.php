<div class="info">
<h2>Import from Predefined Choices</h2>
<h3><b>1)</b> Select a category. <b>2)</b> Customize as you see fit. <b>3)</b> Add to your likert field.</h3>
</div>

<table cellspacing="0">
	<tr>
		<td id="choiceMenu">
			<ul>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_agree2'); return false;">Agreement</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_compare2'); return false;">Comparison</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_important2'); return false;">Importance</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_satisfied2'); return false;">Satisfaction</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_wouldyou2'); return false;">Would You</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_ten'); return false;">Ten Scale</a></li>
			</ul>
		</td>
		<td class="flow">
			<div>&rarr;</div>
		</td>
		<td id="choiceText">
			<textarea id="prepop" name="prepop" class="textarea" rows="10" cols="50"></textarea>
		</td>
	</tr>
</table>

<div class="bulkButton">
	<a href="#" class="button" onclick="processBulkLikertAddition($F('prepop').split('\n')); Lightbox.close(); return false;">
		<img src="template/images/icons/text_list_bullets.png" alt="">
		Add Columns to Likert Field
	</a>
	<a href="#" class="lbOff button negative">
		<img src="template/images/icons/cross.png" alt=""/> Cancel</a>
</div>