
<div class="info">
<h2>Notification Email Customization</h2>
<div>Set the From: and Subject: Headers of your Notification Emails</div>
</div>

<form id="" action="#">
	<ul>
		<li class="complex">
			<label class="desc" for="fromAddress">Your Name or Company</label>
			<div>
			<span class="left">
			<input id="fromAddress" name="fromAddress" class="field text" type="text" maxlength="255" value="" /> 
			<label>e.g. <b>Wile E. Coyote</b> or <b>ACME Corporation</b></label>
			</span>
			<span class="half">
				<input id="notifyPlainText" name="notifyPlainText" class="checkbox field" type="checkbox" value="1" />
				<label class="choice" for="notifyPlainText">Send Emails in Plain Text</label>
			</span>
			</div>
		</li>
		
		<li>
			<label class="desc" for="messageSubject">Message Subject</label>
			<div>
			<input id="messageSubject" name="messageSubject" class="field text large" type="text" maxlength="255" value=""  /> 
			<label><a href="#" onclick="populateEmailOptionsDefaultSubject(); return false;">Reset to Default</a> or view <a href="/docs/templating/" target="_blank">Templating Docs</a> or view <a id="api" target="_blank" href="">API Settings</a> for this form.</label>
			
			</div>
		</li>
		
		<li class="buttons">
			<a href="#" class="button positive" onclick="processShowEmailOptionsLightbox(); return false;">
				<img src="template/images/icons/tick.png" alt=""/> Done</a>
		</li>
	</ul>
</form>