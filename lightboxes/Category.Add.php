<?php
$form_id = isset($_GET['form_id'])?$_GET['form_id']:'';
if(empty($form_id)) return;
include('../config.php');
$db_prefix = DB_PREFIX;
$mysqli = new mysqli($db['db_host'], $db['db_user'], $db['db_pass'], $db['db_name']);
if($mysqli->error) die('Database Connection Error!');
$mysqli->query("SET NAMES 'utf8'");
$mysqli->query("SET time_zone = 'Etc/GMT-7'");
date_default_timezone_set('Etc/GMT-7');
$categories = $mysqli->query("SELECT * FROM {$db_prefix}categories");
$categories_meta = $mysqli->query("SELECT * FROM {$db_prefix}categories_meta WHERE `form_id` = '$form_id'");
$cates = array();
while($category_meta = $categories_meta->fetch_assoc()) {
    $cates[] = $category_meta['category_id'];
}
?>
<form id="addCategoryForm" name="f" class="" action="#">
	<ul>
		<li>
			<label class="desc" for="s_categories">Categories:</label>
			<div>
                <select name="s_categories" id="s_categories" multiple="multiple">
                    <optgroup>
                        <option value="0"></option>
                        <?php
                        while($category = $categories->fetch_assoc()) {
                        ?>
                        <option <?php if(in_array($category['category_id'],$cates)) echo 'selected="selected"'; ?> value="<?php echo $category['category_id']; ?>"><?php echo $category['category_name'] ?></option>
                        <?php
                        }
                        ?>
                    </optgroup>
                </select>
			</div>
		</li>
		<li>
			<a href="#" id="addCategory" class="button" onclick="addCategory(<?php echo $form_id; ?>); return false">
				<img src="template/images/icons/tick.png" alt=""/> Save </a>
			<a href="#" class="lbOff negative button">
				<img src="template/images/icons/cross.png" alt=""/> Cancel </a>
		</li>
	</ul>
</form>