<div class="confirm">
	<h2><b>Whoa.</b> Read this First</h2>
	
	<p>Changing the form this report is based on will <b>REMOVE</b> all widgets and 
		existing data conditions set for this report. This cannot be undone. Are you sure you want to continue?</p>
		
	<a href="#" id="confirmFormDeletion" class="button" onclick="continueWithFormChange(); return false">
		<img src="../../images/icons/tick.png" alt=""/> Yes, I am sure. Please change the form.</a>
		
	<a href="#" class="negative button" onclick="cancelFormChange(); return false">
		<img src="../../images/icons/cross.png" alt=""/> Cancel</a>
</div>