<form id="changePasswordForm" name="f" class="prompt" action="#">
	<ul>
		<li>
			<label class="desc" for="password">Enter Your New Password</label>
			<div>
			<input id="password" name="password" class="text medium" type="password" value="" />
			</div>
		</li>
			
		<li>
			<label class="desc" for="confirm">Confirm New Password</label>
			<div>
			<input id="confirm" name="confirm" class="text medium" type="password" value="" />
			</div>
			<input type="hidden" name="userId" value="969766" id="userId">
			<p id="text" class="text highlight">&nbsp;</p>
		</li>
		<li>
			<a href="#" id="changePassword" class="button" onclick="changePassword(); return false">
				<img src="template/images/icons/tick.png" alt=""/> Save Password</a>
			<a href="#" class="lbOff negative button">
				<img src="template/images/icons/cross.png" alt=""/> Cancel</a>
		</li>
	</ul>
</form>

<!-- JavaScript -->
<script type="text/javascript">
document.f.password.focus();
</script>