<div class="info">
<h2>Awesome. Your report is saved.</h2>
<div>What do you want to do now?</div>
</div>

<ol class="decide">
<li class="blue"><a href="#" onclick="continueEditingReport(); return false;"><span>1.</span> <b>Continue editing</b> this report.</a></li>
<li class="green"><a href="#" onclick="viewPublicReport(); return false;"><span>2.</span> Take a <b>look at it</b> in a new window.</a></li>
<li class="red"><a href="../../reports/"><span>3.</span> I'm finished! Take me back to the <b>Report Manager</b>.</a></li>
</ol>