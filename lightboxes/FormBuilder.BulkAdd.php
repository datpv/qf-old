<div class="info">
<h2>Import Predefined Choices</h2>
<div><b>1)</b> Select a category. <b>2)</b> Customize as you see fit. <b>3)</b> Add to your field.</div>
</div>

<table cellspacing="0">
	<tr>
		<td id="choiceMenu">
			<ul>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_gender'); return false;">Gender</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_age'); return false;">Age</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_employment'); return false;">Employment</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_income'); return false;">Income</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_education'); return false;">Education</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_days'); return false;">Days of the Week</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_months'); return false;">Months of the Year</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_timezones'); return false;">Timezones</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_states'); return false;">U.S. States</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_continents'); return false;">Continents</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_howoften'); return false;">How Often</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_howlong'); return false;">How Long</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_satisfied'); return false;">Satisfaction</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_important'); return false;">Importance</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_agree'); return false;">Agreement</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_compare'); return false;">Comparison</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_wouldyou'); return false;">Would You</a></li>
				<li><a href="#" onclick="$('prepop').value = getBulkSet('pre_football'); return false;">NFL Teams</a></li>
			</ul>
		</td>
		<td class="flow">
			<div>&rarr;</div>
		</td>
		<td id="choiceText">
				<textarea id="prepop" name="prepop" class="textarea" rows="10" cols="50"></textarea>
		</td>
	</tr>
</table>

<div class="bulkButton">
<a href="#" class="button" onclick="processBulkAddition($F('prepop').split('\n')); Lightbox.close(); return false;">
	<img src="template/images/icons/text_list_bullets.png" alt="">
	Add Choices to Field
</a>
<a href="#" class="lbOff button negative">
	<img src="template/images/icons/cross.png" alt=""/> Cancel</a>
</div>