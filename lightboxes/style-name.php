<form id="form1" name="f" class="prompt" action="#" onsubmit="saveNewTheme($F('theme')); return false">
	<h2>
		Specify a Name for Your Theme
	</h2>
	<ul>
		<li>
			<div>
				<input id="theme" name="theme" class="text" type="text" value=""/>
			</div>
			<p id="themeError" class="error">
			</p>
		</li>
		<li class="buttons">
			<a href="#" class="button positive" onclick="saveNewTheme($F('theme')); return false">
			<img src="template/images/icons/tick.png" alt=""/> Submit</a>
			<a href="#" class="button negative lbOff">
			<img src="template/images/icons/cross.png" alt=""/> Cancel</a>
		</li>
	</ul>
</form>
<!-- JavaScript -->
<script type="text/javascript">
	
document.f.theme.focus();

</script>