<?php
if ( ! defined( 'GETOVER' ) ) exit;
    abstract class Applications_BaseController {
        protected $registry, $db_prefix = DB_PREFIX, $mysqli, $view, $helpers, $allowNext = false, $db, $options = array();
        function __construct($registry) {
            $this->registry = $registry;
            $this->db = $registry->db;
            $this->mysqli = $registry->db->getMysqli();
            $this->view = $registry->template;
            $this->helpers = $registry->helpers;
            $this->options = $registry->options;
        }
        abstract function index();
        
        function setAllowNext($bool) {
            $this->allowNext = $bool;
        }
        function getAllowNext() {
            return $this->allowNext;
        }
        function postReq() {
            
        }
        function startSession() {
            session_start();
        }
        function checkPermission() {
            
        }
        function pauseSession() {
            session_write_close();
        }
    }