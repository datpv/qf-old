<?php
if ( ! defined( 'GETOVER' ) ) exit;
    class Applications_Registry {
        private $vars = array();
        function __set($index, $val) {
            $this->vars[$index] = $val;
        }
        function __get($index) {
            return $this->vars[$index];
        }
        function clear($index) {
            unset($this->vars[$index]);
        }
        function clearAll() {
            $this->vars = array();
        }
    }