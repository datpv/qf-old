<?php
if ( ! defined( 'GETOVER' ) ) exit;
    
    class Applications_Router {
        private $registry, 
        $path,
        $file,
        $controller,
        $action,
        $id, $id2;
        public $paged = 1, $f = '';
        private $action_prebuild = array(
            'importer' => array(
                'manager',
                'previewer',
                'builder'
            ),
            'report' => array(
                'auto_builder',
                'builder'
            ),
            'auth' => array(
                'login',
                'logout',
                'signup'
            ),
            'themes' => array(
                'loadThemes',
                'save',
                'rename',
                'delete'
            ),
            'subjects' => array(
                'manager',
                'all',
                'view',
                'registered',
                'builder'
            )
        );
        private $unroute = array(
            'lightboxes'
        );
        private $halfUnroute = array(
        );
        function __construct($registry) {
            $this->registry = $registry;
        }
        function setRouters($args) {
            if(isset($args['controller'])) $this->controller = $args['controller'];
            if(isset($args['action'])) $this->action = $args['action'];
        }
        function getRouters() {
            return array(
                'controller' => $this->controller,
                'action' => $this->action,
                'id' => $this->id,
                'id2' => $this->id2
            );
        }
        function setPath($path) {
            $this->path = $path;
            if(!is_dir($path)) {
                $this->path = __SITE_PATH.'/controllers';
            }
        }
        // get controller, action, id
        function getController() {
            $req = isset($_GET['req'])?$_GET['req']:'index';
            $paged = isset($_GET['paged'])?$_GET['paged']:'1';
            $f = isset($_GET['f'])?$_GET['f']:'';
            if(empty($paged) || !is_numeric($paged) || $paged <= 0) $paged = 1;
            $this->paged = $paged;
            $this->f = $f;
            $path = explode('/', $req);
            $controller = isset($path[0])?$path[0]:'index';
            $action = $id = isset($path[1])?$path[1]:null;
            $id2 = isset($path[2])?$path[2]:null;
            $this->controller = $controller;
            $this->action = 'index';
            if(in_array($controller, $this->unroute)) {
                return false;
            }
            if(array_key_exists($controller, $this->halfUnroute)) {
                if(in_array($action, $this->halfUnroute[$controller])) {
                    return false;
                }
            }
            if(array_key_exists($controller, $this->action_prebuild)) {
                if(in_array($action, $this->action_prebuild[$controller])) {
                    $this->action = $action;
                }
                $id = isset($path[2])?$path[2]:null;
                $id2 = isset($path[3])?$path[3]:null;
            }
            if(!empty($id)) {
                $part = explode('-', $id);
                $id = $part[0];
            }
            if(!empty($id2)) {
                $part2 = explode('-', $id2);
                $id2 = $part2[0];
            }
            $this->id = $id;
            $this->id2 = $id2;
            return $this->file = $this->path.'/'.$this->controller.'Controller.class.php';
             
        }
		// load controller, action and call its
        function loader() {
            if(!$this->getController()) {
                return;
            }
            if(!is_readable($this->file)) {
                $this->controller = 'notfound';
                $this->action = 'index';
                $this->file = $this->path.'/'.$this->controller.'Controller.class.php';
            }
            $class = 'Controllers_' . $this->controller.'Controller';
            
            $control = new $class($this->registry);
            
            $action = $this->action;
            if(!is_callable(array($control, $this->action))) {
                $action = 'index';
            }
            $control->checkPermission();
            $next = true;
            if($_SERVER['REQUEST_METHOD'] == 'POST') {
                $next = false;
                $this->registry->req = $_REQUEST;
                $control->postReq();
                $next = $control->getAllowNext();
            }
            if(!$next) exit();
            $control->$action($this->id);
        }
    }