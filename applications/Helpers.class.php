<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Applications_Helpers {
    private $registry, $mysqli, $options = array();
    function __construct($registry) {
        $this->registry = $registry;
        $this->mysqli = $registry->db->getMysqli();
        $this->options = $registry->options;
    }
    public function getCookies() {
        $sort_by_as = $sort_by_nb = $sort_by_sced = $sort_by_nbed = 'cache_entry_id';
        $active_sort_as = $active_sort_nb = $active_sort_sced = $active_sort_nbed = 'dc';
        $sort_dir_as = $sort_dir_nb = $sort_dir_sced = $sort_dir_nbed = 'ASC';
        $active_dir_as = $active_dir_nb = $active_dir_sced = $active_dir_nbed = 'asc';
        /*
        *
        */
        if(isset($_COOKIE['as_sort'])) {
            if($_COOKIE['as_sort'] == 'dc') {$sort_by_as = 'cache_entry_id';$active_sort_as = 'dc';} 
            elseif($_COOKIE['as_sort'] == 'sys') {$sort_by_as = 'cache_entry_sys';$active_sort_as = 'sys';}
        }
        if(isset($_COOKIE['as_dir'])) {
            if($_COOKIE['as_dir'] == 'asc') {$sort_dir_as = 'ASC';$active_dir_as = 'asc';} 
            elseif($_COOKIE['as_dir'] == 'desc') {$sort_dir_as = 'DESC';$active_dir_as = 'desc';}
        }
        /*
        *
        */
        if(isset($_COOKIE['nb_sort'])) {
            if($_COOKIE['nb_sort'] == 'dc') {$sort_by_nb = 'cache_entry_id';$active_sort_nb = 'dc';} 
            elseif($_COOKIE['nb_sort'] == 't') {$sort_by_nb = 'cache_entry_t';$active_sort_nb = 't';} 
            elseif($_COOKIE['nb_sort'] == 'hs') {$sort_by_nb = 'cache_entry_hs';$active_sort_nb = 'hs';} 
            elseif($_COOKIE['nb_sort'] == 'tg') {$sort_by_nb = 'cache_entry_tg';$active_sort_nb = 'tg';}
        }
        if(isset($_COOKIE['nb_dir'])) {
            if($_COOKIE['nb_dir'] == 'asc') {$sort_dir_nb = 'ASC';$active_dir_nb = 'asc';} 
            elseif($_COOKIE['nb_dir'] == 'desc') {$sort_dir_nb = 'DESC';$active_dir_nb = 'desc';}
        }
        /*
        *
        */
        if(isset($_COOKIE['sced_sort'])) {
            if($_COOKIE['sced_sort'] == 'dc') {$sort_by_sced = 'cache_entry_id';$active_sort_sced = 'dc';} 
            elseif($_COOKIE['sced_sort'] == 't') {$sort_by_sced = 'cache_entry_t';$active_sort_sced = 't';} 
            elseif($_COOKIE['sced_sort'] == 'hs') {$sort_by_sced = 'cache_entry_hs';$active_sort_sced = 'hs';} 
            elseif($_COOKIE['sced_sort'] == 'tg') {$sort_by_sced = 'cache_entry_tg';$active_sort_sced = 'tg';}
        }
        if(isset($_COOKIE['sced_dir'])) {
            if($_COOKIE['sced_dir'] == 'asc') {$sort_dir_sced = 'ASC';$active_dir_sced = 'asc';} 
            elseif($_COOKIE['sced_dir'] == 'desc') {$sort_dir_sced = 'DESC';$active_dir_sced = 'desc';}
        }
        /*
        *
        */
        if(isset($_COOKIE['nbed_sort'])) {
            if($_COOKIE['nbed_sort'] == 'dc') {$sort_by_nbed = 'cache_entry_id';$active_sort_nbed = 'dc';} 
            elseif($_COOKIE['nbed_sort'] == 't') {$sort_by_nbed = 'cache_entry_t';$active_sort_nbed = 't';} 
            elseif($_COOKIE['nbed_sort'] == 'hs') {$sort_by_nbed = 'cache_entry_hs';$active_sort_nbed = 'hs';} 
            elseif($_COOKIE['nbed_sort'] == 'tg') {$sort_by_nbed = 'cache_entry_tg';$active_sort_nbed = 'tg';}
        }
        if(isset($_COOKIE['nbed_dir'])) {
            if($_COOKIE['nbed_dir'] == 'asc') {$sort_dir_nbed = 'ASC';$active_dir_nbed = 'asc';} 
            elseif($_COOKIE['nbed_dir'] == 'desc') {$sort_dir_nbed = 'DESC';$active_dir_nbed = 'desc';}
        }
        $cookies = array(
            'sort_by_as' => $sort_by_as, 'sort_by_nb' => $sort_by_nb, 'sort_by_sced' => $sort_by_sced, 'sort_by_nbed' => $sort_by_nbed,
            'active_sort_as' => $active_sort_as, 'active_sort_nb' => $active_sort_nb, 'active_sort_sced' => $active_sort_sced, 'active_sort_nbed' => $active_sort_nbed,
            'sort_dir_as' => $sort_dir_as, 'sort_dir_nb' => $sort_dir_nb, 'sort_dir_sced' => $sort_dir_sced, 'sort_dir_nbed' => $sort_dir_nbed,
            'active_dir_as' => $active_dir_as, 'active_dir_nb' => $active_dir_nb, 'active_dir_sced' => $active_dir_sced, 'active_dir_nbed' => $active_dir_nbed
        );
        return $cookies;
    }
    function writeSession($session_name,$session_value) {
        $_SESSION[$session_name] = $session_value;
    }
    function extract_array($args) {
        extract($args);
    }
    function isAdmin() {
        return isset($_SESSION['admin_user'])?true:false;
    }
    function isMod() {
        return isset($_SESSION['mod_user'])?true:false;
    }
    function isUser() {
        return isset($_SESSION['user_normal'])?true:false;
    }
    function getUserByParam($param = 'user_id') {
        $user_id = isset($_SESSION['user_normal'])?$_SESSION['user_normal']:'0';
        if(isset($_SESSION['admin_user'])) {
            $user_id = isset($_GET[$param])?$_GET[$param]:'0';
            if(empty($user_id)) {
                $user_id = isset($_SESSION['admin_user'])?$_SESSION['admin_user']:'0';
            }
        }
        return $user_id;
    }
    function getUserByAdmin() {
        $user_id = isset($_SESSION['admin_user'])?$_SESSION['admin_user']:'';
        if(isset($_SESSION['user_normal'])) {
            $user_id = $_SESSION['user_normal'];
        }
        return $user_id;
    }
    function getCurrentUser() {
        $user_id = isset($_SESSION['user_normal'])?$_SESSION['user_normal']:'';
        if(isset($_SESSION['admin_user'])) {
            $user_id = $_SESSION['admin_user'];
        }
        return $user_id;
    }
    function objectToArray($d) {
        if (is_object($d)) {
        	$d = get_object_vars($d);
        }
        if (is_array($d)) {
        	return array_map(array(__CLASS__,__METHOD__), $d);
        }
        else {
        	return $d;
        }
    }
    function escape_string($input) {
        $input = $this->objectToArray($input);
        if(is_array($input)) {
            foreach($input as $key => $v) {
                if(is_array($input[$key])) {
                    $input[$key] = $this->escape_string($input[$key]);
                } elseif(is_string($v)) {
                    $input[$key] = $this->mysqli->real_escape_string(trim($v));
                }
            }
        } elseif(is_string($input)) {
            $input = $this->mysqli->real_escape_string(trim($input));
        }
        return $input;
    }
    function escapeString($input) {
        $input = $this->objectToArray($input);
        if(is_array($input)) {
            foreach($input as $key => $v) {
                if(is_array($input[$key])) {
                    $input[$key] = $this->escape_string($input[$key]);
                } elseif(is_string($v)) {
                    $input[$key] = $this->mysqli->real_escape_string(trim($v));
                }
            }
        } elseif(is_string($input)) {
            $input = $this->mysqli->real_escape_string(trim($input));
        }
        return $input;
    }
    function trim_string($input, $trim_what = ' ') {
        $input = $this->objectToArray($input);
        if(is_array($input)) {
            foreach($input as $key => $v) {
                if(is_array($input[$key])) {
                    $input[$key] = $this->trim_string($input[$key]);
                } elseif(is_string($v)) {
                    $input[$key] = preg_replace('/\s+/', $trim_what, trim($v));
                } 
            }
        } elseif(is_string($input)) {
            $input = preg_replace('/\s+/', $trim_what, trim($input));
        }
        
        return $input;
    }
    public function clean_string($str, $d = '-') {
    	// Replace umlauts with their basic latin representation
    	$chars = array(
    		' ' => $d,
    		'\'' => '',
    		'"' => '',
    		'\'' => '',
    		'&' => $d,
    		"\xc3\xa5" => 'aa',
    		"\xc3\xa6" => 'ae',
    		"\xc3\xb6" => 'oe',
    		"\xc3\x85" => 'aa',
    		"\xc3\x86" => 'ae',
    		"\xc3\x96" => 'oe',
    	);
    	//$str = html_entity_decode($str, ENT_QUOTES, 'UTF-8'); // convert html special chars back to original chars
    	$str = str_replace(array_keys($chars), $chars, $str);
    	$result = false;
    	$str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
    	if (!empty($str)) {
    		$str = strtr($str, array("\xc5\xA5" => 't', "\xc4\xBE" => 'l', "\xc5\xA4" => 'T', "\xc4\xBD" => 'L', "\xc4\x84" => 'A', "\xc4\x85" => 'a', "\xc3\xa1" => 'a', "\xc3\x81" => 'A', "\xc3\xa0" => 'a', "\xc3\x80" => 'A', "\xc3\xa2" => 'a', "\xc3\x82" => 'A', "\xc3\xa3" => 'a', "\xc3\x83" => 'A', "\xc2\xaa" => 'a', "\xc4\x8c" => 'C', "\xc4\x8d" => 'c', "\xc3\xa7" => 'c', "\xc3\x87" => 'C', "\xc3\xa9" => 'e', "\xc3\x89" => 'E', "\xc3\xa8" => 'e', "\xc3\x88" => 'E', "\xc3\xaa" => 'e', "\xc3\x8a" => 'E', "\xc3\xab" => 'e', "\xc3\x8b" =>'E', "\xc4\x98" => 'E', "\xc4\x99" => 'e', "\xc4\x9a" => 'E', "\xc4\x9b" => 'e', "\xc4\x8f" => 'd', "\xc3\xad" => 'i', "\xc3\x8d" => 'I', "\xc3\xac" => 'i', "\xc3\x8c" => 'I', "\xc3\xae" => 'i', "\xc3\x8e" => 'I', "\xc3\xaf" => 'i', "\xc3\x8f" => 'I', "\xc4\xb9" => 'L', "\xc4\xba" => 'l', "\xc4\xbe" => 'l', "\xc5\x87" => 'N', "\xc5\x88" => 'n', "\xc3\xb1" => 'n', "\xc3\x91" => 'N', "\xc3\xb3" => 'o', "\xc3\x93" => 'O', "\xc3\xb2" => 'o', "\xc3\x92" => 'O', "\xc3\xb4" => 'o', "\xc3\x94" => 'O', "\xc3\xb5" => 'o', "\xc3\x95" => 'O', "\xd4\xa5" => 'o', "\xc3\x98" => 'O', "\xc2\xba" => 'o', "\xc3\xb0" => 'o', "\xc5\x94" => 'R', "\xc5\x95" => 'r', "\xc5\x98" => 'R', "\xc5\x99" => 'r', "\xc5\xa0" => 'S', "\xc5\xa1" => 's', "\xc5\xa5" => 't', "\xc3\xba" => 'u', "\xc3\x9a" => 'U', "\xc3\xb9" => 'u', "\xc3\x99" => 'U', "\xc3\xbb" => 'u', "\xc3\x9b" => 'U', "\xc3\xbc" => 'u', "\xc3\x9c" => 'U', "\xc5\xae" => 'U', "\xc5\xaf" => 'u', "\xc3\xbd" => 'y', "\xc3\x9d" => 'Y', "\xc3\xbf" => 'y', "\xc3\xa4" => 'a', "\xc3\x84" => 'A', "\xc3\x9f" => 's', "\xc5\xbd" => 'Z', "\xc5\xbe" => 'z', '?' => '-', ' ' => '-', '/' => '-', '&' => '-', '(' => '-', ')' => '-', '[' => '-', ']' => '-', '%' => '-', '#' => '-', ',' => '-', ':' => '-'));
    		$str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
            $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
            $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
            $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
            $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
            $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
            $str = preg_replace("/(đ)/", 'd', $str);
            $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
            $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
            $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
            $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
            $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
            $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
            $str = preg_replace("/(Đ)/", 'D', $str);
    		$str = strtolower($str); // only lower letters
    		$str = preg_replace("/($d){2,}/", $d, $str); // replace double (and more) dashes with one dash
    		$str = preg_replace("/[^a-z0-9-\.]/", '', $str); // URL can contain latin letters, numbers, dashes and points only
    		$result = trim($str, '-'); // remove trailing dash if exist
    	}
        return $result;
    }
    function unescape_string($input) {
        $input = $this->objectToArray($input);
        if(is_array($input)) {
            foreach($input as $key => $v) {
                if(is_array($input[$key])) {
                    $input[$key] = $this->unescape_string($input[$key]);
                } elseif(is_string($v)) {
                    $input[$key] = stripslashes(trim($v));
                } 
            }
        } elseif(is_string($input)) {
            $input = stripslashes(trim($input));
        }
        return $input;
    }
    /**
     * @brief Generates a Universally Unique IDentifier, version 4.
     *
     * This function generates a truly random UUID. The built in CakePHP String::uuid() function
     * is not cryptographically secure. You should uses this function instead.
     *
     * @see http://tools.ietf.org/html/rfc4122#section-4.4
     * @see http://en.wikipedia.org/wiki/UUID
     * @return string A UUID, made up of 32 hex digits and 4 hyphens.
     */
    function uuidSecure() {
      
        $pr_bits = null;
        $fp = @fopen('/dev/urandom','rb');
        if ($fp !== false) {
            $pr_bits .= @fread($fp, 16);
            @fclose($fp);
        } else {
            // If /dev/urandom isn't available (eg: in non-unix systems), use mt_rand().
            $pr_bits = "";
            for($cnt=0; $cnt < 16; $cnt++){
                $pr_bits .= chr(mt_rand(0, 255));
            }
        }
      
        $time_low = bin2hex(substr($pr_bits,0, 4));
        $time_mid = bin2hex(substr($pr_bits,4, 2));
        $time_hi_and_version = bin2hex(substr($pr_bits,6, 2));
        $clock_seq_hi_and_reserved = bin2hex(substr($pr_bits,8, 2));
        $node = bin2hex(substr($pr_bits,10, 6));
      
        /**
         * Set the four most significant bits (bits 12 through 15) of the
         * time_hi_and_version field to the 4-bit version number from
         * Section 4.1.3.
         * @see http://tools.ietf.org/html/rfc4122#section-4.1.3
         */
        $time_hi_and_version = hexdec($time_hi_and_version);
        $time_hi_and_version = $time_hi_and_version >> 4;
        $time_hi_and_version = $time_hi_and_version | 0x4000;
      
        /**
         * Set the two most significant bits (bits 6 and 7) of the
         * clock_seq_hi_and_reserved to zero and one, respectively.
         */
        $clock_seq_hi_and_reserved = hexdec($clock_seq_hi_and_reserved);
        $clock_seq_hi_and_reserved = $clock_seq_hi_and_reserved >> 2;
        $clock_seq_hi_and_reserved = $clock_seq_hi_and_reserved | 0x8000;
      
        return sprintf('%08s%04s%04x%04x%012s',
            $time_low, $time_mid, $time_hi_and_version, $clock_seq_hi_and_reserved, $node);
    }
    function aasort (&$array, $key) {
        $sorter=array();
        $ret=array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii]=$array[$ii];
        }
        $array=$ret;
    }
    function get_datetime() {
        return date( 'Y-m-d H:i:s' );
    }
    function getField($fields, $fieldName) {
        foreach($fields as $field) {
            if($field['Typeof'] == 'likert') {
                foreach($field['SubFields'] as $subF) {
                    if('Field'.$subF['ColumnId'] == $fieldName) {
                        return $field;
                    }
                }
            }
            if('Field'.$field['ColumnId'] == $fieldName) {
                return $field;
            }
        }
    }
    function getChoices($field, $entries, $gFieldName) {
        $fieldId = 'Field'.$field['ColumnId'];
        $type = $field['Typeof'];
        $validation = isset($field['Validation'])?$field['Validation']:'';
        $choices = array(
            'total' => 0,
            'choices' => array(),
            'unanswered' => null
        );
        if($type == 'likert') {
            $choice2 = $choice = array();$count = array(); $nullV = $total = 0;
            if($validation == 'dc') {
                $j = $i = 0;
                foreach($entries as $k => $entry) {
                    foreach($field['Choices'] as $ks => $subF) {
                        $j++;
                        if($k == $gFieldName.'_'.($ks+1)) {
                            $choice = $this->calEntry($entry);
                            $choice = array_merge($choice,$choice2);
                            $choice2 = $choice;
                            $count[] = count($entry);
                            $total += count($entry);
                        } else {
                            $i++;
                        }
                    }
                }
                $choices['unanswered']  = !empty($count)?max($count):0;
                $choices['total']  = $total;
                $choices['choices'] = $choice;
                return $choices;
            } else {
                foreach($entries as $k => $entry) {
                    if($k == $gFieldName) {
                        $choice = $this->calEntry($entry);
                        $choice = array_merge($choice,$choice2);
                        $choice2 = $choice;
                        $count[] = count($entry);
                        $total += count($entry);
                    }
                }
                $choices['unanswered']  = !empty($count)?max($count):0;
                $choices['total']  = $total;
                $choices['choices'] = $choice;
                return $choices;
            }
        }
        //if($type == 'text' || $type == 'number' || $type == 'textarea' || $type == 'select' || $type == 'radio' || $type == 'likert') {
        if($type != 'checkbox') {
            foreach($entries as $k => $entry) {
                if($k == $fieldId) {
                    $choice = $this->calEntry($entry);
                    $nullV = isset($choice[null])?$choice[null]:0;
                    $choices['total']  = count($entry) - $nullV;
                    $choices['unanswered']  = $nullV;
                    if(isset($choice[null])) unset($choice[null]);
                    $choices['choices'] = $choice;
                    return $choices;
                }
            }
        } elseif(isset($field['SubFields'])) {
            $choice2 = $choice = array();$count = array(); $total = 0;
            foreach($field['SubFields'] as $subF) {
                foreach($entries as $k => $entry) {
                    if($k == 'Field'.$subF['ColumnId']) {
                        $choice = $this->calEntry($entry);
                        $choice = array_merge($choice,$choice2);
                        $choice2 = $choice;
                        $count[] = count($entry);
                        $total += count($entry);
                    }
                }
            }
            $choices['unanswered']  = !empty($count)?max($count):0;
            $choices['total']  = $total;
            $choices['choices'] = $choice;
            return $choices;
        }
    }
    function calEntry($entry) {
        return (array_count_values($entry));
    }
    function validateFields($fields, $posts) {
        $errorFields = array();
        foreach($fields as $field) {
            $type = $field['Typeof'];
            if($type == 'checkbox') {
                if($field['IsRequired']) {
                    $i=count($field['SubFields']);
                    foreach($field['SubFields'] as $subF) {
                        if(empty($posts['Field'.$subF['ColumnId']])) {
                            $i--;
                        }
                    }
                    if($i==0) {
                        $errorFields[$field['ColumnId']] = array(
                            'message' => 'This field is required. Please enter a value.'
                        );
                    }
                }
            } elseif($type == 'likert' || $type == 'address') {
                $validation = isset($field['Validation'])?$field['Validation']:'';
                $post_values = '';
                if($field['IsRequired']) {
                    if($validation == 'na' || empty($validation)) {
                        foreach($field['SubFields'] as $subF) {
                            $post_value = $posts['Field'.$subF['ColumnId']];
                            if(empty($post_value)) {
                                $errorFields[$field['ColumnId']] = array(
                                'message' => 'This field is required. Please enter a value.'
                            );
                                break;
                            }
                        }
                    } elseif($validation == 'dc') {
                        $i=count($field['SubFields']);
        				if($field['IsRequired']) {
        					foreach($field['SubFields'] as $subF) {
        						foreach($field['Choices'] as $k => $choiC) {
        							$post_value = $posts['Field'.$subF['ColumnId'].'_'.($k+1)];
        							if(!empty($post_value)) {
        								$i--;
        								break;
        							}
        						}
        					}
        				}
                        if(!$i==0) {
                            $errorFields[$field['ColumnId']] = array(
                                'message' => 'This field is required. Please enter a value.'
                            );
                        }
                    }
                }
                
            } else {
                $post_value = isset($posts['Field'.$field['ColumnId']])?$posts['Field'.$field['ColumnId']]:'';
                if($field['IsRequired']) {
                    if(empty($post_value) && $post_value !== '0') {
                        $errorFields[$field['ColumnId']] = array(
                            'message' => 'This field is required. Please enter a value.'
                        );
                    }
                }
                if(!empty($post_value)) {
                    $rangeMax = isset($field['RangeMax'])?$field['RangeMax']:'';
                    $rangeMin = isset($field['RangeMin'])?$field['RangeMin']:'0';
                    $rangeType = isset($field['RangeType'])?$field['RangeType']:'';
                    if($type == 'date' || $type == 'eurodate') {
                        $dates = isset($posts['Field'.$field['ColumnId'].'-1'])?$posts['Field'.$field['ColumnId'].'-1']:'';
                        $dates .= '/';
                        $dates .= isset($posts['Field'.$field['ColumnId'].'-2'])?$posts['Field'.$field['ColumnId'].'-2']:'';
                        $dates .= '/';
                        $dates .= isset($posts['Field'.$field['ColumnId']])?$posts['Field'.$field['ColumnId']]:'';
                        if(!$this->check_date($dates,$type)) {
                            $errorFields[$field['ColumnId']] = array(
                                'message' => 'Please enter a valid date.'
                            );
                        }
                    } elseif($type == 'email') {
                        if(!$this->check_email_address($post_value)) {
                            $errorFields[$field['ColumnId']] = array(
                                'message' => 'Please enter a valid email address.'
                            );
                        }
                    } elseif($type == 'url') {
                        if(!$this->check_url($post_value)) {
                            $errorFields[$field['ColumnId']] = array(
                                'message' => 'Please enter a valid url in http://website.com format.'
                            );
                        }
                    } elseif($type == 'date' || $type == 'eurodate') {
                        $post_values = $posts['Field'.$field['ColumnId'].(($type == 'eurodate')?'-2':'-1')] .'/'.$posts['Field'.$field['ColumnId'].(($type == 'eurodate')?'-1':'-2')].'/'.$posts['Field'.$field['ColumnId'].''];
                        $d = trim($post_values,'/');
                        if(!empty($d))
                        if(!$this->check_date($post_values,$type)) {
                            $errorFields[$field['ColumnId']] = array(
                                'message' => 'Please enter a valid date format.'
                            );
                        }
                    } elseif($type == 'time') {
                        
                        $post_values = $post_value . ':' . $posts['Field'.$field['ColumnId'].'-1'] . ':' . $posts['Field'.$field['ColumnId'].'-2'] . ' ' . $posts['Field'.$field['ColumnId'].'-3'];
                        if(!$this->check_time($post_values)) {
                            $errorFields[$field['ColumnId']] = array(
                                'message' => 'Please enter a valid time format.'
                            );
                        }
                    } elseif($type == 'phone') {
                        $post_values = $post_value . $posts['Field'.$field['ColumnId'].'-1'] . $posts['Field'.$field['ColumnId'].'-2'];
                        if(!is_numeric($post_values) || strlen($post_values) <9) {
                            $errorFields[$field['ColumnId']] = array(
                                'message' => 'Please enter a valid phone.'
                            );
                        }
                    } elseif($type == 'number') {
                        if(!is_numeric($post_value)) {
                            $errorFields[$field['ColumnId']] = array(
                                'message' => 'This field is required number not else.'
                            );
                        } elseif(!empty($rangeMax)) {
                            if($post_value > $rangeMax || $post_value < $rangeMin) {
                                $errorFields[$field['ColumnId']] = array(
                                    'message' => 'Enter a number between '.$rangeMin.' and '.$rangeMax.'.'
                                );
                            }
                        }
                    } elseif($type == 'text') {
                        $count = 0;
                        if($rangeType == 'words') {
                            $count = explode(' ',$post_value);
                            $count = count($count);
                        } elseif($rangeType == 'characters') {
                            $count = strlen($post_value);
                        }
                        if(!empty($rangeMax)) {
                            if($count > $rangeMax) {
                                $errorFields[$field['ColumnId']] = array(
                                    'message' => 'Maximum Allowed: '.$rangeMax.' '.$rangeType.'.     Currently Used: '.$count.' characters.'
                                );
                            }
                        }
                    }
                }
            }
            
        }
        return $errorFields;
    }
    function validateUpdateInfo($post) {
        $errorFields = array();
        if(!empty($post['nickh2d'])) {
            if(!$this->check_nick($post['nickh2d'])) {
                $errorFields['nickh2d'] = array(
                    'message' => 'Please enter a valid nick h2d.'
                );
            }
        } else {
            $errorFields['nickh2d'] = array(
                'message' => 'Please enter a nick h2d.'
            );
        }
        if(!empty($post['nickskype'])) {
            if(!$this->check_nick($post['nickskype'])) {
                $errorFields['nickskype'] = array(
                    'message' => 'Please enter a valid nick Skype.'
                );
            }
        } else {
            $errorFields['nickskype'] = array(
                'message' => 'Please enter a nick Skype.'
            );
        }
        if(!empty($post['fullname'])) {
            if(!$this->check_name($post['fullname'])) {
                $errorFields['fullname'] = array(
                    'message' => 'Please enter a valid name.'
                );
            }
        } else {
            $errorFields['fullname'] = array(
                'message' => 'Please enter a name.'
            );
        }
        return $errorFields;
    }
    function validateSignup($post) {
        $errorFields = array();
        if(!empty($post['username'])) {
            if(!$this->check_username($post['username'])) {
                $errorFields['username'] = array(
                    'message' => 'Please enter a valid username.'
                );
            }
        } else {
            $errorFields['username'] = array(
                'message' => 'Please enter a username.'
            );
        }
        if(!empty($post['email'])) {
            if(!$this->check_email_address($post['email'])) {
                $errorFields['email'] = array(
                    'message' => 'Please enter a valid email address.'
                );
            }
        } else {
            $errorFields['email'] = array(
                'message' => 'Please enter a email address.'
            );
        }
        if(!empty($post['password']) || !empty($post['password1'])) {
            if(!$this->check_pass($post['password'],$post['password1'])) {
                $errorFields['password'] = array(
                    'message' => 'Please enter a valid password.'
                );
            }
        } else {
            $errorFields['password'] = array(
                'message' => 'Please enter passwords.'
            );
        }
        if($this->options['show_nickh2d']) {
            if(!empty($post['nickh2d'])) {
                if(!$this->check_nick($post['nickh2d'])) {
                    $errorFields['nickh2d'] = array(
                        'message' => 'Please enter a valid nick h2d.'
                    );
                }
            } else {
                $errorFields['nickh2d'] = array(
                    'message' => 'Please enter a nick h2d.'
                );
            }
        }
        if($this->options['show_nickskype']) {
            if(!empty($post['nickskype'])) {
                if(!$this->check_nick($post['nickskype'])) {
                    $errorFields['nickskype'] = array(
                        'message' => 'Please enter a valid nick Skype.'
                    );
                }
            } else {
                $errorFields['nickskype'] = array(
                    'message' => 'Please enter a nick Skype.'
                );
            }
        }
        if(!empty($post['fullname'])) {
            if(!$this->check_name($post['fullname'])) {
                $errorFields['fullname'] = array(
                    'message' => 'Please enter a valid name.'
                );
            }
        } else {
            $errorFields['fullname'] = array(
                'message' => 'Please enter a name.'
            );
        }
        return $errorFields;
    }
    function isUTF8($string) {
        return (utf8_encode(utf8_decode($string)) == $string);
    }
    function check_name($string) {
        $string = trim($string);
        return is_string($string) && strlen($string) > 4;
    }
    function check_username($string) {
        if(strpos($string, ' ') !== false)  
            return false;
        return $this->isUTF8($string) && strlen($string) > 4 && strlen($string) < 50;
    }
    function check_nick($string) {
        if(strpos($string, ' ') !== false)  
            return false;
        return is_string($string) && strlen($string) > 4;
    }
    function check_pass($pass1, $pass2) {
        if(strpos($pass1, ' ') !== false)  
            return false;
        return $pass1 == $pass2 && strlen($pass1) > 5 && strlen($pass1) < 50;  
    }  
    function check_email_address($email) {
        if (!preg_match('/([a-zA-Z0-9_-]+)(\@)([a-zA-Z0-9_-]+)(\.)([a-zA-Z0-9]{2,4})(\.[a-zA-Z0-9]{2,4})?/',$email)) return false; return true;
    }
    function check_url($url) {
        if(filter_var($url, FILTER_VALIDATE_URL) === FALSE) return false; return true;
    }
    function check_date($date,$type) {
        $date = rtrim($date, '/');
        $ds = explode('/',$date);
        $m = ($type == 'eurodate')?(isset($ds[1])?$ds[1]:''):(isset($ds[0])?$ds[0]:'');
        $d = ($type == 'eurodate')?(isset($ds[0])?$ds[0]:''):(isset($ds[1])?$ds[1]:'');
        $y = (isset($ds[2])?$ds[2]:'');
        if(!is_numeric($m) || !is_numeric($d) || !is_numeric($y)) return false;
        return checkdate($m,$d,$y);
    }
    function check_time($time) {
        if(preg_match('/(0?\d|1[0-2]):(0\d|[0-5]\d):(0\d|[0-5]\d) (AM|PM)/i',$time, $matches))
        return $matches[0] == $time;
    }
    function adminNext($redirect,$ref = '') {
        if(!isset($_SESSION['admin_user'])) {
            header('location: '.$redirect);
            exit();
        }
    }
    function userNext($redirect) {
        if(!isset($_SESSION['user_normal']) && !isset($_SESSION['admin_user'])) {
            header('location: '.$redirect);
            exit();
        }
    }
    function modNext($redirect) {
        if(!isset($_SESSION['mod_user']) && !isset($_SESSION['admin_user'])) {
            header('location: '.$redirect);
            exit();
        }
    }
    function userPrev($redirect) {
        if(isset($_SESSION['user_normal']) || isset($_SESSION['admin_user'])) {
            header('location: '.$redirect);
            exit();
        }
    }
    function stripslashes_for_old_php_mysql($json_string) {
    	$json_string = str_replace('{\"','{"',$json_string);
    	$json_string = str_replace('\"}','"}',$json_string);
    	$json_string = str_replace('\":','":',$json_string);
    	$json_string = str_replace(':\"',':"',$json_string);
    	$json_string = str_replace('\",','",',$json_string);
    	$json_string = str_replace(',\"',',"',$json_string);
    	$json_string = str_replace('\\\"','"',$json_string);
        $json_string = str_replace('\\\n','\n',$json_string);
    	$json_string = preg_replace('#(\\\){2,}#','',$json_string);
    	$json_string = str_replace("\'","'",$json_string);
    	return ($json_string);
    }
    function json_decode_to_array($json_string) {
        /*
        * PHP < 5.3 OR your "magic_quotes_gpc" is ON Use:
        * return $this->objectToArray(json_decode($this->stripslashes_for_old_php_mysql($json_string)));
        * If Not Use:
        * return $this->objectToArray(json_decode($json_string));
        */
        return $this->objectToArray(json_decode($this->stripslashes_for_old_php_mysql($json_string)));
        return $this->objectToArray(json_decode($json_string));
    }
    function json_decode_to_array_not_in_mysql($json_string) {
    	return $this->objectToArray(json_decode(($json_string)));
    }
    function json_encode_then_escape($json_array) {
        $mysqli = $this->mysqli;
        return $mysqli->real_escape_string(json_encode($json_array));
    }
    function adjustBrightness($hex, $steps) {
        // Steps should be between -255 and 255. Negative = darker, positive = lighter
        $steps = max(-255, min(255, $steps));
    
        // Format the hex color string
        $hex = str_replace('#', '', $hex);
        if (strlen($hex) == 3) {
            $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
        }
    
        // Get decimal values
        $r = hexdec(substr($hex,0,2));
        $g = hexdec(substr($hex,2,2));
        $b = hexdec(substr($hex,4,2));
    
        // Adjust number of steps and keep it inside 0 to 255
        $r = max(0,min(255,$r + $steps));
        $g = max(0,min(255,$g + $steps));  
        $b = max(0,min(255,$b + $steps));
    
        $r_hex = str_pad(dechex($r), 2, '0', STR_PAD_LEFT);
        $g_hex = str_pad(dechex($g), 2, '0', STR_PAD_LEFT);
        $b_hex = str_pad(dechex($b), 2, '0', STR_PAD_LEFT);
    
        return '#'.$r_hex.$g_hex.$b_hex;
    }
    function checkSendConfirm($fields) {
        foreach($fields as $field) {
            if($field['Typeof'] == 'email') {
                if($field['Validation'] == 'email') {
                    return $field;
                }
            }    
        }
        return '';
    }
    function sendMail($subject, $html = '', $address = array(), $replyTo = '', $options = array()) {
        $smtp = $this->registry->smtp;
        $mail             = new PHPMailer();
        $body             = $html;
        $mail->IsSMTP(); // telling the class to use SMTP
    	$mail->SMTPSecure = 'tls';
        //$mail->Host       = $smtp['host']; // SMTP server
        $mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)
                                                   // 1 = errors and messages
                                                   // 2 = messages only
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->Host       = $smtp['host']; // sets the SMTP server
        $mail->Port       = $smtp['port']; // set the SMTP port for the GMAIL server
        $mail->Username   = $smtp['username']; // SMTP account username
        $mail->Password   = $smtp['password']; // SMTP account password
        
        //$mail->SetFrom('admin@luuledmy.vn', $options[0]);
    	$mail->SetFrom($smtp['username'], $options[0]);
        if(!empty($replyTo)) $mail->AddReplyTo($replyTo,$options[0]);
        
        $mail->Subject    = $subject;
        
        $mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
        
        $mail->MsgHTML($body);
        foreach($address as $addr => $name) {
            $mail->AddAddress($addr, $name); 
        }
        
        
        //$mail->AddAttachment("images/phpmailer.gif");      // attachment
        //$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment
        $mail->IsHTML(true); // send as HTML
    	$a = true;
        if(!$mail->Send()) {
          echo "Mailer Error: " . $mail->ErrorInfo;
    	  $a = false;
        } else {
          echo "Message sent!";
        }
    	$mail->close();
    	return $a;
    }
    function truncate($text, $length, $deli = '...', $num = 4) {
        $l = strlen($text);
        if($l <= $length) return $text;
        return substr($text,0,$length) . $deli . substr($text,-$num);
    }
    function detectLink($text,$length = 30) {
        if(!is_string($text)) return $text;
        $text = ' '.trim($text);
        $reg_exUrl = "/([^\"])(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        preg_match_all($reg_exUrl, $text, $matches);
        $usedPatterns = array();
        $done = false;
        foreach($matches[0] as $pattern){
            if(!array_key_exists($pattern, $usedPatterns)){
                $usedPatterns[$pattern]=true;
                $t = $this->truncate($pattern,$length);            
                $text = str_replace($pattern, "<a title=\"$pattern\" href=\"$pattern\" rel=\"nofollow\" target=\"blank\">$t</a>", $text);
                $done = true;
            }
        }
        return $text;
    }
    function scoring($posts, $fields) {
        $rightFieldCount = 0;
        $point = 0;
        $others = array();
        foreach($fields as $field) {
            $type = $field['Typeof'];
            if($type == 'checkbox') {
                $i=$j=0;
                $rightFieldCount++;
                foreach($field['SubFields'] as $subF) {
                    $post_value = isset($posts['Field'.$subF['ColumnId']])?$posts['Field'.$subF['ColumnId']]:'';
                    if(!empty($post_value)) {
                        $j++;
                        if($subF['IsRight']) {
                            $i++;
                        }
                    }
                    if($subF['IsRight'] && !empty($post_value)) {
                        $i++;
                        if(!empty($post_value)) {
                            $j++;
                        }
                    }
                }
                if($i==$j && $i != 0) {
                    $point++;
                }
            } elseif($type == 'likert') {
                $validation = isset($field['Validation'])?$field['Validation']:'';
                $post_values = '';
                if($validation == 'na' || empty($validation)) {
                    foreach($field['SubFields'] as $subF) {
                        $rightFieldCount++;
                        $j=$i=0;
                        $post_value = isset($posts['Field'.$subF['ColumnId']])?$posts['Field'.$subF['ColumnId']]:'';
                        foreach($field['Choices'] as $k => $choiC) {
                            if($choiC['IsRight']) {
                                $j++;
                                if(!empty($post_value) && $post_value == $choiC['Choice']) {
                                    $i++;
                                    break;
                                }
                            }
                        }
                        if($i!=0) {
                            $point++;
                        }
                    }
                } elseif($validation == 'dc') {
    				foreach($field['SubFields'] as $subF) {
                        $rightFieldCount++;
                        $j=$i=0;
    					foreach($field['Choices'] as $k => $choiC) {
    						$post_value = $posts['Field'.$subF['ColumnId'].'_'.($k+1)];
                            if(!empty($post_value)) {
    						
                                $j++;
                                if($subF['IsRight']) {
                                    $i++;
                                }
                            }
                            if($subF['IsRight'] && !empty($post_value)) {
                                $i++;
                                if(!empty($post_value)) {
                                    $j++;
                                }
                            }
    					}
                        if($i==$j) {
                            $point++;
                        }
    				}
                }
            } elseif($type == 'select' || $type == 'radio') {
                $rightFieldCount++;
                $rightInput = isset($field['InputRightAnswer'])?$field['InputRightAnswer']:'';
                $rightInput = $this->analizeInput($rightInput);
                $post_value = isset($posts['Field'.$field['ColumnId']])?$posts['Field'.$field['ColumnId']]:'';
                $i=$h=0;
                foreach($field['Choices'] as $k => $choiC) {
    				if($choiC['IsRight']) {
    				    if($type == 'radio') {
        				    if($choiC['Choice'] == 'Other') {
        				        $post_value_other = isset($posts['Field'.$field['ColumnId'].'_other_Other'])?$posts['Field'.$field['ColumnId'].'_other_Other']:'';
                                $post_value_other = $this->stripslashes_for_old_php_mysql($post_value_other);
                                if(!empty($post_value_other)) {
                                    foreach($rightInput as $ri)  {
                                       if(trim($post_value_other) == $ri) {
                                            $i++;
                                            break;
                                        } 
                                    }
                                }
                            } elseif(!empty($post_value) && $post_value == $choiC['Choice']) {
                                $i++;
                                break;
                            }
                        } elseif($type == 'select') {
                            if(!empty($post_value) && $post_value == $choiC['Choice']) {
                                $i++;
                                break;
                            }
                        }
                    }
    			}
                if($i!=0) {
                    $point++;
                }
            } elseif($type == 'text') {
                $rightFieldCount++;
                $i=0;
                $rightInput = isset($field['InputRightAnswer'])?$field['InputRightAnswer']:'';
                if(!empty($rightInput)) {
                    $rightInput = $this->analizeInput($rightInput);
                    
                    $post_value = isset($posts['Field'.$field['ColumnId']])?$posts['Field'.$field['ColumnId']]:'';
                    $post_value = $this->stripslashes_for_old_php_mysql($post_value);
                    if(!empty($post_value)) {
                        foreach($rightInput as $ri)  {
                           if(trim($post_value) == $ri) {
                                $i++;
                                break;
                            } 
                        }
                    }
                }
                if($i!=0) {
                    $point++;
                }
            } else {
                $post_value = isset($posts['Field'.$field['ColumnId']])?$posts['Field'.$field['ColumnId']]:'';
                if(!empty($post_value) || $post_value === '0') {
                    $others[] = array(
                        'MaxValue' => isset($field['RangeMax'])?$field['RangeMax']:'10',
                        'MinValue' => isset($field['RangeMin'])?$field['RangeMin']:'0',
                        'Value' => $post_value
                    );
                }
            }
        }
        return array(
            'RightFieldCount' => $rightFieldCount,
            'point' => $point,
            'other_fields' => $others
        );
    }
    function analizeInput($input) {
        $arr = explode('|', trim($input));
        return array_map('trim',$arr);
    }
    function getExcerptFields($fields,$entries) {
        $exerpts = array();
        foreach($fields as $k => $field) {
            $values =  $titles = array();
            $value = $title = '';
            $validation = isset($field['Validation'])?$field['Validation']:'';
            if(!isset($field['SubFields'])) {
                if(
                isset($entries['Field'.$field['ColumnId']]) && 
                isset($field['Excerpt']) && 
                $field['Excerpt'] == '1') {
                    $title = $field['Title'];
                    $value = $entries['Field'.$field['ColumnId']];
                    if($field['Typeof'] == 'date' || $field['Typeof'] == 'eurodate') {
                        $value = $entries['Field'.$field['ColumnId'].(($field['Typeof'] == 'eurodate')?'-2':'-1')] .'/'.$entries['Field'.$field['ColumnId'].(($field['Typeof'] == 'eurodate')?'-1':'-2')].'/'.$entries['Field'.$field['ColumnId'].''] . ' (mm/dd/yyy)';
                    } elseif($field['Typeof'] == 'time') {
                        $value = $value . ':' . $entries['Field'.$field['ColumnId'].'-1'] . ':' . $entries['Field'.$field['ColumnId'].'-2'] . ' ' . $entries['Field'.$field['ColumnId'].'-3'];
                    } elseif($field['Typeof'] == 'shortname') {
                        $value = $value . ':' . $entries['Field'.$field['ColumnId'].'-1'] . ':' . $entries['Field'.$field['ColumnId'].'-2'] . ' ' . $entries['Field'.$field['ColumnId'].'-3'];
                    }
                    $exerpts[] = array(
                        'title' => $title,
                        'value' => $this->detectLink($value,'30')
                    );
                }
            }
            
                
        }
        return $exerpts;
    }
    function curPageURL() {
        $pageURL = 'http';
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        } else {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }
    
    function getUrl() {
        return __ABS_PATH . '/index.php?req=';
    }
    
    function scoring_detail($form_nb_fields, $entry_content) {
        $scoring_results = array();
        $entries = $this->json_decode_to_array($entry_content['entry_content']);
        foreach($form_nb_fields as $k => $field) {
            $values =  $titles = array();
            $value = $title = $answer = '';
            $validation = isset($field['Validation'])?$field['Validation']:'';
            if(isset($field['SubFields']) && !empty($field['SubFields'])) {
                $j = 0;
                $h=$k2=0;
                $check_value = array();
                foreach($field['SubFields'] as $subF) {
                    if(isset($entries['Field'.$subF['ColumnId']])) {
                        if(!empty($entries['Field'.$subF['ColumnId']])) {
                            $titles[] = $subF['Title'];
                            $values[] = $entries['Field'.$subF['ColumnId']];
                        }
                    }
                    if($field['Typeof'] == 'checkbox') {
                        $post_value = isset($entries['Field'.$subF['ColumnId']])?$entries['Field'.$subF['ColumnId']]:'';
                        if(!empty($post_value)) {
                            $k2++;
                            if($subF['IsRight']) {
                                $h++;
                            }
                            $check_value[] = $subF['ColumnId'];
                        }
                        if($subF['IsRight'] && !empty($post_value)) {
                            $h++;
                            if(!empty($post_value)) {
                                $k2++;
                            }
                        }
                    } elseif($field['Typeof'] == 'likert' && $field['Validation'] == 'dc') {
                        $titles[$j] = $subF['Title'];
                        foreach($field['Choices'] as $k1 => $choiC) {
                            
                            if(isset($entries['Field'.$subF['ColumnId'].'_'.($k1+1)])) {
                                if(!empty($entries['Field'.$subF['ColumnId'].'_'.($k1+1)])) {
                                    $values[$j][] = $entries['Field'.$subF['ColumnId'].'_'.($k1+1)];
                                }
                            } else {
                                $values[$j][] = '';
                            }
                        }
                        $j++;
                    }
                }
                if($h==$k2 && $h != 0) {
                } else {
                    $answer = isset($field['WrongAnswerDescription'])?$field['WrongAnswerDescription']:'';
                    $atype = 'awrong';
                    if(!empty($check_value)) {
                        $scoring_results[] = array(
                            'FieldId' => $field['ColumnId'],
                            'Title' => $field['Title'],
                            'Value' => implode('-',$check_value),
                            'Answer' => $answer,
                            'AType' => $atype,
                            'type' => $field['Typeof'],
                            'field' => $field
                        );
                    }
                }
            } else {
                if(isset($entries['Field'.$field['ColumnId']])) {
                    $title = $field['Title'];
                    $value = $entries['Field'.$field['ColumnId']];
                    if($field['Typeof'] == 'radio' || $field['Typeof'] == 'select') {
                        foreach($field['Choices'] as $rk => $rchoiC) {
                            if($rchoiC['Choice'] == $value){
                                $value = $field['ColumnId'] . '_' . $rk;
                                break;
                            }
                        }
                    }
                    if($field['Typeof'] == 'date' || $field['Typeof'] == 'eurodate') {
                        $value = $entries['Field'.$field['ColumnId'].(($field['Typeof'] == 'eurodate')?'-2':'-1')] .'/'.$entries['Field'.$field['ColumnId'].(($field['Typeof'] == 'eurodate')?'-1':'-2')].'/'.$entries['Field'.$field['ColumnId'].''] . ' (mm/dd/yyy)';
                    } elseif($field['Typeof'] == 'time') {
                        $value = $value . ':' . $entries['Field'.$field['ColumnId'].'-1'] . ':' . $entries['Field'.$field['ColumnId'].'-2'] . ' ' . $entries['Field'.$field['ColumnId'].'-3'];
                    } elseif($field['Typeof'] == 'shortname') {
                        $value = $value . ':' . $entries['Field'.$field['ColumnId'].'-1'] . ':' . $entries['Field'.$field['ColumnId'].'-2'] . ' ' . $entries['Field'.$field['ColumnId'].'-3'];
                    }
                }
            }
            if($field['Typeof'] == 'select' || $field['Typeof'] == 'radio') {
                $post_value = isset($entries['Field'.$field['ColumnId']])?$entries['Field'.$field['ColumnId']]:'';
                $b=0;
                $rightInput = isset($field['InputRightAnswer'])?$field['InputRightAnswer']:'';
                $rightInput = $this->analizeInput($rightInput);
                foreach($field['Choices'] as $k => $choiC) {
    				if($choiC['IsRight']) {
    				    if($field['Typeof'] == 'radio') {
    				        if($choiC['Choice'] == 'Other') {
    				            $post_value_other = isset($entries['Field'.$field['ColumnId'].'_other_Other'])?$entries['Field'.$field['ColumnId'].'_other_Other']:'';
                                
                                if(!empty($post_value_other)) {
                                    $value = $post_value_other;
                                    foreach($rightInput as $ri)  {
                                       if(trim($post_value_other) == $ri) {
                                            $b++;
                                            break;
                                        } 
                                    }
                                }
    				        } else {
    				            if(!empty($post_value) && $post_value == $choiC['Choice']) {
                                    $b++;
                                }
    				        }
    				        
    				    } elseif($field['Typeof'] == 'select') {
    				        if(!empty($post_value) && $post_value == $choiC['Choice']) {
                                    $b++;
                                }
    				    }
                        
                    }
    			}
                if($b!=0) {
                } else {
                    $answer = isset($field['WrongAnswerDescription'])?$field['WrongAnswerDescription']:'';
                    $atype = 'awrong';
                    $scoring_results[] = array(
                        'FieldId' => $field['ColumnId'],
                        'Title' => $title,
                        'Value' => $value,
                        'Answer' => $answer,
                        'AType' => $atype,
                        'type' => $field['Typeof'],
                        'field' => $field
                    );
                }
                
            } elseif($field['Typeof'] == 'likert') {
            } elseif($field['Typeof'] == 'checkbox') {
            } elseif($field['Typeof'] == 'shortname') {
            } elseif($field['Typeof'] == 'address') {
            } elseif($field['Typeof'] == 'text') {
                $b=0;
                $rightInput = isset($field['InputRightAnswer'])?$field['InputRightAnswer']:'';
                $rightInput = $this->analizeInput($rightInput);
                if(!empty($value)) {
                    foreach($rightInput as $ri)  {
                       if(trim($value) == $ri) {
                            $b++;
                            break;
                        } 
                    }
                }
                if($b!=0) {
                } else {
                    $answer = isset($field['WrongAnswerDescription'])?$field['WrongAnswerDescription']:'';
                    $atype = 'awrong';
                    $scoring_results[] = array(
                        'FieldId' => $field['ColumnId'],
                        'Title' => $title,
                        'Value' => $value,
                        'Answer' => $answer,
                        'AType' => $atype,
                        'type' => $field['Typeof'],
                        'field' => $field
                    );
                }
            } else {
            }
        }
        return $scoring_results;
    }
    function get_gravatar( $email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
        $url = 'http://www.gravatar.com/avatar/';
        $url .= md5( strtolower( trim( $email ) ) );
        $url .= "?s=$s&d=$d&r=$r";
        if ( $img ) {
            $url = '<img src="' . $url . '"';
            foreach ( $atts as $key => $val )
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }
        return $url;
    }
    function encrypt($string, $key) {
        $result = '';
        for($i=0; $i<strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key))-1, 1);
            $char = chr(ord($char)+ord($keychar));
            $result.=$char;
        }
        return base64_encode($result);
    }
    
    function decrypt($string, $key) {
        $result = '';
        $string = base64_decode($string);
        
        for($i=0; $i<strlen($string); $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key))-1, 1);
            $char = chr(ord($char)-ord($keychar));
            $result.=$char;
        }
        
        return $result;
    }
    function getFieldById($fields, $field_id) {
        foreach($fields as $field) {
            if($field['ColumnId'] == $field_id) {
                return array('0' => $field);
            }
        }
        return false;
    }
    function getPostCount($posts, $mixed = 'Field') {
        $i = 0;
        foreach($posts as $kp => $post) {
            if(strpos($kp,$mixed) !== false) {
                $i++;
            }
        }
        return $i;
    }
    function startsWith($haystack, $needle) {
        return !strncmp($haystack, $needle, strlen($needle));
    }
    
    function endsWith($haystack, $needle) {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }
    
        return (substr($haystack, -$length) === $needle);
    }
}