<?php
if ( ! defined( 'GETOVER' ) ) exit;
class Applications_Template
{
    private $registry, $vars = array(), $helpers, $db_prefix = DB_PREFIX, $options = array();
    function __construct($registry) {
        $this->registry = $registry;
        $this->helpers = $registry->helpers;
        $this->options = $registry->options;
    }
    function __set($index, $val)
    {
        $this->vars[$index] = $val;
    }
    function __get($index) {
        return isset($this->vars[$index])?$this->vars[$index]:'';
    }
    function render($file, $mode = 'all')
    {
        foreach ($this->vars as $k => $v) {
            $$k = $v;
        }
        $file = __SITE_PATH . '/views/' . $file . '.tpl.php';
        if (!file_exists($file)) {
            include (__SITE_PATH . '/template/header.tpl.php');
            
            include (__SITE_PATH . '/views/index.tpl.php');
            
            include (__SITE_PATH . '/template/footer.tpl.php');
        } else {
            if ($mode == 'only') {
                include ($file);
                return;
            }
            include (__SITE_PATH . '/template/header.tpl.php');
            
            include ($file);
            
            include (__SITE_PATH . '/template/footer.tpl.php');
        }
    }
    function getOptions($option_results, $selected_id, $key_id) {
        $html = '';
        foreach($option_results as $option) :
            $selected = ($selected_id == $option[$key_id])?' selected="selected"':'';
            $html .= '<option value="'.$option[$key_id].'"'.$selected.'>';
    			$html .= $option['theme_name'];
    		$html .= '</option>';
        endforeach;
        return $html;
    }
}
